﻿using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LaRenzo.WebApi.Mappers
{
    public class DriverOrderMapper
    {
        private readonly int _sessionID;

        public DriverOrderMapper(int sessionID)
        {
            _sessionID = sessionID;
        }

        public DriverOrderDto Map(Order order)
        {
            string address = order.Street;
            if (!string.IsNullOrEmpty(order.House))
                address += $" {order.House}";
            if (!string.IsNullOrEmpty(order.Appartament) && !order.Appartament.Equals("0"))
                address += $", кв. {order.Appartament}";
            string clientName = order.Client.LastName;
            if (!string.IsNullOrEmpty(order.Client.FirstName))
                clientName += $" {order.Client.FirstName}";
            if (!string.IsNullOrEmpty(order.Client.MiddleName))
                clientName += $" {order.Client.MiddleName}";
            return new DriverOrderDto
            {
                ID = order.ID,
                Code = order.Code,
                IsDelivered = Equals(order.State?.Name, "Доставлен") || Equals(order.State?.Name, "Оплачен"),
                Address = address,
                ClientName = clientName,
                ClientPhone = order.Phone,
                Comment = order.Additional,
                PaymentMethod = order.PaymentMethod,
                TotalCost = order.TotalCost,
                SessionId = _sessionID
            };
        }
    }
}