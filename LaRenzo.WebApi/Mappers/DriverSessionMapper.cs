﻿using LaRenzo.DataRepository.Entities.Documents.Sessions;
using LaRenzo.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LaRenzo.WebApi.Mappers
{
    public class DriverSessionMapper
    {
        public DriverSessionDto Map(Session session)
        {
            return new DriverSessionDto
            {
                ID = session.ID,
                Opened = session.Opened.ToString("yyyy-MM-ddTHH:mm:ss"),
                Closed = (session.Closed ?? session.Opened.AddHours(24)).ToString("yyyy-MM-ddTHH:mm:ss")
            };
        }
    }
}