﻿using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Drivers;
using LaRenzo.DataRepository.Entities.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Catalogs.Drivers;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Others;
using LaRenzo.WebApi.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace LaRenzo.WebApi.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;
        private readonly IUserManager _userManager;
        private readonly IDriverManager _driverManager;
        private readonly List<ClientCredential> _clientCredentials;
        private readonly string[] _internalServices;

        public ApplicationOAuthProvider(string publicClientId, IUserManager userManager, IDriverManager driverManager)
        {
            _publicClientId = publicClientId ?? throw new ArgumentNullException("publicClientId");
            _userManager = userManager;
            _driverManager = driverManager;
            _clientCredentials = new ClientCredentialsParser().ParseClientCredentials(WebConfigurationManager.AppSettings["clientCredentials"]);
            _internalServices = WebConfigurationManager.AppSettings["internalServices"].Replace(" ", "").Split(',');
        }

        public override Task GrantClientCredentials(OAuthGrantClientCredentialsContext context)
        {
            var claims = context.Scope.Select(x => new Claim("urn:oauth:scope", x)).ToList();
            if ( _internalServices.Contains(context.ClientId))
            {
                claims.Add(new Claim(ClaimTypes.Role, "InternalServices"));
            }

            var identity = new ClaimsIdentity(new GenericIdentity(
                    context.ClientId, OAuthDefaults.AuthenticationType), claims);
            context.Validated(identity);
            return Task.CompletedTask;
        }


        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            string userName = context.UserName?.Trim();
            if (string.IsNullOrEmpty(userName))
            {
                context.SetError("EmptyUserName", "UserName must not be null.");
                return;
            }
            string password = context.Password?.Trim();
            if (string.IsNullOrEmpty(password))
            {
                context.SetError("EmptyPassword", "Password must not be null.");
                return;
            }
            User user = _userManager.Login(userName, password);
            Driver driver = null;
            if (user == null)
            {
                driver = await _driverManager.LoginAsync(userName, password);
                if (driver == null)
                {
                    context.SetError("InvalidLoginOrPassword", "The user name or password is incorrect.");
                    return;
                }
            }
            
            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();

            var applicationUser = new ApplicationUser
            {
                Id = user != null ? user.ID.ToString() : driver.ID.ToString(),
                UserName = user != null ? user.Name : driver.Name,
                Roles = new List<string>
                {
                    user != null ? "User" : "Driver"
                }
            };
            ClaimsIdentity oAuthIdentity = await applicationUser.GenerateUserIdentityAsync(userManager,
               OAuthDefaults.AuthenticationType);
            ClaimsIdentity cookiesIdentity = await applicationUser.GenerateUserIdentityAsync(userManager,
                CookieAuthenticationDefaults.AuthenticationType);

            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, new AuthenticationProperties());
            context.Validated(ticket);
            context.Request.Context.Authentication.SignIn(cookiesIdentity);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.CompletedTask;
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId;
            string clientSecret;
            context.TryGetFormCredentials(out clientId, out clientSecret);

            if (clientId == null)
            {
                context.Validated();
            }
            else if (_clientCredentials.FirstOrDefault(x => x.ClientId.Equals(clientId) && x.ClientSecret.Equals(clientSecret)) != null)
            {
                context.Validated(clientId);
            }

            return base.ValidateClientAuthentication(context);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.CompletedTask;
        }
    }
}