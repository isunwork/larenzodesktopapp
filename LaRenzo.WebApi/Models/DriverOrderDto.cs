﻿using LaRenzo.TransferLibrary;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LaRenzo.WebApi.Models
{
    public class DriverOrderDto
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Address { get; set; }
        public bool IsDelivered { get; set; }
        public string ClientName { get; set; }
        public string ClientPhone { get; set; }
        public string Comment { get; set; }
        public int TotalCost { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public PaymentMethod PaymentMethod { get; set; }
        public int SessionId { get; set; }
    }
}