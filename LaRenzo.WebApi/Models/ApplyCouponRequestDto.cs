﻿using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LaRenzo.WebApi.Models
{
    public class ApplyCouponRequestDto
    {
        public string Code { get; set; }
        public string Brand { get; set; }
        public decimal Sum { get; set; }
        public int[] Items { get; set; }
    }
}