﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LaRenzo.WebApi.Models
{
    public class ApplyCouponResponseDto
    {
        public decimal Sum { get; set; }
        public string LogMessage { get; set; }
        public string Description { get; set; }
    }
}