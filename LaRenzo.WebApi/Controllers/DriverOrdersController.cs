﻿using DataLib.Models;
using LaRenzo.DataRepository.Repositories.Documents.Orders;
using LaRenzo.WebApi.Models;
using LaRenzo.WebApi.Stub;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace LaRenzo.WebApi.Controllers
{
    [RoutePrefix("api/driver/orders")]
    [Authorize(Roles = "Driver")]
    public class DriverOrdersController : BaseApiController
    {
        [Inject]
        public IOrderManager OrderManager { get; set; }

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public DriverOrdersController()
        {
        }

        [Route("sessions")]
        [HttpGet]
        public AllDriverExistedSessionsResponseDto GetAllExistedSessions()
        {
            try
            {
                string logMessage = "";
                _logger.Info(logMessage += $"Получение историю сессий для водителя {UserId}.\r\n");

                var sessions = OrderManager.GetAllExistedSessionsForDriver(UserId);
                if (sessions == null || sessions.Count == 0)
                {
                    _logger.Info(logMessage += $"История сессий для водителя {UserId} пуста.\r\n");
                    return new AllDriverExistedSessionsResponseDto();
                }
                _logger.Info(logMessage += $"История сессий для водителя {UserId} содержит {sessions.Count}.\r\n");
                var sessionMapper = new Mappers.DriverSessionMapper();
                var sessionDtos = sessions
                    .Select(sessionMapper.Map)
                    .ToList();
                return new AllDriverExistedSessionsResponseDto
                {
                    Sessions = sessionDtos
                };
            }
            catch (Exception e)
            {
                _logger.Error($"Произошла ошибка при получении истории сессий для водителя {UserId}.");
                _logger.Error(e);
                throw;
            }
        }

        [Route("")]
        [Route("{sessionID}")]
        [HttpGet]
        public DriverOrdersResponseDto GetOrders(int sessionID = 0)
        {
            try
            {
                string logMessage = "";
                _logger.Info(logMessage += $"Получение информации о заказах для водителя {UserId}.\r\n");
                
                var orders = OrderManager.GetListForDriver(UserId, sessionID);
                //orders = OrdersStorageStub.GetOrders();
                if (orders == null || orders.Count == 0)
                {
                    _logger.Info(logMessage += $"Список заказов для водителя {UserId} пуст.\r\n");
                    return new DriverOrdersResponseDto();
                }
                _logger.Info(logMessage += $"Количество заказов для водителя {UserId} {orders.Count}.\r\n");
                var orderMapper = new Mappers.DriverOrderMapper(sessionID);
                var orderDtos = orders
                    .Select(orderMapper.Map)
                    .ToList();
                return new DriverOrdersResponseDto
                {
                    Orders = orderDtos
                };
            }
            catch (Exception e)
            {
                _logger.Error($"Произошла ошибка при получении заказов для водителя {UserId}.");
                _logger.Error(e);
                throw;
            }            
        }

        [Route("{orderId}/delivered")]
        [HttpPut]
        [Authorize(Roles = "Driver")]
        public void SetOrderStateToDelivered(int orderId)
        {
            try
            {
                string logMessage = "";
                _logger.Info(logMessage += $"Меняем статус заказа {orderId} на доставлен.\r\n");

                OrderManager.SetOrderStateToDelivered(orderId);
            }
            catch (Exception e)
            {
                _logger.Error($"Произошла ошибка при смене статуса заказа {orderId}.");
                _logger.Error(e);
                throw;
            }
        }
    }
}
