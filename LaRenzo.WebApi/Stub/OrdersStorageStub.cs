﻿using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LaRenzo.WebApi.Stub
{
    public class OrdersStorageStub
    {
        public static List<Order> GetOrders()
        {
            return new List<Order>
            {
                new DataRepository.Entities.Documents.DeliveryOrders.Order
                {
                    Additional = "gjfgjf",
                    Client = new DataRepository.Entities.Catalogs.Client.Client
                    {
                        FirstName = "Руслан",
                        MiddleName = "Борисович",
                        LastName = "Рахманкулов"
                    },
                    Code = "67890",
                    ID = 7777,
                    House = "8б",
                    Street = "Фадеева",
                    Appartament = "9",
                    TotalCost = 450,
                    PaymentMethod  = TransferLibrary.PaymentMethod.OnlineCard,
                    Phone = "89241213448"
                },
            new DataRepository.Entities.Documents.DeliveryOrders.Order
            {
                Additional = "asfas",
                Client = new DataRepository.Entities.Catalogs.Client.Client
                {
                    FirstName = "Руслан",
                    MiddleName = "Борисович",
                    LastName = "Рахманкулов"
                },
                Code = "1234",
                ID = 12345,
                House = "8б",
                Street = "Фадеева",
                Appartament = "9",
                TotalCost = 1200,
                PaymentMethod = TransferLibrary.PaymentMethod.Cash,
                Phone = "89241213448"
            }
        };
        }
    }
}