﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace LaRenzo.CallCenter.Authentification
{
    public interface IView
    {
        void Show();
        Task Initialize();
    }
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window, IView
    {
        public LoginWindow(AuthenticationViewModel viewModel)
        {
            ViewModel = viewModel;
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(MainWindow_Loaded);
        }

        public async Task Initialize()
        {
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            TraversalRequest tRequest = new TraversalRequest(FocusNavigationDirection.Next);
            this.MoveFocus(tRequest);

            this.Loaded -= MainWindow_Loaded;
        }

        #region IView Members
        public IViewModel ViewModel
        {
            get { return DataContext as IViewModel; }
            set { DataContext = value; }
        }
        #endregion

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var model = (AuthenticationViewModel) ViewModel;
            if (model.IsAuthenticated && !model.AllActionsDone)
            {
                var result = MessageBox.Show("Вы авторизовались, идёт загрузка данных, вы уверены что хотите выйти?", "Прерывание загрузки", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.No)
                {
                    e.Cancel = true;
                }
            }
            else if (model.Authentication && !model.AllActionsDone)
            {
                var result = MessageBox.Show("Вы в процессе авторизации, идёт загрузка данных, вы уверены что хотите выйти?", "Прерывание загрузки", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.No)
                {
                    e.Cancel = true;
                }
            }
        }
    }
}
