﻿using System;
using System.Windows;
using System.Windows.Data;

namespace LaRenzo.CallCenter.Utils
{
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class VisabilityBooleanConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (targetType != typeof(Visibility))
                throw new InvalidOperationException("The target must be a boolean");
            if (value != null && (bool) value)
            {
                return Visibility.Visible;
            }
            else
            {
                return Visibility.Hidden;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
