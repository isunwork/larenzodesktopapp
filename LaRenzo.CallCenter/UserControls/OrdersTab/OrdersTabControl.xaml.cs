﻿using System.Threading.Tasks;
using System.Windows.Controls;

namespace LaRenzo.CallCenter.UserControls.OrdersTab
{
    /// <summary>
    /// Interaction logic for OrdersTabControl.xaml
    /// </summary>
    public partial class OrdersTabControl
    {
        public OrdersTabControl()
        {
            InitializeComponent();
        }

        public async Task Initialization()
        {
            var branches = DataRepository.Repositories.Content.BranchRepository.GetItems();
            foreach (var branch in branches)
            {
                var brands = DataRepository.Repositories.Content.BrandManager.GetList(null,branch.ID);
                foreach (var brand in brands)
                {
                    var header = $"Филиал: {branch.BranchName}, бренд: {brand.Name}";
                    var tabItem = new TabItem();
                    tabItem.Header = header;
                    var content = new TabItemUserControl();
                    content.BranchId = branch.ID;
                    content.BrandId = brand.ID;
                    await content.Initialize();
                    tabItem.Content = content;
                    TabControlOrganizations.Items.Add(tabItem);
                }
            }
        }

        private void TabControlOrganizations_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems[0] is TabItem item)
            {
                var content = (TabItemUserControl) item.Content;
                DataRepository.Repositories.Content.BranchRepository.SelectedBranchId = content.BranchId;
            }
        }
    }
}
