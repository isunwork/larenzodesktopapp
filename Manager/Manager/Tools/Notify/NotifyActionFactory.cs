﻿using System;
using System.Windows.Forms;
using LaRenzo.Forms.Documents.SessionForms;
using LaRenzo.Forms.Documents.SupplyForms;
using LaRenzo.FormsProvider;

namespace LaRenzo.Tools.Notify
{
    /// <summary>
    /// Отвечает за создание NotifyAction для разных типов событий
    /// </summary>
    public static class NotifyActionFactory
    {
        public static Action GetAction(NotifyOwnerType owner)
        {
            switch (owner)
            {
                case NotifyOwnerType.SessionManager:
                    return GetFormAction<SessionListForm>();
                
                case NotifyOwnerType.SupplyManager:
                    return GetFormAction<SupplyListForm>();
                
                default:
                    return null;
            }
        }


        private static Action GetFormAction<T>() where T : Form
        {
            return SingletonFormProvider<SessionListForm>.GetShowFormAction(() => new SessionListForm());
        }

    }
}
