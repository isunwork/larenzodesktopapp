﻿namespace LaRenzo.Tools.Notify
{
    /// <summary>
    /// Список типов вызыв
    /// </summary>
    public enum NotifyOwnerType
    {
        SessionManager,
        SupplyManager
    }
}
