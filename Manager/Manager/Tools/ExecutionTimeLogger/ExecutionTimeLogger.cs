﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using LaRenzo.Properties;

namespace LaRenzo.Tools
{
    public static class ExecutionTimeLogger
    {
        private static readonly Dictionary<Guid, Stopwatch> Counters = new Dictionary<Guid, Stopwatch>();
        public static BindingList<ExucutionTimeLogEntry> Logs = new BindingList<ExucutionTimeLogEntry>();

   
        /// <summary>
        /// Запускает таймер
        /// </summary>
        /// <returns>Ключ, по которому можно остановить таймер и создать запись в логе</returns>
        public static Guid Start()
        {
            var guid = Guid.NewGuid();
            Counters.Add(guid, Stopwatch.StartNew());
            CheckLogSize();
            return guid;
        }


        private static void CheckLogSize()
        {
            var max = Settings.Default.MaxLoggerSize;
            if (Logs.Count > max) Logs.Remove(Logs.OrderBy(x => x.Created).First());
        }

        /// <summary>
        /// Останавливает таймер и добавляет запись в лог
        /// </summary>
        /// <param name="key">Ключ, полученный функцией Start()</param>
        /// <param name="logMessage">Сообщение для лога</param>
        /// <param name="warningDelay">Если время работы функции больше указаного (в секундах), вывести предупреждение</param>
        /// <param name="comment">Предупреждение, при превышение warningDelay</param>
        public static void Stop(Guid key, string logMessage, int warningDelay = 0, string comment = "")
        {
            var localCounter = Counters.ContainsKey(key) ? Counters[key] : null;
            
            if (localCounter == null) return;

            localCounter.Stop();

            Logs.Add(new ExucutionTimeLogEntry
            {
                Created = DateTime.Now,
                Elapsed = localCounter.Elapsed,
                Message = logMessage,
                TooLong = warningDelay > 0 && localCounter.Elapsed.Seconds > warningDelay
            });

            Counters.Remove(key);

            
        }

		

		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить путь к файлу производительности </summary>
		////=============================================================
		public static string GetPowerFileBody()
		{
			BindingList<ExucutionTimeLogEntry> poverWorkItems = Logs;

			var sb = new StringBuilder();

			foreach (var item in poverWorkItems)
			{
				sb.AppendLine(item.Created + " " + item.Message + " " + item.Elapsed + " " + item.TooLong);
			}
			
			return sb.ToString();
		}

    }

    public class ExucutionTimeLogEntry
    {
        public DateTime Created { get; set; }
        public TimeSpan Elapsed { get; set; }
        public string Message { get; set; }
        public bool TooLong { get; set; }

        public override string ToString()
        {
            return String.Format("{0:g} : {1} | {2:F2}", Created, Message, Elapsed);
        }
    }
}
