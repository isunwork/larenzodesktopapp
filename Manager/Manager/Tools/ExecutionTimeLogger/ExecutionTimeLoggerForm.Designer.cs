﻿namespace LaRenzo.Tools
{
    partial class ExecutionTimeLoggerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnMessage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnEpalsed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(475, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.Visible = false;
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::LaRenzo.Properties.Resources.from_file;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Сохранить в файл";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click_1);
            // 
            // gridControl
            // 
            this.gridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 0);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(475, 426);
            this.gridControl.TabIndex = 1;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnCreated,
            this.columnMessage,
            this.columnEpalsed});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView_RowStyle);
            // 
            // columnCreated
            // 
            this.columnCreated.Caption = "Дата и время";
            this.columnCreated.DisplayFormat.FormatString = "HH:mm:ss";
            this.columnCreated.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.columnCreated.FieldName = "Created";
            this.columnCreated.Name = "columnCreated";
            this.columnCreated.OptionsColumn.AllowEdit = false;
            this.columnCreated.OptionsColumn.ReadOnly = true;
            this.columnCreated.Visible = true;
            this.columnCreated.VisibleIndex = 0;
            // 
            // columnMessage
            // 
            this.columnMessage.Caption = "Операция";
            this.columnMessage.FieldName = "Message";
            this.columnMessage.Name = "columnMessage";
            this.columnMessage.OptionsColumn.AllowEdit = false;
            this.columnMessage.OptionsColumn.ReadOnly = true;
            this.columnMessage.Visible = true;
            this.columnMessage.VisibleIndex = 1;
            // 
            // columnEpalsed
            // 
            this.columnEpalsed.Caption = "Время выполнения";
            this.columnEpalsed.FieldName = "Elapsed";
            this.columnEpalsed.Name = "columnEpalsed";
            this.columnEpalsed.OptionsColumn.AllowEdit = false;
            this.columnEpalsed.OptionsColumn.ReadOnly = true;
            this.columnEpalsed.Visible = true;
            this.columnEpalsed.VisibleIndex = 2;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // ExecutionTimeLoggerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 426);
            this.Controls.Add(this.gridControl);
            this.Controls.Add(this.toolStrip1);
            this.Name = "ExecutionTimeLoggerForm";
            this.Text = "Статистика производительности";
            this.Load += new System.EventHandler(this.ExecutionTimeLoggerForm_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnCreated;
        private DevExpress.XtraGrid.Columns.GridColumn columnMessage;
        private DevExpress.XtraGrid.Columns.GridColumn columnEpalsed;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}