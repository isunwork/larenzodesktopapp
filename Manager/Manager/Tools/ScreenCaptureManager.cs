﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace LaRenzo.Tools
{
	public class ScreenCaptureManager
	{

		public string CaptureAndGetCaptureScreenImageName(bool isUseOldFile)
		{
			string fullFilePath = GetFilePathToCaptureImage();

			if (!isUseOldFile)
			{
				Bitmap bmp = GetCaptureScreenBitmap();
				SaveBitmapToFile(bmp, fullFilePath);
			}

			return fullFilePath;
		}


		private Bitmap GetCaptureScreenBitmap()
		{
			// Получить характеристики экрана
			Rectangle bounds = Screen.PrimaryScreen.Bounds;
			PixelFormat pixelFormat = GetPixelFormat();

			// Создать картинку
			var captured = new Bitmap(bounds.Width, bounds.Height, pixelFormat);

			// Получить экран
			Graphics gdi = Graphics.FromImage(captured);
			gdi.CopyFromScreen(bounds.Left, bounds.Top, 0, 0, bounds.Size);
			
			return captured;
		}


		private void SaveBitmapToFile(Bitmap bmp, string filePath)
		{
			bmp.Save(filePath, ImageFormat.Jpeg);
		}


		private string GetFilePathToCaptureImage()
		{
			DateTime curentDate = DateTime.Now;
			string fileName = string.Format("{0}_{1}_{2}_{3}_{4}_{5}_{6}.jpg", curentDate.Year, curentDate.Month, curentDate.Day, curentDate.Hour, curentDate.Minute, curentDate.Second, curentDate.Millisecond);
			string folderPath = GetFolderPath();
			string fullFilePath = folderPath + fileName;

			return fullFilePath;
		}


		private string GetFolderPath()
		{
			string appPath = Application.StartupPath;
			string folderPath = appPath + "\\ScreenCaptureImages\\";

			if (!Directory.Exists(folderPath))
			{
				Directory.CreateDirectory(folderPath);
			}

			return folderPath;
		}


		private PixelFormat GetPixelFormat()
		{
			int colourDepth = Screen.PrimaryScreen.BitsPerPixel;
			
			PixelFormat pixelFormat;
			switch (colourDepth)
			{
				case 8:
				case 16:
					pixelFormat = PixelFormat.Format16bppRgb565;
					break;

				case 24:
					pixelFormat = PixelFormat.Format24bppRgb;
					break;

				case 32:
					pixelFormat = PixelFormat.Format32bppArgb;
					break;

				default:
					pixelFormat = PixelFormat.Format32bppArgb;
					break;
			}

			return pixelFormat;
		}

	}
}
