﻿namespace LaRenzo.Forms.Parameters.Brand
{
	partial class BrandList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.productGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.subColumnId = new DevExpress.XtraGrid.Columns.GridColumn();
			this.subColumnName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.subColumnIsSet = new DevExpress.XtraGrid.Columns.GridColumn();
			this.subColumnMeasure = new DevExpress.XtraGrid.Columns.GridColumn();
			this.subColumnMinAmount = new DevExpress.XtraGrid.Columns.GridColumn();
			this.mainGridControl = new DevExpress.XtraGrid.GridControl();
			this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.idColumn = new DevExpress.XtraGrid.Columns.GridColumn();
			this.nameColumn = new DevExpress.XtraGrid.Columns.GridColumn();
			this.productsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.nameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
			this.mainToolstrip = new System.Windows.Forms.ToolStrip();
			this.addButton = new System.Windows.Forms.ToolStripButton();
			this.separator = new System.Windows.Forms.ToolStripSeparator();
			this.copyButton = new System.Windows.Forms.ToolStripButton();
			this.editButton = new System.Windows.Forms.ToolStripButton();
			this.deleteButton = new System.Windows.Forms.ToolStripButton();
			this.separator2 = new System.Windows.Forms.ToolStripSeparator();
			this.refreshButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.searchButton = new System.Windows.Forms.ToolStripButton();
			((System.ComponentModel.ISupportInitialize)(this.productGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.productsGridView)).BeginInit();
			this.mainToolstrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// productGridView
			// 
			this.productGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.subColumnId,
            this.subColumnName,
            this.subColumnIsSet,
            this.subColumnMeasure,
            this.subColumnMinAmount});
			this.productGridView.GridControl = this.mainGridControl;
			this.productGridView.Name = "productGridView";
			// 
			// subColumnId
			// 
			this.subColumnId.Caption = "ИД";
			this.subColumnId.FieldName = "Product.ID";
			this.subColumnId.Name = "subColumnId";
			this.subColumnId.OptionsColumn.AllowEdit = false;
			this.subColumnId.OptionsColumn.ReadOnly = true;
			this.subColumnId.Visible = true;
			this.subColumnId.VisibleIndex = 0;
			// 
			// subColumnName
			// 
			this.subColumnName.Caption = "Ниаменование";
			this.subColumnName.FieldName = "Product.Name";
			this.subColumnName.Name = "subColumnName";
			this.subColumnName.OptionsColumn.AllowEdit = false;
			this.subColumnName.OptionsColumn.ReadOnly = true;
			this.subColumnName.Visible = true;
			this.subColumnName.VisibleIndex = 1;
			// 
			// subColumnIsSet
			// 
			this.subColumnIsSet.Caption = "Составной";
			this.subColumnIsSet.FieldName = "Product.IsSetProduct";
			this.subColumnIsSet.Name = "subColumnIsSet";
			this.subColumnIsSet.OptionsColumn.AllowEdit = false;
			this.subColumnIsSet.OptionsColumn.ReadOnly = true;
			this.subColumnIsSet.Visible = true;
			this.subColumnIsSet.VisibleIndex = 2;
			// 
			// subColumnMeasure
			// 
			this.subColumnMeasure.Caption = "Ед.Изм";
			this.subColumnMeasure.FieldName = "Product.Measure.ShortName";
			this.subColumnMeasure.Name = "subColumnMeasure";
			this.subColumnMeasure.OptionsColumn.AllowEdit = false;
			this.subColumnMeasure.OptionsColumn.ReadOnly = true;
			this.subColumnMeasure.Visible = true;
			this.subColumnMeasure.VisibleIndex = 3;
			// 
			// subColumnMinAmount
			// 
			this.subColumnMinAmount.Caption = "Мин. остаток";
			this.subColumnMinAmount.FieldName = "Product.MinAmount";
			this.subColumnMinAmount.Name = "subColumnMinAmount";
			this.subColumnMinAmount.OptionsColumn.AllowEdit = false;
			this.subColumnMinAmount.OptionsColumn.ReadOnly = true;
			this.subColumnMinAmount.Visible = true;
			this.subColumnMinAmount.VisibleIndex = 4;
			// 
			// mainGridControl
			// 
			this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
			this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainGridControl.Location = new System.Drawing.Point(0, 31);
			this.mainGridControl.MainView = this.mainGridView;
			this.mainGridControl.Name = "mainGridControl";
			this.mainGridControl.Size = new System.Drawing.Size(710, 424);
			this.mainGridControl.TabIndex = 4;
			this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView,
            this.productsGridView,
            this.productGridView});
			// 
			// mainGridView
			// 
			this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.idColumn,
            this.nameColumn});
			this.mainGridView.GridControl = this.mainGridControl;
			this.mainGridView.Name = "mainGridView";
			this.mainGridView.OptionsFind.AlwaysVisible = true;
			this.mainGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
			this.mainGridView.OptionsSelection.EnableAppearanceFocusedRow = false;
			this.mainGridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.MainGridViewRowStyle);
			this.mainGridView.DoubleClick += new System.EventHandler(this.MainGridViewDoubleClick);
			// 
			// idColumn
			// 
			this.idColumn.Caption = "Идентификатор";
			this.idColumn.FieldName = "ID";
			this.idColumn.Name = "idColumn";
			this.idColumn.OptionsColumn.AllowEdit = false;
			this.idColumn.OptionsColumn.ReadOnly = true;
			this.idColumn.Width = 143;
			// 
			// nameColumn
			// 
			this.nameColumn.Caption = "Название";
			this.nameColumn.FieldName = "Name";
			this.nameColumn.Name = "nameColumn";
			this.nameColumn.OptionsColumn.AllowEdit = false;
			this.nameColumn.OptionsColumn.ReadOnly = true;
			this.nameColumn.Visible = true;
			this.nameColumn.VisibleIndex = 0;
			this.nameColumn.Width = 235;
			// 
			// productsGridView
			// 
			this.productsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.nameGridColumn});
			this.productsGridView.GridControl = this.mainGridControl;
			this.productsGridView.Name = "productsGridView";
			// 
			// nameGridColumn
			// 
			this.nameGridColumn.Caption = "Название";
			this.nameGridColumn.FieldName = "Products.Name";
			this.nameGridColumn.Name = "nameGridColumn";
			this.nameGridColumn.Visible = true;
			this.nameGridColumn.VisibleIndex = 0;
			// 
			// mainToolstrip
			// 
			this.mainToolstrip.ImageScalingSize = new System.Drawing.Size(24, 24);
			this.mainToolstrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addButton,
            this.separator,
            this.copyButton,
            this.editButton,
            this.deleteButton,
            this.separator2,
            this.refreshButton,
            this.toolStripSeparator1,
            this.searchButton});
			this.mainToolstrip.Location = new System.Drawing.Point(0, 0);
			this.mainToolstrip.Name = "mainToolstrip";
			this.mainToolstrip.Size = new System.Drawing.Size(710, 31);
			this.mainToolstrip.TabIndex = 3;
			this.mainToolstrip.Text = "toolStrip1";
			// 
			// addButton
			// 
			this.addButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.addButton.Image = global::LaRenzo.Properties.Resources.add_wh;
			this.addButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.addButton.Name = "addButton";
			this.addButton.Size = new System.Drawing.Size(28, 28);
			this.addButton.Text = "Создать";
			this.addButton.Click += new System.EventHandler(this.AddButtonClick);
			// 
			// separator
			// 
			this.separator.Name = "separator";
			this.separator.Size = new System.Drawing.Size(6, 31);
			// 
			// copyButton
			// 
			this.copyButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.copyButton.Image = global::LaRenzo.Properties.Resources.copy;
			this.copyButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.copyButton.Name = "copyButton";
			this.copyButton.Size = new System.Drawing.Size(28, 28);
			this.copyButton.Text = "Скопировать";
			this.copyButton.Click += new System.EventHandler(this.CopyButtonClick);
			// 
			// editButton
			// 
			this.editButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.editButton.Image = global::LaRenzo.Properties.Resources.edit_wh;
			this.editButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.editButton.Name = "editButton";
			this.editButton.Size = new System.Drawing.Size(28, 28);
			this.editButton.Text = "Редактировать";
			this.editButton.Click += new System.EventHandler(this.EditButtonClick);
			// 
			// deleteButton
			// 
			this.deleteButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.deleteButton.Image = global::LaRenzo.Properties.Resources.close_wh;
			this.deleteButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.deleteButton.Name = "deleteButton";
			this.deleteButton.Size = new System.Drawing.Size(28, 28);
			this.deleteButton.Text = "Пометить на удаление";
			this.deleteButton.Click += new System.EventHandler(this.DeleteButtonClick);
			// 
			// separator2
			// 
			this.separator2.Name = "separator2";
			this.separator2.Size = new System.Drawing.Size(6, 31);
			// 
			// refreshButton
			// 
			this.refreshButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.refreshButton.Image = global::LaRenzo.Properties.Resources.refresh;
			this.refreshButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.refreshButton.Name = "refreshButton";
			this.refreshButton.Size = new System.Drawing.Size(28, 28);
			this.refreshButton.Text = "Обновить";
			this.refreshButton.Click += new System.EventHandler(this.RefreshButtonClick);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
			// 
			// searchButton
			// 
			this.searchButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.searchButton.Image = global::LaRenzo.Properties.Resources.search;
			this.searchButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.searchButton.Name = "searchButton";
			this.searchButton.Size = new System.Drawing.Size(28, 28);
			this.searchButton.Text = "Найти";
			this.searchButton.Click += new System.EventHandler(this.SearchButtonClick);
			// 
			// BrandList
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(710, 455);
			this.Controls.Add(this.mainGridControl);
			this.Controls.Add(this.mainToolstrip);
			this.Name = "BrandList";
			this.Text = "Бренды";
			((System.ComponentModel.ISupportInitialize)(this.productGridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.productsGridView)).EndInit();
			this.mainToolstrip.ResumeLayout(false);
			this.mainToolstrip.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip mainToolstrip;
        private System.Windows.Forms.ToolStripButton addButton;
        private System.Windows.Forms.ToolStripSeparator separator;
        private System.Windows.Forms.ToolStripButton copyButton;
        private System.Windows.Forms.ToolStripButton editButton;
        private System.Windows.Forms.ToolStripButton deleteButton;
        private System.Windows.Forms.ToolStripSeparator separator2;
        private DevExpress.XtraGrid.GridControl mainGridControl;
		private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
		private DevExpress.XtraGrid.Columns.GridColumn nameColumn;
		private System.Windows.Forms.ToolStripButton refreshButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripButton searchButton;
		private DevExpress.XtraGrid.Columns.GridColumn idColumn;
		private DevExpress.XtraGrid.Views.Grid.GridView productsGridView;
		private DevExpress.XtraGrid.Columns.GridColumn nameGridColumn;
		private DevExpress.XtraGrid.Views.Grid.GridView productGridView;
		private DevExpress.XtraGrid.Columns.GridColumn subColumnId;
		private DevExpress.XtraGrid.Columns.GridColumn subColumnName;
		private DevExpress.XtraGrid.Columns.GridColumn subColumnIsSet;
		private DevExpress.XtraGrid.Columns.GridColumn subColumnMeasure;
		private DevExpress.XtraGrid.Columns.GridColumn subColumnMinAmount;
    }
}