﻿using System;
using System.Windows.Forms;
using LaRenzo.DataRepository.Repositories;

namespace LaRenzo.Forms.Parameters.Brand
{
    public partial class BrandEdit : Form
    {
		/// <summary> Id редактируемого элемента </summary>
		private int itemId = -1;

		/// <summary> Значение контрола </summary>
		public DataRepository.Entities.Settings.Brand ItemValue
		{
			get
			{
				var brend = new DataRepository.Entities.Settings.Brand
				{
					ID = itemId,
					Name = nameTextBox.Text,
					//SiteAddress = siteAddressTextBox.Text,
					//Check = checkTextBox.Text,
				};

				string errMsg = Content.BrandManager.Validate(brend);

				if (!string.IsNullOrEmpty(errMsg))
				{
					MessageBox.Show(errMsg);
					brend = null;
				}

				return brend;
			}
			set
			{
				nameTextBox.Text = value.Name ?? "";
				//siteAddressTextBox.Text = value.SiteAddress ?? "";
				//checkTextBox.Text = value.Check ?? "";

				itemId = value.ID;
			}
		}


		//___________________________________________________________________________________________________________________________________________________________________________________
		public BrandEdit()
        {
            InitializeComponent();

			RefreshControls();
        }


		//___________________________________________________________________________________________________________________________________________________________________________________
		public BrandEdit(DataRepository.Entities.Settings.Brand editItem)
		{
			InitializeComponent();

			RefreshControls();

			ItemValue = editItem;
		}


		//___________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Установить параметры по умолчанию </summary>
		////======================================================
		private void RefreshControls()
		{
			
		}

		
		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Сохранить" </summary>
		////=======================================
        private void OKButtonClick(object sender, EventArgs e)
        {
			if (ItemValue != null)
			{
				Content.BrandManager.Save(ItemValue);

				DialogResult = DialogResult.OK;
			}
        }


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Удалить" </summary>
		////=======================================
		private void CancelButtonClick(object sender, EventArgs e)
        {
			DialogResult = DialogResult.Cancel;
        }
    }
}
