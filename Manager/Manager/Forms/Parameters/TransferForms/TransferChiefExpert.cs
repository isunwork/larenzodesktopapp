﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Entities.Transfers;
using LaRenzo.DataRepository.Entities.Transfers.ChefExpertTransfer;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Products;
using LaRenzo.DataRepository.Repositories.Transfers;
using LaRenzo.Forms.Catalog.Dishes;
using LaRenzo.Forms.Catalog.ProductForms;

namespace LaRenzo.Forms.Parameters.TransferForms
{
    public partial class TransferChiefExpert : Form
    {
        public TransferChiefExpert()
        {
            InitializeComponent();
            cmbAction.DataSource = Content.ChiefExpertTransferManager.GetActionList();
            cmbAction.DisplayMember = "Name";
            cmbAction.ValueMember = "ID";

            cmbFolderForNew.DataSource = Content.ProductManager.GetCategoriesList();
            cmbFolderForNew.DisplayMember = "Name";
            cmbFolderForNew.ValueMember = "ID";

            cmbDishAction.DataSource = Content.ChiefExpertTransferManager.GetActionList();
            cmbDishAction.DisplayMember = "Name";
            cmbDishAction.ValueMember = "ID";

            cmbNewDishCatalog.DataSource = Content.CategoryRepository.GetItems();
            cmbDishAction.DisplayMember = "Name";
            cmbDishAction.ValueMember = "ID";
        }
        
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            if (AddressDlg.ShowDialog() == DialogResult.OK)
            {
                txtPath.Text = AddressDlg.FileName;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtPath.Text = string.Empty;
        }

        private void TransferWizard_NextClick(object sender, DevExpress.XtraWizard.WizardCommandButtonClickEventArgs e)
        {
            switch (e.Page.Name)
            {
                case "StartLoad":
                {
                    var loadFromFile = new BackgroundWorker();
                    loadFromFile.DoWork += (o, args) =>
                        {
                            string path = string.Empty;
                            Invoke((MethodInvoker)delegate { path = txtPath.Text; });

                            args.Result = Content.ChiefExpertTransferManager.GetIngridients(path);
                        };

                    loadFromFile.RunWorkerCompleted += (o, args) =>
                        {
                            mainIngridientGridControl.DataSource = null;
                            mainIngridientGridControl.DataSource = args.Result;
                            lblIngridientsCountValue.Text = ((IList)args.Result).Count.ToString(CultureInfo.InvariantCulture);
                            TransferWizard.SetNextPage();
                        };

                    loadFromFile.RunWorkerAsync();
                    break;
                }
                case "IngridientsLoad":
                {
                    var saveToDb = new BackgroundWorker();
                    saveToDb.DoWork += (o, args) =>
                        {
                            int actionId = 1;
                            bool fromIngr = false;
                            ProductCategory category = null;
                            List<ProductConformity> dataSource = null;

                            Invoke((MethodInvoker)delegate
                                {
                                    dataSource = (List<ProductConformity>)mainIngridientGridControl.DataSource;
                                    actionId = ((NewItemAction)cmbAction.SelectedItem).ID;
                                    fromIngr = cmbCatalogFromIngr.Checked;
                                    category = UseConstFolder.Checked
                                                   ? ((ProductCategory)cmbFolderForNew.SelectedItem)
                                                   : null;
                                });

                            args.Result = Content.ChiefExpertTransferManager.SaveIngridientList(dataSource, actionId, fromIngr, category);
                        };

                    saveToDb.RunWorkerCompleted += (o, args) =>
                        {
                            if (args.Result != null && (bool)args.Result)
                            {
                                TransferWizard.SetNextPage();
                            }
                            else
                            {
                                TransferWizard.SetPreviousPage();
                            }
                        };

                    saveToDb.RunWorkerAsync();
                    break;
                }
                case "IngridientsLoaded":
                    {
                        var dishLoading = new BackgroundWorker();
                        dishLoading.DoWork += (o, args) =>
                            {
                                try
                                {
                                    string path = string.Empty;
                                    Invoke((MethodInvoker)delegate { path = txtPath.Text; });

                                    args.Result = Content.ChiefExpertTransferManager.GetProducts(path);
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(string.Format("{0}, {1}", ex.Message, ex.StackTrace));
                                }
                            };

                        dishLoading.RunWorkerCompleted += (o, args) =>
                            {
                                mainDishGridControl.DataSource = null;
                                mainDishGridControl.DataSource = args.Result;
                                lblDishCount.Text = ((IList)args.Result).Count.ToString(CultureInfo.InvariantCulture);
                                TransferWizard.SetNextPage();
                            };

                        dishLoading.RunWorkerAsync();
                        break;
                    }
                case "ProductLoad":
                    {
                        var saveToDb = new BackgroundWorker();
                        saveToDb.DoWork += (o, args) =>
                            {
                                int actionId = 1;
                                bool fromDish = false;
                                string path = string.Empty;
                                Category category = null;
                                List<DishConformity> dataSource = null;

                                Invoke((MethodInvoker)delegate
                                    {
                                        path = txtPath.Text;
                                        dataSource = (List<DishConformity>)mainDishGridControl.DataSource;
                                        actionId = ((NewItemAction)cmbDishAction.SelectedItem).ID;
                                        fromDish = chkStaticDishCatalog.Checked;
                                        category = chkStaticDishCatalog.Checked
                                                       ? ((Category)cmbNewDishCatalog.SelectedItem)
                                                       : null;
                                    });

                                args.Result = Content.ChiefExpertTransferManager.SaveProductList(dataSource, actionId, fromDish, category, path);
                            };

                        saveToDb.RunWorkerCompleted += (o, args) =>
                            {
                                if (args.Result != null && (bool) args.Result)
                                {
                                    TransferWizard.SetNextPage();
                                }
                                else
                                {
                                    TransferWizard.SetPreviousPage();
                                }
                            };

                        saveToDb.RunWorkerAsync();
                        break;
                    }
            }
        }

        private void TransferChiefExpert_Resize(object sender, EventArgs e)
        {
            progressLoadFromFile.ImageHorzOffset =
                progressSaveIngridients.ImageHorzOffset =
                progressDishLoading.ImageHorzOffset = progressDishLoad.ImageHorzOffset = Width/2 - 150;
        }

        private void UseConstFolder_CheckedChanged(object sender, EventArgs e)
        {
            cmbFolderForNew.Enabled = UseConstFolder.Checked;
            btnSelectFolder.Enabled = UseConstFolder.Checked;

            btnSelectDishCategory.Enabled = chkStaticDishCatalog.Checked;
            cmbNewDishCatalog.Enabled = chkStaticDishCatalog.Checked;
        }

        private void btnSelectFolder_Click(object sender, EventArgs e)
        {
            var selectCategoryForm = new SelectProductCategoryForm();

            if (selectCategoryForm.ShowDialog() == DialogResult.OK)
            {
                cmbFolderForNew.SelectedValue = selectCategoryForm.ProductCategory.ID;
            }
        }

        private void TransferWizard_CancelClick(object sender, CancelEventArgs e)
        {
            Close();
        }

        private void EditRowButton_Click(object sender, EventArgs e)
        {
            var selectedProductForm = new SelectProductForm();
            if (selectedProductForm.ShowDialog() == DialogResult.OK)
            {
                mainIngridientGridView.BeginDataUpdate();
                var selectedRow = (ProductConformity) mainIngridientGridView.GetFocusedRow();
                if (selectedRow != null)
                {
                    selectedRow.Product = selectedProductForm.ProductsList[0];
                }
                mainIngridientGridView.EndDataUpdate();
            }
        }

        private void EditFolderButton_Click(object sender, EventArgs e)
        {
            var selectProductCategoryForm = new SelectProductCategoryForm();
            if (selectProductCategoryForm.ShowDialog() == DialogResult.OK)
            {
                mainIngridientGridView.BeginDataUpdate();
                var selectedRow = (ProductConformity) mainIngridientGridView.GetFocusedRow();
                if (selectedRow != null)
                {
                    selectedRow.ChefProduct.ProductCategory = selectProductCategoryForm.ProductCategory;
                }
                mainIngridientGridView.EndDataUpdate();
            }
        }

        private void DishEdit_Click(object sender, EventArgs e)
        {
            var selectDishForm = new SelectDishForm();
            if (selectDishForm.ShowDialog() == DialogResult.OK)
            {
                mainDishGridView.BeginDataUpdate();

                var selectedRow = (DishConformity) mainDishGridView.GetFocusedRow();
                
                if (selectedRow != null)
                {
                    selectedRow.Dish = selectDishForm.Dish;
                }
                
                mainDishGridView.EndDataUpdate();
            }
        }

        private void DishCategoryEdit_Click(object sender, EventArgs e)
        {
            var selectDishCategoryForm = new SelectDishCategoryForm();
            if (selectDishCategoryForm.ShowDialog() == DialogResult.OK)
            {
                mainDishGridView.BeginDataUpdate();
                var selectedRow = (DishConformity) mainDishGridView.GetFocusedRow();
                if (selectedRow != null)
                {
                    selectedRow.DishChef.DishCategory = selectDishCategoryForm.DishCategory;
                }
                mainDishGridView.EndDataUpdate();
            }
        }

        private void btnSelectDishCategory_Click(object sender, EventArgs e)
        {

        }
    }
}
