﻿namespace LaRenzo.Forms.Parameters.PrinterForms
{
    partial class AddPrinterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddPrinter = new System.Windows.Forms.Button();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lstInstaledPrinters = new System.Windows.Forms.ListBox();
            this.topPanel = new System.Windows.Forms.Panel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.comboBoxPointOfSale = new System.Windows.Forms.ComboBox();
            this.bottomPanel.SuspendLayout();
            this.topPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAddPrinter
            // 
            this.btnAddPrinter.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnAddPrinter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAddPrinter.Location = new System.Drawing.Point(85, 0);
            this.btnAddPrinter.Name = "btnAddPrinter";
            this.btnAddPrinter.Size = new System.Drawing.Size(87, 22);
            this.btnAddPrinter.TabIndex = 4;
            this.btnAddPrinter.Text = "Добавить";
            this.btnAddPrinter.UseVisualStyleBackColor = true;
            this.btnAddPrinter.Click += new System.EventHandler(this.btnAddPrinter_Click);
            // 
            // bottomPanel
            // 
            this.bottomPanel.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.bottomPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bottomPanel.Controls.Add(this.btnAddPrinter);
            this.bottomPanel.Controls.Add(this.btnCancel);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 119);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(261, 24);
            this.bottomPanel.TabIndex = 5;
            // 
            // btnCancel
            // 
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.Location = new System.Drawing.Point(172, 0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 22);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Отменить";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.button1_Click);
            // 
            // lstInstaledPrinters
            // 
            this.lstInstaledPrinters.Dock = System.Windows.Forms.DockStyle.Top;
            this.lstInstaledPrinters.FormattingEnabled = true;
            this.lstInstaledPrinters.Location = new System.Drawing.Point(0, 27);
            this.lstInstaledPrinters.Name = "lstInstaledPrinters";
            this.lstInstaledPrinters.Size = new System.Drawing.Size(261, 82);
            this.lstInstaledPrinters.TabIndex = 6;
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.lblTitle);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(261, 27);
            this.topPanel.TabIndex = 7;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(9, 7);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(143, 13);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Установленные принтеры:";
            // 
            // comboBoxPointOfSale
            // 
            this.comboBoxPointOfSale.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.comboBoxPointOfSale.FormattingEnabled = true;
            this.comboBoxPointOfSale.Location = new System.Drawing.Point(0, 98);
            this.comboBoxPointOfSale.Name = "comboBoxPointOfSale";
            this.comboBoxPointOfSale.Size = new System.Drawing.Size(261, 21);
            this.comboBoxPointOfSale.TabIndex = 8;
            // 
            // AddPrinterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(261, 143);
            this.Controls.Add(this.comboBoxPointOfSale);
            this.Controls.Add(this.lstInstaledPrinters);
            this.Controls.Add(this.bottomPanel);
            this.Controls.Add(this.topPanel);
            this.Name = "AddPrinterForm";
            this.Text = "Добавить принтер";
            this.Load += new System.EventHandler(this.AddPrinterForm_Load);
            this.bottomPanel.ResumeLayout(false);
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAddPrinter;
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ListBox lstInstaledPrinters;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.ComboBox comboBoxPointOfSale;
    }
}