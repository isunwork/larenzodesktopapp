﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Entities.PrintWorksModels;
using LaRenzo.DataRepository.Entities.States;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Printers;
using LaRenzo.DataRepository.Repositories.Documents.Orders;
using LaRenzo.DataRepository.Repositories.Documents.Sessions;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Parameters.PrinterForms
{
    public partial class PrintingMonitor : Form
    {
        public PrintingMonitor()
        {
            InitializeComponent();
            mainGridControl.DataSource = Content.PrintWorksRepository.GetNMonitorRecords((int)nudGenerateAmount.Value, DateTime.Now);
            RepeatWorkMenuItem.Click += async(s,e)=>await RepeatWorkMenuItem_Click();
            
            RepeatDetailMenuItem.Click += async(s,e)=>await RepeatDetailMenuItem_Click();
        }

        private void mainGridView_MasterRowGetChildList(object sender, MasterRowGetChildListEventArgs e)
        {
            if (mainGridView != null)
            {
                var printMonitorRecord = mainGridView.GetRow(e.RowHandle) as PrintMonitorRecord;
                if (printMonitorRecord != null)
                {
                    e.ChildList = Content.PrintWorksRepository.GetDetailMonitorRecords(printMonitorRecord.ID);
                }
            }

        }

        private void mainGridView_MasterRowGetRelationCount(object sender, MasterRowGetRelationCountEventArgs e)
        {
            e.RelationCount = 1;
        }

        private void mainGridView_MasterRowGetRelationDisplayCaption(object sender, MasterRowGetRelationNameEventArgs e)
        {
            e.RelationName = "Задания для принтеров";
        }

        private void mainGridView_MasterRowGetRelationName(object sender, MasterRowGetRelationNameEventArgs e)
        {
            e.RelationName = "Details";
        }

        private void mainGridView_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            var view = sender as GridView;

            if (view != null)
            {
                GridHitInfo hitInfo = view.CalcHitInfo(e.Point);
                if (view.IsDataRow(hitInfo.RowHandle))
                    WorksContextMenu.Show(view.GridControl, e.Point);
            }
        }

        private void detailGridView_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            var view = sender as GridView;

            if (view != null)
            {
                GridHitInfo hitInfo = view.CalcHitInfo(e.Point);
                if (view.IsDataRow(hitInfo.RowHandle))
                    DetailContextMenu.Show(view.GridControl, e.Point);
            }
        }

        private async Task RepeatDetailMenuItem_Click()
        {
            try
            {
                var view = (mainGridControl.FocusedView as GridView);

                if (view != null && view.IsDataRow(view.FocusedRowHandle))
                {
                    var detail = view.GetRow(view.FocusedRowHandle) as PrintMonitorDetailRecord;
                    if (detail != null)
                    {
                        var detailPrintWork = Content.PrintWorksRepository.GetDetailPrintWork(detail.ID);

                        if (detailPrintWork != null)
                        {
                            Order order = Content.OrderManager.GetById(Convert.ToInt32(detailPrintWork.PrintWork.OrderId));
                            if (order != null)
                            {
                                if (detailPrintWork.IsBag)
                                {
                                    PrintingManager.PrintBagCheck(order);
                                    return;
                                }

                                if (detailPrintWork.IsClient)
                                {
                                    await PrintingManager.PrintClientCheck(order);
                                    return;
                                }

                                if (detailPrintWork.IsDelivery)
                                {
                                    PrintingManager.PrintDeliveryCheck(order);
                                    return;
                                }

                                if (detailPrintWork.IsGeneral)
                                {
                                    PrintingManager.PrintSectionCheckFromOrder(order, null);
                                    return;
                                }

                                PrintingManager.PrintSectionCheckFromOrder(order, detailPrintWork.Section);
                            }
                        }
                    }


                }
                else
                {
                    MessageBox.Show(Resources.SelectDetailToPrint);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async Task RepeatWorkMenuItem_Click()
        {
            try
            {
                var view = (mainGridControl.FocusedView as GridView);

                if (view != null && view.IsDataRow(view.FocusedRowHandle))
                {
                    var id = (int)view.GetRowCellValue(view.FocusedRowHandle, columnPrintWorkId);

                    PrintWork printWork = Content.PrintWorksRepository.GetPrintWork(id);

                    if (printWork != null)
                    {
                        if (printWork.IsSession)
                        {
                            var session = Content.SessionManager.GetById(Convert.ToInt32(printWork.SessionId));
                            if (session != null)
                            {
                                string message = String.Empty;
                                var pointOfSales = Content.PointOfSaleRepository.GetItems();
                                foreach (var point in pointOfSales)
                                {
                                    message = PrintingManager.EndSessionCheckPrint(session, point.ID);
                                }

                                if (!string.IsNullOrEmpty(message))
                                {
                                    MessageBox.Show(message);
                                    Content.PrintWorksRepository.UpdatePrintWorkState(printWork, State.Error);
                                }
                                else
                                {
                                    Content.PrintWorksRepository.UpdatePrintWorkState(printWork, State.Compleated);
                                }
                            }
                        }
                        else
                        {
                            Order order = Content.OrderManager.GetById(Convert.ToInt32(printWork.OrderId));
                            if (order != null)
                            {
                                string message = await PrintingManager.Print(order, Settings.Default.PrinterPort, Settings.Default.PrinterIp, -1);
                                if (!string.IsNullOrEmpty(message))
                                {
                                    MessageBox.Show(message);
                                    Content.PrintWorksRepository.UpdatePrintWorkState(printWork, State.Error);
                                }
                                else
                                {
                                    Content.PrintWorksRepository.UpdatePrintWorkState(printWork, State.Compleated);
                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show(Resources.PrintWorksIsNotFound);
                    }
                }
                else
                {
                    MessageBox.Show(
                        Resources.SelectPrintWorksForPrint);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            SetRowStyle(sender, e, columnPrintWorkState);
        }

        private void detailGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            SetRowStyle(sender, e, DetailPrintName);
        }

        private void SetRowStyle(object sender, RowStyleEventArgs e, GridColumn column)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            string state = view.GetRowCellDisplayText(e.RowHandle, column);

            if (state == "Ошибка")
            {
                e.Appearance.BackColor = Color.Salmon;
            }
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            mainGridControl.DataSource = Content.PrintWorksRepository.GetNMonitorRecords((int)nudGenerateAmount.Value,
                                                                              dTimeDateOfData.Value);
        }
    }
}