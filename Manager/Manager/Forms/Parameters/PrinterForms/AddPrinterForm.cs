﻿using System;
using System.Drawing.Printing;
using System.Linq;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Printers;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Printers;

namespace LaRenzo.Forms.Parameters.PrinterForms
{
    public partial class AddPrinterForm : Form
    { 
        public AddPrinterForm()
        {
            InitializeComponent();

        }

        private void btnAddPrinter_Click(object sender, EventArgs e)
        {
            if (lstInstaledPrinters.SelectedItem != null)
            {
                var printer = new Printer
                {
                    Name = lstInstaledPrinters.SelectedItem.ToString(),
                    PointOfSaleId = (int)comboBoxPointOfSale.SelectedValue
                };
                Content.PrinterRepository.AddPrinter(printer);
                DialogResult = DialogResult.OK;
            }
            else
            {
                var ep = new ErrorProvider();
                ep.SetError(lblTitle, "Не выбран принтер");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
           DialogResult = DialogResult.Cancel;
        }

        private void AddPrinterForm_Load(object sender, EventArgs e)
        {
            foreach (var printer in PrinterSettings.InstalledPrinters)
            {
                lstInstaledPrinters.Items.Add(printer);
            }
            var points = Content.PointOfSaleRepository.GetItems();
            comboBoxPointOfSale.DataSource = points;
            comboBoxPointOfSale.DisplayMember = "PointName";
            comboBoxPointOfSale.ValueMember = "Id";
            comboBoxPointOfSale.SelectedValue = points.FirstOrDefault()?.ID;
        }
    }
}
