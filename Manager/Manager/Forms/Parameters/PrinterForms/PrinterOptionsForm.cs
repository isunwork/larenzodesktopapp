﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Printers;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Printers;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Parameters.PrinterForms
{

	
	
	public partial class PrinterForm : Form
    {
        public PrinterForm()
        {
            InitializeComponent();
        }

        private void PrinterForm_Load(object sender, EventArgs e)
        {
            BindData();
        }

	    private void btnAddPrinter_Click(object sender, EventArgs e)
	    {
		    var form = new AddPrinterForm();
		    if (form.ShowDialog() == DialogResult.OK)
		    {
			    lbPrinters.DataSource = Content.PrinterRepository.GetPrinterList();
		    }
	    }


	    private void BindData()
		{
            var checkTypes = Content.PrinterRepository.GetOrderCheckTypes().Select(x => x.Value).ToList();

            cbCheckType.DataSource = checkTypes;
		    lbPrinters.DataSource = Content.PrinterRepository.GetPrinterList();

			List<PrinterTask> pt = Content.PrinterRepository.GetPrinterTaskList();

			gcPrinterTasks.DataSource = GetPrinterTaskList(pt);
		}


		//___________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Вернуть список задач принтера </summary>
		////==================================================
		private List<PrinterInfo> GetPrinterTaskList(List<PrinterTask> pt)
		{
			var printerListInfo = new List<PrinterInfo>();

			List<KeyValuePair<string,string>> checkTypes = Content.PrinterRepository.GetOrderCheckTypes().ToList();

			foreach (PrinterTask printerTask in pt)
			{
				printerListInfo.Add(new PrinterInfo
				{
					ID = printerTask.ID,
					Name =printerTask.Printer.Name,
					Title = checkTypes.Where(x=>x.Key == printerTask.InstructionAlias).Select(x=>x.Value).FirstOrDefault()
				});
			}

			return printerListInfo;
		}



        private void btnRemovePrinter_Click(object sender, EventArgs e)
        {
            var printer = GetSelectedPrinter();
            if (printer == null)
            {
                MessageBox.Show(Resources.SelectPrinter);
            }
            else
            {
                Content.PrinterRepository.RemovePrinter(printer.ID);
				BindData();
            }
        }


        private void btnCheckPrinter_Click(object sender, EventArgs e)
        {
            var printer = GetSelectedPrinter();
            if (printer == null)
            {
                MessageBox.Show(Resources.SelectPrinter);
            }

            //PrintManager.EndSessionCheckPrint(printer.Name, new CheckBuilder().TestCheck(printer.IpAddress), printer.IpAddress);
        }


        private void btnAddPrinterTask_Click(object sender, EventArgs e)
        {
            var printer = GetSelectedPrinter();
            if (printer == null)
            {
                MessageBox.Show(Resources.SelectPrinter);
                return;
            }

            var task = new PrinterTask
                           {
                               PrinterID = printer.ID,
                               InstructionAlias = Content.PrinterRepository.GetOrderCheckTypes().First(x => x.Value == cbCheckType.Text).Key,
                           };

            Content.PrinterRepository.AddPrinterTask(task);
            
            BindData();
        }

        private Printer GetSelectedPrinter()
        {
	        if (lbPrinters.SelectedIndex == -1)
	        {
		        return null;
	        }
	        else
	        {
				return (Printer)lbPrinters.SelectedItem;    
	        }
        }

        private void gridView1_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "CheckTypeCol" && e.IsGetData)
            {
                // ReSharper disable CSharpWarnings::CS0612
                var value = (string)gridView1.GetRowCellValue(e.ListSourceRowIndex, "InstructionAlias");
                // ReSharper restore CSharpWarnings::CS0612

                e.Value = (string.IsNullOrWhiteSpace(value)) ? "Ошибка" : Content.PrinterRepository.GetOrderCheckTypes().FirstOrDefault(x => x.Key == value).Value;
            }
        }

        private void gcPrinterTasks_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void gcPrinterTasks_DoubleClick(object sender, EventArgs e)
        {
            var view = gridView1;
            var pt = view.GridControl.PointToClient(MousePosition);
            var hitInfo = view.CalcHitInfo(pt);

            if (hitInfo.InRowCell) 
            {
                var id = (int)gridView1.GetRowCellValue(hitInfo.RowHandle, "ID");
                Content.PrinterRepository.RemovePrinterTask(id);
                BindData();
            }
        }


        private void gcPrinterTasks_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                var id = (int)gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "ID");
                Content.PrinterRepository.RemovePrinterTask(id);
                BindData();
            }
        }
    }

	public class PrinterInfo
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public string Title { get; set; }
	}
}
