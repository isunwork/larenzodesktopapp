﻿namespace LaRenzo.Forms.Parameters.OptionForms
{
	partial class PrinterFontSettingsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			
			this.okButton = new System.Windows.Forms.Button();
			this.cancelButton = new System.Windows.Forms.Button();
			this.defaultFontsButton = new System.Windows.Forms.Button();
			this.BrandColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.UseName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.FontName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Size = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.IsBold = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Point = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
			fontsDataGridView = new System.Windows.Forms.DataGridView();
			((System.ComponentModel.ISupportInitialize)(fontsDataGridView)).BeginInit();
			this.SuspendLayout();
			// 
			// fontsDataGridView
			// 
			fontsDataGridView.AllowUserToAddRows = false;
			fontsDataGridView.AllowUserToDeleteRows = false;
			fontsDataGridView.AllowUserToResizeColumns = false;
			fontsDataGridView.AllowUserToResizeRows = false;
			fontsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			fontsDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
			fontsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			fontsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BrandColumn,
            this.UseName,
            this.FontName,
            this.Size,
            this.IsBold,
            this.Point,
            this.Description});
			fontsDataGridView.Location = new System.Drawing.Point(2, 10);
			fontsDataGridView.Margin = new System.Windows.Forms.Padding(2);
			fontsDataGridView.MultiSelect = false;
			fontsDataGridView.Name = "fontsDataGridView";
			fontsDataGridView.RowTemplate.Height = 24;
			fontsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			fontsDataGridView.Size = new System.Drawing.Size(962, 240);
			fontsDataGridView.TabIndex = 63;
			fontsDataGridView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.FontsDataGridViewMouseDoubleClick);
			// 
			// okButton
			// 
			this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.okButton.Location = new System.Drawing.Point(896, 255);
			this.okButton.Margin = new System.Windows.Forms.Padding(2);
			this.okButton.Name = "okButton";
			this.okButton.Size = new System.Drawing.Size(68, 23);
			this.okButton.TabIndex = 57;
			this.okButton.Text = "OK";
			this.okButton.UseVisualStyleBackColor = true;
			this.okButton.Click += new System.EventHandler(this.OKButtonClick);
			// 
			// cancelButton
			// 
			this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cancelButton.Location = new System.Drawing.Point(824, 255);
			this.cancelButton.Margin = new System.Windows.Forms.Padding(2);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(68, 23);
			this.cancelButton.TabIndex = 64;
			this.cancelButton.Text = "Отмена";
			this.cancelButton.UseVisualStyleBackColor = true;
			this.cancelButton.Click += new System.EventHandler(this.CancelButtonClick);
			// 
			// defaultFontsButton
			// 
			this.defaultFontsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.defaultFontsButton.Location = new System.Drawing.Point(2, 255);
			this.defaultFontsButton.Margin = new System.Windows.Forms.Padding(2);
			this.defaultFontsButton.Name = "defaultFontsButton";
			this.defaultFontsButton.Size = new System.Drawing.Size(110, 23);
			this.defaultFontsButton.TabIndex = 65;
			this.defaultFontsButton.Text = "По умолчанию";
			this.defaultFontsButton.UseVisualStyleBackColor = true;
			this.defaultFontsButton.Click += new System.EventHandler(this.DefaultFontsButtonClick);
			// 
			// BrandColumn
			// 
			this.BrandColumn.DataPropertyName = "BrandName";
			this.BrandColumn.HeaderText = "Бренд";
			this.BrandColumn.Name = "BrandColumn";
			this.BrandColumn.Width = 63;
			// 
			// UseName
			// 
			this.UseName.DataPropertyName = "TitleName";
			this.UseName.FillWeight = 120F;
			this.UseName.HeaderText = "Применяемость";
			this.UseName.Name = "UseName";
			this.UseName.ReadOnly = true;
			this.UseName.Width = 115;
			// 
			// FontName
			// 
			this.FontName.DataPropertyName = "FontName";
			this.FontName.FillWeight = 170F;
			this.FontName.HeaderText = "Название";
			this.FontName.Name = "FontName";
			this.FontName.ReadOnly = true;
			this.FontName.Width = 82;
			// 
			// Size
			// 
			this.Size.DataPropertyName = "FontSize";
			this.Size.HeaderText = "Размер";
			this.Size.Name = "Size";
			this.Size.ReadOnly = true;
			this.Size.Width = 71;
			// 
			// IsBold
			// 
			this.IsBold.DataPropertyName = "Style";
			this.IsBold.HeaderText = "Стиль";
			this.IsBold.Name = "IsBold";
			this.IsBold.ReadOnly = true;
			this.IsBold.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.IsBold.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.IsBold.Width = 43;
			// 
			// Point
			// 
			this.Point.DataPropertyName = "Unit";
			this.Point.HeaderText = "Единица";
			this.Point.Name = "Point";
			this.Point.ReadOnly = true;
			this.Point.Width = 75;
			// 
			// Description
			// 
			this.Description.DataPropertyName = "Description";
			this.Description.FillWeight = 500F;
			this.Description.HeaderText = "Описание";
			this.Description.Name = "Description";
			this.Description.Width = 82;
			// 
			// PrinterFontSettingsForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(968, 282);
			this.Controls.Add(this.defaultFontsButton);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(fontsDataGridView);
			this.Controls.Add(this.okButton);
			this.Margin = new System.Windows.Forms.Padding(2);
			this.Name = "PrinterFontSettingsForm";
			this.Text = "Настройки шрифтов печати";
			((System.ComponentModel.ISupportInitialize)(fontsDataGridView)).EndInit();
			this.ResumeLayout(false);

		}

		

		#endregion

		private System.Windows.Forms.DataGridView fontsDataGridView;
		private System.Windows.Forms.Button okButton;
		private System.Windows.Forms.DataGridViewTextBoxColumn FontSize;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Button defaultFontsButton;
		private System.Windows.Forms.DataGridViewTextBoxColumn BrandColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn UseName;
		private System.Windows.Forms.DataGridViewTextBoxColumn FontName;
		private System.Windows.Forms.DataGridViewTextBoxColumn Size;
		private System.Windows.Forms.DataGridViewTextBoxColumn IsBold;
		private System.Windows.Forms.DataGridViewTextBoxColumn Point;
		private System.Windows.Forms.DataGridViewTextBoxColumn Description;
	}
}