﻿namespace LaRenzo.Forms.Parameters.OptionForms
{
	partial class SettingsControl
	{
		/// <summary> 
		/// Требуется переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором компонентов

		/// <summary> 
		/// Обязательный метод для поддержки конструктора - не изменяйте 
		/// содержимое данного метода при помощи редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			this.label4 = new System.Windows.Forms.Label();
			this.operNumberTextBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.firstStringChechTextBox = new System.Windows.Forms.TextBox();
			this.changePrinterFontButton = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.devMailSettingsButton = new System.Windows.Forms.Button();
			this.lblCheckFooter = new System.Windows.Forms.Label();
			this.txtCheckFooter = new System.Windows.Forms.TextBox();
			this.lblSites = new System.Windows.Forms.Label();
			this.txtSites = new System.Windows.Forms.TextBox();
			this.lblMaxGap = new System.Windows.Forms.Label();
			this.txtMaxGap = new System.Windows.Forms.TextBox();
			this.lblDeliveryPrice = new System.Windows.Forms.Label();
			this.txtDeliveryPrice = new System.Windows.Forms.TextBox();
			this.lblReservTime = new System.Windows.Forms.Label();
			this.txtReserveTime = new System.Windows.Forms.TextBox();
			this.lblOverheadTime = new System.Windows.Forms.Label();
			this.txtOverheadTime = new System.Windows.Forms.TextBox();
			this.lblCookingTime = new System.Windows.Forms.Label();
			this.txtCookingTime = new System.Windows.Forms.TextBox();
			this.lblFreeDelivery = new System.Windows.Forms.Label();
			this.txtFreeDelivery = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(3, -2);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(97, 13);
			this.label4.TabIndex = 54;
			this.label4.Text = "Номер оператора";
			// 
			// operNumberTextBox
			// 
			this.operNumberTextBox.Location = new System.Drawing.Point(1, 14);
			this.operNumberTextBox.Name = "operNumberTextBox";
			this.operNumberTextBox.Size = new System.Drawing.Size(193, 20);
			this.operNumberTextBox.TabIndex = 53;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(0, 388);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(109, 13);
			this.label3.TabIndex = 52;
			this.label3.Text = "Первая строка чека";
			// 
			// firstStringChechTextBox
			// 
			this.firstStringChechTextBox.Location = new System.Drawing.Point(0, 404);
			this.firstStringChechTextBox.Name = "firstStringChechTextBox";
			this.firstStringChechTextBox.Size = new System.Drawing.Size(340, 20);
			this.firstStringChechTextBox.TabIndex = 51;
			// 
			// changePrinterFontButton
			// 
			this.changePrinterFontButton.Location = new System.Drawing.Point(13, 506);
			this.changePrinterFontButton.Margin = new System.Windows.Forms.Padding(2);
			this.changePrinterFontButton.Name = "changePrinterFontButton";
			this.changePrinterFontButton.Size = new System.Drawing.Size(101, 25);
			this.changePrinterFontButton.TabIndex = 50;
			this.changePrinterFontButton.Text = "Изменить";
			this.changePrinterFontButton.UseVisualStyleBackColor = true;
			this.changePrinterFontButton.Click += new System.EventHandler(this.changePrinterFontButton_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label2.Location = new System.Drawing.Point(0, 490);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(183, 13);
			this.label2.TabIndex = 49;
			this.label2.Text = "Параметры шрифта принтера";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(0, 437);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(207, 13);
			this.label1.TabIndex = 48;
			this.label1.Text = "Настройки почты разработчиков:";
			// 
			// devMailSettingsButton
			// 
			this.devMailSettingsButton.Location = new System.Drawing.Point(13, 453);
			this.devMailSettingsButton.Margin = new System.Windows.Forms.Padding(2);
			this.devMailSettingsButton.Name = "devMailSettingsButton";
			this.devMailSettingsButton.Size = new System.Drawing.Size(101, 23);
			this.devMailSettingsButton.TabIndex = 47;
			this.devMailSettingsButton.Text = "Перейти";
			this.devMailSettingsButton.UseVisualStyleBackColor = true;
			this.devMailSettingsButton.Click += new System.EventHandler(this.devMailSettingsButton_Click);
			// 
			// lblCheckFooter
			// 
			this.lblCheckFooter.AutoSize = true;
			this.lblCheckFooter.Location = new System.Drawing.Point(0, 310);
			this.lblCheckFooter.Name = "lblCheckFooter";
			this.lblCheckFooter.Size = new System.Drawing.Size(162, 13);
			this.lblCheckFooter.TabIndex = 46;
			this.lblCheckFooter.Text = "Текст снизу клиентского чека";
			// 
			// txtCheckFooter
			// 
			this.txtCheckFooter.Location = new System.Drawing.Point(0, 326);
			this.txtCheckFooter.Multiline = true;
			this.txtCheckFooter.Name = "txtCheckFooter";
			this.txtCheckFooter.Size = new System.Drawing.Size(340, 55);
			this.txtCheckFooter.TabIndex = 45;
			// 
			// lblSites
			// 
			this.lblSites.AutoSize = true;
			this.lblSites.Location = new System.Drawing.Point(0, 271);
			this.lblSites.Name = "lblSites";
			this.lblSites.Size = new System.Drawing.Size(340, 13);
			this.lblSites.TabIndex = 44;
			this.lblSites.Text = "Сайты, с которых нужно собирать заказы (без http, разделять `;`)";
			// 
			// txtSites
			// 
			this.txtSites.Location = new System.Drawing.Point(0, 287);
			this.txtSites.Name = "txtSites";
			this.txtSites.Size = new System.Drawing.Size(340, 20);
			this.txtSites.TabIndex = 43;
			// 
			// lblMaxGap
			// 
			this.lblMaxGap.AutoSize = true;
			this.lblMaxGap.Location = new System.Drawing.Point(0, 232);
			this.lblMaxGap.Name = "lblMaxGap";
			this.lblMaxGap.Size = new System.Drawing.Size(195, 13);
			this.lblMaxGap.TabIndex = 42;
			this.lblMaxGap.Text = "Максимальное время между цехами";
			// 
			// txtMaxGap
			// 
			this.txtMaxGap.Location = new System.Drawing.Point(0, 248);
			this.txtMaxGap.Name = "txtMaxGap";
			this.txtMaxGap.Size = new System.Drawing.Size(193, 20);
			this.txtMaxGap.TabIndex = 41;
			// 
			// lblDeliveryPrice
			// 
			this.lblDeliveryPrice.AutoSize = true;
			this.lblDeliveryPrice.Location = new System.Drawing.Point(0, 76);
			this.lblDeliveryPrice.Name = "lblDeliveryPrice";
			this.lblDeliveryPrice.Size = new System.Drawing.Size(106, 13);
			this.lblDeliveryPrice.TabIndex = 40;
			this.lblDeliveryPrice.Text = "Цена доставки, руб";
			// 
			// txtDeliveryPrice
			// 
			this.txtDeliveryPrice.Location = new System.Drawing.Point(0, 92);
			this.txtDeliveryPrice.Name = "txtDeliveryPrice";
			this.txtDeliveryPrice.Size = new System.Drawing.Size(193, 20);
			this.txtDeliveryPrice.TabIndex = 39;
			// 
			// lblReservTime
			// 
			this.lblReservTime.AutoSize = true;
			this.lblReservTime.Location = new System.Drawing.Point(0, 115);
			this.lblReservTime.Name = "lblReservTime";
			this.lblReservTime.Size = new System.Drawing.Size(111, 13);
			this.lblReservTime.TabIndex = 38;
			this.lblReservTime.Text = "Время резерва, мин";
			// 
			// txtReserveTime
			// 
			this.txtReserveTime.Location = new System.Drawing.Point(0, 131);
			this.txtReserveTime.Name = "txtReserveTime";
			this.txtReserveTime.Size = new System.Drawing.Size(193, 20);
			this.txtReserveTime.TabIndex = 37;
			// 
			// lblOverheadTime
			// 
			this.lblOverheadTime.AutoSize = true;
			this.lblOverheadTime.Location = new System.Drawing.Point(0, 154);
			this.lblOverheadTime.Name = "lblOverheadTime";
			this.lblOverheadTime.Size = new System.Drawing.Size(186, 13);
			this.lblOverheadTime.TabIndex = 36;
			this.lblOverheadTime.Text = "Время на накладные расходы, мин";
			// 
			// txtOverheadTime
			// 
			this.txtOverheadTime.Location = new System.Drawing.Point(0, 170);
			this.txtOverheadTime.Name = "txtOverheadTime";
			this.txtOverheadTime.Size = new System.Drawing.Size(193, 20);
			this.txtOverheadTime.TabIndex = 35;
			// 
			// lblCookingTime
			// 
			this.lblCookingTime.AutoSize = true;
			this.lblCookingTime.Location = new System.Drawing.Point(0, 193);
			this.lblCookingTime.Name = "lblCookingTime";
			this.lblCookingTime.Size = new System.Drawing.Size(191, 13);
			this.lblCookingTime.TabIndex = 34;
			this.lblCookingTime.Text = "Время приготов. одного блюда, мин";
			// 
			// txtCookingTime
			// 
			this.txtCookingTime.Location = new System.Drawing.Point(0, 209);
			this.txtCookingTime.Name = "txtCookingTime";
			this.txtCookingTime.Size = new System.Drawing.Size(193, 20);
			this.txtCookingTime.TabIndex = 33;
			// 
			// lblFreeDelivery
			// 
			this.lblFreeDelivery.AutoSize = true;
			this.lblFreeDelivery.Location = new System.Drawing.Point(0, 37);
			this.lblFreeDelivery.Name = "lblFreeDelivery";
			this.lblFreeDelivery.Size = new System.Drawing.Size(154, 13);
			this.lblFreeDelivery.TabIndex = 32;
			this.lblFreeDelivery.Text = "Бесплатная доставка от, руб";
			// 
			// txtFreeDelivery
			// 
			this.txtFreeDelivery.Location = new System.Drawing.Point(0, 53);
			this.txtFreeDelivery.Name = "txtFreeDelivery";
			this.txtFreeDelivery.Size = new System.Drawing.Size(193, 20);
			this.txtFreeDelivery.TabIndex = 31;
			// 
			// SettingsControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.label4);
			this.Controls.Add(this.operNumberTextBox);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.firstStringChechTextBox);
			this.Controls.Add(this.changePrinterFontButton);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.devMailSettingsButton);
			this.Controls.Add(this.lblCheckFooter);
			this.Controls.Add(this.txtCheckFooter);
			this.Controls.Add(this.lblSites);
			this.Controls.Add(this.txtSites);
			this.Controls.Add(this.lblMaxGap);
			this.Controls.Add(this.txtMaxGap);
			this.Controls.Add(this.lblDeliveryPrice);
			this.Controls.Add(this.txtDeliveryPrice);
			this.Controls.Add(this.lblReservTime);
			this.Controls.Add(this.txtReserveTime);
			this.Controls.Add(this.lblOverheadTime);
			this.Controls.Add(this.txtOverheadTime);
			this.Controls.Add(this.lblCookingTime);
			this.Controls.Add(this.txtCookingTime);
			this.Controls.Add(this.lblFreeDelivery);
			this.Controls.Add(this.txtFreeDelivery);
			this.Name = "SettingsControl";
			this.Size = new System.Drawing.Size(343, 538);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox operNumberTextBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox firstStringChechTextBox;
		private System.Windows.Forms.Button changePrinterFontButton;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button devMailSettingsButton;
		private System.Windows.Forms.Label lblCheckFooter;
		private System.Windows.Forms.TextBox txtCheckFooter;
		private System.Windows.Forms.Label lblSites;
		private System.Windows.Forms.TextBox txtSites;
		private System.Windows.Forms.Label lblMaxGap;
		private System.Windows.Forms.TextBox txtMaxGap;
		private System.Windows.Forms.Label lblDeliveryPrice;
		private System.Windows.Forms.TextBox txtDeliveryPrice;
		private System.Windows.Forms.Label lblReservTime;
		private System.Windows.Forms.TextBox txtReserveTime;
		private System.Windows.Forms.Label lblOverheadTime;
		private System.Windows.Forms.TextBox txtOverheadTime;
		private System.Windows.Forms.Label lblCookingTime;
		private System.Windows.Forms.TextBox txtCookingTime;
		private System.Windows.Forms.Label lblFreeDelivery;
		private System.Windows.Forms.TextBox txtFreeDelivery;
	}
}
