﻿using System;
using System.Windows.Forms;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Others.GlobalOptions;

namespace LaRenzo.Forms.Parameters.OptionForms
{
    public partial class SettingsControl : UserControl
	{
		private int brandIdLoc = -1;

		public SettingsControl()
		{
			InitializeComponent();
		}

		//_____________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Установить текущие настройки в контролы формы  </summary>
		////===================================================================
		public void SetCurrentSettingToFormControls(int brandId)
		{
			brandIdLoc = brandId;
			
			txtFreeDelivery.Text = Content.GlobalOptionsManager.GetFreeDeliveryFrom(brandId).ToString();
			txtDeliveryPrice.Text = Content.GlobalOptionsManager.GetDeliveryPrice(brandId).ToString();
			txtReserveTime.Text = Content.GlobalOptionsManager.GetReserveTime(brandId).ToString();
			txtOverheadTime.Text = Content.GlobalOptionsManager.GetOverheadTime(brandId).ToString();
			txtCookingTime.Text = Content.GlobalOptionsManager.GetCookTime(brandId).ToString();
			txtMaxGap.Text = Content.GlobalOptionsManager.GetMaxTimeGap(brandId).ToString();

			txtSites.Text = Content.GlobalOptionsManager.GetSites(brandId);
			txtCheckFooter.Text = Content.GlobalOptionsManager.GetCheckFooter(brandId);

			firstStringChechTextBox.Text = Content.GlobalOptionsManager.GetFirstStringCheck(brandId);

			operNumberTextBox.Text = Content.GlobalOptionsManager.GetOperName(brandId);
		}


		//_____________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Сохранить текущие настройки с контролов формы  </summary>
		////===================================================================
		public void SaveCurrentSettingFromFormControls()
		{
			int brandId = brandIdLoc;

            Content.GlobalOptionsManager.SetFreeDeliveryFrom(int.Parse(txtFreeDelivery.Text), brandId);
            Content.GlobalOptionsManager.SetDeliveryPrice(int.Parse(txtDeliveryPrice.Text), brandId);
            Content.GlobalOptionsManager.SetReserveTime(int.Parse(txtReserveTime.Text), brandId);
            Content.GlobalOptionsManager.SetOverheadTime(int.Parse(txtOverheadTime.Text), brandId);
            Content.GlobalOptionsManager.SetCookTime(int.Parse(txtCookingTime.Text), brandId);
            Content.GlobalOptionsManager.SetMaxTimeGap(int.Parse(txtMaxGap.Text), brandId);

            Content.GlobalOptionsManager.SetFirstStringCheck(firstStringChechTextBox.Text, brandId);

            Content.GlobalOptionsManager.SetSites(txtSites.Text, brandId);
            Content.GlobalOptionsManager.SetCheckFooter(txtCheckFooter.Text, brandId);

            Content.GlobalOptionsManager.SetOperName(int.Parse(operNumberTextBox.Text), brandId);
		}

		
		//_______________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "настройки почты разработчиков" </summary>
		////===========================================================
		private void devMailSettingsButton_Click(object sender, EventArgs e)
		{
			var mailSettings = new DevEmailSettingsForm();
			mailSettings.ShowDialog();
		}


		//_______________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Изменить шрифт принтера" </summary>
		////=====================================================
		private void changePrinterFontButton_Click(object sender, EventArgs e)
		{
			var printerSettingsForm = new PrinterFontSettingsForm(brandIdLoc);
			printerSettingsForm.ShowDialog();
		}
	}
}
