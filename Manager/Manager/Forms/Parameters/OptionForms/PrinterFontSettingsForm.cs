﻿using System.Collections.Generic;
using System.Windows.Forms;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Others.GlobalOptions;
using Font = System.Drawing.Font;

namespace LaRenzo.Forms.Parameters.OptionForms
{
    public partial class PrinterFontSettingsForm : Form
    {
        /// <summary> Структура информации о шрифте </summary>
        private struct FontViwer
        {
            /// <summary> Шрифт </summary>
            public Font Font { get; set; }

            /// <summary> Название бренда </summary>
            public string BrandName { get; set; }

            /// <summary> Заголовок </summary>
            public string TitleName { get; set; }

            /// <summary> Применяемость </summary>
            public string UseName { get; set; }

            /// <summary> Название (для отображения) </summary>
            public string FontName { get; set; }

            /// <summary> Размер (для отображения) </summary>
            public string FontSize { get; set; }

            /// <summary> Поведеине (для отображения) </summary>
            public string Unit { get; set; }

            /// <summary> Стиль (для отображения) </summary>
            public string Style { get; set; }

            /// <summary> Описание</summary>
            public string Description { get; set; }
        }

        /// <summary> Список шрифтов для отображения </summary>
        private List<FontViwer> fontList;

        private int _brandId;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="brandIdLoc"></param>
        public PrinterFontSettingsForm(int brandIdLoc)
        {
            InitializeComponent();
            _brandId = brandIdLoc;
            // Загрузить информацию о принтерах в грид
            LoadFontsSettingsToGrid();
        }


        //______________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Загрузить информацию о принтерах в грид </summary>
        ////============================================================
        private void LoadFontsSettingsToGrid()
        {
            PrintersFontsOptionsFunctions.Instance.CurrentBrandIdForSave = _brandId;
            fontList = new List<FontViwer>
            {
                GetFontViwer(PrintersFontsOptionsFunctions.Instance.PrinterHeaderFont, "Заголовок", PrinterFontUseNames.HEADER, "Шрифт для заголока всех чеков, кроме чека информации о блюдах "),
                GetFontViwer(PrintersFontsOptionsFunctions.Instance.PrinterBodyFont, "Тело", PrinterFontUseNames.BODY, "Шрифт для чека: сессии инфо об админ. операторы, время кухни для цехов кухни, доставки, оператора на кухню"),
                GetFontViwer(PrintersFontsOptionsFunctions.Instance.PrinterItemsFont, "Блюда", PrinterFontUseNames.ITEMS, "Шрифт для блюд в чеке для кухни"),
                GetFontViwer(PrintersFontsOptionsFunctions.Instance.PrinterSmallFont, "Маленький", PrinterFontUseNames.SMALL, "Шрифт для тела чека для клиента"),
                GetFontViwer(PrintersFontsOptionsFunctions.Instance.PrinterStikyFont, "Стикер", PrinterFontUseNames.STICKY, "Шрифт для тела чека информации о блюдах в заказе"),
                GetFontViwer(PrintersFontsOptionsFunctions.Instance.PrinterSessionFont, "Сессия", PrinterFontUseNames.SESSION_BODY, "Шрифт для тела чека при закрытии сессии")
            };

            PrintersFontsOptionsFunctions.Instance.CurrentBrandIdForSave = null;
            fontsDataGridView.AutoGenerateColumns = false;
            fontsDataGridView.DataSource = fontList;
        }


        //______________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Получить объект для отображения информации о шрифте </summary>
        ////========================================================================
        private FontViwer GetFontViwer(Font font, string title, string useName, string description)
        {
            var fv = new FontViwer
            {
                Font = font,
                TitleName = title,
                UseName = useName,
                FontName = font.FontFamily.Name,
                FontSize = font.Size.ToString(),
                Style = font.Style.ToString(),
                Unit = font.Unit.ToString(),
                Description = description,
                BrandName = Content.BrandManager.GetById(_brandId).Name
            };

            return fv;
        }


        //____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Двойной клик по элементу списка шрифтов принтеров </summary>
        ////======================================================================
        private void FontsDataGridViewMouseDoubleClick(object sender, MouseEventArgs e)
        {
            // Получить выбранные строки
            var selectedRows = fontsDataGridView.SelectedRows;

            if (selectedRows.Count > 0)
            {
                var fontViwer = (FontViwer)selectedRows[0].DataBoundItem;

                var fd = new FontDialog
                {
                    Font = fontViwer.Font
                };
                fd.ShowDialog();

                fontViwer = GetFontViwer(fd.Font, fontViwer.TitleName, fontViwer.UseName, fontViwer.Description);

                UpdateItemFromList(fontViwer);
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Обновить элемент в списке шрифтов </summary>
        ////======================================================
        private void UpdateItemFromList(FontViwer fontViwer)
        {
            for (int i = 0; i <= fontList.Count; i++)
            {
                if (fontList[i].UseName == fontViwer.UseName)
                {
                    fontList[i] = fontViwer;
                    fontsDataGridView.DataSource = null;
                    fontsDataGridView.DataSource = fontList;

                    break;
                }
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Сохранить настройки принтеров </summary>
        ////==================================================
        private void SavePrinterSettings()
        {
            PrintersFontsOptionsFunctions.Instance.CurrentBrandIdForSave = _brandId;
            foreach (FontViwer fontViwer in fontList)
            {
                switch (fontViwer.UseName)
                {
                    case PrinterFontUseNames.HEADER:
                        PrintersFontsOptionsFunctions.Instance.PrinterHeaderFont = fontViwer.Font;
                        break;
                    case PrinterFontUseNames.BODY:
                        PrintersFontsOptionsFunctions.Instance.PrinterBodyFont = fontViwer.Font;
                        break;
                    case PrinterFontUseNames.ITEMS:
                        PrintersFontsOptionsFunctions.Instance.PrinterItemsFont = fontViwer.Font;
                        break;
                    case PrinterFontUseNames.SMALL:
                        PrintersFontsOptionsFunctions.Instance.PrinterSmallFont = fontViwer.Font;
                        break;
                    case PrinterFontUseNames.STICKY:
                        PrintersFontsOptionsFunctions.Instance.PrinterStikyFont = fontViwer.Font;
                        break;
                    case PrinterFontUseNames.SESSION_BODY:
                        PrintersFontsOptionsFunctions.Instance.PrinterSessionFont = fontViwer.Font;
                        break;
                }
            }
            PrintersFontsOptionsFunctions.Instance.CurrentBrandIdForSave = null;
        }


        //____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Кнопка "Ok" </summary>
        ////================================
        private void OKButtonClick(object sender, System.EventArgs e)
        {
            SavePrinterSettings();
            Close();
        }


        //____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Кнопка "Отмена" </summary>
        ////====================================
        void CancelButtonClick(object sender, System.EventArgs e)
        {
            Close();
        }


        //____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Кнопка "По умолчанию" </summary>
        ////==========================================
        private void DefaultFontsButtonClick(object sender, System.EventArgs e)
        {
            if (MessageBox.Show(@"Точно установить настройки шрифтов по умолчанию?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                PrintersFontsOptionsFunctions.Instance.CurrentBrandIdForSave = _brandId;
                PrintersFontsOptionsFunctions.Instance.SetDefaultFontsParams();
                PrintersFontsOptionsFunctions.Instance.CurrentBrandIdForSave = null;
                LoadFontsSettingsToGrid();
            }
        }
    }
}
