﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.Forms.Controls;

namespace LaRenzo.Forms.Parameters.Branch
{
    public partial class BranchComboBox : BaseCombo<DataRepository.Entities.Branch.Branch>
    {
        internal override string FildName()
        {
            return "BranchName";
        }

        public BranchComboBox()
        {
            InitializeComponent();
        }

        internal override List<DataRepository.Entities.Branch.Branch> GetList(FilterModel filter = null)
        {
            var list = Content.BranchRepository.GetList(filter, false);
            return list.ToList();
        }

        protected override void OnSelectedItem()
        {
            if (SelectValue != null)
            {
                Content.BranchRepository.SelectedBranchId = SelectValue.ID;
            }
        }
    }
}
