﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using LaRenzo.DataRepository.Repositories;

namespace LaRenzo.Forms.Parameters.PointOfSale
{
    public partial class PointOfSaleEdit : Form
    {
        private DataRepository.Entities.PointOfSale _pointOfSale;
		/// <summary> Значение контрола </summary>
		public DataRepository.Entities.PointOfSale ItemValue
		{
			get
			{
			    _pointOfSale.PointName = nameTextBox.Text;
				return _pointOfSale;
			}
			set
			{
			    _pointOfSale = value;
				nameTextBox.Text = value.PointName ?? "";
			}
		}

        //___________________________________________________________________________________________________________________________________________________________________________________
		public PointOfSaleEdit(DataRepository.Entities.PointOfSale editItem = null)
		{
			InitializeComponent();
            okButton.Click += async(s,e) => await OkButtonClick();
		    if (editItem != null)
		    {
		        ItemValue = editItem;
		    }
		    else
		    {
		        ItemValue = new DataRepository.Entities.PointOfSale();
		    }
		}
		
		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Сохранить" </summary>
		////=======================================
        private async Task OkButtonClick()
        {
			if (ItemValue != null)
			{
				await Content.PointOfSaleRepository.InsertOrUpdateAsync(ItemValue);
                DialogResult = DialogResult.OK;
			}
        }


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Удалить" </summary>
		////=======================================
		private void CancelButtonClick(object sender, EventArgs e)
        {
			DialogResult = DialogResult.Cancel;
        }
    }
}
