﻿namespace LaRenzo.Forms.HelpForms
{
	partial class SendMailForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.fromTextBox = new System.Windows.Forms.TextBox();
			this.titleTextBox = new System.Windows.Forms.TextBox();
			this.messageTextBox = new System.Windows.Forms.TextBox();
			this.appDevRadioButton = new System.Windows.Forms.RadioButton();
			this.siteDevRadioButton = new System.Windows.Forms.RadioButton();
			this.allDevRadioButton = new System.Windows.Forms.RadioButton();
			this.sendButton = new System.Windows.Forms.Button();
			this.sendMailProgressBar = new System.Windows.Forms.ProgressBar();
			this.sendMailBackgroundWorker = new System.ComponentModel.BackgroundWorker();
			this.label5 = new System.Windows.Forms.Label();
			this.selectFilrTextBox = new System.Windows.Forms.TextBox();
			this.selectFileButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(62, 17);
			this.label1.TabIndex = 0;
			this.label1.Text = "От кого:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 36);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(45, 17);
			this.label2.TabIndex = 1;
			this.label2.Text = "Кому:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 128);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(46, 17);
			this.label3.TabIndex = 2;
			this.label3.Text = "Тема:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 192);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(88, 17);
			this.label4.TabIndex = 3;
			this.label4.Text = "Сообщение:";
			// 
			// fromTextBox
			// 
			this.fromTextBox.Enabled = false;
			this.fromTextBox.Location = new System.Drawing.Point(113, 6);
			this.fromTextBox.Name = "fromTextBox";
			this.fromTextBox.Size = new System.Drawing.Size(275, 22);
			this.fromTextBox.TabIndex = 4;
			// 
			// titleTextBox
			// 
			this.titleTextBox.Location = new System.Drawing.Point(113, 122);
			this.titleTextBox.Name = "titleTextBox";
			this.titleTextBox.Size = new System.Drawing.Size(275, 22);
			this.titleTextBox.TabIndex = 5;
			this.titleTextBox.Text = "Проблемы с программой";
			// 
			// messageTextBox
			// 
			this.messageTextBox.AcceptsReturn = true;
			this.messageTextBox.AcceptsTab = true;
			this.messageTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.messageTextBox.Location = new System.Drawing.Point(113, 192);
			this.messageTextBox.Multiline = true;
			this.messageTextBox.Name = "messageTextBox";
			this.messageTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.messageTextBox.Size = new System.Drawing.Size(682, 257);
			this.messageTextBox.TabIndex = 6;
			// 
			// appDevRadioButton
			// 
			this.appDevRadioButton.AutoSize = true;
			this.appDevRadioButton.Checked = true;
			this.appDevRadioButton.Location = new System.Drawing.Point(113, 36);
			this.appDevRadioButton.Name = "appDevRadioButton";
			this.appDevRadioButton.Size = new System.Drawing.Size(200, 21);
			this.appDevRadioButton.TabIndex = 7;
			this.appDevRadioButton.TabStop = true;
			this.appDevRadioButton.Text = "Разработчики программы";
			this.appDevRadioButton.UseVisualStyleBackColor = true;
			// 
			// siteDevRadioButton
			// 
			this.siteDevRadioButton.AutoSize = true;
			this.siteDevRadioButton.Location = new System.Drawing.Point(113, 63);
			this.siteDevRadioButton.Name = "siteDevRadioButton";
			this.siteDevRadioButton.Size = new System.Drawing.Size(165, 21);
			this.siteDevRadioButton.TabIndex = 8;
			this.siteDevRadioButton.Text = "Разработчики сайта";
			this.siteDevRadioButton.UseVisualStyleBackColor = true;
			// 
			// allDevRadioButton
			// 
			this.allDevRadioButton.AutoSize = true;
			this.allDevRadioButton.Location = new System.Drawing.Point(113, 90);
			this.allDevRadioButton.Name = "allDevRadioButton";
			this.allDevRadioButton.Size = new System.Drawing.Size(62, 21);
			this.allDevRadioButton.TabIndex = 9;
			this.allDevRadioButton.Text = "Всем";
			this.allDevRadioButton.UseVisualStyleBackColor = true;
			// 
			// sendButton
			// 
			this.sendButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.sendButton.Location = new System.Drawing.Point(682, 455);
			this.sendButton.Name = "sendButton";
			this.sendButton.Size = new System.Drawing.Size(113, 32);
			this.sendButton.TabIndex = 10;
			this.sendButton.Text = "Отправить";
			this.sendButton.UseVisualStyleBackColor = true;
			this.sendButton.Click += new System.EventHandler(this.SendButtonClick);
			// 
			// sendMailProgressBar
			// 
			this.sendMailProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.sendMailProgressBar.Location = new System.Drawing.Point(113, 455);
			this.sendMailProgressBar.Name = "sendMailProgressBar";
			this.sendMailProgressBar.Size = new System.Drawing.Size(548, 32);
			this.sendMailProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
			this.sendMailProgressBar.TabIndex = 20;
			this.sendMailProgressBar.Visible = false;
			// 
			// sendMailBackgroundWorker
			// 
			this.sendMailBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.SendMailBackgroundWorkerDoWork);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(12, 162);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(49, 17);
			this.label5.TabIndex = 21;
			this.label5.Text = "Файл:";
			// 
			// selectFilrTextBox
			// 
			this.selectFilrTextBox.Location = new System.Drawing.Point(113, 159);
			this.selectFilrTextBox.Name = "selectFilrTextBox";
			this.selectFilrTextBox.Size = new System.Drawing.Size(548, 22);
			this.selectFilrTextBox.TabIndex = 22;
			// 
			// selectFileButton
			// 
			this.selectFileButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.selectFileButton.Location = new System.Drawing.Point(682, 159);
			this.selectFileButton.Name = "selectFileButton";
			this.selectFileButton.Size = new System.Drawing.Size(113, 27);
			this.selectFileButton.TabIndex = 23;
			this.selectFileButton.Text = "Выбрать";
			this.selectFileButton.UseVisualStyleBackColor = true;
			this.selectFileButton.Click += new System.EventHandler(this.SelectFileButtonClick);
			// 
			// SendMailForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(807, 494);
			this.Controls.Add(this.selectFileButton);
			this.Controls.Add(this.selectFilrTextBox);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.sendMailProgressBar);
			this.Controls.Add(this.sendButton);
			this.Controls.Add(this.allDevRadioButton);
			this.Controls.Add(this.siteDevRadioButton);
			this.Controls.Add(this.appDevRadioButton);
			this.Controls.Add(this.messageTextBox);
			this.Controls.Add(this.titleTextBox);
			this.Controls.Add(this.fromTextBox);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "SendMailForm";
			this.Text = "Связь с разработчиками";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox fromTextBox;
		private System.Windows.Forms.TextBox titleTextBox;
		private System.Windows.Forms.TextBox messageTextBox;
		private System.Windows.Forms.RadioButton appDevRadioButton;
		private System.Windows.Forms.RadioButton siteDevRadioButton;
		private System.Windows.Forms.RadioButton allDevRadioButton;
		private System.Windows.Forms.Button sendButton;
		private System.Windows.Forms.ProgressBar sendMailProgressBar;
		private System.ComponentModel.BackgroundWorker sendMailBackgroundWorker;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox selectFilrTextBox;
		private System.Windows.Forms.Button selectFileButton;
	}
}