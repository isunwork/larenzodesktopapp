﻿using System;
using System.Linq;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Others.HelpPages;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Others.HelpPages;

namespace LaRenzo.Forms.HelpForms
{
    public partial class HelpForm : Form
    {
        private static HelpForm _activeHelpForm;
        
        
        private HelpPage _activeHelpPage;

        public HelpPage ActiveHelpPage
        {
            get { return _activeHelpPage; }
            set
            {
                _activeHelpPage = value; 

            }
        }

        public HelpForm()
        {
            InitializeComponent();
            Bind();
            toolStrip.Visible = Content.UserManager.IsAdmin();
        }

        private void treeList1_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {

        }

        private void ShowPage(HelpPage page, bool forceShowForm = true)
        {
            Bind();
            
            if (forceShowForm)
            {
                Show();
                BringToFront(); 
            }

            var targetNode = treeList1.Nodes.FirstOrDefault(x => ((HelpPage)treeList1.GetDataRecordByNode(x)).InternalName == page.InternalName);
            treeList1.FocusedNode = targetNode;
        }

        public static void GetHelpForm(string internalName, string name = "")
        {
            var page = Content.HelpPageManager.GetExistingOrCreate(internalName,
                string.IsNullOrWhiteSpace(name) ? "Новая страница" : name);

            if (_activeHelpForm == null) _activeHelpForm = new HelpForm();
            _activeHelpForm.Bind();
            _activeHelpForm.ShowPage(page);

        }

        private void HelpForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _activeHelpForm = null;
        }

        private void Bind()
        {
            treeList1.DataSource = Content.HelpPageManager.GetItems();
        }

        private void treeList1_Click(object sender, EventArgs e)
        {

        }

        private void treeList1_AfterFocusNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
        {
            var page = (HelpPage)treeList1.GetDataRecordByNode(e.Node);
            richEditControl1.WordMLText = page.Content;}

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (treeList1.FocusedNode != null)
            {
                var editorForm = new HelpPageEditor(((HelpPage) treeList1.GetDataRecordByNode(treeList1.FocusedNode)));
                editorForm.Saved += (o, args) => ShowPage(args.HelpPage, false);
                editorForm.Show();
            }
                
        }

        private void btnAddPage_Click(object sender, EventArgs e)
        {
            var editorForm = new HelpPageEditor();
            editorForm.Saved += (o, args) => ShowPage(args.HelpPage, false);
            editorForm.Show();
        }

        private void btnRemovePage_Click(object sender, EventArgs e)
        {
            var node = treeList1.FocusedNode;
            if (node == null) return;

            var page = (HelpPage)treeList1.GetDataRecordByNode(node);

            if (
                MessageBox.Show("Вы действительно хотите удалить страницу?", "Удаление страницы",
                    MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Content.HelpPageManager.DeleteItem(page);
                Bind();
            }
        }

    }
}
