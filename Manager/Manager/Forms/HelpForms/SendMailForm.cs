﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Others.GlobalOptions;
using LaRenzo.Properties;
using LaRenzo.Tools;

namespace LaRenzo.Forms.HelpForms
{
    public partial class SendMailForm : Form
	{
		/// <summary> Ошибка отправки почты </summary>
		private string sendMailError = "";
		
		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Конструктор </summary>
		////================================
		public SendMailForm()
		{
			InitializeComponent();

			// Подвязка события "окончание отправки почты"
			sendMailBackgroundWorker.RunWorkerCompleted += (SendMailBackgroundWorkerRunWorkerCompleted);

			// Залогиненный пользователь
			fromTextBox.Text = Content.UserManager.GetUserNameOrEmptyString();
		}


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Отправить" </summary>
		////=======================================
		private void SendButtonClick(object sender, EventArgs e)
		{
			if (sendMailBackgroundWorker.IsBusy == false)
			{
				sendMailError = "";
				// Элементы формы не доступны
				FormElementsEnabled = false;
				sendMailBackgroundWorker.RunWorkerAsync();
			}
		}


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить параметры почты </summary>
		////=============================================
		private MailSendParams GetMailSendParams(string toMailAddr, bool isUseOldFile = false)
		{
			var mailParams = new MailSendParams
			{
				MailTitle = titleTextBox.Text,
				Message = "Пишет вам: " + fromTextBox.Text + "\r\n" + messageTextBox.Text,
				ToMailAddress = toMailAddr,
				FromParam = new MailSendParamsFrom
				{
					Address = Content.GlobalOptionsManager.EmailAddress,IsEnabledSSL = Content.GlobalOptionsManager.EmailUseSSL,
					User = Content.GlobalOptionsManager.EmailUser,
					Pass = Content.GlobalOptionsManager.EmailPass,
					SmtpServer = Content.GlobalOptionsManager.EmailSMTPServer,
					SmtpServerPort = Content.GlobalOptionsManager.EmailSMTPPort
				},
				AttachListFileNames = new List<string>(),
				AttachListFileBodys = new List<string>()
			};

			// Файл лога
			if (!string.IsNullOrEmpty(GetLogFilePath()))
			{
				mailParams.AttachListFileNames.Add(GetLogFilePath());
			}
			// Файл производительности
			mailParams.AttachListFileNames.Add(SaveAndGetPowerFilePath(isUseOldFile));

			// Присоединённый файл
			if (!string.IsNullOrEmpty(selectFilrTextBox.Text) && File.Exists(selectFilrTextBox.Text))
			{
				mailParams.AttachListFileNames.Add(selectFilrTextBox.Text);	
			}
			
			return mailParams;
		}

        //___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить путь к лог файлу </summary>
		////==============================================
		private string GetLogFilePath()
		{
			string fileName = DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + ".log";
			string dirPath = Application.StartupPath;
			string fullPath = dirPath + "\\" + fileName;

			if (!File.Exists(fullPath))
			{
				fullPath = "";
			}

			return fullPath;
		}


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить путь к файлу производительности </summary>
		////=============================================================
		private string SaveAndGetPowerFilePath(bool isUseOldFile)
		{
			BindingList<ExucutionTimeLogEntry> poverWorkItems = ExecutionTimeLogger.Logs;

			string fileName = DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + 
															 "-" + DateTime.Now.Millisecond.ToString() + "_ExecutionTime.txt";
			string dirPath = Path.GetTempPath(); 
			string folderPath = dirPath + "\\Temp\\";
			string fullPath = folderPath + fileName;

			// Усли не использовать существующий, создать и наполнить файл
			if (!isUseOldFile)
			{
				// Проверка на существование пути
				var di = new DirectoryInfo(folderPath);
				if (!di.Exists)
				{
					di.Create();
				}

				var fi = new FileInfo(fullPath);
				StreamWriter sw = fi.CreateText();

				foreach (var item in poverWorkItems)
				{
					sw.WriteLine(item.Created + " " + item.Message + " " + item.Elapsed + " " + item.TooLong);
				}

				sw.Close();
			}

			return fullPath;
		}

		
		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Выполнить в фоне отправку почты </summary>
		////====================================================
		private void SendMailBackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
		{
			var mailManager = new SendMailManager();
			var mailParams = GetMailSendParams(Content.GlobalOptionsManager.AppDevEmail);

			if (appDevRadioButton.Checked || allDevRadioButton.Checked)
			{
				mailManager.SendMail(mailParams);
			}

			if (siteDevRadioButton.Checked || allDevRadioButton.Checked)
			{
				mailParams.ToMailAddress = Content.GlobalOptionsManager.SiteDevEmail;
				mailManager.SendMail(mailParams);
			}
		}


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Отправка почты окончена </summary>
		////============================================
		void SendMailBackgroundWorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			if (string.IsNullOrEmpty(sendMailError))
			{
				string messageText = allDevRadioButton.Checked ? "Сообщения упешно отправлены" : "Сообщение упешно отправлено";
				
				MessageBox.Show(messageText);
			}
			else
			{
				MessageBox.Show(@"Произошла ошибка:\r\n" + sendMailError);
			}

			// Элементы формы доступны
			FormElementsEnabled = true;
		}


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Доступность элементов формы </summary>
		private bool FormElementsEnabled
		{
			set
			{
				sendMailProgressBar.Visible = !value;

				appDevRadioButton.Enabled = value;
				siteDevRadioButton.Enabled = value;
				allDevRadioButton.Enabled = value;
				titleTextBox.Enabled = value;
				messageTextBox.Enabled = value;
				sendButton.Enabled = value;
				selectFilrTextBox.Enabled = value;
			}
		}


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Открыть диалог выбора файла </summary>
		////================================================
		private void SelectFileButtonClick(object sender, EventArgs e)
		{
			var ofd = new OpenFileDialog();
			ofd.ShowDialog();

			selectFilrTextBox.Text = ofd.FileName;
		}
	}
}
