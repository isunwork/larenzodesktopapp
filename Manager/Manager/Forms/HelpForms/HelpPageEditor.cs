﻿using System;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Others.HelpPages;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Others.HelpPages;

namespace LaRenzo.Forms.HelpForms
{
    public partial class HelpPageEditor : Form
    {
        private HelpPage _page;

        public delegate void SavedEventHandler(object sender, HelpPageEditorSavedEventArgs args);

        public event SavedEventHandler Saved;

        protected virtual void OnSaved(HelpPageEditorSavedEventArgs args)
        {
            var handler = Saved;
            if (handler != null) handler(this, args);
        }

        public HelpPage Page
        {
            get { return _page; }
            set
            {
                _page = value;
                if (_page != null)
                {
                    barPageInternalName.Enabled = false;
                    barPageInternalName.EditValue = _page.InternalName;
                    barPageName.EditValue = _page.Name;
                    helpPageRichEditControl.WordMLText = _page.Content;
                }
                else _page = new HelpPage();

            }
        }

        public HelpPageEditor(HelpPage page = null)
        {
            InitializeComponent();
            Page = page;
            helpPageRichEditControl.HelpPageContentSaved += SavePageContent;
        }

        private void SavePageContent(object sender, EventArgs args)
        {
            var editor = (HelpPageRichEditControl) sender;
            Page.Content = editor.WordMLText;
            Page.Name = barPageName.EditValue == null ? string.Empty : barPageName.EditValue.ToString();
            Page.InternalName = barPageInternalName.EditValue == null ? string.Empty : barPageInternalName.EditValue.ToString();

            var helpmanager = Content.HelpPageManager;

            if (helpmanager.InternalNameAllowed(Page))
            {
                helpmanager.InsertOrUpdateItem(Page);
                OnSaved(new HelpPageEditorSavedEventArgs(Page));
            }
            else
            {
                MessageBox.Show("Внутреннее имя страницы не допустимо! Скорее всего страница с таким именем уже существует.");
            }
 
        }

        private void barPageInternalName_EditValueChanged(object sender, EventArgs e)
        {
            helpPageRichEditControl.Modified = true;
        }

        private void barPageName_EditValueChanged(object sender, EventArgs e)
        {
            helpPageRichEditControl.Modified = true;

            if (barPageInternalName.EditValue == null) barPageInternalName.EditValue = barPageName.EditValue;
        }
    }

    public class HelpPageEditorSavedEventArgs : EventArgs
    {
        public HelpPage HelpPage { get; set; }

        public HelpPageEditorSavedEventArgs(HelpPage page)
        {
            HelpPage = page;
        }
    }
}
