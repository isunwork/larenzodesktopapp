﻿namespace LaRenzo.Forms.MainFormAndIM
{
    partial class SplashScreen2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.marqueeProgressBarControl3 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl3.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.EditValue = global::LaRenzo.Properties.Resources.logo;
            this.pictureEdit2.Location = new System.Drawing.Point(-19, -5);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.AllowFocused = false;
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.LimeGreen;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.ShowMenu = false;
            this.pictureEdit2.Size = new System.Drawing.Size(489, 298);
            this.pictureEdit2.TabIndex = 9;
            this.pictureEdit2.EditValueChanged += new System.EventHandler(this.pictureEdit2_EditValueChanged);
            // 
            // marqueeProgressBarControl3
            // 
            this.marqueeProgressBarControl3.EditValue = 0;
            this.marqueeProgressBarControl3.Location = new System.Drawing.Point(51, 206);
            this.marqueeProgressBarControl3.Name = "marqueeProgressBarControl3";
            this.marqueeProgressBarControl3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.marqueeProgressBarControl3.Properties.Appearance.BackColor2 = System.Drawing.Color.White;
            this.marqueeProgressBarControl3.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.marqueeProgressBarControl3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.marqueeProgressBarControl3.Properties.EndColor = System.Drawing.Color.OrangeRed;
            this.marqueeProgressBarControl3.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
            this.marqueeProgressBarControl3.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.marqueeProgressBarControl3.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.marqueeProgressBarControl3.Properties.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.marqueeProgressBarControl3.Size = new System.Drawing.Size(347, 18);
            this.marqueeProgressBarControl3.TabIndex = 11;
            this.marqueeProgressBarControl3.EditValueChanged += new System.EventHandler(this.marqueeProgressBarControl3_EditValueChanged);
            // 
            // SplashScreen2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 274);
            this.Controls.Add(this.marqueeProgressBarControl3);
            this.Controls.Add(this.pictureEdit2);
            this.Name = "SplashScreen2";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl3.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.MarqueeProgressBarControl marqueeProgressBarControl3;
    }
}
