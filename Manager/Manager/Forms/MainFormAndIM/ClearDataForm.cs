﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Others.GlobalOptions;

namespace LaRenzo.Forms.MainFormAndIM
{
    public partial class ClearDataForm : Form
    {
        public Action LogOutAction { get; set; }

        public ClearDataForm()
        {
            InitializeComponent();
            if (MainForm.CurrentMainForm != null)
                LogOutAction = MainForm.CurrentMainForm.LogOut;
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            LogOutAction();
            
            if (Check())
            {
                button1.Text = "Удаление...";
                button1.Enabled = false;
                marqueeProgressBarControl1.Visible = true;
                try
                {
                    await Task.Run(() => DeleteData(dateTimePicker1.Value));
                    button1.Enabled = true;
                    marqueeProgressBarControl1.Visible = false;
                    button1.Text = "Удалить";
                    MessageBox.Show("Данные удалены");
                }
                catch
                {
                    MessageBox.Show("В ходе удаление призошла ошибка!");
                    throw;
                }
            }
            
        }

        private void DeleteData(DateTime value)
        {
            var manager = Content.ClearDataManager;
            manager.RemoveHistory(value);
        }

        private bool Check()
        {
            if (!checkBoxMain.Checked || !checkBoxDBFree.Checked || !checkBoxErrorHandling.Checked ||
                !checkBoxReserve.Checked || checkBoxAttention.Checked)
            {
                MessageBox.Show("Вы неправильно заполнили галочки");
                return false;
            }

            var code = 100;
            int.TryParse(textBoxCode.Text, out code);

            if (code != DateTime.Now.Hour*2)
            {
                MessageBox.Show("Проверочный код не верен!");
                return false;
            }

            if (DateTime.Now.AddDays(-60) < dateTimePicker1.Value)
            {
                MessageBox.Show("Нельзя оставить данные менее чем за 60 дней!");
                return false;
            }

            return true;
        }

        private void checkBoxMain_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxAttention.Enabled =
                checkBoxDBFree.Enabled =
                    checkBoxErrorHandling.Enabled =
                        checkBoxReserve.Enabled = checkBoxMain.Checked;
        }
    }
}
