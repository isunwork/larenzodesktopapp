﻿namespace LaRenzo.Forms.MainFormAndIM
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();}
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, null, true, true);
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManagerWaitFormDetails = new DevExpress.XtraSplashScreen.SplashScreenManager(this, null, true, true);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.FileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HistoryMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SessionsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AllSessionCafeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SaleReportMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.продажиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CafeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DeliveryMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DayReportMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TimeManagerMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TimeTableMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DeliveryScheduleMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CatalogsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PartnerMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.WarehouseMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TablesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PeopleMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.UsersMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DriversMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.праваДоступаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DishesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CategoriesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DishMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SectionsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FactorsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.WarehouseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ProductsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ProductMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ProductCategoriesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ClientMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DiscountMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blackListMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ClassifierMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MeasureMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AddressMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OtherSetsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OtherSetCoeffMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OtherSetRuleMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HandoutsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HandoutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HandoutsRuleMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.брендыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PointOfSaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DocumentsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SupplyMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SupplyDocsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReturnDocMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.GoodMovingMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MovingDocMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.WriteOffDocMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PostingDocMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.InventoryMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.WriteoffDishMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RegulationsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.СashСhangeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReportMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RemainsOfGoodReport = new System.Windows.Forms.ToolStripMenuItem();
            this.SettlsWithPartnerReport = new System.Windows.Forms.ToolStripMenuItem();
            this.TransferOfGoodMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ProductOfPartnersMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.комбинацииБлюдToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OptionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ParametrOptionsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PrintersMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DataTransferMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DataTransferChiefMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FisPrinterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.workWithDiscountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FinanceMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TestFinanceDataMenuOtion = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.TransactionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TransactioCategoryMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.WalletMenuOption = new System.Windows.Forms.ToolStripMenuItem();
            this.аналитикаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сообщенияВБанкToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gjvjomПомощьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToAppToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.devToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StatsMenuButton = new System.Windows.Forms.ToolStripMenuItem();
            this.OrdersByUserMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goneClientForPeriodToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReportNewClientsForPeriodToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.simpleSaleReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dishCompatibilityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemDeliveryTime = new System.Windows.Forms.ToolStripMenuItem();
            this.navBarItem1 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem2 = new DevExpress.XtraNavBar.NavBarItem();
            this.mainTStrip = new System.Windows.Forms.ToolStrip();
            this.buttonLogin = new System.Windows.Forms.ToolStripButton();
            this.buttonSession = new System.Windows.Forms.ToolStripButton();
            this.buttonSessionOptions = new System.Windows.Forms.ToolStripButton();
            this.buttonPerformanceCounter = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.linqServerModeSourceSession = new DevExpress.Data.Linq.LinqServerModeSource();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.AutoUpdater = new System.Windows.Forms.Timer(this.components);
            this.timerWebOrders = new System.Windows.Forms.Timer(this.components);
            this.timerBeepForOrders = new System.Windows.Forms.Timer(this.components);
            this.timerPreOrder = new System.Windows.Forms.Timer(this.components);
            this.lblTabsMessage = new System.Windows.Forms.Label();
            this.mainTb = new System.Windows.Forms.TabControl();
            this.callNotifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.failureOrdersReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.mainTStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.linqServerModeSourceSession)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileMenuItem,
            this.HistoryMenuItem,
            this.TimeManagerMenuItem,
            this.CatalogsMenuItem,
            this.DocumentsMenuItem,
            this.ReportMenuItem,
            this.OptionMenuItem,
            this.FinanceMenuItem,
            this.gjvjomПомощьToolStripMenuItem,
            this.StatsMenuButton});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip.Size = new System.Drawing.Size(1161, 28);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip";
            // 
            // FileMenuItem
            // 
            this.FileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExitMenuItem});
            this.FileMenuItem.Name = "FileMenuItem";
            this.FileMenuItem.Size = new System.Drawing.Size(57, 24);
            this.FileMenuItem.Text = "Файл";
            // 
            // ExitMenuItem
            // 
            this.ExitMenuItem.Name = "ExitMenuItem";
            this.ExitMenuItem.Size = new System.Drawing.Size(128, 26);
            this.ExitMenuItem.Text = "Выйти";
            this.ExitMenuItem.Click += new System.EventHandler(this.ExitMenuItem_Click);
            // 
            // HistoryMenuItem
            // 
            this.HistoryMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(222)))), ((int)(((byte)(222)))));
            this.HistoryMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SessionsMenuItem,
            this.AllSessionCafeMenuItem,
            this.SaleReportMenuItem,
            this.продажиToolStripMenuItem});
            this.HistoryMenuItem.Name = "HistoryMenuItem";
            this.HistoryMenuItem.Size = new System.Drawing.Size(80, 24);
            this.HistoryMenuItem.Tag = "999";
            this.HistoryMenuItem.Text = "История";
            this.HistoryMenuItem.Visible = false;
            // 
            // SessionsMenuItem
            // 
            this.SessionsMenuItem.AccessibleRole = System.Windows.Forms.AccessibleRole.Separator;
            this.SessionsMenuItem.Name = "SessionsMenuItem";
            this.SessionsMenuItem.Size = new System.Drawing.Size(273, 26);
            this.SessionsMenuItem.Tag = "9991";
            this.SessionsMenuItem.Text = "Сессии";
            this.SessionsMenuItem.Click += new System.EventHandler(this.SessionsMenuItem_Click);
            // 
            // AllSessionCafeMenuItem
            // 
            this.AllSessionCafeMenuItem.Name = "AllSessionCafeMenuItem";
            this.AllSessionCafeMenuItem.Size = new System.Drawing.Size(273, 26);
            this.AllSessionCafeMenuItem.Tag = "9992";
            this.AllSessionCafeMenuItem.Text = "Сессии (Кафе)";
            this.AllSessionCafeMenuItem.Click += new System.EventHandler(this.AllOrdersCafeMenuItem_Click);
            // 
            // SaleReportMenuItem
            // 
            this.SaleReportMenuItem.Name = "SaleReportMenuItem";
            this.SaleReportMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.SaleReportMenuItem.Size = new System.Drawing.Size(273, 26);
            this.SaleReportMenuItem.Tag = "9993";
            this.SaleReportMenuItem.Text = "Отчёт по продажам";
            this.SaleReportMenuItem.Click += new System.EventHandler(this.SaleReportMenuItem_Click);
            // 
            // продажиToolStripMenuItem
            // 
            this.продажиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CafeMenuItem,
            this.DeliveryMenuItem,
            this.DayReportMenuItem});
            this.продажиToolStripMenuItem.Name = "продажиToolStripMenuItem";
            this.продажиToolStripMenuItem.Size = new System.Drawing.Size(273, 26);
            this.продажиToolStripMenuItem.Tag = "9994";
            this.продажиToolStripMenuItem.Text = "Продажи";
            // 
            // CafeMenuItem
            // 
            this.CafeMenuItem.Name = "CafeMenuItem";
            this.CafeMenuItem.Size = new System.Drawing.Size(366, 26);
            this.CafeMenuItem.Tag = "99941";
            this.CafeMenuItem.Text = "Документы продажи (Кафе)";
            this.CafeMenuItem.Click += new System.EventHandler(this.CafeMenuItem_Click);
            // 
            // DeliveryMenuItem
            // 
            this.DeliveryMenuItem.Name = "DeliveryMenuItem";
            this.DeliveryMenuItem.Size = new System.Drawing.Size(366, 26);
            this.DeliveryMenuItem.Tag = "99942";
            this.DeliveryMenuItem.Text = "Документы продажи (Доставка)";
            this.DeliveryMenuItem.Click += new System.EventHandler(this.DeliveryMenuItem_Click);
            // 
            // DayReportMenuItem
            // 
            this.DayReportMenuItem.Name = "DayReportMenuItem";
            this.DayReportMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.DayReportMenuItem.Size = new System.Drawing.Size(366, 26);
            this.DayReportMenuItem.Tag = "99943";
            this.DayReportMenuItem.Text = "Ежедневный отчёт по продажам";
            this.DayReportMenuItem.Click += new System.EventHandler(this.DayReportMenuItem_Click);
            // 
            // TimeManagerMenuItem
            // 
            this.TimeManagerMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TimeTableMenuItem,
            this.DeliveryScheduleMenuItem});
            this.TimeManagerMenuItem.Name = "TimeManagerMenuItem";
            this.TimeManagerMenuItem.Size = new System.Drawing.Size(182, 24);
            this.TimeManagerMenuItem.Tag = "12";
            this.TimeManagerMenuItem.Text = "Управление временем";
            this.TimeManagerMenuItem.Visible = false;
            // 
            // TimeTableMenuItem
            // 
            this.TimeTableMenuItem.Name = "TimeTableMenuItem";
            this.TimeTableMenuItem.Size = new System.Drawing.Size(232, 26);
            this.TimeTableMenuItem.Tag = "121";
            this.TimeTableMenuItem.Text = "Таблица времени";
            // 
            // DeliveryScheduleMenuItem
            // 
            this.DeliveryScheduleMenuItem.Name = "DeliveryScheduleMenuItem";
            this.DeliveryScheduleMenuItem.Size = new System.Drawing.Size(232, 26);
            this.DeliveryScheduleMenuItem.Tag = "122";
            this.DeliveryScheduleMenuItem.Text = "Расписание доставки";
            this.DeliveryScheduleMenuItem.Click += new System.EventHandler(this.DeliveryScheduleMenuItem_Click);
            // 
            // CatalogsMenuItem
            // 
            this.CatalogsMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PartnerMenuItem,
            this.WarehouseMenuItem,
            this.TablesMenuItem,
            this.PeopleMenuItem,
            this.DishesMenuItem,
            this.ProductsMenuItem,
            this.ClientMenuItem,
            this.ClassifierMenuItem,
            this.OtherSetsMenuItem,
            this.HandoutsMenuItem,
            this.toolStripMenuItem3,
            this.брендыToolStripMenuItem,
            this.PointOfSaleToolStripMenuItem});
            this.CatalogsMenuItem.Name = "CatalogsMenuItem";
            this.CatalogsMenuItem.Size = new System.Drawing.Size(115, 24);
            this.CatalogsMenuItem.Tag = "13";
            this.CatalogsMenuItem.Text = "Справочники";
            this.CatalogsMenuItem.Visible = false;
            // 
            // PartnerMenuItem
            // 
            this.PartnerMenuItem.Name = "PartnerMenuItem";
            this.PartnerMenuItem.Size = new System.Drawing.Size(246, 26);
            this.PartnerMenuItem.Tag = "131";
            this.PartnerMenuItem.Text = "Поставщики";
            this.PartnerMenuItem.Click += new System.EventHandler(this.PartnerMenuItem_Click);
            // 
            // WarehouseMenuItem
            // 
            this.WarehouseMenuItem.Name = "WarehouseMenuItem";
            this.WarehouseMenuItem.Size = new System.Drawing.Size(246, 26);
            this.WarehouseMenuItem.Tag = "132";
            this.WarehouseMenuItem.Text = "Склады";
            this.WarehouseMenuItem.Click += new System.EventHandler(this.WarehouseMenuItem_Click);
            // 
            // TablesMenuItem
            // 
            this.TablesMenuItem.Name = "TablesMenuItem";
            this.TablesMenuItem.Size = new System.Drawing.Size(246, 26);
            this.TablesMenuItem.Tag = "133";
            this.TablesMenuItem.Text = "Столики";
            this.TablesMenuItem.Click += new System.EventHandler(this.TablesMenuItem_Click);
            // 
            // PeopleMenuItem
            // 
            this.PeopleMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.UsersMenuItem,
            this.DriversMenuItem,
            this.праваДоступаToolStripMenuItem});
            this.PeopleMenuItem.Name = "PeopleMenuItem";
            this.PeopleMenuItem.Size = new System.Drawing.Size(246, 26);
            this.PeopleMenuItem.Tag = "134";
            this.PeopleMenuItem.Text = "Сотрудники";
            this.PeopleMenuItem.Visible = false;
            // 
            // UsersMenuItem
            // 
            this.UsersMenuItem.Name = "UsersMenuItem";
            this.UsersMenuItem.Size = new System.Drawing.Size(186, 26);
            this.UsersMenuItem.Tag = "1341";
            this.UsersMenuItem.Text = "Пользователи";
            this.UsersMenuItem.Click += new System.EventHandler(this.UsersMenuItem_Click);
            // 
            // DriversMenuItem
            // 
            this.DriversMenuItem.Name = "DriversMenuItem";
            this.DriversMenuItem.Size = new System.Drawing.Size(186, 26);
            this.DriversMenuItem.Tag = "1342";
            this.DriversMenuItem.Text = "Водители";
            this.DriversMenuItem.Click += new System.EventHandler(this.DriversMenuItem_Click);
            // 
            // праваДоступаToolStripMenuItem
            // 
            this.праваДоступаToolStripMenuItem.Name = "праваДоступаToolStripMenuItem";
            this.праваДоступаToolStripMenuItem.Size = new System.Drawing.Size(186, 26);
            this.праваДоступаToolStripMenuItem.Tag = "1343";
            this.праваДоступаToolStripMenuItem.Text = "Права доступа";
            this.праваДоступаToolStripMenuItem.Click += new System.EventHandler(this.праваДоступаToolStripMenuItem_Click);
            // 
            // DishesMenuItem
            // 
            this.DishesMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CategoriesMenuItem,
            this.DishMenuItem,
            this.SectionsMenuItem,
            this.FactorsMenuItem,
            this.WarehouseToolStripMenuItem});
            this.DishesMenuItem.Name = "DishesMenuItem";
            this.DishesMenuItem.Size = new System.Drawing.Size(246, 26);
            this.DishesMenuItem.Tag = "135";
            this.DishesMenuItem.Text = "Блюда";
            this.DishesMenuItem.Visible = false;
            // 
            // CategoriesMenuItem
            // 
            this.CategoriesMenuItem.Name = "CategoriesMenuItem";
            this.CategoriesMenuItem.Size = new System.Drawing.Size(246, 26);
            this.CategoriesMenuItem.Tag = "1351";
            this.CategoriesMenuItem.Text = "Категории";
            this.CategoriesMenuItem.Click += new System.EventHandler(this.CategoriesMenuItem_Click);
            // 
            // DishMenuItem
            // 
            this.DishMenuItem.Name = "DishMenuItem";
            this.DishMenuItem.Size = new System.Drawing.Size(246, 26);
            this.DishMenuItem.Tag = "1352";
            this.DishMenuItem.Text = "Блюда";
            this.DishMenuItem.Click += new System.EventHandler(this.DishMenuItem_Click);
            // 
            // SectionsMenuItem
            // 
            this.SectionsMenuItem.Name = "SectionsMenuItem";
            this.SectionsMenuItem.Size = new System.Drawing.Size(246, 26);
            this.SectionsMenuItem.Tag = "1353";
            this.SectionsMenuItem.Text = "Цеха";
            this.SectionsMenuItem.Click += new System.EventHandler(this.SectionsMenuItem_Click);
            // 
            // FactorsMenuItem
            // 
            this.FactorsMenuItem.Name = "FactorsMenuItem";
            this.FactorsMenuItem.Size = new System.Drawing.Size(246, 26);
            this.FactorsMenuItem.Tag = "1354";
            this.FactorsMenuItem.Text = "Наборы коэффицентов";
            this.FactorsMenuItem.Click += new System.EventHandler(this.MenuItemFactors_Click);
            // 
            // WarehouseToolStripMenuItem
            // 
            this.WarehouseToolStripMenuItem.Name = "WarehouseToolStripMenuItem";
            this.WarehouseToolStripMenuItem.Size = new System.Drawing.Size(246, 26);
            this.WarehouseToolStripMenuItem.Text = "Склады для блюд";
            this.WarehouseToolStripMenuItem.Click += new System.EventHandler(this.WarehouseToolStripMenuItem_Click);
            // 
            // ProductsMenuItem
            // 
            this.ProductsMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ProductMenuItem,
            this.ProductCategoriesMenuItem});
            this.ProductsMenuItem.Name = "ProductsMenuItem";
            this.ProductsMenuItem.Size = new System.Drawing.Size(246, 26);
            this.ProductsMenuItem.Tag = "136";
            this.ProductsMenuItem.Text = "Продукты";
            this.ProductsMenuItem.Visible = false;
            // 
            // ProductMenuItem
            // 
            this.ProductMenuItem.Name = "ProductMenuItem";
            this.ProductMenuItem.Size = new System.Drawing.Size(233, 26);
            this.ProductMenuItem.Tag = "1361";
            this.ProductMenuItem.Text = "Продукты";
            this.ProductMenuItem.Click += new System.EventHandler(this.ProductMenuItem_Click);
            // 
            // ProductCategoriesMenuItem
            // 
            this.ProductCategoriesMenuItem.Name = "ProductCategoriesMenuItem";
            this.ProductCategoriesMenuItem.Size = new System.Drawing.Size(233, 26);
            this.ProductCategoriesMenuItem.Tag = "1362";
            this.ProductCategoriesMenuItem.Text = "Категории продуктов";
            this.ProductCategoriesMenuItem.Click += new System.EventHandler(this.ProductCategoriesMenuItem_Click);
            // 
            // ClientMenuItem
            // 
            this.ClientMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DiscountMenuItem,
            this.blackListMenuItem});
            this.ClientMenuItem.Name = "ClientMenuItem";
            this.ClientMenuItem.Size = new System.Drawing.Size(246, 26);
            this.ClientMenuItem.Tag = "137";
            this.ClientMenuItem.Text = "Клиенты";
            // 
            // DiscountMenuItem
            // 
            this.DiscountMenuItem.Name = "DiscountMenuItem";
            this.DiscountMenuItem.Size = new System.Drawing.Size(207, 26);
            this.DiscountMenuItem.Tag = "1371";
            this.DiscountMenuItem.Text = "Скидочные карты";
            this.DiscountMenuItem.Click += new System.EventHandler(this.DiscountMenuItem_Click);
            // 
            // blackListMenuItem
            // 
            this.blackListMenuItem.Name = "blackListMenuItem";
            this.blackListMenuItem.Size = new System.Drawing.Size(207, 26);
            this.blackListMenuItem.Tag = "1372";
            this.blackListMenuItem.Text = "Черный список";
            this.blackListMenuItem.Click += new System.EventHandler(this.blackListMenuItem_Click);
            // 
            // ClassifierMenuItem
            // 
            this.ClassifierMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MeasureMenuItem,
            this.AddressMenuItem});
            this.ClassifierMenuItem.Name = "ClassifierMenuItem";
            this.ClassifierMenuItem.Size = new System.Drawing.Size(246, 26);
            this.ClassifierMenuItem.Tag = "138";
            this.ClassifierMenuItem.Text = "Классификаторы";
            // 
            // MeasureMenuItem
            // 
            this.MeasureMenuItem.Name = "MeasureMenuItem";
            this.MeasureMenuItem.Size = new System.Drawing.Size(229, 26);
            this.MeasureMenuItem.Tag = "1381";
            this.MeasureMenuItem.Text = "Единицы измерения";
            this.MeasureMenuItem.Click += new System.EventHandler(this.MeasureMenuItem_Click);
            // 
            // AddressMenuItem
            // 
            this.AddressMenuItem.Name = "AddressMenuItem";
            this.AddressMenuItem.Size = new System.Drawing.Size(229, 26);
            this.AddressMenuItem.Tag = "1382";
            this.AddressMenuItem.Text = "Адреса";
            this.AddressMenuItem.Click += new System.EventHandler(this.AddressMenuItem_Click);
            // 
            // OtherSetsMenuItem
            // 
            this.OtherSetsMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OtherSetCoeffMenuItem,
            this.OtherSetRuleMenuItem});
            this.OtherSetsMenuItem.Name = "OtherSetsMenuItem";
            this.OtherSetsMenuItem.Size = new System.Drawing.Size(246, 26);
            this.OtherSetsMenuItem.Tag = "139";
            this.OtherSetsMenuItem.Text = "Наборы";
            // 
            // OtherSetCoeffMenuItem
            // 
            this.OtherSetCoeffMenuItem.Name = "OtherSetCoeffMenuItem";
            this.OtherSetCoeffMenuItem.Size = new System.Drawing.Size(271, 26);
            this.OtherSetCoeffMenuItem.Tag = "1391";
            this.OtherSetCoeffMenuItem.Text = "Наборы";
            this.OtherSetCoeffMenuItem.Click += new System.EventHandler(this.OtherSetCoeffMenuItem_Click);
            // 
            // OtherSetRuleMenuItem
            // 
            this.OtherSetRuleMenuItem.Name = "OtherSetRuleMenuItem";
            this.OtherSetRuleMenuItem.Size = new System.Drawing.Size(271, 26);
            this.OtherSetRuleMenuItem.Tag = "1391";
            this.OtherSetRuleMenuItem.Text = "Правила расчеты наборов";
            this.OtherSetRuleMenuItem.Click += new System.EventHandler(this.OtherSetRuleMenuItem_Click);
            // 
            // HandoutsMenuItem
            // 
            this.HandoutsMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.HandoutMenuItem,
            this.HandoutsRuleMenuItem});
            this.HandoutsMenuItem.Enabled = false;
            this.HandoutsMenuItem.Name = "HandoutsMenuItem";
            this.HandoutsMenuItem.Size = new System.Drawing.Size(246, 26);
            this.HandoutsMenuItem.Text = "Раздаточный материал";
            // 
            // HandoutMenuItem
            // 
            this.HandoutMenuItem.Name = "HandoutMenuItem";
            this.HandoutMenuItem.Size = new System.Drawing.Size(385, 26);
            this.HandoutMenuItem.Text = "Раздаточный материал";
            // 
            // HandoutsRuleMenuItem
            // 
            this.HandoutsRuleMenuItem.Name = "HandoutsRuleMenuItem";
            this.HandoutsRuleMenuItem.Size = new System.Drawing.Size(385, 26);
            this.HandoutsRuleMenuItem.Text = "Правила расчета раздаточных материалов";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(243, 6);
            // 
            // брендыToolStripMenuItem
            // 
            this.брендыToolStripMenuItem.Name = "брендыToolStripMenuItem";
            this.брендыToolStripMenuItem.Size = new System.Drawing.Size(246, 26);
            this.брендыToolStripMenuItem.Text = "Бренды";
            this.брендыToolStripMenuItem.Click += new System.EventHandler(this.брендыToolStripMenuItem_Click);
            // 
            // PointOfSaleToolStripMenuItem
            // 
            this.PointOfSaleToolStripMenuItem.Name = "PointOfSaleToolStripMenuItem";
            this.PointOfSaleToolStripMenuItem.Size = new System.Drawing.Size(246, 26);
            this.PointOfSaleToolStripMenuItem.Text = "Точки продаж";
            this.PointOfSaleToolStripMenuItem.Click += new System.EventHandler(this.PointOfSaleToolStripMenuItem_Click);
            // 
            // DocumentsMenuItem
            // 
            this.DocumentsMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SupplyMenuItem,
            this.GoodMovingMenuItem,
            this.RegulationsMenuItem});
            this.DocumentsMenuItem.Name = "DocumentsMenuItem";
            this.DocumentsMenuItem.Size = new System.Drawing.Size(99, 24);
            this.DocumentsMenuItem.Tag = "14";
            this.DocumentsMenuItem.Text = "Документы";
            // 
            // SupplyMenuItem
            // 
            this.SupplyMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SupplyDocsMenuItem,
            this.ReturnDocMenuItem});
            this.SupplyMenuItem.Name = "SupplyMenuItem";
            this.SupplyMenuItem.Size = new System.Drawing.Size(289, 26);
            this.SupplyMenuItem.Tag = "142";
            this.SupplyMenuItem.Text = "Закупки и возврат";
            // 
            // SupplyDocsMenuItem
            // 
            this.SupplyDocsMenuItem.Name = "SupplyDocsMenuItem";
            this.SupplyDocsMenuItem.Size = new System.Drawing.Size(255, 26);
            this.SupplyDocsMenuItem.Tag = "1421";
            this.SupplyDocsMenuItem.Text = "Документы поступления";
            this.SupplyDocsMenuItem.Click += new System.EventHandler(this.SupplyDocsMenuItem_Click);
            // 
            // ReturnDocMenuItem
            // 
            this.ReturnDocMenuItem.Name = "ReturnDocMenuItem";
            this.ReturnDocMenuItem.Size = new System.Drawing.Size(255, 26);
            this.ReturnDocMenuItem.Tag = "1422";
            this.ReturnDocMenuItem.Text = "Документы возвратов";
            this.ReturnDocMenuItem.Click += new System.EventHandler(this.ReturnDocMenuItem_Click);
            // 
            // GoodMovingMenuItem
            // 
            this.GoodMovingMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MovingDocMenuItem,
            this.WriteOffDocMenuItem,
            this.PostingDocMenuItem,
            this.InventoryMenuItem,
            this.toolStripSeparator4,
            this.WriteoffDishMenuItem});
            this.GoodMovingMenuItem.Name = "GoodMovingMenuItem";
            this.GoodMovingMenuItem.Size = new System.Drawing.Size(289, 26);
            this.GoodMovingMenuItem.Tag = "143";
            this.GoodMovingMenuItem.Text = "Внутреннее товародвижение";
            // 
            // MovingDocMenuItem
            // 
            this.MovingDocMenuItem.Name = "MovingDocMenuItem";
            this.MovingDocMenuItem.Size = new System.Drawing.Size(277, 26);
            this.MovingDocMenuItem.Tag = "1431";
            this.MovingDocMenuItem.Text = "Документы перемещения";
            this.MovingDocMenuItem.Click += new System.EventHandler(this.MovingDocMenuItem_Click);
            // 
            // WriteOffDocMenuItem
            // 
            this.WriteOffDocMenuItem.Name = "WriteOffDocMenuItem";
            this.WriteOffDocMenuItem.Size = new System.Drawing.Size(277, 26);
            this.WriteOffDocMenuItem.Tag = "1432";
            this.WriteOffDocMenuItem.Text = "Документы списания";
            this.WriteOffDocMenuItem.Click += new System.EventHandler(this.WriteOffDocMenuItem_Click);
            // 
            // PostingDocMenuItem
            // 
            this.PostingDocMenuItem.Name = "PostingDocMenuItem";
            this.PostingDocMenuItem.Size = new System.Drawing.Size(277, 26);
            this.PostingDocMenuItem.Tag = "1433";
            this.PostingDocMenuItem.Text = "Документы оприходования";
            this.PostingDocMenuItem.Click += new System.EventHandler(this.PostingDocMenuItem_Click);
            // 
            // InventoryMenuItem
            // 
            this.InventoryMenuItem.Name = "InventoryMenuItem";
            this.InventoryMenuItem.Size = new System.Drawing.Size(277, 26);
            this.InventoryMenuItem.Tag = "1434";
            this.InventoryMenuItem.Text = "Инвентаризация";
            this.InventoryMenuItem.Click += new System.EventHandler(this.InventoryMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(274, 6);
            // 
            // WriteoffDishMenuItem
            // 
            this.WriteoffDishMenuItem.Name = "WriteoffDishMenuItem";
            this.WriteoffDishMenuItem.Size = new System.Drawing.Size(277, 26);
            this.WriteoffDishMenuItem.Tag = "1435";
            this.WriteoffDishMenuItem.Text = "Списание блюд";
            this.WriteoffDishMenuItem.Click += new System.EventHandler(this.WriteoffDishMenuItem_Click);
            // 
            // RegulationsMenuItem
            // 
            this.RegulationsMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.СashСhangeMenuItem});
            this.RegulationsMenuItem.Name = "RegulationsMenuItem";
            this.RegulationsMenuItem.Size = new System.Drawing.Size(289, 26);
            this.RegulationsMenuItem.Tag = "144";
            this.RegulationsMenuItem.Text = "Регламентные документы";
            // 
            // СashСhangeMenuItem
            // 
            this.СashСhangeMenuItem.Name = "СashСhangeMenuItem";
            this.СashСhangeMenuItem.Size = new System.Drawing.Size(268, 26);
            this.СashСhangeMenuItem.Tag = "1441";
            this.СashСhangeMenuItem.Text = "Закрытие кассовой смены";
            this.СashСhangeMenuItem.Click += new System.EventHandler(this.СashСhangeMenuItem_Click);
            // 
            // ReportMenuItem
            // 
            this.ReportMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RemainsOfGoodReport,
            this.SettlsWithPartnerReport,
            this.TransferOfGoodMenuItem,
            this.ProductOfPartnersMenuItem,
            this.комбинацииБлюдToolStripMenuItem,
            this.failureOrdersReportToolStripMenuItem});
            this.ReportMenuItem.Name = "ReportMenuItem";
            this.ReportMenuItem.Size = new System.Drawing.Size(71, 24);
            this.ReportMenuItem.Tag = "15";
            this.ReportMenuItem.Text = "Отчеты";
            this.ReportMenuItem.Visible = false;
            // 
            // RemainsOfGoodReport
            // 
            this.RemainsOfGoodReport.Name = "RemainsOfGoodReport";
            this.RemainsOfGoodReport.Size = new System.Drawing.Size(314, 26);
            this.RemainsOfGoodReport.Tag = "152";
            this.RemainsOfGoodReport.Text = "Остатки товаров на складах";
            this.RemainsOfGoodReport.Click += new System.EventHandler(this.RemainsOfGoodReport_Click);
            // 
            // SettlsWithPartnerReport
            // 
            this.SettlsWithPartnerReport.Name = "SettlsWithPartnerReport";
            this.SettlsWithPartnerReport.Size = new System.Drawing.Size(314, 26);
            this.SettlsWithPartnerReport.Tag = "153";
            this.SettlsWithPartnerReport.Text = "Взаиморасчеты с контрагентами";
            this.SettlsWithPartnerReport.Click += new System.EventHandler(this.SettlsWithPartnerReport_Click);
            // 
            // TransferOfGoodMenuItem
            // 
            this.TransferOfGoodMenuItem.Name = "TransferOfGoodMenuItem";
            this.TransferOfGoodMenuItem.Size = new System.Drawing.Size(314, 26);
            this.TransferOfGoodMenuItem.Tag = "154";
            this.TransferOfGoodMenuItem.Text = "Движение товаров на складах";
            this.TransferOfGoodMenuItem.Click += new System.EventHandler(this.TransferOfGoodMenuItem_Click);
            // 
            // ProductOfPartnersMenuItem
            // 
            this.ProductOfPartnersMenuItem.Name = "ProductOfPartnersMenuItem";
            this.ProductOfPartnersMenuItem.Size = new System.Drawing.Size(314, 26);
            this.ProductOfPartnersMenuItem.Tag = "155";
            this.ProductOfPartnersMenuItem.Text = "Поставщик -> Продукты";
            this.ProductOfPartnersMenuItem.Click += new System.EventHandler(this.ProductOfPartnersMenuItem_Click);
            // 
            // комбинацииБлюдToolStripMenuItem
            // 
            this.комбинацииБлюдToolStripMenuItem.Name = "комбинацииБлюдToolStripMenuItem";
            this.комбинацииБлюдToolStripMenuItem.Size = new System.Drawing.Size(314, 26);
            this.комбинацииБлюдToolStripMenuItem.Tag = "156";
            this.комбинацииБлюдToolStripMenuItem.Text = "Комбинации блюд";
            this.комбинацииБлюдToolStripMenuItem.Click += new System.EventHandler(this.комбинацииБлюдToolStripMenuItem_Click);
            // 
            // OptionMenuItem
            // 
            this.OptionMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ParametrOptionsMenuItem,
            this.PrintersMenuItem,
            this.DataTransferMenuItem,
            this.FisPrinterToolStripMenuItem,
            this.workWithDiscountToolStripMenuItem});
            this.OptionMenuItem.Name = "OptionMenuItem";
            this.OptionMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.OptionMenuItem.Size = new System.Drawing.Size(95, 24);
            this.OptionMenuItem.Tag = "16";
            this.OptionMenuItem.Text = "Настройка";
            // 
            // ParametrOptionsMenuItem
            // 
            this.ParametrOptionsMenuItem.Name = "ParametrOptionsMenuItem";
            this.ParametrOptionsMenuItem.Size = new System.Drawing.Size(246, 26);
            this.ParametrOptionsMenuItem.Tag = "161";
            this.ParametrOptionsMenuItem.Text = "Настройки программы";
            this.ParametrOptionsMenuItem.Click += new System.EventHandler(this.ParametrOptionsMenuItem_Click);
            // 
            // PrintersMenuItem
            // 
            this.PrintersMenuItem.Name = "PrintersMenuItem";
            this.PrintersMenuItem.Size = new System.Drawing.Size(246, 26);
            this.PrintersMenuItem.Tag = "162";
            this.PrintersMenuItem.Text = "Принтеры";
            this.PrintersMenuItem.Click += new System.EventHandler(this.PrintersMenuItem_Click);
            // 
            // DataTransferMenuItem
            // 
            this.DataTransferMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DataTransferChiefMenuItem});
            this.DataTransferMenuItem.Name = "DataTransferMenuItem";
            this.DataTransferMenuItem.Size = new System.Drawing.Size(246, 26);
            this.DataTransferMenuItem.Tag = "163";
            this.DataTransferMenuItem.Text = "Обмен данными";
            // 
            // DataTransferChiefMenuItem
            // 
            this.DataTransferChiefMenuItem.Name = "DataTransferChiefMenuItem";
            this.DataTransferChiefMenuItem.Size = new System.Drawing.Size(402, 26);
            this.DataTransferChiefMenuItem.Text = "Загрузка данных из программы Шеф Эксперт";
            this.DataTransferChiefMenuItem.Click += new System.EventHandler(this.DataTransferChiefMenuItem_Click);
            // 
            // FisPrinterToolStripMenuItem
            // 
            this.FisPrinterToolStripMenuItem.Name = "FisPrinterToolStripMenuItem";
            this.FisPrinterToolStripMenuItem.Size = new System.Drawing.Size(246, 26);
            this.FisPrinterToolStripMenuItem.Text = "Фискальный принтер";
            this.FisPrinterToolStripMenuItem.Click += new System.EventHandler(this.FisPrinterToolStripMenuItem_Click);
            // 
            // workWithDiscountToolStripMenuItem
            // 
            this.workWithDiscountToolStripMenuItem.Name = "workWithDiscountToolStripMenuItem";
            this.workWithDiscountToolStripMenuItem.Size = new System.Drawing.Size(246, 26);
            this.workWithDiscountToolStripMenuItem.Text = "Работа с купонами";
            this.workWithDiscountToolStripMenuItem.Click += new System.EventHandler(this.workWithDiscountToolStripMenuItem_Click);
            // 
            // FinanceMenuItem
            // 
            this.FinanceMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TestFinanceDataMenuOtion,
            this.toolStripMenuItem2,
            this.TransactionMenuItem,
            this.TransactioCategoryMenuItem,
            this.WalletMenuOption,
            this.аналитикаToolStripMenuItem,
            this.сообщенияВБанкToolStripMenuItem});
            this.FinanceMenuItem.Name = "FinanceMenuItem";
            this.FinanceMenuItem.Size = new System.Drawing.Size(85, 24);
            this.FinanceMenuItem.Text = "Финансы";
            this.FinanceMenuItem.Visible = false;
            // 
            // TestFinanceDataMenuOtion
            // 
            this.TestFinanceDataMenuOtion.Name = "TestFinanceDataMenuOtion";
            this.TestFinanceDataMenuOtion.Size = new System.Drawing.Size(263, 26);
            this.TestFinanceDataMenuOtion.Text = "Создать тестовые данные";
            this.TestFinanceDataMenuOtion.Click += new System.EventHandler(this.TestFinanceDataMenuOtion_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(260, 6);
            // 
            // TransactionMenuItem
            // 
            this.TransactionMenuItem.Name = "TransactionMenuItem";
            this.TransactionMenuItem.Size = new System.Drawing.Size(263, 26);
            this.TransactionMenuItem.Text = "Платежи";
            this.TransactionMenuItem.Click += new System.EventHandler(this.TransactionMenuItem_Click);
            // 
            // TransactioCategoryMenuItem
            // 
            this.TransactioCategoryMenuItem.Name = "TransactioCategoryMenuItem";
            this.TransactioCategoryMenuItem.Size = new System.Drawing.Size(263, 26);
            this.TransactioCategoryMenuItem.Text = "Категории платежей";
            this.TransactioCategoryMenuItem.Click += new System.EventHandler(this.TransactioCategoryMenuItem_Click);
            // 
            // WalletMenuOption
            // 
            this.WalletMenuOption.Name = "WalletMenuOption";
            this.WalletMenuOption.Size = new System.Drawing.Size(263, 26);
            this.WalletMenuOption.Text = "Кошельки";
            this.WalletMenuOption.Click += new System.EventHandler(this.WalletMenuOption_Click);
            // 
            // аналитикаToolStripMenuItem
            // 
            this.аналитикаToolStripMenuItem.Name = "аналитикаToolStripMenuItem";
            this.аналитикаToolStripMenuItem.Size = new System.Drawing.Size(263, 26);
            this.аналитикаToolStripMenuItem.Text = "Аналитика";
            this.аналитикаToolStripMenuItem.Click += new System.EventHandler(this.аналитикаToolStripMenuItem_Click);
            // 
            // сообщенияВБанкToolStripMenuItem
            // 
            this.сообщенияВБанкToolStripMenuItem.Name = "сообщенияВБанкToolStripMenuItem";
            this.сообщенияВБанкToolStripMenuItem.Size = new System.Drawing.Size(263, 26);
            this.сообщенияВБанкToolStripMenuItem.Text = "Связь с банком";
            // 
            // gjvjomПомощьToolStripMenuItem
            // 
            this.gjvjomПомощьToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToAppToolStripMenuItem,
            this.aboutToolStripMenuItem,
            this.devToolStripMenuItem});
            this.gjvjomПомощьToolStripMenuItem.Name = "gjvjomПомощьToolStripMenuItem";
            this.gjvjomПомощьToolStripMenuItem.Size = new System.Drawing.Size(81, 24);
            this.gjvjomПомощьToolStripMenuItem.Tag = "18";
            this.gjvjomПомощьToolStripMenuItem.Text = "Помощь";
            this.gjvjomПомощьToolStripMenuItem.Visible = false;
            // 
            // helpToAppToolStripMenuItem
            // 
            this.helpToAppToolStripMenuItem.Name = "helpToAppToolStripMenuItem";
            this.helpToAppToolStripMenuItem.Size = new System.Drawing.Size(256, 26);
            this.helpToAppToolStripMenuItem.Tag = "181";
            this.helpToAppToolStripMenuItem.Text = "Помощь по программе";
            this.helpToAppToolStripMenuItem.Click += new System.EventHandler(this.HelpToAppToolStripMenuItemClick);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(256, 26);
            this.aboutToolStripMenuItem.Tag = "182";
            this.aboutToolStripMenuItem.Text = "О программе";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItemClick);
            // 
            // devToolStripMenuItem
            // 
            this.devToolStripMenuItem.Name = "devToolStripMenuItem";
            this.devToolStripMenuItem.Size = new System.Drawing.Size(256, 26);
            this.devToolStripMenuItem.Tag = "183";
            this.devToolStripMenuItem.Text = "Связь с разработчиками";
            this.devToolStripMenuItem.Click += new System.EventHandler(this.DevToolStripMenuItemClick);
            // 
            // StatsMenuButton
            // 
            this.StatsMenuButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OrdersByUserMenuItem,
            this.clientsToolStripMenuItem,
            this.goneClientForPeriodToolStripMenuItem,
            this.ReportNewClientsForPeriodToolStripMenuItem,
            this.simpleSaleReportToolStripMenuItem,
            this.dishCompatibilityToolStripMenuItem,
            this.ToolStripMenuItemDeliveryTime});
            this.StatsMenuButton.Name = "StatsMenuButton";
            this.StatsMenuButton.Size = new System.Drawing.Size(189, 24);
            this.StatsMenuButton.Text = "Статистика и отчётность";
            this.StatsMenuButton.Visible = false;
            // 
            // OrdersByUserMenuItem
            // 
            this.OrdersByUserMenuItem.Name = "OrdersByUserMenuItem";
            this.OrdersByUserMenuItem.Size = new System.Drawing.Size(361, 26);
            this.OrdersByUserMenuItem.Text = "Заказы по пользователям";
            this.OrdersByUserMenuItem.Click += new System.EventHandler(this.OrdersByUserMenuItem_Click);
            // 
            // clientsToolStripMenuItem
            // 
            this.clientsToolStripMenuItem.Name = "clientsToolStripMenuItem";
            this.clientsToolStripMenuItem.Size = new System.Drawing.Size(361, 26);
            this.clientsToolStripMenuItem.Text = "Клиенты";
            this.clientsToolStripMenuItem.Click += new System.EventHandler(this.clientsToolStripMenuItem_Click);
            // 
            // goneClientForPeriodToolStripMenuItem
            // 
            this.goneClientForPeriodToolStripMenuItem.Name = "goneClientForPeriodToolStripMenuItem";
            this.goneClientForPeriodToolStripMenuItem.Size = new System.Drawing.Size(361, 26);
            this.goneClientForPeriodToolStripMenuItem.Text = "Отчёт по ушедшим клиентам за период";
            this.goneClientForPeriodToolStripMenuItem.Click += new System.EventHandler(this.goneClientForPeriodToolStripMenuItem_Click);
            // 
            // ReportNewClientsForPeriodToolStripMenuItem
            // 
            this.ReportNewClientsForPeriodToolStripMenuItem.Name = "ReportNewClientsForPeriodToolStripMenuItem";
            this.ReportNewClientsForPeriodToolStripMenuItem.Size = new System.Drawing.Size(361, 26);
            this.ReportNewClientsForPeriodToolStripMenuItem.Text = "Отчёт по новым клиентам за период";
            this.ReportNewClientsForPeriodToolStripMenuItem.Click += new System.EventHandler(this.ReportNewClientsForPeriodToolStripMenuItem_Click);
            // 
            // simpleSaleReportToolStripMenuItem
            // 
            this.simpleSaleReportToolStripMenuItem.Name = "simpleSaleReportToolStripMenuItem";
            this.simpleSaleReportToolStripMenuItem.Size = new System.Drawing.Size(361, 26);
            this.simpleSaleReportToolStripMenuItem.Text = "Простой отчёт по продажам";
            this.simpleSaleReportToolStripMenuItem.Click += new System.EventHandler(this.simpleSaleReportToolStripMenuItem_Click);
            // 
            // dishCompatibilityToolStripMenuItem
            // 
            this.dishCompatibilityToolStripMenuItem.Name = "dishCompatibilityToolStripMenuItem";
            this.dishCompatibilityToolStripMenuItem.Size = new System.Drawing.Size(361, 26);
            this.dishCompatibilityToolStripMenuItem.Text = "Совместимость блюд";
            this.dishCompatibilityToolStripMenuItem.Click += new System.EventHandler(this.dishCompatibilityToolStripMenuItem_Click);
            // 
            // ToolStripMenuItemDeliveryTime
            // 
            this.ToolStripMenuItemDeliveryTime.Name = "ToolStripMenuItemDeliveryTime";
            this.ToolStripMenuItemDeliveryTime.Size = new System.Drawing.Size(361, 26);
            this.ToolStripMenuItemDeliveryTime.Text = "Время доставки";
            this.ToolStripMenuItemDeliveryTime.Click += new System.EventHandler(this.ToolStripMenuItemDeliveryTime_Click);
            // 
            // navBarItem1
            // 
            this.navBarItem1.Name = "navBarItem1";
            // 
            // navBarItem2
            // 
            this.navBarItem2.Name = "navBarItem2";
            // 
            // mainTStrip
            // 
            this.mainTStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.mainTStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonLogin,
            this.buttonSession,
            this.buttonSessionOptions,
            this.buttonPerformanceCounter,
            this.toolStripButton3,
            this.toolStripButton4});
            this.mainTStrip.Location = new System.Drawing.Point(0, 28);
            this.mainTStrip.Name = "mainTStrip";
            this.mainTStrip.Size = new System.Drawing.Size(1161, 39);
            this.mainTStrip.TabIndex = 2;
            this.mainTStrip.Text = "toolStrip1";
            // 
            // buttonLogin
            // 
            this.buttonLogin.Image = global::LaRenzo.Properties.Resources.offline;
            this.buttonLogin.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(158, 36);
            this.buttonLogin.Text = "Не авторизован";
            this.buttonLogin.Click += new System.EventHandler(this.UserLoginMenuItem_Click);
            // 
            // buttonSession
            // 
            this.buttonSession.Image = global::LaRenzo.Properties.Resources.stock_lock_brocken;
            this.buttonSession.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonSession.Name = "buttonSession";
            this.buttonSession.Size = new System.Drawing.Size(153, 36);
            this.buttonSession.Text = "Сессия закрыта";
            this.buttonSession.Click += new System.EventHandler(this.SessionMenuItemClick);
            // 
            // buttonSessionOptions
            // 
            this.buttonSessionOptions.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonSessionOptions.Image = global::LaRenzo.Properties.Resources.advancedsettings_5813;
            this.buttonSessionOptions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonSessionOptions.Name = "buttonSessionOptions";
            this.buttonSessionOptions.Size = new System.Drawing.Size(36, 36);
            this.buttonSessionOptions.Tag = "21";
            this.buttonSessionOptions.Text = "Настройки сессии";
            // 
            // buttonPerformanceCounter
            // 
            this.buttonPerformanceCounter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonPerformanceCounter.Image = global::LaRenzo.Properties.Resources.small_stopwatch;
            this.buttonPerformanceCounter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonPerformanceCounter.Name = "buttonPerformanceCounter";
            this.buttonPerformanceCounter.Size = new System.Drawing.Size(36, 36);
            this.buttonPerformanceCounter.Tag = "23";
            this.buttonPerformanceCounter.Text = "Монитор производительности";
            this.buttonPerformanceCounter.Click += new System.EventHandler(this.buttonPerformanceCounter_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::LaRenzo.Properties.Resources.print_printer;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton3.Tag = "24";
            this.toolStripButton3.Text = "Монитор печати";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.Image = global::LaRenzo.Properties.Resources.print_monitor;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(213, 36);
            this.toolStripButton4.Tag = "25";
            this.toolStripButton4.Text = "Диагностика принтеров";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click_1);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(57, 6);
            // 
            // AutoUpdater
            // 
            this.AutoUpdater.Enabled = true;
            this.AutoUpdater.Interval = 45000;
            this.AutoUpdater.Tick += new System.EventHandler(this.AutoUpdater_Tick);
            // 
            // lblTabsMessage
            // 
            this.lblTabsMessage.AutoSize = true;
            this.lblTabsMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTabsMessage.Location = new System.Drawing.Point(185, 295);
            this.lblTabsMessage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTabsMessage.Name = "lblTabsMessage";
            this.lblTabsMessage.Size = new System.Drawing.Size(0, 31);
            this.lblTabsMessage.TabIndex = 6;
            // 
            // mainTb
            // 
            this.mainTb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTb.Location = new System.Drawing.Point(0, 67);
            this.mainTb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mainTb.Name = "mainTb";
            this.mainTb.SelectedIndex = 0;
            this.mainTb.Size = new System.Drawing.Size(1161, 520);
            this.mainTb.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.mainTb.TabIndex = 7;
            this.mainTb.Tag = "";
            this.mainTb.Selected += new System.Windows.Forms.TabControlEventHandler(this.mainTb_Selected);
            // 
            // callNotifyIcon
            // 
            this.callNotifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("callNotifyIcon.Icon")));
            this.callNotifyIcon.Text = "notifyIcon1";
            this.callNotifyIcon.Visible = true;
            // 
            // failureOrdersReportToolStripMenuItem
            // 
            this.failureOrdersReportToolStripMenuItem.Name = "failureOrdersReportToolStripMenuItem";
            this.failureOrdersReportToolStripMenuItem.Size = new System.Drawing.Size(314, 26);
            this.failureOrdersReportToolStripMenuItem.Text = "Отчет по отказам";
            this.failureOrdersReportToolStripMenuItem.Click += new System.EventHandler(this.failureOrdersReportToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1161, 587);
            this.Controls.Add(this.mainTb);
            this.Controls.Add(this.mainTStrip);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.lblTabsMessage);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "MainForm";
            this.Text = "Zen Pizza";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainFormKeyUp);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.mainTStrip.ResumeLayout(false);
            this.mainTStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.linqServerModeSourceSession)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem FileMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExitMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OptionMenuItem;
        private DevExpress.XtraNavBar.NavBarItem navBarItem1;
        private DevExpress.XtraNavBar.NavBarItem navBarItem2;
        private System.Windows.Forms.ToolStrip mainTStrip;
        public System.Windows.Forms.ToolStripButton buttonLogin;
        public DevExpress.Data.Linq.LinqServerModeSource linqServerModeSourceSession;
        private System.Windows.Forms.Timer AutoUpdater;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        public System.Windows.Forms.ToolStripMenuItem HistoryMenuItem;
        public System.Windows.Forms.ToolStripMenuItem ReportMenuItem;
        public System.Windows.Forms.ToolStripMenuItem SessionsMenuItem;
        public System.Windows.Forms.ToolStripMenuItem SaleReportMenuItem;
		public System.Windows.Forms.ToolStripMenuItem DayReportMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DocumentsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ParametrOptionsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TimeManagerMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TimeTableMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SupplyMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PrintersMenuItem;
        private System.Windows.Forms.Timer timerWebOrders;
        private System.Windows.Forms.Timer timerBeepForOrders;
        private System.Windows.Forms.ToolStripButton buttonSessionOptions;
        private System.Windows.Forms.Timer timerPreOrder;
        private System.Windows.Forms.ToolStripMenuItem DeliveryScheduleMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CatalogsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SupplyDocsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PartnerMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ClassifierMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MeasureMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AddressMenuItem;
        private System.Windows.Forms.ToolStripMenuItem WarehouseMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PeopleMenuItem;
        private System.Windows.Forms.ToolStripMenuItem UsersMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DriversMenuItem;
        public System.Windows.Forms.ToolStripMenuItem DishesMenuItem;
        public System.Windows.Forms.ToolStripMenuItem CategoriesMenuItem;
        public System.Windows.Forms.ToolStripMenuItem DishMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SectionsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem FactorsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ClientMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DiscountMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ProductsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ProductMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ProductCategoriesMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OtherSetsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OtherSetCoeffMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OtherSetRuleMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HandoutsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HandoutMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HandoutsRuleMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReturnDocMenuItem;
        private System.Windows.Forms.ToolStripMenuItem GoodMovingMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MovingDocMenuItem;
        private System.Windows.Forms.ToolStripMenuItem WriteOffDocMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PostingDocMenuItem;
        private System.Windows.Forms.ToolStripMenuItem InventoryMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RegulationsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem СashСhangeMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RemainsOfGoodReport;
        private System.Windows.Forms.ToolStripMenuItem SettlsWithPartnerReport;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem WriteoffDishMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DataTransferMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DataTransferChiefMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TransferOfGoodMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TablesMenuItem;
        private System.Windows.Forms.ToolStripMenuItem продажиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CafeMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DeliveryMenuItem;
        public System.Windows.Forms.ToolStripButton buttonSession;
        private System.Windows.Forms.ToolStripMenuItem AllSessionCafeMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ProductOfPartnersMenuItem;
		private System.Windows.Forms.ToolStripMenuItem blackListMenuItem;
        private System.Windows.Forms.ToolStripButton buttonPerformanceCounter;
        private System.Windows.Forms.ToolStripMenuItem комбинацииБлюдToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem праваДоступаToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.Label lblTabsMessage;
        private System.Windows.Forms.TabControl mainTb;
        private System.Windows.Forms.ToolStripMenuItem FinanceMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TransactionMenuItem;
        private System.Windows.Forms.ToolStripMenuItem WalletMenuOption;
        private System.Windows.Forms.ToolStripMenuItem аналитикаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TransactioCategoryMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TestFinanceDataMenuOtion;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem сообщенияВБанкToolStripMenuItem;
		private System.Windows.Forms.ToolStripButton toolStripButton4;
		private System.Windows.Forms.ToolStripMenuItem gjvjomПомощьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem helpToAppToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem devToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
		private System.Windows.Forms.ToolStripMenuItem брендыToolStripMenuItem;
		private System.Windows.Forms.NotifyIcon callNotifyIcon;
        private System.Windows.Forms.ToolStripMenuItem StatsMenuButton;
        private System.Windows.Forms.ToolStripMenuItem OrdersByUserMenuItem;
        private System.Windows.Forms.ToolStripMenuItem FisPrinterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goneClientForPeriodToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReportNewClientsForPeriodToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem simpleSaleReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dishCompatibilityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PointOfSaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemDeliveryTime;
        private System.Windows.Forms.ToolStripMenuItem WarehouseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem workWithDiscountToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem failureOrdersReportToolStripMenuItem;
    }
}

