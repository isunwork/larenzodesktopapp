﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Media;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LaRenzo.DataRepository.Entities.Catalogs.Drivers;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Entities.PrintWorksModels;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.DataRepository.Entities.States;
using LaRenzo.DataRepository.Repositories.Call;
using LaRenzo.DataRepository.Repositories.Catalogs.BlackList;
using LaRenzo.DataRepository.Repositories.Catalogs.Printers;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Documents.Orders;
using LaRenzo.DataRepository.Repositories.Documents.WebOrders;
using LaRenzo.DataRepository.Repositories.Finance;
using LaRenzo.DataRepository.Repositories.Others.GlobalOptions;
using LaRenzo.DataRepository.Repositories.Others.Times;
using LaRenzo.Forms.BlackList;
using LaRenzo.Forms.Call;
using LaRenzo.Forms.Documents.DeliveryOrderForms;
using LaRenzo.Forms.Documents.WebOrderForms;
using LaRenzo.Forms.PreOrderForms;
using LaRenzo.Forms.StaffForms;
using LaRenzo.FormsProvider;
using LaRenzo.Properties;
using LaRenzo.Tools;
using LaRenzo.TransferLibrary;
using NLog;
using Content = LaRenzo.DataRepository.Repositories.Content;

namespace LaRenzo.Forms.MainFormAndIM.Controls
{
    public partial class ListByBrandControl : UserControl
    {
        private readonly Brand _brand;
        private readonly List<AccessRightInfo> _rights;

        public List<ToolStripItem> MenuItemSession { get; set; }

        public void PrepareMainForm()
        {
            MenuItemSession = GetMenuItemsSession();
        }


        private List<ToolStripItem> GetMenuItemsSession()
        {
            OrderState[] states = Content.StateManager.GetList().ToArray();

            var menuItemsSession = new List<ToolStripItem>();

            //Content.ThreadLocalStorage.UserRights = Content.UserAccessManager.GetForUser(Content.UserManager.UserLogged.ID);


            if (Content.UserAccessManager.GroupContainsRight(Content.ThreadLocalStorage.UserRights, AccessRightAlias.EditOrderStatus))
            {
                foreach (var orderState in states)
                {
                    var item = new ToolStripMenuItem(orderState.Name);
                    if (orderState.Name.Equals(Resources.Failure))
                    {
                        var subItem = new ToolStripMenuItem(Resources.FailureOurFaultReason);
                        item.DropDownItems.Add(subItem);
                        subItem.Click += async (s, e) => await contextGridMenuSession_State_Click(s);

                        subItem = new ToolStripMenuItem(Resources.FailureCustomerFaultReason);
                        item.DropDownItems.Add(subItem);
                        subItem.Click += async (s, e) => await contextGridMenuSession_State_Click(s);
                    }
                    else
                    {
                        item.Click += async (s, e) => await contextGridMenuSession_State_Click(s);
                    }
                    
                    menuItemsSession.Add(item);
                }

                menuItemsSession.Add(new ToolStripSeparator());
            }
            
            // Элемент меню "Сменить способ оплаты"
            var changePaymentItem = new ToolStripMenuItem("Изменить способ оплаты");

            var changePaymentSubItem = new ToolStripMenuItem("Наличные");
            changePaymentItem.DropDownItems.Add(changePaymentSubItem);
            changePaymentSubItem.Click += async (s, e) => await contextGridMenuSession_ChangePayment_Click(PaymentMethod.Cash);

            changePaymentSubItem = new ToolStripMenuItem("Онлайн карта");
            changePaymentItem.DropDownItems.Add(changePaymentSubItem);
            changePaymentSubItem.Click += async (s, e) => await contextGridMenuSession_ChangePayment_Click(PaymentMethod.OnlineCard);

            changePaymentSubItem = new ToolStripMenuItem("Офлайн карта");
            changePaymentItem.DropDownItems.Add(changePaymentSubItem);
            changePaymentSubItem.Click += async (s, e) => await contextGridMenuSession_ChangePayment_Click(PaymentMethod.OfflineCard);
            menuItemsSession.Add(changePaymentItem);

            // Элемент меню "Напечатать чек"
            var printItem = new ToolStripMenuItem("Напечатать чек");
            printItem.Click += async(s,e) => await contextGridMenuSession_Print_Click();
            menuItemsSession.Add(printItem);

            if (Settings.Default.PrinterPort != 0 && !String.IsNullOrEmpty(Settings.Default.PrinterIp))
            {
                if (Content.UserAccessManager.GroupContainsRight(Content.ThreadLocalStorage.UserRights, AccessRightAlias.PrintCheckOneMoreTime))
                {
                    var printItemFis = new ToolStripMenuItem("Напечатать фискальный чек");
                    printItemFis.Click += async(s,e) => await contextGridMenuSession_PrintFis_Click();
                    menuItemsSession.Add(printItemFis);
                }
            }

            if (Content.UserAccessManager.GroupContainsRight(Content.ThreadLocalStorage.UserRights, AccessRightAlias.BlackList))
            {
                var blackListItem = new ToolStripMenuItem("Искать в черном списке");
                blackListItem.Click += contextGridMenuSession_BlackList_Click;
                menuItemsSession.Add(blackListItem);
            }
            if (Content.UserManager.IsAdmin())
            {
                var sendSms = new ToolStripMenuItem("Отправить смс");
                sendSms.Click += contextGridMenuSendSms_Click;
                menuItemsSession.Add(sendSms);
            }
            return menuItemsSession;
        }

        private void contextGridMenuSession_BlackList_Click(object sender, EventArgs e)
        {
            FindInBlackList();
        }

        private void FindInBlackList()
        {
            try
            {
                if (mainGridView.IsDataRow(mainGridView.FocusedRowHandle))
                {
                    var id = (int)mainGridView.GetRowCellValue(mainGridView.FocusedRowHandle, columnIdCol);

                    Order order = Content.OrderManager.GetById(id);

                    SingletonFormProvider<BlackListForm>.ActivateForm(context => new BlackListForm(order), null);
                }
                else
                {
                    MessageBox.Show(Resources.SelectRowToCheckBlackList);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async Task contextGridMenuSession_State_Click(object sender)
        {
            var logger = LogManager.GetCurrentClassLogger();
            var id = (int)mainGridView.GetRowCellValue(mainGridView.GetSelectedRows()[0], columnIdCol);

            var menuItem = (ToolStripMenuItem)sender;

            var newStateName = menuItem.Text == Resources.FailureOurFaultReason || menuItem.Text == Resources.FailureCustomerFaultReason 
                    ? Resources.Failure
                    : menuItem.Text;
            var newState = Content.StateManager.GetByName(newStateName);

            Order order = Content.OrderManager.GetById(id);

            logger.Info($"Change order's state, orderID:{order.ID}, webOrderId:{order.WebOrderId}, state: {order.State.ID}, newState: {newState.ID}, {DateTime.Now}");
            if (order.State.ID != newState.ID)
            {
                if (newState.Name == Resources.Failure && order.Source == "vlru" && !Content.UserManager.IsAdmin())
                {
                    MessageBox.Show("Заказ Vl.ru может отменить только администратор.");
                    return;
                }
                else
                {
                    var oldState = order.State;

                    if (newState.Name == Resources.Failure)
                    {
                        order.FailureReason = menuItem.Text;
                        order.ChangeToFailure = DateTime.Now;
                        if (order.Source == "vlru")
                        {
                            if (order.WebOrderId != 0)
                            {
                                var client = new HttpClient();

                                logger.Info($"Get the url to cancel the order on the site vl.ru, WebOrderId: {order.WebOrderId}, IdBrand:{order.IdBrand}, {DateTime.Now}");
                                var url = Content.GlobalOptionsManager.GetVlCancelUrl(order.IdBrand, order.WebOrderId);
                                if (!String.IsNullOrEmpty(url))
                                {
                                    logger.Info($"Sending a request to cancel the order on the site vl.ru: {url}, {DateTime.Now}");
                                    var responseString = await client.DeleteAsync(url);
                                    if (responseString.IsSuccessStatusCode)
                                    {
                                        logger.Info($"Order canceled successfully on vl.ru: {url}, {DateTime.Now}");
                                        MessageBox.Show("Заказ успешно отменён на Vl.ru");
                                    }
                                    else
                                    {
                                        logger.Info($"Error canceling an order on vl.ru: {url}, {DateTime.Now}");
                                        MessageBox.Show("Ошибка отмены заказа на Vl.ru");
                                        return;
                                    }
                                }
                                else
                                {
                                    logger.Info($"Error receiving url to cancel the order on the site vl.ru, WebOrderId: {order.WebOrderId}, IdBrand:{order.IdBrand}, {DateTime.Now}");
                                    MessageBox.Show("Ошибка отмены заказа на Vl.ru");
                                    return;
                                }
                            }
                            else
                            {
                                logger.Info($"Error canceling the order on the site vl.ru, WebOrderId: {order.WebOrderId}, {DateTime.Now}");
                                MessageBox.Show("Ошибка отмены заказа на Vl.ru");
                                return;
                            }
                        }

                        if (MessageBox.Show(Resources.DeleteReservOnCancelQuestion, Resources.Failure,
                                MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            Content.TimeSheetManager.ClearReservation(order.ReservationGuid);
                        }
                        else return;
                    }

                    if (newState.Name == "В пути")
                    {
                        var driverForm = new DriverSelectionForm(order.ID);
                        driverForm.Show();
                        driverForm.DriverSelected += SetNewDriverToOrder;
                        order.ChangeToOnWay = DateTime.Now;
                    }

                    if (oldState.BankInteractionMode != newState.BankInteractionMode && order.WebOrderGuid != null &&
                        order.PaymentMethod == PaymentMethod.OnlineCard)
                    {
                        Content.BankManager.AddBankInteraction(order.WebOrderGuid.Value, newState.BankInteractionMode,
                            string.Format("Изменен статус заказа {0}. {1} -> {2}. Пользователь - {3}", order.ID,
                                oldState.Name, newState.Name, Content.UserManager.UserLogged.Name));
                    }

                    if (newState.Name == "Оплачен")
                    {
                        if (!order.FiscalCheckPrinted && (order.PaymentMethod == PaymentMethod.OfflineCard))
                        {
                            await GetOrderAndPrintFis();
                        }
                        order.ChangeToPaid = DateTime.Now;
                    }
                    order.StateID = newState.ID;
                    order.State = null;
                    Content.OrderManager.Update(order);
                }
            }
            RefreshGridData();
            if (mainGridView.GetSelectedRows().Count() > 0)
            {
                mainGridView.SetRowCellValue(mainGridView.GetSelectedRows()[0], columnState, menuItem.Text);
            }

            mainGridView.LayoutChanged();
        }

        private void SetNewDriverToOrder(int orderid, Driver driver)
        {
            Content.OrderManager.SetNewDriver(orderid, driver);
            RefreshGridData();
        }

        private async Task contextGridMenuSession_ChangePayment_Click(PaymentMethod paymentMethod)
        {
            await ChangePaymentType(paymentMethod);
            RefreshGridData();
        }

        private async Task contextGridMenuSession_Print_Click()
        {
            await GetOrderAndPrint();
            RefreshGridData();
        }

        private async Task contextGridMenuSession_PrintFis_Click()
        {
            await GetOrderAndPrintFis();
            RefreshGridData();
        }

        private void contextGridMenuSendSms_Click(object sender, EventArgs e)
        {
            if (mainGridView.IsDataRow(mainGridView.FocusedRowHandle))
            {
                var id = (int) mainGridView.GetRowCellValue(mainGridView.FocusedRowHandle, columnIdCol);

                Order order = Content.OrderManager.GetById(id);
                if (!order.SmsSended)
                {
                    SendSms(order);
                }
                else
                {
                    if (MessageBox.Show("Этот человек уже получал смс, вы уверены что хотите отправить ещё?",
                            "Внимание вторая отправка", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        SendSms(order);
                    }
                }
            }
        }

        private static void SendSms(Order order)
        {
            var smsText = Settings.Default.SmsText;
            if (MessageBox.Show(
                    $"Вы хотите отправить смс со следующим текстом? \"{smsText}\"",
                    "Отправить смс", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var sms = new Smsc();
                sms.send_sms("+7"+order.Phone,
                    smsText);
                order.SmsSended = true;
                Content.OrderManager.ChangeSmsSendedStatus(order.ID, true);
            }
        }

        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Получить и напечатать заказ </summary>
        ////================================================
        private async Task GetOrderAndPrintFis()
        {
            try
            {
                if (mainGridView.IsDataRow(mainGridView.FocusedRowHandle))
                {
                    // Получить Id заказа из тычка по гриду
                    var orderId = (int)mainGridView.GetRowCellValue(mainGridView.FocusedRowHandle, columnIdCol);

                    // Получить заказ по id заказа
                    Order selectedToPrintOrder = Content.OrderManager.GetById(orderId);
                    int deliveryPrice = await Content.OrderManager.GetDeliveryPrice(selectedToPrintOrder.OrderItems,
                        selectedToPrintOrder.Brand.ID, selectedToPrintOrder.DiscountCard, selectedToPrintOrder.Coupon);
                    // Печать
                    if (Content.UserManager.UserLogged != null)
                    {
                        // Проверка на печать борлее 1 чека оператором
                        if (!Content.UserAccessManager.GroupContainsRight(Content.ThreadLocalStorage.UserRights, AccessRightAlias.PrintCheckOneMoreTime) && selectedToPrintOrder.FiscalCheckPrinted)
                        {
                            MessageBox.Show(Resources.YouCantPrintMoreThenOneCheck);
                        }
                        else
                        {
                            PrintingManagerElves.PrintToNalogPrinter(selectedToPrintOrder, Settings.Default.PrinterPort,
                                Settings.Default.PrinterIp, deliveryPrice);
                        }
                    }
                }
                else
                {
                    MessageBox.Show(Resources.SelectStringToPrint);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Изменить способ оплаты </summary>
        ////================================================
        private async Task ChangePaymentType(PaymentMethod paymentMethod)
        {
            try
            {
                if (mainGridView.IsDataRow(mainGridView.FocusedRowHandle))
                {
                    // Получить Id заказа из тычка по гриду
                    var orderId = (int)mainGridView.GetRowCellValue(mainGridView.FocusedRowHandle, columnIdCol);
                    Content.OrderManager.ChangePaymentType(orderId, paymentMethod);
                }
                else
                {
                    MessageBox.Show(Resources.SelectStringToPrint);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Получить и напечатать заказ </summary>
        ////================================================
        private async Task GetOrderAndPrint()
        {
            try
            {
                if (mainGridView.IsDataRow(mainGridView.FocusedRowHandle))
                {
                    // Получить Id заказа из тычка по гриду
                    var orderId = (int)mainGridView.GetRowCellValue(mainGridView.FocusedRowHandle, columnIdCol);

                    // Получить заказ по id заказа
                    Order selectedToPrintOrder = Content.OrderManager.GetById(orderId);

                    // Печать
                    await PrintByOrder(selectedToPrintOrder);
                }
                else
                {
                    MessageBox.Show(Resources.SelectStringToPrint);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Напечатать по заказу </summary>
        ////=========================================
        private async Task PrintByOrder(Order orderToPrint)
        {
            // Если юзер залогинен
            if (Content.UserManager.UserLogged != null)
            {
                // Проверка на печать борлее 1 чека оператором
                if (!Content.UserAccessManager.GroupContainsRight(Content.ThreadLocalStorage.UserRights, AccessRightAlias.PrintCheckOneMoreTime) && orderToPrint.DoPrintedByOperator)
                {
                    MessageBox.Show(Resources.YouCantPrintMoreThenOneCheck);
                }
                else
                {
                    // Создать новою задачу на печать и получить её Id
                    int workId = Content.PrintWorksRepository.AddPrintWork(new PrintWork
                    {
                        Created = DateTime.Now,
                        OrderId = orderToPrint.ID,
                        State = (int)State.Processing
                    });

                    // Напечатать или вернуть ошибку печати
                    string errorPrintMessage = await PrintingManager.Print(orderToPrint, Settings.Default.PrinterPort, Settings.Default.PrinterIp, workId);

                    // Если ошибка не null (существует)
                    if (!string.IsNullOrEmpty(errorPrintMessage))
                    {
                        MessageBox.Show(errorPrintMessage);

                        // Устновить состояние задачи печати, как ОШИБКА
                        Content.PrintWorksRepository.UpdatePrintWorkState(workId, State.Error);
                    }
                    else
                    {
                        // Устновить состояние задачи печати, как ВЫПОЛНЕННОЕ
                        Content.PrintWorksRepository.UpdatePrintWorkState(workId, State.Compleated);
                    }

                    // Если не было флага "Напечатан" - установить
                    if (!orderToPrint.DoPrintedByOperator)
                    {
                        orderToPrint.DoPrintedByOperator = true;
                        Content.OrderManager.Update(orderToPrint);
                        RefreshGridData();
                    }
                }
            }
        }

        public ListByBrandControl()
        {
            InitializeComponent();
        }

        public ListByBrandControl(Brand brandLocal, List<AccessRightInfo> rightsLocal)
        {
            InitializeComponent();

            MenuItemSession = GetMenuItemsSession();

            _brand = brandLocal;
            _rights = rightsLocal;

            mainGridView.OptionsView.ShowFooter = Content.UserAccessManager.GroupContainsRight(_rights, AccessRightAlias.ViewDeliveryOrdersSumCount);

            SetControls(true);

            buttonAddOrder.Click += async (s, e) => await buttonAddOrder_Click();
            toolStripButton7.Click += (s, e) =>  toolStripButton7_Click_1();
            timerBeepForOrders.Tick += async (s, e) => await timerBeepForOrders_Tick();
            mainGridView.KeyDown += async (s, e) => await gridView2_KeyDown(s, e);
            mainGridView.DoubleClick += async (s, e) => await mainGridView_DoubleClick(s);
            mainGridView.ActiveFilter.Add(mainGridView.Columns[1], new ColumnFilterInfo($"[Brand.Name] = '{_brand.Name}'"));
        }

        public async void Initialize()
        {
            if (_brand.ID != 2)
            {
                await DoWorkAsyncInfiniteLoop();
            }
        }

        private async Task DoWorkAsyncInfiniteLoop()
        {
            while (true)
            {
                int newOrderCount = Content.WebOrderManager.GetUnhandledWebOrdersCount();
                ChangeWebOrderStatus(newOrderCount);
                await Task.Delay(20000);
            }
            // ReSharper disable once FunctionNeverReturns
        }



        public void SetControls(bool isSessionOpened)
        {

            if (isSessionOpened)
            {
                buttonAddOrder.Enabled = true;

                RefreshGridData();
            }
            else
            {
                buttonAddOrder.Enabled = false;

            }
        }


        //________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Обновить данные грида </summary>
        ////==========================================
        public async void RefreshGridData()
        {
            var first = mainGridView.TopRowIndex;
            var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);
            await BindGridData();
            mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
            mainGridView.MakeRowVisible(first);
        }


        //________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Обновляет список заказов в главном окне формы </summary>
        ////==================================================================
        public async Task BindGridData()
        {
            if (Content.UserManager.UserLogged != null)
            {
                var logKey = ExecutionTimeLogger.Start();

                var orders = await Task.Run(() => Content.OrderManager.GetCurrentSessionList());

                var afterKitchenCount = 0;
                foreach (Order order in orders)
                {
                    if (order.State.Name == "Принят" && order.TimeKitchen <= DateTime.Now)
                    {
                        afterKitchenCount++;
                    }
                }
                mainGridControl.DataSource = orders;


                waitOrdersCountLabel.Text = "Ожидают доставки: " + afterKitchenCount;

                ExecutionTimeLogger.Stop(logKey, "Получение списка заказов из база дынных для главного окна");
            }
        }


        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> При отрисовки Row </summary>
        ////======================================
        private void GridView2RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view != null && e.RowHandle >= 0)
            {
                // Получить заказ из Row
                var order = view.GetRow(e.RowHandle) as Order;

                if (order != null)
                {
                    // Цвет для строки заказа
                    var rowColor = new Color();

                    // Проверка телефона на чёрный лист
                    if (Content.BlackListManager.IsNumberBlack(order.Phone))
                    {
                        e.Appearance.BackColor2 = Color.DarkGray;
                    }
                    // Проверка адреса на чёрный лист
                    else if (Content.BlackListManager.IsAddressBlack(order.Street, order.House, order.Appartament))
                    {
                        e.Appearance.BackColor2 = Color.DarkGray;
                    }

                    // Присвоить цвет в зависимости от состояния заказа
                    switch (order.State.Name)
                    {
                        case "Принят":
                            rowColor = Color.Salmon;
                            break;
                        case "В пути":
                            rowColor = Color.Orange;
                            break;
                        case "Доставлен":
                            rowColor = Color.Yellow;
                            break;
                        case "Оплачен":
                            rowColor = Color.LightGreen;
                            break;
                    }

                    // Если оплата через OnlineCard
                    if (order.PaymentMethod == PaymentMethod.OnlineCard)
                    {
                        e.Appearance.GradientMode = LinearGradientMode.Horizontal;
                        e.Appearance.BackColor2 = Color.LightGreen;
                    }

                    // Время на накладные расходы кухни
                    int addCitchenTime = Content.GlobalOptionsManager.GetOverheadTime(_brand.ID);

                    if (order.State.Name == "Принят" && order.TimeKitchen.AddMinutes(addCitchenTime) <= DateTime.Now)
                    {
                        rowColor = Color.Aqua;
                    }

                    // Установить цвет
                    e.Appearance.BackColor = rowColor;
                }

                if (e.RowHandle == view.FocusedRowHandle)
                {
                    e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
                }
            }
        }

        private async Task buttonAddOrder_Click()
        {
            var dof = new DeliveryOrderForm
            {
                CurentBrand = _brand
            };
            await dof.Initialize();
            dof.Show();
        }

        private async Task ShowDetails(int rowHandle)
        {
            if (mainGridView != null)
            {
                mainGridView.GetRowCellValue(rowHandle, columnIdCol);

                if (Content.UserManager.UserLogged != null)
                {
                    var orderInfo = mainGridView.GetRow(rowHandle) as Order;

                    if (orderInfo == null) return;

                    var mode = DetailsFormModes.ViewProcessed;

                    if (!orderInfo.State.DoSessionCanBeClosed)
                    {
                        mode = Content.UserAccessManager.GroupContainsRight(_rights, AccessRightAlias.EditOrder) ? DetailsFormModes.EditOrder : DetailsFormModes.ViewByOperator;
                    }

                    DeliveryOrderFormOptions formOptions = null;

                    if (orderInfo.PaymentMethod == PaymentMethod.OnlineCard)
                    {
                        formOptions = new DeliveryOrderFormOptions
                        {
                            SaveButtonText = "Заказы, оплаченные онлайн, редактировать запрещено",
                            DoBlockSaveButton = true,
                            DoBlockDishArea = true,
                            DoBlockReservationArea = true
                        };
                    }

                    await EditDetails(orderInfo.ID, mode, formOptions);
                }
            }
        }


        public async Task EditDetails(int id, DetailsFormModes state = DetailsFormModes.EditOrder, DeliveryOrderFormOptions formOptions = null)
        {
            try
            {
                var form = new DeliveryOrderForm(state, id, formOptions)
                {
                    CurentBrand = _brand
                };
                await form.Initialize();
                form.Show();
                form.Activate();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private async Task mainGridView_DoubleClick(object sender)
        {
            var view = (GridView)sender;

            Point pt = view.GridControl.PointToClient(MousePosition);

            GridHitInfo info = view.CalcHitInfo(pt);
            if (view.IsDataRow(info.RowHandle))
            {
                await ShowDetails(info.RowHandle);
            }
        }


        private void buttonPreOrder_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<PreOrdersForm>.ActivateForm(() => new PreOrdersForm());
        }

        private void buttonWebOrders_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<WebOrderForm>.ActivateForm(() => new WebOrderForm(_brand.ID));
        }
        
        private void ChangeWebOrderStatus(int ordersAmount)
        {
            if (ordersAmount > 0)
            {
                buttonWebOrders.Image = Resources.exclamation_4426;
                buttonWebOrders.Text = string.Format("Есть необработанные заказы! ({0})", ordersAmount);

                TurnSound(true, ordersAmount);
            }
            else
            {
                buttonWebOrders.Image = Resources.badgealtexclamation_4268;
                buttonWebOrders.Text = Resources.NotOnlineOrder;
                TurnSound(false);
            }
        }

        private void TurnSound(bool @on, int speedFactor = 1)
        {
            timerBeepForOrders.Interval = 500 + 5000 / speedFactor;
            timerBeepForOrders.Enabled = @on;
        }

        private async Task timerBeepForOrders_Tick()
        {
            var missedCall = await CallManager.Instance.IsMissedCall();
            if (Application.OpenForms["WebOrderForm"] == null || missedCall && !File.Exists("NoSound.txt"))
            {
                new SoundPlayer(Resources.beep).Play();
            }

            if (missedCall)
            {
                toolStripButton7.Image = redPhonePanel.BackgroundImage;
            }
            else
            {
                toolStripButton7.Image = bluePhonePanel.BackgroundImage;
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            RefreshGridData();
        }

        private void searchButton_Click_1(object sender, EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }

        private void toolStripButton7_Click_1()
        {
            var manyCalls = new ManyCalls();
            manyCalls.Initialyze();
            SingletonFormProvider<ManyCalls>.ActivateForm(() => manyCalls);
        }

        private void mainGridView_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (sender is GridView view)
            {
                GridHitInfo hitInfo = view.CalcHitInfo(e.Point);

                if (view.IsDataRow(hitInfo.RowHandle))
                {
                    var menuForRow = new ContextMenuStrip();

                    // Основные пункты меню
                    MenuItemSession.ForEach(x => menuForRow.Items.Add(x));
                    // Разделитель
                    menuForRow.Items.Add(new ToolStripSeparator());

                    // Заказ, по которому кликнули в гриде

                    // Если заказ не "пустой"
                    if (mainGridView.GetRow(hitInfo.RowHandle) is Order order)
                    {
                        if (order.PaymentMethod == PaymentMethod.OnlineCard)
                        {
                            menuForRow.Items.Add("Оплачен через сайт", null, null);
                            menuForRow.Items[menuForRow.Items.Count - 1].Enabled = false;
                        }
                        else
                        {
                            if (order.PaymentMethod == PaymentMethod.OfflineCard)
                            {
                                menuForRow.Items.Add("Оплатят наличными", null, (o, args) => ChangePaymentMethodAndRefresh(order, PaymentMethod.Cash));
                            }
                            else
                            {
                                menuForRow.Items.Add("Оплатят картой курьеру", null, (o, args) => ChangePaymentMethodAndRefresh(order, PaymentMethod.OfflineCard));
                            }
                        }

                        menuForRow.Show(view.GridControl, e.Point);
                    }
                }

            }
        }

        private void ChangePaymentMethodAndRefresh(Order order, PaymentMethod method)
        {
            Content.ThreadLocalStorage.UserRights = Content.UserAccessManager.GetForUser(Content.UserManager.UserLogged.ID);
            if (Content.UserAccessManager.GroupContainsRight(Content.ThreadLocalStorage.UserRights, AccessRightAlias.EditOrderPaymentType))
            {
                Content.OrderManager.ChangePaymentType(order.ID, method);
                RefreshGridData();
            }     
            else
            {
                MessageBox.Show("У вас недостаточно прав.");
            }
        }

        private int _calculatedSumGroup;
        private int _calculatedSumTotal;

        private void mainGridView_CustomSummaryCalculate(object sender, CustomSummaryEventArgs e)
        {
            // Get the summary ID. 
            if (e.Item is GridSummaryItem gridSummaryItem)
            {
                int tag = Convert.ToInt32(gridSummaryItem.Tag);
                var view = sender as GridView;

                if (e.SummaryProcess == CustomSummaryProcess.Start)
                {
                    _calculatedSumGroup = 0;
                    _calculatedSumTotal = 0;
                }
                // Calculation 
                if (e.SummaryProcess == CustomSummaryProcess.Calculate)
                {
                    if (view != null)
                    {
                        var state = (string)(view.GetRowCellValue(e.RowHandle, columnState));
                        if (state != Resources.Failure)
                        {
                            switch (tag)
                            {
                                case 1:
                                    _calculatedSumGroup += Convert.ToInt32(e.FieldValue);
                                    break;
                                case 3:
                                    _calculatedSumTotal += Convert.ToInt32(e.FieldValue);
                                    break;

                            }
                        }
                    }
                }

                // Finalization 
                if (e.SummaryProcess == CustomSummaryProcess.Finalize)
                {
                    if (tag == 1) e.TotalValue = _calculatedSumGroup;
                    if (tag == 3) e.TotalValue = _calculatedSumTotal;
                }
            }

        }

        private void mainGridView_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.Column.Name == "columnBlackList" && e.IsGetData)
            {
                e.Value = false;
            }
        }

        private async Task gridView2_KeyDown(object sender, KeyEventArgs e)
        {
            var view = (GridView)sender;
            if (e.KeyCode == Keys.Enter && view.IsDataRow(view.FocusedRowHandle))
            {
                await ShowDetails(view.FocusedRowHandle);
            }
        }

        private void mainGridView_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            var order = mainGridView.GetRow(e.RowHandle) as Order;

            var isInBlackList = (bool)mainGridView.GetRowCellValue(e.RowHandle, columnBlackList);

            if (e.Column == columnName && order != null && isInBlackList)
            {
                e.Appearance.BackColor = Color.LightSlateGray;
                e.Appearance.BackColor2 = Color.White;
            }
            if (e.Column == gridColumnFisCheckButtons && order != null && order.IsOnlinePayment)
            {
                //e.Column = true;
            }
        }
    }
}
