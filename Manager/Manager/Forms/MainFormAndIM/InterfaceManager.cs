﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Users;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Entities.Documents.Sessions;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Documents.Sessions;
using LaRenzo.Forms.Documents.DeliveryOrderForms;
using LaRenzo.Forms.Documents.SessionForms;
using LaRenzo.Properties;
using System.Linq;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.Forms.MainFormAndIM.Controls;
using LaRenzo.Tools;
using Microsoft.Win32;
using System.IO;
using DataRepository.Entities;

namespace LaRenzo.Forms.MainFormAndIM
{

    public partial class MainForm
    {
        public List<ToolStripItem> MenuItemSession { get; set; }

        private async Task PrepareMainForm()
        {
            Session currentSession = await Content.SessionManager.GetOpened();

            if (currentSession != null) IsSessionOpened = true;

            SetSessionInfo(currentSession);
        }

        public void SetLoginInfo(string name)
        {
            buttonLogin.Text = name;
            buttonLogin.Image = Resources.online;
        }

        public void ClearLoginInfo()
        {
            buttonLogin.Text = Resources.MainFormEnter;
            buttonLogin.Image = Resources.offline;
        }

        public void SetSessionInfo(Session session)
        {
            if (session == null)
            {
                ClearSessionInfo();
                IsSessionOpened = false;
                return;
            }

            IsSessionOpened = true;
            buttonSession.Image = Resources.stock_lock_ok;
            buttonSession.Text = String.Format("{0:dd/MM/yyyy HH:mm}", session.Opened);
        }

        public void ClearSessionInfo()
        {
            buttonSession.Image = Resources.stock_lock_brocken;
            buttonSession.Text = Resources.SessionClose;
        }
        
        //________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Обновить данные грида </summary>
        ////==========================================
        public void RefreshGridData()
        {
            if (IsAuth)
            {

                foreach (TabPage tp in mainTb.TabPages)
                {
                    foreach (Control control in tp.Controls)
                    {
                        var d = control as ListByBrandControl;

                        if (d != null)
                        {
                            d.RefreshGridData();
                        }
                    }
                }


            }
        }


        //___________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Загрузить табы </summary>
        ////===================================
        private async Task LoadTabs(User user)
        {

            //Content.ThreadLocalStorage.UserRights = Content.UserAccessManager.GetForUser(user.ID);
            List<AccessRightInfo> rights = Content.ThreadLocalStorage.UserRights.OrderBy(x => x.Code).ToList();

            if (Content.UserAccessManager.GroupContainsRight(rights, AccessRightAlias.DelivaryTab))
            {
                await LoadBrandTabs(rights);
            }

            if (Content.UserAccessManager.GroupContainsRight(rights, AccessRightAlias.CafeTab))
            {
                await LoadCafeTab();
            }
            var selectedBrand = Content.BrandManager.GetList(new BrandFilter { BrandName = mainTb.SelectedTab.Text });
            if (selectedBrand != null && selectedBrand.Count > 0)
            {
                Content.BrandManager.SelectedBrand = selectedBrand.FirstOrDefault();
            }
        }


        //___________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Загрузить табы брэндов </summary>
        ////===========================================
        private async Task LoadBrandTabs(List<AccessRightInfo> rights)
        {
            List<Brand> brands = Content.BrandManager.GetList();

            foreach (Brand brand in brands)
            {
                if (!string.IsNullOrEmpty(brand.Name))
                {
                    ListByBrandControl listByBrandControl = new ListByBrandControl(brand, rights)
                    {
                        Dock = DockStyle.Fill
                    };
                    listByBrandControl.Initialize();
                    TabPage tp = new TabPage
                    {
                        Text = brand.Name,
                        Padding = new Padding(3),
                        Size = new Size(863, 366),
                        TabIndex = 0,
                        UseVisualStyleBackColor = true,
                        Visible = true
                    };

                    tp.Controls.Add(listByBrandControl);
                    mainTb.TabPages.Add(tp);
                }
            }
        }


        //___________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Загрузить таб кафе </summary>
        ////=======================================
        private async Task LoadCafeTab()
        {
            CaffeControl caffeControl = new CaffeControl();
            await caffeControl.SetControls();

            TabPage tp = new TabPage
            {
                Text = @"Кафе"
            };
            tp.Controls.Add(caffeControl);

            mainTb.TabPages.Add(tp);
        }


        public void UpdateInterface()
        {
            if (IsAuth)
            {
                buttonSession.Enabled = true;
                timerWebOrders.Enabled = true;
                timerBeepForOrders.Enabled = true;
                timerPreOrder.Enabled = true;
                StatsMenuButton.Visible = Content.UserManager.IsAdmin();
            }
            else
            {
                timerWebOrders.Enabled = false;
                timerBeepForOrders.Enabled = false;
                buttonSession.Enabled = false;
                StatsMenuButton.Visible = false;
            }
            CheckAccessRights(Content.UserManager.UserLogged);
        }

        private async Task ChangeIF_OpenSession()
        {
            var session = await Content.SessionManager.GetOpened();
            SetSessionInfo(session);
            IsSessionOpened = true;
            UpdateInterface();
        }

        //___________________________________________________________________________________________________________________________________
        /// <summary> Закрытие сессии </summary>
        ////====================================
        private void CloseSession()
        {
            var processForm = new SessionProcessForm();

            processForm.SessionClosed += (session, args) =>
            {
                IsSessionOpened = false;
                UpdateInterface();
                ClearSessionInfo();
            };

            processForm.SessionOpened += async (s, e) => await  ChangeIF_OpenSession();

            processForm.ShowDialog();
        }

        private async Task NewOrder()
        {
            if (Content.UserManager.UserLogged != null)
            {
                var deliveryOrderForm = new DeliveryOrderForm();
                await deliveryOrderForm.Initialize();
                deliveryOrderForm.Show();
            }
        }

        private void ProcessOrder(string state, Order order)
        {
            RefreshGridData();
        }

        /// <summary>
        /// Пользователь залогинился
        /// </summary>
        /// <param name="user"></param>
        private async Task ProcessUserLogged(User user)
        {
            IsAuth = true;
            await PrepareMainForm();
            SetLoginInfo(user.Name);

            UpdateInterface();

            await LoadTabs(Content.UserManager.UserLogged);

            // Инициализация звонков
            InitCall();

            // Проверка на запущенность CallCenter Infinity
            //TestCallCenterStart();

#if !DEBUG
            // Запуск программы для синхронизации с веб сайтом Ларензо
            LaunchWebOrderManager(user);
#endif
        }

        private void LaunchWebOrderManager(User user)
        {
            if (user.Role == null || user.Role.Name != "Admin")
                return;
            if (IsProcessOpen("LaRenzo.WebOrderProcessor"))
                return;
            string startMenuPath = Environment.GetFolderPath(Environment.SpecialFolder.StartMenu);
            string webOrderManagerPath = Path.Combine(startMenuPath, "Programs", "Larenzo", "WebContent.OrderManager.appref-ms");
            if (!File.Exists(webOrderManagerPath))
                return;
            Process proc = new Process();
            proc.StartInfo.FileName = webOrderManagerPath;
            proc.Start();
        }

        private bool IsProcessOpen(string name)
        {
            foreach (Process clsProcess in Process.GetProcesses())
            {
                if (clsProcess.ProcessName.Contains(name))
                {
                    return true;
                }
            }

            return false;
        }

        private void TestCallCenterStart()
        {
            Process[] proc = Process.GetProcesses();

            bool isStart = false;

            foreach (Process p in proc)
            {
                if (p.ProcessName == "CallCenter")
                {
                    isStart = true;
                    break;
                }
            }

            if (!isStart)
            {
                try
                {
                    Process.Start("CallCenter");
                }
                catch (Exception)
                {
                    //MessageBox.Show($"Не найдена программа CallCenter: {ex.Message}");
                }
            }
        }

        private void CheckAccessRights(User user)
        {
            Content.ThreadLocalStorage.UserRights = Content.UserAccessManager.GetForUser(user?.ID ?? 0);

            //Обработка элементов меню и подменю с внесенными кодами
            var toolBoxItems = GetMenuItems().Cast<ToolStripItem>().Where(x => x.Tag != null && !string.IsNullOrWhiteSpace((string)x.Tag)).ToList();
            toolBoxItems.AddRange(GetToolBarItems());

            var rights = Content.ThreadLocalStorage.UserRights.OrderBy(x => x.Code).ToList();

            foreach (var right in rights)
            {
                var control = toolBoxItems.FirstOrDefault(x => Convert.ToInt32(x.Tag) == right.Code);
                if (control != null)
                {
                    control.Visible = right.Allowed;
                }
            }

            //Обработка кнопки открытия закрытия сессии
            buttonSession.Visible = user != null;
            buttonSession.Enabled = Content.UserAccessManager.GroupContainsRight(rights, AccessRightAlias.SessionButton);

            //Всплывающие уведомление
            NotifyManager.UnsubsctibeFromInventoryChanges();
            if (Content.UserAccessManager.GroupContainsRight(rights, AccessRightAlias.Popups)) NotifyManager.SubscribeToInventoryChanges();
        }

        private IEnumerable<ToolStripItem> GetToolBarItems()
        {
            return Controls.OfType<ToolStrip>().SelectMany(x => x.Items.OfType<ToolStripItem>());
        }

        private IEnumerable<ToolStripMenuItem> GetMenuItems()
        {
            var allItems = new List<ToolStripMenuItem>();

            var items = Controls.OfType<MenuStrip>().SelectMany(x => x.Items.OfType<ToolStripMenuItem>()).ToList();

            foreach (var toolItem in items)
            {

                allItems.Add(toolItem);
                //add sub items
                allItems.AddRange(GetMenuItems(toolItem).ToList());
            }

            return allItems;
        }

        private IEnumerable<ToolStripMenuItem> GetMenuItems(ToolStripMenuItem item)
        {
            foreach (var dropDownItem in item.DropDownItems.OfType<ToolStripMenuItem>())
            {

                if (dropDownItem.DropDownItems.Count > 0)
                {
                    foreach (ToolStripMenuItem subItem in GetMenuItems(dropDownItem))
                        yield return subItem;
                }
                yield return dropDownItem;
            }
        }

        public void LogOut()
        {
            IsAuth = false;
            Content.UserManager.Logout();
            ClearLoginInfo();
            UpdateInterface();

            mainTb.TabPages.Clear();
        }
    }
}
