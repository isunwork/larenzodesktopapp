﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraSplashScreen;

namespace LaRenzo.Forms.MainFormAndIM
{
    public partial class SplashScreen2 : SplashScreen
    {
        public SplashScreen2()
        {
            InitializeComponent();
            TransparencyKey = Color.LimeGreen;

            FormBorderStyle = FormBorderStyle.None;
        }

        #region Overrides

        #endregion

        public enum SplashScreenCommand
        {
        }

        private void pictureEdit2_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void marqueeProgressBarControl3_EditValueChanged(object sender, EventArgs e)
        {

        }
    }
}