﻿namespace LaRenzo.Forms.ReportForms
{
    partial class SettlsWithPartnerReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettlsWithPartnerReport));
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDocument = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnOverPayment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDeficit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pBarGeneration = new System.Windows.Forms.ProgressBar();
            this.lblPeriodEnd = new System.Windows.Forms.Label();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.dTimeEnd = new System.Windows.Forms.DateTimePicker();
            this.backgroundWorkerReportGenerator = new System.ComponentModel.BackgroundWorker();
            this.btnPrint = new System.Windows.Forms.Button();
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.printableComponentLink = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printableComponentLink.ImageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // mainGridControl
            // 
            this.mainGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainGridControl.Location = new System.Drawing.Point(0, 34);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(1014, 458);
            this.mainGridControl.TabIndex = 18;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.BestFitMaxRowCount = 10;
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnName,
            this.columnDocument,
            this.columnOverPayment,
            this.columnDeficit});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.mainGridView.OptionsView.ShowFooter = true;
            // 
            // columnName
            // 
            this.columnName.Caption = "Контрагент";
            this.columnName.FieldName = "Partner.PartnerShortName";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.OptionsColumn.AllowFocus = false;
            this.columnName.OptionsColumn.ReadOnly = true;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 0;
            // 
            // columnDocument
            // 
            this.columnDocument.Caption = "Документ";
            this.columnDocument.FieldName = "SupplyDocument";
            this.columnDocument.Name = "columnDocument";
            this.columnDocument.OptionsColumn.AllowEdit = false;
            this.columnDocument.OptionsColumn.AllowFocus = false;
            this.columnDocument.OptionsColumn.ReadOnly = true;
            this.columnDocument.Visible = true;
            this.columnDocument.VisibleIndex = 1;
            // 
            // columnOverPayment
            // 
            this.columnOverPayment.Caption = "Переплата";
            this.columnOverPayment.DisplayFormat.FormatString = "F2";
            this.columnOverPayment.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnOverPayment.FieldName = "OverPayment";
            this.columnOverPayment.Name = "columnOverPayment";
            this.columnOverPayment.OptionsColumn.AllowEdit = false;
            this.columnOverPayment.OptionsColumn.AllowFocus = false;
            this.columnOverPayment.OptionsColumn.ReadOnly = true;
            this.columnOverPayment.Visible = true;
            this.columnOverPayment.VisibleIndex = 2;
            // 
            // columnDeficit
            // 
            this.columnDeficit.Caption = "Долг";
            this.columnDeficit.DisplayFormat.FormatString = "F2";
            this.columnDeficit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnDeficit.FieldName = "DeficitPayment";
            this.columnDeficit.Name = "columnDeficit";
            this.columnDeficit.OptionsColumn.AllowEdit = false;
            this.columnDeficit.OptionsColumn.AllowFocus = false;
            this.columnDeficit.OptionsColumn.ReadOnly = true;
            this.columnDeficit.Visible = true;
            this.columnDeficit.VisibleIndex = 3;
            // 
            // pBarGeneration
            // 
            this.pBarGeneration.Location = new System.Drawing.Point(355, 5);
            this.pBarGeneration.Name = "pBarGeneration";
            this.pBarGeneration.Size = new System.Drawing.Size(194, 23);
            this.pBarGeneration.TabIndex = 21;
            // 
            // lblPeriodEnd
            // 
            this.lblPeriodEnd.AutoSize = true;
            this.lblPeriodEnd.Location = new System.Drawing.Point(7, 12);
            this.lblPeriodEnd.Name = "lblPeriodEnd";
            this.lblPeriodEnd.Size = new System.Drawing.Size(36, 13);
            this.lblPeriodEnd.TabIndex = 20;
            this.lblPeriodEnd.Text = "Дата:";
            // 
            // btnGenerate
            // 
            this.btnGenerate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnGenerate.Location = new System.Drawing.Point(205, 5);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(144, 23);
            this.btnGenerate.TabIndex = 19;
            this.btnGenerate.Text = "Сгенерировать отчет";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // dTimeEnd
            // 
            this.dTimeEnd.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dTimeEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dTimeEnd.Location = new System.Drawing.Point(49, 8);
            this.dTimeEnd.Name = "dTimeEnd";
            this.dTimeEnd.Size = new System.Drawing.Size(150, 20);
            this.dTimeEnd.TabIndex = 17;
            // 
            // backgroundWorkerReportGenerator
            // 
            this.backgroundWorkerReportGenerator.WorkerReportsProgress = true;
            this.backgroundWorkerReportGenerator.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerReportGenerator_DoWork);
            this.backgroundWorkerReportGenerator.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerReportGenerator_ProgressChanged);
            this.backgroundWorkerReportGenerator.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerReportGenerator_RunWorkerCompleted);
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.Location = new System.Drawing.Point(902, 5);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(100, 23);
            this.btnPrint.TabIndex = 22;
            this.btnPrint.Text = "Печать";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.button1_Click);
            // 
            // printingSystem
            // 
            this.printingSystem.Links.AddRange(new object[] {
            this.printableComponentLink});
            // 
            // printableComponentLink
            // 
            this.printableComponentLink.Component = this.mainGridControl;
            // 
            // 
            // 
            this.printableComponentLink.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("printableComponentLink.ImageCollection.ImageStream")));
            this.printableComponentLink.PrintingSystem = this.printingSystem;
            this.printableComponentLink.PrintingSystemBase = this.printingSystem;
            this.printableComponentLink.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.printableComponentLink_CreateReportHeaderArea);
            // 
            // SettlsWithPartnerReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 496);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.mainGridControl);
            this.Controls.Add(this.pBarGeneration);
            this.Controls.Add(this.lblPeriodEnd);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.dTimeEnd);
            this.Name = "SettlsWithPartnerReport";
            this.Text = "Взаиморасчеты с контрагентами";
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printableComponentLink.ImageCollection)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public DevExpress.XtraGrid.GridControl mainGridControl;
        public DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private System.Windows.Forms.ProgressBar pBarGeneration;
        private System.Windows.Forms.Label lblPeriodEnd;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.DateTimePicker dTimeEnd;
        private System.ComponentModel.BackgroundWorker backgroundWorkerReportGenerator;
        private DevExpress.XtraGrid.Columns.GridColumn columnDocument;
        private DevExpress.XtraGrid.Columns.GridColumn columnOverPayment;
        private DevExpress.XtraGrid.Columns.GridColumn columnDeficit;
        private System.Windows.Forms.Button btnPrint;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;
        private DevExpress.XtraPrinting.PrintableComponentLink printableComponentLink;

    }
}