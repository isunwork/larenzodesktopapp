﻿namespace LaRenzo.Forms.ReportForms
{
    partial class ReportDeliveryTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnStreet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCooking = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCookingCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDelivery = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDeliveryCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.lblPeriodStart = new System.Windows.Forms.Label();
            this.backgroundWorkerReportGenerator = new System.ComponentModel.BackgroundWorker();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnPrint = new System.Windows.Forms.Button();
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.printableComponentLink = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelTimeDeliveryValue = new System.Windows.Forms.Label();
            this.labelTimeDelivery = new System.Windows.Forms.Label();
            this.labelTimeValue = new System.Windows.Forms.Label();
            this.labelTime = new System.Windows.Forms.Label();
            this.dateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerTo = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainGridControl
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.mainGridControl, 6);
            this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.mainGridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.mainGridControl.Location = new System.Drawing.Point(3, 77);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(1237, 422);
            this.mainGridControl.TabIndex = 3;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnStreet,
            this.gridColumnCooking,
            this.gridColumnCookingCount,
            this.gridColumnDelivery,
            this.gridColumnDeliveryCount});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.mainGridView.OptionsView.BestFitMaxRowCount = 10;
            this.mainGridView.OptionsView.ShowFooter = true;
            // 
            // gridColumnStreet
            // 
            this.gridColumnStreet.Caption = "Улица";
            this.gridColumnStreet.FieldName = "Steet";
            this.gridColumnStreet.Name = "gridColumnStreet";
            this.gridColumnStreet.Visible = true;
            this.gridColumnStreet.VisibleIndex = 0;
            // 
            // gridColumnCooking
            // 
            this.gridColumnCooking.Caption = "Готовка (минут)";
            this.gridColumnCooking.FieldName = "CookingTime";
            this.gridColumnCooking.Name = "gridColumnCooking";
            this.gridColumnCooking.Visible = true;
            this.gridColumnCooking.VisibleIndex = 1;
            // 
            // gridColumnCookingCount
            // 
            this.gridColumnCookingCount.Caption = "Количество готовки заказов";
            this.gridColumnCookingCount.FieldName = "CookingTimeCount";
            this.gridColumnCookingCount.Name = "gridColumnCookingCount";
            this.gridColumnCookingCount.Visible = true;
            this.gridColumnCookingCount.VisibleIndex = 2;
            // 
            // gridColumnDelivery
            // 
            this.gridColumnDelivery.Caption = "Доставка (минут)";
            this.gridColumnDelivery.FieldName = "DeliveryTime";
            this.gridColumnDelivery.Name = "gridColumnDelivery";
            this.gridColumnDelivery.Visible = true;
            this.gridColumnDelivery.VisibleIndex = 3;
            // 
            // gridColumnDeliveryCount
            // 
            this.gridColumnDeliveryCount.Caption = "Количество доставки заказов";
            this.gridColumnDeliveryCount.FieldName = "DeliveryTimeCount";
            this.gridColumnDeliveryCount.Name = "gridColumnDeliveryCount";
            this.gridColumnDeliveryCount.Visible = true;
            this.gridColumnDeliveryCount.VisibleIndex = 4;
            // 
            // btnGenerate
            // 
            this.btnGenerate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnGenerate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnGenerate.Location = new System.Drawing.Point(600, 0);
            this.btnGenerate.Margin = new System.Windows.Forms.Padding(0);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(300, 37);
            this.btnGenerate.TabIndex = 3;
            this.btnGenerate.Text = "Сгенерировать отчет";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // lblPeriodStart
            // 
            this.lblPeriodStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPeriodStart.Location = new System.Drawing.Point(3, 0);
            this.lblPeriodStart.Name = "lblPeriodStart";
            this.lblPeriodStart.Size = new System.Drawing.Size(69, 37);
            this.lblPeriodStart.TabIndex = 7;
            this.lblPeriodStart.Text = "Заказы с:";
            this.lblPeriodStart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // backgroundWorkerReportGenerator
            // 
            this.backgroundWorkerReportGenerator.WorkerReportsProgress = true;
            this.backgroundWorkerReportGenerator.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerReportGenerator_DoWork);
            this.backgroundWorkerReportGenerator.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerReportGenerator_RunWorkerCompleted);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // btnPrint
            // 
            this.btnPrint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPrint.Location = new System.Drawing.Point(900, 0);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(0);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(343, 37);
            this.btnPrint.TabIndex = 23;
            this.btnPrint.Text = "Печать";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // printingSystem
            // 
            this.printingSystem.Links.AddRange(new object[] {
            this.printableComponentLink});
            // 
            // printableComponentLink
            // 
            this.printableComponentLink.Component = this.mainGridControl;
            this.printableComponentLink.PrintingSystemBase = this.printingSystem;
            this.printableComponentLink.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.printableComponentLink_CreateReportHeaderArea);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(303, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 37);
            this.label1.TabIndex = 25;
            this.label1.Text = "Заказы по:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 225F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 225F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel1.Controls.Add(this.labelTimeDeliveryValue, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelTimeDelivery, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelTimeValue, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelTime, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.mainGridControl, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnPrint, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnGenerate, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblPeriodStart, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePickerFrom, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePickerTo, 3, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1243, 502);
            this.tableLayoutPanel1.TabIndex = 28;
            // 
            // labelTimeDeliveryValue
            // 
            this.labelTimeDeliveryValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTimeDeliveryValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTimeDeliveryValue.Location = new System.Drawing.Point(903, 37);
            this.labelTimeDeliveryValue.Name = "labelTimeDeliveryValue";
            this.labelTimeDeliveryValue.Size = new System.Drawing.Size(337, 37);
            this.labelTimeDeliveryValue.TabIndex = 32;
            this.labelTimeDeliveryValue.Text = "0 минут";
            this.labelTimeDeliveryValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelTimeDelivery
            // 
            this.labelTimeDelivery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTimeDelivery.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTimeDelivery.Location = new System.Drawing.Point(603, 37);
            this.labelTimeDelivery.Name = "labelTimeDelivery";
            this.labelTimeDelivery.Size = new System.Drawing.Size(294, 37);
            this.labelTimeDelivery.TabIndex = 31;
            this.labelTimeDelivery.Text = "Время доставки:";
            this.labelTimeDelivery.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelTimeValue
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.labelTimeValue, 2);
            this.labelTimeValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTimeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTimeValue.Location = new System.Drawing.Point(303, 37);
            this.labelTimeValue.Name = "labelTimeValue";
            this.labelTimeValue.Size = new System.Drawing.Size(294, 37);
            this.labelTimeValue.TabIndex = 30;
            this.labelTimeValue.Text = "0 минут";
            this.labelTimeValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelTime
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.labelTime, 2);
            this.labelTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTime.Location = new System.Drawing.Point(3, 37);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(294, 37);
            this.labelTime.TabIndex = 29;
            this.labelTime.Text = "Время приготовления:";
            this.labelTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dateTimePickerFrom
            // 
            this.dateTimePickerFrom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateTimePickerFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateTimePickerFrom.Location = new System.Drawing.Point(77, 2);
            this.dateTimePickerFrom.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimePickerFrom.Name = "dateTimePickerFrom";
            this.dateTimePickerFrom.Size = new System.Drawing.Size(221, 35);
            this.dateTimePickerFrom.TabIndex = 27;
            this.dateTimePickerFrom.Value = new System.DateTime(2017, 1, 1, 0, 0, 0, 0);
            // 
            // dateTimePickerTo
            // 
            this.dateTimePickerTo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateTimePickerTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateTimePickerTo.Location = new System.Drawing.Point(377, 2);
            this.dateTimePickerTo.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimePickerTo.Name = "dateTimePickerTo";
            this.dateTimePickerTo.Size = new System.Drawing.Size(221, 35);
            this.dateTimePickerTo.TabIndex = 28;
            this.dateTimePickerTo.Value = new System.DateTime(2017, 9, 1, 0, 0, 0, 0);
            // 
            // ReportDeliveryTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1243, 502);
            this.Controls.Add(this.tableLayoutPanel1);
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(1114, 541);
            this.Name = "ReportDeliveryTime";
            this.Text = "Отчёт по продажам";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SaleReportForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraGrid.GridControl mainGridControl;
        public DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.Label lblPeriodStart;
        private System.ComponentModel.BackgroundWorker backgroundWorkerReportGenerator;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Button btnPrint;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;
        private DevExpress.XtraPrinting.PrintableComponentLink printableComponentLink;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DateTimePicker dateTimePickerFrom;
        private System.Windows.Forms.DateTimePicker dateTimePickerTo;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStreet;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCooking;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCookingCount;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDeliveryCount;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDelivery;
        private System.Windows.Forms.Label labelTimeValue;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Label labelTimeDeliveryValue;
        private System.Windows.Forms.Label labelTimeDelivery;
    }
}