﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Data.PLinq.Helpers;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Reports.DishCombinationReport;
using LaRenzo.Tools;

namespace LaRenzo.Forms.ReportForms
{
    public partial class DishCombinationReport : Form
    {
        private DishCombinationManager _mng;

        private Stopwatch _swStopwatch;
        
        public DishCombinationReport()
        {
            InitializeComponent();

        }


        private async void StartCalc(int searchAmount)
        {
            var key = ExecutionTimeLogger.Start();
            await Task.Run(() => _mng.GetMostPopularDishCombination(GetSelectedCategoriesIDs(), searchAmount));
            progressBar1.Value = 100;
            ExecutionTimeLogger.Stop(key, "Отчет по сочетаниям блюд");

            gridControl1.DataSource = _mng.TopList.OrderByDescending(x => x.Item2).Take(searchAmount);
        }

        private void DishCombinationReport_Load(object sender, EventArgs e)
        {
            var mng = Content.CategoryRepository;
            checkedListBox1.DataSource = mng.GetItems().ToList();
            checkedListBox1.DisplayMember = "Name";
            MarkAll(true);

        }


        private void timer2_Tick(object sender, EventArgs e)
        {
            progressBar1.Value = _mng != null ? _mng.DataStorage != null ? _mng.DataStorage.TotalPercent() : 0 : 0;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            MarkAll(true);
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            MarkAll(false);
        }

        private IEnumerable<int> GetSelectedCategoriesIDs()
        {
            return (from object t in checkedListBox1.CheckedItems select ((Category) t).ID).ToList();
        }


        private void MarkAll(bool isChecked)
        {
            for (int i = 0; i < checkedListBox1.Items.Count; i++)
                checkedListBox1.SetItemChecked(i, isChecked);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            gridControl1.DataSource = null;

            _mng = new DishCombinationManager((int)numericUpDown1.Value);

            StartCalc((int)numericUpDown2.Value);
        }

    }

}
