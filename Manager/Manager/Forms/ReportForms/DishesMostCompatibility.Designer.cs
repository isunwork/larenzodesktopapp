﻿namespace LaRenzo.Forms.ReportForms
{
    partial class DishesMostCompatibility
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.backgroundWorkerReportGenerator = new System.ComponentModel.BackgroundWorker();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnPrint = new System.Windows.Forms.Button();
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.printableComponentLink = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnMainDish = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDish1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDish1Count = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDish2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDish2Count = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDish3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDish3Count = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDish4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDish4Count = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDish5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDish5Count = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.gridColumnMainDishCount = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // backgroundWorkerReportGenerator
            // 
            this.backgroundWorkerReportGenerator.WorkerReportsProgress = true;
            this.backgroundWorkerReportGenerator.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerReportGenerator_DoWork);
            this.backgroundWorkerReportGenerator.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerReportGenerator_RunWorkerCompleted);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // btnPrint
            // 
            this.btnPrint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPrint.Location = new System.Drawing.Point(900, 0);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(0);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(198, 37);
            this.btnPrint.TabIndex = 23;
            this.btnPrint.Text = "Печать";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // printingSystem
            // 
            this.printingSystem.Links.AddRange(new object[] {
            this.printableComponentLink});
            // 
            // printableComponentLink
            // 
            this.printableComponentLink.Component = this.mainGridControl;
            this.printableComponentLink.PrintingSystemBase = this.printingSystem;
            this.printableComponentLink.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.printableComponentLink_CreateReportHeaderArea);
            // 
            // mainGridControl
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.mainGridControl, 6);
            this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.mainGridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.mainGridControl.Location = new System.Drawing.Point(3, 40);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(1092, 453);
            this.mainGridControl.TabIndex = 3;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnMainDish,
            this.gridColumnMainDishCount,
            this.gridColumnDish1,
            this.gridColumnDish1Count,
            this.gridColumnDish2,
            this.gridColumnDish2Count,
            this.gridColumnDish3,
            this.gridColumnDish3Count,
            this.gridColumnDish4,
            this.gridColumnDish4Count,
            this.gridColumnDish5,
            this.gridColumnDish5Count});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.mainGridView.OptionsView.BestFitMaxRowCount = 10;
            this.mainGridView.OptionsView.ShowFooter = true;
            // 
            // gridColumnMainDish
            // 
            this.gridColumnMainDish.Caption = "Основное блюда";
            this.gridColumnMainDish.FieldName = "MainDishName";
            this.gridColumnMainDish.Name = "gridColumnMainDish";
            this.gridColumnMainDish.Visible = true;
            this.gridColumnMainDish.VisibleIndex = 0;
            // 
            // gridColumnDish1
            // 
            this.gridColumnDish1.Caption = "Топ 1 Блюдо";
            this.gridColumnDish1.FieldName = "Dish1Name";
            this.gridColumnDish1.Name = "gridColumnDish1";
            this.gridColumnDish1.Visible = true;
            this.gridColumnDish1.VisibleIndex = 2;
            // 
            // gridColumnDish1Count
            // 
            this.gridColumnDish1Count.Caption = "Число заказов 1";
            this.gridColumnDish1Count.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnDish1Count.FieldName = "Dish1Count";
            this.gridColumnDish1Count.Name = "gridColumnDish1Count";
            this.gridColumnDish1Count.Visible = true;
            this.gridColumnDish1Count.VisibleIndex = 3;
            // 
            // gridColumnDish2
            // 
            this.gridColumnDish2.Caption = "Топ 2 Блюдо";
            this.gridColumnDish2.FieldName = "Dish2Name";
            this.gridColumnDish2.Name = "gridColumnDish2";
            this.gridColumnDish2.Visible = true;
            this.gridColumnDish2.VisibleIndex = 4;
            // 
            // gridColumnDish2Count
            // 
            this.gridColumnDish2Count.Caption = "Число заказов 2";
            this.gridColumnDish2Count.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnDish2Count.FieldName = "Dish2Count";
            this.gridColumnDish2Count.Name = "gridColumnDish2Count";
            this.gridColumnDish2Count.Visible = true;
            this.gridColumnDish2Count.VisibleIndex = 5;
            // 
            // gridColumnDish3
            // 
            this.gridColumnDish3.Caption = "Топ 3 Блюдо";
            this.gridColumnDish3.FieldName = "Dish3Name";
            this.gridColumnDish3.Name = "gridColumnDish3";
            this.gridColumnDish3.Visible = true;
            this.gridColumnDish3.VisibleIndex = 8;
            // 
            // gridColumnDish3Count
            // 
            this.gridColumnDish3Count.Caption = "Число заказов 3";
            this.gridColumnDish3Count.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnDish3Count.FieldName = "Dish3Count";
            this.gridColumnDish3Count.Name = "gridColumnDish3Count";
            this.gridColumnDish3Count.Visible = true;
            this.gridColumnDish3Count.VisibleIndex = 7;
            // 
            // gridColumnDish4
            // 
            this.gridColumnDish4.Caption = "Топ 4 Блюдо";
            this.gridColumnDish4.FieldName = "Dish4Name";
            this.gridColumnDish4.Name = "gridColumnDish4";
            this.gridColumnDish4.Visible = true;
            this.gridColumnDish4.VisibleIndex = 6;
            // 
            // gridColumnDish4Count
            // 
            this.gridColumnDish4Count.Caption = "Число заказов 4";
            this.gridColumnDish4Count.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnDish4Count.FieldName = "Dish4Count";
            this.gridColumnDish4Count.Name = "gridColumnDish4Count";
            this.gridColumnDish4Count.Visible = true;
            this.gridColumnDish4Count.VisibleIndex = 9;
            // 
            // gridColumnDish5
            // 
            this.gridColumnDish5.Caption = "Топ 5 Блюдо";
            this.gridColumnDish5.FieldName = "Dish5Name";
            this.gridColumnDish5.Name = "gridColumnDish5";
            this.gridColumnDish5.Visible = true;
            this.gridColumnDish5.VisibleIndex = 10;
            // 
            // gridColumnDish5Count
            // 
            this.gridColumnDish5Count.Caption = "Число заказов 5";
            this.gridColumnDish5Count.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnDish5Count.FieldName = "Dish5Count";
            this.gridColumnDish5Count.Name = "gridColumnDish5Count";
            this.gridColumnDish5Count.Visible = true;
            this.gridColumnDish5Count.VisibleIndex = 11;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 225F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 225F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel1.Controls.Add(this.btnGenerate, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.mainGridControl, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnPrint, 5, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1098, 502);
            this.tableLayoutPanel1.TabIndex = 28;
            // 
            // btnGenerate
            // 
            this.btnGenerate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnGenerate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnGenerate.Location = new System.Drawing.Point(600, 0);
            this.btnGenerate.Margin = new System.Windows.Forms.Padding(0);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(300, 37);
            this.btnGenerate.TabIndex = 24;
            this.btnGenerate.Text = "Сформировать отчёт";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // gridColumnMainDishCount
            // 
            this.gridColumnMainDishCount.Caption = "Число заказов основного блюда";
            this.gridColumnMainDishCount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnMainDishCount.FieldName = "MainDishCount";
            this.gridColumnMainDishCount.Name = "gridColumnMainDishCount";
            this.gridColumnMainDishCount.Visible = true;
            this.gridColumnMainDishCount.VisibleIndex = 1;
            // 
            // DishesMostCompatibility
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1098, 502);
            this.Controls.Add(this.tableLayoutPanel1);
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(1114, 541);
            this.Name = "DishesMostCompatibility";
            this.Text = "Клиенты";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SaleReportForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.ComponentModel.BackgroundWorker backgroundWorkerReportGenerator;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Button btnPrint;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;
        private DevExpress.XtraPrinting.PrintableComponentLink printableComponentLink;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public DevExpress.XtraGrid.GridControl mainGridControl;
        public DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnMainDish;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDish1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDish2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDish3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDish4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDish5;
        private System.Windows.Forms.Button btnGenerate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDish1Count;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDish2Count;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDish3Count;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDish4Count;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDish5Count;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnMainDishCount;
    }
}