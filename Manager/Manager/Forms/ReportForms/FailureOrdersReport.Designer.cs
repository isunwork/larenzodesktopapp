﻿namespace LaRenzo.Forms.ReportForms
{
    partial class FailureOrdersReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pickerDateFrom = new System.Windows.Forms.DateTimePicker();
            this.pickerDateTo = new System.Windows.Forms.DateTimePicker();
            this.labelDateFrom = new System.Windows.Forms.Label();
            this.labelDateTo = new System.Windows.Forms.Label();
            this.buttonGenerateReport = new System.Windows.Forms.Button();
            this.gridReport = new DevExpress.XtraGrid.GridControl();
            this.gridViewReport = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridFailureReason = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewReport)).BeginInit();
            this.SuspendLayout();
            // 
            // pickerDateFrom
            // 
            this.pickerDateFrom.Location = new System.Drawing.Point(12, 43);
            this.pickerDateFrom.Name = "pickerDateFrom";
            this.pickerDateFrom.Size = new System.Drawing.Size(200, 22);
            this.pickerDateFrom.TabIndex = 0;
            // 
            // pickerDateTo
            // 
            this.pickerDateTo.Location = new System.Drawing.Point(235, 43);
            this.pickerDateTo.Name = "pickerDateTo";
            this.pickerDateTo.Size = new System.Drawing.Size(200, 22);
            this.pickerDateTo.TabIndex = 1;
            // 
            // labelDateFrom
            // 
            this.labelDateFrom.AutoSize = true;
            this.labelDateFrom.Location = new System.Drawing.Point(12, 9);
            this.labelDateFrom.Name = "labelDateFrom";
            this.labelDateFrom.Size = new System.Drawing.Size(61, 17);
            this.labelDateFrom.TabIndex = 2;
            this.labelDateFrom.Text = "Дата от";
            // 
            // labelDateTo
            // 
            this.labelDateTo.AutoSize = true;
            this.labelDateTo.Location = new System.Drawing.Point(232, 9);
            this.labelDateTo.Name = "labelDateTo";
            this.labelDateTo.Size = new System.Drawing.Size(62, 17);
            this.labelDateTo.TabIndex = 3;
            this.labelDateTo.Text = "Дата до";
            // 
            // buttonGenerateReport
            // 
            this.buttonGenerateReport.Location = new System.Drawing.Point(467, 9);
            this.buttonGenerateReport.Name = "buttonGenerateReport";
            this.buttonGenerateReport.Size = new System.Drawing.Size(155, 29);
            this.buttonGenerateReport.TabIndex = 4;
            this.buttonGenerateReport.Text = "Создать отчет";
            this.buttonGenerateReport.UseVisualStyleBackColor = true;
            this.buttonGenerateReport.Click += new System.EventHandler(this.buttonGenerateReport_Click);
            // 
            // gridReport
            // 
            this.gridReport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridReport.CausesValidation = false;
            this.gridReport.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.gridReport.Location = new System.Drawing.Point(0, 90);
            this.gridReport.MainView = this.gridViewReport;
            this.gridReport.Margin = new System.Windows.Forms.Padding(4);
            this.gridReport.Name = "gridReport";
            this.gridReport.Size = new System.Drawing.Size(1057, 359);
            this.gridReport.TabIndex = 1;
            this.gridReport.Tag = "1";
            this.gridReport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewReport});
            // 
            // gridViewReport
            // 
            this.gridViewReport.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridDate,
            this.gridTime,
            this.gridCode,
            this.gridTotalCost,
            this.gridAddress,
            this.gridFailureReason});
            this.gridViewReport.GridControl = this.gridReport;
            this.gridViewReport.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalCost", this.gridTotalCost, "{0:F2} ₽"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "FailureReason", this.gridFailureReason, "{0}")});
            this.gridViewReport.Name = "gridViewReport";
            this.gridViewReport.OptionsView.ShowFooter = true;
            // 
            // gridDate
            // 
            this.gridDate.Caption = "Дата";
            this.gridDate.DisplayFormat.FormatString = "dd.MM.yyyy";
            this.gridDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridDate.FieldName = "Created";
            this.gridDate.Name = "gridDate";
            this.gridDate.Visible = true;
            this.gridDate.VisibleIndex = 0;
            // 
            // gridTime
            // 
            this.gridTime.Caption = "Время";
            this.gridTime.DisplayFormat.FormatString = "HH:mm";
            this.gridTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridTime.FieldName = "Created";
            this.gridTime.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridTime.GroupFormat.FormatString = "HH:mm";
            this.gridTime.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridTime.Name = "gridTime";
            this.gridTime.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.gridTime.Visible = true;
            this.gridTime.VisibleIndex = 1;
            // 
            // gridCode
            // 
            this.gridCode.Caption = "Номер заказа";
            this.gridCode.FieldName = "Code";
            this.gridCode.Name = "gridCode";
            this.gridCode.Visible = true;
            this.gridCode.VisibleIndex = 2;
            // 
            // gridTotalCost
            // 
            this.gridTotalCost.Caption = "Сумма";
            this.gridTotalCost.DisplayFormat.FormatString = "F2";
            this.gridTotalCost.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridTotalCost.FieldName = "TotalCost";
            this.gridTotalCost.Name = "gridTotalCost";
            this.gridTotalCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalCost", "{0:F2} ₽")});
            this.gridTotalCost.Visible = true;
            this.gridTotalCost.VisibleIndex = 3;
            // 
            // gridAddress
            // 
            this.gridAddress.Caption = "Адрес";
            this.gridAddress.FieldName = "Address";
            this.gridAddress.Name = "gridAddress";
            this.gridAddress.UnboundExpression = "Concat([Street], \' \', [House], \' \', [Appartament])";
            this.gridAddress.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.gridAddress.Visible = true;
            this.gridAddress.VisibleIndex = 4;
            // 
            // gridFailureReason
            // 
            this.gridFailureReason.Caption = "Причина отказа";
            this.gridFailureReason.FieldName = "FailureReason";
            this.gridFailureReason.Name = "gridFailureReason";
            this.gridFailureReason.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "FailureReason", "{0}")});
            this.gridFailureReason.Visible = true;
            this.gridFailureReason.VisibleIndex = 5;
            // 
            // FailureOrdersReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1057, 450);
            this.Controls.Add(this.buttonGenerateReport);
            this.Controls.Add(this.labelDateTo);
            this.Controls.Add(this.labelDateFrom);
            this.Controls.Add(this.pickerDateTo);
            this.Controls.Add(this.pickerDateFrom);
            this.Controls.Add(this.gridReport);
            this.Name = "FailureOrdersReport";
            this.Text = "Отчет по отказам";
            ((System.ComponentModel.ISupportInitialize)(this.gridReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewReport)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker pickerDateFrom;
        private System.Windows.Forms.DateTimePicker pickerDateTo;
        private System.Windows.Forms.Label labelDateFrom;
        private System.Windows.Forms.Label labelDateTo;
        private System.Windows.Forms.Button buttonGenerateReport;
        private DevExpress.XtraGrid.GridControl gridReport;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewReport;
        private DevExpress.XtraGrid.Columns.GridColumn gridDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridTime;
        private DevExpress.XtraGrid.Columns.GridColumn gridCode;
        private DevExpress.XtraGrid.Columns.GridColumn gridTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn gridAddress;
        private DevExpress.XtraGrid.Columns.GridColumn gridFailureReason;
    }
}