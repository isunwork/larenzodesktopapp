﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using LaRenzo.DataRepository.Entities.Documents.Sessions;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Documents.Sessions;
using LaRenzo.DataRepository.Repositories.Others.Statistics;
using LaRenzo.Properties;
using LaRenzo.Tools;

namespace LaRenzo.Forms.ReportForms
{

    public partial class SaleReportSimpleForm : Form
    {
        private class SessionComboBoxItem
        {
            public Session Session;
            public SessionComboBoxItem(Session session)
            {
                Session = session;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return $"{Session.Opened} - {Session.Closed}";
            }
        }
        public SaleReportSimpleForm()
        {
            InitializeComponent();
            mainGridView.VertScrollVisibility = ScrollVisibility.Auto;
            mainGridView.ScrollStyle = ScrollStyleFlags.LiveVertScroll;
            var sessions = Content.SessionManager.GetFullList().Where(x=>x.Opened >= DateTime.Now.AddMonths(-6));
            comboBoxSession.Items.Clear();
            foreach (var session in sessions.OrderByDescending(x=>x.ID))
            {
                var item = new SessionComboBoxItem(session);
                comboBoxSession.Items.Add(item);
            }
            cmbSaleMode.SelectedIndex = 0;
            ReportHelper.SetUpButtonDefault(btnGenerate);
        }
        
        private void btnGenerate_Click(object sender, EventArgs e)
        {
            UpdateData();
        }
        
        private void SaleReportForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) UpdateData();
            if (e.KeyCode == Keys.Escape) Close();
        }

        private void UpdateData()
        {
            if (backgroundWorkerReportGenerator.IsBusy)
            {
                MessageBox.Show(Resources.GeneraitingAnotherReport);
            }
            else if (comboBoxSession.SelectedItem == null)
            {
                MessageBox.Show("Выберите сессию за которую хотите получить отчёт");
            }
            else
            {
                ReportHelper.SetUpButtonDefault(btnGenerate);
                backgroundWorkerReportGenerator.RunWorkerAsync();
            }
        }


        private void backgroundWorkerReportGenerator_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            int mode = 0;
            SessionComboBoxItem session = null;
            Invoke((MethodInvoker)delegate
                {
                    mode = cmbSaleMode.SelectedIndex;
                    session = (SessionComboBoxItem) comboBoxSession.SelectedItem;
                });

            var staticManager = Content.StatisticManager;
            e.Result = staticManager.GetSimpleSalesReportData(session.Session.ID, (ReportModeEnum) mode);
        }
        
        private void backgroundWorkerReportGenerator_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            mainGridControl.DataSource = null;
            mainGridControl.DataSource = e.Result;
            ReportHelper.SetUpButtonDefault(btnGenerate);
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            printableComponentLink.CreateDocument();
            printableComponentLink.ShowPreview();
        }

        private void printableComponentLink_CreateReportHeaderArea(object sender, CreateAreaEventArgs e)
        {
            string reportName =
                $"Отчет по продажам " +
                $"({cmbSaleMode.Items[cmbSaleMode.SelectedIndex]}) c " +
                $"{((SessionComboBoxItem) comboBoxSession.SelectedItem).Session.Opened:dd.MM.yyyy} по " +
                $"{((SessionComboBoxItem) comboBoxSession.SelectedItem).Session.Closed:dd.MM.yyyy}";

            TextBrick brick = e.Graph.DrawString(reportName, Color.Navy, new RectangleF(0, 0, 560, 60), BorderSide.None);

            brick.Font = new Font("Arial", 16);

            brick.StringFormat = new BrickStringFormat(StringAlignment.Center);
        }
    }
}
