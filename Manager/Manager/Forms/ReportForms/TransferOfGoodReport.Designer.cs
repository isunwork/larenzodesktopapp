﻿namespace LaRenzo.Forms.ReportForms
{
    partial class TransferOfGoodsReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            this.detailGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnDocument = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnAdd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDelete = new DevExpress.XtraGrid.Columns.GridColumn();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnAmountEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnMeasure = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnSum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnAmountStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnAmountAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnAmountDeleted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lblPeriodEnd = new System.Windows.Forms.Label();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.dTimeEnd = new System.Windows.Forms.DateTimePicker();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmbWarehouse = new System.Windows.Forms.ComboBox();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.printableComponentLink = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            this.lblPeriodStart = new System.Windows.Forms.Label();
            this.dTimeStart = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.detailGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).BeginInit();
            this.SuspendLayout();
            // 
            // detailGridView
            // 
            this.detailGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnDocument,
            this.columnAdd,
            this.columnDelete});
            this.detailGridView.GridControl = this.mainGridControl;
            this.detailGridView.Name = "detailGridView";
            // 
            // columnDocument
            // 
            this.columnDocument.Caption = "Документ";
            this.columnDocument.FieldName = "DocumentName";
            this.columnDocument.Name = "columnDocument";
            this.columnDocument.OptionsColumn.AllowEdit = false;
            this.columnDocument.OptionsColumn.AllowFocus = false;
            this.columnDocument.OptionsColumn.ReadOnly = true;
            this.columnDocument.Visible = true;
            this.columnDocument.VisibleIndex = 0;
            // 
            // columnAdd
            // 
            this.columnAdd.Caption = "Поступление";
            this.columnAdd.FieldName = "ProductAmmountAdded";
            this.columnAdd.Name = "columnAdd";
            this.columnAdd.OptionsColumn.AllowEdit = false;
            this.columnAdd.OptionsColumn.AllowFocus = false;
            this.columnAdd.OptionsColumn.ReadOnly = true;
            this.columnAdd.Visible = true;
            this.columnAdd.VisibleIndex = 1;
            // 
            // columnDelete
            // 
            this.columnDelete.Caption = "Расход";
            this.columnDelete.FieldName = "ProductAmmountDeleted";
            this.columnDelete.Name = "columnDelete";
            this.columnDelete.OptionsColumn.AllowEdit = false;
            this.columnDelete.OptionsColumn.AllowFocus = false;
            this.columnDelete.OptionsColumn.ReadOnly = true;
            this.columnDelete.Visible = true;
            this.columnDelete.VisibleIndex = 2;
            // 
            // mainGridControl
            // 
            this.mainGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            gridLevelNode2.LevelTemplate = this.detailGridView;
            gridLevelNode2.RelationName = "detailGridView";
            this.mainGridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.mainGridControl.Location = new System.Drawing.Point(0, 34);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(1178, 458);
            this.mainGridControl.TabIndex = 12;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView,
            this.detailGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnId,
            this.columnName,
            this.columnAmountEnd,
            this.columnMeasure,
            this.columnPrice,
            this.columnSum,
            this.columnAmountStart,
            this.columnAmountAdded,
            this.columnAmountDeleted});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.mainGridView.OptionsView.BestFitMaxRowCount = 10;
            this.mainGridView.OptionsView.ShowFooter = true;
            this.mainGridView.MasterRowGetChildList += new DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventHandler(this.mainGridView_MasterRowGetChildList);
            this.mainGridView.MasterRowGetRelationName += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.mainGridView_MasterRowGetRelationName);
            this.mainGridView.MasterRowGetRelationDisplayCaption += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.mainGridView_MasterRowGetRelationDisplayCaption);
            this.mainGridView.MasterRowGetRelationCount += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventHandler(this.mainGridView_MasterRowGetRelationCount);
            // 
            // columnId
            // 
            this.columnId.Caption = "Ид";
            this.columnId.FieldName = "ProductId";
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.AllowEdit = false;
            this.columnId.OptionsColumn.AllowFocus = false;
            this.columnId.OptionsColumn.ReadOnly = true;
            this.columnId.Visible = true;
            this.columnId.VisibleIndex = 0;
            this.columnId.Width = 80;
            // 
            // columnName
            // 
            this.columnName.Caption = "Продукт";
            this.columnName.FieldName = "ProductName";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.OptionsColumn.AllowFocus = false;
            this.columnName.OptionsColumn.ReadOnly = true;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 1;
            this.columnName.Width = 247;
            // 
            // columnAmountEnd
            // 
            this.columnAmountEnd.Caption = "Количество конечный остаток";
            this.columnAmountEnd.FieldName = "ProductAmmountEnd";
            this.columnAmountEnd.Name = "columnAmountEnd";
            this.columnAmountEnd.OptionsColumn.AllowEdit = false;
            this.columnAmountEnd.OptionsColumn.AllowFocus = false;
            this.columnAmountEnd.OptionsColumn.ReadOnly = true;
            this.columnAmountEnd.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.columnAmountEnd.Visible = true;
            this.columnAmountEnd.VisibleIndex = 5;
            this.columnAmountEnd.Width = 247;
            // 
            // columnMeasure
            // 
            this.columnMeasure.Caption = "Ед.Изм";
            this.columnMeasure.FieldName = "ProductMeasure";
            this.columnMeasure.Name = "columnMeasure";
            this.columnMeasure.OptionsColumn.AllowEdit = false;
            this.columnMeasure.OptionsColumn.AllowFocus = false;
            this.columnMeasure.OptionsColumn.ReadOnly = true;
            this.columnMeasure.Visible = true;
            this.columnMeasure.VisibleIndex = 6;
            this.columnMeasure.Width = 92;
            // 
            // columnPrice
            // 
            this.columnPrice.Caption = "Цена";
            this.columnPrice.FieldName = "Price";
            this.columnPrice.Name = "columnPrice";
            this.columnPrice.OptionsColumn.AllowEdit = false;
            this.columnPrice.OptionsColumn.AllowFocus = false;
            this.columnPrice.OptionsColumn.ReadOnly = true;
            this.columnPrice.Visible = true;
            this.columnPrice.VisibleIndex = 7;
            // 
            // columnSum
            // 
            this.columnSum.Caption = "Сумма";
            this.columnSum.DisplayFormat.FormatString = "F2";
            this.columnSum.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnSum.FieldName = "Sum";
            this.columnSum.Name = "columnSum";
            this.columnSum.OptionsColumn.AllowEdit = false;
            this.columnSum.OptionsColumn.AllowFocus = false;
            this.columnSum.OptionsColumn.ReadOnly = true;
            this.columnSum.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.columnSum.Visible = true;
            this.columnSum.VisibleIndex = 8;
            // 
            // columnAmountStart
            // 
            this.columnAmountStart.Caption = "Начальный остаток";
            this.columnAmountStart.DisplayFormat.FormatString = "F2";
            this.columnAmountStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnAmountStart.FieldName = "ProductAmmountStart";
            this.columnAmountStart.Name = "columnAmountStart";
            this.columnAmountStart.OptionsColumn.AllowEdit = false;
            this.columnAmountStart.OptionsColumn.AllowFocus = false;
            this.columnAmountStart.OptionsColumn.ReadOnly = true;
            this.columnAmountStart.Visible = true;
            this.columnAmountStart.VisibleIndex = 2;
            // 
            // columnAmountAdded
            // 
            this.columnAmountAdded.Caption = "Приход";
            this.columnAmountAdded.DisplayFormat.FormatString = "F2";
            this.columnAmountAdded.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnAmountAdded.FieldName = "ProductAmmountAdded";
            this.columnAmountAdded.Name = "columnAmountAdded";
            this.columnAmountAdded.OptionsColumn.AllowEdit = false;
            this.columnAmountAdded.OptionsColumn.AllowFocus = false;
            this.columnAmountAdded.OptionsColumn.ReadOnly = true;
            this.columnAmountAdded.Visible = true;
            this.columnAmountAdded.VisibleIndex = 3;
            // 
            // columnAmountDeleted
            // 
            this.columnAmountDeleted.Caption = "Расход";
            this.columnAmountDeleted.DisplayFormat.FormatString = "F2";
            this.columnAmountDeleted.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnAmountDeleted.FieldName = "ProductAmmountDeleted";
            this.columnAmountDeleted.Name = "columnAmountDeleted";
            this.columnAmountDeleted.OptionsColumn.AllowEdit = false;
            this.columnAmountDeleted.OptionsColumn.AllowFocus = false;
            this.columnAmountDeleted.OptionsColumn.ReadOnly = true;
            this.columnAmountDeleted.Visible = true;
            this.columnAmountDeleted.VisibleIndex = 4;
            // 
            // lblPeriodEnd
            // 
            this.lblPeriodEnd.AutoSize = true;
            this.lblPeriodEnd.Location = new System.Drawing.Point(191, 10);
            this.lblPeriodEnd.Name = "lblPeriodEnd";
            this.lblPeriodEnd.Size = new System.Drawing.Size(22, 13);
            this.lblPeriodEnd.TabIndex = 15;
            this.lblPeriodEnd.Text = "по:";
            // 
            // btnGenerate
            // 
            this.btnGenerate.AutoSize = true;
            this.btnGenerate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnGenerate.Location = new System.Drawing.Point(591, 5);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(143, 23);
            this.btnGenerate.TabIndex = 13;
            this.btnGenerate.Text = "Сгенерировать отчет";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // dTimeEnd
            // 
            this.dTimeEnd.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dTimeEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dTimeEnd.Location = new System.Drawing.Point(219, 8);
            this.dTimeEnd.Name = "dTimeEnd";
            this.dTimeEnd.Size = new System.Drawing.Size(150, 20);
            this.dTimeEnd.TabIndex = 11;
            this.dTimeEnd.ValueChanged += new System.EventHandler(this.dTimeEnd_ValueChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // cmbWarehouse
            // 
            this.cmbWarehouse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWarehouse.FormattingEnabled = true;
            this.cmbWarehouse.Location = new System.Drawing.Point(419, 7);
            this.cmbWarehouse.Name = "cmbWarehouse";
            this.cmbWarehouse.Size = new System.Drawing.Size(166, 21);
            this.cmbWarehouse.TabIndex = 17;
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.Location = new System.Drawing.Point(375, 10);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(38, 13);
            this.lblWarehouse.TabIndex = 18;
            this.lblWarehouse.Text = "Склад";
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.Location = new System.Drawing.Point(1066, 5);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(100, 23);
            this.btnPrint.TabIndex = 24;
            this.btnPrint.Text = "Печать";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // printingSystem
            // 
            this.printingSystem.Links.AddRange(new object[] {
            this.printableComponentLink});
            // 
            // printableComponentLink
            // 
            this.printableComponentLink.Component = this.mainGridControl;
            this.printableComponentLink.PrintingSystemBase = this.printingSystem;
            this.printableComponentLink.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.printableComponentLink_CreateReportHeaderArea);
            // 
            // lblPeriodStart
            // 
            this.lblPeriodStart.AutoSize = true;
            this.lblPeriodStart.Location = new System.Drawing.Point(12, 13);
            this.lblPeriodStart.Name = "lblPeriodStart";
            this.lblPeriodStart.Size = new System.Drawing.Size(17, 13);
            this.lblPeriodStart.TabIndex = 26;
            this.lblPeriodStart.Text = "С:";
            // 
            // dTimeStart
            // 
            this.dTimeStart.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dTimeStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dTimeStart.Location = new System.Drawing.Point(35, 8);
            this.dTimeStart.Name = "dTimeStart";
            this.dTimeStart.Size = new System.Drawing.Size(150, 20);
            this.dTimeStart.TabIndex = 25;
            this.dTimeStart.ValueChanged += new System.EventHandler(this.dTimeStart_ValueChanged);
            // 
            // TransferOfGoodsReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1178, 496);
            this.Controls.Add(this.lblPeriodStart);
            this.Controls.Add(this.dTimeStart);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.lblWarehouse);
            this.Controls.Add(this.cmbWarehouse);
            this.Controls.Add(this.mainGridControl);
            this.Controls.Add(this.lblPeriodEnd);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.dTimeEnd);
            this.Name = "TransferOfGoodsReport";
            this.Text = "Движения товаров";
            this.Load += new System.EventHandler(this.RemainsOfGoodReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.detailGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public DevExpress.XtraGrid.GridControl mainGridControl;
        public DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private DevExpress.XtraGrid.Columns.GridColumn columnAmountEnd;
        private System.Windows.Forms.Label lblPeriodEnd;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.DateTimePicker dTimeEnd;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private DevExpress.XtraGrid.Columns.GridColumn columnMeasure;
        private System.Windows.Forms.ComboBox cmbWarehouse;
        private System.Windows.Forms.Label lblWarehouse;
        private System.Windows.Forms.Button btnPrint;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;
        private DevExpress.XtraPrinting.PrintableComponentLink printableComponentLink;
        private DevExpress.XtraGrid.Columns.GridColumn columnPrice;
        private DevExpress.XtraGrid.Columns.GridColumn columnSum;
        private DevExpress.XtraGrid.Columns.GridColumn columnAmountStart;
        private System.Windows.Forms.Label lblPeriodStart;
        private System.Windows.Forms.DateTimePicker dTimeStart;
        private DevExpress.XtraGrid.Columns.GridColumn columnAmountAdded;
        private DevExpress.XtraGrid.Columns.GridColumn columnAmountDeleted;
        private DevExpress.XtraGrid.Views.Grid.GridView detailGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnDocument;
        private DevExpress.XtraGrid.Columns.GridColumn columnAdd;
        private DevExpress.XtraGrid.Columns.GridColumn columnDelete;

    }
}