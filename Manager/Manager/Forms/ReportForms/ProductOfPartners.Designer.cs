﻿namespace LaRenzo.Forms.ReportForms
{
    partial class ProductOfPartners
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.detailGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnDelete = new DevExpress.XtraGrid.Columns.GridColumn();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pBarGeneration = new System.Windows.Forms.ProgressBar();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.backgroundWorkerReportGenerator = new System.ComponentModel.BackgroundWorker();
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.columnPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnFixPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.detailGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).BeginInit();
            this.SuspendLayout();
            // 
            // detailGridView
            // 
            this.detailGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnDelete,
            this.columnFixPrice});
            this.detailGridView.GridControl = this.mainGridControl;
            this.detailGridView.Name = "detailGridView";
            // 
            // columnDelete
            // 
            this.columnDelete.Caption = "Продукт";
            this.columnDelete.FieldName = "Product.Name";
            this.columnDelete.Name = "columnDelete";
            this.columnDelete.OptionsColumn.AllowEdit = false;
            this.columnDelete.OptionsColumn.AllowFocus = false;
            this.columnDelete.OptionsColumn.ReadOnly = true;
            this.columnDelete.Visible = true;
            this.columnDelete.VisibleIndex = 0;
            // 
            // mainGridControl
            // 
            this.mainGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            gridLevelNode1.LevelTemplate = this.detailGridView;
            gridLevelNode1.RelationName = "detailGridView";
            this.mainGridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.mainGridControl.Location = new System.Drawing.Point(0, 37);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(1014, 459);
            this.mainGridControl.TabIndex = 19;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView,
            this.detailGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.BestFitMaxRowCount = 10;
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnName,
            this.columnPhone});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.mainGridView.OptionsView.ShowFooter = true;
            this.mainGridView.MasterRowGetChildList += new DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventHandler(this.mainGridView_MasterRowGetChildList);
            this.mainGridView.MasterRowGetRelationName += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.mainGridView_MasterRowGetRelationName);
            this.mainGridView.MasterRowGetRelationDisplayCaption += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.mainGridView_MasterRowGetRelationDisplayCaption);
            this.mainGridView.MasterRowGetRelationCount += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventHandler(this.mainGridView_MasterRowGetRelationCount);
            // 
            // columnName
            // 
            this.columnName.Caption = "Поставщик";
            this.columnName.FieldName = "PartnerName";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.OptionsColumn.AllowFocus = false;
            this.columnName.OptionsColumn.ReadOnly = true;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 0;
            this.columnName.Width = 247;
            // 
            // pBarGeneration
            // 
            this.pBarGeneration.Location = new System.Drawing.Point(159, 8);
            this.pBarGeneration.Name = "pBarGeneration";
            this.pBarGeneration.Size = new System.Drawing.Size(194, 23);
            this.pBarGeneration.TabIndex = 18;
            // 
            // btnGenerate
            // 
            this.btnGenerate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnGenerate.Location = new System.Drawing.Point(10, 8);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(143, 23);
            this.btnGenerate.TabIndex = 17;
            this.btnGenerate.Text = "Сгенерировать отчет";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // backgroundWorkerReportGenerator
            // 
            this.backgroundWorkerReportGenerator.WorkerReportsProgress = true;
            this.backgroundWorkerReportGenerator.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerReportGenerator_DoWork);
            this.backgroundWorkerReportGenerator.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerReportGenerator_ProgressChanged);
            this.backgroundWorkerReportGenerator.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerReportGenerator_RunWorkerCompleted);
            // 
            // columnPhone
            // 
            this.columnPhone.Caption = "Телефон";
            this.columnPhone.FieldName = "PartnerPhone";
            this.columnPhone.Name = "columnPhone";
            this.columnPhone.Visible = true;
            this.columnPhone.VisibleIndex = 1;
            // 
            // columnFixPrice
            // 
            this.columnFixPrice.Caption = "Фикс. Цена";
            this.columnFixPrice.FieldName = "Product.FixPrice";
            this.columnFixPrice.Name = "columnFixPrice";
            this.columnFixPrice.Visible = true;
            this.columnFixPrice.VisibleIndex = 1;
            // 
            // ProductOfPartners
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 496);
            this.Controls.Add(this.mainGridControl);
            this.Controls.Add(this.pBarGeneration);
            this.Controls.Add(this.btnGenerate);
            this.Name = "ProductOfPartners";
            this.Text = "Поставщик -> Продукты";
            ((System.ComponentModel.ISupportInitialize)(this.detailGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar pBarGeneration;
        private System.Windows.Forms.Button btnGenerate;
        public DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView detailGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnDelete;
        public DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private System.ComponentModel.BackgroundWorker backgroundWorkerReportGenerator;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;
        private DevExpress.XtraGrid.Columns.GridColumn columnFixPrice;
        private DevExpress.XtraGrid.Columns.GridColumn columnPhone;
    }
}