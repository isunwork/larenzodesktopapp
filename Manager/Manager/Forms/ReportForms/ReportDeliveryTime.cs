﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Others.Statistics;
using LaRenzo.DataRepository.Repositories.Reports;
using LaRenzo.Properties;
using LaRenzo.Tools;

namespace LaRenzo.Forms.ReportForms
{
    public partial class ReportDeliveryTime : Form
    {
        public ReportDeliveryTime()
        {
            InitializeComponent();
            mainGridView.VertScrollVisibility = ScrollVisibility.Auto;
            mainGridView.ScrollStyle = ScrollStyleFlags.LiveVertScroll;
            ReportHelper.SetUpButtonDefault(btnGenerate);
            dateTimePickerFrom.Value = DateTime.Now.AddDays(-7);
            dateTimePickerTo.Value = DateTime.Now;
        }
        
        private void btnGenerate_Click(object sender, EventArgs e)
        {
            btnGenerate.Enabled = false;
            UpdateData();
        }
        
        private void SaleReportForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) UpdateData();
            if (e.KeyCode == Keys.Escape) Close();
        }

        private void UpdateData()
        {
            if (backgroundWorkerReportGenerator.IsBusy)
            {
                MessageBox.Show(Resources.GeneraitingAnotherReport);
            }
            else
            {
                ReportHelper.SetUpButtonDefault(btnGenerate);
                backgroundWorkerReportGenerator.RunWorkerAsync();
            }
        }


        private void backgroundWorkerReportGenerator_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            DateTime from = new DateTime();
            DateTime to = new DateTime();
            Invoke((MethodInvoker)delegate
                {
                    from = dateTimePickerFrom.Value;
                    to = dateTimePickerTo.Value;
                });

            var staticManager = Content.StatisticManager;
            e.Result = staticManager.GetDeliveryTimeReportData(from, to);
        }
        
        private void backgroundWorkerReportGenerator_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            var result = (DeliveryTimeReport)e.Result;
            labelTimeValue.Text = result.CookingTime + " минут / заказов " + result.CookingTimeCount;
            labelTimeDeliveryValue.Text = result.DeliveryTime + " минут / заказов " + result.DeliveryTimeCount;
            mainGridControl.DataSource = null;
            mainGridControl.DataSource = result.StreetsList;
            ReportHelper.SetUpButtonDefault(btnGenerate);
            btnGenerate.Enabled = true;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            printableComponentLink.CreateDocument();
            printableComponentLink.ShowPreview();
        }

        private void printableComponentLink_CreateReportHeaderArea(object sender, CreateAreaEventArgs e)
        {
            string reportName =
                $"Отчет времени приготовления и времени доставки " +
                $"c {dateTimePickerFrom.Value:dd.MM.yyyy} " +
                $"по {dateTimePickerTo.Value:dd.MM.yyyy}";

            TextBrick brick = e.Graph.DrawString(reportName, Color.Navy, new RectangleF(0, 0, 560, 60), BorderSide.None);

            brick.Font = new Font("Arial", 16);

            brick.StringFormat = new BrickStringFormat(StringAlignment.Center);
        }
    }
}
