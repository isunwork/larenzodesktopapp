﻿using System;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Warehouses;
using LaRenzo.DataRepository.Repositories.Reports.RemainsOfGoodReport;
using LaRenzo.Properties;
using LaRenzo.Tools;

namespace LaRenzo.Forms.ReportForms
{
    public partial class RemainsOfGoodReport : Form
    {
        public RemainsOfGoodReport()
        {
            InitializeComponent();
            dTimeEnd.Value = DateTime.Now;
            ReportHelper.SetUpButtonDefault(btnGenerate);
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            UpdateData();
        }

        private async void UpdateData()
        {
	        if (backgroundWorkerReportGenerator.IsBusy)
	        {
		        MessageBox.Show(Resources.GeneraitingAnotherReport);
	        }
            else
            {
                var key = ExecutionTimeLogger.Start();
                ReportHelper.SetUpButtonOnStart(btnGenerate);

                var warehouseId = cmbWarehouse.SelectedItem == null ? -1: ((Warehouse)cmbWarehouse.SelectedItem).ID;
               

                var result = await Task.Run(() =>Content.RemainsOfGoodReportManager.GetReportDate(dTimeEnd.Value, warehouseId));

                mainGridControl.DataSource = null;
                mainGridControl.DataSource = result;
                ReportHelper.SetUpButtonDefault(btnGenerate);
                ExecutionTimeLogger.Stop(key, "Отчет об остатках товаров");
            }
            
        }

       

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            /*var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            decimal delta = Convert.ToDecimal(view.GetRowCellDisplayText(e.RowHandle, columnDelta));

            if (delta < 0)
            {
                e.Appearance.BackColor = Color.Salmon;
            }*/
        }

        private void RemainsOfGoodReport_Load(object sender, EventArgs e)
        {
            var warehouses = Content.WarehouseManager.GetWarehouses();
            //warehouses.Add(new Warehouse { ID = -1, IsBase = false, Name = "Все" });
            cmbWarehouse.DataSource = warehouses.OrderBy(x=>x.ID).ToList();
            cmbWarehouse.DisplayMember = "Name";
            cmbWarehouse.ValueMember = "ID";
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            //mainGridView.ShowPrintPreview();
            printableComponentLink.CreateDocument();
            printableComponentLink.ShowPreview();
        }

        private void printableComponentLink_CreateReportHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            DevExpress.XtraPrinting.TextBrick brick;

            brick =
                e.Graph.DrawString(string.Format("Остатки товаров на складах на {0:dd.MM.yyyy hh:mm}", dTimeEnd.Value),
                                   Color.Navy, new RectangleF(0, 0, 560, 60), DevExpress.XtraPrinting.BorderSide.None);

            brick.Font = new Font("Arial", 16);

            brick.StringFormat = new DevExpress.XtraPrinting.BrickStringFormat(StringAlignment.Center);

        }
    }
}