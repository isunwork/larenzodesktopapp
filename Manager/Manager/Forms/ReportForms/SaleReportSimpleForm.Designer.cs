﻿namespace LaRenzo.Forms.ReportForms
{
    partial class SaleReportSimpleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.brandColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnSum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.isCafe = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.lblPeriodStart = new System.Windows.Forms.Label();
            this.backgroundWorkerReportGenerator = new System.ComponentModel.BackgroundWorker();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnPrint = new System.Windows.Forms.Button();
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.printableComponentLink = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            this.cmbSaleMode = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxSession = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainGridControl
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.mainGridControl, 6);
            this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.mainGridControl.Location = new System.Drawing.Point(4, 49);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Margin = new System.Windows.Forms.Padding(4);
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(1454, 557);
            this.mainGridControl.TabIndex = 3;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.brandColumn,
            this.columnId,
            this.columnName,
            this.columnPrice,
            this.columnAmount,
            this.columnSum,
            this.columnCategory,
            this.isCafe});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.mainGridView.OptionsView.BestFitMaxRowCount = 10;
            this.mainGridView.OptionsView.ShowFooter = true;
            // 
            // brandColumn
            // 
            this.brandColumn.Caption = "Бренд";
            this.brandColumn.FieldName = "Brand.Name";
            this.brandColumn.Name = "brandColumn";
            this.brandColumn.OptionsColumn.AllowEdit = false;
            this.brandColumn.OptionsColumn.AllowFocus = false;
            this.brandColumn.OptionsColumn.ReadOnly = true;
            this.brandColumn.Visible = true;
            this.brandColumn.VisibleIndex = 0;
            // 
            // columnId
            // 
            this.columnId.Caption = "Ид";
            this.columnId.FieldName = "Dish.ID";
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.AllowEdit = false;
            this.columnId.OptionsColumn.AllowFocus = false;
            this.columnId.OptionsColumn.ReadOnly = true;
            this.columnId.Visible = true;
            this.columnId.VisibleIndex = 1;
            this.columnId.Width = 63;
            // 
            // columnName
            // 
            this.columnName.Caption = "Название блюда";
            this.columnName.FieldName = "Dish.Name";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.OptionsColumn.AllowFocus = false;
            this.columnName.OptionsColumn.ReadOnly = true;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 2;
            this.columnName.Width = 63;
            // 
            // columnPrice
            // 
            this.columnPrice.Caption = "Цена";
            this.columnPrice.FieldName = "Price";
            this.columnPrice.Name = "columnPrice";
            this.columnPrice.OptionsColumn.AllowEdit = false;
            this.columnPrice.OptionsColumn.AllowFocus = false;
            this.columnPrice.OptionsColumn.ReadOnly = true;
            this.columnPrice.Visible = true;
            this.columnPrice.VisibleIndex = 3;
            this.columnPrice.Width = 60;
            // 
            // columnAmount
            // 
            this.columnAmount.Caption = "Количество";
            this.columnAmount.FieldName = "Amount";
            this.columnAmount.Name = "columnAmount";
            this.columnAmount.OptionsColumn.AllowEdit = false;
            this.columnAmount.OptionsColumn.AllowFocus = false;
            this.columnAmount.OptionsColumn.ReadOnly = true;
            this.columnAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.columnAmount.Visible = true;
            this.columnAmount.VisibleIndex = 4;
            this.columnAmount.Width = 60;
            // 
            // columnSum
            // 
            this.columnSum.Caption = "Сумма";
            this.columnSum.DisplayFormat.FormatString = "F2";
            this.columnSum.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnSum.FieldName = "Total";
            this.columnSum.Name = "columnSum";
            this.columnSum.OptionsColumn.AllowEdit = false;
            this.columnSum.OptionsColumn.AllowFocus = false;
            this.columnSum.OptionsColumn.ReadOnly = true;
            this.columnSum.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Total", "{0:F2} ₽")});
            this.columnSum.UnboundExpression = "[Amount] * [Price]";
            this.columnSum.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.columnSum.Visible = true;
            this.columnSum.VisibleIndex = 5;
            this.columnSum.Width = 60;
            // 
            // columnCategory
            // 
            this.columnCategory.Caption = "Категория";
            this.columnCategory.FieldName = "Dish.Category.Name";
            this.columnCategory.Name = "columnCategory";
            this.columnCategory.OptionsColumn.AllowEdit = false;
            this.columnCategory.OptionsColumn.AllowFocus = false;
            this.columnCategory.OptionsColumn.ReadOnly = true;
            this.columnCategory.Visible = true;
            this.columnCategory.VisibleIndex = 6;
            this.columnCategory.Width = 60;
            // 
            // isCafe
            // 
            this.isCafe.Caption = "Кафе";
            this.isCafe.FieldName = "IsCafe";
            this.isCafe.Name = "isCafe";
            this.isCafe.OptionsColumn.AllowEdit = false;
            this.isCafe.OptionsColumn.AllowFocus = false;
            this.isCafe.OptionsColumn.ReadOnly = true;
            this.isCafe.Visible = true;
            this.isCafe.VisibleIndex = 7;
            this.isCafe.Width = 72;
            // 
            // btnGenerate
            // 
            this.btnGenerate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnGenerate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnGenerate.Location = new System.Drawing.Point(870, 0);
            this.btnGenerate.Margin = new System.Windows.Forms.Padding(0);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(400, 45);
            this.btnGenerate.TabIndex = 3;
            this.btnGenerate.Text = "Сгенерировать отчет";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // lblPeriodStart
            // 
            this.lblPeriodStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPeriodStart.Location = new System.Drawing.Point(4, 0);
            this.lblPeriodStart.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPeriodStart.Name = "lblPeriodStart";
            this.lblPeriodStart.Size = new System.Drawing.Size(67, 45);
            this.lblPeriodStart.TabIndex = 7;
            this.lblPeriodStart.Text = "Сессия:";
            this.lblPeriodStart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // backgroundWorkerReportGenerator
            // 
            this.backgroundWorkerReportGenerator.WorkerReportsProgress = true;
            this.backgroundWorkerReportGenerator.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerReportGenerator_DoWork);
            this.backgroundWorkerReportGenerator.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerReportGenerator_RunWorkerCompleted);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // btnPrint
            // 
            this.btnPrint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPrint.Location = new System.Drawing.Point(1270, 0);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(0);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(192, 45);
            this.btnPrint.TabIndex = 23;
            this.btnPrint.Text = "Печать";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // printingSystem
            // 
            this.printingSystem.Links.AddRange(new object[] {
            this.printableComponentLink});
            // 
            // printableComponentLink
            // 
            this.printableComponentLink.Component = this.mainGridControl;
            this.printableComponentLink.PrintingSystemBase = this.printingSystem;
            this.printableComponentLink.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.printableComponentLink_CreateReportHeaderArea);
            // 
            // cmbSaleMode
            // 
            this.cmbSaleMode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbSaleMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSaleMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbSaleMode.FormattingEnabled = true;
            this.cmbSaleMode.ItemHeight = 29;
            this.cmbSaleMode.Items.AddRange(new object[] {
            "Все",
            "Кафе",
            "Доставка"});
            this.cmbSaleMode.Location = new System.Drawing.Point(714, 4);
            this.cmbSaleMode.Margin = new System.Windows.Forms.Padding(4);
            this.cmbSaleMode.Name = "cmbSaleMode";
            this.cmbSaleMode.Size = new System.Drawing.Size(152, 37);
            this.cmbSaleMode.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(629, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 45);
            this.label1.TabIndex = 25;
            this.label1.Text = "Продажи:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBoxSession
            // 
            this.comboBoxSession.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxSession.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSession.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBoxSession.FormattingEnabled = true;
            this.comboBoxSession.ItemHeight = 29;
            this.comboBoxSession.Items.AddRange(new object[] {
            "Все",
            "Кафе",
            "Доставка"});
            this.comboBoxSession.Location = new System.Drawing.Point(79, 4);
            this.comboBoxSession.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxSession.Name = "comboBoxSession";
            this.comboBoxSession.Size = new System.Drawing.Size(542, 37);
            this.comboBoxSession.TabIndex = 26;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 550F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.Controls.Add(this.mainGridControl, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnPrint, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnGenerate, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblPeriodStart, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.comboBoxSession, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.cmbSaleMode, 3, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1462, 610);
            this.tableLayoutPanel1.TabIndex = 28;
            // 
            // SaleReportSimpleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1462, 610);
            this.Controls.Add(this.tableLayoutPanel1);
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1480, 657);
            this.Name = "SaleReportSimpleForm";
            this.Text = "Отчёт по продажам";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SaleReportForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraGrid.GridControl mainGridControl;
        public DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private DevExpress.XtraGrid.Columns.GridColumn columnPrice;
        private DevExpress.XtraGrid.Columns.GridColumn columnAmount;
        private DevExpress.XtraGrid.Columns.GridColumn columnSum;
        private DevExpress.XtraGrid.Columns.GridColumn columnCategory;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.Label lblPeriodStart;
        private System.ComponentModel.BackgroundWorker backgroundWorkerReportGenerator;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Button btnPrint;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;
        private DevExpress.XtraPrinting.PrintableComponentLink printableComponentLink;
        private System.Windows.Forms.ComboBox cmbSaleMode;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraGrid.Columns.GridColumn isCafe;
		private DevExpress.XtraGrid.Columns.GridColumn brandColumn;
        private System.Windows.Forms.ComboBox comboBoxSession;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}