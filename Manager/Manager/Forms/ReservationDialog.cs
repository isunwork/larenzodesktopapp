﻿using System;
using System.Windows.Forms;

namespace LaRenzo.DataRepository.Forms
{
    public partial class ReservationDialog : Form
    {
        public ReservationDialog(DateTime? first, DateTime? second)
        {
            InitializeComponent();

	        if (!first.HasValue && !second.HasValue)
	        {
		        MessageBox.Show("Не удалось зарезервировать время, отчистите таблицу времени");

		        DialogResult = DialogResult.Yes;
		        //Close();
	        }
	        else
	        {
				if (first.HasValue)
		        {
			        btnFirstTime.Text = String.Format("{0:t}", first.Value.AddMinutes(1));
		        }
		        else
		        {
			        btnFirstTime.Text = "Не доступно";
			        btnFirstTime.Enabled = false;
		        }

		        if (second.HasValue)
		        {
			        btnSecondTime.Text = String.Format("{0:t}", second.Value.AddMinutes(1));
		        }
		        else
		        {
			        btnSecondTime.Text = "Не доступно";
			        btnSecondTime.Enabled = false;
		        }
	        }
        }

        private void btnFirstTime_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Yes;
            Close();
        }

        private void btnSecondTime_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.No;
            Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
