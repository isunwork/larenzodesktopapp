﻿using System;
using System.Windows.Forms;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Documents.Orders;

namespace LaRenzo.Forms.PreOrderForms
{
    public partial class EditPreOrderTime : Form
    {
        private readonly int _id;
        private readonly DateTime _dateTime;

        public EditPreOrderTime(int id, DateTime dateTime):this()
        {
            _id = id;
            dtPicker.Value = dateTime;
        }

        public EditPreOrderTime()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Content.OrderManager.UpdatePreOrderTime(_id, dtPicker.Value);

            DialogResult = DialogResult.OK;
        }
    }
}
