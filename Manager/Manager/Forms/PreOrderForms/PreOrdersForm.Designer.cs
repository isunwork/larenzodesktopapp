﻿namespace LaRenzo.Forms.PreOrderForms
{
    partial class PreOrdersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			DevExpress.XtraGrid.Columns.GridColumn columnName;
			DevExpress.XtraGrid.Columns.GridColumn columnPhone;
			DevExpress.XtraGrid.Columns.GridColumn columnDate;
			DevExpress.XtraGrid.Columns.GridColumn columnTime;
			DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
			this.gridDoBuy = new DevExpress.XtraGrid.Columns.GridColumn();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.dTimeDatePreOrders = new System.Windows.Forms.DateTimePicker();
			this.cmbShowAllAfter = new System.Windows.Forms.CheckBox();
			this.splashScreenManagerWaitFormDetails = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::LaRenzo.Forms.WaitFormGeneral), true, true);
			this.ContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.создатьЗаказToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.редактироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.DeleteMenuButton = new System.Windows.Forms.ToolStripMenuItem();
			this.восстановитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.btnEditPreOrderListOptions = new System.Windows.Forms.Button();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPreOrders = new System.Windows.Forms.TabPage();
			this.mainGridControl = new DevExpress.XtraGrid.GridControl();
			this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.columnID = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columntTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnIsDeleted = new DevExpress.XtraGrid.Columns.GridColumn();
			this.tabProductList = new System.Windows.Forms.TabPage();
			this.productGridControl = new DevExpress.XtraGrid.GridControl();
			this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.columnProductName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnNeededAmount = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnStockAmount = new DevExpress.XtraGrid.Columns.GridColumn();
			this.brandColumn = new DevExpress.XtraGrid.Columns.GridColumn();
			columnName = new DevExpress.XtraGrid.Columns.GridColumn();
			columnPhone = new DevExpress.XtraGrid.Columns.GridColumn();
			columnDate = new DevExpress.XtraGrid.Columns.GridColumn();
			columnTime = new DevExpress.XtraGrid.Columns.GridColumn();
			this.ContextMenu.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPreOrders.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
			this.tabProductList.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.productGridControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// columnName
			// 
			columnName.Caption = "Имя";
			columnName.FieldName = "Order.Name";
			columnName.Name = "columnName";
			columnName.OptionsColumn.AllowEdit = false;
			columnName.OptionsColumn.ReadOnly = true;
			columnName.Visible = true;
			columnName.VisibleIndex = 1;
			// 
			// columnPhone
			// 
			columnPhone.Caption = "Телефон";
			columnPhone.FieldName = "Order.Phone";
			columnPhone.Name = "columnPhone";
			columnPhone.OptionsColumn.AllowEdit = false;
			columnPhone.OptionsColumn.ReadOnly = true;
			columnPhone.Visible = true;
			columnPhone.VisibleIndex = 2;
			// 
			// columnDate
			// 
			columnDate.Caption = "Дата";
			columnDate.DisplayFormat.FormatString = "d";
			columnDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			columnDate.FieldName = "ReservationDateTime";
			columnDate.Name = "columnDate";
			columnDate.OptionsColumn.AllowEdit = false;
			columnDate.OptionsColumn.ReadOnly = true;
			columnDate.UnboundType = DevExpress.Data.UnboundColumnType.String;
			columnDate.Visible = true;
			columnDate.VisibleIndex = 3;
			// 
			// columnTime
			// 
			columnTime.Caption = "Время";
			columnTime.DisplayFormat.FormatString = "HH:mm";
			columnTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			columnTime.FieldName = "ReservationDateTime";
			columnTime.Name = "columnTime";
			columnTime.OptionsColumn.AllowEdit = false;
			columnTime.OptionsColumn.ReadOnly = true;
			columnTime.UnboundType = DevExpress.Data.UnboundColumnType.String;
			columnTime.Visible = true;
			columnTime.VisibleIndex = 4;
			// 
			// gridDoBuy
			// 
			this.gridDoBuy.Caption = "Нужно докупить";
			this.gridDoBuy.FieldName = "DoBuy";
			this.gridDoBuy.Name = "gridDoBuy";
			this.gridDoBuy.OptionsColumn.AllowEdit = false;
			this.gridDoBuy.OptionsColumn.ReadOnly = true;
			this.gridDoBuy.UnboundExpression = "[StockAmount] - [NeededAmount] < 0";
			this.gridDoBuy.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
			this.gridDoBuy.Visible = true;
			this.gridDoBuy.VisibleIndex = 3;
			// 
			// toolStrip1
			// 
			this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
			this.toolStrip1.Location = new System.Drawing.Point(0, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(1047, 25);
			this.toolStrip1.TabIndex = 0;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// dTimeDatePreOrders
			// 
			this.dTimeDatePreOrders.Location = new System.Drawing.Point(16, 3);
			this.dTimeDatePreOrders.Name = "dTimeDatePreOrders";
			this.dTimeDatePreOrders.Size = new System.Drawing.Size(200, 20);
			this.dTimeDatePreOrders.TabIndex = 2;
			this.dTimeDatePreOrders.ValueChanged += new System.EventHandler(this.datePreOrders_ValueChanged);
			// 
			// cmbShowAllAfter
			// 
			this.cmbShowAllAfter.AutoSize = true;
			this.cmbShowAllAfter.Location = new System.Drawing.Point(222, 3);
			this.cmbShowAllAfter.Name = "cmbShowAllAfter";
			this.cmbShowAllAfter.Size = new System.Drawing.Size(281, 17);
			this.cmbShowAllAfter.TabIndex = 3;
			this.cmbShowAllAfter.Text = "Показать все предзаказы после выбранной даты";
			this.cmbShowAllAfter.UseVisualStyleBackColor = true;
			this.cmbShowAllAfter.CheckedChanged += new System.EventHandler(this.checkBoxShowAllAfter_CheckedChanged);
			// 
			// ContextMenu
			// 
			this.ContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.создатьЗаказToolStripMenuItem,
            this.редактироватьToolStripMenuItem,
            this.DeleteMenuButton,
            this.восстановитьToolStripMenuItem});
			this.ContextMenu.Name = "ContextMenu";
			this.ContextMenu.Size = new System.Drawing.Size(155, 92);
			// 
			// создатьЗаказToolStripMenuItem
			// 
			this.создатьЗаказToolStripMenuItem.Name = "создатьЗаказToolStripMenuItem";
			this.создатьЗаказToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
			this.создатьЗаказToolStripMenuItem.Text = "Создать заказ";
			// 
			// редактироватьToolStripMenuItem
			// 
			this.редактироватьToolStripMenuItem.Name = "редактироватьToolStripMenuItem";
			this.редактироватьToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
			this.редактироватьToolStripMenuItem.Text = "Редактировать";
			// 
			// DeleteMenuButton
			// 
			this.DeleteMenuButton.Name = "DeleteMenuButton";
			this.DeleteMenuButton.Size = new System.Drawing.Size(154, 22);
			this.DeleteMenuButton.Text = "Удалить";
			this.DeleteMenuButton.Click += new System.EventHandler(this.RemovePreOrderBtn_Click);
			// 
			// восстановитьToolStripMenuItem
			// 
			this.восстановитьToolStripMenuItem.Name = "восстановитьToolStripMenuItem";
			this.восстановитьToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
			this.восстановитьToolStripMenuItem.Text = "Восстановить";
			this.восстановитьToolStripMenuItem.Click += new System.EventHandler(this.RestorePreOrderBtn_Click);
			// 
			// btnEditPreOrderListOptions
			// 
			this.btnEditPreOrderListOptions.Location = new System.Drawing.Point(510, 1);
			this.btnEditPreOrderListOptions.Name = "btnEditPreOrderListOptions";
			this.btnEditPreOrderListOptions.Size = new System.Drawing.Size(75, 23);
			this.btnEditPreOrderListOptions.TabIndex = 4;
			this.btnEditPreOrderListOptions.Text = "Изменить";
			this.btnEditPreOrderListOptions.UseVisualStyleBackColor = true;
			this.btnEditPreOrderListOptions.Click += new System.EventHandler(this.btnEditPreOrderListOptions_Click);
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPreOrders);
			this.tabControl1.Controls.Add(this.tabProductList);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 25);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(1047, 567);
			this.tabControl1.TabIndex = 5;
			// 
			// tabPreOrders
			// 
			this.tabPreOrders.Controls.Add(this.mainGridControl);
			this.tabPreOrders.Location = new System.Drawing.Point(4, 22);
			this.tabPreOrders.Name = "tabPreOrders";
			this.tabPreOrders.Padding = new System.Windows.Forms.Padding(3);
			this.tabPreOrders.Size = new System.Drawing.Size(1039, 541);
			this.tabPreOrders.TabIndex = 0;
			this.tabPreOrders.Text = "Предзаказы";
			this.tabPreOrders.UseVisualStyleBackColor = true;
			// 
			// mainGridControl
			// 
			this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
			this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainGridControl.Location = new System.Drawing.Point(3, 3);
			this.mainGridControl.MainView = this.mainGridView;
			this.mainGridControl.Name = "mainGridControl";
			this.mainGridControl.Size = new System.Drawing.Size(1033, 535);
			this.mainGridControl.TabIndex = 2;
			this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
			// 
			// mainGridView
			// 
			this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.brandColumn,
            this.columnID,
            columnName,
            columnPhone,
            columnDate,
            columnTime,
            this.columntTotalCost,
            this.columnIsDeleted});
			this.mainGridView.GridControl = this.mainGridControl;
			this.mainGridView.Name = "mainGridView";
			this.mainGridView.OptionsView.ShowFooter = true;
			this.mainGridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.mainGridView_RowStyle);
			this.mainGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.mainGridView_PopupMenuShowing);
			// 
			// columnID
			// 
			this.columnID.Caption = "Ид заказа";
			this.columnID.FieldName = "Order.ID";
			this.columnID.Name = "columnID";
			this.columnID.OptionsColumn.AllowEdit = false;
			this.columnID.OptionsColumn.ReadOnly = true;
			// 
			// columntTotalCost
			// 
			this.columntTotalCost.Caption = "Сумма (без скидки)";
			this.columntTotalCost.FieldName = "Order.TotalCostWithoutDiscount";
			this.columntTotalCost.Name = "columntTotalCost";
			this.columntTotalCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
			this.columntTotalCost.Visible = true;
			this.columntTotalCost.VisibleIndex = 5;
			// 
			// columnIsDeleted
			// 
			this.columnIsDeleted.Caption = "Заказ удален";
			this.columnIsDeleted.FieldName = "IsDeleted";
			this.columnIsDeleted.Name = "columnIsDeleted";
			this.columnIsDeleted.Visible = true;
			this.columnIsDeleted.VisibleIndex = 6;
			// 
			// tabProductList
			// 
			this.tabProductList.Controls.Add(this.productGridControl);
			this.tabProductList.Location = new System.Drawing.Point(4, 22);
			this.tabProductList.Name = "tabProductList";
			this.tabProductList.Padding = new System.Windows.Forms.Padding(3);
			this.tabProductList.Size = new System.Drawing.Size(1039, 541);
			this.tabProductList.TabIndex = 1;
			this.tabProductList.Text = "Перечень необходимых продуктов";
			this.tabProductList.UseVisualStyleBackColor = true;
			// 
			// productGridControl
			// 
			this.productGridControl.Cursor = System.Windows.Forms.Cursors.Default;
			this.productGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.productGridControl.Location = new System.Drawing.Point(3, 3);
			this.productGridControl.MainView = this.gridView1;
			this.productGridControl.Name = "productGridControl";
			this.productGridControl.Size = new System.Drawing.Size(1033, 535);
			this.productGridControl.TabIndex = 0;
			this.productGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
			// 
			// gridView1
			// 
			this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnProductName,
            this.columnNeededAmount,
            this.columnStockAmount,
            this.gridDoBuy});
			styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			styleFormatCondition1.Appearance.Options.UseBackColor = true;
			styleFormatCondition1.ApplyToRow = true;
			styleFormatCondition1.Column = this.gridDoBuy;
			styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
			styleFormatCondition1.Name = "TestSelection";
			styleFormatCondition1.Value1 = true;
			styleFormatCondition1.Value2 = true;
			this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
			this.gridView1.GridControl = this.productGridControl;
			this.gridView1.Name = "gridView1";
			// 
			// columnProductName
			// 
			this.columnProductName.Caption = "Продукт";
			this.columnProductName.FieldName = "ProductName";
			this.columnProductName.Name = "columnProductName";
			this.columnProductName.OptionsColumn.AllowEdit = false;
			this.columnProductName.OptionsColumn.ReadOnly = true;
			this.columnProductName.Visible = true;
			this.columnProductName.VisibleIndex = 0;
			// 
			// columnNeededAmount
			// 
			this.columnNeededAmount.Caption = "Потребуется";
			this.columnNeededAmount.DisplayFormat.FormatString = "F2";
			this.columnNeededAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.columnNeededAmount.FieldName = "NeededAmount";
			this.columnNeededAmount.Name = "columnNeededAmount";
			this.columnNeededAmount.OptionsColumn.AllowEdit = false;
			this.columnNeededAmount.OptionsColumn.ReadOnly = true;
			this.columnNeededAmount.Visible = true;
			this.columnNeededAmount.VisibleIndex = 1;
			// 
			// columnStockAmount
			// 
			this.columnStockAmount.Caption = "Есть в наличии";
			this.columnStockAmount.DisplayFormat.FormatString = "F2";
			this.columnStockAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.columnStockAmount.FieldName = "StockAmount";
			this.columnStockAmount.Name = "columnStockAmount";
			this.columnStockAmount.OptionsColumn.AllowEdit = false;
			this.columnStockAmount.OptionsColumn.ReadOnly = true;
			this.columnStockAmount.Visible = true;
			this.columnStockAmount.VisibleIndex = 2;
			// 
			// brandColumn
			// 
			this.brandColumn.Caption = "Бренд";
			this.brandColumn.FieldName = "Order.Brand.Name";
			this.brandColumn.Name = "brandColumn";
			this.brandColumn.Visible = true;
			this.brandColumn.VisibleIndex = 0;
			// 
			// PreOrdersForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1047, 592);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.btnEditPreOrderListOptions);
			this.Controls.Add(this.cmbShowAllAfter);
			this.Controls.Add(this.dTimeDatePreOrders);
			this.Controls.Add(this.toolStrip1);
			this.Name = "PreOrdersForm";
			this.Text = "Предзаказы";
			this.Load += new System.EventHandler(this.PreOrdersForm_Load);
			this.ContextMenu.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tabPreOrders.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
			this.tabProductList.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.productGridControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.DateTimePicker dTimeDatePreOrders;
        private System.Windows.Forms.CheckBox cmbShowAllAfter;
        private System.Windows.Forms.ContextMenuStrip ContextMenu;
        private System.Windows.Forms.ToolStripMenuItem создатьЗаказToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem редактироватьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DeleteMenuButton;
        private System.Windows.Forms.Button btnEditPreOrderListOptions;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPreOrders;
        private DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnID;
        private System.Windows.Forms.TabPage tabProductList;
        private DevExpress.XtraGrid.GridControl productGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn columnProductName;
        private DevExpress.XtraGrid.Columns.GridColumn columnNeededAmount;
        private DevExpress.XtraGrid.Columns.GridColumn columnStockAmount;
        private DevExpress.XtraGrid.Columns.GridColumn gridDoBuy;
        private DevExpress.XtraGrid.Columns.GridColumn columntTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn columnIsDeleted;
		private System.Windows.Forms.ToolStripMenuItem восстановитьToolStripMenuItem;
		private DevExpress.XtraGrid.Columns.GridColumn brandColumn;
		private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManagerWaitFormDetails;
    }
}