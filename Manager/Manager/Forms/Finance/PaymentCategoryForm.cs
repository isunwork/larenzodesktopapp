﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils.Menu;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Menu;
using DevExpress.XtraTreeList.Nodes;
using LaRenzo.DataRepository.Entities.Finance;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Finance;

namespace LaRenzo.Forms.Finance
{
    public partial class PaymentCategoryForm : Form
    {
        public PaymentCategoryForm()
        {
            InitializeComponent();
        }

        private void PaymentCategoryForm_Load(object sender, EventArgs e)
        {
            Bind();

        }

        private void Bind()
        {
            var data = Content.PaymentCategoryRepository.GetItems().ToList();
            data.Add(new PaymentCategory
            {
                ID = 0,
                Parent = -1,
                Name = "Все"
            });
            treeList1.RootValue = -1;
            treeList1.DataSource = data;
            treeList1.ParentFieldName = "Parent";
            treeList1.KeyFieldName = "ID";
            treeList1.ExpandAll();
        }

        private void treeList1_ParentChanged(object sender, EventArgs e)
        {
            
        }

        private void treeList1_AfterDragNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
        {
            var category = treeList1.GetDataRecordByNode(e.Node) as PaymentCategory;
            if (category != null) Content.PaymentCategoryRepository.UpdateItem(category);
        }

        private void treeList1_PopupMenuShowing(object sender, DevExpress.XtraTreeList.PopupMenuShowingEventArgs e)
        {
            var hitInfo = treeList1.CalcHitInfo(e.Point);

            var node = hitInfo.Node;

            e.Allow = hitInfo.HitInfoType == HitInfoType.Cell;
            e.Menu = new TreeListMenu(treeList1);

            var cat = (treeList1.GetDataRecordByNode(node) as PaymentCategory);
            
            if (cat != null && cat.ID != 0) e.Menu.Items.Add(new DXMenuItem("Удалить эту категорию", (o, args) => RemoveCategory(node)));
            e.Menu.Items.Add(new DXMenuItem("Добавить подгатегорию", (o, args) => ShowCategoryPrompt(node)));
        }

        private void ShowCategoryPrompt(TreeListNode parentNode)
        {
            var prompt = new CategoryPrompt();
            prompt.Closed += (sender, args) => AddCategory(prompt.Name, parentNode);

            prompt.ShowDialog();
        }

        private void AddCategory(string catName, TreeListNode parentNode)
        {
            var categoryDefaultName = catName;

            if (parentNode == null) return;
            var parentCategory = treeList1.GetDataRecordByNode(parentNode) as PaymentCategory;

            if (parentCategory == null) return;

            var newCategory = new PaymentCategory
            {
                Parent = parentCategory.ID,
                Name = categoryDefaultName
            };

            Content.PaymentCategoryRepository.InsertItem(newCategory);

            Bind();
        }

        private void RemoveCategory(TreeListNode node)
        {
            if (node == null) return;

            if (MessageBox.Show("Удалить категорию?", "Вы уверены?", MessageBoxButtons.YesNo) == DialogResult.No)
                return;
            
            var firstLevelNodes  = node.Nodes.Where(x => x.ParentNode == node).ToList();
            
            firstLevelNodes.ForEach(x => ChangeParentNode(x, node.ParentNode));
            
            var category = treeList1.GetDataRecordByNode(node) as PaymentCategory;
            treeList1.DeleteNode(node);

            if (category != null) Content.PaymentCategoryRepository.DeleteItem(category);

       
        }

        private void ChangeParentNode(TreeListNode currentNode, TreeListNode newParentNode)
        {
            var cat = treeList1.GetDataRecordByNode(currentNode) as PaymentCategory;
            var parCat = treeList1.GetDataRecordByNode(newParentNode) as PaymentCategory;

            if (cat != null && parCat != null)
            {
                cat.Parent = parCat.ID;
                Content.PaymentCategoryRepository.UpdateItem(cat);
                
            }

            treeList1.MoveNode(currentNode, newParentNode);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            treeList1.AppendNode(new PaymentCategory
            {
                Name = "LALALA"
            }, null);
        }
    }
}
