﻿using System;
using System.Windows.Forms;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Finance;
using LaRenzo.FormsProvider;

namespace LaRenzo.Forms.Finance
{
    public partial class PaymentListForm : Form
    {
        public PaymentListForm()
        {
            InitializeComponent();
        }

        private void PaymentForm_Load(object sender, EventArgs e)
        {
            paymentGrid.GetData = () => Content.PaymentRepository.GetItems();
            paymentGrid.LoadOrUpdateDate();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<NewPaymentForm>.ActivateForm(() =>
            {
                var form = new NewPaymentForm();
                form.FormClosed += (o, args) => paymentGrid.LoadOrUpdateDate();
                return form;
            });
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Content.PaymentCategoryRepository.CreateTestCategories();
        }
    }
}
