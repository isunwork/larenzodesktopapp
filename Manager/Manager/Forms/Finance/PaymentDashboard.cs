﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraCharts;
using LaRenzo.DataRepository.Entities.Finance;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Finance;
using Series = DevExpress.XtraCharts.Series;

namespace LaRenzo.Forms.Finance
{
    public partial class PaymentDashboard : Form
    {
        public PaymentDashboard()
        {
            InitializeComponent();

      
        }

        private void PaymentDashboard_Load(object sender, EventArgs e)
        {
            var data = Content.PaymentDashboardRepository.GetCategoriesWithSummaries();
            treeList1.DataSource = data;
            /* treeList1.ParentFieldName = "ID";
            treeList1.KeyFieldName = "ParentID";*/
        }

        

        private void treeList1_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            if (e == null || e.Node == null) return;

            var info = treeList1.GetDataRecordByNode(e.Node) as PaymentDashboardCategoryInfo;

            if (info == null) return;

            var data = Content.PaymentCategoryRepository.GetPayments(info.ID);;

            paymentGrid1.GetData = () => data;
            paymentGrid1.LoadOrUpdateDate();

            SetUpDonout(info.ID);
            MakeLines(data);
        }

        private void SetUpDonout(int categoryID)
        {

        }

        private void MakeLines(List<Payment> data)
        {
            chartControl1.Series.Clear();
            //chartControl1.DataSource = data;

            var incomeSeries = new Series("Доход", ViewType.Line);
            incomeSeries.View.Color = Color.Green;

            var outcomeSeries = new Series("Расход", ViewType.Line);
            outcomeSeries.View.Color = Color.Red;

            var totalSeries = new Series("Итого", ViewType.Area);
            totalSeries.View.Color = Color.CornflowerBlue;


            var dataByDays = data.GroupBy(x => x.Created.Date).Select(x => new
            {
                Date = x.Key,
                Income = x.Any(a => a.Total > 0) ? x.Where(a => a.Total > 0).Sum(a => a.Total) : 0,
                Outcome = x.Any(a => a.Total < 0) ? x.Where(a => a.Total<0).Sum(a => a.Total) : 0
            }).ToList();

            foreach (var day in dataByDays)
            {
                incomeSeries.Points.Add(new SeriesPoint(day.Date, day.Income));
                outcomeSeries.Points.Add(new SeriesPoint(day.Date, day.Outcome));
                totalSeries.Points.Add(new SeriesPoint(day.Date, day.Income + day.Outcome));
            }

            chartControl1.Series.Add(totalSeries);
            chartControl1.Series.Add(incomeSeries);
            chartControl1.Series.Add(outcomeSeries);

        }


    }
}
