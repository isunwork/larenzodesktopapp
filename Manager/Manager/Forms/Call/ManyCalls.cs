﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataRepository.Entities;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LaRenzo.DataRepository.Entities.Calls;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Call;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.Forms.Documents.DeliveryOrderForms;
using LaRenzo.Forms.MainFormAndIM;
using LaRenzo.TransferLibrary;

namespace LaRenzo.Forms.Call
{
	public partial class ManyCalls : Form
	{
		public ManyCalls()
		{
			InitializeComponent();
		}
        
        public void Initialyze()
        {
            gridView1.DoubleClick += async (s, e) => await gridView1_DoubleClick(s);
            
            callButton.Click += async (s, e) => await callButton_Click();
			
            testCallTimer.Tick += async (s, e) => await testCallTimer_Tick();
			
            Load += async (s, e) => await ManyCalls_Load();
	    }

		private async Task gridView1_DoubleClick(object sender)
		{
			var view = (GridView)sender;

			Point pt = view.GridControl.PointToClient(MousePosition);

			GridHitInfo info = view.CalcHitInfo(pt);
			if (view.IsDataRow(info.RowHandle))
			{
				var orderInfo = gridView1.GetRow(info.RowHandle) as CallData;

				if (orderInfo != null)
				{
					await CallManager.Instance.Call(orderInfo.PhoneNumber);
					
					var ci = new CallInfo(orderInfo)
					{
						CallData = {StatusName = "Набор номера"}
					};
				    await ci.Initialyze(Content.CallRepository.Save, Content.OrderManager.GetOrdersByPhone);

					if (ci.CurentOrders != null && ci.CurentOrders.Count > 0)
					{
						await ProcessOrder(ci);
					}
					else
					{
						var dof = new DeliveryOrderForm(ci)
						{
							CurentBrand = Content.BrandManager.GetList(new BrandFilter { BrandName = "Ларензо" }).FirstOrDefault()
                        };
					    await dof.Initialize();
						dof.ShowDialog();
					}
				}

			}
		}

		private async Task ProcessOrder(CallInfo ci)
		{
			Order order = ci.CurentOrders[0];
			
			var mode = DetailsFormModes.ViewProcessed;

			if (!order.State.DoSessionCanBeClosed)
			{
				mode = Content.UserAccessManager.GroupContainsRight(Content.ThreadLocalStorage.UserRights, AccessRightAlias.EditOrder) ? DetailsFormModes.EditOrder : DetailsFormModes.ViewByOperator;
			}

			DeliveryOrderFormOptions formOptions = null;

			if (order.PaymentMethod == PaymentMethod.OnlineCard)
			{
				formOptions = new DeliveryOrderFormOptions
				{
					SaveButtonText = "Заказы, оплаченные онлайн, редактировать запрещено",
					DoBlockSaveButton = true,
					DoBlockDishArea = true,
					DoBlockReservationArea = true
				};
			}

			await EditDetails(order.ID,ci, mode, formOptions);
		}

		public async Task EditDetails(int id, CallInfo ci, DetailsFormModes state = DetailsFormModes.EditOrder, DeliveryOrderFormOptions formOptions = null)
		{
			try
			{
				var form = new DeliveryOrderForm(state, id, formOptions, ci)
				{
					CurentBrand = Content.BrandManager.GetList().First()
				};
			    await form.Initialize();
				form.Show();
				form.Activate();
			}

			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}

		}


		private void DigitButtonClick(object sender, EventArgs e)
		{
			var bt = (Button) sender;
			numberTextBox.Text += bt.Text;
		}


		private async Task callButton_Click()
		{
			if (CallManager.Instance.NowCall != null)
			{
				CallManager.Instance.DropCall(CallManager.Instance.NowCall.CallData);
			}
			else
			{
				if (numberTextBox.Text.Length >= 7)
				{
					await CallManager.Instance.Call(numberTextBox.Text);
				}
				else
				{
					MessageBox.Show(@"Слишком короткий номер");
				}
			}
		}


		private void delButton_Click(object sender, EventArgs e)
		{
			if (numberTextBox.Text.Length > 0)
			{
				numberTextBox.Text = numberTextBox.Text.Substring(0, numberTextBox.Text.Length - 1);
			}
		}


		private void gridView1_RowStyle(object sender, RowStyleEventArgs e)
		{
			var view = sender as GridView;

			if (view != null && e.RowHandle >= 0)
			{
				// Получить заказ из Row
				var callData = view.GetRow(e.RowHandle) as CallData;

				if (callData != null)
				{
					// Цвет для строки заказа
					Color rowColor;

					switch (callData.StatusName)
					{
						case "Пропущен":
							rowColor = Color.Salmon;
							break;
						case "Завершон":
							rowColor = Color.LightGreen;
							break;
						default:
							rowColor = Color.Orange;
							break;
					}

					// Установить цвет
					e.Appearance.BackColor = rowColor;
				}
			}
		}
                
		private bool _lastCallStatus;

		private async Task testCallTimer_Tick()
		{
			await UpdateGrids();
		}

		private int _updateCount = 20;


		private async Task UpdateGrids()
		{
			if (_updateCount >= 20)
			{
				_updateCount = 0;

			    var collList = await Content.CallRepository.ListCalls();
				manyCallsGridControl.DataSource = collList.OrderByDescending(x=>x.Created);
			}

			UpdateNowCall();

			_updateCount++;
		}


		private void UpdateNowCall()
		{
			bool isNowCall = CallManager.Instance.NowCall != null;

			if (isNowCall != _lastCallStatus)
			{
				_lastCallStatus = isNowCall;

				if (isNowCall)
				{
					CallData cd = CallManager.Instance.NowCall.CallData;
					var nowList = new List<CallData> {cd};

					nowCallsGridControl.BeginInit();
					nowCallsGridControl.DataSource = nowList;
					nowCallsGridControl.EndInit();

					callButton.BackgroundImage = offPanel.BackgroundImage;
				}
				else
				{
					nowCallsGridControl.DataSource = null;
					callButton.BackgroundImage = onPanel.BackgroundImage;
				}
			}
		}

		private async Task ManyCalls_Load()
		{
			await UpdateGrids();
		}


		private void gridView2_DoubleClick(object sender, EventArgs e)
		{

		}

	}
}
