﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Repositories;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Repositories.Call;
using LaRenzo.DataRepository.Repositories.Catalogs.BlackList;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Others.GlobalOptions;
using LaRenzo.Forms.Documents.DeliveryOrderForms;
using LaRenzo.Forms.MainFormAndIM;
using LaRenzo.TransferLibrary;
using DataRepository.Entities;

namespace LaRenzo.Forms.Call
{
    public partial class InterCall : Form
    {
        public int BrandId;

        private CallInfo _callInfoLocal;

        public InterCall(int brandId)
        {
            BrandId = brandId;

            InitializeComponent();
            
            mainGridView.DoubleClick += async (s,e) => await mainGridView_DoubleClick(s);
            
            gridView1.DoubleClick += async (s,e) => await gridView1_DoubleClick(s);
            
            answerButton.Click += async (s,e) => await answerButton_Click();
        }

        private Order _lastOrder;

        public void SetCall(CallInfo callInfo)
        {

            bool isBlackNumber = Content.BlackListManager.IsNumberBlack(callInfo.CallData.PhoneNumber);

            if (isBlackNumber)
            {
                blackLabel.Visible = true;
            }

            _callInfoLocal = callInfo;

            clientNameLabel.Text = callInfo.CallData.Name;
            clientPhoneLabel.Text = callInfo.CallData.PhoneNumber;


            if (callInfo.TotalSumm < 10)
            {
                clientStatusLabel.Text = @"Новый клиент";
            }
            if (callInfo.TotalSumm < 5000)
            {
                clientStatusLabel.Text = @"Постоянный клиент";
            }
            else if (callInfo.TotalSumm < 10000)
            {
                clientStatusLabel.Text = @"Бронзовый клиент";
            }
            else if (callInfo.TotalSumm < 15000)
            {
                clientStatusLabel.Text = @"Серебрянный клиент";
            }
            else if (callInfo.TotalSumm < 25000)
            {
                clientStatusLabel.Text = @"Золотой клиент";
            }
            else
            {
                clientStatusLabel.Text = @"Премиум клиент";
            }

            if (callInfo.OldNames != null)
            {
                foreach (string str in callInfo.OldNames)
                {
                    oldNamesTextBox.Text += str + "\r\n";
                }
            }

            if (callInfo.CurentOrders != null && callInfo.CurentOrders.Count > 0)
            {
                mainGridControl.DataSource = callInfo.CurentOrders;

                _lastOrder = callInfo.CurentOrders[0];
            }


            oldGridControl.DataSource = callInfo.LastOrders;

            timer1.Enabled = true;
        }


        private async Task answerButton_Click()
        {
            CallManager.Instance.AnswerCall(_callInfoLocal.CallData);

            answerButton.Visible = false;

            if (_lastOrder == null)
            {
                var dof = new DeliveryOrderForm(_callInfoLocal)
                {
                    CurentBrand = Content.BrandManager.GetList(new BrandFilter { BrandName = "Ларензо" }).FirstOrDefault()
                };
                await dof.Initialize();
                dof.ShowDialog();
            }
            else
            {
                await ProcessOrder(_lastOrder);
            }

        }

        private async Task mainGridView_DoubleClick(object sender)
        {
            var view = (GridView)sender;

            Point pt = view.GridControl.PointToClient(MousePosition);

            GridHitInfo info = view.CalcHitInfo(pt);
            if (view.IsDataRow(info.RowHandle))
            {
                await ShowDetails(info.RowHandle, mainGridView);
            }
        }

        private async Task gridView1_DoubleClick(object sender)
        {
            var view = (GridView)sender;

            Point pt = view.GridControl.PointToClient(MousePosition);

            GridHitInfo info = view.CalcHitInfo(pt);
            if (view.IsDataRow(info.RowHandle))
            {
                await ShowDetails(info.RowHandle, gridView1);
            }
        }


        private async Task ShowDetails(int rowHandle, GridView gv)
        {
            if (gv != null)
            {
                gv.GetRowCellValue(rowHandle, columnIdCol);

                if (Content.UserManager.UserLogged != null)
                {
                    var orderInfo = mainGridView.GetRow(rowHandle) as Order;

                    if (orderInfo == null) return;

                    await ProcessOrder(orderInfo);
                }
            }
        }


        private async Task ProcessOrder(Order order)
        {
            var mode = DetailsFormModes.ViewProcessed;

            if (!order.State.DoSessionCanBeClosed)
            {
                mode = Content.UserAccessManager.GroupContainsRight(Content.ThreadLocalStorage.UserRights, AccessRightAlias.EditOrder) ? DetailsFormModes.EditOrder : DetailsFormModes.ViewByOperator;
            }

            DeliveryOrderFormOptions formOptions = null;

            if (order.PaymentMethod == PaymentMethod.OnlineCard)
            {
                formOptions = new DeliveryOrderFormOptions
                {
                    SaveButtonText = "Заказы, оплаченные онлайн, редактировать запрещено",
                    DoBlockSaveButton = true,
                    DoBlockDishArea = true,
                    DoBlockReservationArea = true
                };
            }

            await EditDetails(order.ID, mode, formOptions);
        }


        public async Task EditDetails(int id, DetailsFormModes state = DetailsFormModes.EditOrder, DeliveryOrderFormOptions formOptions = null)
        {
            try
            {
                var form = new DeliveryOrderForm(state, id, formOptions)
                {
                    CurentBrand = Content.BrandManager.GetList().First()
                };
                await form.Initialize();
                form.Show();
                form.Activate();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void mainGridView_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view != null && e.RowHandle >= 0)
            {
                // Получить заказ из Row
                var order = view.GetRow(e.RowHandle) as Order;

                if (order != null)
                {
                    // Цвет для строки заказа
                    var rowColor = new Color();

                    // Проверка телефона на чёрный лист
                    if (Content.BlackListManager.IsNumberBlack(order.Phone))
                    {
                        e.Appearance.BackColor2 = Color.DarkGray;
                    }
                    // Проверка адреса на чёрный лист
                    else if (Content.BlackListManager.IsAddressBlack(order.Street, order.House, order.Appartament))
                    {
                        e.Appearance.BackColor2 = Color.DarkGray;
                    }

                    // Присвоить цвет в зависимости от состояния заказа
                    switch (order.State.Name)
                    {
                        case "Принят":
                            rowColor = Color.Salmon;
                            break;
                        case "В пути":
                            rowColor = Color.Orange;
                            break;
                        case "Доставлен":
                            rowColor = Color.Yellow;
                            break;
                        case "Оплачен":
                            rowColor = Color.LightGreen;
                            break;
                    }

                    // Если оплата через OnlineCard
                    if (order.PaymentMethod == PaymentMethod.OnlineCard)
                    {
                        e.Appearance.GradientMode = LinearGradientMode.Horizontal;
                        e.Appearance.BackColor2 = Color.LightGreen;
                    }

                    // Время на накладные расходы кухни
                    int addCitchenTime = Content.GlobalOptionsManager.GetOverheadTime(BrandId);

                    if (order.State.Name == "Принят" && order.TimeKitchen.AddMinutes(addCitchenTime) <= DateTime.Now)
                    {
                        rowColor = Color.Aqua;
                    }

                    // Установить цвет
                    e.Appearance.BackColor = rowColor;
                }

                if (e.RowHandle == view.FocusedRowHandle)
                {
                    e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
                }
            }
        }

        private void gridView1_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view != null && e.RowHandle >= 0)
            {
                // Получить заказ из Row
                var order = view.GetRow(e.RowHandle) as Order;

                if (order != null)
                {
                    // Цвет для строки заказа
                    var rowColor = new Color();

                    // Проверка телефона на чёрный лист
                    if (Content.BlackListManager.IsNumberBlack(order.Phone))
                    {
                        e.Appearance.BackColor2 = Color.DarkGray;
                    }
                    // Проверка адреса на чёрный лист
                    else if (Content.BlackListManager.IsAddressBlack(order.Street, order.House, order.Appartament))
                    {
                        e.Appearance.BackColor2 = Color.DarkGray;
                    }

                    // Присвоить цвет в зависимости от состояния заказа
                    switch (order.State.Name)
                    {
                        case "Принят":
                            rowColor = Color.Salmon;
                            break;
                        case "В пути":
                            rowColor = Color.Orange;
                            break;
                        case "Доставлен":
                            rowColor = Color.Yellow;
                            break;
                        case "Оплачен":
                            rowColor = Color.LightGreen;
                            break;
                    }

                    // Если оплата через OnlineCard
                    if (order.PaymentMethod == PaymentMethod.OnlineCard)
                    {
                        e.Appearance.GradientMode = LinearGradientMode.Horizontal;
                        e.Appearance.BackColor2 = Color.LightGreen;
                    }

                    // Время на накладные расходы кухни
                    int addCitchenTime = Content.GlobalOptionsManager.GetOverheadTime(BrandId);

                    if (order.State.Name == "Принят" && order.TimeKitchen.AddMinutes(addCitchenTime) <= DateTime.Now)
                    {
                        rowColor = Color.Aqua;
                    }

                    // Установить цвет
                    e.Appearance.BackColor = rowColor;
                }

                if (e.RowHandle == view.FocusedRowHandle)
                {
                    e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            answerButton.Top -= 5;
            answerButton.Left -= 5;

            answerButton.Height += 10;
            answerButton.Width += 10;

            if (answerButton.Height > 101)
            {
                answerButton.Height = 71;
                answerButton.Top = 45;
                answerButton.Left = 25;
                answerButton.Width = 265;
            }

            if (CallManager.Instance.NowCall == null)
            {
                Close();
            }
        }

        private void InterCall_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                CallManager.Instance.DropCall(CallManager.Instance.NowCall.CallData);
            }
            catch
            {

            }
        }
    }
}
