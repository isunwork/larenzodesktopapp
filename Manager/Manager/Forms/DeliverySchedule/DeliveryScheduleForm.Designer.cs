﻿using LaRenzo.ControlsLib.BindFilterListControl;

namespace LaRenzo.Forms.DeliverySchedule
{
    partial class DeliveryScheduleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.spltMain = new System.Windows.Forms.SplitContainer();
            this.GroupList = new System.Windows.Forms.ListBox();
            this.GroupListFilter = new System.Windows.Forms.TextBox();
            this.toolStripGroup = new System.Windows.Forms.ToolStrip();
            this.btnAdd = new System.Windows.Forms.ToolStripButton();
            this.btnRemove = new System.Windows.Forms.ToolStripButton();
            this.btnEdit = new System.Windows.Forms.ToolStripButton();
            this.btnRefresh = new System.Windows.Forms.ToolStripButton();
            this.spltRight = new System.Windows.Forms.SplitContainer();
            this.bFilterAddresses = new LaRenzo.ControlsLib.BindFilterListControl.BindFilterList();
            this.toolStripAddress = new System.Windows.Forms.ToolStrip();
            this.btnAddAddress = new System.Windows.Forms.ToolStripButton();
            this.btndeleteAddress = new System.Windows.Forms.ToolStripButton();
            this.btnRefreshAddress = new System.Windows.Forms.ToolStripButton();
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabMonday = new System.Windows.Forms.TabPage();
            this.gridMonday = new DevExpress.XtraGrid.GridControl();
            this.gridViewMonday = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MondayTimeId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MondayTimeBegin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MondayTimeEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MondayWaiting = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MondayPointOfSale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MondayPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabTuesday = new System.Windows.Forms.TabPage();
            this.gridTuesday = new DevExpress.XtraGrid.GridControl();
            this.gridViewTuesday = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.TuesdayTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TuesdayTimeBegin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TuesdayTimeEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TuesdayWaiting = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TuesdayPointOfSale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TuesdayPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabWednesday = new System.Windows.Forms.TabPage();
            this.gridWednesday = new DevExpress.XtraGrid.GridControl();
            this.gridViewWednesday = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.WednesdayTimeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WednesdayTimeBegin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WednesdayTimeEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WednesdayTimeWaiting = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WednesdayPointOfSale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WednesdayPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabThursday = new System.Windows.Forms.TabPage();
            this.gridThursday = new DevExpress.XtraGrid.GridControl();
            this.gridViewThursday = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ThursdayTimeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ThursdayTimeBegin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ThursdayTimeEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ThursdayTimeWaiting = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ThursdayPointOfSale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ThursdayPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabFriday = new System.Windows.Forms.TabPage();
            this.gridFriday = new DevExpress.XtraGrid.GridControl();
            this.gridViewFriday = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.FridayTimeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FridayTimeBegin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FridayTimeEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FridayTimeWaiting = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FridayPointOfSale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FridayPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabSaturday = new System.Windows.Forms.TabPage();
            this.gridSaturday = new DevExpress.XtraGrid.GridControl();
            this.gridViewSaturday = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SaturdayTimeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SaturdayTimeBegin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SaturdayTimeEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SaturdayTimeWaiting = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SaturdayPointOfSale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SaturdayPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabSunday = new System.Windows.Forms.TabPage();
            this.gridSunday = new DevExpress.XtraGrid.GridControl();
            this.gridViewSunday = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SundayTimeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SundayTimeBegin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SundayTimeEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SundayTimeWaiting = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SundayPointOfSale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SundayPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.toolStripSchedule = new System.Windows.Forms.ToolStrip();
            this.btnAddSchedule = new System.Windows.Forms.ToolStripButton();
            this.btnDeleteSchedule = new System.Windows.Forms.ToolStripButton();
            this.btnRefreshSchedule = new System.Windows.Forms.ToolStripButton();
            this.comboBoxPointOfSale = new System.Windows.Forms.ToolStripComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.spltMain)).BeginInit();
            this.spltMain.Panel1.SuspendLayout();
            this.spltMain.Panel2.SuspendLayout();
            this.spltMain.SuspendLayout();
            this.toolStripGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spltRight)).BeginInit();
            this.spltRight.Panel1.SuspendLayout();
            this.spltRight.Panel2.SuspendLayout();
            this.spltRight.SuspendLayout();
            this.toolStripAddress.SuspendLayout();
            this.tabMain.SuspendLayout();
            this.tabMonday.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMonday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMonday)).BeginInit();
            this.tabTuesday.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTuesday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTuesday)).BeginInit();
            this.tabWednesday.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridWednesday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWednesday)).BeginInit();
            this.tabThursday.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridThursday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewThursday)).BeginInit();
            this.tabFriday.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridFriday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewFriday)).BeginInit();
            this.tabSaturday.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSaturday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSaturday)).BeginInit();
            this.tabSunday.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSunday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSunday)).BeginInit();
            this.toolStripSchedule.SuspendLayout();
            this.SuspendLayout();
            // 
            // spltMain
            // 
            this.spltMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltMain.Location = new System.Drawing.Point(0, 0);
            this.spltMain.Margin = new System.Windows.Forms.Padding(4);
            this.spltMain.Name = "spltMain";
            // 
            // spltMain.Panel1
            // 
            this.spltMain.Panel1.Controls.Add(this.GroupList);
            this.spltMain.Panel1.Controls.Add(this.GroupListFilter);
            this.spltMain.Panel1.Controls.Add(this.toolStripGroup);
            // 
            // spltMain.Panel2
            // 
            this.spltMain.Panel2.Controls.Add(this.spltRight);
            this.spltMain.Size = new System.Drawing.Size(972, 575);
            this.spltMain.SplitterDistance = 324;
            this.spltMain.SplitterWidth = 5;
            this.spltMain.TabIndex = 0;
            // 
            // GroupList
            // 
            this.GroupList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GroupList.FormattingEnabled = true;
            this.GroupList.ItemHeight = 16;
            this.GroupList.Location = new System.Drawing.Point(0, 53);
            this.GroupList.Margin = new System.Windows.Forms.Padding(4);
            this.GroupList.Name = "GroupList";
            this.GroupList.Size = new System.Drawing.Size(324, 522);
            this.GroupList.TabIndex = 3;
            this.GroupList.SelectedValueChanged += new System.EventHandler(this.GroupList_SelectedValueChanged);
            // 
            // GroupListFilter
            // 
            this.GroupListFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GroupListFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.GroupListFilter.Location = new System.Drawing.Point(0, 31);
            this.GroupListFilter.Margin = new System.Windows.Forms.Padding(4);
            this.GroupListFilter.Name = "GroupListFilter";
            this.GroupListFilter.Size = new System.Drawing.Size(324, 22);
            this.GroupListFilter.TabIndex = 4;
            this.GroupListFilter.TextChanged += new System.EventHandler(this.GroupListFilter_TextChanged);
            // 
            // toolStripGroup
            // 
            this.toolStripGroup.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStripGroup.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.btnRemove,
            this.btnEdit,
            this.btnRefresh});
            this.toolStripGroup.Location = new System.Drawing.Point(0, 0);
            this.toolStripGroup.Name = "toolStripGroup";
            this.toolStripGroup.Size = new System.Drawing.Size(324, 31);
            this.toolStripGroup.TabIndex = 1;
            // 
            // btnAdd
            // 
            this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdd.Image = global::LaRenzo.Properties.Resources.add_wh;
            this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(28, 28);
            this.btnAdd.Text = "Добавить группу документов";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRemove.Image = global::LaRenzo.Properties.Resources.close_wh;
            this.btnRemove.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(28, 28);
            this.btnRemove.Text = "Удалить группу документов";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEdit.Image = global::LaRenzo.Properties.Resources.edit_wh;
            this.btnEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(28, 28);
            this.btnEdit.Text = "Редактировать группу документов";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRefresh.Image = global::LaRenzo.Properties.Resources.refresh;
            this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(28, 28);
            this.btnRefresh.Text = "Обновить";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // spltRight
            // 
            this.spltRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltRight.Location = new System.Drawing.Point(0, 0);
            this.spltRight.Margin = new System.Windows.Forms.Padding(4);
            this.spltRight.Name = "spltRight";
            this.spltRight.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spltRight.Panel1
            // 
            this.spltRight.Panel1.Controls.Add(this.bFilterAddresses);
            this.spltRight.Panel1.Controls.Add(this.toolStripAddress);
            // 
            // spltRight.Panel2
            // 
            this.spltRight.Panel2.Controls.Add(this.tabMain);
            this.spltRight.Panel2.Controls.Add(this.toolStripSchedule);
            this.spltRight.Size = new System.Drawing.Size(643, 575);
            this.spltRight.SplitterDistance = 259;
            this.spltRight.SplitterWidth = 5;
            this.spltRight.TabIndex = 0;
            // 
            // bFilterAddresses
            // 
            this.bFilterAddresses.AutoSize = true;
            this.bFilterAddresses.BackColor = System.Drawing.SystemColors.Control;
            this.bFilterAddresses.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bFilterAddresses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bFilterAddresses.Filter = "";
            this.bFilterAddresses.Location = new System.Drawing.Point(0, 31);
            this.bFilterAddresses.Margin = new System.Windows.Forms.Padding(5);
            this.bFilterAddresses.Name = "bFilterAddresses";
            this.bFilterAddresses.Size = new System.Drawing.Size(643, 228);
            this.bFilterAddresses.TabIndex = 3;
            // 
            // toolStripAddress
            // 
            this.toolStripAddress.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStripAddress.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddAddress,
            this.btndeleteAddress,
            this.btnRefreshAddress});
            this.toolStripAddress.Location = new System.Drawing.Point(0, 0);
            this.toolStripAddress.Name = "toolStripAddress";
            this.toolStripAddress.Size = new System.Drawing.Size(643, 31);
            this.toolStripAddress.TabIndex = 2;
            // 
            // btnAddAddress
            // 
            this.btnAddAddress.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddAddress.Enabled = false;
            this.btnAddAddress.Image = global::LaRenzo.Properties.Resources.add_wh;
            this.btnAddAddress.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddAddress.Name = "btnAddAddress";
            this.btnAddAddress.Size = new System.Drawing.Size(28, 28);
            this.btnAddAddress.Text = "Добавить адрес";
            this.btnAddAddress.Click += new System.EventHandler(this.btnAddAddress_Click);
            // 
            // btndeleteAddress
            // 
            this.btndeleteAddress.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btndeleteAddress.Enabled = false;
            this.btndeleteAddress.Image = global::LaRenzo.Properties.Resources.close_wh;
            this.btndeleteAddress.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btndeleteAddress.Name = "btndeleteAddress";
            this.btndeleteAddress.Size = new System.Drawing.Size(28, 28);
            this.btndeleteAddress.Text = "Удалить адрес";
            this.btndeleteAddress.Click += new System.EventHandler(this.btnDeleteAddress_Click);
            // 
            // btnRefreshAddress
            // 
            this.btnRefreshAddress.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRefreshAddress.Image = global::LaRenzo.Properties.Resources.refresh;
            this.btnRefreshAddress.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefreshAddress.Name = "btnRefreshAddress";
            this.btnRefreshAddress.Size = new System.Drawing.Size(28, 28);
            this.btnRefreshAddress.Text = "Обновить";
            this.btnRefreshAddress.Click += new System.EventHandler(this.btnRefreshAddress_Click);
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tabMonday);
            this.tabMain.Controls.Add(this.tabTuesday);
            this.tabMain.Controls.Add(this.tabWednesday);
            this.tabMain.Controls.Add(this.tabThursday);
            this.tabMain.Controls.Add(this.tabFriday);
            this.tabMain.Controls.Add(this.tabSaturday);
            this.tabMain.Controls.Add(this.tabSunday);
            this.tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabMain.Location = new System.Drawing.Point(0, 31);
            this.tabMain.Margin = new System.Windows.Forms.Padding(4);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(643, 280);
            this.tabMain.TabIndex = 3;
            // 
            // tabMonday
            // 
            this.tabMonday.Controls.Add(this.gridMonday);
            this.tabMonday.Location = new System.Drawing.Point(4, 26);
            this.tabMonday.Margin = new System.Windows.Forms.Padding(4);
            this.tabMonday.Name = "tabMonday";
            this.tabMonday.Padding = new System.Windows.Forms.Padding(4);
            this.tabMonday.Size = new System.Drawing.Size(635, 250);
            this.tabMonday.TabIndex = 0;
            this.tabMonday.Text = "Понедельник";
            this.tabMonday.UseVisualStyleBackColor = true;
            // 
            // gridMonday
            // 
            this.gridMonday.CausesValidation = false;
            this.gridMonday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridMonday.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.gridMonday.Location = new System.Drawing.Point(4, 4);
            this.gridMonday.MainView = this.gridViewMonday;
            this.gridMonday.Margin = new System.Windows.Forms.Padding(4);
            this.gridMonday.Name = "gridMonday";
            this.gridMonday.Size = new System.Drawing.Size(627, 242);
            this.gridMonday.TabIndex = 1;
            this.gridMonday.Tag = "1";
            this.gridMonday.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewMonday});
            // 
            // gridViewMonday
            // 
            this.gridViewMonday.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.MondayTimeId,
            this.MondayTimeBegin,
            this.MondayTimeEnd,
            this.MondayWaiting,
            this.MondayPointOfSale,
            this.MondayPriority});
            this.gridViewMonday.GridControl = this.gridMonday;
            this.gridViewMonday.Name = "gridViewMonday";
            // 
            // MondayTimeId
            // 
            this.MondayTimeId.Caption = " ";
            this.MondayTimeId.FieldName = "ID";
            this.MondayTimeId.Name = "MondayTimeId";
            this.MondayTimeId.OptionsColumn.AllowEdit = false;
            this.MondayTimeId.OptionsColumn.AllowFocus = false;
            this.MondayTimeId.OptionsColumn.ReadOnly = true;
            // 
            // MondayTimeBegin
            // 
            this.MondayTimeBegin.Caption = "Время начала";
            this.MondayTimeBegin.FieldName = "DeliveryBegin";
            this.MondayTimeBegin.Name = "MondayTimeBegin";
            this.MondayTimeBegin.OptionsColumn.AllowEdit = false;
            this.MondayTimeBegin.OptionsColumn.AllowFocus = false;
            this.MondayTimeBegin.OptionsColumn.ReadOnly = true;
            this.MondayTimeBegin.Visible = true;
            this.MondayTimeBegin.VisibleIndex = 0;
            // 
            // MondayTimeEnd
            // 
            this.MondayTimeEnd.Caption = "Время окончания";
            this.MondayTimeEnd.FieldName = "DeliveryEnd";
            this.MondayTimeEnd.Name = "MondayTimeEnd";
            this.MondayTimeEnd.OptionsColumn.AllowEdit = false;
            this.MondayTimeEnd.OptionsColumn.AllowFocus = false;
            this.MondayTimeEnd.OptionsColumn.ReadOnly = true;
            this.MondayTimeEnd.Visible = true;
            this.MondayTimeEnd.VisibleIndex = 1;
            // 
            // MondayWaiting
            // 
            this.MondayWaiting.Caption = "Ожидание";
            this.MondayWaiting.FieldName = "DeliveryTime";
            this.MondayWaiting.Name = "MondayWaiting";
            this.MondayWaiting.OptionsColumn.AllowEdit = false;
            this.MondayWaiting.OptionsColumn.AllowFocus = false;
            this.MondayWaiting.OptionsColumn.ReadOnly = true;
            this.MondayWaiting.Visible = true;
            this.MondayWaiting.VisibleIndex = 2;
            // 
            // MondayPointOfSale
            // 
            this.MondayPointOfSale.Caption = "Точка продаж";
            this.MondayPointOfSale.FieldName = "PointOfSale.PointName";
            this.MondayPointOfSale.Name = "MondayPointOfSale";
            this.MondayPointOfSale.OptionsColumn.AllowEdit = false;
            this.MondayPointOfSale.OptionsColumn.AllowFocus = false;
            this.MondayPointOfSale.OptionsColumn.ReadOnly = true;
            this.MondayPointOfSale.Visible = true;
            this.MondayPointOfSale.VisibleIndex = 3;
            // 
            // MondayPriority
            // 
            this.MondayPriority.Caption = "Приоритет";
            this.MondayPriority.FieldName = "Priority";
            this.MondayPriority.Name = "MondayPriority";
            this.MondayPriority.OptionsColumn.AllowEdit = false;
            this.MondayPriority.OptionsColumn.AllowFocus = false;
            this.MondayPriority.OptionsColumn.ReadOnly = true;
            this.MondayPriority.Visible = true;
            this.MondayPriority.VisibleIndex = 4;
            // 
            // tabTuesday
            // 
            this.tabTuesday.Controls.Add(this.gridTuesday);
            this.tabTuesday.Location = new System.Drawing.Point(4, 26);
            this.tabTuesday.Margin = new System.Windows.Forms.Padding(4);
            this.tabTuesday.Name = "tabTuesday";
            this.tabTuesday.Padding = new System.Windows.Forms.Padding(4);
            this.tabTuesday.Size = new System.Drawing.Size(635, 250);
            this.tabTuesday.TabIndex = 1;
            this.tabTuesday.Text = "Вторник";
            this.tabTuesday.UseVisualStyleBackColor = true;
            // 
            // gridTuesday
            // 
            this.gridTuesday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridTuesday.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.gridTuesday.Location = new System.Drawing.Point(4, 4);
            this.gridTuesday.MainView = this.gridViewTuesday;
            this.gridTuesday.Margin = new System.Windows.Forms.Padding(4);
            this.gridTuesday.Name = "gridTuesday";
            this.gridTuesday.Size = new System.Drawing.Size(627, 242);
            this.gridTuesday.TabIndex = 2;
            this.gridTuesday.Tag = "2";
            this.gridTuesday.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTuesday});
            // 
            // gridViewTuesday
            // 
            this.gridViewTuesday.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.TuesdayTime,
            this.TuesdayTimeBegin,
            this.TuesdayTimeEnd,
            this.TuesdayWaiting,
            this.TuesdayPointOfSale,
            this.TuesdayPriority});
            this.gridViewTuesday.GridControl = this.gridTuesday;
            this.gridViewTuesday.Name = "gridViewTuesday";
            // 
            // TuesdayTime
            // 
            this.TuesdayTime.Caption = " ";
            this.TuesdayTime.FieldName = "ID";
            this.TuesdayTime.Name = "TuesdayTime";
            this.TuesdayTime.OptionsColumn.AllowEdit = false;
            this.TuesdayTime.OptionsColumn.AllowFocus = false;
            this.TuesdayTime.OptionsColumn.ReadOnly = true;
            // 
            // TuesdayTimeBegin
            // 
            this.TuesdayTimeBegin.Caption = "Время начала";
            this.TuesdayTimeBegin.FieldName = "DeliveryBegin";
            this.TuesdayTimeBegin.Name = "TuesdayTimeBegin";
            this.TuesdayTimeBegin.OptionsColumn.AllowEdit = false;
            this.TuesdayTimeBegin.OptionsColumn.AllowFocus = false;
            this.TuesdayTimeBegin.OptionsColumn.ReadOnly = true;
            this.TuesdayTimeBegin.Visible = true;
            this.TuesdayTimeBegin.VisibleIndex = 0;
            // 
            // TuesdayTimeEnd
            // 
            this.TuesdayTimeEnd.Caption = "Время окончания";
            this.TuesdayTimeEnd.FieldName = "DeliveryEnd";
            this.TuesdayTimeEnd.Name = "TuesdayTimeEnd";
            this.TuesdayTimeEnd.OptionsColumn.AllowEdit = false;
            this.TuesdayTimeEnd.OptionsColumn.AllowFocus = false;
            this.TuesdayTimeEnd.OptionsColumn.ReadOnly = true;
            this.TuesdayTimeEnd.Visible = true;
            this.TuesdayTimeEnd.VisibleIndex = 1;
            // 
            // TuesdayWaiting
            // 
            this.TuesdayWaiting.Caption = "Ожидание";
            this.TuesdayWaiting.FieldName = "DeliveryTime";
            this.TuesdayWaiting.Name = "TuesdayWaiting";
            this.TuesdayWaiting.OptionsColumn.AllowEdit = false;
            this.TuesdayWaiting.OptionsColumn.AllowFocus = false;
            this.TuesdayWaiting.OptionsColumn.ReadOnly = true;
            this.TuesdayWaiting.Visible = true;
            this.TuesdayWaiting.VisibleIndex = 2;
            // 
            // TuesdayPointOfSale
            // 
            this.TuesdayPointOfSale.Caption = "Точка продаж";
            this.TuesdayPointOfSale.FieldName = "PointOfSale.PointName";
            this.TuesdayPointOfSale.Name = "TuesdayPointOfSale";
            this.TuesdayPointOfSale.OptionsColumn.AllowEdit = false;
            this.TuesdayPointOfSale.OptionsColumn.AllowFocus = false;
            this.TuesdayPointOfSale.OptionsColumn.ReadOnly = true;
            this.TuesdayPointOfSale.Visible = true;
            this.TuesdayPointOfSale.VisibleIndex = 3;
            // 
            // TuesdayPriority
            // 
            this.TuesdayPriority.Caption = "Приоритет";
            this.TuesdayPriority.FieldName = "Priority";
            this.TuesdayPriority.Name = "TuesdayPriority";
            this.TuesdayPriority.OptionsColumn.AllowEdit = false;
            this.TuesdayPriority.OptionsColumn.AllowFocus = false;
            this.TuesdayPriority.OptionsColumn.ReadOnly = true;
            this.TuesdayPriority.Visible = true;
            this.TuesdayPriority.VisibleIndex = 4;
            // 
            // tabWednesday
            // 
            this.tabWednesday.Controls.Add(this.gridWednesday);
            this.tabWednesday.Location = new System.Drawing.Point(4, 26);
            this.tabWednesday.Margin = new System.Windows.Forms.Padding(4);
            this.tabWednesday.Name = "tabWednesday";
            this.tabWednesday.Padding = new System.Windows.Forms.Padding(4);
            this.tabWednesday.Size = new System.Drawing.Size(635, 250);
            this.tabWednesday.TabIndex = 2;
            this.tabWednesday.Text = "Среда";
            this.tabWednesday.UseVisualStyleBackColor = true;
            // 
            // gridWednesday
            // 
            this.gridWednesday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridWednesday.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.gridWednesday.Location = new System.Drawing.Point(4, 4);
            this.gridWednesday.MainView = this.gridViewWednesday;
            this.gridWednesday.Margin = new System.Windows.Forms.Padding(4);
            this.gridWednesday.Name = "gridWednesday";
            this.gridWednesday.Size = new System.Drawing.Size(627, 242);
            this.gridWednesday.TabIndex = 2;
            this.gridWednesday.Tag = "3";
            this.gridWednesday.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWednesday});
            // 
            // gridViewWednesday
            // 
            this.gridViewWednesday.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.WednesdayTimeID,
            this.WednesdayTimeBegin,
            this.WednesdayTimeEnd,
            this.WednesdayTimeWaiting,
            this.WednesdayPointOfSale,
            this.WednesdayPriority});
            this.gridViewWednesday.GridControl = this.gridWednesday;
            this.gridViewWednesday.Name = "gridViewWednesday";
            // 
            // WednesdayTimeID
            // 
            this.WednesdayTimeID.Caption = " ";
            this.WednesdayTimeID.FieldName = "ID";
            this.WednesdayTimeID.Name = "WednesdayTimeID";
            this.WednesdayTimeID.OptionsColumn.AllowEdit = false;
            this.WednesdayTimeID.OptionsColumn.AllowFocus = false;
            this.WednesdayTimeID.OptionsColumn.ReadOnly = true;
            // 
            // WednesdayTimeBegin
            // 
            this.WednesdayTimeBegin.Caption = "Время начала";
            this.WednesdayTimeBegin.FieldName = "DeliveryBegin";
            this.WednesdayTimeBegin.Name = "WednesdayTimeBegin";
            this.WednesdayTimeBegin.OptionsColumn.AllowEdit = false;
            this.WednesdayTimeBegin.OptionsColumn.AllowFocus = false;
            this.WednesdayTimeBegin.OptionsColumn.ReadOnly = true;
            this.WednesdayTimeBegin.Visible = true;
            this.WednesdayTimeBegin.VisibleIndex = 0;
            // 
            // WednesdayTimeEnd
            // 
            this.WednesdayTimeEnd.Caption = "Время окончания";
            this.WednesdayTimeEnd.FieldName = "DeliveryEnd";
            this.WednesdayTimeEnd.Name = "WednesdayTimeEnd";
            this.WednesdayTimeEnd.OptionsColumn.AllowEdit = false;
            this.WednesdayTimeEnd.OptionsColumn.AllowFocus = false;
            this.WednesdayTimeEnd.OptionsColumn.ReadOnly = true;
            this.WednesdayTimeEnd.Visible = true;
            this.WednesdayTimeEnd.VisibleIndex = 1;
            // 
            // WednesdayTimeWaiting
            // 
            this.WednesdayTimeWaiting.Caption = "Ожидание";
            this.WednesdayTimeWaiting.FieldName = "DeliveryTime";
            this.WednesdayTimeWaiting.Name = "WednesdayTimeWaiting";
            this.WednesdayTimeWaiting.OptionsColumn.AllowEdit = false;
            this.WednesdayTimeWaiting.OptionsColumn.AllowFocus = false;
            this.WednesdayTimeWaiting.OptionsColumn.ReadOnly = true;
            this.WednesdayTimeWaiting.Visible = true;
            this.WednesdayTimeWaiting.VisibleIndex = 2;
            // 
            // WednesdayPointOfSale
            // 
            this.WednesdayPointOfSale.Caption = "Точка продаж";
            this.WednesdayPointOfSale.FieldName = "PointOfSale.PointName";
            this.WednesdayPointOfSale.Name = "WednesdayPointOfSale";
            this.WednesdayPointOfSale.OptionsColumn.AllowEdit = false;
            this.WednesdayPointOfSale.OptionsColumn.AllowFocus = false;
            this.WednesdayPointOfSale.OptionsColumn.ReadOnly = true;
            this.WednesdayPointOfSale.Visible = true;
            this.WednesdayPointOfSale.VisibleIndex = 3;
            // 
            // WednesdayPriority
            // 
            this.WednesdayPriority.Caption = "Приоритет";
            this.WednesdayPriority.FieldName = "Priority";
            this.WednesdayPriority.Name = "WednesdayPriority";
            this.WednesdayPriority.OptionsColumn.AllowEdit = false;
            this.WednesdayPriority.OptionsColumn.AllowFocus = false;
            this.WednesdayPriority.OptionsColumn.ReadOnly = true;
            this.WednesdayPriority.Visible = true;
            this.WednesdayPriority.VisibleIndex = 4;
            // 
            // tabThursday
            // 
            this.tabThursday.Controls.Add(this.gridThursday);
            this.tabThursday.Location = new System.Drawing.Point(4, 26);
            this.tabThursday.Margin = new System.Windows.Forms.Padding(4);
            this.tabThursday.Name = "tabThursday";
            this.tabThursday.Padding = new System.Windows.Forms.Padding(4);
            this.tabThursday.Size = new System.Drawing.Size(635, 250);
            this.tabThursday.TabIndex = 3;
            this.tabThursday.Text = "Четверг";
            this.tabThursday.UseVisualStyleBackColor = true;
            // 
            // gridThursday
            // 
            this.gridThursday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridThursday.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.gridThursday.Location = new System.Drawing.Point(4, 4);
            this.gridThursday.MainView = this.gridViewThursday;
            this.gridThursday.Margin = new System.Windows.Forms.Padding(4);
            this.gridThursday.Name = "gridThursday";
            this.gridThursday.Size = new System.Drawing.Size(627, 242);
            this.gridThursday.TabIndex = 2;
            this.gridThursday.Tag = "4";
            this.gridThursday.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewThursday});
            // 
            // gridViewThursday
            // 
            this.gridViewThursday.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ThursdayTimeID,
            this.ThursdayTimeBegin,
            this.ThursdayTimeEnd,
            this.ThursdayTimeWaiting,
            this.ThursdayPointOfSale,
            this.ThursdayPriority});
            this.gridViewThursday.GridControl = this.gridThursday;
            this.gridViewThursday.Name = "gridViewThursday";
            // 
            // ThursdayTimeID
            // 
            this.ThursdayTimeID.Caption = " ";
            this.ThursdayTimeID.FieldName = "ID";
            this.ThursdayTimeID.Name = "ThursdayTimeID";
            this.ThursdayTimeID.OptionsColumn.AllowEdit = false;
            this.ThursdayTimeID.OptionsColumn.AllowFocus = false;
            this.ThursdayTimeID.OptionsColumn.ReadOnly = true;
            // 
            // ThursdayTimeBegin
            // 
            this.ThursdayTimeBegin.Caption = "Время начала";
            this.ThursdayTimeBegin.FieldName = "DeliveryBegin";
            this.ThursdayTimeBegin.Name = "ThursdayTimeBegin";
            this.ThursdayTimeBegin.OptionsColumn.AllowEdit = false;
            this.ThursdayTimeBegin.OptionsColumn.AllowFocus = false;
            this.ThursdayTimeBegin.OptionsColumn.ReadOnly = true;
            this.ThursdayTimeBegin.Visible = true;
            this.ThursdayTimeBegin.VisibleIndex = 0;
            // 
            // ThursdayTimeEnd
            // 
            this.ThursdayTimeEnd.Caption = "Время окончания";
            this.ThursdayTimeEnd.FieldName = "DeliveryEnd";
            this.ThursdayTimeEnd.Name = "ThursdayTimeEnd";
            this.ThursdayTimeEnd.OptionsColumn.AllowEdit = false;
            this.ThursdayTimeEnd.OptionsColumn.AllowFocus = false;
            this.ThursdayTimeEnd.OptionsColumn.ReadOnly = true;
            this.ThursdayTimeEnd.Visible = true;
            this.ThursdayTimeEnd.VisibleIndex = 1;
            // 
            // ThursdayTimeWaiting
            // 
            this.ThursdayTimeWaiting.Caption = "Ожидание";
            this.ThursdayTimeWaiting.FieldName = "DeliveryTime";
            this.ThursdayTimeWaiting.Name = "ThursdayTimeWaiting";
            this.ThursdayTimeWaiting.OptionsColumn.AllowEdit = false;
            this.ThursdayTimeWaiting.OptionsColumn.AllowFocus = false;
            this.ThursdayTimeWaiting.OptionsColumn.ReadOnly = true;
            this.ThursdayTimeWaiting.Visible = true;
            this.ThursdayTimeWaiting.VisibleIndex = 2;
            // 
            // ThursdayPointOfSale
            // 
            this.ThursdayPointOfSale.Caption = "Точка продаж";
            this.ThursdayPointOfSale.FieldName = "PointOfSale.PointName";
            this.ThursdayPointOfSale.Name = "ThursdayPointOfSale";
            this.ThursdayPointOfSale.OptionsColumn.AllowEdit = false;
            this.ThursdayPointOfSale.OptionsColumn.AllowFocus = false;
            this.ThursdayPointOfSale.OptionsColumn.ReadOnly = true;
            this.ThursdayPointOfSale.Visible = true;
            this.ThursdayPointOfSale.VisibleIndex = 3;
            // 
            // ThursdayPriority
            // 
            this.ThursdayPriority.Caption = "Приоритет";
            this.ThursdayPriority.FieldName = "Priority";
            this.ThursdayPriority.Name = "ThursdayPriority";
            this.ThursdayPriority.OptionsColumn.AllowEdit = false;
            this.ThursdayPriority.OptionsColumn.AllowFocus = false;
            this.ThursdayPriority.OptionsColumn.ReadOnly = true;
            this.ThursdayPriority.Visible = true;
            this.ThursdayPriority.VisibleIndex = 4;
            // 
            // tabFriday
            // 
            this.tabFriday.Controls.Add(this.gridFriday);
            this.tabFriday.Location = new System.Drawing.Point(4, 26);
            this.tabFriday.Margin = new System.Windows.Forms.Padding(4);
            this.tabFriday.Name = "tabFriday";
            this.tabFriday.Padding = new System.Windows.Forms.Padding(4);
            this.tabFriday.Size = new System.Drawing.Size(635, 250);
            this.tabFriday.TabIndex = 4;
            this.tabFriday.Text = "Пятница";
            this.tabFriday.UseVisualStyleBackColor = true;
            // 
            // gridFriday
            // 
            this.gridFriday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridFriday.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.gridFriday.Location = new System.Drawing.Point(4, 4);
            this.gridFriday.MainView = this.gridViewFriday;
            this.gridFriday.Margin = new System.Windows.Forms.Padding(4);
            this.gridFriday.Name = "gridFriday";
            this.gridFriday.Size = new System.Drawing.Size(627, 242);
            this.gridFriday.TabIndex = 2;
            this.gridFriday.Tag = "5";
            this.gridFriday.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewFriday});
            // 
            // gridViewFriday
            // 
            this.gridViewFriday.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.FridayTimeID,
            this.FridayTimeBegin,
            this.FridayTimeEnd,
            this.FridayTimeWaiting,
            this.FridayPointOfSale,
            this.FridayPriority});
            this.gridViewFriday.GridControl = this.gridFriday;
            this.gridViewFriday.Name = "gridViewFriday";
            // 
            // FridayTimeID
            // 
            this.FridayTimeID.Caption = " ";
            this.FridayTimeID.FieldName = "ID";
            this.FridayTimeID.Name = "FridayTimeID";
            this.FridayTimeID.OptionsColumn.AllowEdit = false;
            this.FridayTimeID.OptionsColumn.AllowFocus = false;
            this.FridayTimeID.OptionsColumn.ReadOnly = true;
            // 
            // FridayTimeBegin
            // 
            this.FridayTimeBegin.Caption = "Время начала";
            this.FridayTimeBegin.FieldName = "DeliveryBegin";
            this.FridayTimeBegin.Name = "FridayTimeBegin";
            this.FridayTimeBegin.OptionsColumn.AllowEdit = false;
            this.FridayTimeBegin.OptionsColumn.AllowFocus = false;
            this.FridayTimeBegin.OptionsColumn.ReadOnly = true;
            this.FridayTimeBegin.Visible = true;
            this.FridayTimeBegin.VisibleIndex = 0;
            // 
            // FridayTimeEnd
            // 
            this.FridayTimeEnd.Caption = "Время окончания";
            this.FridayTimeEnd.FieldName = "DeliveryEnd";
            this.FridayTimeEnd.Name = "FridayTimeEnd";
            this.FridayTimeEnd.OptionsColumn.AllowEdit = false;
            this.FridayTimeEnd.OptionsColumn.AllowFocus = false;
            this.FridayTimeEnd.OptionsColumn.ReadOnly = true;
            this.FridayTimeEnd.Visible = true;
            this.FridayTimeEnd.VisibleIndex = 1;
            // 
            // FridayTimeWaiting
            // 
            this.FridayTimeWaiting.Caption = "Ожидание";
            this.FridayTimeWaiting.FieldName = "DeliveryTime";
            this.FridayTimeWaiting.Name = "FridayTimeWaiting";
            this.FridayTimeWaiting.OptionsColumn.AllowEdit = false;
            this.FridayTimeWaiting.OptionsColumn.AllowFocus = false;
            this.FridayTimeWaiting.OptionsColumn.ReadOnly = true;
            this.FridayTimeWaiting.Visible = true;
            this.FridayTimeWaiting.VisibleIndex = 2;
            // 
            // FridayPointOfSale
            // 
            this.FridayPointOfSale.Caption = "Точка продаж";
            this.FridayPointOfSale.FieldName = "PointOfSale.PointName";
            this.FridayPointOfSale.Name = "FridayPointOfSale";
            this.FridayPointOfSale.OptionsColumn.AllowEdit = false;
            this.FridayPointOfSale.OptionsColumn.AllowFocus = false;
            this.FridayPointOfSale.OptionsColumn.ReadOnly = true;
            this.FridayPointOfSale.Visible = true;
            this.FridayPointOfSale.VisibleIndex = 3;
            // 
            // FridayPriority
            // 
            this.FridayPriority.Caption = "Приоритет";
            this.FridayPriority.FieldName = "Priority";
            this.FridayPriority.Name = "FridayPriority";
            this.FridayPriority.OptionsColumn.AllowEdit = false;
            this.FridayPriority.OptionsColumn.AllowFocus = false;
            this.FridayPriority.OptionsColumn.ReadOnly = true;
            this.FridayPriority.Visible = true;
            this.FridayPriority.VisibleIndex = 4;
            // 
            // tabSaturday
            // 
            this.tabSaturday.Controls.Add(this.gridSaturday);
            this.tabSaturday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabSaturday.ForeColor = System.Drawing.Color.Tomato;
            this.tabSaturday.Location = new System.Drawing.Point(4, 26);
            this.tabSaturday.Margin = new System.Windows.Forms.Padding(4);
            this.tabSaturday.Name = "tabSaturday";
            this.tabSaturday.Padding = new System.Windows.Forms.Padding(4);
            this.tabSaturday.Size = new System.Drawing.Size(635, 250);
            this.tabSaturday.TabIndex = 5;
            this.tabSaturday.Text = "Суббота";
            this.tabSaturday.UseVisualStyleBackColor = true;
            // 
            // gridSaturday
            // 
            this.gridSaturday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSaturday.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.gridSaturday.Location = new System.Drawing.Point(4, 4);
            this.gridSaturday.MainView = this.gridViewSaturday;
            this.gridSaturday.Margin = new System.Windows.Forms.Padding(4);
            this.gridSaturday.Name = "gridSaturday";
            this.gridSaturday.Size = new System.Drawing.Size(627, 242);
            this.gridSaturday.TabIndex = 2;
            this.gridSaturday.Tag = "6";
            this.gridSaturday.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSaturday});
            // 
            // gridViewSaturday
            // 
            this.gridViewSaturday.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SaturdayTimeID,
            this.SaturdayTimeBegin,
            this.SaturdayTimeEnd,
            this.SaturdayTimeWaiting,
            this.SaturdayPointOfSale,
            this.SaturdayPriority});
            this.gridViewSaturday.GridControl = this.gridSaturday;
            this.gridViewSaturday.Name = "gridViewSaturday";
            // 
            // SaturdayTimeID
            // 
            this.SaturdayTimeID.Caption = " ";
            this.SaturdayTimeID.FieldName = "ID";
            this.SaturdayTimeID.Name = "SaturdayTimeID";
            this.SaturdayTimeID.OptionsColumn.AllowEdit = false;
            this.SaturdayTimeID.OptionsColumn.AllowFocus = false;
            this.SaturdayTimeID.OptionsColumn.ReadOnly = true;
            // 
            // SaturdayTimeBegin
            // 
            this.SaturdayTimeBegin.Caption = "Время начала";
            this.SaturdayTimeBegin.FieldName = "DeliveryBegin";
            this.SaturdayTimeBegin.Name = "SaturdayTimeBegin";
            this.SaturdayTimeBegin.OptionsColumn.AllowEdit = false;
            this.SaturdayTimeBegin.OptionsColumn.AllowFocus = false;
            this.SaturdayTimeBegin.OptionsColumn.ReadOnly = true;
            this.SaturdayTimeBegin.Visible = true;
            this.SaturdayTimeBegin.VisibleIndex = 0;
            // 
            // SaturdayTimeEnd
            // 
            this.SaturdayTimeEnd.Caption = "Время окончания";
            this.SaturdayTimeEnd.FieldName = "DeliveryEnd";
            this.SaturdayTimeEnd.Name = "SaturdayTimeEnd";
            this.SaturdayTimeEnd.OptionsColumn.AllowEdit = false;
            this.SaturdayTimeEnd.OptionsColumn.AllowFocus = false;
            this.SaturdayTimeEnd.OptionsColumn.ReadOnly = true;
            this.SaturdayTimeEnd.Visible = true;
            this.SaturdayTimeEnd.VisibleIndex = 1;
            // 
            // SaturdayTimeWaiting
            // 
            this.SaturdayTimeWaiting.Caption = "Ожидание";
            this.SaturdayTimeWaiting.FieldName = "DeliveryTime";
            this.SaturdayTimeWaiting.Name = "SaturdayTimeWaiting";
            this.SaturdayTimeWaiting.OptionsColumn.AllowEdit = false;
            this.SaturdayTimeWaiting.OptionsColumn.AllowFocus = false;
            this.SaturdayTimeWaiting.OptionsColumn.ReadOnly = true;
            this.SaturdayTimeWaiting.Visible = true;
            this.SaturdayTimeWaiting.VisibleIndex = 2;
            // 
            // SaturdayPointOfSale
            // 
            this.SaturdayPointOfSale.Caption = "Точка продаж";
            this.SaturdayPointOfSale.FieldName = "PointOfSale.PointName";
            this.SaturdayPointOfSale.Name = "SaturdayPointOfSale";
            this.SaturdayPointOfSale.OptionsColumn.AllowEdit = false;
            this.SaturdayPointOfSale.OptionsColumn.AllowFocus = false;
            this.SaturdayPointOfSale.OptionsColumn.ReadOnly = true;
            this.SaturdayPointOfSale.Visible = true;
            this.SaturdayPointOfSale.VisibleIndex = 3;
            // 
            // SaturdayPriority
            // 
            this.SaturdayPriority.Caption = "Приоритет";
            this.SaturdayPriority.FieldName = "Priority";
            this.SaturdayPriority.Name = "SaturdayPriority";
            this.SaturdayPriority.OptionsColumn.AllowEdit = false;
            this.SaturdayPriority.OptionsColumn.AllowFocus = false;
            this.SaturdayPriority.OptionsColumn.ReadOnly = true;
            this.SaturdayPriority.Visible = true;
            this.SaturdayPriority.VisibleIndex = 4;
            // 
            // tabSunday
            // 
            this.tabSunday.Controls.Add(this.gridSunday);
            this.tabSunday.ForeColor = System.Drawing.Color.Tomato;
            this.tabSunday.Location = new System.Drawing.Point(4, 26);
            this.tabSunday.Margin = new System.Windows.Forms.Padding(4);
            this.tabSunday.Name = "tabSunday";
            this.tabSunday.Padding = new System.Windows.Forms.Padding(4);
            this.tabSunday.Size = new System.Drawing.Size(635, 250);
            this.tabSunday.TabIndex = 6;
            this.tabSunday.Text = "Воскресенье";
            this.tabSunday.UseVisualStyleBackColor = true;
            // 
            // gridSunday
            // 
            this.gridSunday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSunday.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.gridSunday.Location = new System.Drawing.Point(4, 4);
            this.gridSunday.MainView = this.gridViewSunday;
            this.gridSunday.Margin = new System.Windows.Forms.Padding(4);
            this.gridSunday.Name = "gridSunday";
            this.gridSunday.Size = new System.Drawing.Size(627, 242);
            this.gridSunday.TabIndex = 2;
            this.gridSunday.Tag = "7";
            this.gridSunday.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSunday});
            // 
            // gridViewSunday
            // 
            this.gridViewSunday.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SundayTimeID,
            this.SundayTimeBegin,
            this.SundayTimeEnd,
            this.SundayTimeWaiting,
            this.SundayPointOfSale,
            this.SundayPriority});
            this.gridViewSunday.GridControl = this.gridSunday;
            this.gridViewSunday.Name = "gridViewSunday";
            // 
            // SundayTimeID
            // 
            this.SundayTimeID.Caption = " ";
            this.SundayTimeID.FieldName = "ID";
            this.SundayTimeID.Name = "SundayTimeID";
            this.SundayTimeID.OptionsColumn.AllowEdit = false;
            this.SundayTimeID.OptionsColumn.AllowFocus = false;
            this.SundayTimeID.OptionsColumn.ReadOnly = true;
            // 
            // SundayTimeBegin
            // 
            this.SundayTimeBegin.Caption = "Время начала";
            this.SundayTimeBegin.FieldName = "DeliveryBegin";
            this.SundayTimeBegin.Name = "SundayTimeBegin";
            this.SundayTimeBegin.OptionsColumn.AllowEdit = false;
            this.SundayTimeBegin.OptionsColumn.AllowFocus = false;
            this.SundayTimeBegin.OptionsColumn.ReadOnly = true;
            this.SundayTimeBegin.Visible = true;
            this.SundayTimeBegin.VisibleIndex = 0;
            // 
            // SundayTimeEnd
            // 
            this.SundayTimeEnd.Caption = "Время окончания";
            this.SundayTimeEnd.FieldName = "DeliveryEnd";
            this.SundayTimeEnd.Name = "SundayTimeEnd";
            this.SundayTimeEnd.OptionsColumn.AllowEdit = false;
            this.SundayTimeEnd.OptionsColumn.AllowFocus = false;
            this.SundayTimeEnd.OptionsColumn.ReadOnly = true;
            this.SundayTimeEnd.Visible = true;
            this.SundayTimeEnd.VisibleIndex = 1;
            // 
            // SundayTimeWaiting
            // 
            this.SundayTimeWaiting.Caption = "Ожидание";
            this.SundayTimeWaiting.FieldName = "DeliveryTime";
            this.SundayTimeWaiting.Name = "SundayTimeWaiting";
            this.SundayTimeWaiting.OptionsColumn.AllowEdit = false;
            this.SundayTimeWaiting.OptionsColumn.AllowFocus = false;
            this.SundayTimeWaiting.OptionsColumn.ReadOnly = true;
            this.SundayTimeWaiting.Visible = true;
            this.SundayTimeWaiting.VisibleIndex = 2;
            // 
            // SundayPointOfSale
            // 
            this.SundayPointOfSale.Caption = "Точка продаж";
            this.SundayPointOfSale.FieldName = "PointOfSale.PointName";
            this.SundayPointOfSale.Name = "SundayPointOfSale";
            this.SundayPointOfSale.OptionsColumn.AllowEdit = false;
            this.SundayPointOfSale.OptionsColumn.AllowFocus = false;
            this.SundayPointOfSale.OptionsColumn.ReadOnly = true;
            this.SundayPointOfSale.Visible = true;
            this.SundayPointOfSale.VisibleIndex = 3;
            // 
            // SundayPriority
            // 
            this.SundayPriority.Caption = "Приоритет";
            this.SundayPriority.FieldName = "Priority";
            this.SundayPriority.Name = "SundayPriority";
            this.SundayPriority.OptionsColumn.AllowEdit = false;
            this.SundayPriority.OptionsColumn.AllowFocus = false;
            this.SundayPriority.OptionsColumn.ReadOnly = true;
            this.SundayPriority.Visible = true;
            this.SundayPriority.VisibleIndex = 4;
            // 
            // toolStripSchedule
            // 
            this.toolStripSchedule.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStripSchedule.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddSchedule,
            this.btnDeleteSchedule,
            this.btnRefreshSchedule,
            this.comboBoxPointOfSale});
            this.toolStripSchedule.Location = new System.Drawing.Point(0, 0);
            this.toolStripSchedule.Name = "toolStripSchedule";
            this.toolStripSchedule.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStripSchedule.Size = new System.Drawing.Size(643, 31);
            this.toolStripSchedule.TabIndex = 2;
            // 
            // btnAddSchedule
            // 
            this.btnAddSchedule.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddSchedule.Enabled = false;
            this.btnAddSchedule.Image = global::LaRenzo.Properties.Resources.add_wh;
            this.btnAddSchedule.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddSchedule.Name = "btnAddSchedule";
            this.btnAddSchedule.Size = new System.Drawing.Size(28, 28);
            this.btnAddSchedule.Text = "Добавить группу документов";
            this.btnAddSchedule.Click += new System.EventHandler(this.btnAddSchedule_Click);
            // 
            // btnDeleteSchedule
            // 
            this.btnDeleteSchedule.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDeleteSchedule.Enabled = false;
            this.btnDeleteSchedule.Image = global::LaRenzo.Properties.Resources.close_wh;
            this.btnDeleteSchedule.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDeleteSchedule.Name = "btnDeleteSchedule";
            this.btnDeleteSchedule.Size = new System.Drawing.Size(28, 28);
            this.btnDeleteSchedule.Text = "Удалить группу документов";
            this.btnDeleteSchedule.Click += new System.EventHandler(this.btnDeleteSchedule_Click);
            // 
            // btnRefreshSchedule
            // 
            this.btnRefreshSchedule.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRefreshSchedule.Image = global::LaRenzo.Properties.Resources.refresh;
            this.btnRefreshSchedule.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefreshSchedule.Name = "btnRefreshSchedule";
            this.btnRefreshSchedule.Size = new System.Drawing.Size(28, 28);
            this.btnRefreshSchedule.Text = "Обновить";
            this.btnRefreshSchedule.Click += new System.EventHandler(this.btnRefreshSchedule_Click);
            // 
            // comboBoxPointOfSale
            // 
            this.comboBoxPointOfSale.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxPointOfSale.Name = "comboBoxPointOfSale";
            this.comboBoxPointOfSale.Size = new System.Drawing.Size(200, 31);
            this.comboBoxPointOfSale.SelectedIndexChanged += new System.EventHandler(this.comboBoxPointOfSale_SelectedIndexChanged);
            // 
            // DeliveryScheduleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 575);
            this.Controls.Add(this.spltMain);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "DeliveryScheduleForm";
            this.Text = "Время доставки";
            this.Load += new System.EventHandler(this.DeliverySchedule_Load);
            this.spltMain.Panel1.ResumeLayout(false);
            this.spltMain.Panel1.PerformLayout();
            this.spltMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltMain)).EndInit();
            this.spltMain.ResumeLayout(false);
            this.toolStripGroup.ResumeLayout(false);
            this.toolStripGroup.PerformLayout();
            this.spltRight.Panel1.ResumeLayout(false);
            this.spltRight.Panel1.PerformLayout();
            this.spltRight.Panel2.ResumeLayout(false);
            this.spltRight.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spltRight)).EndInit();
            this.spltRight.ResumeLayout(false);
            this.toolStripAddress.ResumeLayout(false);
            this.toolStripAddress.PerformLayout();
            this.tabMain.ResumeLayout(false);
            this.tabMonday.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridMonday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMonday)).EndInit();
            this.tabTuesday.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridTuesday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTuesday)).EndInit();
            this.tabWednesday.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridWednesday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWednesday)).EndInit();
            this.tabThursday.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridThursday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewThursday)).EndInit();
            this.tabFriday.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridFriday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewFriday)).EndInit();
            this.tabSaturday.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSaturday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSaturday)).EndInit();
            this.tabSunday.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSunday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSunday)).EndInit();
            this.toolStripSchedule.ResumeLayout(false);
            this.toolStripSchedule.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer spltMain;
        private System.Windows.Forms.SplitContainer spltRight;
        private System.Windows.Forms.ToolStrip toolStripGroup;
        private System.Windows.Forms.ToolStripButton btnAdd;
        private System.Windows.Forms.ToolStripButton btnRemove;
        private System.Windows.Forms.ToolStripButton btnEdit;
        private System.Windows.Forms.ToolStripButton btnRefresh;
        private System.Windows.Forms.ToolStrip toolStripAddress;
        private System.Windows.Forms.ToolStripButton btnAddAddress;
        private System.Windows.Forms.ToolStripButton btndeleteAddress;
        private System.Windows.Forms.ToolStripButton btnRefreshAddress;
        private BindFilterList bFilterAddresses;
        private System.Windows.Forms.ToolStrip toolStripSchedule;
        private System.Windows.Forms.ToolStripButton btnAddSchedule;
        private System.Windows.Forms.ToolStripButton btnDeleteSchedule;
        private System.Windows.Forms.ToolStripButton btnRefreshSchedule;
        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabMonday;
        private DevExpress.XtraGrid.GridControl gridMonday;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewMonday;
        private DevExpress.XtraGrid.Columns.GridColumn MondayTimeBegin;
        private DevExpress.XtraGrid.Columns.GridColumn MondayTimeEnd;
        private DevExpress.XtraGrid.Columns.GridColumn MondayWaiting;
        private System.Windows.Forms.TabPage tabTuesday;
        private DevExpress.XtraGrid.GridControl gridTuesday;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewTuesday;
        private DevExpress.XtraGrid.Columns.GridColumn TuesdayTimeBegin;
        private DevExpress.XtraGrid.Columns.GridColumn TuesdayTimeEnd;
        private DevExpress.XtraGrid.Columns.GridColumn TuesdayWaiting;
        private System.Windows.Forms.TabPage tabWednesday;
        private DevExpress.XtraGrid.GridControl gridWednesday;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWednesday;
        private DevExpress.XtraGrid.Columns.GridColumn WednesdayTimeBegin;
        private DevExpress.XtraGrid.Columns.GridColumn WednesdayTimeEnd;
        private DevExpress.XtraGrid.Columns.GridColumn WednesdayTimeWaiting;
        private System.Windows.Forms.TabPage tabThursday;
        private DevExpress.XtraGrid.GridControl gridThursday;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewThursday;
        private DevExpress.XtraGrid.Columns.GridColumn ThursdayTimeBegin;
        private DevExpress.XtraGrid.Columns.GridColumn ThursdayTimeEnd;
        private DevExpress.XtraGrid.Columns.GridColumn ThursdayTimeWaiting;
        private System.Windows.Forms.TabPage tabFriday;
        private DevExpress.XtraGrid.GridControl gridFriday;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewFriday;
        private DevExpress.XtraGrid.Columns.GridColumn FridayTimeBegin;
        private DevExpress.XtraGrid.Columns.GridColumn FridayTimeEnd;
        private DevExpress.XtraGrid.Columns.GridColumn FridayTimeWaiting;
        private System.Windows.Forms.TabPage tabSaturday;
        private DevExpress.XtraGrid.GridControl gridSaturday;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSaturday;
        private DevExpress.XtraGrid.Columns.GridColumn SaturdayTimeBegin;
        private DevExpress.XtraGrid.Columns.GridColumn SaturdayTimeEnd;
        private DevExpress.XtraGrid.Columns.GridColumn SaturdayTimeWaiting;
        private System.Windows.Forms.TabPage tabSunday;
        private DevExpress.XtraGrid.GridControl gridSunday;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSunday;
        private DevExpress.XtraGrid.Columns.GridColumn SundayTimeBegin;
        private DevExpress.XtraGrid.Columns.GridColumn SundayTimeEnd;
        private DevExpress.XtraGrid.Columns.GridColumn SundayTimeWaiting;
        private System.Windows.Forms.TextBox GroupListFilter;
        private System.Windows.Forms.ListBox GroupList;
        private DevExpress.XtraGrid.Columns.GridColumn MondayTimeId;
        private DevExpress.XtraGrid.Columns.GridColumn TuesdayTime;
        private DevExpress.XtraGrid.Columns.GridColumn WednesdayTimeID;
        private DevExpress.XtraGrid.Columns.GridColumn ThursdayTimeID;
        private DevExpress.XtraGrid.Columns.GridColumn FridayTimeID;
        private DevExpress.XtraGrid.Columns.GridColumn SaturdayTimeID;
        private DevExpress.XtraGrid.Columns.GridColumn SundayTimeID;
        private System.Windows.Forms.ToolStripComboBox comboBoxPointOfSale;
        private DevExpress.XtraGrid.Columns.GridColumn MondayPointOfSale;
        private DevExpress.XtraGrid.Columns.GridColumn MondayPriority;
        private DevExpress.XtraGrid.Columns.GridColumn TuesdayPointOfSale;
        private DevExpress.XtraGrid.Columns.GridColumn TuesdayPriority;
        private DevExpress.XtraGrid.Columns.GridColumn WednesdayPointOfSale;
        private DevExpress.XtraGrid.Columns.GridColumn WednesdayPriority;
        private DevExpress.XtraGrid.Columns.GridColumn ThursdayPointOfSale;
        private DevExpress.XtraGrid.Columns.GridColumn ThursdayPriority;
        private DevExpress.XtraGrid.Columns.GridColumn FridayPointOfSale;
        private DevExpress.XtraGrid.Columns.GridColumn FridayPriority;
        private DevExpress.XtraGrid.Columns.GridColumn SaturdayPointOfSale;
        private DevExpress.XtraGrid.Columns.GridColumn SaturdayPriority;
        private DevExpress.XtraGrid.Columns.GridColumn SundayPointOfSale;
        private DevExpress.XtraGrid.Columns.GridColumn SundayPriority;
    }
}