﻿namespace LaRenzo.Forms.DeliverySchedule
{
    partial class MultiEditScheduleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.lblWaiting = new System.Windows.Forms.Label();
            this.lblDeliveryEnd = new System.Windows.Forms.Label();
            this.lblDeliveryBegin = new System.Windows.Forms.Label();
            this.tEditWaiting = new DevExpress.XtraEditors.TimeEdit();
            this.tEditDeliveryEnd = new DevExpress.XtraEditors.TimeEdit();
            this.tEditDeliveryBegin = new DevExpress.XtraEditors.TimeEdit();
            this.tipError = new DevExpress.Utils.ToolTipController(this.components);
            this.flowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.chkBoxMonday = new System.Windows.Forms.CheckBox();
            this.chkBoxTuesday = new System.Windows.Forms.CheckBox();
            this.chkBoxWednesday = new System.Windows.Forms.CheckBox();
            this.chkBoxThursday = new System.Windows.Forms.CheckBox();
            this.chkBoxFriday = new System.Windows.Forms.CheckBox();
            this.chkBoxSaturday = new System.Windows.Forms.CheckBox();
            this.chkBoxSunday = new System.Windows.Forms.CheckBox();
            this.pnlMiddle = new System.Windows.Forms.Panel();
            this.numPriority = new System.Windows.Forms.NumericUpDown();
            this.lblPriority = new System.Windows.Forms.Label();
            this.comboBoxPointOfSale = new System.Windows.Forms.ComboBox();
            this.pnlBottom.SuspendLayout();
            this.pnlTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tEditWaiting.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tEditDeliveryEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tEditDeliveryBegin.Properties)).BeginInit();
            this.flowLayoutPanel.SuspendLayout();
            this.pnlMiddle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPriority)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.btnSave);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 151);
            this.pnlBottom.Margin = new System.Windows.Forms.Padding(4);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(536, 29);
            this.pnlBottom.TabIndex = 4;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.Location = new System.Drawing.Point(302, 0);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(116, 27);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.Location = new System.Drawing.Point(418, 0);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(116, 27);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.lblWaiting);
            this.pnlTop.Controls.Add(this.lblDeliveryEnd);
            this.pnlTop.Controls.Add(this.lblDeliveryBegin);
            this.pnlTop.Controls.Add(this.tEditWaiting);
            this.pnlTop.Controls.Add(this.tEditDeliveryEnd);
            this.pnlTop.Controls.Add(this.tEditDeliveryBegin);
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Margin = new System.Windows.Forms.Padding(4);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(536, 34);
            this.pnlTop.TabIndex = 5;
            // 
            // lblWaiting
            // 
            this.lblWaiting.AutoSize = true;
            this.lblWaiting.Location = new System.Drawing.Point(285, 7);
            this.lblWaiting.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblWaiting.Name = "lblWaiting";
            this.lblWaiting.Size = new System.Drawing.Size(116, 17);
            this.lblWaiting.TabIndex = 13;
            this.lblWaiting.Text = "время доставки:";
            // 
            // lblDeliveryEnd
            // 
            this.lblDeliveryEnd.AutoSize = true;
            this.lblDeliveryEnd.Location = new System.Drawing.Point(140, 7);
            this.lblDeliveryEnd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDeliveryEnd.Name = "lblDeliveryEnd";
            this.lblDeliveryEnd.Size = new System.Drawing.Size(24, 17);
            this.lblDeliveryEnd.TabIndex = 12;
            this.lblDeliveryEnd.Text = "по";
            // 
            // lblDeliveryBegin
            // 
            this.lblDeliveryBegin.AutoSize = true;
            this.lblDeliveryBegin.Location = new System.Drawing.Point(4, 7);
            this.lblDeliveryBegin.Margin = new System.Windows.Forms.Padding(4);
            this.lblDeliveryBegin.Name = "lblDeliveryBegin";
            this.lblDeliveryBegin.Size = new System.Drawing.Size(15, 17);
            this.lblDeliveryBegin.TabIndex = 11;
            this.lblDeliveryBegin.Text = "с";
            // 
            // tEditWaiting
            // 
            this.tEditWaiting.EditValue = new System.DateTime(2013, 4, 30, 0, 0, 0, 0);
            this.tEditWaiting.Location = new System.Drawing.Point(415, 4);
            this.tEditWaiting.Margin = new System.Windows.Forms.Padding(4);
            this.tEditWaiting.Name = "tEditWaiting";
            // 
            // 
            // 
            this.tEditWaiting.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.tEditWaiting.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.tEditWaiting.Properties.DisplayFormat.FormatString = "HH:mm:ss";
            this.tEditWaiting.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tEditWaiting.Properties.EditFormat.FormatString = "HH:mm:ss";
            this.tEditWaiting.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tEditWaiting.Properties.EditValueChangedDelay = 2500;
            this.tEditWaiting.Properties.Mask.EditMask = "HH:mm:ss";
            this.tEditWaiting.Size = new System.Drawing.Size(113, 22);
            this.tEditWaiting.TabIndex = 10;
            // 
            // tEditDeliveryEnd
            // 
            this.tEditDeliveryEnd.EditValue = new System.DateTime(2013, 4, 30, 0, 0, 0, 0);
            this.tEditDeliveryEnd.Location = new System.Drawing.Point(169, 4);
            this.tEditDeliveryEnd.Margin = new System.Windows.Forms.Padding(4);
            this.tEditDeliveryEnd.Name = "tEditDeliveryEnd";
            // 
            // 
            // 
            this.tEditDeliveryEnd.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.tEditDeliveryEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.tEditDeliveryEnd.Properties.DisplayFormat.FormatString = "HH:mm:ss";
            this.tEditDeliveryEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tEditDeliveryEnd.Properties.EditFormat.FormatString = "HH:mm:ss";
            this.tEditDeliveryEnd.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tEditDeliveryEnd.Properties.EditValueChangedDelay = 2500;
            this.tEditDeliveryEnd.Properties.Mask.EditMask = "HH:mm:ss";
            this.tEditDeliveryEnd.Size = new System.Drawing.Size(101, 22);
            this.tEditDeliveryEnd.TabIndex = 9;
            // 
            // tEditDeliveryBegin
            // 
            this.tEditDeliveryBegin.CausesValidation = false;
            this.tEditDeliveryBegin.EditValue = new System.DateTime(2013, 4, 30, 0, 0, 0, 0);
            this.tEditDeliveryBegin.Location = new System.Drawing.Point(31, 4);
            this.tEditDeliveryBegin.Margin = new System.Windows.Forms.Padding(4);
            this.tEditDeliveryBegin.Name = "tEditDeliveryBegin";
            // 
            // 
            // 
            this.tEditDeliveryBegin.Properties.AllowFocused = false;
            this.tEditDeliveryBegin.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.tEditDeliveryBegin.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.tEditDeliveryBegin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.tEditDeliveryBegin.Properties.DisplayFormat.FormatString = "HH:mm:ss";
            this.tEditDeliveryBegin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tEditDeliveryBegin.Properties.EditFormat.FormatString = "HH:mm:ss";
            this.tEditDeliveryBegin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tEditDeliveryBegin.Properties.EditValueChangedDelay = 2500;
            this.tEditDeliveryBegin.Properties.Mask.EditMask = "HH:mm:ss";
            this.tEditDeliveryBegin.Size = new System.Drawing.Size(101, 22);
            this.tEditDeliveryBegin.TabIndex = 8;
            // 
            // tipError
            // 
            this.tipError.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tipError.Appearance.Options.UseFont = true;
            this.tipError.AppearanceTitle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tipError.AppearanceTitle.ForeColor = System.Drawing.Color.OrangeRed;
            this.tipError.AppearanceTitle.Options.UseFont = true;
            this.tipError.AppearanceTitle.Options.UseForeColor = true;
            this.tipError.ShowBeak = true;
            // 
            // flowLayoutPanel
            // 
            this.flowLayoutPanel.Controls.Add(this.chkBoxMonday);
            this.flowLayoutPanel.Controls.Add(this.chkBoxTuesday);
            this.flowLayoutPanel.Controls.Add(this.chkBoxWednesday);
            this.flowLayoutPanel.Controls.Add(this.chkBoxThursday);
            this.flowLayoutPanel.Controls.Add(this.chkBoxFriday);
            this.flowLayoutPanel.Controls.Add(this.chkBoxSaturday);
            this.flowLayoutPanel.Controls.Add(this.chkBoxSunday);
            this.flowLayoutPanel.Location = new System.Drawing.Point(0, 82);
            this.flowLayoutPanel.Margin = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel.Name = "flowLayoutPanel";
            this.flowLayoutPanel.Size = new System.Drawing.Size(536, 63);
            this.flowLayoutPanel.TabIndex = 6;
            // 
            // chkBoxMonday
            // 
            this.chkBoxMonday.AutoSize = true;
            this.chkBoxMonday.Location = new System.Drawing.Point(4, 4);
            this.chkBoxMonday.Margin = new System.Windows.Forms.Padding(4);
            this.chkBoxMonday.Name = "chkBoxMonday";
            this.chkBoxMonday.Size = new System.Drawing.Size(118, 21);
            this.chkBoxMonday.TabIndex = 0;
            this.chkBoxMonday.Tag = "1";
            this.chkBoxMonday.Text = "Понедельник";
            this.chkBoxMonday.UseVisualStyleBackColor = true;
            // 
            // chkBoxTuesday
            // 
            this.chkBoxTuesday.AutoSize = true;
            this.chkBoxTuesday.Location = new System.Drawing.Point(130, 4);
            this.chkBoxTuesday.Margin = new System.Windows.Forms.Padding(4);
            this.chkBoxTuesday.Name = "chkBoxTuesday";
            this.chkBoxTuesday.Size = new System.Drawing.Size(85, 21);
            this.chkBoxTuesday.TabIndex = 1;
            this.chkBoxTuesday.Tag = "2";
            this.chkBoxTuesday.Text = "Вторник";
            this.chkBoxTuesday.UseVisualStyleBackColor = true;
            // 
            // chkBoxWednesday
            // 
            this.chkBoxWednesday.AutoSize = true;
            this.chkBoxWednesday.Location = new System.Drawing.Point(223, 4);
            this.chkBoxWednesday.Margin = new System.Windows.Forms.Padding(4);
            this.chkBoxWednesday.Name = "chkBoxWednesday";
            this.chkBoxWednesday.Size = new System.Drawing.Size(71, 21);
            this.chkBoxWednesday.TabIndex = 2;
            this.chkBoxWednesday.Tag = "3";
            this.chkBoxWednesday.Text = "Среда";
            this.chkBoxWednesday.UseVisualStyleBackColor = true;
            // 
            // chkBoxThursday
            // 
            this.chkBoxThursday.AutoSize = true;
            this.chkBoxThursday.Location = new System.Drawing.Point(302, 4);
            this.chkBoxThursday.Margin = new System.Windows.Forms.Padding(4);
            this.chkBoxThursday.Name = "chkBoxThursday";
            this.chkBoxThursday.Size = new System.Drawing.Size(83, 21);
            this.chkBoxThursday.TabIndex = 3;
            this.chkBoxThursday.Tag = "4";
            this.chkBoxThursday.Text = "Четверг";
            this.chkBoxThursday.UseVisualStyleBackColor = true;
            // 
            // chkBoxFriday
            // 
            this.chkBoxFriday.AutoSize = true;
            this.chkBoxFriday.Location = new System.Drawing.Point(393, 4);
            this.chkBoxFriday.Margin = new System.Windows.Forms.Padding(4);
            this.chkBoxFriday.Name = "chkBoxFriday";
            this.chkBoxFriday.Size = new System.Drawing.Size(87, 21);
            this.chkBoxFriday.TabIndex = 4;
            this.chkBoxFriday.Tag = "5";
            this.chkBoxFriday.Text = "Пятница";
            this.chkBoxFriday.UseVisualStyleBackColor = true;
            // 
            // chkBoxSaturday
            // 
            this.chkBoxSaturday.AutoSize = true;
            this.chkBoxSaturday.ForeColor = System.Drawing.Color.Tomato;
            this.chkBoxSaturday.Location = new System.Drawing.Point(4, 33);
            this.chkBoxSaturday.Margin = new System.Windows.Forms.Padding(4, 4, 40, 4);
            this.chkBoxSaturday.Name = "chkBoxSaturday";
            this.chkBoxSaturday.Size = new System.Drawing.Size(85, 21);
            this.chkBoxSaturday.TabIndex = 5;
            this.chkBoxSaturday.Tag = "6";
            this.chkBoxSaturday.Text = "Суббота";
            this.chkBoxSaturday.UseVisualStyleBackColor = true;
            // 
            // chkBoxSunday
            // 
            this.chkBoxSunday.AutoSize = true;
            this.chkBoxSunday.ForeColor = System.Drawing.Color.Tomato;
            this.chkBoxSunday.Location = new System.Drawing.Point(133, 33);
            this.chkBoxSunday.Margin = new System.Windows.Forms.Padding(4);
            this.chkBoxSunday.Name = "chkBoxSunday";
            this.chkBoxSunday.Size = new System.Drawing.Size(115, 21);
            this.chkBoxSunday.TabIndex = 6;
            this.chkBoxSunday.Tag = "7";
            this.chkBoxSunday.Text = "Воскресенье";
            this.chkBoxSunday.UseVisualStyleBackColor = true;
            // 
            // pnlMiddle
            // 
            this.pnlMiddle.Controls.Add(this.numPriority);
            this.pnlMiddle.Controls.Add(this.lblPriority);
            this.pnlMiddle.Controls.Add(this.comboBoxPointOfSale);
            this.pnlMiddle.Location = new System.Drawing.Point(0, 41);
            this.pnlMiddle.Name = "pnlMiddle";
            this.pnlMiddle.Size = new System.Drawing.Size(536, 34);
            this.pnlMiddle.TabIndex = 7;
            // 
            // numPriority
            // 
            this.numPriority.Location = new System.Drawing.Point(414, 4);
            this.numPriority.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPriority.Name = "numPriority";
            this.numPriority.Size = new System.Drawing.Size(115, 22);
            this.numPriority.TabIndex = 2;
            this.numPriority.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblPriority
            // 
            this.lblPriority.AutoSize = true;
            this.lblPriority.Location = new System.Drawing.Point(285, 7);
            this.lblPriority.Name = "lblPriority";
            this.lblPriority.Size = new System.Drawing.Size(78, 17);
            this.lblPriority.TabIndex = 1;
            this.lblPriority.Text = "приоритет";
            // 
            // comboBoxPointOfSale
            // 
            this.comboBoxPointOfSale.FormattingEnabled = true;
            this.comboBoxPointOfSale.Location = new System.Drawing.Point(3, 4);
            this.comboBoxPointOfSale.Name = "comboBoxPointOfSale";
            this.comboBoxPointOfSale.Size = new System.Drawing.Size(161, 24);
            this.comboBoxPointOfSale.TabIndex = 0;
            // 
            // MultiEditScheduleForm
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(536, 180);
            this.Controls.Add(this.flowLayoutPanel);
            this.Controls.Add(this.pnlMiddle);
            this.Controls.Add(this.pnlTop);
            this.Controls.Add(this.pnlBottom);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(610, 300);
            this.MinimizeBox = false;
            this.Name = "MultiEditScheduleForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Элемент расписания";
            this.Load += new System.EventHandler(this.MultiEditScheduleForm_Load);
            this.pnlBottom.ResumeLayout(false);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tEditWaiting.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tEditDeliveryEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tEditDeliveryBegin.Properties)).EndInit();
            this.flowLayoutPanel.ResumeLayout(false);
            this.flowLayoutPanel.PerformLayout();
            this.pnlMiddle.ResumeLayout(false);
            this.pnlMiddle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPriority)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Label lblWaiting;
        private System.Windows.Forms.Label lblDeliveryEnd;
        private System.Windows.Forms.Label lblDeliveryBegin;
        private DevExpress.XtraEditors.TimeEdit tEditWaiting;
        private DevExpress.XtraEditors.TimeEdit tEditDeliveryEnd;
        private DevExpress.XtraEditors.TimeEdit tEditDeliveryBegin;
        private DevExpress.Utils.ToolTipController tipError;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel;
        private System.Windows.Forms.CheckBox chkBoxMonday;
        private System.Windows.Forms.CheckBox chkBoxTuesday;
        private System.Windows.Forms.CheckBox chkBoxWednesday;
        private System.Windows.Forms.CheckBox chkBoxThursday;
        private System.Windows.Forms.CheckBox chkBoxFriday;
        private System.Windows.Forms.CheckBox chkBoxSaturday;
        private System.Windows.Forms.CheckBox chkBoxSunday;
        private System.Windows.Forms.Panel pnlMiddle;
        private System.Windows.Forms.ComboBox comboBoxPointOfSale;
        private System.Windows.Forms.NumericUpDown numPriority;
        private System.Windows.Forms.Label lblPriority;
    }
}