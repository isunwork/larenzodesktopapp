﻿namespace LaRenzo.Forms.HistoryForms
{
    partial class SessionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.detailGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.DishIdCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DishCustmerNameCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DishTotalCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DishCustomerPhoneCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DishCodeCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Driver = new DevExpress.XtraGrid.Columns.GridColumn();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnIdCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnOpenDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCloseDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.linqServerModeSourceSessions = new DevExpress.Data.Linq.LinqServerModeSource();
            this.SessionContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.PrintSessionCheck = new System.Windows.Forms.ToolStripMenuItem();
            this.OrderContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.PrintOrderCheck = new System.Windows.Forms.ToolStripMenuItem();
            this.moveOrdersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.detailGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linqServerModeSourceSessions)).BeginInit();
            this.SessionContextMenu.SuspendLayout();
            this.OrderContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // detailGridView
            // 
            this.detailGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.DishIdCol,
            this.DishCustmerNameCol,
            this.DishTotalCol,
            this.DishCustomerPhoneCol,
            this.DishCodeCol,
            this.Driver});
            this.detailGridView.GridControl = this.mainGridControl;
            this.detailGridView.Name = "detailGridView";
            this.detailGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView2_PopupMenuShowing);
            // 
            // DishIdCol
            // 
            this.DishIdCol.Caption = "Ид";
            this.DishIdCol.FieldName = "ID";
            this.DishIdCol.Name = "DishIdCol";
            this.DishIdCol.OptionsColumn.AllowEdit = false;
            this.DishIdCol.OptionsColumn.AllowFocus = false;
            this.DishIdCol.OptionsColumn.ReadOnly = true;
            this.DishIdCol.Visible = true;
            this.DishIdCol.VisibleIndex = 0;
            // 
            // DishCustmerNameCol
            // 
            this.DishCustmerNameCol.Caption = "Имя";
            this.DishCustmerNameCol.FieldName = "Name";
            this.DishCustmerNameCol.Name = "DishCustmerNameCol";
            this.DishCustmerNameCol.OptionsColumn.AllowEdit = false;
            this.DishCustmerNameCol.OptionsColumn.AllowFocus = false;
            this.DishCustmerNameCol.OptionsColumn.ReadOnly = true;
            this.DishCustmerNameCol.Visible = true;
            this.DishCustmerNameCol.VisibleIndex = 2;
            // 
            // DishTotalCol
            // 
            this.DishTotalCol.Caption = "Сумма заказа";
            this.DishTotalCol.FieldName = "TotalCost";
            this.DishTotalCol.Name = "DishTotalCol";
            this.DishTotalCol.OptionsColumn.AllowEdit = false;
            this.DishTotalCol.OptionsColumn.AllowFocus = false;
            this.DishTotalCol.OptionsColumn.ReadOnly = true;
            this.DishTotalCol.Visible = true;
            this.DishTotalCol.VisibleIndex = 1;
            // 
            // DishCustomerPhoneCol
            // 
            this.DishCustomerPhoneCol.Caption = "Телефон";
            this.DishCustomerPhoneCol.FieldName = "Phone";
            this.DishCustomerPhoneCol.Name = "DishCustomerPhoneCol";
            this.DishCustomerPhoneCol.OptionsColumn.AllowEdit = false;
            this.DishCustomerPhoneCol.OptionsColumn.AllowFocus = false;
            this.DishCustomerPhoneCol.OptionsColumn.ReadOnly = true;
            this.DishCustomerPhoneCol.Visible = true;
            this.DishCustomerPhoneCol.VisibleIndex = 3;
            // 
            // DishCodeCol
            // 
            this.DishCodeCol.Caption = "Код заказа";
            this.DishCodeCol.FieldName = "Code";
            this.DishCodeCol.Name = "DishCodeCol";
            this.DishCodeCol.OptionsColumn.AllowEdit = false;
            this.DishCodeCol.OptionsColumn.AllowFocus = false;
            this.DishCodeCol.OptionsColumn.ReadOnly = true;
            this.DishCodeCol.Visible = true;
            this.DishCodeCol.VisibleIndex = 4;
            // 
            // Driver
            // 
            this.Driver.Caption = "Водитель";
            this.Driver.FieldName = "Driver.Name";
            this.Driver.Name = "Driver";
            this.Driver.OptionsColumn.AllowEdit = false;
            this.Driver.OptionsColumn.AllowFocus = false;
            this.Driver.OptionsColumn.ReadOnly = true;
            this.Driver.Visible = true;
            this.Driver.VisibleIndex = 5;
            // 
            // mainGridControl
            // 
            this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.detailGridView;
            gridLevelNode1.RelationName = "Orders";
            this.mainGridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.mainGridControl.Location = new System.Drawing.Point(0, 0);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(987, 467);
            this.mainGridControl.TabIndex = 4;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView,
            this.detailGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnIdCol,
            this.columnOpenDate,
            this.columnCloseDate,
            this.columnTotal});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.mainGridView.OptionsView.ShowFooter = true;
            this.mainGridView.MasterRowGetChildList += new DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventHandler(this.gridView1_MasterRowGetChildList);
            this.mainGridView.MasterRowGetRelationName += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.gridView1_MasterRowGetRelationName);
            this.mainGridView.MasterRowGetRelationDisplayCaption += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.gridView1_MasterRowGetRelationDisplayCaption);
            this.mainGridView.MasterRowGetRelationCount += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventHandler(this.gridView1_MasterRowGetRelationCount);
            this.mainGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            // 
            // columnIdCol
            // 
            this.columnIdCol.Caption = "Ид";
            this.columnIdCol.FieldName = "ID";
            this.columnIdCol.Name = "columnIdCol";
            this.columnIdCol.OptionsColumn.AllowEdit = false;
            this.columnIdCol.OptionsColumn.AllowFocus = false;
            this.columnIdCol.OptionsColumn.ReadOnly = true;
            this.columnIdCol.Visible = true;
            this.columnIdCol.VisibleIndex = 0;
            // 
            // columnOpenDate
            // 
            this.columnOpenDate.Caption = "Открыта";
            this.columnOpenDate.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm";
            this.columnOpenDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.columnOpenDate.FieldName = "Opened";
            this.columnOpenDate.Name = "columnOpenDate";
            this.columnOpenDate.OptionsColumn.AllowEdit = false;
            this.columnOpenDate.OptionsColumn.AllowFocus = false;
            this.columnOpenDate.OptionsColumn.ReadOnly = true;
            this.columnOpenDate.Visible = true;
            this.columnOpenDate.VisibleIndex = 1;
            // 
            // columnCloseDate
            // 
            this.columnCloseDate.Caption = "Закрыта";
            this.columnCloseDate.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm";
            this.columnCloseDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.columnCloseDate.FieldName = "Closed";
            this.columnCloseDate.Name = "columnCloseDate";
            this.columnCloseDate.OptionsColumn.AllowEdit = false;
            this.columnCloseDate.OptionsColumn.AllowFocus = false;
            this.columnCloseDate.OptionsColumn.ReadOnly = true;
            this.columnCloseDate.Visible = true;
            this.columnCloseDate.VisibleIndex = 2;
            // 
            // columnTotal
            // 
            this.columnTotal.Caption = "Выручка за сессиию";
            this.columnTotal.FieldName = "TotalRevenue";
            this.columnTotal.Name = "columnTotal";
            this.columnTotal.OptionsColumn.AllowEdit = false;
            this.columnTotal.OptionsColumn.AllowFocus = false;
            this.columnTotal.OptionsColumn.ReadOnly = true;
            this.columnTotal.Visible = true;
            this.columnTotal.VisibleIndex = 3;
            // 
            // linqServerModeSourceSessions
            // 
            this.linqServerModeSourceSessions.KeyExpression = "ID";
            // 
            // SessionContextMenu
            // 
            this.SessionContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PrintSessionCheck,
            this.moveOrdersToolStripMenuItem});
            this.SessionContextMenu.Name = "SessionContextMenu";
            this.SessionContextMenu.Size = new System.Drawing.Size(258, 70);
            // 
            // PrintSessionCheck
            // 
            this.PrintSessionCheck.Name = "PrintSessionCheck";
            this.PrintSessionCheck.Size = new System.Drawing.Size(257, 22);
            this.PrintSessionCheck.Text = "Напечатать чек";
            this.PrintSessionCheck.Click += new System.EventHandler(this.напечататьЧекToolStripMenuItem_Click);
            // 
            // OrderContextMenu
            // 
            this.OrderContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PrintOrderCheck});
            this.OrderContextMenu.Name = "SessionContextMenu";
            this.OrderContextMenu.Size = new System.Drawing.Size(160, 26);
            // 
            // PrintOrderCheck
            // 
            this.PrintOrderCheck.Name = "PrintOrderCheck";
            this.PrintOrderCheck.Size = new System.Drawing.Size(159, 22);
            this.PrintOrderCheck.Text = "Напечатать чек";
            // 
            // moveOrdersToolStripMenuItem
            // 
            this.moveOrdersToolStripMenuItem.Name = "moveOrdersToolStripMenuItem";
            this.moveOrdersToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.moveOrdersToolStripMenuItem.Text = "Перенести заказы из этой сессии";
            // 
            // SessionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(987, 467);
            this.Controls.Add(this.mainGridControl);
            this.Name = "SessionsForm";
            this.Text = "Все сессии";
            ((System.ComponentModel.ISupportInitialize)(this.detailGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linqServerModeSourceSessions)).EndInit();
            this.SessionContextMenu.ResumeLayout(false);
            this.OrderContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.Data.Linq.LinqServerModeSource linqServerModeSourceSessions;
        public DevExpress.XtraGrid.GridControl mainGridControl;
        public DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnIdCol;
        private DevExpress.XtraGrid.Columns.GridColumn columnOpenDate;
        private DevExpress.XtraGrid.Columns.GridColumn columnCloseDate;
        private DevExpress.XtraGrid.Columns.GridColumn columnTotal;
        private DevExpress.XtraGrid.Views.Grid.GridView detailGridView;
        private DevExpress.XtraGrid.Columns.GridColumn DishIdCol;
        private DevExpress.XtraGrid.Columns.GridColumn DishCustmerNameCol;
        private DevExpress.XtraGrid.Columns.GridColumn DishTotalCol;
        private DevExpress.XtraGrid.Columns.GridColumn DishCustomerPhoneCol;
        private DevExpress.XtraGrid.Columns.GridColumn DishCodeCol;
        private System.Windows.Forms.ContextMenuStrip SessionContextMenu;
        private System.Windows.Forms.ToolStripMenuItem PrintSessionCheck;
        private System.Windows.Forms.ContextMenuStrip OrderContextMenu;
        private System.Windows.Forms.ToolStripMenuItem PrintOrderCheck;
        private DevExpress.XtraGrid.Columns.GridColumn Driver;
        private System.Windows.Forms.ToolStripMenuItem moveOrdersToolStripMenuItem;
    }
}