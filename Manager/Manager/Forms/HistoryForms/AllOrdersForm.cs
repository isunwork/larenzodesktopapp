﻿#region File Info

// Author: Sxquer
// Project : Manager
// Filename : AllOrdersForm.cs
// Created : 24/10/2012 (12:22)
// Last Modified : 24/10/2012 (12:45)

#endregion

using System;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Entities.PrintWorksModels;
using LaRenzo.DataRepository.Entities.States;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Printers;
using LaRenzo.DataRepository.Repositories.Documents.Orders;
using LaRenzo.Forms.Documents.DeliveryOrderForms;
using LaRenzo.Properties;

namespace LaRenzo.Forms.HistoryForms
{
    public partial class AllOrdersForm : Form
    {
        public AllOrdersForm()
        {
            InitializeComponent();
            mainGridView.DoubleClick += async (s, e) => await gridView1_DoubleClick(s);
            mainGridControl.KeyDown += async (s, e) => await gridControlAllOrders_KeyDown(e);
            PrintOrderCheck.Click += async (s, e) => await PrintOrderCheck_Click();
        }

        private void AllOrdersForm_Load(object sender, EventArgs e)
        {
            dtBegin.Value = DateTime.Now.AddMonths(-1);
            dtEnd.Value = DateTime.Now;
            chkOnlyWeb.Checked = false;

            UpdateData();
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            var view = sender as GridView;

            if (view != null)
            {
                GridHitInfo hitInfo = view.CalcHitInfo(e.Point);
                if (view.IsDataRow(hitInfo.RowHandle))
                {
                    OrderContextMenu.Show(view.GridControl, e.Point);
                }
            }
        }

        private void DeleteOrder_Click(object sender, EventArgs e)
        {
            try
            {
                var view = mainGridView;

                if (view.IsDataRow(view.FocusedRowHandle))
                {
                    var id = (int)view.GetRowCellValue(view.FocusedRowHandle, columnId);
                    Content.OrderManager.Remove(id);
                    UpdateData();
                }
                else
                {
                    MessageBox.Show(Resources.SelectOrderLineForRemove);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private async Task PrintOrderCheck_Click()
        {
            try
            {
                var view = mainGridView;
                if (view.IsDataRow(view.FocusedRowHandle))
                {
                    var id = (int)view.GetRowCellValue(view.FocusedRowHandle, columnId);

                    Order order = Content.OrderManager.GetById(id);
                    int workId = Content.PrintWorksRepository.AddPrintWork(new PrintWork
                    {
                        Created = DateTime.Now,
                        OrderId = id,
                        State = (int)State.Processing
                    });

                    string message = await PrintingManager.Print(order, Settings.Default.PrinterPort, Settings.Default.PrinterIp, workId);
                    if (!string.IsNullOrEmpty(message))
                    {
                        MessageBox.Show(message);
                        Content.PrintWorksRepository.UpdatePrintWorkState(workId, State.Error);
                    }
                    else
                    {
                        Content.PrintWorksRepository.UpdatePrintWorkState(workId, State.Compleated);
                    }
                }
                else
                {
                    MessageBox.Show(Resources.SelectOrderLineForPrint);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async Task gridControlAllOrders_KeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                await ShowDetails(mainGridView.FocusedRowHandle);
            }
        }

        private async Task gridView1_DoubleClick(object sender)
        {
            var view = (GridView)sender;

            Point pt = view.GridControl.PointToClient(MousePosition);

            GridHitInfo info = view.CalcHitInfo(pt);
            if (view.IsDataRow(view.FocusedRowHandle))
            {
                await ShowDetails((int)view.GetRowCellValue(info.RowHandle, columnId));
            }
        }

        private async Task ShowDetails(int id)
        {
            try
            {
                var form = new DeliveryOrderForm(DetailsFormModes.ViewByManager, id);
                await form.Initialize();
                form.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void UpdateData()
        {
            if (backgroundWorkerGenerate.IsBusy)
            {
                MessageBox.Show(Resources.GeneratingReport);
            }
            else
            {
                splashScreenManagerWaitForm.ShowWaitForm();
                splashScreenManagerWaitForm.SetWaitFormCaption("Подождите...");
                splashScreenManagerWaitForm.SetWaitFormDescription("Идет обработка вашего запроса");
                backgroundWorkerGenerate.RunWorkerAsync();
            }
        }


        private void backgroundWorkerGenerate_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            e.Result = Content.OrderManager.GetByPeriod(dtBegin.Value, dtEnd.Value, chkOnlyWeb.Checked);
        }

        private void backgroundWorkerGenerate_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            mainGridControl.DataSource = null;
            mainGridControl.DataSource = e.Result;
            splashScreenManagerWaitForm.CloseWaitForm();
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            UpdateData();
        }

        private void AllOrdersForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (backgroundWorkerGenerate.IsBusy)
                backgroundWorkerGenerate.CancelAsync();

            if (splashScreenManagerWaitForm.IsSplashFormVisible) splashScreenManagerWaitForm.CloseWaitForm();
        }


        private int _calculatedSumTotal;

        private void gridView1_CustomSummaryCalculate(object sender, CustomSummaryEventArgs e)
        {
            // Get the summary ID. 
            var gridSummaryItem = e.Item as GridSummaryItem;

            if (gridSummaryItem != null)
            {
                int tag = Convert.ToInt32(gridSummaryItem.Tag);
                var view = sender as GridView;

                if (e.SummaryProcess == CustomSummaryProcess.Start)
                {
                    _calculatedSumTotal = 0;
                }
                // Calculation 
                if (e.SummaryProcess == CustomSummaryProcess.Calculate)
                {
                    if (view != null)
                    {
                        var state = (string)(view.GetRowCellValue(e.RowHandle, columnState));
                        if (state != "Отказ" && tag == 1)

                            _calculatedSumTotal += Convert.ToInt32(e.FieldValue);
                    }
                }

                // Finalization 
                if (e.SummaryProcess == CustomSummaryProcess.Finalize)
                {
                    if (tag == 1) e.TotalValue = _calculatedSumTotal;
                }
            }
        }

        private void gridView1_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "SaleCol" && e.IsGetData)
            {
                var order = (Order)e.Row;
                e.Value = order.TotalCostWithoutDiscount - order.TotalCost;
            }

        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            using (var saveDialog = new SaveFileDialog())
            {
                saveDialog.Filter = Resources.ExportFormatFiles;
                if (saveDialog.ShowDialog() != DialogResult.Cancel)
                {
                    string exportFilePath = saveDialog.FileName;
                    string fileExtenstion = new FileInfo(exportFilePath).Extension;
                    switch (fileExtenstion)
                    {
                        case ".xls":
                            mainGridControl.ExportToXls(exportFilePath);
                            break;
                        case ".xlsx":
                            mainGridControl.ExportToXlsx(exportFilePath);
                            break;
                        case ".rtf":
                            mainGridControl.ExportToRtf(exportFilePath);
                            break;
                        case ".pdf":
                            mainGridControl.ExportToPdf(exportFilePath);
                            break;
                        case ".html":
                            mainGridControl.ExportToHtml(exportFilePath);
                            break;
                        case ".mht":
                            mainGridControl.ExportToMht(exportFilePath);
                            break;
                        case ".csv":
                            mainGridControl.ExportToCsv(exportFilePath);
                            break;
                        case ".txt":
                            mainGridControl.ExportToText(exportFilePath);
                            break;
                    }

                }
            }
        }
    }
}