﻿namespace LaRenzo.Forms.HistoryForms
{
    partial class AllOrdersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.brendColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnSessionId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnSessionOpened = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnSessionClosed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCreationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnKitchen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnTimeClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnStreet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnHouse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnFlat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPorch = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnTotalCostWithoutDisc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnSale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDriver = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnWindow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnWeb = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnIsOnlinePayment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sourceColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.linqServerModeSourceAllOrders = new DevExpress.Data.Linq.LinqServerModeSource();
            this.OrderContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.PrintOrderCheck = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteOrder = new System.Windows.Forms.ToolStripMenuItem();
            this.lblLastOrders = new System.Windows.Forms.Label();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.backgroundWorkerGenerate = new System.ComponentModel.BackgroundWorker();
            this.splashScreenManagerWaitForm = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::LaRenzo.Forms.WaitFormGeneral), true, true);
            this.dtBegin = new System.Windows.Forms.DateTimePicker();
            this.dtEnd = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.chkOnlyWeb = new System.Windows.Forms.CheckBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.mainPnl = new System.Windows.Forms.Panel();
            this.gridColumnPointOfSale = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linqServerModeSourceAllOrders)).BeginInit();
            this.OrderContextMenu.SuspendLayout();
            this.mainPnl.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.brendColumn,
            this.columnSessionId,
            this.columnSessionOpened,
            this.columnSessionClosed,
            this.columnId,
            this.columnName,
            this.columnPhone,
            this.columnCost,
            this.columnCode,
            this.columnCreationDate,
            this.columnState,
            this.columnKitchen,
            this.columnTimeClient,
            this.columnStreet,
            this.columnHouse,
            this.columnFlat,
            this.columnPorch,
            this.columnTotalCostWithoutDisc,
            this.columnSale,
            this.columnDriver,
            this.columnUser,
            this.columnWindow,
            this.columnWeb,
            this.columnCreated,
            this.columnIsOnlinePayment,
            this.sourceColumn,
            this.gridColumnPointOfSale});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalCost", this.columnCost, "")});
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.mainGridView.OptionsView.ShowFooter = true;
            this.mainGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.mainGridView.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(this.gridView1_CustomSummaryCalculate);
            this.mainGridView.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridView1_CustomUnboundColumnData);
            // 
            // brendColumn
            // 
            this.brendColumn.Caption = "Бренд";
            this.brendColumn.FieldName = "Brand.Name";
            this.brendColumn.Name = "brendColumn";
            this.brendColumn.Visible = true;
            this.brendColumn.VisibleIndex = 0;
            // 
            // columnSessionId
            // 
            this.columnSessionId.Caption = "Номер сессии";
            this.columnSessionId.FieldName = "SessionID";
            this.columnSessionId.Name = "columnSessionId";
            this.columnSessionId.OptionsColumn.AllowEdit = false;
            this.columnSessionId.OptionsColumn.AllowFocus = false;
            this.columnSessionId.OptionsColumn.ReadOnly = true;
            this.columnSessionId.Visible = true;
            this.columnSessionId.VisibleIndex = 1;
            // 
            // columnSessionOpened
            // 
            this.columnSessionOpened.Caption = "Сессия была открыта";
            this.columnSessionOpened.FieldName = "Session.Opened";
            this.columnSessionOpened.Name = "columnSessionOpened";
            this.columnSessionOpened.OptionsColumn.AllowEdit = false;
            this.columnSessionOpened.OptionsColumn.AllowFocus = false;
            this.columnSessionOpened.OptionsColumn.ReadOnly = true;
            this.columnSessionOpened.Visible = true;
            this.columnSessionOpened.VisibleIndex = 2;
            // 
            // columnSessionClosed
            // 
            this.columnSessionClosed.Caption = "Сессия была закрыта";
            this.columnSessionClosed.FieldName = "Session.Closed";
            this.columnSessionClosed.Name = "columnSessionClosed";
            this.columnSessionClosed.OptionsColumn.AllowEdit = false;
            this.columnSessionClosed.OptionsColumn.AllowFocus = false;
            this.columnSessionClosed.OptionsColumn.ReadOnly = true;
            this.columnSessionClosed.Visible = true;
            this.columnSessionClosed.VisibleIndex = 3;
            // 
            // columnId
            // 
            this.columnId.Caption = "Ид";
            this.columnId.FieldName = "ID";
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.AllowEdit = false;
            this.columnId.OptionsColumn.AllowFocus = false;
            this.columnId.OptionsColumn.ReadOnly = true;
            // 
            // columnName
            // 
            this.columnName.Caption = "Имя";
            this.columnName.FieldName = "Name";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.OptionsColumn.AllowFocus = false;
            this.columnName.OptionsColumn.ReadOnly = true;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 6;
            // 
            // columnPhone
            // 
            this.columnPhone.Caption = "Телефон";
            this.columnPhone.FieldName = "Phone";
            this.columnPhone.Name = "columnPhone";
            this.columnPhone.OptionsColumn.AllowEdit = false;
            this.columnPhone.OptionsColumn.AllowFocus = false;
            this.columnPhone.OptionsColumn.ReadOnly = true;
            this.columnPhone.Visible = true;
            this.columnPhone.VisibleIndex = 8;
            // 
            // columnCost
            // 
            this.columnCost.Caption = "Сумма заказа";
            this.columnCost.FieldName = "TotalCost";
            this.columnCost.Name = "columnCost";
            this.columnCost.OptionsColumn.AllowEdit = false;
            this.columnCost.OptionsColumn.AllowFocus = false;
            this.columnCost.OptionsColumn.ReadOnly = true;
            this.columnCost.Visible = true;
            this.columnCost.VisibleIndex = 10;
            // 
            // columnCode
            // 
            this.columnCode.Caption = "Код заказа";
            this.columnCode.FieldName = "Code";
            this.columnCode.Name = "columnCode";
            this.columnCode.OptionsColumn.AllowEdit = false;
            this.columnCode.OptionsColumn.AllowFocus = false;
            this.columnCode.OptionsColumn.ReadOnly = true;
            this.columnCode.Visible = true;
            this.columnCode.VisibleIndex = 5;
            // 
            // columnCreationDate
            // 
            this.columnCreationDate.Caption = "Время созадния";
            this.columnCreationDate.DisplayFormat.FormatString = "HH:mm";
            this.columnCreationDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.columnCreationDate.FieldName = "Created";
            this.columnCreationDate.Name = "columnCreationDate";
            this.columnCreationDate.OptionsColumn.AllowEdit = false;
            this.columnCreationDate.OptionsColumn.AllowFocus = false;
            this.columnCreationDate.OptionsColumn.ReadOnly = true;
            // 
            // columnState
            // 
            this.columnState.Caption = "Состояние заказа";
            this.columnState.FieldName = "State.Name";
            this.columnState.Name = "columnState";
            this.columnState.OptionsColumn.AllowEdit = false;
            this.columnState.OptionsColumn.AllowFocus = false;
            this.columnState.OptionsColumn.ReadOnly = true;
            this.columnState.Visible = true;
            this.columnState.VisibleIndex = 11;
            // 
            // columnKitchen
            // 
            this.columnKitchen.Caption = "Время Кухни";
            this.columnKitchen.DisplayFormat.FormatString = "HH:mm";
            this.columnKitchen.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.columnKitchen.FieldName = "TimeKitchen";
            this.columnKitchen.Name = "columnKitchen";
            this.columnKitchen.OptionsColumn.AllowEdit = false;
            this.columnKitchen.OptionsColumn.AllowFocus = false;
            this.columnKitchen.OptionsColumn.ReadOnly = true;
            // 
            // columnTimeClient
            // 
            this.columnTimeClient.Caption = "Время Клиент";
            this.columnTimeClient.DisplayFormat.FormatString = "HH:mm";
            this.columnTimeClient.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.columnTimeClient.FieldName = "TimeClient";
            this.columnTimeClient.Name = "columnTimeClient";
            this.columnTimeClient.OptionsColumn.AllowEdit = false;
            this.columnTimeClient.OptionsColumn.AllowFocus = false;
            this.columnTimeClient.OptionsColumn.ReadOnly = true;
            // 
            // columnStreet
            // 
            this.columnStreet.Caption = "Улица";
            this.columnStreet.FieldName = "Street";
            this.columnStreet.Name = "columnStreet";
            this.columnStreet.OptionsColumn.AllowEdit = false;
            this.columnStreet.OptionsColumn.AllowFocus = false;
            this.columnStreet.OptionsColumn.ReadOnly = true;
            this.columnStreet.Visible = true;
            this.columnStreet.VisibleIndex = 12;
            // 
            // columnHouse
            // 
            this.columnHouse.Caption = "Дом";
            this.columnHouse.FieldName = "House";
            this.columnHouse.Name = "columnHouse";
            this.columnHouse.OptionsColumn.AllowEdit = false;
            this.columnHouse.OptionsColumn.AllowFocus = false;
            this.columnHouse.OptionsColumn.ReadOnly = true;
            this.columnHouse.Visible = true;
            this.columnHouse.VisibleIndex = 13;
            // 
            // columnFlat
            // 
            this.columnFlat.Caption = "Квартира";
            this.columnFlat.FieldName = "Appartament";
            this.columnFlat.Name = "columnFlat";
            this.columnFlat.OptionsColumn.AllowEdit = false;
            this.columnFlat.OptionsColumn.AllowFocus = false;
            this.columnFlat.OptionsColumn.ReadOnly = true;
            this.columnFlat.Visible = true;
            this.columnFlat.VisibleIndex = 14;
            // 
            // columnPorch
            // 
            this.columnPorch.Caption = "Подъезд";
            this.columnPorch.FieldName = "Porch";
            this.columnPorch.Name = "columnPorch";
            this.columnPorch.OptionsColumn.AllowEdit = false;
            this.columnPorch.OptionsColumn.AllowFocus = false;
            this.columnPorch.OptionsColumn.ReadOnly = true;
            this.columnPorch.Visible = true;
            this.columnPorch.VisibleIndex = 15;
            // 
            // columnTotalCostWithoutDisc
            // 
            this.columnTotalCostWithoutDisc.Caption = "Без скидки";
            this.columnTotalCostWithoutDisc.FieldName = "TotalCostWithoutDiscount";
            this.columnTotalCostWithoutDisc.Name = "columnTotalCostWithoutDisc";
            this.columnTotalCostWithoutDisc.OptionsColumn.AllowEdit = false;
            this.columnTotalCostWithoutDisc.OptionsColumn.ReadOnly = true;
            this.columnTotalCostWithoutDisc.Visible = true;
            this.columnTotalCostWithoutDisc.VisibleIndex = 7;
            // 
            // columnSale
            // 
            this.columnSale.Caption = "Скидка";
            this.columnSale.FieldName = "SaleCol";
            this.columnSale.Name = "columnSale";
            this.columnSale.OptionsColumn.AllowEdit = false;
            this.columnSale.OptionsColumn.ReadOnly = true;
            this.columnSale.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.columnSale.Visible = true;
            this.columnSale.VisibleIndex = 9;
            // 
            // columnDriver
            // 
            this.columnDriver.Caption = "Водитель";
            this.columnDriver.FieldName = "Driver.Name";
            this.columnDriver.Name = "columnDriver";
            this.columnDriver.OptionsColumn.AllowEdit = false;
            this.columnDriver.OptionsColumn.AllowFocus = false;
            this.columnDriver.OptionsColumn.ReadOnly = true;
            this.columnDriver.Visible = true;
            this.columnDriver.VisibleIndex = 16;
            // 
            // columnUser
            // 
            this.columnUser.Caption = "Пользователь";
            this.columnUser.FieldName = "User";
            this.columnUser.Name = "columnUser";
            this.columnUser.Visible = true;
            this.columnUser.VisibleIndex = 17;
            // 
            // columnWindow
            // 
            this.columnWindow.Caption = "Окно";
            this.columnWindow.FieldName = "IsWindow";
            this.columnWindow.Name = "columnWindow";
            this.columnWindow.OptionsColumn.AllowEdit = false;
            this.columnWindow.OptionsColumn.AllowFocus = false;
            this.columnWindow.OptionsColumn.ReadOnly = true;
            this.columnWindow.Visible = true;
            this.columnWindow.VisibleIndex = 19;
            this.columnWindow.Width = 52;
            // 
            // columnWeb
            // 
            this.columnWeb.Caption = "Web";
            this.columnWeb.FieldName = "IsWebRequest";
            this.columnWeb.Name = "columnWeb";
            this.columnWeb.OptionsColumn.AllowEdit = false;
            this.columnWeb.OptionsColumn.AllowFocus = false;
            this.columnWeb.OptionsColumn.ReadOnly = true;
            this.columnWeb.Visible = true;
            this.columnWeb.VisibleIndex = 18;
            // 
            // columnCreated
            // 
            this.columnCreated.Caption = "Дата заказа";
            this.columnCreated.DisplayFormat.FormatString = "dd.MM.yyyy hh:mm";
            this.columnCreated.FieldName = "Created";
            this.columnCreated.Name = "columnCreated";
            this.columnCreated.OptionsColumn.AllowEdit = false;
            this.columnCreated.OptionsColumn.AllowFocus = false;
            this.columnCreated.OptionsColumn.ReadOnly = true;
            this.columnCreated.Visible = true;
            this.columnCreated.VisibleIndex = 4;
            // 
            // columnIsOnlinePayment
            // 
            this.columnIsOnlinePayment.Caption = "Метод оплаты";
            this.columnIsOnlinePayment.FieldName = "PaymentMethod";
            this.columnIsOnlinePayment.Name = "columnIsOnlinePayment";
            this.columnIsOnlinePayment.Visible = true;
            this.columnIsOnlinePayment.VisibleIndex = 20;
            // 
            // sourceColumn
            // 
            this.sourceColumn.Caption = "Источник";
            this.sourceColumn.FieldName = "Source";
            this.sourceColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.sourceColumn.Name = "sourceColumn";
            this.sourceColumn.Visible = true;
            this.sourceColumn.VisibleIndex = 22;
            // 
            // mainGridControl
            // 
            this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl.Location = new System.Drawing.Point(0, 30);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(1186, 470);
            this.mainGridControl.TabIndex = 3;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // linqServerModeSourceAllOrders
            // 
            this.linqServerModeSourceAllOrders.KeyExpression = "ID";
            // 
            // OrderContextMenu
            // 
            this.OrderContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PrintOrderCheck,
            this.DeleteOrder});
            this.OrderContextMenu.Name = "SessionContextMenu";
            this.OrderContextMenu.Size = new System.Drawing.Size(160, 48);
            // 
            // PrintOrderCheck
            // 
            this.PrintOrderCheck.Name = "PrintOrderCheck";
            this.PrintOrderCheck.Size = new System.Drawing.Size(159, 22);
            this.PrintOrderCheck.Text = "Напечатать чек";
            // 
            // DeleteOrder
            // 
            this.DeleteOrder.Enabled = false;
            this.DeleteOrder.Name = "DeleteOrder";
            this.DeleteOrder.Size = new System.Drawing.Size(159, 22);
            this.DeleteOrder.Text = "Удалить заказ";
            this.DeleteOrder.Click += new System.EventHandler(this.DeleteOrder_Click);
            // 
            // lblLastOrders
            // 
            this.lblLastOrders.AutoSize = true;
            this.lblLastOrders.Location = new System.Drawing.Point(5, 8);
            this.lblLastOrders.Name = "lblLastOrders";
            this.lblLastOrders.Size = new System.Drawing.Size(57, 13);
            this.lblLastOrders.TabIndex = 5;
            this.lblLastOrders.Text = "Период  с";
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(431, 3);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(132, 23);
            this.btnGenerate.TabIndex = 6;
            this.btnGenerate.Text = "Получить данные";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // backgroundWorkerGenerate
            // 
            this.backgroundWorkerGenerate.WorkerSupportsCancellation = true;
            this.backgroundWorkerGenerate.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerGenerate_DoWork);
            this.backgroundWorkerGenerate.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerGenerate_RunWorkerCompleted);
            // 
            // dtBegin
            // 
            this.dtBegin.Location = new System.Drawing.Point(65, 6);
            this.dtBegin.Name = "dtBegin";
            this.dtBegin.Size = new System.Drawing.Size(118, 20);
            this.dtBegin.TabIndex = 9;
            // 
            // dtEnd
            // 
            this.dtEnd.Location = new System.Drawing.Point(214, 6);
            this.dtEnd.Name = "dtEnd";
            this.dtEnd.Size = new System.Drawing.Size(118, 20);
            this.dtEnd.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(189, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "по";
            // 
            // chkOnlyWeb
            // 
            this.chkOnlyWeb.AutoSize = true;
            this.chkOnlyWeb.Location = new System.Drawing.Point(338, 7);
            this.chkOnlyWeb.Name = "chkOnlyWeb";
            this.chkOnlyWeb.Size = new System.Drawing.Size(87, 17);
            this.chkOnlyWeb.TabIndex = 12;
            this.chkOnlyWeb.Text = "только Web";
            this.chkOnlyWeb.UseVisualStyleBackColor = true;
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExport.Location = new System.Drawing.Point(1059, 3);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(124, 23);
            this.btnExport.TabIndex = 13;
            this.btnExport.Text = "Экспорт";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // mainPnl
            // 
            this.mainPnl.Controls.Add(this.btnExport);
            this.mainPnl.Controls.Add(this.lblLastOrders);
            this.mainPnl.Controls.Add(this.btnGenerate);
            this.mainPnl.Controls.Add(this.chkOnlyWeb);
            this.mainPnl.Controls.Add(this.dtBegin);
            this.mainPnl.Controls.Add(this.label1);
            this.mainPnl.Controls.Add(this.dtEnd);
            this.mainPnl.Dock = System.Windows.Forms.DockStyle.Top;
            this.mainPnl.Location = new System.Drawing.Point(0, 0);
            this.mainPnl.Name = "mainPnl";
            this.mainPnl.Size = new System.Drawing.Size(1186, 30);
            this.mainPnl.TabIndex = 14;
            // 
            // gridColumnPointOfSale
            // 
            this.gridColumnPointOfSale.Caption = "Точка продаж";
            this.gridColumnPointOfSale.FieldName = "PointOfSale.PointName";
            this.gridColumnPointOfSale.Name = "gridColumnPointOfSale";
            this.gridColumnPointOfSale.Visible = true;
            this.gridColumnPointOfSale.VisibleIndex = 21;
            // 
            // AllOrdersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1186, 500);
            this.Controls.Add(this.mainGridControl);
            this.Controls.Add(this.mainPnl);
            this.Name = "AllOrdersForm";
            this.Text = "Все заказы";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AllOrdersForm_FormClosed);
            this.Load += new System.EventHandler(this.AllOrdersForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linqServerModeSourceAllOrders)).EndInit();
            this.OrderContextMenu.ResumeLayout(false);
            this.mainPnl.ResumeLayout(false);
            this.mainPnl.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private DevExpress.XtraGrid.Columns.GridColumn columnPhone;
        private DevExpress.XtraGrid.Columns.GridColumn columnCost;
        private DevExpress.XtraGrid.Columns.GridColumn columnCode;
        private DevExpress.XtraGrid.Columns.GridColumn columnCreationDate;
        private DevExpress.XtraGrid.Columns.GridColumn columnState;
        private DevExpress.XtraGrid.Columns.GridColumn columnSessionId;
        private DevExpress.XtraGrid.Columns.GridColumn columnSessionOpened;
        private DevExpress.XtraGrid.Columns.GridColumn columnSessionClosed;
        private DevExpress.XtraGrid.Columns.GridColumn columnKitchen;
        private DevExpress.XtraGrid.Columns.GridColumn columnTimeClient;
        public DevExpress.XtraGrid.GridControl mainGridControl;
        public DevExpress.Data.Linq.LinqServerModeSource linqServerModeSourceAllOrders;
        private System.Windows.Forms.ContextMenuStrip OrderContextMenu;
        private System.Windows.Forms.ToolStripMenuItem PrintOrderCheck;
        private System.Windows.Forms.ToolStripMenuItem DeleteOrder;
        private DevExpress.XtraGrid.Columns.GridColumn columnStreet;
        private DevExpress.XtraGrid.Columns.GridColumn columnHouse;
        private DevExpress.XtraGrid.Columns.GridColumn columnFlat;
        private DevExpress.XtraGrid.Columns.GridColumn columnPorch;
        private System.Windows.Forms.Label lblLastOrders;
        private System.Windows.Forms.Button btnGenerate;
        private System.ComponentModel.BackgroundWorker backgroundWorkerGenerate;
        private DevExpress.XtraGrid.Columns.GridColumn columnTotalCostWithoutDisc;
        private DevExpress.XtraGrid.Columns.GridColumn columnSale;
        private DevExpress.XtraGrid.Columns.GridColumn columnDriver;
        private DevExpress.XtraGrid.Columns.GridColumn columnUser;
        private DevExpress.XtraGrid.Columns.GridColumn columnWeb;
        private System.Windows.Forms.DateTimePicker dtBegin;
        private System.Windows.Forms.DateTimePicker dtEnd;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraGrid.Columns.GridColumn columnCreated;
        private System.Windows.Forms.CheckBox chkOnlyWeb;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Panel mainPnl;
		private DevExpress.XtraGrid.Columns.GridColumn columnIsOnlinePayment;
		private DevExpress.XtraGrid.Columns.GridColumn brendColumn;
		private DevExpress.XtraGrid.Columns.GridColumn sourceColumn;
        private DevExpress.XtraGrid.Columns.GridColumn columnWindow;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPointOfSale;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManagerWaitForm;
    }
}