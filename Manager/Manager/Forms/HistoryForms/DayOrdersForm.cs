﻿#region File Info

// Author: Sxquer
// Project : Manager
// Filename : AllOrdersForm.cs
// Created : 24/10/2012 (12:22)
// Last Modified : 24/10/2012 (12:45)

#endregion

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Documents.Orders;
using LaRenzo.Properties;

namespace LaRenzo.Forms.HistoryForms
{
    public partial class DayOrdersForm : Form
    {
		public DayOrdersForm()
        {
            InitializeComponent();
        }

        private void AllOrdersForm_Load(object sender, EventArgs e)
        {
            dtBegin.Value = DateTime.Now;
            //dtEnd.Value = DateTime.Now;
            //chkOnlyWeb.Checked = false;

            UpdateData();
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            var view = sender as GridView;

            if (view != null)
            {
                GridHitInfo hitInfo = view.CalcHitInfo(e.Point);
	            if (view.IsDataRow(hitInfo.RowHandle))
	            {
		            OrderContextMenu.Show(view.GridControl, e.Point);
	            }
            }
        }

        private void DeleteOrder_Click(object sender, EventArgs e)
        {
        }


        private void PrintOrderCheck_Click(object sender, EventArgs e)
        {
        }

       

       

      


        private void UpdateData()
        {
	        if (backgroundWorkerGenerate.IsBusy)
	        {
		        MessageBox.Show(Resources.GeneratingReport);
	        }
	        else
	        {
		        splashScreenManagerWaitForm.ShowWaitForm();
		        splashScreenManagerWaitForm.SetWaitFormCaption("Подождите...");
		        splashScreenManagerWaitForm.SetWaitFormDescription("Идет обработка вашего запроса");
		        backgroundWorkerGenerate.RunWorkerAsync();
	        }
        }


        private void backgroundWorkerGenerate_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            e.Result = Content.OrderManager.GetByPeriod(dtBegin.Value,dtBegin.Value,false);
        }


        private void backgroundWorkerGenerate_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
			//mainGridControl.DataSource = null;
			//mainGridControl.DataSource = e.Result;

			SetValue((List<Order>)e.Result);

            splashScreenManagerWaitForm.CloseWaitForm();
        }


	    private void SetValue(List<Order> orderList)
	    {
		    float summ = 0;
			float midSumm = 0;int enterCall = 0;
			int enterOrder = 0;
			int orderCount = 0;
			int cancelCount = 0;

			foreach (Order order in orderList)
			{
				summ += order.TotalCost;

				if (order.State.Name == "Отказ")
				{
					cancelCount++;
				}

				if (order.IsWebRequest)
				{
					enterOrder++;
				}

			}

		    orderCount = orderList.Count - cancelCount;
			enterCall = orderList.Count - enterOrder;

		    if (orderCount > 0)
		    {
			    midSumm = summ/orderCount;
		    }


		    label8.Text = midSumm.ToString();
		    label9.Text = cancelCount.ToString();
		    label10.Text = orderCount.ToString();
		    label11.Text = enterOrder.ToString();
		    label12.Text = enterCall.ToString();
			label7.Text = summ.ToString();
	    }



        private void btnGenerate_Click(object sender, EventArgs e)
        {
            UpdateData();
        }

        private void AllOrdersForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (backgroundWorkerGenerate.IsBusy)
                backgroundWorkerGenerate.CancelAsync();
            
            if (splashScreenManagerWaitForm.IsSplashFormVisible) splashScreenManagerWaitForm.CloseWaitForm();
        }
        private void gridView1_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "SaleCol" && e.IsGetData)
            {
                var order = (Order) e.Row;
                e.Value = order.TotalCostWithoutDiscount - order.TotalCost;
            }
                
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
			//using (var saveDialog = new SaveFileDialog())
			//{
			//	saveDialog.Filter = Resources.ExportFormatFiles;
			//	if (saveDialog.ShowDialog() != DialogResult.Cancel)
			//	{
			//		string exportFilePath = saveDialog.FileName;
			//		string fileExtenstion = new FileInfo(exportFilePath).Extension;
			//		switch (fileExtenstion)
			//		{
			//			case ".xls":
			//				mainGridControl.ExportToXls(exportFilePath);
			//				break;
			//			case ".xlsx":
			//				mainGridControl.ExportToXlsx(exportFilePath);
			//				break;
			//			case ".rtf":
			//				mainGridControl.ExportToRtf(exportFilePath);
			//				break;
			//			case ".pdf":
			//				mainGridControl.ExportToPdf(exportFilePath);
			//				break;
			//			case ".html":
			//				mainGridControl.ExportToHtml(exportFilePath);
			//				break;
			//			case ".mht":
			//				mainGridControl.ExportToMht(exportFilePath);
			//				break;
			//			case ".csv":
			//				mainGridControl.ExportToCsv(exportFilePath);
			//				break;
			//			case ".txt":
			//				mainGridControl.ExportToText(exportFilePath);
			//				break;
			//			default:
			//				break;
			//		}

			//	}
			//}
        }
    }
}