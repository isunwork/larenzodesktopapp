﻿using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Documents.CafeOrders;
using LaRenzo.DataRepository.Entities.Documents.Sessions;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Documents.Sessions;

namespace LaRenzo.Forms.HistoryForms
{
    public partial class CafeSessionsForm : Form
    {
        public CafeSessionsForm()
        {
            InitializeComponent();
            mainGridControl.DataSource = Content.SessionManager.GetFullList();
        }
        
        private void gridView1_MasterRowGetChildList(object sender,
                                                     MasterRowGetChildListEventArgs
                                                         e)
        {
            if (mainGridView != null)
            {
                var session = mainGridView.GetRow(e.RowHandle) as Session;
                if (session != null)
                {
                    e.ChildList = Content.CafeOrderDocumentRepository.GetItems(new CafeOrderDocumentFilter
                        {
                            IsSessionFilter = true,
                            SessionId = session.ID
                        }).ToList();
                }
            }
        }

        private void gridView1_MasterRowGetRelationCount(object sender,
                                                         MasterRowGetRelationCountEventArgs
                                                             e)
        {
            e.RelationCount = 1;
        }

        private void gridView1_MasterRowGetRelationDisplayCaption(object sender,
                                                                  MasterRowGetRelationNameEventArgs
                                                                      e)
        {
            e.RelationName = "Заказы кафе";
        }

        private void gridView1_MasterRowGetRelationName(object sender,
                                                        MasterRowGetRelationNameEventArgs
                                                            e)
        {
            e.RelationName = "CafeOrderDocuments";
        }
    }
}