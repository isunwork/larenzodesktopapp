﻿namespace LaRenzo.Forms.HistoryForms
{
	partial class DayOrdersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.linqServerModeSourceAllOrders = new DevExpress.Data.Linq.LinqServerModeSource();
			this.OrderContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.PrintOrderCheck = new System.Windows.Forms.ToolStripMenuItem();
			this.DeleteOrder = new System.Windows.Forms.ToolStripMenuItem();
			this.lblLastOrders = new System.Windows.Forms.Label();
			this.btnGenerate = new System.Windows.Forms.Button();
			this.backgroundWorkerGenerate = new System.ComponentModel.BackgroundWorker();
			this.splashScreenManagerWaitForm = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::LaRenzo.Forms.WaitFormGeneral), true, true);
			this.dtBegin = new System.Windows.Forms.DateTimePicker();
			this.btnExport = new System.Windows.Forms.Button();
			this.mainPnl = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.linqServerModeSourceAllOrders)).BeginInit();
			this.OrderContextMenu.SuspendLayout();
			this.mainPnl.SuspendLayout();
			this.SuspendLayout();
			// 
			// linqServerModeSourceAllOrders
			// 
			this.linqServerModeSourceAllOrders.KeyExpression = "ID";
			// 
			// OrderContextMenu
			// 
			this.OrderContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PrintOrderCheck,
            this.DeleteOrder});
			this.OrderContextMenu.Name = "SessionContextMenu";
			this.OrderContextMenu.Size = new System.Drawing.Size(160, 48);
			// 
			// PrintOrderCheck
			// 
			this.PrintOrderCheck.Name = "PrintOrderCheck";
			this.PrintOrderCheck.Size = new System.Drawing.Size(159, 22);
			this.PrintOrderCheck.Text = "Напечатать чек";
			this.PrintOrderCheck.Click += new System.EventHandler(this.PrintOrderCheck_Click);
			// 
			// DeleteOrder
			// 
			this.DeleteOrder.Enabled = false;
			this.DeleteOrder.Name = "DeleteOrder";
			this.DeleteOrder.Size = new System.Drawing.Size(159, 22);
			this.DeleteOrder.Text = "Удалить заказ";
			this.DeleteOrder.Click += new System.EventHandler(this.DeleteOrder_Click);
			// 
			// lblLastOrders
			// 
			this.lblLastOrders.AutoSize = true;
			this.lblLastOrders.Location = new System.Drawing.Point(5, 8);
			this.lblLastOrders.Name = "lblLastOrders";
			this.lblLastOrders.Size = new System.Drawing.Size(33, 13);
			this.lblLastOrders.TabIndex = 5;
			this.lblLastOrders.Text = "Дата";
			// 
			// btnGenerate
			// 
			this.btnGenerate.Location = new System.Drawing.Point(431, 3);
			this.btnGenerate.Name = "btnGenerate";
			this.btnGenerate.Size = new System.Drawing.Size(132, 23);
			this.btnGenerate.TabIndex = 6;
			this.btnGenerate.Text = "Получить данные";
			this.btnGenerate.UseVisualStyleBackColor = true;
			this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
			// 
			// backgroundWorkerGenerate
			// 
			this.backgroundWorkerGenerate.WorkerSupportsCancellation = true;
			this.backgroundWorkerGenerate.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerGenerate_DoWork);
			this.backgroundWorkerGenerate.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerGenerate_RunWorkerCompleted);
			// 
			// dtBegin
			// 
			this.dtBegin.Location = new System.Drawing.Point(65, 6);
			this.dtBegin.Name = "dtBegin";
			this.dtBegin.Size = new System.Drawing.Size(118, 20);
			this.dtBegin.TabIndex = 9;
			// 
			// btnExport
			// 
			this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnExport.Location = new System.Drawing.Point(770, 3);
			this.btnExport.Name = "btnExport";
			this.btnExport.Size = new System.Drawing.Size(124, 23);
			this.btnExport.TabIndex = 13;
			this.btnExport.Text = "Экспорт";
			this.btnExport.UseVisualStyleBackColor = true;
			this.btnExport.Visible = false;
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			// 
			// mainPnl
			// 
			this.mainPnl.Controls.Add(this.btnExport);
			this.mainPnl.Controls.Add(this.lblLastOrders);
			this.mainPnl.Controls.Add(this.btnGenerate);
			this.mainPnl.Controls.Add(this.dtBegin);
			this.mainPnl.Dock = System.Windows.Forms.DockStyle.Top;
			this.mainPnl.Location = new System.Drawing.Point(0, 0);
			this.mainPnl.Name = "mainPnl";
			this.mainPnl.Size = new System.Drawing.Size(897, 30);
			this.mainPnl.TabIndex = 14;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(29, 54);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(61, 13);
			this.label1.TabIndex = 15;
			this.label1.Text = "Вх. звонки";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(29, 77);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(58, 13);
			this.label2.TabIndex = 16;
			this.label2.Text = "Вх.заявки";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(29, 102);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(86, 13);
			this.label3.TabIndex = 17;
			this.label3.Text = "Кол-во заказов";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(29, 125);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(85, 13);
			this.label4.TabIndex = 18;
			this.label4.Text = "Кол-во отказов";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(29, 147);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(40, 13);
			this.label5.TabIndex = 19;
			this.label5.Text = "Ср.чек";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(29, 170);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(50, 13);
			this.label6.TabIndex = 20;
			this.label6.Text = "Выручка";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.BackColor = System.Drawing.Color.White;
			this.label7.Location = new System.Drawing.Point(149, 170);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(25, 13);
			this.label7.TabIndex = 26;
			this.label7.Text = "      ";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.BackColor = System.Drawing.Color.White;
			this.label8.Location = new System.Drawing.Point(149, 147);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(25, 13);
			this.label8.TabIndex = 25;
			this.label8.Text = "      ";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.BackColor = System.Drawing.Color.White;
			this.label9.Location = new System.Drawing.Point(149, 125);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(25, 13);
			this.label9.TabIndex = 24;
			this.label9.Text = "      ";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.BackColor = System.Drawing.Color.White;
			this.label10.Location = new System.Drawing.Point(149, 102);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(25, 13);
			this.label10.TabIndex = 23;
			this.label10.Text = "      ";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.BackColor = System.Drawing.Color.White;
			this.label11.Location = new System.Drawing.Point(149, 77);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(25, 13);
			this.label11.TabIndex = 22;
			this.label11.Text = "      ";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.BackColor = System.Drawing.Color.White;
			this.label12.Location = new System.Drawing.Point(149, 54);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(25, 13);
			this.label12.TabIndex = 21;
			this.label12.Text = "      ";
			// 
			// DayOrdersForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(897, 500);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.mainPnl);
			this.Name = "DayOrdersForm";
			this.Text = "Ежедневный отчёт по продажам";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AllOrdersForm_FormClosed);
			this.Load += new System.EventHandler(this.AllOrdersForm_Load);
			((System.ComponentModel.ISupportInitialize)(this.linqServerModeSourceAllOrders)).EndInit();
			this.OrderContextMenu.ResumeLayout(false);
			this.mainPnl.ResumeLayout(false);
			this.mainPnl.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

		public DevExpress.Data.Linq.LinqServerModeSource linqServerModeSourceAllOrders;
        private System.Windows.Forms.ContextMenuStrip OrderContextMenu;
        private System.Windows.Forms.ToolStripMenuItem PrintOrderCheck;
		private System.Windows.Forms.ToolStripMenuItem DeleteOrder;
        private System.Windows.Forms.Label lblLastOrders;
        private System.Windows.Forms.Button btnGenerate;
		private System.ComponentModel.BackgroundWorker backgroundWorkerGenerate;
		private System.Windows.Forms.DateTimePicker dtBegin;
        private System.Windows.Forms.Button btnExport;
		private System.Windows.Forms.Panel mainPnl;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManagerWaitForm;

    }
}