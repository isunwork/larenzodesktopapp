﻿#region File Info

// Author: Sxquer
// Project : Manager
// Filename : SessionsForm.cs
// Created : 24/10/2012 (12:22)
// Last Modified : 24/10/2012 (12:46)

#endregion

using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Entities.Documents.Sessions;
using LaRenzo.DataRepository.Entities.PrintWorksModels;
using LaRenzo.DataRepository.Entities.States;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Printers;
using LaRenzo.DataRepository.Repositories.Documents.Orders;
using LaRenzo.DataRepository.Repositories.Documents.Sessions;
using LaRenzo.Forms.Documents.DeliveryOrderForms;
using LaRenzo.Properties;

namespace LaRenzo.Forms.HistoryForms
{
    public partial class SessionsForm : Form
    {
        public SessionsForm()
        {
            InitializeComponent();
            mainGridControl.DataSource = Content.SessionManager.GetFullList();
            moveOrdersToolStripMenuItem.Click += async (s, e) => await moveOrdersToolStripMenuItem_Click();
            detailGridView.DoubleClick += async (s, e) => await gridView2_DoubleClick(s);
            
            PrintOrderCheck.Click += async (s, e) => await PrintOrderCheck_Click();
        }

        private void gridView1_MasterRowGetChildList(object sender,
                                                     MasterRowGetChildListEventArgs
                                                         e)
        {
            if (mainGridView != null)
            {
                var session = mainGridView.GetRow(e.RowHandle) as Session;
                if (session != null)
                    e.ChildList = Content.SessionManager.GetOrdersInSession(session.ID);
            }
        }

        private void gridView1_MasterRowGetRelationCount(object sender,
                                                         MasterRowGetRelationCountEventArgs
                                                             e)
        {
            e.RelationCount = 1;
        }

        private void gridView1_MasterRowGetRelationDisplayCaption(object sender,
                                                                  MasterRowGetRelationNameEventArgs
                                                                      e)
        {
            e.RelationName = "Заказы";
        }

        private void gridView1_MasterRowGetRelationName(object sender,
                                                        MasterRowGetRelationNameEventArgs
                                                            e)
        {
            e.RelationName = "Orders";
        }

        private void напечататьЧекToolStripMenuItem_Click(object sender,
                                                          EventArgs e)
        {
            try
            {
                if (mainGridView.IsDataRow(mainGridView.FocusedRowHandle))
                {
                    var id =
                        (int)
                        mainGridView.GetRowCellValue(mainGridView.FocusedRowHandle,
                                                  columnIdCol);

                    var session = Content.SessionManager.GetById(id);

                    if (session != null)
                    {
                        string message = String.Empty;
                        var pointOfSales = Content.PointOfSaleRepository.GetItems();
                        foreach (var point in pointOfSales)
                        {
                            message = PrintingManager.EndSessionCheckPrint(session, point.ID);
                        }

                        if (!string.IsNullOrEmpty(message))
                        {
                            MessageBox.Show(message);
                            Content.PrintWorksRepository.AddPrintWork(new PrintWork
                            {
                                Created = DateTime.Now,
                                IsSession = true,
                                SessionId = id,
                                State = (int)State.Error
                            });
                        }
                        else
                        {
                            Content.PrintWorksRepository.AddPrintWork(new PrintWork
                            {
                                Created = DateTime.Now,
                                IsSession = true,
                                SessionId = id,
                                State = (int)State.Compleated
                            });
                        }
                    }
                    else
                    {
                        MessageBox.Show(string.Format("Сессия с id={0} не найдена", id), Resources.WarningTitle,
                                        MessageBoxButtons.OK);
                    }
                }
                else
                {
                    MessageBox.Show(
                        Resources.SelectSessionStringForPrint);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gridView1_PopupMenuShowing(object sender,
                                                PopupMenuShowingEventArgs e)
        {
            var view = sender as GridView;

            if (view != null)
            {
                GridHitInfo hitInfo = view.CalcHitInfo(e.Point);
                if (view.IsDataRow(hitInfo.RowHandle))
                    SessionContextMenu.Show(view.GridControl, e.Point);
            }
        }

        private async Task gridView2_DoubleClick(object sender)
        {
            var view = (GridView)sender;

            Point pt = view.GridControl.PointToClient(MousePosition);

            GridHitInfo info = view.CalcHitInfo(pt);
            if (view.IsDataRow(view.FocusedRowHandle))
            {
                try
                {
                    var form = new DeliveryOrderForm(DetailsFormModes.ViewByManager,
                                               (int)
                                               view.GetRowCellValue(
                                                   info.RowHandle, columnIdCol));
                    await form.Initialize();
                    form.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void gridView2_PopupMenuShowing(object sender,
                                                PopupMenuShowingEventArgs e)
        {
            var view = sender as GridView;

            if (view != null)
            {
                GridHitInfo hitInfo = view.CalcHitInfo(e.Point);
                if (view.IsDataRow(hitInfo.RowHandle))
                    OrderContextMenu.Show(view.GridControl, e.Point);
            }
        }

        private async Task PrintOrderCheck_Click()
        {
            try
            {
                var view = (mainGridControl.FocusedView as GridView);

                if (view != null && view.IsDataRow(view.FocusedRowHandle))
                {
                    var id =
                        (int)
                        view.GetRowCellValue(view.FocusedRowHandle, DishIdCol);

                    Order order = Content.OrderManager.GetById(id);
                    int workId = Content.PrintWorksRepository.AddPrintWork(new PrintWork
                    {
                        Created = DateTime.Now,
                        OrderId = id,
                        State = (int)State.Processing
                    });

                    string message = await PrintingManager.Print(order, Settings.Default.PrinterPort, Settings.Default.PrinterIp, workId);
                    if (!string.IsNullOrEmpty(message))
                    {
                        MessageBox.Show(message);
                        Content.PrintWorksRepository.UpdatePrintWorkState(workId, State.Error);
                    }
                    else
                    {
                        Content.PrintWorksRepository.UpdatePrintWorkState(workId, State.Compleated);
                    }

                }
                else
                {
                    MessageBox.Show(
                        Resources.SelectOrderStringForPrint);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async Task moveOrdersToolStripMenuItem_Click()
        {
            var id =
                (int)
                mainGridView.GetRowCellValue(mainGridView.FocusedRowHandle,
                    columnIdCol);

            var sessionOld = Content.SessionManager.GetById(id);
            var sessionCurrent = await Content.SessionManager.GetOpened();
            Content.SessionManager.TransferOrders(sessionOld, sessionCurrent);
        }
    }
}