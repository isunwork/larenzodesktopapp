﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using DevExpress.Data;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Catalogs.Tables;
using LaRenzo.DataRepository.Entities.Documents.CafeOrders;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Repositories.Catalogs.Printers;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Documents.Sessions;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Documents.CafeOrderForms
{
    public partial class CafePlaceControl : UserControl
    {
        /// <summary> Документ заказа </summary>
        //private readonly CafeOrderDocument document;
        /// <summary> Позиции заказа </summary>
        private List<CafeOrderDocumentItem> documentItems = new List<CafeOrderDocumentItem>();
        /// <summary> Скидочная карта </summary>
        private DiscountCard discountCard;
        private ErrorProvider _errorProvider;

        private Table table;

        private Table Table
        {
            set
            {
                table = value;
                txtTable.Text = value.Name;
            }

            get { return table; }
        }

        /// <summary> Редактируется заказ(true) или новый (false)</summary>
        private bool IsEdit { set; get; }

        private bool CanEdit { set; get; }

        /// <summary> Номер места </summary>
        public int PlaceIndex;

        private bool isProcessed;

        private int id = 0;

        private async Task<CafeOrderDocument> GetCafeDocument()
        {
            var cafeDoc = new CafeOrderDocument();

            cafeDoc.ID = id;

            // Дата документа
            cafeDoc.Created = dtnDocDate.Value;

            // Id стола
            cafeDoc.TableID = Table.ID;

            // Скидочная карта
            cafeDoc.DiscountCardID = DiscountCard == null ? (int?)null : DiscountCard.ID;

            // Завершённость заказа
            cafeDoc.IsProcessed = isProcessed;

            // Сумма заказа
            float documentSum = documentItems.Sum(x => x.Sum);

            if (DiscountCard != null)
            {
                var discountRate = GetDiscountRate(DiscountCard.Discount);
                documentSum = documentSum * discountRate;
            }
            cafeDoc.DocumentSum = (decimal)documentSum;
            cafeDoc.PointOfSaleId = (int)comboBoxPointOfSale.SelectedValue;

            // Если редактирование

            // Номер стола
            cafeDoc.Table = Table;

            // Номре места
            cafeDoc.PlaceIndex = PlaceIndex;

            // Номер заказа
            cafeDoc.Code = txtNumber.Text;

            // Комментирии
            cafeDoc.Comment = rtxtComment.Text;
            cafeDoc.Note = rtxtComment.Text;

            // Позиции в заказе
            cafeDoc.CafeOrderItems = documentItems;

            // Тип оплаты
            cafeDoc.PaymentType = PaymentType;

            // Кол-во персон
            cafeDoc.PersonsCount = (int)personNumericUpDown.Value;

            // Сессия
            cafeDoc.Session = await Content.SessionManager.GetOpened();

            if (cafeDoc.Session != null)
            {
                cafeDoc.SessionID = cafeDoc.Session.ID;
            }

            // Номер места
            cafeDoc.PlaceIndex = PlaceIndex;
            ChangeOpenCloseStyle(cafeDoc.IsProcessed, cafeDoc.CafeOrderItems);
            return cafeDoc;
        }

        private CafeOrderDocument CafeDocument
        {
            set
            {
                if (value != null)
                {
                    IsEdit = true;

                    id = value.ID;

                    // Тип оплаты
                    PaymentType = value.PaymentType;

                    // Комментирии
                    rtxtComment.Text = value.Comment;

                    // Номер заказа
                    txtNumber.Text = value.Code;

                    // Кол-во персон
                    personNumericUpDown.Value = value.PersonsCount;

                    //
                    isProcessed = value.IsProcessed;

                    // Скидочная карта
                    DiscountCard = value.DiscountCard;

                    ChangeOpenCloseStyle(value.IsProcessed, value.CafeOrderItems);

                    // Позиции в заказе
                    documentItems = (List<CafeOrderDocumentItem>)value.CafeOrderItems;
                    UpdateDataGreed(documentItems);
                }
                else
                {
                    ClearPlace();
                    UpdateDataGreed(documentItems);
                }
            }
        }

        public void SetDocByAdmin(CafeOrderDocument cafeDocument)
        {
            CafeDocument = cafeDocument;
        }

        private void ChangeOpenCloseStyle(bool isProcessedValue, IList<CafeOrderDocumentItem> cafeOrderItems)
        {
            if (!isProcessedValue && cafeOrderItems.Any(x => x.ID > 0))
            {
                btnTableState.Text = Resources.CloseTable;
                btnTableState.ForeColor = Color.Green;
                btnTableState.Visible = true;
            }
            else
            {
                btnTableState.Visible = false;
            }
        }


        public void Init(Table cafeTable, int place)
        {
            PlaceIndex = place;

            if (cafeTable != null)
            {
                Table = cafeTable;

                var codf = new CafeOrderDocumentFilter
                {
                    IsOpen = true,
                    Position = place,
                    CurrentTable = cafeTable,
                    IsPositionFilter = true
                };

                CafeOrderDocument cod = Content.CafeOrderDocumentRepository.GetItem(codf);

                CafeDocument = cod;
            }
        }


        private DiscountCard DiscountCard
        {
            set
            {
                discountCard = value;
            }
            get
            {
                return discountCard;
            }
        }

        public CafePlaceControl()
        {
            InitializeComponent();
            comboBoxPointOfSale.DisplayMember = "PointName";
            comboBoxPointOfSale.ValueMember = "Id";
            if (!DesignMode)
            {
                var points = Content.PointOfSaleRepository.GetItems();
                comboBoxPointOfSale.DataSource = points;
                comboBoxPointOfSale.SelectedValue = points.FirstOrDefault()?.ID;
            }
            _errorProvider = new ErrorProvider();
        }


        /// <summary>
        /// Закрыть столик
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnTableState_Click(object sender, EventArgs e)
        {
            if (!ValidateCafeOrder())
            {
                MessageBox.Show("Не корректно заполнены данные в заказе");
                return;
            }
            if (!isProcessed)
            {
                isProcessed = true;
                await SaveCafeOrder();
                var doc = await GetCafeDocument();
                PrintingManager.PrintCaffeClientCheck(doc);
                // Печать на налоговый принтер
                PrintingManagerElves.PrintToNalogPrinter(doc.DocumentSum, doc.PaymentType, doc.CafeOrderItems);
                
                ClearPlace();
            }

            var form = FindForm();
            if (form is CafeOrderForm orderForm)
            {
                orderForm.updateStateTimer_Tick(null, null);
                orderForm.Close();
            }
        }

        public async Task<int> CountDishes()
        {
            var doc = await GetCafeDocument();
            return doc.CafeOrderItems.Count(x => x.ID > 0);
        }

        private void ClearPlace()
        {
            id = 0;
            documentItems = new List<CafeOrderDocumentItem>();
            UpdateDataGreed(documentItems);

            PaymentType = 0;
            IsEdit = false;

            DiscountCard = null;
        }


        public bool ValidateCafeOrder()
        {
            int personsCount = (int)personNumericUpDown.Value;
            if (personsCount <= 0)
            {
                _errorProvider.SetError(personNumericUpDown, "Укажите количестов персон");
                return false;
            }
            _errorProvider.SetError(personNumericUpDown, "");
            return true;
        }

        //__________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Сохранить заказ кафе </summary>
        ////=========================================
        public async Task SaveCafeOrder()
        {
            CafeOrderDocument cafeDocument = await GetCafeDocument();

            if (cafeDocument.CafeOrderItems.Count > 0 || IsEdit)
            {
                if (cafeDocument.ID == 0)
                {
                    await Content.CafeOrderDocumentRepository.InsertItemAsync(cafeDocument);

                    id = cafeDocument.ID;
                }
                else
                {
                    Content.CafeOrderDocumentRepository.UpdateItem(cafeDocument);
                }
            }
        }


        //________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Посчитать скидку </summary>
        ////=====================================
        private float GetDiscountRate(float discount)
        {
            return (100 - discount) / 100;
        }


        //________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Тип оплаты </summary>
        ////===============================
        private PaymentType PaymentType
        {
            get
            {
                PaymentType paymentType;

                if (moneyRadioButton.Checked)
                {
                    paymentType = PaymentType.Money;
                }
                else if (cardRadioButton.Checked)
                {
                    paymentType = PaymentType.Card;
                }
                else
                {
                    paymentType = PaymentType.NoMoney;
                }

                return paymentType;
            }
            set
            {
                switch (value)
                {
                    case PaymentType.Money:
                        moneyRadioButton.Checked = true;
                        break;
                    case PaymentType.Card:
                        cardRadioButton.Checked = true;
                        break;
                    case PaymentType.NoMoney:
                        onlineRadioButton.Checked = true;
                        break;
                }
            }
        }


        public void AddDish(Dish dish, bool isEditable = true)
        {
            // Если оно не пустое
            if (dish != null)
            {
                // Найти блюдо в списке заказанных
                CafeOrderDocumentItem foundDish = documentItems.FirstOrDefault(x => x.Dish.ID == dish.ID && x.IsEditable);

                // Если блюдо есть в заказе, увеличить кол-во
                if (foundDish != null && foundDish.IsEditable)
                {
                    foundDish.Amount += 1;
                    foundDish.Sum = foundDish.Amount * foundDish.Price;
                }
                else  // Если нет, добавить в заказ
                {
                    documentItems.Add(new CafeOrderDocumentItem
                    {
                        ID = -1,
                        DishID = dish.ID,
                        Dish = dish,
                        Amount = 1,
                        Price = dish.CafePrice,
                        Sum = dish.CafePrice,
                        IsEditable = isEditable
                    });
                }
                UpdateDataGreed(documentItems);
            }
        }


        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            var currentRow = (CafeOrderDocumentItem)view.GetRow(e.RowHandle);

            if (!currentRow.IsEditable)
            {
                e.Appearance.BeginUpdate();

                e.Appearance.ForeColor = Color.LightGray;

                e.Appearance.EndUpdate();
            }
        }

        private void mainGridView_DoubleClick(object sender, EventArgs e)
        {
            var item = (CafeOrderDocumentItem)mainGridView.GetRow(mainGridView.FocusedRowHandle);

            if (item == null)
                return;

            if (!item.IsEditable && !CanEdit && !Content.UserManager.IsAdmin())
                return;

            SubDish(item);
        }

        private void SubDish(CafeOrderDocumentItem item)
        {
            CafeOrderDocumentItem foundedDish = documentItems.FirstOrDefault(x => x.ID == item.ID);

            if (foundedDish != null && foundedDish.Amount > 1)
            {
                foundedDish.Amount -= 1;
                foundedDish.Sum = foundedDish.Amount * foundedDish.Price;
            }
            else
            {
                documentItems.Remove(item);
            }

            UpdateDataGreed(documentItems);
        }


        private void UpdateDataGreed(IList<CafeOrderDocumentItem> docItems)
        {
            // Хня для обновления дев-грида
            mainGridView.BeginSummaryUpdate();
            mainGridView.BeginUpdate();

            // Отобразить в списке заказа
            mainGridControl1.DataSource = docItems;

            mainGridView.UpdateSummary();

            mainGridView.EndUpdate();
            mainGridView.EndSummaryUpdate();
        }


        public async Task TakeOrder()
        {
            if (!ValidateCafeOrder())
            {
                MessageBox.Show("Не корректно заполнены данные в заказе");
                return;
            }
            await SaveCafeOrder();

            await PrintCafeCheck();
        }


        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Напечатать чек для кафе </summary>
        ////============================================
        public async Task PrintCafeCheck()
        {
            try
            {
                CafeOrderDocument cafeDocument = await GetCafeDocument(); ;

                // Печать чека по отделам кухни
                PrintingManager.PrintCheckToAllKitchenSectionFromKitchen(cafeDocument, null, documentItems);

                PrintingManager.PrintSectionCafeCheck(cafeDocument);
                PrintingManager.PrintSectionCafeCheck(cafeDocument);
                PrintingManager.PrintSectionBarCheck(cafeDocument);

                if (Table != null && Table.ID == -1)
                {
                    PrintingManager.PrintCaffeClientCheck(cafeDocument);
                    // Печать на налоговый принтер
                    PrintingManagerElves.PrintToNalogPrinter(cafeDocument.DocumentSum, cafeDocument.PaymentType, cafeDocument.CafeOrderItems);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }


        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Напечатать чек для кафе </summary>
        ////============================================
        public async Task PrintCafeCheckToClient()
        {
            if (!ValidateCafeOrder())
            {
                MessageBox.Show("Не корректно заполнены данные в заказе");
                return;
            }
            await SaveCafeOrder();

            var doc = await GetCafeDocument();
            PrintingManager.PrintCaffeClientCheck(doc);
            // Печать на налоговый принтер
            PrintingManagerElves.PrintToNalogPrinter(doc.DocumentSum, doc.PaymentType, doc.CafeOrderItems);
        }


        private void numDiscountCard_ValueChanged(object sender, EventArgs e)
        {
            if (numDiscountCard.Value == 0)
            {
                numDiscountCard.BackColor = Color.White;
                DiscountCard = null;
            }
            else
            {
                DiscountCard = Content.DiscountCardManager.GetByNumber((int)numDiscountCard.Value);
                numDiscountCard.BackColor = DiscountCard == null ? Color.Tomato : Color.PaleGreen;
            }

            mainGridView.RefreshData();
        }


        private void mainGridView_CustomSummaryCalculate(object sender, CustomSummaryEventArgs e)
        {
            if (e != null)
            {
                if (e.SummaryProcess == CustomSummaryProcess.Start)
                {
                    e.TotalValue = 0;
                }

                if (e.SummaryProcess == CustomSummaryProcess.Calculate)
                {
                    e.TotalValue = (int)e.TotalValue + (int)e.FieldValue;
                }

                if (e.SummaryProcess == CustomSummaryProcess.Finalize)
                {
                    if (DiscountCard != null && e != null && e.TotalValue != null)
                    {
                        var sum = (int)e.TotalValue * GetDiscountRate(DiscountCard.Discount);
                        var discount = (int)e.TotalValue - sum;
                        e.TotalValue = string.Format("{0} р. (Скидка {1} р.)", sum, discount.ToString("N"));
                    }
                }
            }
        }

        private void numDiscountCard_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(numDiscountCard.Text)) numDiscountCard.Value = 0;
        }

        private void mainGridView_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            var view = sender as GridView;

            if (view != null)
            {
                GridHitInfo hitInfo = view.CalcHitInfo(e.Point);

                if (view.IsDataRow(hitInfo.RowHandle))
                {
                    // Заказ, по которому кликнули в гриде
                    SendToAtherPlaceCafeOrder = mainGridView.GetRow(hitInfo.RowHandle) as CafeOrderDocumentItem;

                    contextMenuStrip.Show(view.GridControl, e.Point);
                }
            }
        }

        public CafeOrderDocumentItem SendToAtherPlaceCafeOrder;

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SendCafeOrderEvent(SendToAtherPlaceCafeOrder, 1);

            SubDish(SendToAtherPlaceCafeOrder);
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            SendCafeOrderEvent(SendToAtherPlaceCafeOrder, 2);

            SubDish(SendToAtherPlaceCafeOrder);
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            SendCafeOrderEvent(SendToAtherPlaceCafeOrder, 3);

            SubDish(SendToAtherPlaceCafeOrder);
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            SendCafeOrderEvent(SendToAtherPlaceCafeOrder, 4);

            SubDish(SendToAtherPlaceCafeOrder);
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            SendCafeOrderEvent(SendToAtherPlaceCafeOrder, 5);

            SubDish(SendToAtherPlaceCafeOrder);
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            SendCafeOrderEvent(SendToAtherPlaceCafeOrder, 6);

            SubDish(SendToAtherPlaceCafeOrder);
        }


        public event EventHandler<SendCafeOrderEventArgs> OnSendCafeOrderEvent;

        protected virtual void SendCafeOrderEvent(CafeOrderDocumentItem cafeOrderDocumentItem, int toPlace)
        {
            SendCafeOrderEventArgs e = new SendCafeOrderEventArgs(cafeOrderDocumentItem, toPlace);

            EventHandler<SendCafeOrderEventArgs> handler = OnSendCafeOrderEvent;

            // Event will be null if there are no subscribers
            if (handler != null)
            {
                // Use the () operator to raise the event.
                handler(this, e);
            }
        }


        public delegate void SendCafeOrderEventHandler(object sender, SendCafeOrderEventArgs a);

        private void personNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            ValidateCafeOrder();
        }
    }


    public class SendCafeOrderEventArgs : EventArgs
    {
        public SendCafeOrderEventArgs(CafeOrderDocumentItem codi, int goToPlace)
        {
            cafeOrderDocumentItem = codi;
            toPlace = goToPlace;
        }

        private CafeOrderDocumentItem cafeOrderDocumentItem;

        public CafeOrderDocumentItem CafeOrderDocumentItem
        {
            get { return cafeOrderDocumentItem; }
        }

        private int toPlace;

        public int ToPlace
        {
            get { return toPlace; }
        }

    }

}
