﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataRepository.Entities;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Catalogs.Tables;
using LaRenzo.DataRepository.Entities.Documents.CafeOrders;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using NLog;


namespace LaRenzo.Forms.Documents.CafeOrderForms
{
    public partial class CafeOrderForm : Form
    {
        private readonly Table _table;

        private readonly int _placeNumber;

        public CafeOrderForm()
        {
            InitializeComponent();
        }


        //__________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> При открытии из главного окна "столики кафе" </summary>
        ////=================================================================
        public CafeOrderForm(FilterModel filter) : this()
        {
            var value = (CafeOrderDocumentFilter)filter;

            _table = value.CurrentTable;

            if (_table.Name[_table.Name.Length - 2].ToString() == ".")
            {
                _placeNumber = Convert.ToInt32(_table.Name[_table.Name.Length - 1].ToString());
            }
            InitTabs(_placeNumber);
        }


        public CafeOrderForm(CafeOrderDocument document) : this()
        {
            _table = document.Table;

            InitTabs(0);
            cafePlaceControl1.SetDocByAdmin(document);
        }


        //_________________________________________________________________________________________________________________________________________________________________________________________
        private void InitTabs(int placeNumberParam)
        {
            cafePlaceControl1.Init(_table, placeNumberParam);
            cafePlaceControl1.OnSendCafeOrderEvent += cafePlaceControl_OnSendCafeOrderEvent;
        }

        void cafePlaceControl_OnSendCafeOrderEvent(object sender, SendCafeOrderEventArgs e)
        {
            if (e.CafeOrderDocumentItem != null)
            {
                switch (e.ToPlace)
                {
                    case 1:
                        cafePlaceControl1.AddDish(e.CafeOrderDocumentItem.Dish, e.CafeOrderDocumentItem.IsEditable);
                        break;
                }
            }
        }
        //_________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Загрузка меню и прошлых заказов </summary>
        ////====================================================
        private void LoadFormsEntityList()
        {
            var brand = Content.BrandManager.GetList(new BrandFilter { BrandName = "Ларензо" }).FirstOrDefault();
            // Категории меню
            if (brand != null)
            {
                LoadCategoryList(Content.CategoryRepository.GetItems().Where(x => x.Brand.ID == brand.ID).OrderBy(y => y.Weight));

                // Блюда
                LoadDishList(Content.DishRepository.GetItems(new DishFilter { FilterByBrandId = true, BrandId = brand.ID }));
            }
        }


        //_________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Загрузка категории меню </summary>
        ////============================================
        private void LoadCategoryList(IEnumerable<Category> items)
        {
            lstCategory.Items.Add("Все");

            if (items != null)
            {
                IList<Category> categories = items as IList<Category> ?? items.ToList();

                foreach (Category item in categories)
                {
                    lstCategory.Items.Add(item.Name);
                }

            }
        }

        private void LoadDishList(IEnumerable<Dish> items)
        {
            lstDish.Items.Clear();

            foreach (Dish item in items)
            {
                lstDish.Items.Add(item);
            }
        }


        //_________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> При загрузки формы </summary>
        ////=======================================
        private void CafeOrderFormLoad(object sender, EventArgs e)
        {
            // Загрузить списки меню
            LoadFormsEntityList();

            // Если админ, можно сохранять и печатать
            btnSaveOrder.Visible = btnPrint.Visible = Content.UserManager.IsAdmin();

        }




        //_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Клик по категории </summary>
        ////======================================
        private void lstCategory_MouseClick(object sender, MouseEventArgs e)
        {
            var category = (string)lstCategory.Items[lstCategory.SelectedIndex];
            var brand = Content.BrandManager.GetList(new BrandFilter { BrandName = "Ларензо" }).FirstOrDefault();
            if (brand != null)
            {
                IEnumerable<Dish> dishList = (category == "Все")
                    ? Content.DishRepository.GetItems(new DishFilter
                    {
                        BrandId = brand.ID
                    })
                    : Content.DishRepository.GetItems(new DishFilter
                    {
                        CategoryName = category,
                        FilterByCategoryName = true,
                        BrandId = brand.ID

                    });

                LoadDishList(dishList);
            }
        }


        //________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Кнопка "Отменить" </summary>
        ////======================================
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }


        //________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Клик по блюду </summary>
        ////==================================
        private void LstDishMouseDoubleClick(object sender, MouseEventArgs e)
        {
            // Выбранное блюдо
            var dish = (Dish)lstDish.SelectedItem;

            // Если оно не пустое
            if (dish != null)
            {
                SetDishToPlace(dish);
            }
        }


        //________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Установить блюдо в нужный таб </summary>
        ////==================================================
        private void SetDishToPlace(Dish dish)
        {
            switch (tabControl1.SelectedIndex)
            {
                case 0:
                    cafePlaceControl1.AddDish(dish);
                    break;
            }
        }


        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Кнопка "Заказать" </summary>
        ////======================================
        private async void BtnSaveClick(object sender, EventArgs e)
        {
            await GetOrderByTab();
        }


        private async Task GetOrderByTab()
        {
            switch (tabControl1.SelectedIndex)
            {
                case 0:
                    await cafePlaceControl1.TakeOrder();
                    break;
            }
        }
        
        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Кнопка "Сохранить" </summary>
        ////=======================================
        private async void BtnSaveOrderClick(object sender, EventArgs e)
        {
            await SaveAllTab();
        }
       
        private async Task SaveAllTab()
        {
            if (!cafePlaceControl1.ValidateCafeOrder())
            {
                MessageBox.Show("Не корректно заполнены данные в заказе");
                return;
            }
            await cafePlaceControl1.SaveCafeOrder();
        }
        
        private async void BtnPrintClick(object sender, EventArgs e)
        {
            await PrintClientCheckByTab();
        }

        private async Task PrintClientCheckByTab()
        {
            switch (tabControl1.SelectedIndex)
            {
                case 0:
                    await cafePlaceControl1.PrintCafeCheckToClient();
                    break;
            }
        }

        public async void updateStateTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                bool isTableBusy = await SetTab(tabPage1, cafePlaceControl1, _placeNumber.ToString());

                switch (_placeNumber)
                {
                    case 0:
                        _table.IsOpen = isTableBusy;
                        break;
                    case 1:
                        _table.IsOpenP1 = isTableBusy;
                        break;
                    case 2:
                        _table.IsOpenP2 = isTableBusy;
                        break;
                    case 3:
                        _table.IsOpenP3 = isTableBusy;
                        break;
                    case 4:
                        _table.IsOpenP4 = isTableBusy;
                        break;
                }

                Content.TableRepository.UpdateItem(_table);
            }
            catch (Exception ex)
            {
                var logger = LogManager.GetCurrentClassLogger();
                logger.Info($"Ошибка при обновлении информации о столиках - {_table?.ID},{tabPage1},{cafePlaceControl1},{_placeNumber}");
                logger.Error(ex);
            }
        }
        
        private async Task<bool> SetTab(TabPage tp, CafePlaceControl cpc, string place)
        {
            bool isTableBusy = false;
            var countOfDishes = await cpc.CountDishes();
            if (countOfDishes > 0)
            {
                isTableBusy = true;
                await SetHeadText(tp, cpc, place);
            }
            else
            {
                await SetHeadText(tp, cpc, place);
            }

            return isTableBusy;
        }

        private static async Task SetHeadText(TabPage tp, CafePlaceControl cpc, string place)
        {
            if (place == "0")
            {
                tp.Text = "Стол";
            }
            else
            {
                var countDishes = await cpc.CountDishes();
                if (countDishes > 0)
                {
                    tp.Text = "Место " + place + " (" + countDishes + ")";
                }
                else
                {
                    tp.Text = "Место " + place;
                }
            }
        }

        private void CafeOrderForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            updateStateTimer_Tick(null, null);
        }
    }
}