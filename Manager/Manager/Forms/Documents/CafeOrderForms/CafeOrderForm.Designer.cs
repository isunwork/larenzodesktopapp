﻿using System.Windows.Forms;

namespace LaRenzo.Forms.Documents.CafeOrderForms
{
    partial class CafeOrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cafePlaceControl1 = new LaRenzo.Forms.Documents.CafeOrderForms.CafePlaceControl();
            this.mainSplit = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.lstCategory = new DevExpress.XtraEditors.ListBoxControl();
            this.lstDish = new DevExpress.XtraEditors.ListBoxControl();
            this.rightSplit = new DevExpress.XtraEditors.SplitContainerControl();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panelCheckOut = new DevExpress.XtraEditors.PanelControl();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnSaveOrder = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.updateStateTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplit)).BeginInit();
            this.mainSplit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lstCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lstDish)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightSplit)).BeginInit();
            this.rightSplit.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelCheckOut)).BeginInit();
            this.panelCheckOut.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // cafePlaceControl1
            // 
            this.cafePlaceControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cafePlaceControl1.Location = new System.Drawing.Point(4, 4);
            this.cafePlaceControl1.Margin = new System.Windows.Forms.Padding(5);
            this.cafePlaceControl1.Name = "cafePlaceControl1";
            this.cafePlaceControl1.Size = new System.Drawing.Size(957, 599);
            this.cafePlaceControl1.TabIndex = 0;
            // 
            // mainSplit
            // 
            this.mainSplit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainSplit.Location = new System.Drawing.Point(11, 10);
            this.mainSplit.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.mainSplit.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.mainSplit.Margin = new System.Windows.Forms.Padding(4);
            this.mainSplit.Name = "mainSplit";
            this.mainSplit.Panel1.Controls.Add(this.splitContainerControl1);
            this.mainSplit.Panel1.Text = "Panel1";
            this.mainSplit.Panel2.Controls.Add(this.rightSplit);
            this.mainSplit.Panel2.Text = "Panel2";
            this.mainSplit.Size = new System.Drawing.Size(1342, 701);
            this.mainSplit.SplitterPosition = 364;
            this.mainSplit.TabIndex = 1;
            this.mainSplit.Text = "splitContainerControl1";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.lstCategory);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.lstDish);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(364, 701);
            this.splitContainerControl1.SplitterPosition = 171;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // lstCategory
            // 
            this.lstCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstCategory.Location = new System.Drawing.Point(0, 0);
            this.lstCategory.Margin = new System.Windows.Forms.Padding(4);
            this.lstCategory.Name = "lstCategory";
            this.lstCategory.Size = new System.Drawing.Size(171, 701);
            this.lstCategory.TabIndex = 0;
            this.lstCategory.ToolTip = "Категории";
            this.lstCategory.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lstCategory_MouseClick);
            // 
            // lstDish
            // 
            this.lstDish.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstDish.Location = new System.Drawing.Point(0, 0);
            this.lstDish.Margin = new System.Windows.Forms.Padding(4);
            this.lstDish.Name = "lstDish";
            this.lstDish.Size = new System.Drawing.Size(188, 701);
            this.lstDish.TabIndex = 1;
            this.lstDish.ToolTip = "Блюда";
            this.lstDish.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.LstDishMouseDoubleClick);
            // 
            // rightSplit
            // 
            this.rightSplit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rightSplit.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.rightSplit.Horizontal = false;
            this.rightSplit.IsSplitterFixed = true;
            this.rightSplit.Location = new System.Drawing.Point(0, 0);
            this.rightSplit.Margin = new System.Windows.Forms.Padding(4);
            this.rightSplit.Name = "rightSplit";
            this.rightSplit.Panel1.Controls.Add(this.tabControl1);
            this.rightSplit.Panel1.Text = "Panel1";
            this.rightSplit.Panel2.Controls.Add(this.panelCheckOut);
            this.rightSplit.Panel2.MinSize = 50;
            this.rightSplit.Panel2.Text = "Panel2";
            this.rightSplit.Size = new System.Drawing.Size(973, 701);
            this.rightSplit.SplitterPosition = 60;
            this.rightSplit.TabIndex = 0;
            this.rightSplit.Text = "splitContainerControl1";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(973, 636);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.cafePlaceControl1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(965, 607);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Место 1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panelCheckOut
            // 
            this.panelCheckOut.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.panelCheckOut.Controls.Add(this.pnlBottom);
            this.panelCheckOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCheckOut.Location = new System.Drawing.Point(0, 0);
            this.panelCheckOut.Margin = new System.Windows.Forms.Padding(4);
            this.panelCheckOut.Name = "panelCheckOut";
            this.panelCheckOut.Size = new System.Drawing.Size(973, 60);
            this.panelCheckOut.TabIndex = 0;
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.btnPrint);
            this.pnlBottom.Controls.Add(this.btnSaveOrder);
            this.pnlBottom.Controls.Add(this.btnSave);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBottom.Location = new System.Drawing.Point(2, 2);
            this.pnlBottom.Margin = new System.Windows.Forms.Padding(4);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(969, 56);
            this.pnlBottom.TabIndex = 1012;
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPrint.Location = new System.Drawing.Point(223, 5);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(125, 31);
            this.btnPrint.TabIndex = 5;
            this.btnPrint.Text = "Чек клиента";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.BtnPrintClick);
            // 
            // btnSaveOrder
            // 
            this.btnSaveOrder.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSaveOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSaveOrder.Location = new System.Drawing.Point(361, 5);
            this.btnSaveOrder.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnSaveOrder.Name = "btnSaveOrder";
            this.btnSaveOrder.Size = new System.Drawing.Size(116, 31);
            this.btnSaveOrder.TabIndex = 4;
            this.btnSaveOrder.Text = "Сохранить";
            this.btnSaveOrder.UseVisualStyleBackColor = true;
            this.btnSaveOrder.Click += new System.EventHandler(this.BtnSaveOrderClick);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.Location = new System.Drawing.Point(491, 5);
            this.btnSave.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(116, 31);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Заказать";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSaveClick);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(618, 5);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 27, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(116, 31);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // updateStateTimer
            // 
            this.updateStateTimer.Enabled = true;
            this.updateStateTimer.Interval = 2000;
            this.updateStateTimer.Tick += new System.EventHandler(this.updateStateTimer_Tick);
            // 
            // CafeOrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1342, 701);
            this.Controls.Add(this.mainSplit);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CafeOrderForm";
            this.Text = "Заказ Кафе";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CafeOrderForm_FormClosing);
            this.Load += new System.EventHandler(this.CafeOrderFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplit)).EndInit();
            this.mainSplit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lstCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lstDish)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightSplit)).EndInit();
            this.rightSplit.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelCheckOut)).EndInit();
            this.panelCheckOut.ResumeLayout(false);
            this.pnlBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.SplitContainerControl mainSplit;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.ListBoxControl lstCategory;
        private DevExpress.XtraEditors.ListBoxControl lstDish;
        private DevExpress.XtraEditors.SplitContainerControl rightSplit;
        private DevExpress.XtraEditors.PanelControl panelCheckOut;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSaveOrder;
        private System.Windows.Forms.Button btnPrint;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private CafePlaceControl cafePlaceControl1;
		private System.Windows.Forms.Timer updateStateTimer;
    }
}