﻿namespace LaRenzo.Forms.Documents.CafeOrderForms
{
    partial class CafeOrderListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.columnUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnTable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnIsProcessed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDocumentSum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnSession = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnItemsAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDishesAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDrinksAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.сolumnPersonsCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.btnRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.btnEdit = new System.Windows.Forms.ToolStripButton();
            this.btnCopy = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAdd = new System.Windows.Forms.ToolStripButton();
            this.mainToolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.searchButton = new System.Windows.Forms.ToolStripButton();
            this.panelFilter = new System.Windows.Forms.Panel();
            this.buttonCreate = new System.Windows.Forms.Button();
            this.dateTo = new System.Windows.Forms.DateTimePicker();
            this.dateFrom = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            this.mainToolStrip.SuspendLayout();
            this.panelFilter.SuspendLayout();
            this.SuspendLayout();
            // 
            // columnUser
            // 
            this.columnUser.Caption = "Пользователь";
            this.columnUser.FieldName = "User";
            this.columnUser.Name = "columnUser";
            this.columnUser.OptionsColumn.AllowEdit = false;
            this.columnUser.OptionsColumn.ReadOnly = true;
            this.columnUser.Visible = true;
            this.columnUser.VisibleIndex = 9;
            // 
            // columnTable
            // 
            this.columnTable.Caption = "Столик №";
            this.columnTable.FieldName = "Table.Name";
            this.columnTable.Name = "columnTable";
            this.columnTable.OptionsColumn.AllowEdit = false;
            this.columnTable.OptionsColumn.ReadOnly = true;
            this.columnTable.Visible = true;
            this.columnTable.VisibleIndex = 3;
            this.columnTable.Width = 84;
            // 
            // columnDate
            // 
            this.columnDate.Caption = "Дата";
            this.columnDate.FieldName = "Created";
            this.columnDate.Name = "columnDate";
            this.columnDate.OptionsColumn.AllowEdit = false;
            this.columnDate.OptionsColumn.ReadOnly = true;
            this.columnDate.Visible = true;
            this.columnDate.VisibleIndex = 2;
            this.columnDate.Width = 123;
            // 
            // columnNumber
            // 
            this.columnNumber.Caption = "Номер";
            this.columnNumber.FieldName = "Code";
            this.columnNumber.Name = "columnNumber";
            this.columnNumber.OptionsColumn.AllowEdit = false;
            this.columnNumber.OptionsColumn.ReadOnly = true;
            this.columnNumber.Width = 123;
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnNumber,
            this.columnDate,
            this.columnTable,
            this.columnIsProcessed,
            this.columnUser,
            this.columnDocumentSum,
            this.columnId,
            this.columnSession,
            this.columnItemsAmount,
            this.columnDishesAmount,
            this.columnDrinksAmount,
            this.сolumnPersonsCount});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DocumentSum", this.columnDocumentSum, "{0:F2} ₽"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "columnItemsAmount", this.columnItemsAmount, "{0:#.##}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "columnDishesAmount", this.columnDishesAmount, "{0:#.##}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "columnDrinksAmount", this.columnDrinksAmount, "{0:#.##}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PersonsCount", this.сolumnPersonsCount, "{0:#.##}")});
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.mainGridView.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.mainGridView.OptionsView.ShowFooter = true;
            this.mainGridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.mainGridView_RowStyle);
            this.mainGridView.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.mainGridView_CustomUnboundColumnData);
            this.mainGridView.DoubleClick += new System.EventHandler(this.mainGridView_DoubleClick);
            // 
            // columnIsProcessed
            // 
            this.columnIsProcessed.Caption = "Проведен";
            this.columnIsProcessed.FieldName = "IsProcessed";
            this.columnIsProcessed.Name = "columnIsProcessed";
            this.columnIsProcessed.OptionsColumn.AllowEdit = false;
            this.columnIsProcessed.OptionsColumn.ReadOnly = true;
            this.columnIsProcessed.Visible = true;
            this.columnIsProcessed.VisibleIndex = 0;
            this.columnIsProcessed.Width = 50;
            // 
            // columnDocumentSum
            // 
            this.columnDocumentSum.Caption = "Сумма чека";
            this.columnDocumentSum.DisplayFormat.FormatString = "F2";
            this.columnDocumentSum.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnDocumentSum.FieldName = "DocumentSum";
            this.columnDocumentSum.Name = "columnDocumentSum";
            this.columnDocumentSum.OptionsColumn.AllowEdit = false;
            this.columnDocumentSum.OptionsColumn.ReadOnly = true;
            this.columnDocumentSum.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DocumentSum", "{0:F2} ₽")});
            this.columnDocumentSum.Visible = true;
            this.columnDocumentSum.VisibleIndex = 4;
            // 
            // columnId
            // 
            this.columnId.Caption = "ИД";
            this.columnId.FieldName = "ID";
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.AllowEdit = false;
            this.columnId.OptionsColumn.ReadOnly = true;
            // 
            // columnSession
            // 
            this.columnSession.Caption = "Сессия";
            this.columnSession.FieldName = "SessionID";
            this.columnSession.Name = "columnSession";
            this.columnSession.OptionsColumn.AllowEdit = false;
            this.columnSession.OptionsColumn.ReadOnly = true;
            this.columnSession.Visible = true;
            this.columnSession.VisibleIndex = 1;
            // 
            // columnItemsAmount
            // 
            this.columnItemsAmount.Caption = "Кол-во позиций в чеке";
            this.columnItemsAmount.FieldName = "columnItemsAmount";
            this.columnItemsAmount.Name = "columnItemsAmount";
            this.columnItemsAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "columnItemsAmount", "{0:#.##}")});
            this.columnItemsAmount.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.columnItemsAmount.Visible = true;
            this.columnItemsAmount.VisibleIndex = 5;
            // 
            // columnDishesAmount
            // 
            this.columnDishesAmount.Caption = "Кол-во блюд в чеке";
            this.columnDishesAmount.FieldName = "columnDishesAmount";
            this.columnDishesAmount.Name = "columnDishesAmount";
            this.columnDishesAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "columnDishesAmount", "{0:#.##}")});
            this.columnDishesAmount.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.columnDishesAmount.Visible = true;
            this.columnDishesAmount.VisibleIndex = 6;
            // 
            // columnDrinksAmount
            // 
            this.columnDrinksAmount.Caption = "Кол-во напитков в чеке";
            this.columnDrinksAmount.FieldName = "columnDrinksAmount";
            this.columnDrinksAmount.Name = "columnDrinksAmount";
            this.columnDrinksAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "columnDrinksAmount", "{0:#.##}")});
            this.columnDrinksAmount.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.columnDrinksAmount.Visible = true;
            this.columnDrinksAmount.VisibleIndex = 7;
            // 
            // сolumnPersonsCount
            // 
            this.сolumnPersonsCount.Caption = "Кол-во персон";
            this.сolumnPersonsCount.FieldName = "PersonsCount";
            this.сolumnPersonsCount.Name = "сolumnPersonsCount";
            this.сolumnPersonsCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PersonsCount", "{0:#.##}")});
            this.сolumnPersonsCount.Visible = true;
            this.сolumnPersonsCount.VisibleIndex = 8;
            // 
            // mainGridControl
            // 
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.mainGridControl.Location = new System.Drawing.Point(0, 31);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Margin = new System.Windows.Forms.Padding(4);
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(915, 545);
            this.mainGridControl.TabIndex = 7;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // btnRefresh
            // 
            this.btnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRefresh.Image = global::LaRenzo.Properties.Resources.refresh;
            this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(28, 28);
            this.btnRefresh.Text = "Обновить";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // btnDelete
            // 
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = global::LaRenzo.Properties.Resources.close_wh;
            this.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(28, 28);
            this.btnDelete.Text = "Пометить на удаление";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEdit.Image = global::LaRenzo.Properties.Resources.edit_wh;
            this.btnEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(28, 28);
            this.btnEdit.Text = "Редактировать поступление";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnCopy
            // 
            this.btnCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCopy.Image = global::LaRenzo.Properties.Resources.copy;
            this.btnCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(28, 28);
            this.btnCopy.Text = "Скопировать посупление";
            this.btnCopy.Visible = false;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            this.toolStripSeparator1.Visible = false;
            // 
            // btnAdd
            // 
            this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdd.Image = global::LaRenzo.Properties.Resources.add_wh;
            this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(28, 28);
            this.btnAdd.Text = "Создать поступление";
            this.btnAdd.Visible = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.toolStripSeparator1,
            this.btnCopy,
            this.btnEdit,
            this.btnDelete,
            this.toolStripSeparator2,
            this.btnRefresh,
            this.toolStripSeparator3,
            this.searchButton});
            this.mainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Size = new System.Drawing.Size(915, 31);
            this.mainToolStrip.TabIndex = 6;
            this.mainToolStrip.Text = "toolStrip1";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 31);
            // 
            // searchButton
            // 
            this.searchButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.searchButton.Image = global::LaRenzo.Properties.Resources.search;
            this.searchButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(28, 28);
            this.searchButton.Text = "Найти";
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // panelFilter
            // 
            this.panelFilter.Controls.Add(this.buttonCreate);
            this.panelFilter.Controls.Add(this.dateTo);
            this.panelFilter.Controls.Add(this.dateFrom);
            this.panelFilter.Location = new System.Drawing.Point(354, 0);
            this.panelFilter.Name = "panelFilter";
            this.panelFilter.Size = new System.Drawing.Size(561, 31);
            this.panelFilter.TabIndex = 8;
            // 
            // buttonCreate
            // 
            this.buttonCreate.Location = new System.Drawing.Point(436, 3);
            this.buttonCreate.Name = "buttonCreate";
            this.buttonCreate.Size = new System.Drawing.Size(113, 23);
            this.buttonCreate.TabIndex = 2;
            this.buttonCreate.Text = "Создать";
            this.buttonCreate.UseVisualStyleBackColor = true;
            this.buttonCreate.Click += new System.EventHandler(this.buttonCreate_Click);
            // 
            // dateTo
            // 
            this.dateTo.Location = new System.Drawing.Point(218, 4);
            this.dateTo.Name = "dateTo";
            this.dateTo.Size = new System.Drawing.Size(200, 22);
            this.dateTo.TabIndex = 1;
            // 
            // dateFrom
            // 
            this.dateFrom.Location = new System.Drawing.Point(3, 4);
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.Size = new System.Drawing.Size(200, 22);
            this.dateFrom.TabIndex = 0;
            // 
            // CafeOrderListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 576);
            this.Controls.Add(this.panelFilter);
            this.Controls.Add(this.mainGridControl);
            this.Controls.Add(this.mainToolStrip);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CafeOrderListForm";
            this.Text = "Заказы кафе";
            this.Load += new System.EventHandler(this.CafeOrderListForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.panelFilter.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn columnUser;
        private DevExpress.XtraGrid.Columns.GridColumn columnTable;
        private DevExpress.XtraGrid.Columns.GridColumn columnDate;
        private DevExpress.XtraGrid.Columns.GridColumn columnNumber;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnIsProcessed;
        private DevExpress.XtraGrid.GridControl mainGridControl;
        private System.Windows.Forms.ToolStripButton btnRefresh;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnDelete;
        private System.Windows.Forms.ToolStripButton btnEdit;
        private System.Windows.Forms.ToolStripButton btnCopy;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnAdd;
        private System.Windows.Forms.ToolStrip mainToolStrip;
        private DevExpress.XtraGrid.Columns.GridColumn columnDocumentSum;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraGrid.Columns.GridColumn columnSession;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton searchButton;
        private DevExpress.XtraGrid.Columns.GridColumn columnItemsAmount;
        private DevExpress.XtraGrid.Columns.GridColumn columnDishesAmount;
        private DevExpress.XtraGrid.Columns.GridColumn columnDrinksAmount;
        private DevExpress.XtraGrid.Columns.GridColumn сolumnPersonsCount;
        private System.Windows.Forms.Panel panelFilter;
        private System.Windows.Forms.Button buttonCreate;
        private System.Windows.Forms.DateTimePicker dateTo;
        private System.Windows.Forms.DateTimePicker dateFrom;
    }
}