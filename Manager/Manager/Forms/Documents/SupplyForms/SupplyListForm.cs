﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting.Native;
using LaRenzo.DataRepository.Entities.Documents.Supply;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Documents.Supply;
using LaRenzo.Forms.MainFormAndIM;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Documents.SupplyForms
{
    public partial class SupplyListForm : Form
    {
        public SupplyListForm()
        {
            InitializeComponent();
            LoadSupplyList(new object(), new EventArgs());
            CheckAccessRights();

        }

        private void CheckAccessRights()
        {

            var toolBoxItems = GetToolBarItems().ToList();
            /*foreach (var control in toolBoxItems)
            {
                var tag = control.Tag;
                var right = rights.FirstOrDefault(x => x.Code == Convert.ToInt32(tag));
                control.Visible = (right == null) || right.Allowed;
            }*/

            var rights = Content.ThreadLocalStorage.UserRights;

            foreach (var right in rights)
            {
                var controls = toolBoxItems.Where(x => Convert.ToInt32(x.Tag) == right.Code);
                if (controls.Any()) controls.ForEach(x => x.Visible = right.Allowed);

            }
        }

        private IEnumerable<ToolStripItem> GetToolBarItems()
        {
            return Controls.OfType<ToolStrip>().SelectMany(x => x.Items.OfType<ToolStripItem>());
        }


        private void LoadSupplyList(object sender, EventArgs args)
        {
            int countOfRows = 100;
            try
            {
                countOfRows = Convert.ToInt32(textBoxCountOfRows.Text);
            }
            catch
            {
            }
            mainGridControl.DataSource = Content.SupplyManager.GetSupplyDocuments(countOfRows, checkBoxNotPaid.Checked);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var supplyForm = new SupplyForm();
            supplyForm.FormClosed += UpdateDataGrid;
            supplyForm.Show();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            UpdateDataGrid(sender, e);
        }

        private void UpdateDataGrid(object sender, EventArgs e)
        {
            var first = mainGridView.TopRowIndex;
            var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);
            LoadSupplyList(sender, e);
            mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
            mainGridView.MakeRowVisible(first);
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            var selectedRow = (SupplyDocument) mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetSupplyForm(false, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.SupplyDocumentIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void GetSupplyForm(bool isEdit, int supplyId)
        {
            var supplyDocument = Content.SupplyManager.GetSupplyDocument(supplyId);

            var supplyForm = new SupplyForm(isEdit, supplyDocument);

            supplyForm.FormClosed += UpdateDataGrid;
            supplyForm.Show();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var selectedRow = (SupplyDocument)mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetSupplyForm(true, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.SupplyDocumentIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var selectedRow = (SupplyDocument) mainGridView.GetFocusedRow();

            if (selectedRow == null)
            {
                MessageBox.Show(Resources.SupplyDocumentIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return;
            }

            string msg = selectedRow.IsMarkToDelete
                             ? Resources.UnMarkDeleteDocument
                             : Resources.MarkDeleteDocument;

            if (MessageBox.Show(msg, Resources.WarningTitle, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Content.SupplyManager.DeleteSupplyDocument(selectedRow);
                UpdateDataGrid(sender, e);
            }
        }

        private void mainGridView_DoubleClick(object sender, EventArgs e)
        {
            if (!Content.UserAccessManager.GroupContainsRight(Content.ThreadLocalStorage.UserRights, AccessRightAlias.EditDocuments))
            {
                MessageBox.Show(Resources.YouCantEditDocuments);
                return;
            }
            
            var view = (GridView)sender;

            var pt = view.GridControl.PointToClient(MousePosition);

            var info = view.CalcHitInfo(pt);
            if (view.IsDataRow(info.RowHandle))
            {
                var supplyId = (int)view.GetRowCellValue(info.RowHandle, columnNumber);
                GetSupplyForm(true, supplyId);
            }
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            var currentRow = (SupplyDocument)view.GetRow(e.RowHandle);

            if (currentRow.IsMarkToDelete)
                e.Appearance.BackColor = Color.Salmon;

            if (currentRow.IsProcessed)
                e.Appearance.BackColor = Color.LightGreen;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }

        private void SupplyListForm_Load(object sender, EventArgs e)
        {
            if (Content.UserManager.IsWarehouseManager())
            {

                btnCopy.Enabled = btnEdit.Enabled = btnDelete.Enabled = false;


            } 
        }

        private void textBoxCountOfRows_TextChanged(object sender, EventArgs e)
        {
            LoadSupplyList(new object(), new EventArgs());
        }

        private void checkBoxNotPaid_CheckedChanged(object sender, EventArgs e)
        {
            LoadSupplyList(new object(), new EventArgs());
        }
    }
}