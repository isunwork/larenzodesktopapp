﻿namespace LaRenzo.Forms.Documents.SupplyForms
{
    partial class SupplyListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPartner = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnWarehous = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnSum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnIsProcessed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPayment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnUserMod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DateMod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.mainToolStrip = new System.Windows.Forms.ToolStrip();
            this.btnAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnCopy = new System.Windows.Forms.ToolStripButton();
            this.btnEdit = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrinters = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnPrintSupplyForm = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.searchButton = new System.Windows.Forms.ToolStripButton();
            this.textBoxCountOfRows = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxNotPaid = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            this.mainToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainGridControl
            // 
            this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mainGridControl.Location = new System.Drawing.Point(0, 31);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(915, 545);
            this.mainGridControl.TabIndex = 1;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnNumber,
            this.columnDate,
            this.columnPartner,
            this.columnWarehous,
            this.columnSum,
            this.columnIsProcessed,
            this.columnUser,
            this.columnPayment,
            this.gridColumn1,
            this.columnUserMod,
            this.DateMod});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.mainGridView.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.mainGridView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways;
            this.mainGridView.OptionsView.ShowFooter = true;
            this.mainGridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.mainGridView_RowStyle);
            this.mainGridView.DoubleClick += new System.EventHandler(this.mainGridView_DoubleClick);
            // 
            // columnNumber
            // 
            this.columnNumber.Caption = "Номер";
            this.columnNumber.FieldName = "ID";
            this.columnNumber.Name = "columnNumber";
            this.columnNumber.OptionsColumn.AllowEdit = false;
            this.columnNumber.OptionsColumn.ReadOnly = true;
            this.columnNumber.Visible = true;
            this.columnNumber.VisibleIndex = 1;
            this.columnNumber.Width = 78;
            // 
            // columnDate
            // 
            this.columnDate.Caption = "Дата";
            this.columnDate.FieldName = "DocumentDate";
            this.columnDate.Name = "columnDate";
            this.columnDate.OptionsColumn.AllowEdit = false;
            this.columnDate.OptionsColumn.ReadOnly = true;
            this.columnDate.Visible = true;
            this.columnDate.VisibleIndex = 2;
            this.columnDate.Width = 49;
            // 
            // columnPartner
            // 
            this.columnPartner.Caption = "Контрагент";
            this.columnPartner.FieldName = "Partner.PartnerShortName";
            this.columnPartner.Name = "columnPartner";
            this.columnPartner.OptionsColumn.AllowEdit = false;
            this.columnPartner.OptionsColumn.ReadOnly = true;
            this.columnPartner.Visible = true;
            this.columnPartner.VisibleIndex = 3;
            this.columnPartner.Width = 78;
            // 
            // columnWarehous
            // 
            this.columnWarehous.Caption = "Склад";
            this.columnWarehous.FieldName = "Warehouse.Name";
            this.columnWarehous.Name = "columnWarehous";
            this.columnWarehous.OptionsColumn.AllowEdit = false;
            this.columnWarehous.OptionsColumn.ReadOnly = true;
            this.columnWarehous.Visible = true;
            this.columnWarehous.VisibleIndex = 4;
            this.columnWarehous.Width = 64;
            // 
            // columnSum
            // 
            this.columnSum.Caption = "Сумма";
            this.columnSum.DisplayFormat.FormatString = "F2";
            this.columnSum.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnSum.FieldName = "DocumentSum";
            this.columnSum.Name = "columnSum";
            this.columnSum.OptionsColumn.AllowEdit = false;
            this.columnSum.OptionsColumn.ReadOnly = true;
            this.columnSum.Visible = true;
            this.columnSum.VisibleIndex = 5;
            this.columnSum.Width = 67;
            // 
            // columnIsProcessed
            // 
            this.columnIsProcessed.Caption = "Проведен";
            this.columnIsProcessed.FieldName = "IsProcessed";
            this.columnIsProcessed.Name = "columnIsProcessed";
            this.columnIsProcessed.OptionsColumn.AllowEdit = false;
            this.columnIsProcessed.OptionsColumn.ReadOnly = true;
            this.columnIsProcessed.Visible = true;
            this.columnIsProcessed.VisibleIndex = 0;
            this.columnIsProcessed.Width = 32;
            // 
            // columnUser
            // 
            this.columnUser.Caption = "Пользователь";
            this.columnUser.FieldName = "User";
            this.columnUser.Name = "columnUser";
            this.columnUser.OptionsColumn.AllowEdit = false;
            this.columnUser.OptionsColumn.AllowFocus = false;
            this.columnUser.OptionsColumn.ReadOnly = true;
            this.columnUser.Visible = true;
            this.columnUser.VisibleIndex = 8;
            this.columnUser.Width = 68;
            // 
            // columnPayment
            // 
            this.columnPayment.Caption = "Оплачено";
            this.columnPayment.DisplayFormat.FormatString = "F2";
            this.columnPayment.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnPayment.FieldName = "PaySum";
            this.columnPayment.Name = "columnPayment";
            this.columnPayment.OptionsColumn.AllowEdit = false;
            this.columnPayment.OptionsColumn.AllowFocus = false;
            this.columnPayment.OptionsColumn.ReadOnly = true;
            this.columnPayment.Visible = true;
            this.columnPayment.VisibleIndex = 6;
            this.columnPayment.Width = 58;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Долг";
            this.gridColumn1.DisplayFormat.FormatString = "F2";
            this.gridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn1.FieldName = "gridColumn1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "gridColumn1", "Numeric F2")});
            this.gridColumn1.UnboundExpression = "[DocumentSum] - [PaySum]";
            this.gridColumn1.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 7;
            this.gridColumn1.Width = 58;
            // 
            // columnUserMod
            // 
            this.columnUserMod.Caption = "Последний пользователь";
            this.columnUserMod.FieldName = "AuthorModified";
            this.columnUserMod.Name = "columnUserMod";
            this.columnUserMod.Visible = true;
            this.columnUserMod.VisibleIndex = 9;
            this.columnUserMod.Width = 58;
            // 
            // DateMod
            // 
            this.DateMod.Caption = "Дата изменения";
            this.DateMod.DisplayFormat.FormatString = "g";
            this.DateMod.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DateMod.FieldName = "DateModified";
            this.DateMod.Name = "DateMod";
            this.DateMod.Visible = true;
            this.DateMod.VisibleIndex = 10;
            this.DateMod.Width = 58;
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.toolStripSeparator1,
            this.btnCopy,
            this.btnEdit,
            this.btnDelete,
            this.toolStripSeparator2,
            this.btnPrinters,
            this.btnRefresh,
            this.toolStripSeparator3,
            this.searchButton});
            this.mainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Size = new System.Drawing.Size(915, 31);
            this.mainToolStrip.TabIndex = 2;
            this.mainToolStrip.Text = "toolStrip1";
            // 
            // btnAdd
            // 
            this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdd.Image = global::LaRenzo.Properties.Resources.add_wh;
            this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(28, 28);
            this.btnAdd.Text = "Создать поступление";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            this.toolStripSeparator1.Tag = "44";
            // 
            // btnCopy
            // 
            this.btnCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCopy.Image = global::LaRenzo.Properties.Resources.copy;
            this.btnCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(28, 28);
            this.btnCopy.Tag = "44";
            this.btnCopy.Text = "Скопировать посупление";
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEdit.Image = global::LaRenzo.Properties.Resources.edit_wh;
            this.btnEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(28, 28);
            this.btnEdit.Tag = "44";
            this.btnEdit.Text = "Редактировать поступление";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = global::LaRenzo.Properties.Resources.close_wh;
            this.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(28, 28);
            this.btnDelete.Tag = "44";
            this.btnDelete.Text = "Пометить на удаление";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
            this.toolStripSeparator2.Tag = "44";
            // 
            // btnPrinters
            // 
            this.btnPrinters.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrinters.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnPrintSupplyForm});
            this.btnPrinters.Image = global::LaRenzo.Properties.Resources.print_printer;
            this.btnPrinters.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrinters.Name = "btnPrinters";
            this.btnPrinters.Size = new System.Drawing.Size(38, 28);
            this.btnPrinters.Tag = "44";
            this.btnPrinters.Text = "toolStripDropDownButton1";
            // 
            // btnPrintSupplyForm
            // 
            this.btnPrintSupplyForm.Name = "btnPrintSupplyForm";
            this.btnPrintSupplyForm.Size = new System.Drawing.Size(236, 26);
            this.btnPrintSupplyForm.Text = "Поступление товаров";
            // 
            // btnRefresh
            // 
            this.btnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRefresh.Image = global::LaRenzo.Properties.Resources.refresh;
            this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(28, 28);
            this.btnRefresh.Text = "Обновить";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 31);
            // 
            // searchButton
            // 
            this.searchButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.searchButton.Image = global::LaRenzo.Properties.Resources.search;
            this.searchButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(28, 28);
            this.searchButton.Text = "Найти";
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // textBoxCountOfRows
            // 
            this.textBoxCountOfRows.Location = new System.Drawing.Point(450, 4);
            this.textBoxCountOfRows.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxCountOfRows.Name = "textBoxCountOfRows";
            this.textBoxCountOfRows.Size = new System.Drawing.Size(80, 22);
            this.textBoxCountOfRows.TabIndex = 3;
            this.textBoxCountOfRows.Text = "100";
            this.textBoxCountOfRows.TextChanged += new System.EventHandler(this.textBoxCountOfRows_TextChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(300, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Количество записей:";
            // 
            // checkBoxNotPaid
            // 
            this.checkBoxNotPaid.AutoSize = true;
            this.checkBoxNotPaid.Location = new System.Drawing.Point(560, 6);
            this.checkBoxNotPaid.Name = "checkBoxNotPaid";
            this.checkBoxNotPaid.Size = new System.Drawing.Size(134, 21);
            this.checkBoxNotPaid.TabIndex = 5;
            this.checkBoxNotPaid.Text = "Не оплаченные";
            this.checkBoxNotPaid.UseVisualStyleBackColor = true;
            this.checkBoxNotPaid.CheckedChanged += new System.EventHandler(this.checkBoxNotPaid_CheckedChanged);
            // 
            // SupplyListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 576);
            this.Controls.Add(this.checkBoxNotPaid);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxCountOfRows);
            this.Controls.Add(this.mainGridControl);
            this.Controls.Add(this.mainToolStrip);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "SupplyListForm";
            this.Text = "Поступления товаров";
            this.Load += new System.EventHandler(this.SupplyListForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnNumber;
        private DevExpress.XtraGrid.Columns.GridColumn columnDate;
        private DevExpress.XtraGrid.Columns.GridColumn columnPartner;
        private DevExpress.XtraGrid.Columns.GridColumn columnWarehous;
        private DevExpress.XtraGrid.Columns.GridColumn columnSum;
        private System.Windows.Forms.ToolStrip mainToolStrip;
        private System.Windows.Forms.ToolStripButton btnAdd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnCopy;
        private System.Windows.Forms.ToolStripButton btnEdit;
        private System.Windows.Forms.ToolStripButton btnDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripDropDownButton btnPrinters;
        private System.Windows.Forms.ToolStripMenuItem btnPrintSupplyForm;
        private DevExpress.XtraGrid.Columns.GridColumn columnIsProcessed;
        private System.Windows.Forms.ToolStripButton btnRefresh;
        private DevExpress.XtraGrid.Columns.GridColumn columnUser;
        private DevExpress.XtraGrid.Columns.GridColumn columnPayment;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton searchButton;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private System.Windows.Forms.TextBox textBoxCountOfRows;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraGrid.Columns.GridColumn columnUserMod;
        private DevExpress.XtraGrid.Columns.GridColumn DateMod;
        private System.Windows.Forms.CheckBox checkBoxNotPaid;
    }
}