﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using LaRenzo.DataRepository.Entities.Catalogs.Partners;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Entities.Documents.Supply;
using LaRenzo.DataRepository.Entities.Wirings;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Partners;
using LaRenzo.DataRepository.Repositories.Catalogs.Products;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Catalogs.Warehouses;
using LaRenzo.DataRepository.Repositories.Documents.Supply;
using LaRenzo.DataRepository.Repositories.Wiringses.AveragesPrice;
using LaRenzo.DataRepository.Repositories.Wiringses.RemainsOfGood;
using LaRenzo.DataRepository.Repositories.Wiringses.SettlementsWithPartner;
using LaRenzo.Forms.Catalog.PartnersForms;
using LaRenzo.Forms.Catalog.WarehouseForms;
using LaRenzo.Forms.MainFormAndIM;
using LaRenzo.FormsProvider;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Documents.SupplyForms
{
    public partial class SupplyForm : Form
	{
		private bool _isEditWindow;

		private string _userName = string.Empty;

		private bool DocumentIsProcessed { set; get; }

		private readonly List<SupplyDocumentItem> _documentItems;

		public SupplyForm()
		{
			InitializeComponent();
			_documentItems = new List<SupplyDocumentItem>();
			lstProduct.DisplayMember = "Name";

			var partners = Content.PartnerManager.GetPartners();
			cmbPartner.DataSource = partners;
			cmbPartner.DisplayMember = "PartnerShortName";
			cmbPartner.ValueMember = "ID";

			var warehouses = Content.WarehouseManager.GetWarehouses();
			cmbWarehouse.DataSource = warehouses;
			cmbWarehouse.DisplayMember = "Name";
			cmbWarehouse.ValueMember = "ID";

			var defaultWarehouse = Content.WarehouseManager.GetWarehouses().FirstOrDefault(x => x.IsBase);
			if (defaultWarehouse != null)
			{
				cmbWarehouse.SelectedValue = defaultWarehouse.ID;
			}
		}

		public SupplyForm(bool isEdit, SupplyDocument document): this()
		{
			_isEditWindow = isEdit;

			_userName = document.User;

			WindowsFormProvider.FillControlsFromModel(this, document);

			DocumentIsProcessed = document.IsProcessed;

			numPaySum.Value = document.PaySum;
			dtnInputDate.Value = document.InputDate;
			dtnDocDate.Value = document.DocumentDate;
			dtnPayDate.Value = document.PayDate;

			if (!string.IsNullOrEmpty(txtNumber.Text))
			{
				_documentItems = Content.SupplyManager.GetSupplyDocumentItems(document.ID);
			}
			mainGridControl.DataSource = _documentItems;

			if (document.DocumentDate <= DateTime.Now.AddHours(-2))
			{
				treeListCategories.Enabled = false;
				lstProduct.Enabled = false;
				mainGridControl.Enabled = false;
				//richOtherInfo.Enabled = false;
				txtNumber.Enabled = false;
				dtnDocDate.Enabled = false;
				txtInputNumber.Enabled = false;
				dtnInputDate.Enabled = false;
				btnProcess.Enabled = false;
			}
		}

		private void FillCategoties()
		{
			treeListCategories.DataSource = Content.ProductManager.GetCategoriesList();

			var nodeAll = treeListCategories.AppendNode(null, null);
			nodeAll.SetValue(NameCol, "Все");
			nodeAll.SetValue(IdCol, 0);
			treeListCategories.SetNodeIndex(nodeAll, 0);
			treeListCategories.FocusedNode = nodeAll;
		}

		private void SupplyForm_Load(object sender, EventArgs e)
		{
			FillCategoties();

			if (DocumentIsProcessed)
			{
				btnProcess.Text = Resources.Unprocess;
				btnProcess.ForeColor = Color.DarkRed;
			}
			else
			{
				btnProcess.Text = Resources.Process;
				btnProcess.ForeColor = Color.Green;
			}
		}

		private void treeListCategories_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
		{
			if (e.Node != null)
			{
				var id = (int)e.Node.GetValue(IdCol);
				FillProducts(id);
			}
		}

		private void FillProducts(int catId)
		{
			var products = Content.ProductManager.GetAllProductsInCategory(catId).OrderBy(x => x.Name).ToList();
			if (products.Count != 0)
			{
				lstProduct.DataSource = products;
			}
		}

		private void btnSelectParent_Click(object sender, EventArgs e)
		{
			var selectPartnerForm = new SelectPartnerForm();

			if (selectPartnerForm.ShowDialog() == DialogResult.OK)
			{
				cmbPartner.SelectedValue = selectPartnerForm.Partner.ID;
			}
		}

		private void btnSelectWarehouse_Click(object sender, EventArgs e)
		{
			var selectWarehouseForm = new SelectWarehousezForm();

			if (selectWarehouseForm.ShowDialog() == DialogResult.OK)
			{
				cmbWarehouse.SelectedValue = selectWarehouseForm.Warehouse.ID;
			}
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void lstProduct_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			mainGridView.BeginDataUpdate();
			var product = (Product)lstProduct.SelectedItem;

			if (product == null)
				return;

			_documentItems.Add(new SupplyDocumentItem
			{
				ID = -1,
				Product = product,
				ProductID = product.ID,
				Amount = 0,
				Price = 0,
				Sum = 0
			});

			mainGridControl.DataSource = _documentItems;

			mainGridView.EndDataUpdate();
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			if (cmbPartner.SelectedItem == null)
			{
				MessageBox.Show(Resources.PartnerIsNull, Resources.WarningTitle, MessageBoxButtons.OK,
								MessageBoxIcon.Warning);
				return;
			}

			var documentSum = _documentItems.Sum(x => x.Sum);

			var supplyDoc = new SupplyDocument
			{
				ID = string.IsNullOrEmpty(txtNumber.Text) ? 0 : Convert.ToInt32(txtNumber.Text.Trim()),
				DocumentDate = dtnDocDate.Value,
				InputDate = dtnInputDate.Value,
				InputNumber = txtInputNumber.Text,
				PartnerID = ((Partner)cmbPartner.SelectedItem).ID,
				WarehouseID = ((Warehouse)cmbWarehouse.SelectedItem).ID,
				PayDate = dtnPayDate.Value,
				IsProcessed = DocumentIsProcessed,
				PaySum = numPaySum.Value,
				DocumentSum = documentSum,
				Note = richOtherInfo.Text
			};

			if (_isEditWindow)
			{
				supplyDoc.User = _userName;

                Content.SupplyManager.UpdateSupplyDocument(new SupplyData
                {
                    Document = supplyDoc,
                    DocumentItems = _documentItems
                });
			}
			else
			{
				supplyDoc.User = _userName = Content.UserManager.GetUserNameOrEmptyString();
				int docId = Content.SupplyManager.AddSupplyDocument(supplyDoc);
                Content.SupplyManager.UpdateSupplyDocument(new SupplyData {
                    Document = supplyDoc,
                    DocumentItems = _documentItems
                });
				txtNumber.Text = docId.ToString(CultureInfo.InvariantCulture);
				_isEditWindow = true;
			}
		}

		private void mainGridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
		{
			if (e.Column.Name == "columnAmmount" || e.Column.Name == "columnSum")
			{
				var view = (GridView)sender;
				var currentRow = (SupplyDocumentItem)view.GetRow(e.RowHandle);

				currentRow.Amount = Math.Round(currentRow.Amount, 3);
				currentRow.Sum = Math.Round(currentRow.Sum, 3);

				currentRow.Price = currentRow.Amount != 0
									   ? Math.Round(currentRow.Sum / Convert.ToDouble(currentRow.Amount), 3)
									   : 0;
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			btnSave_Click(sender, e);
			if (DocumentIsProcessed)
			{
				UnProcessDocument();
			}
			else
			{
				ProcessDocument();
			}
		}

		private void UnProcessDocument()
		{
            Content.RemainsOfGoodWiringsManager.UnProcessSupplyDocument(Convert.ToInt32(txtNumber.Text));
            Content.SettlementsWithPartnerManager.UnProcessWirings(Convert.ToInt32(txtNumber.Text));
            Content.AveragePriceWiringsManager.UnProcessWirings(Convert.ToInt32(txtNumber.Text));

			DocumentIsProcessed = false;

			btnProcess.Text = Resources.Process;
			btnProcess.ForeColor = Color.Green;
		}

		private void ProcessDocument()
		{
			if (!Content.UserAccessManager.GroupContainsRight(Content.ThreadLocalStorage.UserRights, AccessRightAlias.EditDocuments))
				if (MessageBox.Show("После проводки документа окно будет закрыто и вы не сможете редактировать документ. Продолжить?", "Вы уверены?", MessageBoxButtons.YesNo) == DialogResult.No)
				{
					return;
				}

			if (string.IsNullOrEmpty(txtNumber.Text))
			{
				MessageBox.Show(Resources.DocIsNotProcessed, Resources.WarningTitle, MessageBoxButtons.OK,
								MessageBoxIcon.Warning);
				return;
			}

			int documentId = Convert.ToInt32(txtNumber.Text);

			var wiringses = _documentItems.Select(documentItem => new RemainsOfGoodWirings
			{
				Amount = documentItem.Amount,
				Date = dtnDocDate.Value,
				WarehouseID = ((Warehouse)cmbWarehouse.SelectedItem).ID,
				ProductID = documentItem.ProductID,
				SupplyDocumentID = documentId
			}).ToList();

            Content.RemainsOfGoodWiringsManager.ProcessSupplyDocument(documentId, wiringses);

			var documentSum = _documentItems.Sum(x => x.Sum);

			var settlementWirings = new SettlsWithPartnerWirings
			{
				Date = dtnDocDate.Value,
				PartnerID = ((Partner)cmbPartner.SelectedItem).ID,
				SupplyDocumentID = documentId,
				Payment = Math.Round((double)numPaySum.Value, 3),
				Sum = documentSum,
			};

            Content.SettlementsWithPartnerManager.ProcessWirings(documentId, settlementWirings);

			var averageWiringses = _documentItems.Select(documentItem => new AveragePriceWirings
			{
				Date = dtnDocDate.Value,
				SupplyDocumentID = documentId,
				Price = documentItem.Price,
				ProductID = documentItem.ProductID,
			}).ToList();

            Content.AveragePriceWiringsManager.ProcessWirings(documentId, averageWiringses);

			DocumentIsProcessed = true;

			btnProcess.Text = Resources.Unprocess;
			btnProcess.ForeColor = Color.DarkRed;

			if (!Content.UserAccessManager.GroupContainsRight(Content.ThreadLocalStorage.UserRights, AccessRightAlias.EditDocuments)) Close();
		}

		private void mainGridControl_DoubleClick(object sender, EventArgs e)
		{
			mainGridView.BeginDataUpdate();
			var item = (SupplyDocumentItem)mainGridView.GetRow(mainGridView.FocusedRowHandle);

			if (item == null)
				return;

			_documentItems.Remove(item);

			mainGridControl.DataSource = _documentItems;

			mainGridView.EndDataUpdate();
		}

		private void numPaySum_ValueChanged(object sender, EventArgs e)
		{
			numPaySum.Value = Math.Round(numPaySum.Value, 3);
		}

		private void printableComponentLink_CreateReportHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
		{
			var brick =
				e.Graph.DrawString(
					string.Format(
						"Поставщик: {0}\nСклад: {1}\nВходящий документ №{2} от {3:dd.MM.yyyy}\nОплачено {4} руб. на дату {5:dd.MM.yyyy}",
						cmbPartner.Text, cmbWarehouse.Text, txtInputNumber.Text, dtnInputDate.Value, numPaySum.Value,
						dtnPayDate.Value), Color.Navy,
					new RectangleF(0, 0, 560, 90), BorderSide.None);

			brick.Font = new Font("Arial", 12);
			brick.HorzAlignment = HorzAlignment.Near;
			brick.StringFormat = new BrickStringFormat(StringAlignment.Near);
		}

		private void btnPrint_Click(object sender, EventArgs e)
		{
			printableComponentLink.CreateDocument();
			printableComponentLink.ShowPreview();
		}

		private void printableComponentLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
		{
			var brick =
			  e.Graph.DrawString(
				  string.Format("Поступление товаров на склад №{0} от {1:dd.MM.yyyy}.",
								txtNumber.Text, dtnDocDate.Value), Color.Navy,
				  new RectangleF(0, 0, 560, 60), BorderSide.None);

			brick.Font = new Font("Arial", 16);
			brick.HorzAlignment = HorzAlignment.Near;
			brick.StringFormat = new BrickStringFormat(StringAlignment.Near);
		}

		private void SupplyForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (Content.UserAccessManager.GroupContainsRight(Content.ThreadLocalStorage.UserRights, AccessRightAlias.EditDocuments)) return;
			if (MessageBox.Show("После закрытия окна вы не сможете редактировать документ. Продолжить?", "Вы уверены?", MessageBoxButtons.YesNo) == DialogResult.No)
			{
				e.Cancel = true;
			}
		}
	}
}