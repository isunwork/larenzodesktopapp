﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Entities.Documents.Moving;
using LaRenzo.DataRepository.Entities.Wirings;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Documents.Moving;
using LaRenzo.DataRepository.Repositories.Reports.RemainsOfGoodReport;
using LaRenzo.Forms.Catalog.WarehouseForms;
using LaRenzo.FormsProvider;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Documents.MovingForms
{
    public partial class MovingForm : Form
    {
        /// <summary> Режим редактирования </summary>
		private bool isEditWindowMode;

        /// <summary> Имя пользователя </summary>
        private string userName = string.Empty;

        private bool DocumentIsProcessed { set; get; }

        /// <summary> Элементы документа перемещения </summary>
        private readonly List<MovingDocumentItem> documentItems;

        public MovingForm()
        {
            InitializeComponent();
            documentItems = new List<MovingDocumentItem>();
            lstProduct.DisplayMember = "Name";

            var warehouses = Content.WarehouseManager.GetWarehouses();
            fromWarehouseComboBox.DataSource = warehouses;
            fromWarehouseComboBox.DisplayMember = "Name";
            fromWarehouseComboBox.ValueMember = "ID";

            toWarehouseComboBox.DataSource = Content.WarehouseManager.GetWarehouses();
            toWarehouseComboBox.DisplayMember = "Name";
            toWarehouseComboBox.ValueMember = "ID";

            var defaultWarehouse =
                Content.WarehouseManager.GetWarehouses().FirstOrDefault(x => x.IsBase);
            if (defaultWarehouse != null)
                fromWarehouseComboBox.SelectedValue = defaultWarehouse.ID;
        }

        public MovingForm(bool isEdit, MovingDocument document) : this()
        {
            isEditWindowMode = isEdit;
            userName = document.User;

            WindowsFormProvider.FillControlsFromModel(this, document);

            DocumentIsProcessed = document.IsProcessed;

            dtnDocDate.Value = document.DocumentDate;

            if (!string.IsNullOrEmpty(txtNumber.Text))
            {
                documentItems = Content.MovingManager.GetMovingDocumentItems(document.ID);
            }

            mainGridControl.DataSource = documentItems;
        }

        private void FillCategoties()
        {
            treeListCategories.DataSource = Content.ProductManager.GetCategoriesList();

            var nodeAll = treeListCategories.AppendNode(null, null);
            nodeAll.SetValue(NameCol, "Все");
            nodeAll.SetValue(IdCol, 0);
            treeListCategories.SetNodeIndex(nodeAll, 0);
            treeListCategories.FocusedNode = nodeAll;
        }

        private void MovingForm_Load(object sender, EventArgs e)
        {
            FillCategoties();

            if (DocumentIsProcessed)
            {
                btnProcess.Text = Resources.Unprocess;
                btnProcess.ForeColor = Color.DarkRed;
            }
            else
            {
                btnProcess.Text = Resources.Process;
                btnProcess.ForeColor = Color.Green;
            }
        }

        private void treeListCategories_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            if (e.Node != null)
            {
                var id = (int)e.Node.GetValue(IdCol);
                FillProducts(id);
            }
        }

        private void FillProducts(int catId)
        {
            var products = Content.ProductManager.GetAllProductsInCategory(catId).OrderBy(x => x.Name).ToList();
            if (products.Count != 0)
            {
                lstProduct.DataSource = products;
            }
        }

        private void btnSelectParent_Click(object sender, EventArgs e)
        {
            var selectPartnerForm = new SelectWarehousezForm();

            if (selectPartnerForm.ShowDialog() == DialogResult.OK)
            {
                toWarehouseComboBox.SelectedValue = selectPartnerForm.Warehouse.ID;
            }
        }

        private void btnSelectWarehouse_Click(object sender, EventArgs e)
        {
            var selectWarehouseForm = new SelectWarehousezForm();

            if (selectWarehouseForm.ShowDialog() == DialogResult.OK)
            {
                fromWarehouseComboBox.SelectedValue = selectWarehouseForm.Warehouse.ID;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lstProduct_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            mainGridView.BeginDataUpdate();
            var product = (Product)lstProduct.SelectedItem;

            if (product == null)
                return;

            documentItems.Add(new MovingDocumentItem
            {
                ID = -1,
                Product = product,
                ProductID = product.ID,
                Amount = 0,
            });

            mainGridControl.DataSource = documentItems;
            mainGridView.EndDataUpdate();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var movingDocument = new MovingDocument
            {
                ID = string.IsNullOrEmpty(txtNumber.Text) ? 0 : Convert.ToInt32(txtNumber.Text.Trim()),
                DocumentDate = dtnDocDate.Value,
                PurposeWarehouseID = ((Warehouse)toWarehouseComboBox.SelectedItem).ID,
                WarehouseID = ((Warehouse)fromWarehouseComboBox.SelectedItem).ID,
                IsProcessed = DocumentIsProcessed,
                Note = richOtherInfo.Text
            };

            if (isEditWindowMode)
            {
                movingDocument.User = userName;
                Content.MovingManager.UpdateMovingDocument(new MovingData
                {
                    Document = movingDocument,
                    DocumentItems = documentItems
                });

                //string err = MovingManager.TestMovingDocument(((Warehouse) fromWarehouseComboBox.SelectedItem).ID, documentItems);
                string err = "";

                if (string.IsNullOrEmpty(err))
                {

                    if (DocumentIsProcessed)
                    {
                        UnProcessDocument();
                        ProcessDocument();
                    }
                }
                else
                {
                    MessageBox.Show(err);
                }
            }
            else
            {
                movingDocument.User = userName = Content.UserManager.IsLogged() ? Content.UserManager.UserLogged.Name : string.Empty;
                int docId = Content.MovingManager.AddMovingDocument(movingDocument);

                //string err = MovingManager.TestMovingDocument(((Warehouse)fromWarehouseComboBox.SelectedItem).ID, documentItems);

                string err = "";

                if (string.IsNullOrEmpty(err))
                {
                    Content.MovingManager.UpdateMovingDocument(new MovingData
                    {
                        Document = movingDocument,
                        DocumentItems = documentItems
                    });
                    txtNumber.Text = docId.ToString(CultureInfo.InvariantCulture);
                    isEditWindowMode = true;
                }
                else
                {
                    MessageBox.Show(err);
                }
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Можно ли сохранить документ </summary>
        ///=================================================
        private bool IsCanSave
        {
            get
            {
                bool isCanSave = !(isEditWindowMode && Content.UserManager.UserLogged.Role.Name != "Admin");

                return isCanSave;
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Сохранить документ </summary>
        ////=======================================
        private void SaveDoc()
        {
            var movingDocument = new MovingDocument
            {
                ID = string.IsNullOrEmpty(txtNumber.Text) ? 0 : Convert.ToInt32(txtNumber.Text.Trim()),
                DocumentDate = dtnDocDate.Value,
                PurposeWarehouseID = ((Warehouse)toWarehouseComboBox.SelectedItem).ID,
                WarehouseID = ((Warehouse)fromWarehouseComboBox.SelectedItem).ID,
                IsProcessed = DocumentIsProcessed,
                Note = richOtherInfo.Text
            };


            if (isEditWindowMode)
            {
                movingDocument.User = userName;

                string err = Content.MovingManager.TestMovingDocument(((Warehouse)fromWarehouseComboBox.SelectedItem).ID, documentItems);

                if (string.IsNullOrEmpty(err))
                {
                    Content.MovingManager.UpdateMovingDocument(new MovingData
                    {
                        Document = movingDocument,
                        DocumentItems = documentItems
                    });

                    if (DocumentIsProcessed)
                    {
                        UnProcessDocument();
                        ProcessDocument();
                    }
                }
                else
                {
                    MessageBox.Show(err);
                }
            }
            else
            {
                movingDocument.User = userName = Content.UserManager.IsLogged() ? Content.UserManager.UserLogged.Name : string.Empty;
                int docId = Content.MovingManager.AddMovingDocument(movingDocument);

                string err = Content.MovingManager.TestMovingDocument(((Warehouse)fromWarehouseComboBox.SelectedItem).ID, documentItems);

                if (string.IsNullOrEmpty(err))
                {

                    Content.MovingManager.UpdateMovingDocument(new MovingData {
                        Document = movingDocument,
                        DocumentItems = documentItems
                    });
                    txtNumber.Text = docId.ToString(CultureInfo.InvariantCulture);
                    isEditWindowMode = true;
                }
                else
                {
                    MessageBox.Show(err);
                }
            }
        }


        //_____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Отменить документ </summary>
        ////======================================
        private void UnProcessDocument()
        {
            Content.RemainsOfGoodWiringsManager.UnProcessMovingDocument(Convert.ToInt32(txtNumber.Text));

            DocumentIsProcessed = false;

            btnProcess.Text = Resources.Process;
            btnProcess.ForeColor = Color.Green;
        }


        //_____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Провести документ </summary>
        ////======================================
        private void ProcessDocument()
        {
            if (string.IsNullOrEmpty(txtNumber.Text))
            {
                MessageBox.Show(Resources.DocIsNotProcessed, Resources.WarningTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                List<RemainsOfGoodWirings> wiringses = documentItems.Select(documentItem => new RemainsOfGoodWirings
                {
                    Amount = -documentItem.Amount,
                    Date = dtnDocDate.Value,
                    WarehouseID = ((Warehouse)fromWarehouseComboBox.SelectedItem).ID,
                    ProductID = documentItem.ProductID,
                    MovingDocumentID = Convert.ToInt32(txtNumber.Text)
                }).ToList();

                // Проверить на наличие товара на выбранном складе
                string testMessage = TestProductsAmount(wiringses, (Warehouse)fromWarehouseComboBox.SelectedItem, dtnDocDate.Value);

                // Если нет сообщения об ошибки, проводим
                if (string.IsNullOrEmpty(testMessage))
                {
                    Content.RemainsOfGoodWiringsManager.ProcessMovingDocument(Convert.ToInt32(txtNumber.Text), wiringses, (Warehouse)toWarehouseComboBox.SelectedItem);

                    DocumentIsProcessed = true;

                    btnProcess.Text = Resources.Unprocess;
                    btnProcess.ForeColor = Color.DarkRed;
                }
                else    // Если есть сообщение об ошибки - выводим его
                {
                    MessageBox.Show(testMessage);
                }

            }
        }


        //_____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Проверить на наличие товара </summary>
        ////================================================
        private string TestProductsAmount(List<RemainsOfGoodWirings> wiringses, Warehouse fromWarehouse, DateTime dateOfOperation)
        {
            string testMessage = "";

            // Получить текущий срез по складу
            var reportManager = Content.RemainsOfGoodReportManager;
            List<RemainsOfGoodReportRecord> reportItems = reportManager.GetReportDate(dateOfOperation, fromWarehouse.ID);

            if (wiringses != null && wiringses.Count > 0)
            {
                foreach (RemainsOfGoodWirings remainsOfGoodWiringse in wiringses)
                {
                    foreach (RemainsOfGoodReportRecord remainsOfGoodReportRecord in reportItems)
                    {
                        if (remainsOfGoodWiringse.ProductID == remainsOfGoodReportRecord.ProductId)
                        {
                            if (-remainsOfGoodWiringse.Amount > remainsOfGoodReportRecord.ProductCount)
                            {
                                string productName = "";

                                if (remainsOfGoodWiringse.Product != null)
                                {
                                    productName = remainsOfGoodWiringse.Product.Name;
                                }
                                else
                                {
                                    var product = Content.ProductManager.GetById(remainsOfGoodWiringse.ProductID);
                                    if (product != null)
                                    {
                                        productName = product.Name;
                                    }
                                }

                                testMessage += "Продукта " + productName + " меньше чем переводится (есть " + remainsOfGoodReportRecord.ProductCount + ", а запрашивается " + -remainsOfGoodWiringse.Amount + ") \r\n";

                            }

                            break;
                        }
                    }
                }
            }

            return testMessage;
        }




        private void btnProcess_Click(object sender, EventArgs e)
        {
            btnSave_Click(sender, e);

            if (IsCanSave)
            {
                if (DocumentIsProcessed)
                {
                    UnProcessDocument();
                }
                else
                {
                    ProcessDocument();
                }
            }
            else
            {
                MessageBox.Show(@"У вас нет прав");
            }
        }

        private void mainGridControl_DoubleClick(object sender, EventArgs e)
        {
            mainGridView.BeginDataUpdate();
            var item = (MovingDocumentItem)mainGridView.GetRow(mainGridView.FocusedRowHandle);

            if (item == null)
                return;

            documentItems.Remove(item);

            mainGridControl.DataSource = documentItems;

            mainGridView.EndDataUpdate();
        }

        private void mainGridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.Name == "columnAmmount")
            {
                var view = (GridView)sender;
                var currentRow = (MovingDocumentItem)view.GetRow(e.RowHandle);

                currentRow.Amount = Math.Round(currentRow.Amount, 3);
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            printableComponentLink.CreateDocument();
            printableComponentLink.ShowPreview();
        }

        private void printableComponentLink_CreateReportHeaderArea(object sender, CreateAreaEventArgs e)
        {
            var brick = e.Graph.DrawString(string.Format("Склад отправитель: {0}\nСклад получатель: {1}", fromWarehouseComboBox.Text, toWarehouseComboBox.Text), Color.Navy, new RectangleF(0, 0, 560, 60), BorderSide.None);

            brick.Font = new Font("Arial", 12);
            brick.HorzAlignment = HorzAlignment.Near;
            brick.StringFormat = new BrickStringFormat(StringAlignment.Near);
        }

        private void printableComponentLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            var brick = e.Graph.DrawString(string.Format("Перемещение товаров №{0} от {1:dd.MM.yyyy}.", txtNumber.Text,
                                                 dtnDocDate.Value), Color.Navy, new RectangleF(0, 0, 560, 60), BorderSide.None);

            brick.Font = new Font("Arial", 16);
            brick.HorzAlignment = HorzAlignment.Near;
            brick.StringFormat = new BrickStringFormat(StringAlignment.Near);
        }
    }
}