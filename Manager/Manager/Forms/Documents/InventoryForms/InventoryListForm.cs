﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Documents.Inventories;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Documents.Inventory;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Documents.InventoryForms
{
    public partial class InventoryListForm : Form
    {
        public InventoryListForm()
        {
            InitializeComponent();
            LoadInventoryList(new object(), new EventArgs());
        }

        private void LoadInventoryList(object sender, EventArgs args)
        {
            mainGridControl.DataSource = Content.InventoryManager.GetInventoryDocuments();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var selectedRow = (InventoryDocument)mainGridView.GetFocusedRow();

            if (selectedRow == null)
            {
                MessageBox.Show(Resources.InventoryDocumentIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return;
            }

            string msg = selectedRow.IsMarkToDelete
                         ? Resources.UnMarkDeleteDocument
                         : Resources.MarkDeleteDocument;

            if (MessageBox.Show(msg, Resources.WarningTitle, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Content.InventoryManager.DeleteInventoryDocument(selectedRow);
                UpdateDataGrid(sender, e);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var inventoryForm = new InventoryForm();
            inventoryForm.FormClosed += UpdateDataGrid;
            inventoryForm.Show();
        }

        private void UpdateDataGrid(object sender, EventArgs e)
        {
            var first = mainGridView.TopRowIndex;
            var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);
            LoadInventoryList(sender, e);
            mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
            mainGridView.MakeRowVisible(first);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            UpdateDataGrid(sender, e);
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            var selectedRow = (InventoryDocument)mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetInventoryForm(false, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.InventoryDocumentIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void GetInventoryForm(bool isEdit, int documentId)
        {
            var inventoryDocument = Content.InventoryManager.GetInventoryDocument(documentId);

            var inventoryForm = new InventoryForm(isEdit, inventoryDocument);

            inventoryForm.FormClosed += LoadInventoryList;
            inventoryForm.Show();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var selectedRow = (InventoryDocument)mainGridView.GetFocusedRow();
             if (selectedRow != null)
             {
                 GetInventoryForm(true, selectedRow.ID);
             }
             else
             {
                 MessageBox.Show(Resources.InventoryDocumentIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                 MessageBoxIcon.Warning);
             }
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            var currentRow = (InventoryDocument)view.GetRow(e.RowHandle);

            if (currentRow.IsMarkToDelete)
                e.Appearance.BackColor = Color.Salmon;

            if (currentRow.IsProcessed)
                e.Appearance.BackColor = Color.LightGreen;

            if (currentRow.NeedToRecalculate)
            {
                e.Appearance.GradientMode = LinearGradientMode.Horizontal;
                e.Appearance.BackColor2 = Color.CornflowerBlue;
            }
               

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }

        private void mainGridView_DoubleClick(object sender, EventArgs e)
        {
            var view = (GridView)sender;

            var pt = view.GridControl.PointToClient(MousePosition);

            var info = view.CalcHitInfo(pt);
            if (view.IsDataRow(info.RowHandle))
            {
                var documentId = (int)view.GetRowCellValue(info.RowHandle, columnNumber);
                GetInventoryForm(true, documentId);
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }
    }
}