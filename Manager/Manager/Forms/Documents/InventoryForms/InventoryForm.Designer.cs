﻿namespace LaRenzo.Forms.Documents.InventoryForms
{
    partial class InventoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InventoryForm));
            this.spltMain = new System.Windows.Forms.SplitContainer();
            this.spltLeft = new System.Windows.Forms.SplitContainer();
            this.treeListCategories = new DevExpress.XtraTreeList.TreeList();
            this.NameCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.IdCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.imgList = new System.Windows.Forms.ImageList(this.components);
            this.lstProduct = new System.Windows.Forms.ListBox();
            this.spltRight = new System.Windows.Forms.SplitContainer();
            this.spltMainCenter = new System.Windows.Forms.SplitContainer();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnAmmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnBaseMeasure = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnAmountFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDelta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDeltaFin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnAmountFin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.mainToolStrip = new System.Windows.Forms.ToolStrip();
            this.FillInventoryDoc = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.marqueeProgressBarControl3 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnProcess = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cmbWarehouse = new System.Windows.Forms.ComboBox();
            this.btnSelectWarehouse = new System.Windows.Forms.Button();
            this.txtNumber = new System.Windows.Forms.TextBox();
            this.dtnDocDate = new System.Windows.Forms.DateTimePicker();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.lblNumber = new System.Windows.Forms.Label();
            this.richOtherInfo = new System.Windows.Forms.RichTextBox();
            this.backgroundWorkerProcess = new System.ComponentModel.BackgroundWorker();
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.printableComponentLink = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.spltMain)).BeginInit();
            this.spltMain.Panel1.SuspendLayout();
            this.spltMain.Panel2.SuspendLayout();
            this.spltMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spltLeft)).BeginInit();
            this.spltLeft.Panel1.SuspendLayout();
            this.spltLeft.Panel2.SuspendLayout();
            this.spltLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListCategories)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spltRight)).BeginInit();
            this.spltRight.Panel1.SuspendLayout();
            this.spltRight.Panel2.SuspendLayout();
            this.spltRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spltMainCenter)).BeginInit();
            this.spltMainCenter.Panel1.SuspendLayout();
            this.spltMainCenter.Panel2.SuspendLayout();
            this.spltMainCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            this.mainToolStrip.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).BeginInit();
            this.SuspendLayout();
            // 
            // spltMain
            // 
            this.spltMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltMain.Location = new System.Drawing.Point(0, 0);
            this.spltMain.Name = "spltMain";
            // 
            // spltMain.Panel1
            // 
            this.spltMain.Panel1.Controls.Add(this.spltLeft);
            // 
            // spltMain.Panel2
            // 
            this.spltMain.Panel2.Controls.Add(this.spltRight);
            this.spltMain.Size = new System.Drawing.Size(1216, 692);
            this.spltMain.SplitterDistance = 406;
            this.spltMain.TabIndex = 3;
            // 
            // spltLeft
            // 
            this.spltLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltLeft.Location = new System.Drawing.Point(0, 0);
            this.spltLeft.Name = "spltLeft";
            // 
            // spltLeft.Panel1
            // 
            this.spltLeft.Panel1.Controls.Add(this.treeListCategories);
            // 
            // spltLeft.Panel2
            // 
            this.spltLeft.Panel2.Controls.Add(this.lstProduct);
            this.spltLeft.Size = new System.Drawing.Size(406, 692);
            this.spltLeft.SplitterDistance = 191;
            this.spltLeft.TabIndex = 0;
            // 
            // treeListCategories
            // 
            this.treeListCategories.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.NameCol,
            this.IdCol});
            this.treeListCategories.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListCategories.DragNodesMode = DevExpress.XtraTreeList.TreeListDragNodesMode.Standard;
            this.treeListCategories.Location = new System.Drawing.Point(0, 0);
            this.treeListCategories.Name = "treeListCategories";
            this.treeListCategories.OptionsBehavior.DragNodes = true;
            this.treeListCategories.OptionsBehavior.Editable = false;
            this.treeListCategories.OptionsBehavior.EnableFiltering = true;
            this.treeListCategories.ParentFieldName = "ParentCategoryID";
            this.treeListCategories.SelectImageList = this.imgList;
            this.treeListCategories.Size = new System.Drawing.Size(191, 692);
            this.treeListCategories.TabIndex = 0;
            this.treeListCategories.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeListCategories_FocusedNodeChanged);
            // 
            // NameCol
            // 
            this.NameCol.Caption = "Категории";
            this.NameCol.FieldName = "Name";
            this.NameCol.MinWidth = 37;
            this.NameCol.Name = "NameCol";
            this.NameCol.Visible = true;
            this.NameCol.VisibleIndex = 0;
            // 
            // IdCol
            // 
            this.IdCol.Caption = "ID";
            this.IdCol.FieldName = "ID";
            this.IdCol.Name = "IdCol";
            // 
            // imgList
            // 
            this.imgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgList.ImageStream")));
            this.imgList.TransparentColor = System.Drawing.Color.Transparent;
            this.imgList.Images.SetKeyName(0, "Deleket-Sleek-Xp-Basic-Folder.ico");
            // 
            // lstProduct
            // 
            this.lstProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstProduct.FormattingEnabled = true;
            this.lstProduct.Location = new System.Drawing.Point(0, 0);
            this.lstProduct.Name = "lstProduct";
            this.lstProduct.Size = new System.Drawing.Size(211, 692);
            this.lstProduct.TabIndex = 0;
            this.lstProduct.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstProduct_MouseDoubleClick);
            // 
            // spltRight
            // 
            this.spltRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltRight.Location = new System.Drawing.Point(0, 0);
            this.spltRight.Name = "spltRight";
            // 
            // spltRight.Panel1
            // 
            this.spltRight.Panel1.Controls.Add(this.spltMainCenter);
            // 
            // spltRight.Panel2
            // 
            this.spltRight.Panel2.Controls.Add(this.richOtherInfo);
            this.spltRight.Size = new System.Drawing.Size(806, 692);
            this.spltRight.SplitterDistance = 590;
            this.spltRight.TabIndex = 0;
            // 
            // spltMainCenter
            // 
            this.spltMainCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltMainCenter.Location = new System.Drawing.Point(0, 0);
            this.spltMainCenter.Name = "spltMainCenter";
            this.spltMainCenter.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spltMainCenter.Panel1
            // 
            this.spltMainCenter.Panel1.Controls.Add(this.mainGridControl);
            this.spltMainCenter.Panel1.Controls.Add(this.mainToolStrip);
            // 
            // spltMainCenter.Panel2
            // 
            this.spltMainCenter.Panel2.Controls.Add(this.bottomPanel);
            this.spltMainCenter.Size = new System.Drawing.Size(590, 692);
            this.spltMainCenter.SplitterDistance = 595;
            this.spltMainCenter.TabIndex = 0;
            // 
            // mainGridControl
            // 
            this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl.Location = new System.Drawing.Point(0, 25);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(590, 570);
            this.mainGridControl.TabIndex = 5;
            this.mainGridControl.Tag = "1";
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnName,
            this.columnAmmount,
            this.columnBaseMeasure,
            this.columnAmountFact,
            this.columnDelta,
            this.columnDeltaFin,
            this.columnAmountFin,
            this.columnPrice});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsView.ShowFooter = true;
            this.mainGridView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.mainGridView_CellValueChanged);
            this.mainGridView.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.mainGridView_MouseWheel);
            this.mainGridView.DoubleClick += new System.EventHandler(this.mainGridView_DoubleClick);
            // 
            // columnName
            // 
            this.columnName.Caption = "Название";
            this.columnName.FieldName = "Product.Name";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.OptionsColumn.AllowFocus = false;
            this.columnName.OptionsColumn.ReadOnly = true;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 0;
            this.columnName.Width = 169;
            // 
            // columnAmmount
            // 
            this.columnAmmount.Caption = "Кол. (учет)";
            this.columnAmmount.DisplayFormat.FormatString = "F3";
            this.columnAmmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnAmmount.FieldName = "Amount";
            this.columnAmmount.Name = "columnAmmount";
            this.columnAmmount.OptionsColumn.AllowEdit = false;
            this.columnAmmount.OptionsColumn.AllowFocus = false;
            this.columnAmmount.OptionsColumn.ReadOnly = true;
            this.columnAmmount.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.columnAmmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.columnAmmount.Visible = true;
            this.columnAmmount.VisibleIndex = 1;
            this.columnAmmount.Width = 105;
            // 
            // columnBaseMeasure
            // 
            this.columnBaseMeasure.Caption = "Ед. Изм.";
            this.columnBaseMeasure.FieldName = "Product.Measure.Name";
            this.columnBaseMeasure.Name = "columnBaseMeasure";
            this.columnBaseMeasure.OptionsColumn.AllowEdit = false;
            this.columnBaseMeasure.OptionsColumn.ReadOnly = true;
            this.columnBaseMeasure.Visible = true;
            this.columnBaseMeasure.VisibleIndex = 4;
            this.columnBaseMeasure.Width = 50;
            // 
            // columnAmountFact
            // 
            this.columnAmountFact.Caption = "Кол. (Факт)";
            this.columnAmountFact.DisplayFormat.FormatString = "F3";
            this.columnAmountFact.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnAmountFact.FieldName = "AmountFact";
            this.columnAmountFact.Name = "columnAmountFact";
            this.columnAmountFact.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.columnAmountFact.Visible = true;
            this.columnAmountFact.VisibleIndex = 2;
            this.columnAmountFact.Width = 105;
            // 
            // columnDelta
            // 
            this.columnDelta.Caption = "Отклонение";
            this.columnDelta.DisplayFormat.FormatString = "F3";
            this.columnDelta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnDelta.FieldName = "Delta";
            this.columnDelta.Name = "columnDelta";
            this.columnDelta.OptionsColumn.AllowEdit = false;
            this.columnDelta.OptionsColumn.ReadOnly = true;
            this.columnDelta.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.columnDelta.Visible = true;
            this.columnDelta.VisibleIndex = 3;
            this.columnDelta.Width = 105;
            // 
            // columnDeltaFin
            // 
            this.columnDeltaFin.Caption = "Сумма откл.";
            this.columnDeltaFin.FieldName = "DeltaFin";
            this.columnDeltaFin.Name = "columnDeltaFin";
            this.columnDeltaFin.OptionsColumn.AllowEdit = false;
            this.columnDeltaFin.OptionsColumn.AllowFocus = false;
            this.columnDeltaFin.OptionsColumn.ReadOnly = true;
            this.columnDeltaFin.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.columnDeltaFin.Visible = true;
            this.columnDeltaFin.VisibleIndex = 7;
            // 
            // columnAmountFin
            // 
            this.columnAmountFin.Caption = "Сумма факт";
            this.columnAmountFin.FieldName = "AmmountFactFin";
            this.columnAmountFin.Name = "columnAmountFin";
            this.columnAmountFin.OptionsColumn.AllowEdit = false;
            this.columnAmountFin.OptionsColumn.AllowFocus = false;
            this.columnAmountFin.OptionsColumn.ReadOnly = true;
            this.columnAmountFin.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.columnAmountFin.Visible = true;
            this.columnAmountFin.VisibleIndex = 6;
            // 
            // columnPrice
            // 
            this.columnPrice.Caption = "Цена";
            this.columnPrice.FieldName = "Price";
            this.columnPrice.Name = "columnPrice";
            this.columnPrice.OptionsColumn.AllowEdit = false;
            this.columnPrice.OptionsColumn.AllowFocus = false;
            this.columnPrice.OptionsColumn.ReadOnly = true;
            this.columnPrice.Visible = true;
            this.columnPrice.VisibleIndex = 5;
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FillInventoryDoc,
            this.toolStripSeparator1,
            this.btnRefresh,
            this.toolStripSeparator2,
            this.toolStripButton1});
            this.mainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Size = new System.Drawing.Size(590, 25);
            this.mainToolStrip.TabIndex = 6;
            this.mainToolStrip.Text = "toolStrip1";
            // 
            // FillInventoryDoc
            // 
            this.FillInventoryDoc.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.FillInventoryDoc.Image = ((System.Drawing.Image)(resources.GetObject("FillInventoryDoc.Image")));
            this.FillInventoryDoc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.FillInventoryDoc.Name = "FillInventoryDoc";
            this.FillInventoryDoc.Size = new System.Drawing.Size(70, 22);
            this.FillInventoryDoc.Text = "Заполнить";
            this.FillInventoryDoc.Click += new System.EventHandler(this.FillInventoryBuutton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnRefresh
            // 
            this.btnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnRefresh.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Image")));
            this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(65, 22);
            this.btnRefresh.Text = "Обновить";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.pnlBottom);
            this.bottomPanel.Controls.Add(this.cmbWarehouse);
            this.bottomPanel.Controls.Add(this.btnSelectWarehouse);
            this.bottomPanel.Controls.Add(this.txtNumber);
            this.bottomPanel.Controls.Add(this.dtnDocDate);
            this.bottomPanel.Controls.Add(this.lblDate);
            this.bottomPanel.Controls.Add(this.lblWarehouse);
            this.bottomPanel.Controls.Add(this.lblNumber);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bottomPanel.Location = new System.Drawing.Point(0, 0);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(590, 93);
            this.bottomPanel.TabIndex = 6;
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.marqueeProgressBarControl3);
            this.pnlBottom.Controls.Add(this.btnPrint);
            this.pnlBottom.Controls.Add(this.btnProcess);
            this.pnlBottom.Controls.Add(this.btnSave);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 69);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(590, 24);
            this.pnlBottom.TabIndex = 55;
            // 
            // marqueeProgressBarControl3
            // 
            this.marqueeProgressBarControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.marqueeProgressBarControl3.EditValue = 0;
            this.marqueeProgressBarControl3.Enabled = false;
            this.marqueeProgressBarControl3.Location = new System.Drawing.Point(3, 2);
            this.marqueeProgressBarControl3.Name = "marqueeProgressBarControl3";
            this.marqueeProgressBarControl3.Properties.Appearance.BackColor = System.Drawing.Color.Gainsboro;
            this.marqueeProgressBarControl3.Properties.Appearance.BackColor2 = System.Drawing.Color.Gainsboro;
            this.marqueeProgressBarControl3.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.marqueeProgressBarControl3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.marqueeProgressBarControl3.Properties.EndColor = System.Drawing.Color.ForestGreen;
            this.marqueeProgressBarControl3.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
            this.marqueeProgressBarControl3.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.marqueeProgressBarControl3.Properties.StartColor = System.Drawing.Color.ForestGreen;
            this.marqueeProgressBarControl3.Size = new System.Drawing.Size(218, 18);
            this.marqueeProgressBarControl3.TabIndex = 12;
            // 
            // btnPrint
            // 
            this.btnPrint.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPrint.Location = new System.Drawing.Point(227, 0);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(87, 22);
            this.btnPrint.TabIndex = 5;
            this.btnPrint.Text = "Печать";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnProcess.ForeColor = System.Drawing.Color.Green;
            this.btnProcess.Location = new System.Drawing.Point(314, 0);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(100, 22);
            this.btnProcess.TabIndex = 6;
            this.btnProcess.Text = "Провести";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.Location = new System.Drawing.Point(414, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 22);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Записать";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.Location = new System.Drawing.Point(501, 0);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 22);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cmbWarehouse
            // 
            this.cmbWarehouse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWarehouse.FormattingEnabled = true;
            this.cmbWarehouse.Location = new System.Drawing.Point(53, 41);
            this.cmbWarehouse.Name = "cmbWarehouse";
            this.cmbWarehouse.Size = new System.Drawing.Size(252, 21);
            this.cmbWarehouse.TabIndex = 3;
            this.cmbWarehouse.Tag = "WarehouseID";
            // 
            // btnSelectWarehouse
            // 
            this.btnSelectWarehouse.Location = new System.Drawing.Point(316, 42);
            this.btnSelectWarehouse.Name = "btnSelectWarehouse";
            this.btnSelectWarehouse.Size = new System.Drawing.Size(24, 20);
            this.btnSelectWarehouse.TabIndex = 4;
            this.btnSelectWarehouse.Text = "...";
            this.btnSelectWarehouse.UseVisualStyleBackColor = true;
            this.btnSelectWarehouse.Click += new System.EventHandler(this.btnSelectWarehouse_Click);
            // 
            // txtNumber
            // 
            this.txtNumber.Enabled = false;
            this.txtNumber.Location = new System.Drawing.Point(53, 12);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(120, 20);
            this.txtNumber.TabIndex = 1;
            this.txtNumber.Tag = "id";
            // 
            // dtnDocDate
            // 
            this.dtnDocDate.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtnDocDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtnDocDate.Location = new System.Drawing.Point(208, 12);
            this.dtnDocDate.Name = "dtnDocDate";
            this.dtnDocDate.Size = new System.Drawing.Size(132, 20);
            this.dtnDocDate.TabIndex = 2;
            this.dtnDocDate.Tag = "DocumentDate";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(181, 15);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(21, 13);
            this.lblDate.TabIndex = 42;
            this.lblDate.Text = "от:";
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.Location = new System.Drawing.Point(6, 44);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(41, 13);
            this.lblWarehouse.TabIndex = 44;
            this.lblWarehouse.Text = "Склад:";
            // 
            // lblNumber
            // 
            this.lblNumber.AutoSize = true;
            this.lblNumber.Location = new System.Drawing.Point(3, 15);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(44, 13);
            this.lblNumber.TabIndex = 46;
            this.lblNumber.Text = "Номер:";
            // 
            // richOtherInfo
            // 
            this.richOtherInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richOtherInfo.Location = new System.Drawing.Point(0, 0);
            this.richOtherInfo.Name = "richOtherInfo";
            this.richOtherInfo.Size = new System.Drawing.Size(212, 692);
            this.richOtherInfo.TabIndex = 0;
            this.richOtherInfo.Tag = "Note";
            this.richOtherInfo.Text = "Дополнительная информация:";
            // 
            // backgroundWorkerProcess
            // 
            this.backgroundWorkerProcess.WorkerReportsProgress = true;
            this.backgroundWorkerProcess.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerProcess_DoWork);
            this.backgroundWorkerProcess.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerProcess_ProgressChanged);
            this.backgroundWorkerProcess.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerReportGenerator_RunWorkerCompleted);
            // 
            // printingSystem
            // 
            this.printingSystem.Links.AddRange(new object[] {
            this.printableComponentLink});
            // 
            // printableComponentLink
            // 
            this.printableComponentLink.Component = this.mainGridControl;
            this.printableComponentLink.Landscape = true;
            this.printableComponentLink.Margins = new System.Drawing.Printing.Margins(100, 100, 76, 100);
            this.printableComponentLink.MinMargins = new System.Drawing.Printing.Margins(20, 20, 50, 20);
            this.printableComponentLink.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.printableComponentLink.PrintingSystemBase = this.printingSystem;
            this.printableComponentLink.CreateMarginalHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.printableComponentLink_CreateMarginalHeaderArea);
            this.printableComponentLink.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.printableComponentLink_CreateReportHeaderArea);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(170, 22);
            this.toolStripButton1.Text = "Показать только недосдачу";
            this.toolStripButton1.ToolTipText = "Автоматически добавляет к списку позиции инвентаризации фильтр, который отображае" +
    "т те позиции, по которым есть недосдача > 0";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // InventoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1216, 692);
            this.Controls.Add(this.spltMain);
            this.Name = "InventoryForm";
            this.Text = "Инвентаризация товаров";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.InventoryForm_FormClosing);
            this.Load += new System.EventHandler(this.InventoryForm_Load);
            this.spltMain.Panel1.ResumeLayout(false);
            this.spltMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltMain)).EndInit();
            this.spltMain.ResumeLayout(false);
            this.spltLeft.Panel1.ResumeLayout(false);
            this.spltLeft.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltLeft)).EndInit();
            this.spltLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListCategories)).EndInit();
            this.spltRight.Panel1.ResumeLayout(false);
            this.spltRight.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltRight)).EndInit();
            this.spltRight.ResumeLayout(false);
            this.spltMainCenter.Panel1.ResumeLayout(false);
            this.spltMainCenter.Panel1.PerformLayout();
            this.spltMainCenter.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltMainCenter)).EndInit();
            this.spltMainCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.bottomPanel.ResumeLayout(false);
            this.bottomPanel.PerformLayout();
            this.pnlBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer spltMain;
        private System.Windows.Forms.SplitContainer spltLeft;
        private DevExpress.XtraTreeList.TreeList treeListCategories;
        private DevExpress.XtraTreeList.Columns.TreeListColumn NameCol;
        private DevExpress.XtraTreeList.Columns.TreeListColumn IdCol;
        private System.Windows.Forms.ImageList imgList;
        private System.Windows.Forms.ListBox lstProduct;
        private System.Windows.Forms.SplitContainer spltRight;
        private System.Windows.Forms.SplitContainer spltMainCenter;
        private DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private DevExpress.XtraGrid.Columns.GridColumn columnAmmount;
        private DevExpress.XtraGrid.Columns.GridColumn columnBaseMeasure;
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox cmbWarehouse;
        private System.Windows.Forms.Button btnSelectWarehouse;
        private System.Windows.Forms.TextBox txtNumber;
        private System.Windows.Forms.DateTimePicker dtnDocDate;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblWarehouse;
        private System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.RichTextBox richOtherInfo;
        private DevExpress.XtraGrid.Columns.GridColumn columnAmountFact;
        private DevExpress.XtraGrid.Columns.GridColumn columnDelta;
        private System.Windows.Forms.ToolStrip mainToolStrip;
        private System.Windows.Forms.ToolStripButton FillInventoryDoc;
        private System.ComponentModel.BackgroundWorker backgroundWorkerProcess;
        private DevExpress.XtraEditors.MarqueeProgressBarControl marqueeProgressBarControl3;
        private DevExpress.XtraGrid.Columns.GridColumn columnDeltaFin;
        private DevExpress.XtraGrid.Columns.GridColumn columnAmountFin;
        private DevExpress.XtraGrid.Columns.GridColumn columnPrice;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnRefresh;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;
        private DevExpress.XtraPrinting.PrintableComponentLink printableComponentLink;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
    }
}