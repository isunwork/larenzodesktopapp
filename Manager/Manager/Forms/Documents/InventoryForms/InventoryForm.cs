﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Entities.Documents.Inventories;
using LaRenzo.DataRepository.Entities.Wirings;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Products;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Catalogs.Warehouses;
using LaRenzo.DataRepository.Repositories.Documents.Inventory;
using LaRenzo.DataRepository.Repositories.Wiringses.RemainsOfGood;
using LaRenzo.Forms.Catalog.WarehouseForms;
using LaRenzo.FormsProvider;
using LaRenzo.Properties;
using CellValueChangedEventArgs = DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs;

namespace LaRenzo.Forms.Documents.InventoryForms
{
    public partial class InventoryForm : Form
    {

        private string _userName = string.Empty;
        private List<InventoryDocumentItem> _documentItems;
        private bool _isEditWindow;

        public InventoryForm()
        {
            InitializeComponent();
            _documentItems = new List<InventoryDocumentItem>();
            lstProduct.DisplayMember = "Name";

            List<Warehouse> warehouses = Content.WarehouseManager.GetWarehouses();
            cmbWarehouse.DataSource = warehouses;
            cmbWarehouse.DisplayMember = "Name";
            cmbWarehouse.ValueMember = "ID";

            Warehouse defaultWarehouse =
                Content.WarehouseManager.GetWarehouses().FirstOrDefault(x => x.IsBase);
            if (defaultWarehouse != null)
                cmbWarehouse.SelectedValue = defaultWarehouse.ID;
        }

        public InventoryForm(bool isEdit, InventoryDocument document)
            : this()
        {
            _isEditWindow = isEdit;

            _userName = document.User;
            
            WindowsFormProvider.FillControlsFromModel(this, document);

            DocumentIsProcessed = document.IsProcessed;

            dtnDocDate.Value = document.DocumentDate;

            if (!string.IsNullOrEmpty(txtNumber.Text))
                _documentItems = Content.InventoryManager.GetInventoryDocumentItems(document.ID);

            mainGridControl.DataSource = _documentItems;


        }

        private bool DocumentIsProcessed { set; get; }

        private void FillCategoties()
        {
            treeListCategories.DataSource = Content.ProductManager.GetCategoriesList();

            TreeListNode nodeAll = treeListCategories.AppendNode(null, null);
            nodeAll.SetValue(NameCol, "Все");
            nodeAll.SetValue(IdCol, 0);
            treeListCategories.SetNodeIndex(nodeAll, 0);
            treeListCategories.FocusedNode = nodeAll;
        }

        private void InventoryForm_Load(object sender, EventArgs e)
        {
            FillCategoties();

            if (DocumentIsProcessed)
            {
                btnProcess.Text = Resources.Unprocess;
                btnProcess.ForeColor = Color.DarkRed;
            }
            else
            {
                btnProcess.Text = Resources.Process;
                btnProcess.ForeColor = Color.Green;
            }
        }

        private void treeListCategories_FocusedNodeChanged(object sender, FocusedNodeChangedEventArgs e)
        {
            if (e.Node != null)
            {
                var id = (int) e.Node.GetValue(IdCol);
                FillProducts(id);
            }
        }

        private void FillProducts(int catId)
        {
            List<Product> products = Content.ProductManager.GetAllProductsInCategory(catId).OrderBy(x => x.Name).ToList();
            if (products.Count != 0)
            {
                lstProduct.DataSource = products;
            }
        }

        private void btnSelectWarehouse_Click(object sender, EventArgs e)
        {
            var selectWarehouseForm = new SelectWarehousezForm();

            if (selectWarehouseForm.ShowDialog() == DialogResult.OK)
            {
                cmbWarehouse.SelectedValue = selectWarehouseForm.Warehouse.ID;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lstProduct_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            mainGridView.BeginDataUpdate();
            var product = (Product) lstProduct.SelectedItem;

            if (product == null)
                return;

            if (_documentItems != null && _documentItems.All(x => x.ProductID != product.ID))
            {
                _documentItems.Add(new InventoryDocumentItem
                    {
                        ID = -1,
                        Product = product,
                        ProductID = product.ID,
                        Amount = 0,
                        AmountFact = 0,
                        Delta = 0
                    });
            }

            mainGridControl.DataSource = _documentItems;
            mainGridView.EndDataUpdate();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveInventoryDocument();
        }

        private void SaveInventoryDocument()
        {
            if (backgroundWorkerProcess.IsBusy)
                return;

            backgroundWorkerProcess = new BackgroundWorker();

            marqueeProgressBarControl3.Enabled = true;

            var inventoryDocument = new InventoryDocument
            {
                ID = string.IsNullOrEmpty(txtNumber.Text) ? 0 : Convert.ToInt32(txtNumber.Text.Trim()),
                DocumentDate = dtnDocDate.Value,
                WarehouseID = ((Warehouse) cmbWarehouse.SelectedItem).ID,
                IsProcessed = DocumentIsProcessed,
                Note = richOtherInfo.Text
            };

            if (_isEditWindow)
            {
                inventoryDocument.User = _userName;
                backgroundWorkerProcess.DoWork +=
                    (o, args) =>
                    {
                        Content.InventoryManager.UpdateInventoryDocument(new InventoryData {
                            Document = inventoryDocument,
                            DocumentItems = _documentItems
                        });

                        if (DocumentIsProcessed)
                        {
                            UnProcessDocument();
                            ProcessDocument();
                        }
                    };
            }
            else
            {
                inventoryDocument.User = _userName = (Content.UserManager.IsLogged()) ? Content.UserManager.UserLogged.Name : string.Empty;
                backgroundWorkerProcess.DoWork += (o, args) =>
                {
                    int docId = Content.InventoryManager.AddInventoryDocument(inventoryDocument);
                    Content.InventoryManager.UpdateInventoryDocument(new InventoryData
                    {
                        Document = inventoryDocument,
                        DocumentItems = _documentItems
                    });

                    Invoke(
                        (MethodInvoker) delegate { txtNumber.Text = docId.ToString(CultureInfo.InvariantCulture); });

                    _isEditWindow = true;
                };
            }

            backgroundWorkerProcess.RunWorkerCompleted += (o, args) => { marqueeProgressBarControl3.Enabled = false; };

            backgroundWorkerProcess.RunWorkerAsync();
        }

        private void UnProcessDocument()
        {
            Content.RemainsOfGoodWiringsManager.UnProcessInventoryDocument(Convert.ToInt32(txtNumber.Text));

            DocumentIsProcessed = false;
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (backgroundWorkerProcess.IsBusy)
                return;

            backgroundWorkerProcess = new BackgroundWorker();

            marqueeProgressBarControl3.Enabled = true;

            var inventoryDocument = new InventoryDocument
                {
                    ID =
                        string.IsNullOrEmpty(txtNumber.Text)
                            ? 0
                            : Convert.ToInt32(txtNumber.Text.Trim()),
                    DocumentDate = dtnDocDate.Value,
                    WarehouseID = ((Warehouse) cmbWarehouse.SelectedItem).ID,
                    IsProcessed = DocumentIsProcessed,
                };

            backgroundWorkerProcess.DoWork += (o, args) =>
                {
                    if (_isEditWindow)
                    {
                        inventoryDocument.User = _userName;
                        Content.InventoryManager.UpdateInventoryDocument(new InventoryData
                        {
                            Document = inventoryDocument,
                            DocumentItems = _documentItems
                        });

                        if (DocumentIsProcessed)
                        {
                            UnProcessDocument();
                            ProcessDocument();
                        }
                    }
                    else
                    {
                        inventoryDocument.User = _userName = Content.UserManager.IsLogged() ? Content.UserManager.UserLogged.Name : string.Empty;
                        int docId = Content.InventoryManager.AddInventoryDocument(inventoryDocument);
                        Content.InventoryManager.UpdateInventoryDocument(new InventoryData
                        {
                            Document = inventoryDocument,
                            DocumentItems = _documentItems
                        });

                        Invoke(
                            (MethodInvoker) delegate { txtNumber.Text = docId.ToString(CultureInfo.InvariantCulture); });

                        _isEditWindow = true;
                    }

                    if (DocumentIsProcessed)
                    {
                        UnProcessDocument();
                    }
                    else
                    {
                        ProcessDocument();
                    }
                };

            backgroundWorkerProcess.RunWorkerCompleted += (o, args) =>
                {
                    marqueeProgressBarControl3.Enabled = false;

                    if (DocumentIsProcessed)
                    {
                        btnProcess.Text = Resources.Unprocess;
                        btnProcess.ForeColor = Color.DarkRed;
                    }
                    else
                    {
                        btnProcess.Text = Resources.Process;
                        btnProcess.ForeColor = Color.Green;
                    }

                    SaveInventoryDocument();
                };

            backgroundWorkerProcess.RunWorkerAsync();
        }

        private void mainGridView_DoubleClick(object sender, EventArgs e)
        {
            var view = (GridView)sender;
            var pt = view.GridControl.PointToClient(Control.MousePosition);
            var info = view.CalcHitInfo(pt);

            if (info.InDataRow)
            {
                mainGridView.BeginDataUpdate();
                var item = (InventoryDocumentItem)mainGridView.GetRow(mainGridView.FocusedRowHandle);

                if (item == null)
                    return;

                _documentItems.Remove(item);

                mainGridControl.DataSource = _documentItems;

                mainGridView.EndDataUpdate();
            }

            
        }

        private void ProcessDocument()
        {
            int inventroyDocumentId = -1;
            int warehouseId = 0;

            Invoke((MethodInvoker) delegate
                {
                    warehouseId = ((Warehouse) cmbWarehouse.SelectedItem).ID;
                    inventroyDocumentId = Convert.ToInt32(txtNumber.Text);
                });

            if (string.IsNullOrEmpty(txtNumber.Text))
            {
                MessageBox.Show(Resources.DocIsNotProcessed, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return;
            }

            Invoke((MethodInvoker)RefreshInitialAmount);


            List<RemainsOfGoodWirings> wiringses = _documentItems.Select(documentItem => new RemainsOfGoodWirings
                {
                    Amount = documentItem.AmountFact - documentItem.Amount,
                    Date = dtnDocDate.Value,
                    WarehouseID = warehouseId,
                    ProductID = documentItem.ProductID,
                    InventoryDocumentID = inventroyDocumentId
                }).ToList();


            Content.RemainsOfGoodWiringsManager.ProcessInventoryDocument(inventroyDocumentId, wiringses);

            DocumentIsProcessed = true;
        }

        private void FillInventoryBuutton_Click(object sender, EventArgs e)
        {
            if (
                MessageBox.Show(Resources.RefillDocument, Resources.Warning, MessageBoxButtons.YesNo,
                                MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                mainGridView.BeginDataUpdate();

                _documentItems.Clear();

                int warehouseId = (Warehouse) cmbWarehouse.SelectedItem == null
                                      ? -1
                                      : ((Warehouse) cmbWarehouse.SelectedItem).ID;
                var sw = new Stopwatch();
                sw.Start();
                _documentItems = Content.InventoryManager.FillInventoryItems(warehouseId, dtnDocDate.Value.AddSeconds(-1));
                sw.Stop();
                mainGridControl.DataSource = _documentItems;
                mainGridView.EndDataUpdate();
            }
        }

        private void mainGridView_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (e.Column.Name == "columnAmmount" || e.Column.Name == "columnAmountFact" ||
                e.Column.Name == "columnPrice")
            {
                var view = (GridView) sender;
                var currentRow = (InventoryDocumentItem) view.GetRow(e.RowHandle);

                currentRow.AmountFact = Math.Round(currentRow.AmountFact, 3);

                CalculateDocumentItemsFields(currentRow);
            }
        }

        private static void CalculateDocumentItemsFields(InventoryDocumentItem currentRow)
        {
            currentRow.Delta = Math.Round(currentRow.Amount - currentRow.AmountFact, 3);

            currentRow.AmmountFactFin = Math.Round(currentRow.AmountFact*(decimal) currentRow.Product.FixPrice, 2);

            currentRow.DeltaFin = Math.Round(currentRow.Delta*(decimal) currentRow.Product.FixPrice, 2);
        }

        private void backgroundWorkerReportGenerator_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show(Resources.DocumentIsProcessed);
        }

        private void InventoryForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (backgroundWorkerProcess.IsBusy)
            {
                MessageBox.Show(Resources.BackgroundOperationIsProcessing, Resources.WarningTitle,
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
        }

        private void backgroundWorkerProcess_DoWork(object sender, DoWorkEventArgs e)
        {
        }

        private void backgroundWorkerProcess_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
        }

        private void mainGridView_MouseWheel(object sender, MouseEventArgs e)
        {
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            if (
                MessageBox.Show(Resources.RefreshDocumentItems, Resources.Warning,
                                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                RefreshInitialAmount();
            }
        }

        private void RefreshInitialAmount()
        {
            mainGridView.BeginDataUpdate();

            int warehouseId = (Warehouse) cmbWarehouse.SelectedItem == null
                ? -1
                : ((Warehouse) cmbWarehouse.SelectedItem).ID;

            List<InventoryDocumentItem> items = Content.InventoryManager.FillInventoryItems(warehouseId, dtnDocDate.Value.AddSeconds(-1));

            foreach (InventoryDocumentItem documentItem in _documentItems)
            {
                InventoryDocumentItem founditem = items.FirstOrDefault(x => x.ProductID == documentItem.ProductID);

                if (founditem == null) continue;

                documentItem.Amount = Math.Round(founditem.Amount, 3);

                CalculateDocumentItemsFields(documentItem);
            }

            mainGridControl.DataSource = _documentItems;

            mainGridView.EndDataUpdate();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            printableComponentLink.CreateDocument();
            printableComponentLink.ShowPreview();
        }

        private void printableComponentLink_CreateReportHeaderArea(object sender, CreateAreaEventArgs e)
        {
            var brick = e.Graph.DrawString(string.Format("Склад: {0}", cmbWarehouse.Text), Color.Navy, new RectangleF(0, 0, 560, 30), BorderSide.None);

            brick.Font = new Font("Arial", 12);
            brick.HorzAlignment = HorzAlignment.Near;
            brick.StringFormat = new BrickStringFormat(StringAlignment.Near);
        }

        private void printableComponentLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            var brick = e.Graph.DrawString(string.Format("Инвентаризация товаров №{0} от {1:dd.MM.yyyy}.", txtNumber.Text,
                                                 dtnDocDate.Value), Color.Navy, new RectangleF(0, 0, 560, 60), BorderSide.None);

            brick.Font = new Font("Arial", 16);
            brick.HorzAlignment = HorzAlignment.Near;
            brick.StringFormat = new BrickStringFormat(StringAlignment.Near);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
           // MessageBox.Show(mainGridView.ActiveFilterCriteria);

            var text_del = "Удалить фильтр";
            var text_normal = "Показать только недосдачу";

            if (toolStripButton1.Text == text_normal)
            {
                mainGridView.ActiveFilterEnabled = true;
                mainGridView.ActiveFilterString = "[DeltaFin] > 0";
                toolStripButton1.Text = text_del;
            }
            else
            {
                mainGridView.ActiveFilterEnabled = false;
                toolStripButton1.Text = text_normal;
            }
            


        }
    }
}