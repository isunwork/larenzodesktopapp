﻿namespace LaRenzo.Forms.Documents.WriteOffDishes
{
    partial class WriteoffDishForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WriteoffDishForm));
            this.richWebOrder = new System.Windows.Forms.RichTextBox();
            this.mainSplit = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.lstCategory = new DevExpress.XtraEditors.ListBoxControl();
            this.lstDish = new System.Windows.Forms.ListBox();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.rightSplit = new DevExpress.XtraEditors.SplitContainerControl();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NameCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.AmountCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PriceCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CostCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnProcess = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cmbWarehouse = new System.Windows.Forms.ComboBox();
            this.btnSelectWarehouse = new System.Windows.Forms.Button();
            this.txtNumber = new System.Windows.Forms.TextBox();
            this.dtnDocDate = new System.Windows.Forms.DateTimePicker();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.lblNumber = new System.Windows.Forms.Label();
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.printableComponentLink = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplit)).BeginInit();
            this.mainSplit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lstCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rightSplit)).BeginInit();
            this.rightSplit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            this.bottomPanel.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printableComponentLink.ImageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // richWebOrder
            // 
            this.richWebOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richWebOrder.Location = new System.Drawing.Point(0, 0);
            this.richWebOrder.Name = "richWebOrder";
            this.richWebOrder.Size = new System.Drawing.Size(231, 692);
            this.richWebOrder.TabIndex = 0;
            this.richWebOrder.Tag = "Note";
            this.richWebOrder.Text = "";
            // 
            // mainSplit
            // 
            this.mainSplit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainSplit.Location = new System.Drawing.Point(0, 0);
            this.mainSplit.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.mainSplit.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.mainSplit.Name = "mainSplit";
            this.mainSplit.Panel1.Controls.Add(this.splitContainerControl1);
            this.mainSplit.Panel1.Text = "Panel1";
            this.mainSplit.Panel2.Controls.Add(this.splitContainerControl2);
            this.mainSplit.Panel2.Text = "Panel2";
            this.mainSplit.Size = new System.Drawing.Size(1217, 692);
            this.mainSplit.SplitterPosition = 364;
            this.mainSplit.TabIndex = 1;
            this.mainSplit.Text = "splitContainerControl1";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.lstCategory);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.lstDish);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(364, 692);
            this.splitContainerControl1.SplitterPosition = 171;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // lstCategory
            // 
            this.lstCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstCategory.Location = new System.Drawing.Point(0, 0);
            this.lstCategory.Name = "lstCategory";
            this.lstCategory.Size = new System.Drawing.Size(171, 692);
            this.lstCategory.TabIndex = 0;
            this.lstCategory.ToolTip = "Категории";
            this.lstCategory.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lstCategory_MouseClick);
            // 
            // lstDish
            // 
            this.lstDish.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstDish.FormattingEnabled = true;
            this.lstDish.Location = new System.Drawing.Point(0, 0);
            this.lstDish.Name = "lstDish";
            this.lstDish.Size = new System.Drawing.Size(188, 692);
            this.lstDish.TabIndex = 1;
            this.lstDish.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstDish_MouseDoubleClick_1);
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.rightSplit);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.richWebOrder);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(848, 692);
            this.splitContainerControl2.SplitterPosition = 612;
            this.splitContainerControl2.TabIndex = 2006;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // rightSplit
            // 
            this.rightSplit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rightSplit.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.rightSplit.Horizontal = false;
            this.rightSplit.Location = new System.Drawing.Point(0, 0);
            this.rightSplit.Name = "rightSplit";
            this.rightSplit.Panel1.Controls.Add(this.mainGridControl);
            this.rightSplit.Panel1.Text = "Panel1";
            this.rightSplit.Panel2.Controls.Add(this.bottomPanel);
            this.rightSplit.Panel2.Text = "Panel2";
            this.rightSplit.Size = new System.Drawing.Size(612, 692);
            this.rightSplit.SplitterPosition = 97;
            this.rightSplit.TabIndex = 0;
            this.rightSplit.Text = "splitContainerControl1";
            // 
            // mainGridControl
            // 
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl.Location = new System.Drawing.Point(0, 0);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(612, 590);
            this.mainGridControl.TabIndex = 2;
            this.mainGridControl.Tag = "1";
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.NameCol,
            this.AmountCol,
            this.PriceCol,
            this.CostCol});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsView.ShowFooter = true;
            this.mainGridView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.mainGridView_CellValueChanged);
            this.mainGridView.DoubleClick += new System.EventHandler(this.mainGridView_DoubleClick);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "№";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Width = 84;
            // 
            // NameCol
            // 
            this.NameCol.Caption = "Название";
            this.NameCol.FieldName = "Dish.Name";
            this.NameCol.Name = "NameCol";
            this.NameCol.OptionsColumn.AllowEdit = false;
            this.NameCol.OptionsColumn.AllowFocus = false;
            this.NameCol.OptionsColumn.ReadOnly = true;
            this.NameCol.Visible = true;
            this.NameCol.VisibleIndex = 0;
            this.NameCol.Width = 594;
            // 
            // AmountCol
            // 
            this.AmountCol.Caption = "Количество";
            this.AmountCol.FieldName = "Amount";
            this.AmountCol.Name = "AmountCol";
            this.AmountCol.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.AmountCol.Visible = true;
            this.AmountCol.VisibleIndex = 1;
            this.AmountCol.Width = 349;
            // 
            // PriceCol
            // 
            this.PriceCol.Caption = "Цена";
            this.PriceCol.FieldName = "Dish.Price";
            this.PriceCol.Name = "PriceCol";
            this.PriceCol.OptionsColumn.AllowEdit = false;
            this.PriceCol.OptionsColumn.AllowFocus = false;
            this.PriceCol.OptionsColumn.ReadOnly = true;
            this.PriceCol.Visible = true;
            this.PriceCol.VisibleIndex = 2;
            this.PriceCol.Width = 349;
            // 
            // CostCol
            // 
            this.CostCol.Caption = "Сумма";
            this.CostCol.DisplayFormat.FormatString = "c2 руб.";
            this.CostCol.FieldName = "Sum";
            this.CostCol.Name = "CostCol";
            this.CostCol.OptionsColumn.AllowEdit = false;
            this.CostCol.OptionsColumn.AllowFocus = false;
            this.CostCol.OptionsColumn.ReadOnly = true;
            this.CostCol.ShowUnboundExpressionMenu = true;
            this.CostCol.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.CostCol.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.CostCol.Visible = true;
            this.CostCol.VisibleIndex = 3;
            this.CostCol.Width = 358;
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.pnlBottom);
            this.bottomPanel.Controls.Add(this.cmbWarehouse);
            this.bottomPanel.Controls.Add(this.btnSelectWarehouse);
            this.bottomPanel.Controls.Add(this.txtNumber);
            this.bottomPanel.Controls.Add(this.dtnDocDate);
            this.bottomPanel.Controls.Add(this.lblDate);
            this.bottomPanel.Controls.Add(this.lblWarehouse);
            this.bottomPanel.Controls.Add(this.lblNumber);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bottomPanel.Location = new System.Drawing.Point(0, 0);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(612, 97);
            this.bottomPanel.TabIndex = 7;
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.btnPrint);
            this.pnlBottom.Controls.Add(this.btnProcess);
            this.pnlBottom.Controls.Add(this.btnSave);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 73);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(612, 24);
            this.pnlBottom.TabIndex = 55;
            // 
            // btnPrint
            // 
            this.btnPrint.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPrint.Location = new System.Drawing.Point(249, 0);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(87, 22);
            this.btnPrint.TabIndex = 5;
            this.btnPrint.Text = "Печать";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnProcess.ForeColor = System.Drawing.Color.Green;
            this.btnProcess.Location = new System.Drawing.Point(336, 0);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(100, 22);
            this.btnProcess.TabIndex = 6;
            this.btnProcess.Text = "Провести";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.Location = new System.Drawing.Point(436, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 22);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Записать";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.Location = new System.Drawing.Point(523, 0);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 22);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cmbWarehouse
            // 
            this.cmbWarehouse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWarehouse.FormattingEnabled = true;
            this.cmbWarehouse.Location = new System.Drawing.Point(88, 40);
            this.cmbWarehouse.Name = "cmbWarehouse";
            this.cmbWarehouse.Size = new System.Drawing.Size(238, 21);
            this.cmbWarehouse.TabIndex = 3;
            this.cmbWarehouse.Tag = "WarehouseID";
            // 
            // btnSelectWarehouse
            // 
            this.btnSelectWarehouse.Location = new System.Drawing.Point(332, 41);
            this.btnSelectWarehouse.Name = "btnSelectWarehouse";
            this.btnSelectWarehouse.Size = new System.Drawing.Size(24, 20);
            this.btnSelectWarehouse.TabIndex = 4;
            this.btnSelectWarehouse.Text = "...";
            this.btnSelectWarehouse.UseVisualStyleBackColor = true;
            this.btnSelectWarehouse.Click += new System.EventHandler(this.btnSelectWarehouse_Click);
            // 
            // txtNumber
            // 
            this.txtNumber.Enabled = false;
            this.txtNumber.Location = new System.Drawing.Point(88, 14);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(110, 20);
            this.txtNumber.TabIndex = 1;
            this.txtNumber.Tag = "id";
            // 
            // dtnDocDate
            // 
            this.dtnDocDate.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtnDocDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtnDocDate.Location = new System.Drawing.Point(231, 14);
            this.dtnDocDate.Name = "dtnDocDate";
            this.dtnDocDate.Size = new System.Drawing.Size(127, 20);
            this.dtnDocDate.TabIndex = 2;
            this.dtnDocDate.Tag = "DocumentDate";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(204, 17);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(21, 13);
            this.lblDate.TabIndex = 42;
            this.lblDate.Text = "от:";
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.Location = new System.Drawing.Point(41, 43);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(41, 13);
            this.lblWarehouse.TabIndex = 44;
            this.lblWarehouse.Text = "Склад:";
            // 
            // lblNumber
            // 
            this.lblNumber.AutoSize = true;
            this.lblNumber.Location = new System.Drawing.Point(38, 18);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(44, 13);
            this.lblNumber.TabIndex = 46;
            this.lblNumber.Text = "Номер:";
            // 
            // printingSystem
            // 
            this.printingSystem.Links.AddRange(new object[] {
            this.printableComponentLink});
            // 
            // printableComponentLink
            // 
            this.printableComponentLink.Component = this.mainGridControl;
            // 
            // 
            // 
            this.printableComponentLink.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("printableComponentLink1.ImageCollection.ImageStream")));
            this.printableComponentLink.Landscape = true;
            this.printableComponentLink.Margins = new System.Drawing.Printing.Margins(100, 100, 76, 100);
            this.printableComponentLink.MinMargins = new System.Drawing.Printing.Margins(20, 20, 50, 20);
            this.printableComponentLink.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.printableComponentLink.PrintingSystem = this.printingSystem;
            this.printableComponentLink.PrintingSystemBase = this.printingSystem;
            this.printableComponentLink.CreateMarginalHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.printableComponentLink_CreateMarginalHeaderArea);
            this.printableComponentLink.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.printableComponentLink_CreateReportHeaderArea);
            // 
            // WriteoffDishForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1216, 692);
            this.Controls.Add(this.mainSplit);
            this.Name = "WriteoffDishForm";
            this.Text = "Списание блюд";
            this.Load += new System.EventHandler(this.WriteoffDishForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplit)).EndInit();
            this.mainSplit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lstCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rightSplit)).EndInit();
            this.rightSplit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            this.bottomPanel.ResumeLayout(false);
            this.bottomPanel.PerformLayout();
            this.pnlBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printableComponentLink.ImageCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox richWebOrder;
        private DevExpress.XtraEditors.SplitContainerControl mainSplit;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.ListBoxControl lstCategory;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.SplitContainerControl rightSplit;
        private DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn NameCol;
        private DevExpress.XtraGrid.Columns.GridColumn AmountCol;
        private DevExpress.XtraGrid.Columns.GridColumn PriceCol;
        private DevExpress.XtraGrid.Columns.GridColumn CostCol;
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox cmbWarehouse;
        private System.Windows.Forms.Button btnSelectWarehouse;
        private System.Windows.Forms.TextBox txtNumber;
        private System.Windows.Forms.DateTimePicker dtnDocDate;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblWarehouse;
        private System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.ListBox lstDish;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;
        private DevExpress.XtraPrinting.PrintableComponentLink printableComponentLink;
    }
}