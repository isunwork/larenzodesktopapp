﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Entities.Documents.WriteoffDish;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Categories;
using LaRenzo.DataRepository.Repositories.Catalogs.Dishes;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Catalogs.Warehouses;
using LaRenzo.DataRepository.Repositories.Documents.WriteoffDish;
using LaRenzo.DataRepository.Repositories.Wiringses.RemainsOfGood;
using LaRenzo.Forms.Catalog.WarehouseForms;
using LaRenzo.FormsProvider;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Documents.WriteOffDishes
{
    public partial class WriteoffDishForm : Form
    {
        private bool _isEditWindow;

        private string _userName;

        private bool DocumentIsProcessed { set; get; }

        private readonly List<WriteoffDishDocumentItem> _documentItems;

        public WriteoffDishForm()
        {
            InitializeComponent();
            _documentItems = new List<WriteoffDishDocumentItem>();

            var warehouses = Content.WarehouseManager.GetWarehouses();
            cmbWarehouse.DataSource = warehouses;
            cmbWarehouse.DisplayMember = "Name";
            cmbWarehouse.ValueMember = "ID";

            lstDish.DisplayMember = "Name";

            var defaultWarehouse = Content.WarehouseManager.GetWarehouses().FirstOrDefault(x => x.IsBase);
	        if (defaultWarehouse != null)
	        {
		        cmbWarehouse.SelectedValue = defaultWarehouse.ID;
	        }
        }

        public WriteoffDishForm(bool isEdit, WriteoffDishDocument document):this()
        {
            _isEditWindow = isEdit;

            _userName = document.User;

            WindowsFormProvider.FillControlsFromModel(this, document);

            DocumentIsProcessed = document.IsProcessed;

            dtnDocDate.Value = document.DocumentDate;

	        if (!string.IsNullOrEmpty(txtNumber.Text))
	        {
		        _documentItems = Content.WriteoffDishManager.GetWriteoffDocumentItems(document.ID);
	        }

	        mainGridControl.DataSource = _documentItems;
        }


        private void btnProcess_Click(object sender, EventArgs e)
        {
            btnSave_Click(sender, e);
            if (DocumentIsProcessed)
            {
                UnProcessDocument();
            }
            else
            {
                ProcessDocument();
            }
        }

        private void WriteoffDishForm_Load(object sender, EventArgs e)
        {
            FillCategoryList(Content.CategoryRepository.GetItems().OrderBy(y => y.Weight));
            FillDishList(Content.DishRepository.GetItems(null));

            if (DocumentIsProcessed)
            {
                btnProcess.Text = Resources.Unprocess;
                btnProcess.ForeColor = Color.DarkRed;
            }
            else
            {
                btnProcess.Text = Resources.Process;
                btnProcess.ForeColor = Color.Green;
            }
        }


	    private int SelectedBrendId
	    {
		    get
		    {
			    return Content.BrandManager.SelectedBrand.ID;
		    }
	    }


        private void FillCategoryList(IEnumerable<Category> list)
        {
            lstCategory.Items.Add("Все");
            foreach (Category category in list)
            {
	            if (category.Brand_ID == SelectedBrendId)
	            {
		            lstCategory.Items.Add(category.Name);
	            }
            }
        }

        private void FillDishList(IEnumerable<Dish> list)
        {
            lstDish.DataSource = list;
        }

        private void lstCategory_MouseClick(object sender, MouseEventArgs e)
        {
            var category = (string)lstCategory.Items[lstCategory.SelectedIndex];

            IEnumerable<Dish> list = (category == "Все")
										 ? Content.DishRepository.GetItems(new DishFilter
										 {
											 BrandId = SelectedBrendId
										 })
                                         : Content.DishRepository.GetItems(new DishFilter
                                             {
                                                 FilterByCategoryName = true,
                                                 CategoryName = category,
												 BrandId = SelectedBrendId
                                             });

            FillDishList(list.ToList());
        }

        private void lstDish_MouseDoubleClick_1(object sender, MouseEventArgs e)
        {
            mainGridView.BeginDataUpdate();

            var dish = (Dish) lstDish.SelectedItem;

            if (dish == null)
                return;

            _documentItems.Add(new WriteoffDishDocumentItem
                {
                    ID = -1,
                    DishID = dish.ID,
                    Dish = dish,
                    Amount = 0,
                    Price = dish.Price,
                    Sum = 0
                });

            mainGridControl.DataSource = _documentItems;

            mainGridView.EndDataUpdate();
        }

        private void btnSelectWarehouse_Click(object sender, EventArgs e)
        {
            var selectWarehouseForm = new SelectWarehousezForm();

            if (selectWarehouseForm.ShowDialog() == DialogResult.OK)
            {
                cmbWarehouse.SelectedValue = selectWarehouseForm.Warehouse.ID;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var documentSum = _documentItems.Sum(x => x.Sum);

            var writeoffDoc = new WriteoffDishDocument
            {
                ID = string.IsNullOrEmpty(txtNumber.Text) ? 0 : Convert.ToInt32(txtNumber.Text.Trim()),
                DocumentDate = dtnDocDate.Value,
                DocumentSum = documentSum,
                IsProcessed = DocumentIsProcessed,
                WarehouseID = ((Warehouse)cmbWarehouse.SelectedItem).ID,
                Note = richWebOrder.Text
            };

            if (_isEditWindow)
            {
                writeoffDoc.User = _userName;

                Content.WriteoffDishManager.UpdateWriteoffDishDocument(new WriteoffDishData {
                    Document = writeoffDoc,
                    DocumentItems = _documentItems
                });

                if (DocumentIsProcessed)
                {
                    UnProcessDocument();
                    ProcessDocument();
                }
            }
            else
            {
                writeoffDoc.User = _userName = Content.UserManager.GetUserNameOrEmptyString();

                int docId = Content.WriteoffDishManager.AddWriteoffDishDocument(writeoffDoc);
                Content.WriteoffDishManager.UpdateWriteoffDishDocument(new WriteoffDishData
                {
                    Document = writeoffDoc,
                    DocumentItems = _documentItems
                });
                txtNumber.Text = docId.ToString(CultureInfo.InvariantCulture);
                _isEditWindow = true;
            }
        }

        private void mainGridView_DoubleClick(object sender, EventArgs e)
        {
            mainGridView.BeginDataUpdate();
            var item = (WriteoffDishDocumentItem)mainGridView.GetRow(mainGridView.FocusedRowHandle);

            if (item == null)
                return;

            _documentItems.Remove(item);

            mainGridControl.DataSource = _documentItems;

            mainGridView.EndDataUpdate();
        }

        private void mainGridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.Name == "AmountCol")
            {
                var view = (GridView)sender;
                var currentRow = (WriteoffDishDocumentItem)view.GetRow(e.RowHandle);

                currentRow.Price = Math.Round(currentRow.Price, 3);
                currentRow.Sum = Math.Round(currentRow.Price * Convert.ToDouble(currentRow.Amount));
            }
        }

        private void UnProcessDocument()
        {
            Content.RemainsOfGoodWiringsManager.UnProcessWriteoffDishDocument(Convert.ToInt32(txtNumber.Text));

            DocumentIsProcessed = false;

            btnProcess.Text = Resources.Process;
            btnProcess.ForeColor = Color.Green;
        }

        private void ProcessDocument()
        {
            if (string.IsNullOrEmpty(txtNumber.Text))
            {
                MessageBox.Show(Resources.WriteoffDocumentIsNotProcessed, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return;
            }

            Content.RemainsOfGoodWiringsManager.ProcessWriteoffDishDocument(Convert.ToInt32(txtNumber.Text), _documentItems);
            DocumentIsProcessed = true;

            btnProcess.Text = Resources.Unprocess;
            btnProcess.ForeColor = Color.DarkRed;
        }

        private void printableComponentLink_CreateReportHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            var brick =
                e.Graph.DrawString(
                    string.Format(
                        "Склад: {0}", cmbWarehouse.Text), Color.Navy,
                    new RectangleF(0, 0, 560, 30), BorderSide.None);

            brick.Font = new Font("Arial", 12);
            brick.HorzAlignment = HorzAlignment.Near;
            brick.StringFormat = new BrickStringFormat(StringAlignment.Near);
        }

        private void printableComponentLink_CreateMarginalHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            var brick =
              e.Graph.DrawString(
                  string.Format("Списание блюд №{0} от {1:dd.MM.yyyy}.",
                                txtNumber.Text, dtnDocDate.Value), Color.Navy,
                  new RectangleF(0, 0, 560, 60), BorderSide.None);

            brick.Font = new Font("Arial", 16);
            brick.HorzAlignment = HorzAlignment.Near;
            brick.StringFormat = new BrickStringFormat(StringAlignment.Near);
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            printableComponentLink.CreateDocument();
            printableComponentLink.ShowPreview();
        }
    }
}