﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Documents.Writeoff;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Documents.Writeoffs;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Documents.WriteOff
{
    public partial class WriteoffListForm : Form
    {
        public WriteoffListForm()
        {
            InitializeComponent();
            LoadWriteOffList(new object(), new EventArgs());
        }

        private void LoadWriteOffList(object sender, EventArgs args)
        {
            mainGridControl.DataSource = Content.WriteoffManager.GetWriteoffDocuments();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var writeoffForm = new WriteoffForm();
            writeoffForm.FormClosed += UpdateDataGrid;
            writeoffForm.Show();
        }

        private void UpdateDataGrid(object sender, EventArgs e)
        {
            var first = mainGridView.TopRowIndex;
            var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);
            LoadWriteOffList(sender, e);
            mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
            mainGridView.MakeRowVisible(first);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            UpdateDataGrid(sender, e);
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            var selectedRow = (WriteoffDocument) mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetWriteoffForm(false, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.WriteoffDocumentIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void GetWriteoffForm(bool isEdit, int writeoffId)
        {
            var writeoffDocument = Content.WriteoffManager.GetWriteoffDocument(writeoffId);

            var writeoffForm = new WriteoffForm(isEdit, writeoffDocument);

            writeoffForm.FormClosed += UpdateDataGrid;
            writeoffForm.Show();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var selectedRow = (WriteoffDocument) mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetWriteoffForm(true, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.WriteoffDocumentIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var selectedRow = (WriteoffDocument) mainGridView.GetFocusedRow();

            if (selectedRow == null)
            {
                MessageBox.Show(Resources.WriteoffDocumentIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                               MessageBoxIcon.Warning);
                return;
            }

            string msg = selectedRow.IsMarkToDelete
                             ? Resources.UnMarkDeleteDocument
                             : Resources.MarkDeleteDocument;

            if (MessageBox.Show(msg, Resources.WarningTitle, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Content.WriteoffManager.DeleteWriteoffDocument(selectedRow);
                UpdateDataGrid(sender, e);
            }
        }

        private void mainGridView_DoubleClick(object sender, EventArgs e)
        {
            var view = (GridView)sender;

            var pt = view.GridControl.PointToClient(MousePosition);

            var info = view.CalcHitInfo(pt);
            if (view.IsDataRow(info.RowHandle))
            {
                var supplyId = (int)view.GetRowCellValue(info.RowHandle, columnNumber);
                GetWriteoffForm(true, supplyId);
            }
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            var currentRow = (WriteoffDocument)view.GetRow(e.RowHandle);

            if (currentRow.IsMarkToDelete)
                e.Appearance.BackColor = Color.Salmon;

            if (currentRow.IsProcessed)
                e.Appearance.BackColor = Color.LightGreen;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }
    }
}