﻿using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;

namespace LaRenzo.Forms.Documents.DeliveryOrderForms
{
    public partial class DeliveryOrderFormPreOrder : DeliveryOrderForm
    {
        public PreOrder PreOrder { get; set; }

        public DeliveryOrderFormPreOrder(PreOrder preOrder, DetailsFormModes mode = DetailsFormModes.NewOrderFromPreOrder):base(mode, preOrder.OrderID)
        {
            InitializeComponent();
            PreOrder = preOrder;
        }
    }
}
