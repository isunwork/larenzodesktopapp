﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LaRenzo.DataHelpers;
using LaRenzo.DataRepository.Entities.Catalogs.Addresses;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Entities.Catalogs.Client;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Entities.Documents.WebOrders;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Call;
using LaRenzo.DataRepository.Repositories.Catalogs.Addresses;
using LaRenzo.DataRepository.Repositories.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Documents.Orders;
using LaRenzo.DataRepository.Repositories.Documents.Sessions;
using LaRenzo.DataRepository.Repositories.Others.DeliverySchedulies;
using LaRenzo.DataRepository.Repositories.Others.Times;
using LaRenzo.Properties;
using NLog;
using PaymentMethod = LaRenzo.TransferLibrary.PaymentMethod;

namespace LaRenzo.Forms.Documents.DeliveryOrderForms
{

    public partial class DeliveryOrderForm : Form
    {
        private List<Address> _addresses;
        private Brand _curentBrand;
        private Dictionary<int, int> _deliveryCategories = new Dictionary<int, int> {
#if !TESTDB
            { 1, 284 }, { 2, 285 }
#else
            { 1, 279 }, { 2, 280 }
#endif
        };
        private Dictionary<int, int> _deliveryDishes = new Dictionary<int, int> {
#if !TESTDB
            { 1, 4625 }, { 2, 4626 }
#else
            { 1, 4314 }, { 2, 4315 }
#endif
        };
        public Brand CurentBrand
        {
            get
            {
                if (_curentBrand == null && Order != null)
                {
                    return Order.Brand;
                }
                return _curentBrand;
            }
            set => _curentBrand = value;
        }

        private bool _lastCallStatus;

        protected WebOrder WebOrder { get; set; }

#region Privates
        /// <summary>
        /// To-date list of order items
        /// </summary>
        protected BindingList<OrderItem> OrderItems;

        private bool _isFormInitialized;

        /// <summary>
        /// Details Form Mode (New / EditPreOrder / View). See DetailsFormModes enum.
        /// </summary>
        private readonly DetailsFormModes _state;

        private Guid _orderGuid = Guid.NewGuid();
        private readonly PreOrder _preOrder;
        private bool _needDelivery = true;
        private DiscountCard _discountCard;

        private Coupon _coupon;

        public bool IsWebOrder;
        private bool _isOnlinePayment;
        protected Guid? WebOrderGuid;

        protected DeliveryOrderFormOptions FormOptions;

        protected PaymentMethod PaymentMethod
        {
            get { return _paymentMethod; }
            set
            {
                _isOnlinePayment = value != PaymentMethod.Cash && value != PaymentMethod.Undefined;
                _paymentMethod = value;
            }
        }

        private Order Order { get; set; }

        private OrderReserveTypes _reserveType = OrderReserveTypes.ForwardReserve;

        private PaymentMethod _paymentMethod;

        private bool IsReservedCorrectly
        {
            set
            {
                if (value)
                {
                    btnReserve.BackColor = DefaultBackColor;
                    btnReserve.Enabled = true;
                    btnDone.Enabled = true;
                }
                else
                {
                    btnReserve.BackColor = Color.Plum;
                    btnReserve.Enabled = true;
                    btnDone.Enabled = false;
                }
            }
        }

#endregion

#region Events

        /// <summary>
        /// </summary>
        /// <param name="state">Состояние заказа (добавлен/обновлен)</param>
        /// <param name="order">Заказ</param>
        public delegate void OrderProcessedEventHandler(string state, Order order);

        /// <summary>
        ///   Срабатывает после обработки заказа
        /// </summary>
        public event OrderProcessedEventHandler OrderProcessed;

        public static event OrderProcessedEventHandler OrderProcessedStatic;

        private static void OnOrderProcessedStatic(string state, Order order)
        {
            OrderProcessedStatic?.Invoke(state, order);
        }

        public void OnOrderProcessed(string state, Order order)
        {
            OrderProcessed?.Invoke(state, order);
        }

#endregion

#region Constructors

        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Новый заказ </summary>
        ////================================
        public DeliveryOrderForm() : this(DetailsFormModes.NewOrder, 0)
        {

            CallManager.Instance.IsOpenChoose = true;
            btnReserve.Enabled = true;
        }

        public async Task Initialize()
        {
            var points = Content.PointOfSaleRepository.GetItems().Where(x => x.Active).ToList();
            comboBoxPointOfSale.DataSource = points;
            comboBoxPointOfSale.DisplayMember = "PointName";
            comboBoxPointOfSale.ValueMember = "Id";
            comboBoxPointOfSale.SelectedValue = points.FirstOrDefault()?.ID;
            comboBoxPointOfSale.SelectedValueChanged += ComboBoxPointOfSale_SelectedValueChanged;

            dateTimePickerBirthDate.Format = DateTimePickerFormat.Custom;
            dateTimePickerBirthDate.CustomFormat = " ";
            dateTimePickerBirthDate.Text = "";
            dateTimePickerBirthDate.ValueChanged += dateTimePickerBirthDate_ValueChanged;
            Load += async (s, e) => await DetailsForm_Load();

            InitializeEvents();
            await InitDayRadioButton();
            SetPrintCheckbox();
            await SetFildsByCallInfo(_curentCallInfo);
        }

        protected void InitializeEvents()
        {
            buttonFillFromPhone.Click += async (s, e) => await buttonFillFromPhone_Click();
            mainGridView.CustomSummaryCalculate += (s, e) => mainGridView_CustomSummaryCalculate(e);
            textBoxCouponCode.TextChanged += (s, e) => textBoxCouponCode_TextChanged();
            buttonApplyCoupon.Click += async (s, e) => await buttonApplyCoupon_Click();
            txtPhone.TextChanged += async (s, e) => await maskedTextBox1_TextChanged();
            rbNearestTime.CheckedChanged += async (s, e) => await rbNearestTime_CheckedChanged(s);
            btnDone.Click += async (s, e) => await ButtonDoneClick();
            
            chkNeedDelivery.CheckedChanged += async (s, e) => await checkBoxNeedDelivery_CheckedChanged();
            lstDish.MouseDoubleClick += async (s, e) => await DishList_MouseDoubleClick();
            mainGridView.CellValueChanged += async (s, e) => await gridView1_CellValueChanged();
            mainGridView.DoubleClick += async (s, e) => await mainGridView_DoubleClick(e);
            fastDriverCheckBox.CheckedChanged += async (s, e) => await fastDriverCheckBox_CheckedChanged();
            numCardNumber.ValueChanged += async (s, e) => await numericUpDown1_ValueChanged();
            chkIsWindow.CheckedChanged += async (s, e) => await IsWindow_CheckedChanged();
            favDishesListBoxControl.DoubleClick += async (s, e) => await favDishesListBoxControl_DoubleClick();
        }

        private void ShowScheduleEmptyMessage()
        {
            MessageBox.Show("Добавьте расписание доставки в форме управления расписанием для данной улицы и текущего времени.",
                            "Не добавлено расписание доставки");
        }

        private void ComboBoxPointOfSale_SelectedValueChanged(object sender, EventArgs e)
        {
            var address = _addresses?.FirstOrDefault(x => x.Name == mruEditStreets.Text);
            if (address == null)
                return;

            var selectedPointOfSaleId = (int?)comboBoxPointOfSale.SelectedValue;
            var schedule = Content.DeliveryScheduleManager.GetSchedule(address, DateTime.Now);
            if (schedule == null)
            {
                ShowScheduleEmptyMessage();
                return;
            }

            if (schedule.PointOfSaleId != selectedPointOfSaleId)
            {
                if (MessageBox.Show(
                        "Выбранная точка продаж не оптимальная для улицы, которую вы выбрали, изменить точку?",
                        "Изменение точки продаж", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {

                }
                else
                {
                    comboBoxPointOfSale.SelectedValue = schedule.PointOfSaleId;
                }
            }
        }

        /// <summary> Текущее инфо по звонку </summary>
        private CallInfo _curentCallInfo;

        /// <summary>
        /// Новый заказ от звонка
        /// </summary>
        /// <param name="ci"></param>
        public DeliveryOrderForm(CallInfo ci) : this(DetailsFormModes.NewOrder, 0)
        {
            CallManager.Instance.IsOpenChoose = true;
            btnReserve.Enabled = true;

            _curentCallInfo = ci;

            _isNowCall = true;
        }


        internal async Task SetFildsByCallInfo(CallInfo ci, bool webOrders = false)
        {
            if (ci != null)
            {
                if (!webOrders)
                {
                    numCardNumber.Text = ci.CardNumber;

                    textEditFirstName.Text = ci.CallData.Name;

                    string pn = ci.CallData.PhoneNumber;

                    if (pn[0] == '8')
                    {
                        pn = pn.Substring(1);
                    }

                    txtPhone.Text = pn;

                    mruEditStreets.Text = ci.Street;
                    txtHouse.Text = ci.House;
                    txtAppartament.Text = ci.Appartament;
                    txtPorch.Text = ci.Porch;
                }

                favDishesListBoxControl.Visible = true;

                await FillFavList(ci.LastOrders);
            }
        }

        private async Task FillFavList(List<Order> lastOrdersList)
        {
            favDishesIdListBoxControl.Items.Clear();
            favDishesListBoxControl.Items.Clear();
            var favDishes = await Content.OrderManager.GetDisheByFavorite(lastOrdersList);
            List<DishFavorite> dishes = favDishes.Take(15).ToList();

            foreach (DishFavorite dishFavorite in dishes)
            {
                favDishesIdListBoxControl.Items.Add(dishFavorite.Dish.ID);
                string dishNameAndCount = dishFavorite.Dish.Name + "(" + dishFavorite.Count + ")";
                favDishesListBoxControl.Items.Add(dishNameAndCount);
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Заказ на основе предзаказа </summary>
        ////===============================================
        public DeliveryOrderForm(PreOrder preorder, Brand brand, bool doEdit = false)
        {
            CurentBrand = brand;
            CallManager.Instance.IsOpenChoose = true;

            InitializeComponent();
            btnReserve.Enabled = true;
            if (doEdit)
            {
                //Ставим галочку "Предзаказ"
                chkPreOrder.Checked = true;
                _state = DetailsFormModes.EditOrder;
            }
            else
            {
                _state = DetailsFormModes.NewOrderFromPreOrder;
            }

            Order = Content.OrderManager.GetById(preorder.Order.ID);
            _preOrder = preorder;

            if (Content.UserManager.UserLogged.RoleID == 1)
            {
                FormOptions = new DeliveryOrderFormOptions
                {
                    DoBlockDishArea = false,
                    DoBlockReservationArea = false,
                    DoBlockSaveButton = false
                };
            }
            if (Order?.CouponId != null)
                SetTextBoxCouponCode(Order.CouponId ?? 0);
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Указать текущий бренд </summary>
        ////==========================================
        private void SetBrandName()
        {
            // Указать бренд заказа
            Text += " (" + CurentBrand.Name + ")";
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        public DeliveryOrderForm(DetailsFormModes state, int idToEdit, DeliveryOrderFormOptions formOptions = null, CallInfo ci = null)
        {
            CallManager.Instance.IsOpenChoose = true;

            _curentCallInfo = ci;

            InitializeComponent();

            _state = state;
            OrderItems = new BindingList<OrderItem>();

            Order = Content.OrderManager.GetById(idToEdit);
            FormOptions = formOptions;

            if (state == DetailsFormModes.EditOrder)
            {
                if (Order == null) throw new OrderNotFoundException();
                chkPreOrder.Enabled = false;

                PersonCount = Order.PersonsCount;
            }

            if (state == DetailsFormModes.ViewProcessed)
            {
                FormOptions = new DeliveryOrderFormOptions
                {
                    DoBlockDishArea = true,
                    DoBlockReservationArea = true,
                    DoBlockSaveButton = true,
                    SaveButtonText = "Редактирование обработанных заказов запрещено"
                };
            }
            else if (Content.UserManager.UserLogged.RoleID == 1)
            {
                FormOptions = new DeliveryOrderFormOptions
                {
                    DoBlockDishArea = false,
                    DoBlockReservationArea = false,
                    DoBlockSaveButton = false
                };
            }
            if (Order?.CouponId != null)
                SetTextBoxCouponCode(Order.CouponId ?? 0);
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private async Task DetailsForm_Load()
        {
            await InitializeDefaultData();
            OrderItems.ListChanged += ListChangedHandler;

            await UpdateClientTimeState();

            datePreOrder.MinDate = DateTime.Today.AddDays(1);
            Activate();
            _isFormInitialized = true;

            if (Content.UserManager.UserLogged.RoleID == 1)
            {
                SetEnableAllEdit();
            }
            // Указать текущий бренд
            SetBrandName();
        }


        private int PersonCount
        {
            get
            {
                return personComboBox.SelectedIndex - 1;
            }
            set
            {
                personComboBox.SelectedIndex = value + 1;
            }

        }



        //_____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Включить разрешение на редактирование </summary>
        ////==========================================================
        internal void SetEnableAllEdit()
        {
            FormOptions = new DeliveryOrderFormOptions
            {
                DoBlockDishArea = false,
                DoBlockReservationArea = false,
                DoBlockSaveButton = false
            };

            // Адрес
            mruEditStreets.Enabled = true;
            txtHouse.Enabled = true;
            txtAppartament.Enabled = true;
            txtPorch.Enabled = true;
            txtAdditional.Enabled = true;


            // Блюда
            lstCategory.Enabled = true;
            lstDish.Enabled = true;

            // Заказы
            mainGridControl.Enabled = true;
            mainSplit.Panel1.Enabled = rightSplit.Panel1.Enabled = true;

            // Панель заказа
            panelRealTimeOrder.Enabled = true;
            orderTypeAndReservationPanel.Enabled = true;
            panelPreOrder.Enabled = true;

            // Кнопка
            if (btnDone.Text != "Создать заказ")
            {
                btnDone.Text = "Сохранить";
            }

            chkPreOrder.Enabled = true;

        }


        /// <summary>
        /// Отобразить название чисел в названиях радиокнопок
        /// </summary>
        /// <returns></returns>
        private async Task InitDayRadioButton()
        {
            var currentSession = await Content.SessionManager.GetOpened();

            // Если сессия открыта установить подписи контролов в соответствии с сессонной датой
            if (currentSession != null)
            {
                firstDayRadioButton.Text = currentSession.Opened.Day + " число";
                secondDayRadioButton.Text = currentSession.Opened.AddDays(1).Day + " число";

                // Установка "радио" по умолчанию в правильный текущий день
                if (Content.SessionManager.AddDayCount(currentSession) == 0)
                {
                    firstDayRadioButton.Checked = true;
                }
                else
                {
                    firstDayRadioButton.Enabled = false;
                    secondDayRadioButton.Checked = true;
                }
            }

            // Если заказ не пустой установить день в соответствии с времененм кухни
            if (Order?.TimeKitchen.Day == 2)
            {
                if (Content.SessionManager.AddDayCount(currentSession) > 0)
                {
                    firstDayRadioButton.Enabled = false;
                }

                firstDayRadioButton.Checked = false;
                secondDayRadioButton.Checked = true;
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private async Task UpdateClientTimeState()
        {
            var session = await Content.SessionManager.GetOpened();
            if (session != null)
            {
                tEditClient.Enabled = !rbNearestTime.Checked || session.CanEditTime;
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Срабатывает при изменении списка блюд в заказе </summary>
        ///====================================================================
        private void ListChangedHandler(object sender, ListChangedEventArgs e)
        {
            if (chkPreOrder.Checked)
            {
                IsReservedCorrectly = true;
                return;
            }
            if (!(_state == DetailsFormModes.EditOrder && Order.ReserveType == OrderReserveTypes.ForwardReserve))
            {
                IsReservedCorrectly = false;
            }
        }

#endregion


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Инициализирует данные для формы </summary>
        ////====================================================
        private async Task InitializeDefaultData()
        {
            try
            {

                if (_state == DetailsFormModes.NewOrder)
                {
                    tEditClient.Time = DateTime.Now;
                    tEditKitchen.Time = DateTime.Now;
                    OrderItems = OrderItems ?? new BindingList<OrderItem>();

                    btnDone.Text = Resources.Create;
                    rbNearestTime.Select();

                    IsReservedCorrectly = false;
                }

                if (_state == DetailsFormModes.NewOrderFromPreOrder)
                {
                    FillFormWithOrder();
                    tEditClient.Time = _preOrder.ReservationDateTime;
                    tEditKitchen.Time = _preOrder.ReservationDateTime.AddHours(-1);
                    rbSelectedTime.Select();

                    btnDone.Text = Resources.Create;
                    IsReservedCorrectly = false;

                }

                if (_state == DetailsFormModes.EditOrder || _state == DetailsFormModes.ViewByManager || _state == DetailsFormModes.ViewByOperator || _state == DetailsFormModes.ViewProcessed)
                {
                    FillFormWithOrder();

                    if (Order.ReserveType == OrderReserveTypes.ForwardReserve)
                    {
                        rbNearestTime.Select();
                    }
                    else
                    {
                        _reserveType = OrderReserveTypes.BackwardReserve;
                        rbSelectedTime.Select();
                    }

                    if (_state == DetailsFormModes.EditOrder)
                    {
                        btnDone.Text = Resources.Save;
                    }

                    if (_state == DetailsFormModes.ViewByManager)
                    {

                        btnDone.Text = Resources.EditIsNotAllowed;
                        rightSplit.Panel2.Enabled = false;

                    }
                    if (_state == DetailsFormModes.ViewByOperator || _state == DetailsFormModes.ViewProcessed)
                    {
                        btnDone.Enabled = false;
                        btnDone.Text = Resources.EditIsNotAllowedSayAdmin;
                    }
                    IsReservedCorrectly = true;

                    chkPreOrder.Enabled = false;
                }


                if (_preOrder != null)
                {
                    tEditPreOrder.Time = _preOrder.ReservationDateTime;
                    datePreOrder.Value = _preOrder.ReservationDateTime.Date;
                    tEditClient.Time = tEditPreOrder.Time;
                }


                mainGridControl.DataSource = OrderItems;
                _addresses = Content.AddressManager.GetAddressList();
                mruEditStreets.Properties.Items.AddRange(_addresses.Select(x => x.ToString()).ToList());

                await ManualUpdateOnGrid(CurentBrand.ID);


                UpdateFormOptions();
                FillPaymentMethodList();
                FillCategoryList();

                FillDishList(Content.DishRepository.GetItems(new DishFilter { BrandId = CurentBrand.ID }));

            }
            catch (Exception e)
            {
                var logger = LogManager.GetCurrentClassLogger();
                logger.Info($"Ошибка при инициализации формы заказа: {_state},{_preOrder?.ID},{Order?.ID},{OrderItems?.Count},{CurentBrand?.ID}");
                logger.Error(e);
                MessageBox.Show(
                    "В процессе открытия формы заказа возникла ошибка, попробуйте повторить операцию, если ошибка будет повторяться, сообщите об этом.", "Ошибка открытия формы заказа");
            }
        }

        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Устанавливает флаги чеков </summary>
        ////==============================================
        private void SetPrintCheckbox()
        {
            if (Order != null)
            {
                is5kRadioButton.Checked = Order.Is5k;
                isUnCashRadioButton.Checked = Order.IsUnCash;
                isPaidRadioButton.Checked = Order.IsClientPaid;
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Устанавливает опции формы в зависимости на основе переменной FormOptions типа DeliveryOrderFormOptions </summary>
        protected void UpdateFormOptions()
        {
            if (FormOptions == null) return;

            btnDone.Enabled = !FormOptions.DoBlockSaveButton;
            btnDone.Text = FormOptions.SaveButtonText;
            mainSplit.Panel1.Enabled = rightSplit.Panel1.Enabled = !FormOptions.DoBlockDishArea;
            orderTypeAndReservationPanel.Enabled = !FormOptions.DoBlockReservationArea;
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void FillPaymentMethodList()
        {
            paymentMethodList.ValueMember = "Method";
            paymentMethodList.DisplayMember = "Name";
            paymentMethodList.DropDownStyle = ComboBoxStyle.DropDownList;

            var bufferPm = PaymentMethod;

            if (Content.UserManager.UserLogged.Role.Name == "Admin")
            {
                paymentMethodList.DataSource = new List<object>
                {
                    new {Method = PaymentMethod.Cash, Name = "Наличные"},
                    new {Method = PaymentMethod.OnlineCard, Name = "Картой онлайн"},
                    new {Method = PaymentMethod.OfflineCard, Name = "Картой курьеру"}
                };
                PaymentMethod = bufferPm == PaymentMethod.Undefined ? PaymentMethod.Cash : bufferPm;
                paymentMethodList.SelectedValue = PaymentMethod;
                var userRights =  Content.UserAccessManager.GetForUser(Content.UserManager.UserLogged.ID);
                var userCanEditPaymentMethod = Content.UserAccessManager.GroupContainsRight(userRights, AccessRightAlias.DeliveryOrderPaymentMethod);
                paymentMethodList.Enabled = userCanEditPaymentMethod;
            }
            else
            {
                if (PaymentMethod != PaymentMethod.OnlineCard)
                {
                    paymentMethodList.DataSource = new List<object>
                    {
                        new {Method = PaymentMethod.Cash, Name = "Наличные"},
                        new {Method = PaymentMethod.OfflineCard, Name = "Картой курьеру"}
                    };

                    paymentMethodList.ValueMember = "Method";
                    paymentMethodList.DisplayMember = "Name";

                    PaymentMethod = bufferPm == PaymentMethod.Undefined ? PaymentMethod.Cash : bufferPm;
                    paymentMethodList.SelectedValue = PaymentMethod;

                    toolTipPaymentMethod.SetToolTip(panelPaymentMethod, "Выбирите тип оплаты");
                }
                else
                {
                    paymentMethodList.DataSource = new List<object>
                    {
                        new {Method = PaymentMethod.Cash, Name = "Наличные"},
                        new {Method = PaymentMethod.OnlineCard, Name = "Картой онлайн"},
                        new {Method = PaymentMethod.OfflineCard, Name = "Картой курьеру"}
                    };

                    PaymentMethod = PaymentMethod.OnlineCard;
                    paymentMethodList.SelectedValue = PaymentMethod;

                    var userRights = Content.UserAccessManager.GetForUser(Content.UserManager.UserLogged.ID);
                    var userCanEditPaymentMethod = Content.UserAccessManager.GroupContainsRight(userRights, AccessRightAlias.DeliveryOrderPaymentMethod);

                    paymentMethodList.Enabled = userCanEditPaymentMethod;
                }
            }

        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void FillFormWithOrder()
        {
            PaymentMethod = Order.PaymentMethod;
            _isOnlinePayment = Order.IsOnlinePayment;
            _orderGuid = Order.ReservationGuid ?? Guid.NewGuid();
            var orderItems = Content.OrderManager.GetItemList(Order.ID);
            var deliveryItem = orderItems.FirstOrDefault(x => x.DishID == _deliveryDishes[CurentBrand.ID]);
            orderItems.Remove(deliveryItem);
            OrderItems = new BindingList<OrderItem>(orderItems);
            numCardNumber.Text = (Order.DiscountCardID.HasValue)
                                         ? Order.DiscountCard.CardNumber.ToString(CultureInfo.InvariantCulture)
                                         : string.Empty;
            mruEditStreets.Text = Order.Street;
            txtHouse.Text = Order.House;
            txtAppartament.Text = Order.Appartament;
            txtPorch.Text = Order.Porch;
            txtAdditional.Text = Order.Additional;
            textBoxWebOrderId.Text = Order.WebOrderId.ToString();
            textEditFirstName.Text = Order.Name;
            if (Order.PointOfSaleId != 0)
            {
                comboBoxPointOfSale.SelectedValue = Order.PointOfSaleId;
            }
            txtPhone.Text = Order.Phone;
            tEditClient.Time = Order.TimeClient;
            tEditKitchen.Time = Order.TimeKitchen;
            chkNeedDelivery.Checked = Order.DoCalculateDelivery;
            chkIsWindow.Checked = Order.IsWindow;
            PersonCount = Order.PersonsCount;
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Fill form column with dishes of selected category </summary>
        ////======================================================================
        private void FillDishList(IEnumerable<Dish> list)
        {
            list = list.OrderBy(x => x.Name);

            lstDishListIDs.Items.Clear();
            lstDish.Items.Clear();

            foreach (var dish in list)
            {
                lstDishListIDs.Items.Add(dish.ID);
                lstDish.Items.Add(dish.Name);
            }
        }




        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Fill form column with all categories </summary>
        ////=========================================================
        private void FillCategoryList()
        {
            int deliveryCategoryID = _deliveryCategories[CurentBrand.ID];
            var list = Content.CategoryRepository.GetItems()
                .Where(x => x.ID != deliveryCategoryID)
                .OrderBy(y => y.Weight);

            lstCategory.Items.Add("Все");
            lstCategory.Items.Add("Доп. предложения");

            foreach (Category category in list)
            {
                if (category.Brand != null && category.Brand.ID == CurentBrand.ID)
                {
                    lstCategory.Items.Add(category.Name);
                }
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private async Task DishList_MouseDoubleClick()
        {
            mainGridControl.BeginUpdate();

            var id = (int)lstDishListIDs.Items[lstDish.SelectedIndex];

            if (OrderItems.Count(x => x.DishID == id) == 1)
            {
                OrderItems.Single(x => x.DishID == id).Amount += 1;
            }
            else
            {
                OrderItems.Add(new OrderItem
                {
                    Amount = 1,
                    DishID = id,
                    Dish = Content.DishRepository.GetItem(new DishFilter { FilterByDishId = true, DishId = id, BrandId = CurentBrand.ID })
                });
            }

            mainGridControl.EndUpdate();
            await ManualUpdateOnGrid(CurentBrand.ID);

            mainGridView.RefreshData();
        }

        /// <summary> Нужно ли спрашивать о закрытии </summary>
        private bool _isNeedAsk = true;

        /// <summary>
        /// Нажато "Создать заказ"
        /// </summary>
        /// <returns></returns>
        private async Task ButtonDoneClick()
        {
            if (personComboBox.SelectedIndex < 1)
            {
                MessageBox.Show("Необходимо выбрать кол-во персон");
            }
            else
            {
                _isNeedAsk = false;
                await ProcessOrder();
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private DateTime ParsePreOrderDateTime()
        {
            var date = datePreOrder.Value;
            var time = tEditPreOrder.Time;
            return new DateTime(date.Year, date.Month, date.Day, time.Hour, time.Minute, 0);
        }


        private void ClearAdditionalInfo()
        {
            string str = txtAdditional.Text;

            str = str.Replace("!!", "");
            str = str.Replace("%%", "");
            str = str.Replace("??", "");

            txtAdditional.Text = str;
        }


        //____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Создание нового заказа </summary>
        ////===========================================
        private async Task ProcessOrder()
        {
            try
            {
                ClearAdditionalInfo();

                // Total cost WITH delivery WITHOUT Discount
                int totalCostWithoutDiscount = OrderItems.Sum(x => x.Amount * x.Dish.Price);

                // Remove delivery from orderItems
                OrderItems = GetRealOrderItems();

                if (!OrderItems.Any())
                {
                    MessageBox.Show(Resources.OrderIsNotEmpty);
                    return;
                }

                decimal totalCostWithoutDelivery = OrderItems.Where(x => x.Dish.DiscountAppliсability).Sum(x => x.Amount * x.Dish.Price);
                var deliveryCost = totalCostWithoutDiscount - totalCostWithoutDelivery;
                var totalCost = totalCostWithoutDelivery;
                if (_discountCard != null)
                {
                    totalCost = totalCost * Content.OrderManager.GetDiscountRate(_discountCard.Discount);
                }
                else if (_coupon != null)
                {
                    totalCost = await Content.OrderManager.ApplyCoupon(totalCost, _coupon, Content.OrderManager.MapOrderItemsToDishes(OrderItems));
                    _coupon.ActivatedCount++;
                    if (_coupon.ActivationCount == _coupon.ActivatedCount)
                    {
                        _coupon.Activated = true;
                    }
                    await Content.CouponRepository.InsertOrUpdateAsync(_coupon);
                }
                totalCost += deliveryCost;
                bool needAddDelivery = deliveryCost > 0;
                // Получить поля с формы
                if (_state == DetailsFormModes.NewOrder)
                {
                    Order = new Order();
                }
                await FillOrderWithFormData(totalCost, totalCostWithoutDiscount);
                if (_state == DetailsFormModes.EditOrder)
                {
                    // Если ошибка - уйти
                    if (!await ValidateOrder()) return;
                    // Обновить заказ, поля заказа
                    var orderItems = GetRealOrderItems().ToList();
                    AddRealDeliveryItem(orderItems, needAddDelivery);
                    Content.OrderManager.Update(Order, orderItems);
                    // Если не предзаказ
                    if (!chkPreOrder.Checked)
                    {
                        // 
                        Content.TimeSheetManager.AddOrderToTimeSheet(_orderGuid);
                        OnOrderProcessed("update", Order);
                        OnOrderProcessedStatic("update", Order);
                    }
                    else
                    {
                        if (!chkIsWindow.Checked)
                        {
                            _preOrder.ReservationDateTime = ParsePreOrderDateTime();
                        }
                        Content.PreOrderManager.Update(_preOrder);
                    }
                }

                else if (_state == DetailsFormModes.NewOrderFromPreOrder)
                {
                    var session = await Content.SessionManager.GetOpened();
                    Order.SessionID = session.ID;

                    if (!await ValidateOrder())
                    {
                        return;
                    }

                    // Если не предзаказ и 
                    if (!chkPreOrder.Checked && !Content.TimeSheetManager.AddOrderToTimeSheet(_orderGuid) && !chkIsWindow.Checked)
                    {
                        MessageBox.Show(Resources.ReservedIsNotCompleated);
                        return;
                    }

                    var orderItems = GetRealOrderItems().ToList();
                    AddRealDeliveryItem(orderItems, needAddDelivery);
                    Content.OrderManager.Update(Order, orderItems);

                    OnOrderProcessed("create", Order);
                    OnOrderProcessedStatic("create", Order);
                }

                else if (_state == DetailsFormModes.NewOrder)
                {
                    var session = await Content.SessionManager.GetOpened();
                    Order.Created = DateTime.Now;
                    Order.StateID = 1;
                    Order.CreditStatus = CreditStatuses.FULL_RESTORED;
                    Order.SessionID = chkPreOrder.Checked ? (int?)null : session.ID;
                    Order.User = Content.UserManager.UserLogged.Name;
                    Order.WebOrderGuid = WebOrderGuid;
                    Order.IdBrand = CurentBrand.ID;
                    Order.Brand = CurentBrand;

                    if (!await ValidateOrder()) return;


                    // Если не предзаказ и ...
                    if (!chkPreOrder.Checked && !Content.TimeSheetManager.AddOrderToTimeSheet(_orderGuid) && !chkIsWindow.Checked)
                    {
                        MessageBox.Show(Resources.ReservedIsNotCompleated);
                        return;
                    }

                    Order.ReservationGuid = _orderGuid;
                    var orderItems = GetRealOrderItems().ToList();
                    AddRealDeliveryItem(orderItems, needAddDelivery);
                    Content.OrderManager.Add(Order, orderItems);

                    if (chkPreOrder.Checked)
                    {
                        Content.PreOrderManager.Create(Order.ID, ParsePreOrderDateTime());
                    }
                    else
                    {
                        OnOrderProcessed("create", Order);
                        OnOrderProcessedStatic("create", Order);
                    }
                }

                Close();
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    error += String.Format("{0}{1}", Environment.NewLine, ex.Message);
                }

                MessageBox.Show(string.Format("Произошла ошибка. {0}{1}", Environment.NewLine, error));
            }

        }

        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private async Task<bool> ValidateOrder()
        {
            var errors = Content.OrderManager.ValidateOrder(Order);
            if (errors != string.Empty)
            {
                MessageBox.Show(errors);
                await ManualUpdateOnGrid(CurentBrand.ID);
                return false;
            }
            return true;
        }


        //__________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Заполнить поля заказа из формы </summary>
        ////===================================================
        private async Task FillOrderWithFormData(decimal totalCost, int totalCostWithoutDiscount)
        {
            Order.Street = mruEditStreets.Text;
            Order.House = txtHouse.Text;
            Order.Appartament = txtAppartament.Text;
            Order.Porch = txtPorch.Text;
            Order.Additional = txtAdditional.Text;
            if (!String.IsNullOrEmpty(textBoxWebOrderId.Text))
            {
                Order.WebOrderId = Convert.ToInt32(textBoxWebOrderId.Text);
            }
            Order.PointOfSaleId = (int)comboBoxPointOfSale.SelectedValue;
            Order.Phone = txtPhone.Text;
            Order.Name = textEditFirstName.Text;
            Order.DoCalculateDelivery = _needDelivery;
            Order.IsWindow = chkIsWindow.Checked;
            Order.TimeClient = tEditClient.Time;
            Order.TimeKitchen = tEditKitchen.Time;
            Order.ReserveType = _reserveType;
            Order.DiscountCard = _discountCard;
            Order.DiscountCardID = _discountCard?.ID;
            Order.CouponId = _coupon?.ID;
            Order.PaymentMethod = PaymentMethod;
            Order.IsOnlinePayment = _isOnlinePayment;
            Order.IsWebRequest = IsWebOrder;
            Order.TotalCost = Convert.ToInt32(totalCost);
            Order.TotalCostWithoutDiscount = totalCostWithoutDiscount;
            Order.Is5k = is5kRadioButton.Checked;
            Order.IsUnCash = isUnCashRadioButton.Checked;
            Order.IsClientPaid = isPaidRadioButton.Checked;
            Order.PersonsCount = PersonCount;
            Order.IsFastDriver = fastDriverCheckBox.Checked;
            Order.IsNearTime = rbNearestTime.Checked;
            if (WebOrder != null && Order != null && !string.IsNullOrEmpty(WebOrder.Source))
            {
                Order.Source = WebOrder.Source;
            }

            await AddEditClientFromOrder(Order);
        }

        private async Task AddEditClientFromOrder(Order order)
        {
            if (!String.IsNullOrEmpty(order.Phone))
            {
                var clientId = await Content.ClientPhoneRepository.GetClientIdByPhone(order.Phone);
                Client client;
                ClientAddress address;
                if (clientId == 0)
                {
                    client = new Client();
                    address = new ClientAddress();
                }
                else
                {
                    client = await Content.ClientRepository.GetItemAsync(clientId);
                    address = (await Content.ClientAddressRepository.GetListAsync(clientId, order.Street, order.House)).FirstOrDefault();
                    if (address == null)
                    {
                        address = new ClientAddress();
                    }
                }

                client.FirstName = textEditFirstName.Text;
                client.LastName = textEditLastName.Text;
                client.MiddleName = textEditSecondName.Text;
                if (radioButtonFemale.Checked || radioButtonMale.Checked)
                {
                    client.Gender = radioButtonFemale.Checked ? GenderEnum.Female : GenderEnum.Male;
                }

                if (dateTimePickerBirthDate.Text != " ")
                {
                    client.BirthDate = null;
                }

                client.Email = textEditEmail.Text;
                await Content.ClientRepository.InsertOrUpdateAsync(client);

                if (clientId == 0)
                {
                    var phone = new ClientPhone();
                    phone.ClientId = client.ID;
                    phone.Phone = order.Phone;
                    await Content.ClientPhoneRepository.InsertOrUpdateAsync(phone);
                }

                address.ClientId = client.ID;
                address.House = order.House;
                address.Street = order.Street;
                address.Additional = order.Additional;
                address.Appartament = order.Appartament;
                address.Porch = order.Porch;
                await Content.ClientAddressRepository.InsertOrUpdateAsync(address);
                order.ClientId = client.ID;
            }
        }

        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private async Task gridView1_CellValueChanged()
        {
            await ManualUpdateOnGrid(CurentBrand.ID);
            mainGridView.RefreshData();
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void categoryList_MouseClick(object sender, MouseEventArgs e)
        {
            var category = (string)lstCategory.Items[lstCategory.SelectedIndex];

            IEnumerable<Dish> list;
            if (category == "Все")
                list = Content.DishRepository.GetItems(new DishFilter
                {
                    BrandId = CurentBrand.ID
                });
            else if (category == "Доп. предложения")
                list = Content.AdditionalOfferDishRepository
                    .GetItems(OrderItems.Select(x => x.DishID))
                    .Select(x => x.Dish);
            else
                list = Content.DishRepository.GetItems(new DishFilter
                {
                    CategoryName = category,
                    FilterByCategoryName = true,
                    BrandId = CurentBrand.ID
                });

            FillDishList(list.ToList());
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Manual add/remove/process _orderItems. For example, handles delivery </summary>
        protected async Task ManualUpdateOnGrid(int brandId)
        {
            await HandleDelivery(brandId);

            if (mainGridView.RowCount > 0)
            {
                mainGridView.RefreshData();
                mainGridView.UpdateTotalSummary();
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private async Task HandleDelivery(int brandId)
        {
            int deliveryPrice = await Content.OrderManager.GetDeliveryPrice(OrderItems, brandId, _discountCard, _coupon);

            if (_needDelivery && deliveryPrice > 0)
            {
                if (OrderItems != null && OrderItems.Count(x => x.ID == -1) == 0)
                {
                    OrderItems.Add(new OrderItem
                    {
                        ID = -1,
                        Amount = 1,
                        Dish = new Dish
                        {
                            Name = "Доставка",
                            Price = deliveryPrice,
                            DiscountAppliсability = false
                        },
                    });
                }
            }
            else
            {
                OrderItems.Remove(OrderItems.FirstOrDefault(x => x.ID == -1));
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void DetailsForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) Close();
            if (e.KeyCode == Keys.Enter)
            {
                if (mruEditStreets.Focused)
                {
                    mruEditStreets.Text = mruEditStreets.SelectedText;
                }
                NextControl();
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void NextControl()
        {
            SelectNextControl(ActiveControl, true, true, true, true);
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private async Task checkBoxNeedDelivery_CheckedChanged()
        {
            bool chkNeedDeliveryChecked = chkNeedDelivery.Checked;
            if (chkNeedDeliveryChecked)
            {
                btnReserve.Enabled = !string.IsNullOrWhiteSpace(mruEditStreets.Text);
            }

            chkIsWindow.Checked = !chkNeedDeliveryChecked;
            _needDelivery = chkNeedDeliveryChecked;
            await ManualUpdateOnGrid(CurentBrand.ID);
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Returns only orders that should be fully processed  For example - delivery doesn't included in this list </summary>
        ////=============================================================================================================================
        private BindingList<OrderItem> GetRealOrderItems()
        {
            var items = new BindingList<OrderItem>(OrderItems.ToList());
            var deliveryItem = items.FirstOrDefault(x => x.ID == -1);
            if (deliveryItem == null)
                return items;
            // Удаляем позицию Доставка
            items.Remove(deliveryItem); 
            return items;
        }

        /// <summary>
        /// Добавляет реальную позицию Доставка
        /// </summary>
        /// <param name="needAddDelivery"></param>
        /// <returns></returns>
        private void AddRealDeliveryItem(List<OrderItem> orderItems, bool needAddDelivery)
        {
            if (!needAddDelivery)
                return;

            var realDeliveryDish = Content.DishRepository.GetItem(new DishFilter { FilterByDishId = true, DishId = _deliveryDishes[CurentBrand.ID], BrandId = CurentBrand.ID });
            if (realDeliveryDish == null)
            {
                MessageBox.Show("На найдено блюдо Доставка для данного бренда.");
                return;
            }
            orderItems.Add(new OrderItem
            {
                Amount = 1,
                DishID = _deliveryDishes[CurentBrand.ID],
                Dish = realDeliveryDish
            });
        }



        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void DetailsForm_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (_isNeedAsk && MessageBox.Show("Вы точно хотите закрыть окно?", "Внимание", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                e.Cancel = true;
            }
            else
            {
                CallManager.Instance.IsOpenChoose = true;
                Content.TimeSheetManager.ClearReservation(_orderGuid, false);
            }
        }


        //_____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Нажато "Зарезервировать" </summary>
        ////=============================================
        private async void BtnReserveClick(object sender, EventArgs e)
        {
            var items = GetRealOrderItems().ToList();

            // Проверка на наличие заказов
            if (!items.Any())
            {
                MessageBox.Show(Resources.OrderIsNotEmpty);
                return;
            }

            // Получить текущую сессию
            var session = await Content.SessionManager.GetOpened();

            DateTime timeWithDelivery;

            //Резерв окна на ближайшее время
            if (chkIsWindow.Checked && !rbSelectedTime.Checked)
            {
                tEditKitchen.Time = DateTime.Now;
                timeWithDelivery = tEditKitchen.Time;

                if (session != null)
                {
                    timeWithDelivery = timeWithDelivery.Add(session.DeltaWindow);
                }
                tEditClient.Time = timeWithDelivery;

                IsReservedCorrectly = true;
                if (Order != null && Order.ReservationGuid != null)
                {
                    Content.TimeSheetManager.ClearReservation(Order.ReservationGuid);
                }

                return;
            }

            //Резерв окна на заданное время (расчётное время)
            if (chkIsWindow.Checked && rbSelectedTime.Checked)
            {
                var clientTime = tEditClient.Time;

                // Если сессия открыта
                if (session != null)
                {
                    // Пересчитать время кухни относительно клиентского времени
                    tEditKitchen.Time = clientTime.Add(-session.DeltaWindow);
                }

                if (tEditKitchen.Time < DateTime.Now)
                {
                    rbNearestTime.Checked = true;
                }

                IsReservedCorrectly = true;
                if (Order != null && Order.ReservationGuid != null)
                {
                    Content.TimeSheetManager.ClearReservation(Order.ReservationGuid);
                }

                return;
            }

            // быстрая доставка
            if (fastDriverCheckBox.Checked && rbSelectedTime.Checked)
            {
                var clientTime = tEditClient.Time;

                // Если сессия открыта
                if (session != null)
                {
                    // Пересчитать время кухни относительно клиентского времени
                    tEditKitchen.Time = clientTime.Add(-session.DeltaWindow);
                }

                if (tEditKitchen.Time < DateTime.Now)
                {
                    rbNearestTime.Checked = true;
                }

                IsReservedCorrectly = true;
                if (Order != null && Order.ReservationGuid != null)
                {
                    Content.TimeSheetManager.ClearReservation(Order.ReservationGuid);
                }

                return;
            }

            // Ближайшее время
            if (_reserveType == OrderReserveTypes.ForwardReserve)
            {
                tEditKitchen.Time = await ReserveTimeManager.ReserveTimeForward(Content.TimeSheetManager.ConvertOrderItemsToTimeCooking(Content.TimeSheetManager.SeparateToSections(items), CurentBrand.ID), _orderGuid, CurentBrand.ID, (int)comboBoxPointOfSale.SelectedValue);
            }

            // Расчётное время
            if (_reserveType == OrderReserveTypes.BackwardReserve)
            {
                bool isSecondDay = secondDayRadioButton.Checked;
                var dict = Content.TimeSheetManager.ConvertOrderItemsToTimeCooking(Content.TimeSheetManager.SeparateToSections(items), CurentBrand.ID);

                tEditKitchen.Time = ReserveTimeManager.ReserveTimeBackward(dict, _orderGuid, tEditKitchen.Time, isSecondDay ? 1 : 0, CurentBrand.ID, (int)comboBoxPointOfSale.SelectedValue);
            }

            timeWithDelivery = tEditKitchen.Time;

            var address = _addresses.FirstOrDefault(x => x.Name == mruEditStreets.Text);
            var pointOfSaleId = (int)comboBoxPointOfSale.SelectedValue;
            TimeSpan deliveryTime = Content.DeliveryScheduleManager.GetDeliveryTime(address, tEditKitchen.Time, pointOfSaleId);
            if (session != null)
            {
                timeWithDelivery = timeWithDelivery.Add(session.Delta);
            }

            tEditClient.Time = timeWithDelivery.Add(deliveryTime);

            IsReservedCorrectly = true;

            // Установить правильный день в "радио"
            if (session != null && tEditKitchen.Time.Day == session.Opened.Day)
            {
                firstDayRadioButton.Checked = true;
                secondDayRadioButton.Checked = false;
            }
            else
            {
                firstDayRadioButton.Checked = false;
                secondDayRadioButton.Checked = true;
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private async Task rbNearestTime_CheckedChanged(object sender)
        {
            var rb = sender as RadioButton;
            if (rb == null) return;

            if (rb.Checked)
            {
                _reserveType = OrderReserveTypes.ForwardReserve;
                tEditKitchen.Enabled = false;
            }
            else tEditKitchen.Enabled = true;

            IsReservedCorrectly = false;

            // Нельзя выбрать дату в "радио"
            firstDayRadioButton.Enabled = false;
            secondDayRadioButton.Enabled = false;

            await UpdateClientTimeState();
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private async void rbSelectedTime_CheckedChanged(object sender, EventArgs e)
        {
            // Можно выбрать дату в "радио"
            var session = await Content.SessionManager.GetOpened();
            if (Content.SessionManager.AddDayCount(session) == 0)
            {
                firstDayRadioButton.Enabled = true;
            }

            secondDayRadioButton.Enabled = true;


            _reserveType = OrderReserveTypes.BackwardReserve;
            IsReservedCorrectly = false;

            await UpdateClientTimeState();
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void timeKitchen_EditValueChanged(object sender, EventArgs e)
        {
            IsReservedCorrectly = false;
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void textName_EditValueChanged(object sender, EventArgs e)
        {
            if (textEditFirstName.Text.Length > 0)
                textEditFirstName.Text = char.ToUpper(textEditFirstName.Text[0]) + textEditFirstName.Text.Substring(1);
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private async Task maskedTextBox1_TextChanged()
        {
            txtPhone.Text = txtPhone.Text.Trim();

            if (txtPhone.Text == Resources.Number_Eight || txtPhone.Text == Resources.Number_Seven)
                txtPhone.Text = string.Empty;

            if (txtPhone.Text.StartsWith("2"))
            {
                txtPhone.Mask = Resources.NullMask;
                var startIndex = txtPhone.Text.Length;

                if (startIndex >= 5) startIndex += 2;
                else if (startIndex >= 3) startIndex++;

                if (_isFormInitialized) BeginInvoke(new Action(() => txtPhone.Select(startIndex, 0)));
                else txtPhone.Select(startIndex, 0);
            }
            else
            {
                txtPhone.Mask = Resources.NullMaskWithCode;
                if (txtPhone.Text.Length == 1)
                {
                    if (_isFormInitialized)
                    {
                        BeginInvoke(new Action(() =>
                        {
                            txtPhone.Select(2, 0);
                        }));
                    }
                }
            }

            if (txtPhone.MaskCompleted)
            {
                await FillFormFromPhone();
            }
        }

        private async Task FillFormFromPhone(bool clearData = false)
        {
            var phone = txtPhone.Text;
            var orders = await Content.OrderManager.GetOrdersByPhone(phone);
            await FillFavList(orders);
            var clientId = await Content.ClientPhoneRepository.GetClientIdByPhone(phone);
            if (clientId > 0)
            {
                if (clearData)
                {
                    ClearCustomerData(false);
                }
                var client = await Content.ClientRepository.GetItemAsync(clientId);
                if (String.IsNullOrEmpty(textEditFirstName.Text))
                    textEditFirstName.Text = client.FirstName;
                if (String.IsNullOrEmpty(textEditSecondName.Text))
                    textEditSecondName.Text = client.MiddleName;
                if (String.IsNullOrEmpty(textEditLastName.Text))
                    textEditLastName.Text = client.LastName;
                if (client.Gender == GenderEnum.Female)
                {
                    radioButtonFemale.Checked = true;
                    radioButtonMale.Checked = false;
                }
                else if (client.Gender == GenderEnum.Male)
                {
                    radioButtonFemale.Checked = false;
                    radioButtonMale.Checked = true;
                }

                if (client.BirthDate != null && client.BirthDate > dateTimePickerBirthDate.MinDate &&
                    client.BirthDate < dateTimePickerBirthDate.MaxDate)
                {
                    dateTimePickerBirthDate.Value = client.BirthDate.Value;
                }

                textEditEmail.Text = client.Email;
                var address =
                    (await Content.ClientAddressRepository.GetListAsync(clientId, null, null, true))
                    .OrderBy(x => x.DateModified).LastOrDefault();
                if (address != null)
                {
                    if (String.IsNullOrEmpty(txtHouse.Text))
                        txtHouse.Text = address.House;
                    if (String.IsNullOrEmpty(mruEditStreets.Text))
                        mruEditStreets.Text = address.Street;
                    if (String.IsNullOrEmpty(txtAdditional.Text))
                        txtAdditional.Text = address.Additional;
                    if (String.IsNullOrEmpty(txtAppartament.Text))
                        txtAppartament.Text = address.Appartament;
                    if (String.IsNullOrEmpty(txtPorch.Text))
                        txtPorch.Text = address.Porch;
                }
            }
        }

        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void textPhone_Enter(object sender, EventArgs e)
        {
            BeginInvoke((MethodInvoker)(() => txtPhone.SelectAll()));
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void checkBoxPreOrder_CheckedChanged(object sender, EventArgs e)
        {
            panelPreOrder.Visible = chkPreOrder.Checked;
            panelRealTimeOrder.Visible = !chkPreOrder.Checked;
            IsReservedCorrectly = chkPreOrder.Checked;
            Content.TimeSheetManager.ClearReservation(_orderGuid);
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void gridViewOrderItems_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "")
                e.DisplayText = (e.ListSourceRowIndex + 1).ToString(CultureInfo.InvariantCulture);
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void labelControl12_Click(object sender, EventArgs e)
        {

        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        protected async Task ApplyDiscount()
        {
            _discountCard = Content.DiscountCardManager.GetByNumber((int)numCardNumber.Value);
            if (_discountCard != null)
            {
                lblCardStatus.Text = Resources.CardNumberFound;
                lblCardStatus.BackColor = Color.LightGreen;
                if (_state == DetailsFormModes.NewOrder || _state == DetailsFormModes.ViewWebOrder)
                {
                    FillFromDiscountCard(_discountCard);
                }
                discountLabel.Text = string.Format("Скидка {0}%", _discountCard.Discount);
            }
            else
            {
                lblCardStatus.Text = Resources.CardNumberNotFound;
                lblCardStatus.BackColor = Color.LightCoral;
                if (_state == DetailsFormModes.NewOrder || _state == DetailsFormModes.ViewWebOrder)
                {
                    ClearCustomerData();
                }
                discountLabel.Text = "Скидка 0%";
            }
        }

        private async Task numericUpDown1_ValueChanged()
        {
            await ApplyDiscount();
            await ManualUpdateOnGrid(CurentBrand.ID);
            mainGridView.RefreshData();
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void FillFromDiscountCard(DiscountCard card)
        {
            textEditFirstName.Text = card.Name;
            mruEditStreets.Text = card.Street;
            txtHouse.Text = card.House;
            txtAppartament.Text = card.Appaertment.HasValue
                                       ? card.Appaertment.Value.ToString(CultureInfo.InvariantCulture)
                                       : string.Empty;
            txtPorch.Text = card.Porch.HasValue ? card.Porch.Value.ToString(CultureInfo.InvariantCulture) : string.Empty;

            txtPhone.Text = card.Phone;
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void ClearCustomerData(bool clearPhone = true)
        {
            textEditFirstName.Text = string.Empty;
            textEditEmail.Text = string.Empty;
            textEditLastName.Text = string.Empty;
            textEditSecondName.Text = string.Empty;
            mruEditStreets.Text = string.Empty;
            txtHouse.Text = string.Empty;
            txtAppartament.Text = string.Empty;
            txtPorch.Text = string.Empty;
            if (clearPhone)
            {
                txtPhone.Text = string.Empty;
            }
            txtAdditional.Text = string.Empty;
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private async void reCalculate_Click(object sender, EventArgs e)
        {
            var session = await Content.SessionManager.GetOpened();
            var address = _addresses.FirstOrDefault(x => x.Name == mruEditStreets.Text);
            var pointOfSaleId = (int)comboBoxPointOfSale.SelectedValue;
            TimeSpan deliveryTime = Content.DeliveryScheduleManager.GetDeliveryTime(address, tEditKitchen.Time, pointOfSaleId);
            DateTime timeWithDelivery = tEditKitchen.Time;
            if (session != null)
                timeWithDelivery = timeWithDelivery.Add(session.Delta);

            tEditClient.Time = timeWithDelivery.Add(deliveryTime);
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Чек - окно </summary>
        ////===============================
        private async Task IsWindow_CheckedChanged()
        {
            bool chkIsWindowChecked = chkIsWindow.Checked;
            if (chkIsWindowChecked)
            {
                btnReserve.Enabled = true;
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(mruEditStreets.Text))
                {
                    btnReserve.Enabled = !chkIsWindowChecked;
                }
            }

            chkNeedDelivery.Checked = !chkIsWindowChecked;
            mruEditStreets.Enabled = !chkIsWindowChecked;
            txtHouse.Enabled = !chkIsWindowChecked;
            txtAppartament.Enabled = !chkIsWindowChecked;
            txtPorch.Enabled = !chkIsWindowChecked;

            _needDelivery = chkNeedDelivery.Checked;
            await ManualUpdateOnGrid(CurentBrand.ID);

            if (chkIsWindowChecked && !rbSelectedTime.Checked)
            {
                IsReservedCorrectly = true;
                if (Order != null && Order.ReservationGuid != null)
                {
                    Content.TimeSheetManager.ClearReservation(Order.ReservationGuid);
                }
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Чек - быстрая доставка </summary>
        ////===========================================
        private async Task fastDriverCheckBox_CheckedChanged()
        {
            if (fastDriverCheckBox.Checked)
            {
                btnReserve.Enabled = true;
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(mruEditStreets.Text))
                {
                    btnReserve.Enabled = !fastDriverCheckBox.Checked;
                }
            }
            await ManualUpdateOnGrid(CurentBrand.ID);

            if (fastDriverCheckBox.Checked && !rbSelectedTime.Checked)
            {
                IsReservedCorrectly = true;
                if (Order != null && Order.ReservationGuid != null)
                {
                    Content.TimeSheetManager.ClearReservation(Order.ReservationGuid);
                }
            }
        }

        /// <summary>
        /// Рассчёт финальной суммы
        /// </summary>
        /// <param name="e"></param>
        private void mainGridView_CustomSummaryCalculate(CustomSummaryEventArgs e)
        {
            if (e.SummaryProcess == CustomSummaryProcess.Start)
            {
                e.TotalValue = Convert.ToDecimal(0);
            }

            if (e.SummaryProcess == CustomSummaryProcess.Calculate)
            {
                if (_discountCard != null || _coupon != null)
                {
                    var dish = ((OrderItem)e.Row).Dish;
                    if (dish.DiscountAppliсability)
                    {
                        e.TotalValue = Convert.ToDecimal(e.TotalValue) + Convert.ToDecimal(e.FieldValue);
                    }
                }
                else
                {
                    e.TotalValue = Convert.ToDecimal(e.TotalValue) + Convert.ToDecimal(e.FieldValue);
                }
            }

            if (e.SummaryProcess == CustomSummaryProcess.Finalize)
            {
                if (_discountCard != null)
                {
                    if (e.TotalValue != null)
                    {
                        var totalValue = Convert.ToDecimal(e.TotalValue);
                        var discountRate = Content.OrderManager.GetDiscountRate(_discountCard.Discount);
                        var sum = (totalValue * discountRate);
                        var discount = totalValue - sum;
                        var delivery = OrderItems.Where(x => x.Dish.DiscountAppliсability == false).Sum(x => x.Dish.Price);
                        if (delivery > 0)
                        {
                            sum = sum + delivery;
                        }
                        e.TotalValue = $"{sum:F2} р. (Скидка {discount:F2} р.)";
                    }
                }
                else if (_coupon != null)
                {
                    if (e.TotalValue != null)
                    {
                        var totalValue = Convert.ToDecimal(e.TotalValue);
                        var sum = Content.OrderManager.ApplyCouponSync(totalValue, _coupon, Content.OrderManager.MapOrderItemsToDishes(OrderItems));
                        var discount = totalValue - sum;
                        var delivery = OrderItems.Where(x => x.Dish.DiscountAppliсability == false).Sum(x => x.Dish.Price);
                        if (delivery > 0)
                        {
                            sum = sum + delivery;
                        }
                        e.TotalValue = $"{sum:F2} р. (Скидка по купону {discount:F2} р.)";
                    }
                }
                else
                {
                    e.TotalValue = $"{e.TotalValue:F2} р.";
                }
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void numCardNumber_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(numCardNumber.Text)) numCardNumber.Value = 0;
        }

        //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void paymentMethodList_SelectedIndexChanged(object sender, EventArgs e)
        {
            PaymentMethod = (PaymentMethod)paymentMethodList.SelectedValue;
            if (PaymentMethod == PaymentMethod.OfflineCard || PaymentMethod == PaymentMethod.OnlineCard)
            {
                if (!isPaidRadioButton.Checked)
                {
                    isUnCashRadioButton.Checked = true;
                }
            }
            else if (isUnCashRadioButton.Checked)
            {
                isUnCashRadioButton.Checked = false;
            }
        }

        private void mruEditStreets_Properties_SelectedValueChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(mruEditStreets.Text))
            {
                btnReserve.Enabled = false;
                return;
            }
            var address = _addresses.FirstOrDefault(x => x.Name == mruEditStreets.Text);
            if (address == null)
            {
                btnReserve.Enabled = false;
                return;
            }
            var schedule = Content.DeliveryScheduleManager.GetSchedule(address, DateTime.Now);
            if (schedule == null)
            {
                btnReserve.Enabled = false;
                ShowScheduleEmptyMessage();
                return;
            }

            comboBoxPointOfSale.SelectedValue = schedule.PointOfSaleId; 
            if (chkNeedDelivery.Checked && !string.IsNullOrWhiteSpace(mruEditStreets.Text))
            {
                btnReserve.Enabled = true;
            }

            if (chkNeedDelivery.Checked && string.IsNullOrWhiteSpace(mruEditStreets.Text))
            {
                btnReserve.Enabled = false;
            }
        }


        private bool _isNowCall;

        //__________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Клик по кнопки позвонить </summary>
        ////=============================================
        private async void callButton_Click(object sender, EventArgs e)
        {
            if (!CallManager.Instance.IsNowSpeak)
            {
                _curentCallInfo = await CallManager.Instance.Call(txtPhone.Text);
            }
            else
            {
                if (_curentCallInfo != null)
                {
                    CallManager.Instance.DropCall(_curentCallInfo.CallData);
                }
            }

            _isNowCall = !_isNowCall;
        }


        private async Task favDishesListBoxControl_DoubleClick()
        {
            mainGridControl.BeginUpdate();

            var id = (int)favDishesIdListBoxControl.Items[favDishesListBoxControl.SelectedIndex];

            if (OrderItems.Count(x => x.DishID == id) == 1)
            {
                OrderItems.Single(x => x.DishID == id).Amount += 1;
            }
            else
            {
                OrderItems.Add(new OrderItem
                {
                    Amount = 1,
                    DishID = id,
                    Dish = Content.DishRepository.GetItem(new DishFilter { FilterByDishId = true, DishId = id, BrandId = CurentBrand.ID })
                });
            }

            mainGridControl.EndUpdate();
            await ManualUpdateOnGrid(CurentBrand.ID);

            mainGridView.RefreshData();
        }



        private void testCallTimer_Tick(object sender, EventArgs e)
        {
            bool isNowCall = CallManager.Instance.IsNowSpeak;

            if (isNowCall != _lastCallStatus)
            {
                _lastCallStatus = isNowCall;

                if (isNowCall)
                {
                    callButton.BackgroundImage = offPanel.BackgroundImage;
                }
                else
                {
                    callButton.BackgroundImage = onPanel.BackgroundImage;
                }
            }
        }

        private void DeliveryOrderForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            CallManager.Instance.IsOpenChoose = false;
        }

        private async Task mainGridView_DoubleClick(EventArgs e)
        {
            if (e is DXMouseEventArgs ea)
            {
                GridHitInfo info = mainGridView.CalcHitInfo(ea.Location);
                if (info.InRow || info.InRowCell)
                {
                    var item = (OrderItem)mainGridView.GetRow(mainGridView.FocusedRowHandle);
                    if (item.ID != -1)
                    {
                        mainGridView.DeleteRow(mainGridView.FocusedRowHandle);
                        await ManualUpdateOnGrid(CurentBrand.ID);
                    }
                }
            }
        }

        private void isUnCashRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (isUnCashRadioButton.Checked)
            {
                if (PaymentMethod != PaymentMethod.OfflineCard && PaymentMethod != PaymentMethod.OnlineCard)
                {
                    PaymentMethod = PaymentMethod.OfflineCard;
                    paymentMethodList.SelectedValue = PaymentMethod;
                }
            }
        }

        private void is5kRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (PaymentMethod != PaymentMethod.Cash)
            {
                PaymentMethod = PaymentMethod.Cash;
                paymentMethodList.SelectedValue = PaymentMethod;
            }
        }

        private async Task buttonFillFromPhone_Click()
        {
            await FillFormFromPhone(true);
        }

        private void dateTimePickerBirthDate_ValueChanged(object sender, EventArgs e)
        {
            dateTimePickerBirthDate.CustomFormat = "dd.MM.yyyy";
        }

        protected void SetTextBoxCouponCode(string couponCode)

        {
            var coupon = GetCouponByCode(couponCode);
            if (coupon == null)
                return;

            _coupon = coupon;
            textBoxCouponCode.Text = coupon.Code;
        }
        protected void SetTextBoxCouponCode(int couponId)

        {
            var coupon = Content.CouponRepository.GetCouponById(couponId);
            if (coupon == null)
                return;

            _coupon = coupon;
            textBoxCouponCode.Text = coupon.Code;
            SetLabelCouponInfoText(coupon);
        }

        private void textBoxCouponCode_TextChanged()
        {
            var coupon = GetCouponByCode(textBoxCouponCode.Text);
            SetLabelCouponInfoText(coupon);
        }

        private Coupon GetCouponByCode(string couponCode)
        {
            Coupon coupon;
            if (!string.IsNullOrEmpty(couponCode))
            {
                coupon = Content.CouponRepository.GetCouponByCode(couponCode);
            }
            else
            {
                coupon = null;
            }
            return coupon;
        }
        
        private void SetLabelCouponInfoText(Coupon coupon)
        {
            if (coupon == null)
            {
                labelCouponInfo.Text = "Информация по купону не найдена";
                buttonApplyCoupon.Enabled = false;
            }
            else
            {
                labelCouponInfo.Text = Content.OrderManager.RuleToString(coupon.CouponRule);
                buttonApplyCoupon.Enabled = true;
            }
        }

        protected async Task buttonApplyCoupon_Click()
        {
            Coupon coupon = GetCouponByCode(textBoxCouponCode.Text);

            if (coupon != null)
            {
                _coupon = coupon;
                await ManualUpdateOnGrid(CurentBrand.ID);
                mainGridView.RefreshData();

            }
        }


    }

    public enum DetailsFormModes
    {
        NewOrder = 0, // Create new order
        EditOrder = 1, // EditPreOrder order
        ViewByManager = 2, // View order
        ViewByOperator = 3, // *
        NewOrderFromPreOrder = 4,
        ViewWebOrder = 5,
        ViewProcessed = 6
    }

    public class OrderNotFoundException : Exception
    {
        public OrderNotFoundException() : base("Заказа с таким Ид не существует. Вероятно, он был удален администратором.")
        {

        }
    }
}
