﻿namespace LaRenzo.Forms.Documents.WebOrderForms
{
    partial class WebOrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.dateWebOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnIsNewOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPaymentMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.сolumnDeliveryMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.brandColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sourceGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.timerBind = new System.Windows.Forms.Timer(this.components);
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainGridControl
            // 
            this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.mainGridControl.Location = new System.Drawing.Point(0, 39);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Margin = new System.Windows.Forms.Padding(4);
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(1310, 553);
            this.mainGridControl.TabIndex = 0;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.dateWebOrder,
            this.columnName,
            this.columnPhone,
            this.columnAddress,
            this.columnTime,
            this.columnIsNewOrder,
            this.columnPaid,
            this.columnPaymentMethod,
            this.сolumnDeliveryMethod,
            this.brandColumn,
            this.sourceGridColumn});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.mainGridView.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.mainGridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.mainGridView_RowStyle);
            this.mainGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.mainGridView_PopupMenuShowing);
            // 
            // dateWebOrder
            // 
            this.dateWebOrder.Caption = "Дата создания";
            this.dateWebOrder.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm";
            this.dateWebOrder.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateWebOrder.FieldName = "CreateDate";
            this.dateWebOrder.Name = "dateWebOrder";
            this.dateWebOrder.OptionsColumn.AllowEdit = false;
            this.dateWebOrder.OptionsColumn.ReadOnly = true;
            this.dateWebOrder.Visible = true;
            this.dateWebOrder.VisibleIndex = 0;
            // 
            // columnName
            // 
            this.columnName.Caption = "Имя";
            this.columnName.FieldName = "Name";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.OptionsColumn.ReadOnly = true;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 1;
            // 
            // columnPhone
            // 
            this.columnPhone.Caption = "Телефон";
            this.columnPhone.FieldName = "Phone";
            this.columnPhone.Name = "columnPhone";
            this.columnPhone.OptionsColumn.AllowEdit = false;
            this.columnPhone.OptionsColumn.ReadOnly = true;
            this.columnPhone.Visible = true;
            this.columnPhone.VisibleIndex = 2;
            // 
            // columnAddress
            // 
            this.columnAddress.Caption = "Адрес";
            this.columnAddress.FieldName = "Address";
            this.columnAddress.Name = "columnAddress";
            this.columnAddress.OptionsColumn.AllowEdit = false;
            this.columnAddress.OptionsColumn.ReadOnly = true;
            this.columnAddress.Visible = true;
            this.columnAddress.VisibleIndex = 3;
            // 
            // columnTime
            // 
            this.columnTime.Caption = "Время";
            this.columnTime.FieldName = "Time";
            this.columnTime.Name = "columnTime";
            this.columnTime.OptionsColumn.AllowEdit = false;
            this.columnTime.OptionsColumn.ReadOnly = true;
            this.columnTime.Visible = true;
            this.columnTime.VisibleIndex = 4;
            // 
            // columnIsNewOrder
            // 
            this.columnIsNewOrder.Caption = "Новый сайт";
            this.columnIsNewOrder.FieldName = "IsNewSiteOrder";
            this.columnIsNewOrder.Name = "columnIsNewOrder";
            this.columnIsNewOrder.OptionsColumn.AllowEdit = false;
            this.columnIsNewOrder.OptionsColumn.ReadOnly = true;
            this.columnIsNewOrder.Visible = true;
            this.columnIsNewOrder.VisibleIndex = 5;
            // 
            // columnPaid
            // 
            this.columnPaid.Caption = "Оплачен?";
            this.columnPaid.FieldName = "IsPaid";
            this.columnPaid.Name = "columnPaid";
            this.columnPaid.OptionsColumn.AllowEdit = false;
            this.columnPaid.OptionsColumn.ReadOnly = true;
            this.columnPaid.Visible = true;
            this.columnPaid.VisibleIndex = 6;
            // 
            // columnPaymentMethod
            // 
            this.columnPaymentMethod.Caption = "Метод оплаты";
            this.columnPaymentMethod.FieldName = "PaymentMethod";
            this.columnPaymentMethod.Name = "columnPaymentMethod";
            this.columnPaymentMethod.OptionsColumn.AllowEdit = false;
            this.columnPaymentMethod.OptionsColumn.ReadOnly = true;
            this.columnPaymentMethod.Visible = true;
            this.columnPaymentMethod.VisibleIndex = 7;
            // 
            // сolumnDeliveryMethod
            // 
            this.сolumnDeliveryMethod.Caption = "Доставка";
            this.сolumnDeliveryMethod.FieldName = "DeliveryMethodName";
            this.сolumnDeliveryMethod.Name = "сolumnDeliveryMethod";
            this.сolumnDeliveryMethod.OptionsColumn.AllowEdit = false;
            this.сolumnDeliveryMethod.OptionsColumn.ReadOnly = true;
            this.сolumnDeliveryMethod.Visible = true;
            this.сolumnDeliveryMethod.VisibleIndex = 8;
            // 
            // brandColumn
            // 
            this.brandColumn.Caption = "Бренд";
            this.brandColumn.FieldName = "Brand.Name";
            this.brandColumn.Name = "brandColumn";
            this.brandColumn.Visible = true;
            this.brandColumn.VisibleIndex = 9;
            // 
            // sourceGridColumn
            // 
            this.sourceGridColumn.Caption = "Источник";
            this.sourceGridColumn.FieldName = "Source";
            this.sourceGridColumn.Name = "sourceGridColumn";
            this.sourceGridColumn.Visible = true;
            this.sourceGridColumn.VisibleIndex = 10;
            // 
            // timerBind
            // 
            this.timerBind.Enabled = true;
            this.timerBind.Interval = 10000;
            // 
            // toolStrip
            // 
            this.toolStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.toolStrip.Size = new System.Drawing.Size(1310, 39);
            this.toolStrip.TabIndex = 2;
            this.toolStrip.Text = "toolStrip";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::LaRenzo.Properties.Resources.cross_48;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(101, 36);
            this.toolStripButton1.Text = "Удалить";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::LaRenzo.Properties.Resources.button_ok_4587;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(233, 36);
            this.toolStripButton2.Text = "Показать зависшие заказы";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // contextMenu
            // 
            this.contextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(61, 4);
            // 
            // WebOrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1310, 592);
            this.Controls.Add(this.mainGridControl);
            this.Controls.Add(this.toolStrip);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "WebOrderForm";
            this.Text = "Интернет заказы";
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private DevExpress.XtraGrid.Columns.GridColumn columnPhone;
        private DevExpress.XtraGrid.Columns.GridColumn columnAddress;
        private DevExpress.XtraGrid.Columns.GridColumn columnTime;
        private System.Windows.Forms.Timer timerBind;
        private DevExpress.XtraGrid.Columns.GridColumn columnIsNewOrder;
        private DevExpress.XtraGrid.Columns.GridColumn columnPaid;
        private DevExpress.XtraGrid.Columns.GridColumn columnPaymentMethod;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
		private DevExpress.XtraGrid.Columns.GridColumn сolumnDeliveryMethod;
		private DevExpress.XtraGrid.Columns.GridColumn brandColumn;
		private DevExpress.XtraGrid.Columns.GridColumn dateWebOrder;
		private DevExpress.XtraGrid.Columns.GridColumn sourceGridColumn;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
    }
}