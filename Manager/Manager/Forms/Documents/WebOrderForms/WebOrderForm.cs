﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Documents.WebOrders;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Documents.WebOrders;
using LaRenzo.Forms.Documents.DeliveryOrderForms;
using LaRenzo.Properties;
using LaRenzo.TransferLibrary;
using NLog;
using static LaRenzo.DataRepository.Repositories.Documents.WebOrders.WebOrderManager;

namespace LaRenzo.Forms.Documents.WebOrderForms
{
    public partial class WebOrderForm : Form
    {
        public int BrandId;

        private bool _onlyNewWebOrders = true;

        public WebOrderForm(int brandId)
        {
            BrandId = brandId;

            InitializeComponent();
            toolStripButton1.Click += async (s, e) => await DeleteToolStripButtonClick();
            mainGridView.DoubleClick += async (s, e) => await mainGridView_DoubleClick(s, e);
            timerBind.Tick += (s, e) => timerBind_Tick();
            Load += (s, e) => WebOrderForm_Load();
        }

        private void WebOrderForm_Load()
        {
            toolStrip.Visible = Content.UserManager.IsAdmin();
            Bind();
        }


        private void Bind()
        {
            try
            {

                List<WebOrder> webOrders = Content.WebOrderManager.GetUnhandledWebOrders(_onlyNewWebOrders);
                mainGridControl.DataSource = webOrders;
            }
            catch (Exception e1)
            {
                var logger = LogManager.GetCurrentClassLogger();
                logger.Info("Ошибка при первом получении веб заказов.");
                try
                {
                    List<WebOrder> webOrders = Content.WebOrderManager.GetUnhandledWebOrders(_onlyNewWebOrders);
                    mainGridControl.DataSource = webOrders;
                }
                catch (Exception e2)
                {
                    logger.Info("Ошибка при втором получении веб заказов.");
                    logger.Error(e2);
                    throw;
                }
            }
        }


        //___________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Двойной клик по элементу Grid </summary>
        ///===================================================
        private async Task mainGridView_DoubleClick(object sender, EventArgs args)
        {
            DXMouseEventArgs ea = args as DXMouseEventArgs;
            if (sender is GridView view)
            {
                if (ea != null)
                {
                    var info = view.CalcHitInfo(ea.Location);
                    if (info.InRow || info.InRowCell)
                    {
                        var webOrder = (WebOrder)mainGridView.GetRow(info.RowHandle);
                        await ShowWebOrderDetails(webOrder, webOrder.Brand);
                    }
                }
            }
        }


        //___________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Проверка на оплату по карте и не оплаченность </summary>
        ///===================================================================
        private bool IsUnpaidOnlineCardOrder(WebOrder webOrder)
        {
            bool isUnpaidOnlineCardOrder = false;

            if (webOrder != null)
            {
                isUnpaidOnlineCardOrder = !webOrder.IsPaid && webOrder.PaymentMethod == PaymentMethod.OnlineCard;
            }

            return isUnpaidOnlineCardOrder;
        }


        //___________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Показать детали web заказа </summary>
        ///================================================
        private async Task ShowWebOrderDetails(WebOrder webOrder, Brand brand)
        {
            var orderStateMessage = Content.WebOrderManager.CheckIfOrderCanBeProcessed(webOrder, brand.ID, Settings.Default.SiteProtocol);

            if (orderStateMessage != string.Empty)
            {
                if (Content.UserManager.IsAdmin() && Equals(webOrder.Status, WebOrderStatus.Processing))
                {
                    var result = MessageBox.Show("Остановить обработку заказа? Внимание: если есть открытая форма с этим заказом в вашей программе" + 
                        " или в программе другого менеджера, то она не закроется автоматически.", "Отмена обработки заказа", MessageBoxButtons.OKCancel);
                    if (result == DialogResult.OK)
                    {
                        Content.WebOrderManager.StopProcessing(webOrder);
                        Bind();
                    }

                }
                else
                {

                    MessageBox.Show(orderStateMessage);
                    Bind();
                }
            }
            else
            {
                // Проверка на оплату по карте и не оплаченность
                var form = IsUnpaidOnlineCardOrder(webOrder) ? await DeliveryOrderFormFactory.GetForm(webOrder, brand, DetailsFormModes.ViewWebOrder) : await DeliveryOrderFormFactory.GetForm(webOrder, brand);

                form.CurentBrand = brand;
                form.IsWebOrder = true;
                form.OrderProcessed += (state, order) => Bind();
                form.Show();
            }
        }


        private void timerBind_Tick()
        {
            Bind();
        }


        //___________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Нажато на "Удалить заказ" </summary>
        ////==============================================
        private async Task DeleteToolStripButtonClick()
        {
            var webOrder = (WebOrder)mainGridView.GetFocusedRow();

            if (webOrder != null)
            {
                await DeleteWebOrder(webOrder);
            }
        }


        //___________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Удалить web заказ </summary>
        ////======================================
        private async Task DeleteWebOrder(WebOrder webOrder)
        {
            if (MessageBox.Show(@"Вы действительно хотите удалить интернет заказ?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                string orderStateMessage = Content.WebOrderManager.CheckIfOrderCanBeProcessed(webOrder, BrandId, Settings.Default.SiteProtocol);

                if (orderStateMessage != string.Empty)
                {
                    MessageBox.Show(orderStateMessage);

                }
                else
                {
                    await Content.WebOrderManager.SetOrderProcessed(webOrder, BrandId, Settings.Default.SiteProtocol);
                }

                Bind();
            }
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var inProcess = DateTime.Now.AddMinutes(-30);
            var webOrder = mainGridView.GetRow(e.RowHandle) as WebOrder;

            if (webOrder == null) return;

            if (!webOrder.IsPaid && webOrder.PaymentMethod == PaymentMethod.OnlineCard)
                e.Appearance.BackColor = Color.Salmon;

            else if (Equals(webOrder.Status, WebOrderStatus.Processing))
                if (webOrder.CreateDate.CompareTo(inProcess) > 0)
                    e.Appearance.BackColor = Color.Orange;
                else
                    e.Appearance.BackColor = Color.Red;
        }

        private void mainGridView_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            var view = sender as GridView;

            if (view != null)
            {
                var hitInfo = view.CalcHitInfo(e.Point);

                if (view.IsDataRow(hitInfo.RowHandle))
                {
                    var webOrder = view.GetRow(hitInfo.RowHandle) as WebOrder;

                    if (webOrder == null) return;

                    contextMenu.Items.Clear();

                    contextMenu.Items.Add(new ToolStripButton("Наличные", null, (o, args) => ChangeWebOrderPaymentMethod(webOrder.ID, PaymentMethod.Cash)));

                    contextMenu.Items.Add(new ToolStripButton("Картой онлайн", null, (o, args) => ChangeWebOrderPaymentMethod(webOrder.ID, PaymentMethod.OnlineCard)));

                    contextMenu.Items.Add(new ToolStripButton("Картой курьеру", null, (o, args) => ChangeWebOrderPaymentMethod(webOrder.ID, PaymentMethod.OfflineCard)));

                    contextMenu.Show(view.GridControl, e.Point);
                }
            }
        }

        private void ChangeWebOrderPaymentMethod(int id, PaymentMethod method)
        {
            Content.WebOrderManager.ChangePaymentMethod(id, method);
            Bind();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            _onlyNewWebOrders = !_onlyNewWebOrders;
            toolStripButton2.Text = _onlyNewWebOrders ? "Показать зависшие заказы" : "Скрыть зависшие заказы";
            toolStripButton2.Image = _onlyNewWebOrders ? Properties.Resources.button_ok_4587 : Properties.Resources.cross_48;
            Bind();
        }
    }
}
