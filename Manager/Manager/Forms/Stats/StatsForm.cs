﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.XtraCharts;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Documents.Orders;

namespace LaRenzo.Forms.Stats
{
    public partial class StatsForm : Form
    {
        private IEnumerable<StatsOrderCount> Orders { get; set; }
        public StatsForm()
        {
            InitializeComponent();
            textBoxFilterToExclude.Text = "0,00,000,0000,00000,0000000000,111,123";
            GetData();
            GetStatsData();
        }

        private void GetStatsData()
        {
            mainGridControl.DataSource = GetGrid();
            columnOrdersCount.SortOrder = ColumnSortOrder.Descending;
            chartControl.Series.Clear();
            chartControl.Series.Add(GetSeries());
        }

        public void GetData()
        {
            Orders = Content.OrderManager.GetStatsList();
        }

        public Series GetSeries()
        {
            int ordersInTopCount = 0;
            var phones = textBoxFilterToExclude.Text.Split(',').Select(p => p.Trim());
            var orders = Orders.Where(x => !phones.Contains(x.Phone));
            var result = new Series("Заказы пользователей", ViewType.Pie3D) {LegendTextPattern = "{A}: {VP:p0}"};
            foreach (var order in orders.OrderByDescending(x=>x.OrdersCount).Take(10))
            {
                ordersInTopCount += order.OrdersCount;
                result.Points.Add(new SeriesPoint(order.Phone, order.OrdersCount));

            }
            chartControl.Titles.Add(new ChartTitle { Text = $"Заказы первых 10 пользователей: всего {ordersInTopCount}", WordWrap = true});
            result.Name = $"Заказы первых 10 пользователей: всего {ordersInTopCount}";
            return result;
        }

        public IEnumerable<StatsOrderCount> GetGrid()
        {
            var phones = textBoxFilterToExclude.Text.Split(',').Select(p => p.Trim());
            return Orders.Where(x=> !phones.Contains(x.Phone));
        }

        private void buttonApply_Click(object sender, System.EventArgs e)
        {
            GetStatsData();
        }
    }
}