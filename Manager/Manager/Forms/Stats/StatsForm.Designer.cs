﻿namespace LaRenzo.Forms.Stats
{
    partial class StatsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraCharts.SimpleDiagram3D simpleDiagram3D1 = new DevExpress.XtraCharts.SimpleDiagram3D();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.Pie3DSeriesView pie3DSeriesView1 = new DevExpress.XtraCharts.Pie3DSeriesView();
            DevExpress.XtraCharts.Pie3DSeriesView pie3DSeriesView2 = new DevExpress.XtraCharts.Pie3DSeriesView();
            this.detailGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.DishIdCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DishCustmerNameCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DishTotalCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DishCustomerPhoneCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DishCodeCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Driver = new DevExpress.XtraGrid.Columns.GridColumn();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnPhoneNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnOrdersCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.chartControl = new DevExpress.XtraCharts.ChartControl();
            this.textBoxFilterToExclude = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonApply = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.detailGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram3D1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // detailGridView
            // 
            this.detailGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.DishIdCol,
            this.DishCustmerNameCol,
            this.DishTotalCol,
            this.DishCustomerPhoneCol,
            this.DishCodeCol,
            this.Driver});
            this.detailGridView.GridControl = this.mainGridControl;
            this.detailGridView.Name = "detailGridView";
            // 
            // DishIdCol
            // 
            this.DishIdCol.Caption = "Ид";
            this.DishIdCol.FieldName = "ID";
            this.DishIdCol.Name = "DishIdCol";
            this.DishIdCol.OptionsColumn.AllowEdit = false;
            this.DishIdCol.OptionsColumn.AllowFocus = false;
            this.DishIdCol.OptionsColumn.ReadOnly = true;
            this.DishIdCol.Visible = true;
            this.DishIdCol.VisibleIndex = 0;
            // 
            // DishCustmerNameCol
            // 
            this.DishCustmerNameCol.Caption = "Имя";
            this.DishCustmerNameCol.FieldName = "Name";
            this.DishCustmerNameCol.Name = "DishCustmerNameCol";
            this.DishCustmerNameCol.OptionsColumn.AllowEdit = false;
            this.DishCustmerNameCol.OptionsColumn.AllowFocus = false;
            this.DishCustmerNameCol.OptionsColumn.ReadOnly = true;
            this.DishCustmerNameCol.Visible = true;
            this.DishCustmerNameCol.VisibleIndex = 2;
            // 
            // DishTotalCol
            // 
            this.DishTotalCol.Caption = "Сумма заказа";
            this.DishTotalCol.FieldName = "TotalCost";
            this.DishTotalCol.Name = "DishTotalCol";
            this.DishTotalCol.OptionsColumn.AllowEdit = false;
            this.DishTotalCol.OptionsColumn.AllowFocus = false;
            this.DishTotalCol.OptionsColumn.ReadOnly = true;
            this.DishTotalCol.Visible = true;
            this.DishTotalCol.VisibleIndex = 1;
            // 
            // DishCustomerPhoneCol
            // 
            this.DishCustomerPhoneCol.Caption = "Телефон";
            this.DishCustomerPhoneCol.FieldName = "Phone";
            this.DishCustomerPhoneCol.Name = "DishCustomerPhoneCol";
            this.DishCustomerPhoneCol.OptionsColumn.AllowEdit = false;
            this.DishCustomerPhoneCol.OptionsColumn.AllowFocus = false;
            this.DishCustomerPhoneCol.OptionsColumn.ReadOnly = true;
            this.DishCustomerPhoneCol.Visible = true;
            this.DishCustomerPhoneCol.VisibleIndex = 3;
            // 
            // DishCodeCol
            // 
            this.DishCodeCol.Caption = "Код заказа";
            this.DishCodeCol.FieldName = "Code";
            this.DishCodeCol.Name = "DishCodeCol";
            this.DishCodeCol.OptionsColumn.AllowEdit = false;
            this.DishCodeCol.OptionsColumn.AllowFocus = false;
            this.DishCodeCol.OptionsColumn.ReadOnly = true;
            this.DishCodeCol.Visible = true;
            this.DishCodeCol.VisibleIndex = 4;
            // 
            // Driver
            // 
            this.Driver.Caption = "Водитель";
            this.Driver.FieldName = "Driver.Name";
            this.Driver.Name = "Driver";
            this.Driver.OptionsColumn.AllowEdit = false;
            this.Driver.OptionsColumn.AllowFocus = false;
            this.Driver.OptionsColumn.ReadOnly = true;
            this.Driver.Visible = true;
            this.Driver.VisibleIndex = 5;
            // 
            // mainGridControl
            // 
            this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            gridLevelNode1.LevelTemplate = this.detailGridView;
            gridLevelNode1.RelationName = "Orders";
            this.mainGridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.mainGridControl.Location = new System.Drawing.Point(0, 0);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Margin = new System.Windows.Forms.Padding(4);
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(617, 541);
            this.mainGridControl.TabIndex = 4;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView,
            this.detailGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnPhoneNumber,
            this.columnOrdersCount});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.mainGridView.OptionsView.ShowFooter = true;
            // 
            // columnPhoneNumber
            // 
            this.columnPhoneNumber.Caption = "Номер телефона";
            this.columnPhoneNumber.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnPhoneNumber.FieldName = "Phone";
            this.columnPhoneNumber.Name = "columnPhoneNumber";
            this.columnPhoneNumber.OptionsColumn.AllowEdit = false;
            this.columnPhoneNumber.OptionsColumn.AllowFocus = false;
            this.columnPhoneNumber.OptionsColumn.ReadOnly = true;
            this.columnPhoneNumber.Visible = true;
            this.columnPhoneNumber.VisibleIndex = 0;
            // 
            // columnOrdersCount
            // 
            this.columnOrdersCount.Caption = "Количество заказов";
            this.columnOrdersCount.FieldName = "OrdersCount";
            this.columnOrdersCount.Name = "columnOrdersCount";
            this.columnOrdersCount.OptionsColumn.AllowEdit = false;
            this.columnOrdersCount.OptionsColumn.AllowFocus = false;
            this.columnOrdersCount.OptionsColumn.ReadOnly = true;
            this.columnOrdersCount.Visible = true;
            this.columnOrdersCount.VisibleIndex = 1;
            // 
            // chartControl
            // 
            this.chartControl.AppearanceNameSerializable = "In A Fog";
            simpleDiagram3D1.RotationMatrixSerializable = "1;0;0;0;0;0.5;-0.866025403784439;0;0;0.866025403784439;0.5;0;0;0;0;1";
            this.chartControl.Diagram = simpleDiagram3D1;
            this.chartControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl.Location = new System.Drawing.Point(0, 0);
            this.chartControl.Name = "chartControl";
            series1.Name = "Series 1";
            pie3DSeriesView1.SizeAsPercentage = 100D;
            pie3DSeriesView1.SweepDirection = DevExpress.XtraCharts.PieSweepDirection.Counterclockwise;
            series1.View = pie3DSeriesView1;
            this.chartControl.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1};
            pie3DSeriesView2.SizeAsPercentage = 100D;
            pie3DSeriesView2.SweepDirection = DevExpress.XtraCharts.PieSweepDirection.Counterclockwise;
            this.chartControl.SeriesTemplate.View = pie3DSeriesView2;
            this.chartControl.Size = new System.Drawing.Size(695, 541);
            this.chartControl.TabIndex = 5;
            // 
            // textBoxFilterToExclude
            // 
            this.textBoxFilterToExclude.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFilterToExclude.Location = new System.Drawing.Point(450, 0);
            this.textBoxFilterToExclude.Margin = new System.Windows.Forms.Padding(0);
            this.textBoxFilterToExclude.Multiline = true;
            this.textBoxFilterToExclude.Name = "textBoxFilterToExclude";
            this.textBoxFilterToExclude.Size = new System.Drawing.Size(753, 30);
            this.textBoxFilterToExclude.TabIndex = 6;
            this.textBoxFilterToExclude.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(450, 30);
            this.label1.TabIndex = 7;
            this.label1.Text = "Введите номера, через запятую, чтобы исключить их из выборки:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonApply
            // 
            this.buttonApply.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonApply.Location = new System.Drawing.Point(1210, 3);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(103, 24);
            this.buttonApply.TabIndex = 8;
            this.buttonApply.Text = "Применить";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(10);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer1.Panel1MinSize = 30;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1316, 575);
            this.splitContainer1.SplitterDistance = 30;
            this.splitContainer1.TabIndex = 9;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 450F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 113F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonApply, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBoxFilterToExclude, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1316, 30);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.mainGridControl);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.chartControl);
            this.splitContainer2.Size = new System.Drawing.Size(1316, 541);
            this.splitContainer2.SplitterDistance = 617;
            this.splitContainer2.TabIndex = 0;
            // 
            // StatsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1316, 575);
            this.Controls.Add(this.splitContainer1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "StatsForm";
            this.Text = "Количество заказов по номерам";
            ((System.ComponentModel.ISupportInitialize)(this.detailGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram3D1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraGrid.Columns.GridColumn columnPhoneNumber;
        private DevExpress.XtraGrid.Columns.GridColumn columnOrdersCount;
        private DevExpress.XtraGrid.Views.Grid.GridView detailGridView;
        private DevExpress.XtraGrid.Columns.GridColumn DishIdCol;
        private DevExpress.XtraGrid.Columns.GridColumn DishCustmerNameCol;
        private DevExpress.XtraGrid.Columns.GridColumn DishTotalCol;
        private DevExpress.XtraGrid.Columns.GridColumn DishCustomerPhoneCol;
        private DevExpress.XtraGrid.Columns.GridColumn DishCodeCol;
        private DevExpress.XtraGrid.Columns.GridColumn Driver;
        private DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraCharts.ChartControl chartControl;
        private System.Windows.Forms.TextBox textBoxFilterToExclude;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}