﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Catalogs.BlackList;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.BlackList;

namespace LaRenzo.Forms.BlackList
{
    public partial class BlackListForm : Form
    {
        public BlackListForm()
        {
            InitializeComponent();
            RefreshBlackList();
        }


        public BlackListForm(Order order)
		{
            InitializeComponent();

	        var data = Content.BlackListManager.GetListByFilter(order.Phone, order.Street, order.House, order.Appartament);

            if (!data.Any())
	        {
		        if (MessageBox.Show($"Занести {order.Name} в черный список?", @"Ничего на найдено", MessageBoxButtons.YesNo) == DialogResult.Yes)
		        {
			        ShowDetailBlackItemForm(new BlackListItem
			        {
				        Name = order.Name,
				        Phone = order.Phone,
				        Street = order.Street,
				        House = order.House,
				        Appartment = order.Appartament
			        });

			        RefreshBlackList();
		        }
	        }
	        else
	        {
		        RefreshBlackList(data);
	        }
        }


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Обновит чёрный чёрный список </summary>
		////=================================================
        public void RefreshBlackList(List<BlackListItem> list = null )
        {
	        mainGridControl.DataSource = list ?? Content.BlackListManager.GetList();
        }


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Обновить" </summary>
		////======================================
		private void BtnRefreshClick(object sender, EventArgs e)
        {
            RefreshBlackList();
        }


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Добавить" </summary>
		////======================================
		private void BtnAddClick(object sender, EventArgs e)
        {
            ShowDetailBlackItemForm(null);
        }


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Показать форму деталей </summary>
		////===========================================
        private void ShowDetailBlackItemForm(BlackListItem item, bool isEdit = false)
        {
            var form = new BlackListItemForm(item, isEdit);
            form.FormClosed += (sender, args) => RefreshBlackList();
            form.Show();
        }


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Двойной клик по записи </summary>
		////===========================================
        private void MainGridViewDoubleClick(object sender, EventArgs e)
        {
            var view = (GridView) sender;

	        if (view.IsDataRow(view.FocusedRowHandle))
	        {
		        const bool isEdit = true;
		        ShowDetailBlackItemForm((BlackListItem)view.GetRow(view.FocusedRowHandle), isEdit);
	        }
        }
    }
}
