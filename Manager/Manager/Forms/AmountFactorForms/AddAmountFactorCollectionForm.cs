﻿using System;
using System.Windows.Forms;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Others.AmountFactories;
using LaRenzo.Properties;

namespace LaRenzo.Forms.AmountFactorForms
{
    public partial class AddAmountFactorCollectionForm : Form
    {
        public event EventHandler CollectionAdded;

        public void OnCollectionAdded(EventArgs e)
        {
            EventHandler handler = CollectionAdded;
            if (handler != null) handler(this, e);
        }

        public AddAmountFactorCollectionForm()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtName.Text))
                MessageBox.Show(Resources.EnterName);
            else
            {
                Content.AmountFactorManager.Add(txtName.Text);
                OnCollectionAdded(EventArgs.Empty);
                Close();
            }
            
        }
    }
}
