﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using LaRenzo.ControlsLib.BindFilterListControl;
using LaRenzo.DataRepository.Entities.Others.AmountFactories;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Others.AmountFactories;
using LaRenzo.Properties;

namespace LaRenzo.Forms.AmountFactorForms
{
    public partial class AmountFactorCollectionsForm : Form
    {
        private int _currentCollectionId;
        
        public AmountFactorCollectionsForm()
        {
            InitializeComponent();
            TagDeselected();
        }



        private void AmountFactorCollectionsForm_Load(object sender, EventArgs e)
        {
            BindCollections();
        }

        private void DataOnListChanged(object sender, ListChangedEventArgs e)
        {
            
            if (e.ListChangedType == ListChangedType.ItemChanged || e.ListChangedType == ListChangedType.ItemAdded)
            {
                var factor = mainGridView.GetRow(mainGridView.GetSelectedRows()[0]) as AmountFactor;
                if (factor != null)
                {
                    factor.ParentID = _currentCollectionId;
                    Content.AmountFactorManager.AddOrUpdateFactor(factor);
                } 
            }

        }
        /*
        private AmountFactor GetSelectedFactor()
        {

        }*/

        private void toolStripButtonAddCollection_Click(object sender, EventArgs e)
        {
            var form = new AddAmountFactorCollectionForm();
            form.CollectionAdded += (o, args) => BindCollections();
            form.ShowDialog();
        }

        private void BindCollections()
        {
            bFilterCollections.Bind(Content.AmountFactorManager.GetList().Select(x => new BindFilterList.BindListRow(x.ID, x.Name)).ToList());
        }

        private void BindFactors(int collectionId)
        {
            var data = new BindingList<AmountFactor>(Content.AmountFactorManager.GetFactors(collectionId).OrderBy(x => x.Amount).ToList());


            data.ListChanged += DataOnListChanged;
            data.RaiseListChangedEvents = true;
            mainGridControl.DataSource = data;
        }

        private void flCollections_SelectedRowChanged(object sender, SelectedRowChangedEventArgs e)
        {
            BindFactors(e.ID);
            TagSelected(e.ID);
        }


        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var factor = mainGridView.GetRow(mainGridView.GetSelectedRows()[0]) as AmountFactor;

            mainGridView.DeleteSelectedRows();

            if (factor != null) 
			{
                Content.AmountFactorManager.RemoveFactor(factor.ID);
	        }
        }

        private void toolStripButtonRemoveCollection_Click(object sender, EventArgs e)
        {
            var selected = bFilterCollections.GetSelected();
            if (selected != null)
            {
                Content.AmountFactorManager.RemoveCollection(selected.ID);
                BindCollections();
                TagDeselected();
            }
            else MessageBox.Show(Resources.SelectTagToDelete);

        }

        private void TagDeselected()
        {
            mainGridControl.Enabled = false;
            _currentCollectionId = 0;
        }

        private void TagSelected(int id)
        {
            mainGridControl.Enabled = true;
            _currentCollectionId = id;
        }
    }
}
