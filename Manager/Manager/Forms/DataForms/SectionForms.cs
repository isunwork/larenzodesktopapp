﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.Forms.DataForms.Controls;

namespace LaRenzo.Forms.DataForms
{
    public partial class SectionsForm : Form
    {
        public SectionsForm()
        {
            InitializeComponent();
        }



        private void SectionsForm_Load(object sender, EventArgs e)
        {
	        LoadBrandTabs();
        }


		//___________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Загрузить табы брэндов </summary>
		////===========================================
		private void LoadBrandTabs()
		{
			List<Brand> brands = Content.BrandManager.GetList();

			foreach (Brand brand in brands)
			{
				if (!string.IsNullOrEmpty(brand.Name))
				{
					SectionControl listByBrandControl = new SectionControl(brand)
					{
						Dock = DockStyle.Fill
					};

					TabPage tp = new TabPage
					{
						Text = brand.Name,
						TabIndex = brand.ID,
						Visible = true
					};

					tp.Controls.Add(listByBrandControl);

					tabControl.TabPages.Add(tp);
				}
			}
		}

    }
}
