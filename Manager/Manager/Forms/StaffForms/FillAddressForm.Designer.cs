﻿namespace LaRenzo.Forms.StaffForms
{
    partial class FillAddressForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbStreet = new System.Windows.Forms.ComboBox();
            this.lblCountry = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.cmbCountry = new System.Windows.Forms.ComboBox();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cmbHouse = new System.Windows.Forms.ComboBox();
            this.cmbPart = new System.Windows.Forms.ComboBox();
            this.cmbBuild = new System.Windows.Forms.ComboBox();
            this.cmbFlat = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.textHouse = new System.Windows.Forms.TextBox();
            this.txtPart = new System.Windows.Forms.TextBox();
            this.textBuild = new System.Windows.Forms.TextBox();
            this.txtFlat = new System.Windows.Forms.TextBox();
            this.pnlBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbStreet
            // 
            this.cmbStreet.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbStreet.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.cmbStreet.FormattingEnabled = true;
            this.cmbStreet.Location = new System.Drawing.Point(12, 107);
            this.cmbStreet.Name = "cmbStreet";
            this.cmbStreet.Size = new System.Drawing.Size(314, 21);
            this.cmbStreet.TabIndex = 3;
            this.cmbStreet.Tag = "3";
            // 
            // lblCountry
            // 
            this.lblCountry.Location = new System.Drawing.Point(12, 12);
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Size = new System.Drawing.Size(37, 13);
            this.lblCountry.TabIndex = 1011;
            this.lblCountry.Text = "Страна";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 43);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(132, 13);
            this.labelControl2.TabIndex = 1008;
            this.labelControl2.Text = "Город, населенный пункт";
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.btnOk);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 240);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(336, 24);
            this.pnlBottom.TabIndex = 1014;
            // 
            // btnOk
            // 
            this.btnOk.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnOk.Location = new System.Drawing.Point(160, 0);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(87, 22);
            this.btnOk.TabIndex = 12;
            this.btnOk.Text = "ОК";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.Location = new System.Drawing.Point(247, 0);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 22);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(189, 12);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(37, 13);
            this.labelControl3.TabIndex = 1011;
            this.labelControl3.Text = "Индекс";
            // 
            // cmbCountry
            // 
            this.cmbCountry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCountry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.cmbCountry.FormattingEnabled = true;
            this.cmbCountry.Items.AddRange(new object[] {
            "Россия"});
            this.cmbCountry.Location = new System.Drawing.Point(55, 9);
            this.cmbCountry.Name = "cmbCountry";
            this.cmbCountry.Size = new System.Drawing.Size(128, 21);
            this.cmbCountry.TabIndex = 0;
            this.cmbCountry.Tag = "1";
            this.cmbCountry.Text = "Россия";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(12, 88);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(31, 13);
            this.labelControl4.TabIndex = 1008;
            this.labelControl4.Text = "Улица";
            // 
            // cmbHouse
            // 
            this.cmbHouse.FormattingEnabled = true;
            this.cmbHouse.Items.AddRange(new object[] {
            "Дом",
            "Владение",
            "Домовладение"});
            this.cmbHouse.Location = new System.Drawing.Point(12, 134);
            this.cmbHouse.Name = "cmbHouse";
            this.cmbHouse.Size = new System.Drawing.Size(121, 21);
            this.cmbHouse.TabIndex = 4;
            this.cmbHouse.Tag = "40";
            this.cmbHouse.Text = "Дом";
            // 
            // cmbPart
            // 
            this.cmbPart.FormattingEnabled = true;
            this.cmbPart.Items.AddRange(new object[] {
            "Корпус",
            "Строение",
            "Литера",
            "Сооружение",
            "Участок"});
            this.cmbPart.Location = new System.Drawing.Point(12, 161);
            this.cmbPart.Name = "cmbPart";
            this.cmbPart.Size = new System.Drawing.Size(121, 21);
            this.cmbPart.TabIndex = 6;
            this.cmbPart.Tag = "50";
            this.cmbPart.Text = "Корпус";
            // 
            // cmbBuild
            // 
            this.cmbBuild.FormattingEnabled = true;
            this.cmbBuild.Items.AddRange(new object[] {
            "Корпус",
            "Строение",
            "Литера",
            "Сооружение",
            "Участок"});
            this.cmbBuild.Location = new System.Drawing.Point(12, 188);
            this.cmbBuild.Name = "cmbBuild";
            this.cmbBuild.Size = new System.Drawing.Size(121, 21);
            this.cmbBuild.TabIndex = 8;
            this.cmbBuild.Tag = "60";
            this.cmbBuild.Text = "Строение";
            // 
            // cmbFlat
            // 
            this.cmbFlat.FormattingEnabled = true;
            this.cmbFlat.Items.AddRange(new object[] {
            "Квартира",
            "Офис",
            "Бокс",
            "Помещение",
            "Комната"});
            this.cmbFlat.Location = new System.Drawing.Point(12, 215);
            this.cmbFlat.Name = "cmbFlat";
            this.cmbFlat.Size = new System.Drawing.Size(121, 21);
            this.cmbFlat.TabIndex = 10;
            this.cmbFlat.Tag = "70";
            this.cmbFlat.Text = "Квартира";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(232, 9);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(94, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.Tag = "0";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(12, 62);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(314, 20);
            this.txtCity.TabIndex = 2;
            this.txtCity.Tag = "2";
            // 
            // textHouse
            // 
            this.textHouse.Location = new System.Drawing.Point(139, 134);
            this.textHouse.Name = "textHouse";
            this.textHouse.Size = new System.Drawing.Size(76, 20);
            this.textHouse.TabIndex = 5;
            this.textHouse.Tag = "41";
            // 
            // txtPart
            // 
            this.txtPart.Location = new System.Drawing.Point(139, 161);
            this.txtPart.Name = "txtPart";
            this.txtPart.Size = new System.Drawing.Size(76, 20);
            this.txtPart.TabIndex = 7;
            this.txtPart.Tag = "51";
            // 
            // textBuild
            // 
            this.textBuild.Location = new System.Drawing.Point(139, 189);
            this.textBuild.Name = "textBuild";
            this.textBuild.Size = new System.Drawing.Size(76, 20);
            this.textBuild.TabIndex = 9;
            this.textBuild.Tag = "61";
            // 
            // txtFlat
            // 
            this.txtFlat.Location = new System.Drawing.Point(139, 216);
            this.txtFlat.Name = "txtFlat";
            this.txtFlat.Size = new System.Drawing.Size(76, 20);
            this.txtFlat.TabIndex = 11;
            this.txtFlat.Tag = "71";
            // 
            // FillAddressForm
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(336, 264);
            this.Controls.Add(this.txtCity);
            this.Controls.Add(this.txtFlat);
            this.Controls.Add(this.textBuild);
            this.Controls.Add(this.txtPart);
            this.Controls.Add(this.textHouse);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.cmbFlat);
            this.Controls.Add(this.cmbBuild);
            this.Controls.Add(this.cmbPart);
            this.Controls.Add(this.cmbHouse);
            this.Controls.Add(this.cmbCountry);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.cmbStreet);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.lblCountry);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(352, 302);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(352, 302);
            this.Name = "FillAddressForm";
            this.Text = "Адрес";
            this.pnlBottom.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbStreet;
        private DevExpress.XtraEditors.LabelControl lblCountry;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.ComboBox cmbCountry;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private System.Windows.Forms.ComboBox cmbHouse;
        private System.Windows.Forms.ComboBox cmbPart;
        private System.Windows.Forms.ComboBox cmbBuild;
        private System.Windows.Forms.ComboBox cmbFlat;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.TextBox textHouse;
        private System.Windows.Forms.TextBox txtPart;
        private System.Windows.Forms.TextBox textBuild;
        private System.Windows.Forms.TextBox txtFlat;
    }
}