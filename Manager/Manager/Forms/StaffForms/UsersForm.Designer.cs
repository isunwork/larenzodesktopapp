﻿using LaRenzo.ControlsLib.BindFilterListControl;

namespace LaRenzo.Forms.StaffForms
{
    partial class UsersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainSplitControl = new DevExpress.XtraEditors.SplitContainerControl();
            this.bFilterListUsers = new LaRenzo.ControlsLib.BindFilterListControl.BindFilterList();
            this.btnUserRights = new System.Windows.Forms.Button();
            this.chkBoxUserIsCafe = new System.Windows.Forms.CheckBox();
            this.chkWarehouseManager = new System.Windows.Forms.CheckBox();
            this.txtUserPassAgain = new System.Windows.Forms.TextBox();
            this.txtUserPass = new System.Windows.Forms.TextBox();
            this.chkBoxUserIsAdmin = new System.Windows.Forms.CheckBox();
            this.lblRePassword = new System.Windows.Forms.Label();
            this.btnUserDrop = new System.Windows.Forms.Button();
            this.btnUserDelete = new System.Windows.Forms.Button();
            this.btnUserUpdate = new System.Windows.Forms.Button();
            this.btnUserAdd = new System.Windows.Forms.Button();
            this.lblNewPassword = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxPrintPassword = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitControl)).BeginInit();
            this.mainSplitControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainSplitControl
            // 
            this.mainSplitControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitControl.Location = new System.Drawing.Point(0, 0);
            this.mainSplitControl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mainSplitControl.Name = "mainSplitControl";
            this.mainSplitControl.Panel1.Controls.Add(this.bFilterListUsers);
            this.mainSplitControl.Panel1.Text = "Panel1";
            this.mainSplitControl.Panel2.Controls.Add(this.comboBoxPrintPassword);
            this.mainSplitControl.Panel2.Controls.Add(this.label1);
            this.mainSplitControl.Panel2.Controls.Add(this.btnUserRights);
            this.mainSplitControl.Panel2.Controls.Add(this.chkBoxUserIsCafe);
            this.mainSplitControl.Panel2.Controls.Add(this.chkWarehouseManager);
            this.mainSplitControl.Panel2.Controls.Add(this.txtUserPassAgain);
            this.mainSplitControl.Panel2.Controls.Add(this.txtUserPass);
            this.mainSplitControl.Panel2.Controls.Add(this.chkBoxUserIsAdmin);
            this.mainSplitControl.Panel2.Controls.Add(this.lblRePassword);
            this.mainSplitControl.Panel2.Controls.Add(this.btnUserDrop);
            this.mainSplitControl.Panel2.Controls.Add(this.btnUserDelete);
            this.mainSplitControl.Panel2.Controls.Add(this.btnUserUpdate);
            this.mainSplitControl.Panel2.Controls.Add(this.btnUserAdd);
            this.mainSplitControl.Panel2.Controls.Add(this.lblNewPassword);
            this.mainSplitControl.Panel2.Controls.Add(this.lblUserName);
            this.mainSplitControl.Panel2.Controls.Add(this.txtUserName);
            this.mainSplitControl.Panel2.Text = "Panel2";
            this.mainSplitControl.Size = new System.Drawing.Size(564, 389);
            this.mainSplitControl.SplitterPosition = 187;
            this.mainSplitControl.TabIndex = 1;
            this.mainSplitControl.Text = "splitContainerControl5";
            // 
            // bFilterListUsers
            // 
            this.bFilterListUsers.AutoSize = true;
            this.bFilterListUsers.BackColor = System.Drawing.SystemColors.Control;
            this.bFilterListUsers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bFilterListUsers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bFilterListUsers.Filter = "";
            this.bFilterListUsers.Location = new System.Drawing.Point(0, 0);
            this.bFilterListUsers.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.bFilterListUsers.Name = "bFilterListUsers";
            this.bFilterListUsers.Size = new System.Drawing.Size(187, 389);
            this.bFilterListUsers.TabIndex = 2;
            this.bFilterListUsers.SelectedRowChanged += new System.EventHandler<LaRenzo.ControlsLib.BindFilterListControl.SelectedRowChangedEventArgs>(this.bFilterListUsers_SelectedRowChanged);
            // 
            // btnUserRights
            // 
            this.btnUserRights.Location = new System.Drawing.Point(164, 308);
            this.btnUserRights.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnUserRights.Name = "btnUserRights";
            this.btnUserRights.Size = new System.Drawing.Size(133, 28);
            this.btnUserRights.TabIndex = 31;
            this.btnUserRights.Text = "Права";
            this.btnUserRights.UseVisualStyleBackColor = true;
            this.btnUserRights.Click += new System.EventHandler(this.btnUserRights_Click);
            // 
            // chkBoxUserIsCafe
            // 
            this.chkBoxUserIsCafe.AutoSize = true;
            this.chkBoxUserIsCafe.BackColor = System.Drawing.SystemColors.Control;
            this.chkBoxUserIsCafe.Location = new System.Drawing.Point(172, 181);
            this.chkBoxUserIsCafe.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkBoxUserIsCafe.Name = "chkBoxUserIsCafe";
            this.chkBoxUserIsCafe.Size = new System.Drawing.Size(74, 21);
            this.chkBoxUserIsCafe.TabIndex = 30;
            this.chkBoxUserIsCafe.Text = "Кафе?";
            this.chkBoxUserIsCafe.UseVisualStyleBackColor = false;
            this.chkBoxUserIsCafe.Visible = false;
            // 
            // chkWarehouseManager
            // 
            this.chkWarehouseManager.AutoSize = true;
            this.chkWarehouseManager.Location = new System.Drawing.Point(23, 212);
            this.chkWarehouseManager.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkWarehouseManager.Name = "chkWarehouseManager";
            this.chkWarehouseManager.Size = new System.Drawing.Size(112, 21);
            this.chkWarehouseManager.TabIndex = 29;
            this.chkWarehouseManager.Text = "Кладовщик?";
            this.chkWarehouseManager.UseVisualStyleBackColor = true;
            this.chkWarehouseManager.Visible = false;
            // 
            // txtUserPassAgain
            // 
            this.txtUserPassAgain.Location = new System.Drawing.Point(23, 148);
            this.txtUserPassAgain.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUserPassAgain.Name = "txtUserPassAgain";
            this.txtUserPassAgain.PasswordChar = '*';
            this.txtUserPassAgain.Size = new System.Drawing.Size(268, 22);
            this.txtUserPassAgain.TabIndex = 28;
            // 
            // txtUserPass
            // 
            this.txtUserPass.Location = new System.Drawing.Point(19, 90);
            this.txtUserPass.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUserPass.Name = "txtUserPass";
            this.txtUserPass.PasswordChar = '*';
            this.txtUserPass.Size = new System.Drawing.Size(272, 22);
            this.txtUserPass.TabIndex = 27;
            // 
            // chkBoxUserIsAdmin
            // 
            this.chkBoxUserIsAdmin.AutoSize = true;
            this.chkBoxUserIsAdmin.Location = new System.Drawing.Point(23, 181);
            this.chkBoxUserIsAdmin.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkBoxUserIsAdmin.Name = "chkBoxUserIsAdmin";
            this.chkBoxUserIsAdmin.Size = new System.Drawing.Size(141, 21);
            this.chkBoxUserIsAdmin.TabIndex = 24;
            this.chkBoxUserIsAdmin.Text = "Администратор?";
            this.chkBoxUserIsAdmin.UseVisualStyleBackColor = true;
            // 
            // lblRePassword
            // 
            this.lblRePassword.AutoSize = true;
            this.lblRePassword.Location = new System.Drawing.Point(19, 128);
            this.lblRePassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRePassword.Name = "lblRePassword";
            this.lblRePassword.Size = new System.Drawing.Size(129, 17);
            this.lblRePassword.TabIndex = 23;
            this.lblRePassword.Text = "Повторить пароль";
            // 
            // btnUserDrop
            // 
            this.btnUserDrop.Image = global::LaRenzo.Properties.Resources.cross;
            this.btnUserDrop.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnUserDrop.Location = new System.Drawing.Point(192, 27);
            this.btnUserDrop.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnUserDrop.Name = "btnUserDrop";
            this.btnUserDrop.Size = new System.Drawing.Size(105, 30);
            this.btnUserDrop.TabIndex = 21;
            this.btnUserDrop.Text = "Сбросить";
            this.btnUserDrop.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUserDrop.UseVisualStyleBackColor = true;
            this.btnUserDrop.Click += new System.EventHandler(this.btnUserDrop_Click);
            // 
            // btnUserDelete
            // 
            this.btnUserDelete.Location = new System.Drawing.Point(23, 346);
            this.btnUserDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnUserDelete.Name = "btnUserDelete";
            this.btnUserDelete.Size = new System.Drawing.Size(133, 28);
            this.btnUserDelete.TabIndex = 19;
            this.btnUserDelete.Text = "Удалить";
            this.btnUserDelete.UseVisualStyleBackColor = true;
            this.btnUserDelete.Click += new System.EventHandler(this.btnUserDelete_Click);
            // 
            // btnUserUpdate
            // 
            this.btnUserUpdate.Location = new System.Drawing.Point(23, 308);
            this.btnUserUpdate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnUserUpdate.Name = "btnUserUpdate";
            this.btnUserUpdate.Size = new System.Drawing.Size(133, 28);
            this.btnUserUpdate.TabIndex = 18;
            this.btnUserUpdate.Text = "Сохранить";
            this.btnUserUpdate.UseVisualStyleBackColor = true;
            this.btnUserUpdate.Click += new System.EventHandler(this.btnUserUpdate_Click);
            // 
            // btnUserAdd
            // 
            this.btnUserAdd.Location = new System.Drawing.Point(23, 272);
            this.btnUserAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnUserAdd.Name = "btnUserAdd";
            this.btnUserAdd.Size = new System.Drawing.Size(133, 28);
            this.btnUserAdd.TabIndex = 17;
            this.btnUserAdd.Text = "Добавить";
            this.btnUserAdd.UseVisualStyleBackColor = true;
            this.btnUserAdd.Click += new System.EventHandler(this.btnUserAdd_Click);
            // 
            // lblNewPassword
            // 
            this.lblNewPassword.AutoSize = true;
            this.lblNewPassword.Location = new System.Drawing.Point(19, 70);
            this.lblNewPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNewPassword.Name = "lblNewPassword";
            this.lblNewPassword.Size = new System.Drawing.Size(102, 17);
            this.lblNewPassword.TabIndex = 14;
            this.lblNewPassword.Text = "Новый пароль";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(19, 12);
            this.lblUserName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(131, 17);
            this.lblUserName.TabIndex = 12;
            this.lblUserName.Text = "Имя пользователя";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(23, 32);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(160, 22);
            this.txtUserName.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 240);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 17);
            this.label1.TabIndex = 32;
            this.label1.Text = "Пароль кассы";
            // 
            // comboBoxPrintPassword
            // 
            this.comboBoxPrintPassword.FormattingEnabled = true;
            this.comboBoxPrintPassword.Location = new System.Drawing.Point(170, 237);
            this.comboBoxPrintPassword.Name = "comboBoxPrintPassword";
            this.comboBoxPrintPassword.Size = new System.Drawing.Size(121, 24);
            this.comboBoxPrintPassword.TabIndex = 33;
            // 
            // UsersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 389);
            this.Controls.Add(this.mainSplitControl);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "UsersForm";
            this.Text = "Пользователи";
            this.Load += new System.EventHandler(this.UsersForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitControl)).EndInit();
            this.mainSplitControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl mainSplitControl;
        public System.Windows.Forms.CheckBox chkBoxUserIsAdmin;
        private System.Windows.Forms.Label lblRePassword;
        public System.Windows.Forms.Button btnUserDrop;
        public System.Windows.Forms.Button btnUserDelete;
        public System.Windows.Forms.Button btnUserUpdate;
        public System.Windows.Forms.Button btnUserAdd;
        private System.Windows.Forms.Label lblNewPassword;
        private System.Windows.Forms.Label lblUserName;
        public System.Windows.Forms.TextBox txtUserName;
        private BindFilterList bFilterListUsers;
        private System.Windows.Forms.TextBox txtUserPassAgain;
        private System.Windows.Forms.TextBox txtUserPass;
        public System.Windows.Forms.CheckBox chkWarehouseManager;
        public System.Windows.Forms.CheckBox chkBoxUserIsCafe;
        public System.Windows.Forms.Button btnUserRights;
        private System.Windows.Forms.ComboBox comboBoxPrintPassword;
        private System.Windows.Forms.Label label1;
    }
}