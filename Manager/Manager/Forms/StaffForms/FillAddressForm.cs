﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using LaRenzo.DataRepository.Repositories.Catalogs.Addresses;
using LaRenzo.DataRepository.Repositories;

namespace LaRenzo.Forms.StaffForms
{
    public partial class FillAddressForm : Form
    {
        public string FullAddress;
        private readonly List<Control> _addressControls = new List<Control>();

        public FillAddressForm()
        {
            InitializeComponent();
            _addressControls.AddRange(Controls.OfType<TextBox>().AsEnumerable());
            _addressControls.AddRange(Controls.OfType<ComboBox>().AsEnumerable());
            cmbStreet.AutoCompleteCustomSource.AddRange(GetAddressFromDataBase());
        }

        private string[] GetAddressFromDataBase()
        {
            return Content.AddressManager.GetAddressArray() as string[];
        }

        private void GetAddressFullStr()
        {
            var sb = new StringBuilder();
            var addresses = _addressControls.OrderBy(x => Convert.ToInt32(x.Tag)).ToList();

            for (int i = 0; i < addresses.Count(); i++)
            {
                if (Convert.ToInt32(addresses[i].Tag) < 10)
                {
                    sb.Append(!string.IsNullOrEmpty(addresses[i].Text) ? addresses[i].Text + ", " : string.Empty);
                }
                else
                {
                    if ((Convert.ToInt32(addresses[i].Tag)) % 10 == 0)
                    {
                        sb.Append(!string.IsNullOrEmpty(addresses[i + 1].Text)
                            ? addresses[i].Text + " " + addresses[i + 1].Text + (i+1 == addresses.Count() - 1 ? "": ", ")
                                      : string.Empty);
                    }
                }
            }

            FullAddress = sb.ToString();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            GetAddressFullStr();
            DialogResult = DialogResult.OK;
        }
    }
}
