﻿using System;
using System.Linq;
using System.Windows.Forms;
using LaRenzo.Properties;

namespace LaRenzo.Forms.StaffForms
{
    public partial class SimpleTextDialogForm : Form
    {
        public string FieldName { get; set; }
        public string Result { get; set; }
        public string Input { get; set; }

        public SimpleTextDialogForm()
        {
            InitializeComponent();
        }

        private void AddressGroupDialogForm_Load(object sender, EventArgs e)
        {
            lblName.Text = FieldName;
            txtResult.Text = Input;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!txtResult.Text.Any())
            {
                var ep = new ErrorProvider();
                ep.SetError(txtResult, Resources.ErrorRequiredField);
            }
            else
            {
                Result = txtResult.Text;
                DialogResult = DialogResult.OK;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
