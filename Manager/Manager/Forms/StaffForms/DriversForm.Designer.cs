﻿using LaRenzo.ControlsLib.BindFilterListControl;

namespace LaRenzo.Forms.StaffForms
{
    partial class DriversForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainSplitControl = new DevExpress.XtraEditors.SplitContainerControl();
            this.bFilterListDrivers = new LaRenzo.ControlsLib.BindFilterListControl.BindFilterList();
            this.buttonDrop = new System.Windows.Forms.Button();
            this.txtIdToEdit = new System.Windows.Forms.TextBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblNewPassword = new System.Windows.Forms.Label();
            this.txtNewPass = new System.Windows.Forms.TextBox();
            this.lblRePassword = new System.Windows.Forms.Label();
            this.txtRePass = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitControl)).BeginInit();
            this.mainSplitControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainSplitControl
            // 
            this.mainSplitControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitControl.Location = new System.Drawing.Point(0, 0);
            this.mainSplitControl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mainSplitControl.Name = "mainSplitControl";
            this.mainSplitControl.Panel1.Controls.Add(this.bFilterListDrivers);
            this.mainSplitControl.Panel1.Text = "Panel1";
            this.mainSplitControl.Panel2.Controls.Add(this.txtRePass);
            this.mainSplitControl.Panel2.Controls.Add(this.lblRePassword);
            this.mainSplitControl.Panel2.Controls.Add(this.txtNewPass);
            this.mainSplitControl.Panel2.Controls.Add(this.lblNewPassword);
            this.mainSplitControl.Panel2.Controls.Add(this.buttonDrop);
            this.mainSplitControl.Panel2.Controls.Add(this.txtIdToEdit);
            this.mainSplitControl.Panel2.Controls.Add(this.btnDelete);
            this.mainSplitControl.Panel2.Controls.Add(this.btnUpdate);
            this.mainSplitControl.Panel2.Controls.Add(this.btnAdd);
            this.mainSplitControl.Panel2.Controls.Add(this.lblName);
            this.mainSplitControl.Panel2.Controls.Add(this.txtName);
            this.mainSplitControl.Panel2.Text = "Panel2";
            this.mainSplitControl.Size = new System.Drawing.Size(532, 367);
            this.mainSplitControl.SplitterPosition = 225;
            this.mainSplitControl.TabIndex = 1;
            this.mainSplitControl.Text = "splitContainerControl2";
            // 
            // bFilterListDrivers
            // 
            this.bFilterListDrivers.AutoSize = true;
            this.bFilterListDrivers.BackColor = System.Drawing.SystemColors.Control;
            this.bFilterListDrivers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bFilterListDrivers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bFilterListDrivers.Filter = "";
            this.bFilterListDrivers.Location = new System.Drawing.Point(0, 0);
            this.bFilterListDrivers.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.bFilterListDrivers.Name = "bFilterListDrivers";
            this.bFilterListDrivers.Size = new System.Drawing.Size(225, 367);
            this.bFilterListDrivers.TabIndex = 2;
            this.bFilterListDrivers.SelectedRowChanged += new System.EventHandler<LaRenzo.ControlsLib.BindFilterListControl.SelectedRowChangedEventArgs>(this.bFilterListDrivers_SelectedRowChanged);
            // 
            // buttonDrop
            // 
            this.buttonDrop.Image = global::LaRenzo.Properties.Resources.cross;
            this.buttonDrop.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.buttonDrop.Location = new System.Drawing.Point(172, 38);
            this.buttonDrop.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonDrop.Name = "buttonDrop";
            this.buttonDrop.Size = new System.Drawing.Size(112, 30);
            this.buttonDrop.TabIndex = 6;
            this.buttonDrop.Text = "Сбросить";
            this.buttonDrop.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonDrop.UseVisualStyleBackColor = true;
            this.buttonDrop.Click += new System.EventHandler(this.btnDrop_Click);
            // 
            // txtIdToEdit
            // 
            this.txtIdToEdit.Location = new System.Drawing.Point(19, 74);
            this.txtIdToEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtIdToEdit.Name = "txtIdToEdit";
            this.txtIdToEdit.Size = new System.Drawing.Size(144, 23);
            this.txtIdToEdit.TabIndex = 5;
            this.txtIdToEdit.Visible = false;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(19, 294);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(133, 28);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.Text = "Удалить";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnCatDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(19, 256);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(133, 28);
            this.btnUpdate.TabIndex = 3;
            this.btnUpdate.Text = "Сохранить";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnCatUpdate_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(19, 220);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(133, 28);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Добавить";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnCatAdd_Click);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(17, 22);
            this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(98, 17);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Имя водителя";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(19, 42);
            this.txtName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(144, 23);
            this.txtName.TabIndex = 0;
            // 
            // lblNewPassword
            // 
            this.lblNewPassword.AutoSize = true;
            this.lblNewPassword.Location = new System.Drawing.Point(17, 105);
            this.lblNewPassword.Name = "lblNewPassword";
            this.lblNewPassword.Size = new System.Drawing.Size(100, 17);
            this.lblNewPassword.TabIndex = 7;
            this.lblNewPassword.Text = "Новый пароль";
            // 
            // txtNewPass
            // 
            this.txtNewPass.Location = new System.Drawing.Point(19, 126);
            this.txtNewPass.Name = "txtNewPass";
            this.txtNewPass.PasswordChar = '*';
            this.txtNewPass.Size = new System.Drawing.Size(144, 23);
            this.txtNewPass.TabIndex = 8;
            // 
            // lblRePassword
            // 
            this.lblRePassword.AutoSize = true;
            this.lblRePassword.Location = new System.Drawing.Point(17, 156);
            this.lblRePassword.Name = "lblRePassword";
            this.lblRePassword.Size = new System.Drawing.Size(129, 17);
            this.lblRePassword.TabIndex = 9;
            this.lblRePassword.Text = "Повторить пароль";
            // 
            // txtRePass
            // 
            this.txtRePass.Location = new System.Drawing.Point(20, 177);
            this.txtRePass.Name = "txtRePass";
            this.txtRePass.PasswordChar = '*';
            this.txtRePass.Size = new System.Drawing.Size(143, 23);
            this.txtRePass.TabIndex = 10;
            // 
            // DriversForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(532, 367);
            this.Controls.Add(this.mainSplitControl);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "DriversForm";
            this.Text = "Водители";
            this.Load += new System.EventHandler(this.CategoriesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitControl)).EndInit();
            this.mainSplitControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl mainSplitControl;
        private System.Windows.Forms.Button buttonDrop;
        public System.Windows.Forms.TextBox txtIdToEdit;
        public System.Windows.Forms.Button btnDelete;
        public System.Windows.Forms.Button btnUpdate;
        public System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label lblName;
        public System.Windows.Forms.TextBox txtName;
        private BindFilterList bFilterListDrivers;
        private System.Windows.Forms.Label lblNewPassword;
        private System.Windows.Forms.TextBox txtNewPass;
        private System.Windows.Forms.Label lblRePassword;
        private System.Windows.Forms.TextBox txtRePass;
    }
}