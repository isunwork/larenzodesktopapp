﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.Forms.Controls
{
    public abstract partial class BaseCombo<T> : UserControl where T : IBaseItem
    {
        /// <summary>
        /// Поле мэпинга данных
        /// </summary>
        /// <returns></returns>
        internal abstract string FildName();

        /// <summary> Последний Where запрос </summary>
        internal FilterModel LastFilter;

        protected BaseCombo()
        {
            InitializeComponent();
        }

        /// <summary> Получить список по параметрам </summary>
        internal abstract List<T> GetList(FilterModel filter = null);

        //________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Обновить список </summary>
        ////====================================
        public void InitializeOrRefreshList(FilterModel filter = null)
        {
            LastFilter = filter;

            listComboBox.DataSource = GetList(filter);

            listComboBox.DisplayMember = FildName();
            listComboBox.ValueMember = "ID";

            if (listComboBox.DataSource != null)
            {
                listComboBox.SelectedIndex = -1;
            }
        }

        /// <summary>
        /// Установить выбранное значение по ID
        /// </summary>
        public void SelectValueById(int id)
        {
            listComboBox.SelectedValue = id;
        }

        /// <summary>
        /// Выбранное значение
        /// </summary>
        public T SelectValue
        {
            get
            {
                return (T)listComboBox.SelectedItem;
            }
            set
            {
                if (value != null)
                {
                    foreach (T empType in listComboBox.Items)
                    {
                        if (empType.ID == value.ID)
                        {
                            listComboBox.SelectedItem = empType;
                        }
                    }
                }
            }
        }

        //________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Что-то выбрано в комбаре </summary>
        ////=============================================
        private void ListComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            OnSelectedItem();
        }


        //________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Отобразить форму диалога </summary>
        ////=============================================
        internal void ShowEditDialog(Form editForm)
        {
            editForm.ShowDialog();
            InitializeOrRefreshList(LastFilter);
        }


        public event EventHandler SelectedItem;

        protected virtual void OnSelectedItem()
        {
            EventHandler handler = SelectedItem;
            handler?.Invoke(this, new EventArgs());
        }
    }
}
