﻿namespace LaRenzo.Forms.StaffForms.Controls
{
	partial class DateTimeControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.timeEdit = new DevExpress.XtraEditors.TimeEdit();
			this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
			((System.ComponentModel.ISupportInitialize)(this.timeEdit.Properties)).BeginInit();
			this.SuspendLayout();
			// 
			// timeEdit
			// 
			this.timeEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.timeEdit.EditValue = new System.DateTime(((long)(0)));
			this.timeEdit.Location = new System.Drawing.Point(180, -1);
			this.timeEdit.Name = "timeEdit";
			this.timeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.timeEdit.Size = new System.Drawing.Size(58, 22);
			this.timeEdit.TabIndex = 19;
			this.timeEdit.EditValueChanged += new System.EventHandler(this.timeEdit_EditValueChanged);
			// 
			// dateTimePicker
			// 
			this.dateTimePicker.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dateTimePicker.Location = new System.Drawing.Point(0, -1);
			this.dateTimePicker.Name = "dateTimePicker";
			this.dateTimePicker.Size = new System.Drawing.Size(174, 22);
			this.dateTimePicker.TabIndex = 18;
			// 
			// DateTimeControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.timeEdit);
			this.Controls.Add(this.dateTimePicker);
			this.Name = "DateTimeControl";
			this.Size = new System.Drawing.Size(234, 21);
			((System.ComponentModel.ISupportInitialize)(this.timeEdit.Properties)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraEditors.TimeEdit timeEdit;
		private System.Windows.Forms.DateTimePicker dateTimePicker;
	}
}
