﻿using System.Windows.Forms;

namespace LaRenzo.Forms.Controls
{
	public partial class InputControl : UserControl
	{
		public InputControl()
		{
			InitializeComponent();
		}

		/// <summary> Значение контрола </summary>
		public string Value
		{
			get
			{
				return inputTextBox.Text;
			}
			set
			{
				inputTextBox.Text = value ?? "";
			}
		}

		/// <summary> Значение контрола приведённое к int </summary>
		public int IntValue
		{
			get
			{
				int value;
				int.TryParse(inputTextBox.Text, out value);
				
				return value;
			}
		}

		/// <summary> Значение контрола приведённое к float </summary>
		public float FlotaValue
		{
			get
			{
				float value;
				float.TryParse(inputTextBox.Text, out value);

				return value;
			}
		}

	}
}
