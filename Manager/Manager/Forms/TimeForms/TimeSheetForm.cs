﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Entities.Documents.WebOrders;
using LaRenzo.DataRepository.Entities.Others.TimeSheets;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Sections;
using LaRenzo.DataRepository.Repositories.Documents.Orders;
using LaRenzo.DataRepository.Repositories.Documents.WebOrders;
using LaRenzo.DataRepository.Repositories.Others.Times;
using LaRenzo.Forms.Documents.DeliveryOrderForms;
using NLog;

namespace LaRenzo.Forms.TimeForms
{
    public partial class TimeSheetForm : Form
    {
        private readonly ColorRoller _colorRoller = new ColorRoller();
        private readonly Dictionary<Guid, Color> _colorPairs = new Dictionary<Guid, Color>();
        private List<TimeSheet> _dataSource;
        private int? _selectedPointOfSaleId;
        private int? _selectedBrandId;

        /// <summary> Переменная нужна, чтобы отслеживать выделенный заказ, и уменьшить кол-во запросов к БД </summary>
		private Guid? _selectedGuid;

        public TimeSheetForm()
        {
            InitializeComponent();
            mainGridView.DoubleClick += async (s, e) => await gridView1_DoubleClick(s);
        }

        private void TimeSheetForm_Load(object sender, EventArgs e)
        {
            var points = Content.PointOfSaleRepository.GetItems();
            var brands = Content.BrandManager.GetList();
            if (toolStripComboBox.ComboBox != null)
            {
                toolStripComboBox.ComboBox.DataSource = points;
                toolStripComboBox.ComboBox.DisplayMember = "PointName";
                toolStripComboBox.ComboBox.ValueMember = "Id";
                toolStripComboBox.ComboBox.SelectedValue = points.FirstOrDefault()?.ID;
                toolStripComboBox.ComboBox.SelectedValueChanged += ComboBoxOnSelectedValueChanged;
                _selectedPointOfSaleId = points.FirstOrDefault()?.ID;
            }
            if (toolStripComboBoxBrand.ComboBox != null)
            {
                toolStripComboBoxBrand.ComboBox.DataSource = brands;
                toolStripComboBoxBrand.ComboBox.DisplayMember = "Name";
                toolStripComboBoxBrand.ComboBox.ValueMember = "Id";
                toolStripComboBoxBrand.ComboBox.SelectedValue = brands.FirstOrDefault()?.ID;
                toolStripComboBoxBrand.ComboBox.SelectedValueChanged += ComboBoxBrandOnSelectedValueChanged;
                _selectedBrandId = brands.FirstOrDefault()?.ID;
            }
            BindData();
            mainGridControl.ForceInitialize();
            GoToCurrentTime();
            timerUpdate.Enabled = true;
        }

        private void ComboBoxBrandOnSelectedValueChanged(object sender, EventArgs e)
        {
            _selectedBrandId = (int?)toolStripComboBoxBrand.ComboBox?.SelectedValue;
            BindData();
        }

        private void ComboBoxOnSelectedValueChanged(object sender, EventArgs e)
        {
            _selectedPointOfSaleId = (int?)toolStripComboBox.ComboBox?.SelectedValue;
            BindData();
        }

        #region Form control handlers

        private void timerUpdater_Tick(object sender, EventArgs e)
        {
            BindData();
        }

        private void btnClearTime_Click(object sender, EventArgs e)
        {
            ShowWaitForm("Время очищается");
            Content.TimeSheetManager.SetTimeCells(GetKeyValuePairForSelectedCells(), string.Empty, (int)toolStripComboBox.ComboBox.SelectedValue, (int)toolStripComboBoxBrand.ComboBox.SelectedValue);
            BindData();
            HideWaitForm();
        }

        private void btnBlockTime_Click(object sender, EventArgs e)
        {
            ShowWaitForm("Установка перерыва");
            Content.TimeSheetManager.SetTimeCells(GetKeyValuePairForSelectedCells(), "Blocked", (int)toolStripComboBox.ComboBox.SelectedValue, (int)toolStripComboBoxBrand.ComboBox.SelectedValue);
            BindData();
            HideWaitForm();
        }

        private void ShowWaitForm(string msg)
        {
            splashScreenManagerChangeState.ShowWaitForm();
            splashScreenManagerChangeState.SetWaitFormCaption(msg);
            splashScreenManagerChangeState.SetWaitFormDescription("Подождите...");
        }

        private void HideWaitForm()
        {
            splashScreenManagerChangeState.CloseWaitForm();
        }

        private void btnCurrentTime_Click(object sender, EventArgs e)
        {
            GoToCurrentTime();
        }

        private Guid? GetGuidByCell(int rowHandle, string fieldName)
        {
            Guid? guid = null;

            var time = (DateTime)mainGridView.GetRowCellValue(rowHandle, "Time");

            if (fieldName != "Time")
            {
                var sheet =
                    _dataSource.FirstOrDefault(
                        x => x.Time == time && x.SectionID == int.Parse(fieldName));
                guid = sheet == null ? null : sheet.ReservationGuid;
            }

            return guid;
        }

        private void gridView1_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            Guid? guid = GetGuidByCell(e.RowHandle, e.Column.FieldName);

            var view = sender as GridView;
            if (view == null) return;


            bool isCellSelected = view.IsCellSelected(e.RowHandle, e.Column);
            var color = GetColorForOrder(guid);

            if (e.CellValue as string == new TimeSheetStateMapper().GetRus("Blocked"))
                color = Color.Gainsboro;

            if (e.CellValue as string == new TimeSheetStateMapper().GetRus("Reserved"))
                color = Color.Red;

            if (isCellSelected)
                color = string.IsNullOrWhiteSpace(e.CellValue as string) ? Color.RoyalBlue : ControlPaint.Dark(color);

            if (e.Column.FieldName == "Time")
                color = Color.White;

            e.Appearance.BackColor = color;
        }

        #endregion


        //________________________________________________________________________________________________________________________________________________________________________________
        /// <summary>  </summary>
        private void BindData()
        {
            var index = mainGridView.TopRowIndex;
            var selectedCells = mainGridView.GetSelectedCells();
            try
            {
                _dataSource = Content.TimeSheetManager.GetNoTrackingList();
            }
            catch (Exception e1)
            {
                var logger = LogManager.GetCurrentClassLogger();
                logger.Info("Ошибка при первом получении таблицы времени.");
                try
                {
                    _dataSource = Content.TimeSheetManager.GetNoTrackingList();
                }
                catch (Exception e2)
                {
                    logger.Info("Ошибка при втором получении таблицы времени.");
                    logger.Error(e2);
                    throw;
                }
            }
            _dataSource = _dataSource.Where(x => x.PointOfSaleId == _selectedPointOfSaleId && x.BrandId == _selectedBrandId).ToList();
            mainGridControl.DataSource = Content.TimeSheetManager.GetTimeSheets(_dataSource);

            var colTime = mainGridView.Columns["Time"];
            if (colTime != null)
            {
                colTime.DisplayFormat.FormatType = FormatType.DateTime;
                colTime.DisplayFormat.FormatString = "t";
            }

            var sections = Content.SectionRepository.GetList();
            foreach (var section in sections)
            {
                mainGridView.Columns[section.ID.ToString(CultureInfo.InvariantCulture)].Caption = section.Name;
            }

            mainGridView.FocusedRowHandle = index;
            selectedCells.ToList().ForEach(x => mainGridView.SelectCell(x));
        }


        //________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Получить время из номера строки </summary>
        ////====================================================
        private DateTime GetRowTime(int rowHandle)
        {
            return (DateTime)mainGridView.GetRowCellValue(rowHandle, "Time");
        }


        private List<KeyValuePair<DateTime, int>> GetKeyValuePairForSelectedCells()
        {
            var cells = mainGridView.GetSelectedCells().Where(x => x.Column.FieldName != "Time");

            return cells.Select(gridCell => new KeyValuePair<DateTime, int>(
                    GetRowTime(gridCell.RowHandle),
                    int.Parse(gridCell.Column.FieldName))).
                    ToList();
        }


        private void GoToCurrentTime()
        {
            var tspan = DateTime.Now.TimeOfDay;
            var time = Content.TimeSheetManager.GetNormalizedDateTime(new TimeSpan(tspan.Hours, tspan.Minutes, 0));

            var rowHandle = mainGridView.LocateByValue("Time", time);

            mainGridView.FocusedRowHandle = rowHandle;
            mainGridView.MakeRowVisible(rowHandle, false);

        }

        private void TimeSheetForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            timerUpdate.Enabled = false;
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            var guidList = new List<Guid?>();
            string orderMsg = lbSelectedOrder.Text;
            mainGridView.GetSelectedCells().ToList().ForEach(x => guidList.Add(GetGuidByCell(x.RowHandle, x.Column.FieldName)));

            guidList = guidList.Where(x => x.HasValue).ToList();

            switch (guidList.Distinct().Count())
            {
                case 0:
                    orderMsg = "Заказ не выбран";
                    _selectedGuid = null;
                    break;
                case 1:
                    var gid = guidList[0];
                    if (gid != null)
                    {
                        var guid = gid.Value;
                        if (_selectedGuid != guid)
                        {
                            if (!backgroundWorkerGetGuid.IsBusy)
                            {
                                backgroundWorkerGetGuid.RunWorkerAsync(guid);
                                orderMsg = "Заказ ...";
                                _selectedGuid = guid;
                            }
                            else orderMsg = "Повторите запрос через несколько секунд";
                        }
                    }
                    break;
                default:
                    orderMsg = "Выбрано несколько заказов";
                    _selectedGuid = null;
                    break;
            }

            lbSelectedOrder.Text = orderMsg;

            var selectedRowsWithoutTime = mainGridView.GetSelectedCells().Where(
                    x => x.Column.FieldName != "Time").GroupBy(x => x.RowHandle).Count();

            lblSelectedRowsCount.Text = String.Format("Выделено {0} минут", selectedRowsWithoutTime);

            // Don't update table while selecting something inside
            timerUpdate.Stop();
            timerUpdate.Start();
        }

        private Color GetColorForOrder(Guid? reservationGuid)
        {
            if (reservationGuid == null) return Color.White;

            var guid = (Guid)reservationGuid;

            if (_colorPairs.ContainsKey(guid))
                return _colorPairs[guid];

            var color = _colorRoller.Next();
            _colorPairs.Add(guid, color);
            return color;
        }

        private class ColorRoller
        {
            private readonly List<Color> _colors = new List<Color>
                                                      {
                                                          Color.BurlyWood,
                                                          Color.Teal,
                                                          Color.SkyBlue,
                                                          Color.SlateBlue,
                                                          Color.Lavender


                                                      };

            private int _autoIndex;

            private int AutoIndex
            {
                get
                {
                    var current = _autoIndex;
                    _autoIndex++;
                    if (_autoIndex >= _colors.Count) _autoIndex = 0;

                    return current;
                }
            }

            public Color Next()
            {
                return _colors[AutoIndex];
            }
        }

        private async Task gridView1_DoubleClick(object sender)
        {
            var view = (GridView)sender;

            Point pt = view.GridControl.PointToClient(MousePosition);

            GridHitInfo info = view.CalcHitInfo(pt);
            if (view.IsDataRow(info.RowHandle))
            {
                var guid = GetGuidByCell(info.RowHandle, info.Column.FieldName);
                Order order = null;
                if (guid.HasValue)
                    order =
                        Content.OrderManager.GetByGuid(guid.Value);
                if (order != null)
                {
                    try
                    {
                        var deliveryOrderForm = new DeliveryOrderForm(DetailsFormModes.EditOrder, order.ID);
                        await deliveryOrderForm.Initialize();
                        deliveryOrderForm.Show();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private void backgroundWorkerGetGuid_DoWork(object sender, DoWorkEventArgs e)
        {
            var guid = (Guid)e.Argument;
            var order = Content.OrderManager.GetByGuid(guid);
            e.Result = order != null ? string.Format("Заказ {0}", order.Code) : "Заказ не проведен";
        }

        private void backgroundWorkerGetGuid_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lbSelectedOrder.Text = e.Result.ToString();
        }


    }
}
