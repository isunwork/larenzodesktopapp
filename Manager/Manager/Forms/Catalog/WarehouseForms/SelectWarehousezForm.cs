﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Warehouses;

namespace LaRenzo.Forms.Catalog.WarehouseForms
{
    public partial class SelectWarehousezForm : Form
    {
        public Warehouse Warehouse { set; get; }

        public SelectWarehousezForm()
        {
            InitializeComponent();
            LoadWarehouseList();
        }

        private void LoadWarehouseList()
        {
            mainGridControl.DataSource = Content.WarehouseManager.GetWarehouses();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Warehouse = (Warehouse) mainGridView.GetFocusedRow();
            if (Warehouse == null)
                return;

            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void mainGridView_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }
    }
}