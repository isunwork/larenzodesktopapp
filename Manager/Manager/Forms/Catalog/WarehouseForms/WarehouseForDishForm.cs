﻿using System;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Warehouses;
using LaRenzo.FormsProvider;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.WarehouseForms
{
    public partial class WarehouseForDishForm : Form
    {
        private readonly bool _isEditWindow;

        public WarehouseForDishForm()
        {
            InitializeComponent();
        }
        
        public WarehouseForDishForm(bool isEditWindow, WarehouseForDish warehouse):this()
        {
            _isEditWindow = isEditWindow;
            WindowsFormProvider.FillControlsFromModel(this, warehouse);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFullName.Text))
            {
                var ep = new ErrorProvider();
                ep.SetError(txtFullName, Resources.NameIsEmpty);
                return;
            }

            var warehouse = new WarehouseForDish
            {
                ID = string.IsNullOrEmpty(txtID.Text) ? 0 : Convert.ToInt32(txtID.Text.Trim()),
                Name = txtFullName.Text.Trim()
            };

            if (_isEditWindow)
            {
                Content.WarehouseForDishManager.UpdateWarehouse(warehouse);
            }
            else
            {
                Content.WarehouseForDishManager.AddWarehouse(warehouse);
            }

            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}