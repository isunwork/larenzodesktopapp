﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Catalogs.Tables;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.Tables
{
    public partial class TableListForm : Form
    {
        public TableListForm()
        {
            InitializeComponent();
        }

        #region Private Methods

        private void LoadTableList()
        {
            var first = mainGridView.TopRowIndex;
            var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);
            mainGridControl.DataSource = Content.TableRepository.GetItems();
            mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
            mainGridView.MakeRowVisible(first);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            GetTableForm(false, -1);
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            var selectedRow = (Table) mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetTableForm(false, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.TableIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void GetTableForm(bool isEdit, int tableId)
        {
            var table = Content.TableRepository.GetItem(new TableFilter{TableID = tableId});

            var tableForm = new TableForm(isEdit, table);

            if (tableForm.ShowDialog() == DialogResult.OK)
            {
                LoadTableList();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var selectedRow = (Table) mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetTableForm(true, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.TableIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Resources.DeleteTableQuestion, Resources.WarningTitle, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var selectedRow = (Table) mainGridView.GetFocusedRow();
                if (selectedRow != null)
                {
                    Content.TableRepository.DeleteItem(new Table {ID = selectedRow.ID});
                    LoadTableList();
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadTableList();
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }

        private void mainGridView_DoubleClick(object sender, EventArgs e)
        {
            var view = (GridView) sender;
            
            var pt = view.GridControl.PointToClient(MousePosition);
            
            var info = view.CalcHitInfo(pt);

            if (view.IsDataRow(info.RowHandle))
            {
                var tableId = (int) view.GetRowCellValue(info.RowHandle, columnId);

                GetTableForm(true, tableId);
            }
        }

        private void TableListForm_Load(object sender, EventArgs e)
        {
            LoadTableList();
        }

        #endregion Private Methods

        private void searchButton_Click(object sender, EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }
    }
}