﻿using System;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Tables;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.FormsProvider;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.Tables
{
    public partial class TableForm : Form
    {
        #region Private Field

        private readonly bool _isEditWindow;

        private readonly Table _table;

        #endregion Private Field

        #region Ctor

        public TableForm()
        {
            InitializeComponent();
        }

        public TableForm(bool isEdit, Table table):this()
        {
            _isEditWindow = isEdit;

            _table = table ?? new Table();

            WindowsFormProvider.FillControlsFromModel(this, table);
        }

        #endregion Ctor

        #region Private Method

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!CheckRequireFields())
                return;

            FillEntityProperties();

            if (_isEditWindow)
            {
                Content.TableRepository.UpdateItem(_table);
            }
            else
            {
                Content.TableRepository.InsertItem(_table);
            }

            DialogResult = DialogResult.OK;
        }

        private void FillEntityProperties()
        {
            _table.Name = txtName.Text;
            _table.Comment = txtComment.Text;
        }

        private bool CheckRequireFields()
        {
            if (string.IsNullOrWhiteSpace(txtName.Text))
            {
                SetError(txtName, Resources.NameIsEmpty);
                return false;
            }

            return true;
        }

        private void SetError(Control control, string message)
        {
            var ep = new ErrorProvider();
            ep.SetError(control, message);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        #endregion Private Method
    }
}