﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.Dishes.Controls
{
	public partial class DishCategoryControl : UserControl
	{
		private Brand brand;
		
		public DishCategoryControl()
		{
			InitializeComponent();
		}

		public DishCategoryControl(Brand curentBrand):this()
		{
			brand = curentBrand;
		}

		

        private void LoadDishCategoryList()
        {
            var first = mainGridView.TopRowIndex;
            var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);

			mainGridControl.DataSource = Content.CategoryRepository.GetItems().Where(x=>x.Brand_ID == brand.ID).OrderBy(y => y.Weight);;
            mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
            mainGridView.MakeRowVisible(first);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            GetCategoryForm(false, -1);
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            var selectedRow = (Category) mainGridView.GetFocusedRow();

            if (selectedRow != null)
            {
                GetCategoryForm(false, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.CategoryIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void GetCategoryForm(bool isEdit, int categoryId)
        {
            var category = Content.CategoryRepository.GetItem(categoryId);

            var categoryForm = new DishCategoryForm(isEdit, category ?? new Category(), brand.ID);

            if (categoryForm.ShowDialog() == DialogResult.OK)
            {
                LoadDishCategoryList();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var selectedRow = (Category)mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetCategoryForm(true, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.CategoryIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Resources.DeleteCategoryQuestion, Resources.WarningTitle, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var selectedRow = (Category)mainGridView.GetFocusedRow();

                if (selectedRow != null)
                {
                    Content.CategoryRepository.DeleteItem(new Category { ID = selectedRow.ID });
                    LoadDishCategoryList();
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadDishCategoryList();
        }

        private void mainGridView_DoubleClick(object sender, EventArgs e)
        {
            var view = (GridView) sender;

            var pt = view.GridControl.PointToClient(MousePosition);

            GridHitInfo info = view.CalcHitInfo(pt);
            if (view.IsDataRow(info.RowHandle))
            {
                var categoryId = (int) view.GetRowCellValue(info.RowHandle, columnId);
                GetCategoryForm(true, categoryId);
            }
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }
		
        private void searchButton_Click(object sender, EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }

		private void DishCategoryControl_Load(object sender, EventArgs e)
		{
			LoadDishCategoryList();
		}
	}
}
