﻿namespace LaRenzo.Forms.Catalog.Dishes.Controls
{
	partial class DishList
	{
		/// <summary> 
		/// Требуется переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором компонентов

		/// <summary> 
		/// Обязательный метод для поддержки конструктора - не изменяйте 
		/// содержимое данного метода при помощи редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DishList));
			this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
			this.lstCategory = new DevExpress.XtraEditors.ListBoxControl();
			this.mainGridControl = new DevExpress.XtraGrid.GridControl();
			this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.mainToolstrip = new System.Windows.Forms.ToolStrip();
			this.btnAdd = new System.Windows.Forms.ToolStripButton();
			this.separator = new System.Windows.Forms.ToolStripSeparator();
			this.btnCopy = new System.Windows.Forms.ToolStripButton();
			this.btnEdit = new System.Windows.Forms.ToolStripButton();
			this.btnDelete = new System.Windows.Forms.ToolStripButton();
			this.separator2 = new System.Windows.Forms.ToolStripSeparator();
			this.btnRefresh = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.searchButton = new System.Windows.Forms.ToolStripButton();
			this.btnShowHide = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
			((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
			this.mainSplitContainer.Panel1.SuspendLayout();
			this.mainSplitContainer.Panel2.SuspendLayout();
			this.mainSplitContainer.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.lstCategory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
			this.mainToolstrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// mainSplitContainer
			// 
			this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainSplitContainer.Location = new System.Drawing.Point(0, 31);
			this.mainSplitContainer.Name = "mainSplitContainer";
			// 
			// mainSplitContainer.Panel1
			// 
			this.mainSplitContainer.Panel1.Controls.Add(this.lstCategory);
			// 
			// mainSplitContainer.Panel2
			// 
			this.mainSplitContainer.Panel2.Controls.Add(this.mainGridControl);
			this.mainSplitContainer.Size = new System.Drawing.Size(476, 411);
			this.mainSplitContainer.SplitterDistance = 158;
			this.mainSplitContainer.TabIndex = 11;
			// 
			// lstCategory
			// 
			this.lstCategory.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lstCategory.Location = new System.Drawing.Point(0, 0);
			this.lstCategory.Name = "lstCategory";
			this.lstCategory.Size = new System.Drawing.Size(158, 411);
			this.lstCategory.TabIndex = 1;
			this.lstCategory.ToolTip = "Категории";
			this.lstCategory.Click += new System.EventHandler(this.lstCategory_Click);
			// 
			// mainGridControl
			// 
			this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
			this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainGridControl.Location = new System.Drawing.Point(0, 0);
			this.mainGridControl.MainView = this.mainGridView;
			this.mainGridControl.Name = "mainGridControl";
			this.mainGridControl.Size = new System.Drawing.Size(314, 411);
			this.mainGridControl.TabIndex = 8;
			this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView,
            this.gridView1});
			// 
			// mainGridView
			// 
			this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnId,
            this.columnName});
			this.mainGridView.GridControl = this.mainGridControl;
			this.mainGridView.Name = "mainGridView";
			this.mainGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
			this.mainGridView.OptionsSelection.EnableAppearanceFocusedRow = false;
			this.mainGridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.mainGridView_RowStyle);
			this.mainGridView.DoubleClick += new System.EventHandler(this.mainGridView_DoubleClick);
			// 
			// columnId
			// 
			this.columnId.Caption = "ИД";
			this.columnId.FieldName = "ID";
			this.columnId.Name = "columnId";
			this.columnId.OptionsColumn.AllowEdit = false;
			this.columnId.OptionsColumn.ReadOnly = true;
			this.columnId.Visible = true;
			this.columnId.VisibleIndex = 0;
			this.columnId.Width = 67;
			// 
			// columnName
			// 
			this.columnName.Caption = "Наименование";
			this.columnName.FieldName = "Name";
			this.columnName.Name = "columnName";
			this.columnName.OptionsColumn.AllowEdit = false;
			this.columnName.OptionsColumn.ReadOnly = true;
			this.columnName.Visible = true;
			this.columnName.VisibleIndex = 1;
			this.columnName.Width = 419;
			// 
			// gridView1
			// 
			this.gridView1.GridControl = this.mainGridControl;
			this.gridView1.Name = "gridView1";
			// 
			// mainToolstrip
			// 
			this.mainToolstrip.ImageScalingSize = new System.Drawing.Size(24, 24);
			this.mainToolstrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.separator,
            this.btnCopy,
            this.btnEdit,
            this.btnDelete,
            this.separator2,
            this.btnRefresh,
            this.toolStripSeparator1,
            this.searchButton,
            this.btnShowHide,
            this.toolStripSeparator2,
            this.toolStripButton1});
			this.mainToolstrip.Location = new System.Drawing.Point(0, 0);
			this.mainToolstrip.Name = "mainToolstrip";
			this.mainToolstrip.Size = new System.Drawing.Size(476, 31);
			this.mainToolstrip.TabIndex = 10;
			this.mainToolstrip.Text = "toolStrip1";
			// 
			// btnAdd
			// 
			this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnAdd.Image = global::LaRenzo.Properties.Resources.add_wh;
			this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(28, 28);
			this.btnAdd.Text = "Создать коэфициент";
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// separator
			// 
			this.separator.Name = "separator";
			this.separator.Size = new System.Drawing.Size(6, 31);
			// 
			// btnCopy
			// 
			this.btnCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnCopy.Image = global::LaRenzo.Properties.Resources.copy;
			this.btnCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnCopy.Name = "btnCopy";
			this.btnCopy.Size = new System.Drawing.Size(28, 28);
			this.btnCopy.Text = "Скопировать коэфициент";
			this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
			// 
			// btnEdit
			// 
			this.btnEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnEdit.Image = global::LaRenzo.Properties.Resources.edit_wh;
			this.btnEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnEdit.Name = "btnEdit";
			this.btnEdit.Size = new System.Drawing.Size(28, 28);
			this.btnEdit.Text = "Редактировать коэфициент";
			this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
			// 
			// btnDelete
			// 
			this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnDelete.Image = global::LaRenzo.Properties.Resources.close_wh;
			this.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new System.Drawing.Size(28, 28);
			this.btnDelete.Text = "Пометить на удаление";
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			// 
			// separator2
			// 
			this.separator2.Name = "separator2";
			this.separator2.Size = new System.Drawing.Size(6, 31);
			// 
			// btnRefresh
			// 
			this.btnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnRefresh.Image = global::LaRenzo.Properties.Resources.refresh;
			this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnRefresh.Name = "btnRefresh";
			this.btnRefresh.Size = new System.Drawing.Size(28, 28);
			this.btnRefresh.Text = "Обновить";
			this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
			// 
			// searchButton
			// 
			this.searchButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.searchButton.Image = global::LaRenzo.Properties.Resources.search;
			this.searchButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.searchButton.Name = "searchButton";
			this.searchButton.Size = new System.Drawing.Size(28, 28);
			this.searchButton.Text = "Найти";
			this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
			// 
			// btnShowHide
			// 
			this.btnShowHide.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnShowHide.Image = ((System.Drawing.Image)(resources.GetObject("btnShowHide.Image")));
			this.btnShowHide.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnShowHide.Name = "btnShowHide";
			this.btnShowHide.Size = new System.Drawing.Size(61, 28);
			this.btnShowHide.Text = "Скрытые";
			this.btnShowHide.Click += new System.EventHandler(this.btnShowHide_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
			// 
			// toolStripButton1
			// 
			this.toolStripButton1.Image = global::LaRenzo.Properties.Resources._1427714162_flow_merge_128;
			this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton1.Name = "toolStripButton1";
			this.toolStripButton1.Size = new System.Drawing.Size(177, 28);
			this.toolStripButton1.Text = "Объеденить ингридиенты";
			this.toolStripButton1.ToolTipText = "Сканирует все рецепты и объеденяет повторяющиеся ингридиенты. Количество суммируе" +
    "тся";
			this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
			// 
			// DishList
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.mainSplitContainer);
			this.Controls.Add(this.mainToolstrip);
			this.Name = "DishList";
			this.Size = new System.Drawing.Size(476, 442);
			this.Load += new System.EventHandler(this.DishList_Load);
			this.mainSplitContainer.Panel1.ResumeLayout(false);
			this.mainSplitContainer.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
			this.mainSplitContainer.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.lstCategory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
			this.mainToolstrip.ResumeLayout(false);
			this.mainToolstrip.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.SplitContainer mainSplitContainer;
		private DevExpress.XtraEditors.ListBoxControl lstCategory;
		private DevExpress.XtraGrid.GridControl mainGridControl;
		private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
		private DevExpress.XtraGrid.Columns.GridColumn columnId;
		private DevExpress.XtraGrid.Columns.GridColumn columnName;
		private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
		private System.Windows.Forms.ToolStrip mainToolstrip;
		private System.Windows.Forms.ToolStripButton btnAdd;
		private System.Windows.Forms.ToolStripSeparator separator;
		private System.Windows.Forms.ToolStripButton btnCopy;
		private System.Windows.Forms.ToolStripButton btnEdit;
		private System.Windows.Forms.ToolStripButton btnDelete;
		private System.Windows.Forms.ToolStripSeparator separator2;
		private System.Windows.Forms.ToolStripButton btnRefresh;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripButton searchButton;
		private System.Windows.Forms.ToolStripButton btnShowHide;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripButton toolStripButton1;

	}
}
