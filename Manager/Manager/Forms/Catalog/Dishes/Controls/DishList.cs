﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.FormsProvider;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.Dishes.Controls
{
	public partial class DishList : UserControl
	{
		private Brand brand;
		
		public DishList()
		{
			InitializeComponent();
		}


		public DishList(Brand curentBrand)
		{
			InitializeComponent();

			brand = curentBrand;

		}


		private void LoadEntitiesList()
        {

			// Загрузить список категорий c фильтром по бренду
			var index = LoadCategoryList(Content.CategoryRepository.GetItems().OrderBy(y => y.Weight));

            if (index >= 0)
            {
                lstCategory.SelectedIndex = index;

                var category = (string)lstCategory.Items[index];

                var first = mainGridView.TopRowIndex;
                var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);

                IEnumerable<Dish> list = (category == "Все")
								 ? Content.DishRepository.GetItems(new DishFilter { FilterByIsDeleteMark = btnShowHide.Checked, BrandId = curentBrandId })
								 : Content.DishRepository.GetItems(new DishFilter { FilterByIsDeleteMark = btnShowHide.Checked, FilterByCategoryName = true, CategoryName = category, BrandId = curentBrandId });

                LoadDishList(list.ToList());

                mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
                mainGridView.MakeRowVisible(first);
            }
            else
            {
				LoadDishList(Content.DishRepository.GetItems(new DishFilter { BrandId = curentBrandId }));
            }
        }


		//__________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Id текущего бренда </summary>
		////=======================================
		private int curentBrandId
	    {
		    get
		    {
				return brand.ID;
		    }
	    }

		
		//__________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Загрузить список категорий c фильтром по бренду </summary>
		///=====================================================================
        private int LoadCategoryList(IEnumerable<Category> list)
        {
	        var index = lstCategory.SelectedIndex;

            lstCategory.Items.Clear();

            lstCategory.Items.Add("Все");

            foreach (Category category in list)
            {
	            if (category.Brand != null && category.Brand.ID == curentBrandId)
	            {
		            lstCategory.Items.Add(category.Name);
	            }
            }

            return index;
        }


        private void LoadDishList(IEnumerable<Dish> list)
        {
            mainGridControl.DataSource = list;
        }


		//_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Клик по категории </summary>
		////======================================
        private void lstCategory_Click(object sender, EventArgs e)
        {
            var category = (string)lstCategory.Items[lstCategory.SelectedIndex];

            IEnumerable<Dish> list = (category == "Все")
                                         ? Content.DishRepository.GetItems(new DishFilter
                                         {
                                             FilterByIsDeleteMark = btnShowHide.Checked,
											 BrandId = curentBrandId
                                         })
                                         : Content.DishRepository.GetItems(new DishFilter
                                             {
                                                 FilterByIsDeleteMark = btnShowHide.Checked,
                                                 FilterByCategoryName = true,
												 CategoryName = category,
												 BrandId = curentBrandId});

            LoadDishList(list.ToList());
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            GetDishForm(false, -1);
        }

        private void btnCopy_Click(object sender, EventArgs e)
		{
            var selectedValue = (Dish) mainGridView.GetFocusedRow();

            if (selectedValue != null)
            {
                GetDishForm(false, selectedValue.ID);
            }
            else
            {
                MessageBox.Show(Resources.DishIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void GetDishForm(bool isEdit, int id)
        {
            var dish = Content.DishRepository.GetItem(new DishFilter
            {
	            FilterByDishId = true, 
				DishId = id,
				BrandId = curentBrandId
            });

            var dishForm = new DishForm(isEdit, dish ?? new Dish {ItemsOfDishSets = new BindingList<ItemOfDishSet>(), IdBrand = curentBrandId});

            if (dishForm.ShowDialog() == DialogResult.OK)
            {
                var category = (string)lstCategory.Items[lstCategory.SelectedIndex];

                IEnumerable<Dish> list = (category == "Все")? Content.DishRepository.GetItems(new DishFilter
                                                 {
                                                     FilterByIsDeleteMark = btnShowHide.Checked,
                                                 })
                                             : Content.DishRepository.GetItems(new DishFilter
                                                 {
                                                     FilterByIsDeleteMark = btnShowHide.Checked,
                                                     FilterByCategoryName = true,
                                                     CategoryName = category
                                                 });

                LoadEntitiesList();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var selectedValue = (Dish) mainGridView.GetFocusedRow();
            if (selectedValue != null)
            {
                GetDishForm(true, selectedValue.ID);
            }
            else
            {
                MessageBox.Show(Resources.DishIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                               MessageBoxIcon.Warning);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadEntitiesList();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Resources.DeleteDishQuestion, Resources.WarningTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                var selectedValue = (Dish) mainGridView.GetFocusedRow();
                if (selectedValue != null)
                {
                    try
                    {
                        Content.DishRepository.DeleteItem(selectedValue);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(string.Format(Resources.DeleteDishError, ex.Message), Resources.WarningTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }

            LoadEntitiesList();
        }

        private void mainGridView_DoubleClick(object sender, EventArgs e)
        {
            var selectedValue = (Dish) mainGridView.GetFocusedRow();
            if (selectedValue != null)
            {
                GetDishForm(true, selectedValue.ID);
            }
            else
            {
                MessageBox.Show(Resources.DishIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            var currentRow = (Dish)view.GetRow(e.RowHandle);

            if (currentRow.IsDeleted)
                e.Appearance.BackColor = Color.Salmon;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }
		
        private void searchButton_Click(object sender, EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }

        private void btnShowHide_Click(object sender, EventArgs e)
        {
            btnShowHide.Checked = !btnShowHide.Checked;
            LoadEntitiesList();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<CollapseIngridinetForm>.ActivateForm(() => new CollapseIngridinetForm());
        }

		private void DishList_Load(object sender, EventArgs e)
		{
			LoadEntitiesList();
		}
	}
}
