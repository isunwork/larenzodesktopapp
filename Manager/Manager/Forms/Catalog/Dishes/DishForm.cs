﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Entities.Catalogs.Sections;
using LaRenzo.DataRepository.Entities.Catalogs.Sets;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Entities.Others.AmountFactories;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Cutlery;
using LaRenzo.DataRepository.Repositories.Catalogs.Measures;
using LaRenzo.DataRepository.Repositories.Catalogs.Sets;
using LaRenzo.DataRepository.Repositories.Catalogs.Warehouses;
using LaRenzo.DataRepository.Repositories.Others.AmountFactories;
using LaRenzo.Forms.Catalog.ProductForms;
using LaRenzo.Forms.Catalog.SetsForms;
using LaRenzo.FormsProvider;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.Dishes
{
    public partial class DishForm : Form
    {
        #region Private Fields

        private readonly bool _isEditWindow;

        private readonly List<ItemOfDishSet> _itemOfProductSets;

        private readonly Dish _dish;
        private List<AdditionalOfferDish> _additionalOfferDishes;

        #endregion Private Fields

        #region Ctor

        public DishForm()
        {
            InitializeComponent();
        }

        public DishForm(bool isEdit, Dish dish)
            : this()
        {

            _dish = dish ?? new Dish();

            _isEditWindow = isEdit;

            SetControlsParams();

            WindowsFormProvider.FillControlsFromModel(this, dish);

            _itemOfProductSets = dish?.ItemsOfDishSets?.ToList();

            mainGridControl.DataSource = _itemOfProductSets;
            Text += $" {_dish.Name}";
        }

        #endregion Ctor

        #region Private Methods

        private void SetControlsParams()
        {
            cmbCutlery.DataSource = Content.CutleryRepository.GetCutleryList();
            cmbCutlery.DisplayMember = "Name";
            cmbCutlery.ValueMember = "ID";

            cmbCutleryCoefficient.DataSource = Content.SetManager.GetSets();
            cmbCutleryCoefficient.DisplayMember = "Name";
            cmbCutleryCoefficient.ValueMember = "ID";

            var categories = Content.CategoryRepository.GetItems().OrderBy(y => y.Weight).ToList();
            cmbDishCat.DataSource = categories;
            cmbDishCat.DisplayMember = "Name";
            cmbDishCat.ValueMember = "ID";

            comboBoxSection.DataSource = Content.SectionRepository.GetItems().OrderBy(y => y.Name).ToList();
            comboBoxSection.DisplayMember = "Name";
            comboBoxSection.ValueMember = "ID";

            var amountFactorCollections = Content.AmountFactorManager.GetList();
            amountFactorCollections.Add(new AmountFactorCollection { Name = string.Empty });

            cmbAFCollections.DataSource = amountFactorCollections.OrderBy(x => x.Name).ToList();
            cmbAFCollections.DisplayMember = "Name";
            cmbAFCollections.ValueMember = "ID";

            cmbWarehouse.DataSource = Content.WarehouseForDishManager.GetWarehouses();
            cmbWarehouse.DisplayMember = "Name";
            cmbWarehouse.ValueMember = "ID";

            listBoxCategory.DisplayMember = "Name";
            FillCategoryList(categories);

            listBoxDish.DisplayMember = "Name";
            FillDishList();

            listBoxSelected.DisplayMember = "Name";
            FillAdditionalOfferList();
        }

        private void FillCategoryList(List<Category> categories)
        {
            listBoxCategory.Items.Clear();
            foreach (var category in categories)
            {
                listBoxCategory.Items.Add(category);
            }
        }

        private void FillDishList()
        {
            var dishes = Content.DishRepository.GetItems(null).Where(x => x.IsDeleted == false).OrderBy(x => x.Name).ToList();

            listBoxDish.Items.Clear();
            if (listBoxCategory.SelectedIndex > 0)
            {
                var category = (Category)listBoxCategory.SelectedItem;
                dishes = dishes.Where(x => x.CategoryID == category.ID).ToList();
            }

            foreach (var dish in dishes)
            {
                listBoxDish.Items.Add(dish);
            }
        }

        private void FillAdditionalOfferList()
        {
            if (_dish == null || _dish.ID <= 0)
                return;

            _additionalOfferDishes = Content.AdditionalOfferDishRepository.GetItems(_dish.ID).OrderBy(x => x.Dish.Name).ToList();

            listBoxSelected.Items.Clear();

            foreach (var item in _additionalOfferDishes)
            {
                listBoxSelected.Items.Add(item);
            }

        }
        private void chkCalculateCoefficient_CheckedChanged(object sender, EventArgs e)
        {
            cmbCutleryCoefficient.Enabled = chkCalculateCoefficient.Checked;
            btnSelectCoefficient.Enabled = chkCalculateCoefficient.Checked;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            if (!CheckRequireFields())
                return;

            FillEntityProperties();

            if (_isEditWindow)
            {
                Content.DishRepository.UpdateItem(_dish);
            }
            else
            {
                Content.DishRepository.InsertItem(_dish);
            }
            SaveAdditionalOfferDishes();
            DialogResult = DialogResult.OK;
        }

        private void SaveAdditionalOfferDishes()
        {
            var itemsToDelete = _additionalOfferDishes.ToList();
            foreach (var selected in listBoxSelected.Items)
            {
                var item = (AdditionalOfferDish)selected;
                if (item.ID > 0)
                    itemsToDelete.Remove(item);
                item.MainDishId = _dish.ID;
                Content.AdditionalOfferDishRepository.InsertOrUpdate(item);
            }
            foreach(var item in itemsToDelete)
            {
                Content.AdditionalOfferDishRepository.DeleteItem(item);
            }
        }

        private void FillEntityProperties()
        {
            _dish.Name = txtName.Text;
            _dish.DiscountAppliсability = checkBoxDiscountApplicability.Checked;
            _dish.InternalName = txtInternalName.Text;
            var warehouseid = Convert.ToInt32(cmbWarehouse.SelectedValue) <= 0
                                  ? (int?)null
                                  : Convert.ToInt32(cmbWarehouse.SelectedValue);
            _dish.WarehouseForDishId = warehouseid;

            if(_isEditWindow)
                _dish.WarehouseForDish = (WarehouseForDish)cmbWarehouse.SelectedItem;
            
            _dish.UseCustomWarehouse = chkCustomWarehouse.Checked;
            _dish.Structure = txtStructure.Text;
            _dish.CafePrice = Convert.ToInt32(txtCafePrice.Text);
            var startTemperature = Convert.ToInt32(txtStartTemperature.Text);
            _dish.StartTemperature = startTemperature;
            var sku = Convert.ToInt32(txtSku.Text);
            _dish.SKU = sku;
            _dish.AddressProduction = txtAddressProduction.Text;
            _dish.CalculateByCoefficient = chkCalculateCoefficient.Checked;
            _dish.IsAlcohole = chkAlcohol.Checked;
            var carbohydrates = Convert.ToInt32(txtCarbohydrates.Text);
            _dish.Carbohydrates = carbohydrates;
            var categoryId = Convert.ToInt32(cmbDishCat.SelectedValue) <= 0
                                 ? 0
                                 : Convert.ToInt32(cmbDishCat.SelectedValue);
            _dish.CategoryID = categoryId;

            if (_isEditWindow)
                _dish.Category = (Category)cmbDishCat.SelectedItem;
            
            var sectionId = Convert.ToInt32(comboBoxSection.SelectedValue) <= 0
                ? 0
                : Convert.ToInt32(comboBoxSection.SelectedValue);
            _dish.SectionID = sectionId;

            if (_isEditWindow)
                _dish.Section = (Section)comboBoxSection.SelectedItem;


            _dish.ChefExpertCode = txtExternalCode.Text;
            var coeffEnergy = Convert.ToInt32(txtCoeffEnergy.Text);
            _dish.CoeffEnergy = coeffEnergy;
            var coefficeientId = Convert.ToInt32(cmbCutleryCoefficient.SelectedValue) <= 0
                                     ? (int?)null
                                     : Convert.ToInt32(cmbCutleryCoefficient.SelectedValue);
            _dish.CoefficientId = coefficeientId;
           
            if (_isEditWindow)
                _dish.Set = (Set)cmbCutleryCoefficient.SelectedItem;
            
            _dish.CookingTime = Convert.ToInt32(txtCook.Text);
            _dish.Cutlery = Convert.ToInt32(cmbCutlery.SelectedValue) <= 0
                                ? 0
                                : Convert.ToInt32(cmbCutlery.SelectedValue);
            _dish.DoCountWhenDeliveryCostCalculated = chckDelivery.Checked;
            _dish.EndTemperature = Convert.ToInt32(txtEndTemperature.Text);
            _dish.FactorCollectionID = Convert.ToInt32(cmbAFCollections.SelectedValue) <= 0
                                           ? (int?)null
                                           : Convert.ToInt32(cmbAFCollections.SelectedValue);

            if (_isEditWindow)
                _dish.FactorCollection = (AmountFactorCollection)cmbAFCollections.SelectedItem;
            
            _dish.Fat = Convert.ToInt32(txtFat.Text);
            _dish.FitHour = Convert.ToInt32(txtFitHour.Text);
            _dish.Manufacturer = txtManufactured.Text;
            _dish.MarkEAC = chkMarkEAC.Checked;
            _dish.MarkGOST = chkMarkGOST.Checked;
            _dish.Price = Convert.ToInt32(txtPrice.Text);
            _dish.Netto = Convert.ToInt32(txtNetto.Text);
            _dish.Protein = Convert.ToInt32(txtProtein.Text);
            _dish.CTO = txtCTO.Text;
            _dish.Barcode = txtBarcode.Text;
	        _dish.Brand = Content.BrandManager.GetById(_dish.IdBrand)??Content.BrandManager.SelectedBrand;
            _dish.ItemsOfDishSets = _itemOfProductSets;
        }

        private bool CheckRequireFields()
        {
            if (string.IsNullOrWhiteSpace(txtName.Text))
            {
                SetError(txtName, Resources.NameIsEmpty);
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtInternalName.Text))
            {
                SetError(txtInternalName, Resources.ClientNameIsEmpty);
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtSku.Text))
            {
                SetError(txtSku, Resources.SKUIsEmpty);
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtCook.Text))
            {
                SetError(txtCook, Resources.CookTimeIsEmpty);
                return false;
            }

            if (cmbDishCat.SelectedValue == null)
            {
                SetError(cmbDishCat, Resources.DishCatIsEmpty);
                return false;
            }

            if (comboBoxSection.SelectedValue == null)
            {
                SetError(comboBoxSection, "Не выбран цех блюда");
                return false;
            }

            return true;
        }

        private void SetError(Control control, string message)
        {
            var ep = new ErrorProvider();
            ep.SetError(control, message);
        }

        private void AddIngridientButton_Click(object sender, EventArgs e)
        {
            mainGridControl.BeginUpdate();

            var selectProductForm = new SelectProductForm();

			if (selectProductForm.ShowDialog() == DialogResult.OK && selectProductForm.ProductsList != null && selectProductForm.ProductsList.Count > 0)
            {
				foreach (Product product in selectProductForm.ProductsList)
	            {
		            _itemOfProductSets.Add(new ItemOfDishSet
					{
						DishId = string.IsNullOrEmpty(txtId.Text) ? 0 : Convert.ToInt32(txtId.Text),
						MeasureId = product.MeasureID,
						ProductId = product.ID,
						Measure = Content.MeasureManager.GetMeasure(product.MeasureID),
						Product = product
					});
	            }
            }
            mainGridControl.DataSource = _itemOfProductSets;

            mainGridControl.EndUpdate();
        }

        private void DeleteIngridientButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Resources.DeleteIngridientFromList, Resources.WarningTitle, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                mainGridControl.BeginUpdate();

                var selectedRow = (ItemOfDishSet)mainGridView.GetFocusedRow();
                if (selectedRow != null)
                {
                    _itemOfProductSets.Remove(selectedRow);
                }

                mainGridControl.EndUpdate();
            }
        }

        private void chkCustomWarehouse_CheckedChanged(object sender, EventArgs e)
        {
            cmbWarehouse.Enabled = chkCustomWarehouse.Checked;
        }

        private void btnSelectCoefficient_Click(object sender, EventArgs e)
        {
            var selectCoefficientForm = new SelectSetForm();

            if (selectCoefficientForm.ShowDialog() == DialogResult.OK)
                cmbCutleryCoefficient.SelectedValue = selectCoefficientForm.Set.ID;
        }

        private void CheckAvailableChar(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar) || '-'.Equals(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        #endregion Private Methods

        private void tabTopDishes_Enter(object sender, EventArgs e)
        {
            if (gridTopDishes.DataSource == null && _dish != null)
            {
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormCaption("Подготовка данных...");
                splashScreenManager.SetWaitFormDescription("Подождите немного");
                
                gridTopDishes.DataSource = Content.DishRepository.GetTopList(_dish.ID); 
                
                splashScreenManager.CloseWaitForm();

            }
            
        }

        private void listBoxCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillDishList();
        }

        private void listBoxDish_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = listBoxDish.IndexFromPoint(e.Location);
            if (index == ListBox.NoMatches)
                return;

            var dish = (Dish)listBoxDish.Items[index];
            foreach(var item in listBoxSelected.Items)
            {
                if (((AdditionalOfferDish)item).DishId == dish.ID)
                    return;
            }

            listBoxSelected.Items.Add(new AdditionalOfferDish {  DishId = dish.ID, Name = dish.Name });            
        }

        private void listBoxSelected_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = listBoxSelected.IndexFromPoint(e.Location);
            if (index != ListBox.NoMatches)
            {
                var item = listBoxSelected.Items[index];
                listBoxSelected.Items.Remove(item);
            }
        }
    }
}