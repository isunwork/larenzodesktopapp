﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.Dishes
{
    public partial class SelectDishForm : Form
    {
        #region Public Properties

        public Dish Dish { set; get; }

        #endregion Public Properties
        
        #region Ctor

        public SelectDishForm()
        {
            InitializeComponent();
        }
        
        #endregion Ctor

        #region Private Methods

        private void LoadEntitiesList()
        {
            var index = LoadCategoryList(Content.CategoryRepository.GetItems().OrderBy(y => y.Weight));

            if (index >= 0)
            {
                lstCategory.SelectedIndex = index;

                var category = (string)lstCategory.Items[index];

                var first = mainGridView.TopRowIndex;
                var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);

                IEnumerable<Dish> list = (category == "Все")
                                 ? Content.DishRepository.GetItems(null)
                                 : Content.DishRepository.GetItems(new DishFilter
                                 {
                                     FilterByCategoryName = true,
                                     CategoryName = category
                                 });

                LoadDishList(list.ToList());

                mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
                mainGridView.MakeRowVisible(first);
            }
            else
            {
                LoadDishList(Content.DishRepository.GetItems(null));
            }
        }

        private int LoadCategoryList(IEnumerable<Category> list)
        {
            var index = lstCategory.SelectedIndex;

            lstCategory.Items.Clear();

            lstCategory.Items.Add("Все");
            foreach (Category category in list)
            {
                lstCategory.Items.Add(category.Name);
            }

            return index;
        }

        private void LoadDishList(IEnumerable<Dish> list)
        {
            mainGridControl.DataSource = list;
        }

        private void lstCategory_Click(object sender, EventArgs e)
        {
            var category = (string)lstCategory.Items[lstCategory.SelectedIndex];

            IEnumerable<Dish> list = (category == "Все")
                                         ? Content.DishRepository.GetItems(null)
                                         : Content.DishRepository.GetItems(new DishFilter
                                             {
                                                 FilterByCategoryName = true,
                                                 CategoryName = category
                                             });

            LoadDishList(list.ToList());
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var dishForm = new DishForm();
            if (dishForm.ShowDialog() == DialogResult.OK)
            {
                if (lstCategory.SelectedIndex != -1)
                {
                    var category = (string)lstCategory.Items[lstCategory.SelectedIndex];

                    IEnumerable<Dish> list = (category == "Все")
                                                 ? Content.DishRepository.GetItems(null)
                                                 : Content.DishRepository.GetItems(new DishFilter
                                                     {
                                                         FilterByCategoryName = true,
                                                         CategoryName = category
                                                     });

                    LoadDishList(list.ToList());
                }
            }
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            var selectedValue = (Dish) mainGridView.GetFocusedRow();
            if (selectedValue != null)
            {
                GetDishForm(false, selectedValue.ID);
            }
            else
            {
                MessageBox.Show(Resources.DishIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void GetDishForm(bool isEdit, int id)
        {
            var dish = Content.DishRepository.GetItem(new DishFilter { FilterByDishId = true, DishId = id });
            var dishForm = new DishForm(isEdit, dish);
                
            if (dishForm.ShowDialog() == DialogResult.OK)
            {
                var category = (string)lstCategory.Items[lstCategory.SelectedIndex];

                IEnumerable<Dish> list = (category == "Все")
                                             ? Content.DishRepository.GetItems(null)
                                             : Content.DishRepository.GetItems(new DishFilter
                                                 {
                                                     FilterByCategoryName = true,
                                                     CategoryName = category
                                                 });

                LoadDishList(list.ToList());
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var selectedValue = (Dish) mainGridView.GetFocusedRow();
            if (selectedValue != null)
            {
                GetDishForm(true, selectedValue.ID);
            }
            else
            {
                MessageBox.Show(Resources.DishIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                               MessageBoxIcon.Warning);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
           LoadEntitiesList();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Resources.DeleteDishQuestion, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                var selectedValue = (Dish) mainGridView.GetFocusedRow();
                if (selectedValue != null)
                {
                    try
                    {
                        Content.DishRepository.DeleteItem(selectedValue);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(string.Format(Resources.DeleteDishError, ex.Message),
                                        Resources.WarningTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }

            LoadEntitiesList();
        }

        private void mainGridView_DoubleClick(object sender, EventArgs e)
        {
            var selectedValue = (Dish) mainGridView.GetFocusedRow();
            if (selectedValue != null)
            {
                GetDishForm(true, selectedValue.ID);
            }
            else
            {
                MessageBox.Show(Resources.DishIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                               MessageBoxIcon.Warning);
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            var selectedValue = (Dish)mainGridView.GetFocusedRow();
            if (selectedValue != null)
            {
                Dish = selectedValue;
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show(Resources.DishIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void SelectDishForm_Load(object sender, EventArgs e)
        {
            LoadEntitiesList();
        }

        #endregion Private Methods

        private void searchButton_Click(object sender, EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }
    }
}