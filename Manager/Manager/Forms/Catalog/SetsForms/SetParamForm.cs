﻿using System;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Sets;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Sets;
using LaRenzo.FormsProvider;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.SetsForms
{
    public partial class SetParamForm : Form
    {
        private Set _set;

        private Set Set
        {
            set 
            { 
                _set = value;
                txtCoefficient.Text = value.Name;
            }
            get { return _set; }
        }

        private readonly bool _isEditWindow;

        public SetParamForm()
        {
            InitializeComponent();

            cmbRoundedValue.SelectedIndex = 0;
        }

        public SetParamForm(bool isEdit, SetParams setParams)
            : this()
        {
            _isEditWindow = isEdit;

            WindowsFormProvider.FillControlsFromModel(this, setParams);
            
            numValue.Value = setParams.DishValue;
            numMinValue.Value = setParams.MinValue;
            numMaxValue.Value = setParams.MaxValue;
            numIncrement.Value = setParams.IncrementValue;
            
            Set = Content.SetManager.GetSet(setParams.SetID);

            cmbRoundedValue.SelectedIndex = setParams.RoundDirection;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (Set == null)
            {
                var ep = new ErrorProvider();
                ep.SetError(btnSelectCoefficient, Resources.CoefficientIsNotSelected);
                return;
            }

            if (numMinValue.Value > numMaxValue.Value || numMinValue.Value == 0 || numMaxValue.Value == 0)
            {
                var ep = new ErrorProvider();
                ep.SetError(numMaxValue, Resources.IntervalValueIsNotValid);
                return;
            }

            var coefficientParam = new SetParams
                {
                    ID = string.IsNullOrEmpty(txtId.Text) ? 0: Convert.ToInt32(txtId.Text.Trim()),
                    SetID = Set.ID,
                    DishValue = Convert.ToInt32(numValue.Value),
                    MinValue = Convert.ToInt32(numMinValue.Value),
                    MaxValue = Convert.ToInt32(numMaxValue.Value),
                    IncrementValue = Convert.ToInt32(numIncrement.Value),
                    IsIncremental = chkIsIncrement.Checked,
                    Round = chkRounded.Checked,
                    RoundDirection = cmbRoundedValue.SelectedIndex
                };

            if (_isEditWindow)
            {
                Content.SetParamsManager.UpdateSetParams(coefficientParam);
            }
            else
            {
                Content.SetParamsManager.AddSetParams(coefficientParam);
            }

            DialogResult = DialogResult.OK;
        }

        private void btnSelectCoefficient_Click(object sender, EventArgs e)
        {
            var selectCoefficientForm = new SelectSetForm();

            if (selectCoefficientForm.ShowDialog() == DialogResult.OK)
            {
                Set = selectCoefficientForm.Set;
            }
        }

        private void chkRounded_CheckedChanged(object sender, EventArgs e)
        {
            cmbRoundedValue.Enabled = chkRounded.Checked;
        }
    }
}