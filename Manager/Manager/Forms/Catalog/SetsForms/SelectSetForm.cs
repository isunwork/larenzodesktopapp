﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Catalogs.Sets;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Sets;

namespace LaRenzo.Forms.Catalog.SetsForms
{
    public partial class SelectSetForm : Form
    {
        public Set Set { set; get; }

        public SelectSetForm()
        {
            InitializeComponent();
            LoadSetList();
        }

        private void LoadSetList()
        {
            mainGridControl.DataSource = Content.SetManager.GetSets();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Set = (Set) mainGridView.GetFocusedRow();
            if (Set == null)
                return;
            
            DialogResult = DialogResult.OK;
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }
    }
}