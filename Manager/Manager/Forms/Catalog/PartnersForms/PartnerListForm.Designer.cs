﻿namespace LaRenzo.Forms.Catalog.PartnersForms
{
    partial class PartnerListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

			DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
			this.productGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.subColumnId = new DevExpress.XtraGrid.Columns.GridColumn();
			this.subColumnName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.subColumnIsSet = new DevExpress.XtraGrid.Columns.GridColumn();
			this.subColumnMeasure = new DevExpress.XtraGrid.Columns.GridColumn();
			this.subColumnMinAmount = new DevExpress.XtraGrid.Columns.GridColumn();
			this.mainGridControl = new DevExpress.XtraGrid.GridControl();
			this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.idColumn = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.dayOrderColumn = new DevExpress.XtraGrid.Columns.GridColumn();
			this.contactFaceColumn = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnPhone = new DevExpress.XtraGrid.Columns.GridColumn();
			this.productsColumn = new DevExpress.XtraGrid.Columns.GridColumn();
			this.productsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.nameGridColumn = new DevExpress.XtraGrid.Columns.GridColumn();
			this.mainToolstrip = new System.Windows.Forms.ToolStrip();
			this.btnAdd = new System.Windows.Forms.ToolStripButton();
			this.separator = new System.Windows.Forms.ToolStripSeparator();
			this.btnCopy = new System.Windows.Forms.ToolStripButton();
			this.btnEdit = new System.Windows.Forms.ToolStripButton();
			this.btnDelete = new System.Windows.Forms.ToolStripButton();
			this.separator2 = new System.Windows.Forms.ToolStripSeparator();
			this.btnRefresh = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.searchButton = new System.Windows.Forms.ToolStripButton();
			((System.ComponentModel.ISupportInitialize)(this.productGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.productsGridView)).BeginInit();
			this.mainToolstrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// productGridView
			// 
			this.productGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.subColumnId,
            this.subColumnName,
            this.subColumnIsSet,
            this.subColumnMeasure,
            this.subColumnMinAmount});
            this.productGridView.GridControl = this.mainGridControl;
            this.productGridView.Name = "productGridView";
            // 
            // subColumnId
            // 
            this.subColumnId.Caption = "ИД";
            this.subColumnId.FieldName = "Product.ID";
            this.subColumnId.Name = "subColumnId";
            this.subColumnId.OptionsColumn.AllowEdit = false;
            this.subColumnId.OptionsColumn.ReadOnly = true;
            this.subColumnId.Visible = true;
            this.subColumnId.VisibleIndex = 0;
            // 
            // subColumnName
            // 
            this.subColumnName.Caption = "Ниаменование";
            this.subColumnName.FieldName = "Product.Name";
            this.subColumnName.Name = "subColumnName";
            this.subColumnName.OptionsColumn.AllowEdit = false;
            this.subColumnName.OptionsColumn.ReadOnly = true;
            this.subColumnName.Visible = true;
            this.subColumnName.VisibleIndex = 1;
            // 
            // subColumnIsSet
            // 
            this.subColumnIsSet.Caption = "Составной";
            this.subColumnIsSet.FieldName = "Product.IsSetProduct";
            this.subColumnIsSet.Name = "subColumnIsSet";
            this.subColumnIsSet.OptionsColumn.AllowEdit = false;
            this.subColumnIsSet.OptionsColumn.ReadOnly = true;
            this.subColumnIsSet.Visible = true;
            this.subColumnIsSet.VisibleIndex = 2;
            // 
            // subColumnMeasure
            // 
            this.subColumnMeasure.Caption = "Ед.Изм";
            this.subColumnMeasure.FieldName = "Product.Measure.ShortName";
            this.subColumnMeasure.Name = "subColumnMeasure";
            this.subColumnMeasure.OptionsColumn.AllowEdit = false;
            this.subColumnMeasure.OptionsColumn.ReadOnly = true;
            this.subColumnMeasure.Visible = true;
            this.subColumnMeasure.VisibleIndex = 3;
            // 
            // subColumnMinAmount
            // 
            this.subColumnMinAmount.Caption = "Мин. остаток";
            this.subColumnMinAmount.FieldName = "Product.MinAmount";
            this.subColumnMinAmount.Name = "subColumnMinAmount";
            this.subColumnMinAmount.OptionsColumn.AllowEdit = false;
            this.subColumnMinAmount.OptionsColumn.ReadOnly = true;
            this.subColumnMinAmount.Visible = true;
            this.subColumnMinAmount.VisibleIndex = 4;
            // 
            // mainGridControl
            // 
            this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.productGridView;
            gridLevelNode1.RelationName = "Products";
            this.mainGridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.mainGridControl.Location = new System.Drawing.Point(0, 31);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(710, 424);
            this.mainGridControl.TabIndex = 4;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView,
            this.productsGridView,
            this.productGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.idColumn,
            this.columnName,
            this.dayOrderColumn,
            this.contactFaceColumn,
            this.columnPhone,
            this.productsColumn});
			this.mainGridView.GridControl = this.mainGridControl;
			this.mainGridView.Name = "mainGridView";
			this.mainGridView.OptionsFind.AlwaysVisible = true;
			this.mainGridView.OptionsFind.FindNullPrompt = "Введите текст для поиска...";
			this.mainGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
			this.mainGridView.OptionsSelection.EnableAppearanceFocusedRow = false;
			this.mainGridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.mainGridView_RowStyle);
			this.mainGridView.DoubleClick += new System.EventHandler(this.MainGridViewDoubleClick);
			// 
			// idColumn
			// 
			this.idColumn.Caption = "Идентификатор";
			this.idColumn.FieldName = "Partner1.ID";
			this.idColumn.Name = "idColumn";
			this.idColumn.OptionsColumn.AllowEdit = false;
			this.idColumn.OptionsColumn.ReadOnly = true;
			this.idColumn.Visible = true;
			this.idColumn.VisibleIndex = 0;
			this.idColumn.Width = 105;
			// 
			// columnName
			// 
			this.columnName.Caption = "Организация";
			this.columnName.FieldName = "Partner1.PartnerName";
			this.columnName.Name = "columnName";
			this.columnName.OptionsColumn.AllowEdit = false;
			this.columnName.OptionsColumn.ReadOnly = true;
			this.columnName.Visible = true;
			this.columnName.VisibleIndex = 1;
			this.columnName.Width = 228;
			// 
			// dayOrderColumn
			// 
			this.dayOrderColumn.Caption = "Примечание по дню заказа";
			this.dayOrderColumn.FieldName = "Partner1.PartnerRemarkDayOrder";
			this.dayOrderColumn.Name = "dayOrderColumn";
			this.dayOrderColumn.OptionsColumn.AllowEdit = false;
			this.dayOrderColumn.OptionsColumn.ReadOnly = true;
			this.dayOrderColumn.Visible = true;
			this.dayOrderColumn.VisibleIndex = 2;
			this.dayOrderColumn.Width = 170;
			// 
			// contactFaceColumn
			// 
			this.contactFaceColumn.Caption = "Контакт.лицо";
			this.contactFaceColumn.FieldName = "Partner1.PartnerContactFace";
			this.contactFaceColumn.Name = "contactFaceColumn";
			this.contactFaceColumn.OptionsColumn.AllowEdit = false;
			this.contactFaceColumn.OptionsColumn.ReadOnly = true;
			this.contactFaceColumn.Visible = true;
			this.contactFaceColumn.VisibleIndex = 3;
			this.contactFaceColumn.Width = 101;
			// 
			// columnPhone
			// 
			this.columnPhone.Caption = "Телефон";
			this.columnPhone.FieldName = "Partner1.PartnerPhone";
			this.columnPhone.Name = "columnPhone";
			this.columnPhone.OptionsColumn.AllowEdit = false;
			this.columnPhone.OptionsColumn.ReadOnly = true;
			this.columnPhone.Visible = true;
			this.columnPhone.VisibleIndex = 4;
			this.columnPhone.Width = 130;
			// 
			// productsColumn
			// 
			this.productsColumn.Caption = "Наименование";
			this.productsColumn.FieldName = "AllProductsNames";
			this.productsColumn.Name = "productsColumn";
			this.productsColumn.OptionsColumn.AllowEdit = false;
			this.productsColumn.OptionsColumn.ReadOnly = true;
			this.productsColumn.Visible = true;
			this.productsColumn.VisibleIndex = 5;
			this.productsColumn.Width = 195;
			// 
			// productsGridView
			// 
			this.productsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.nameGridColumn});
            this.productsGridView.GridControl = this.mainGridControl;
            this.productsGridView.Name = "productsGridView";
            // 
            // nameGridColumn
            // 
            this.nameGridColumn.Caption = "Название";
            this.nameGridColumn.FieldName = "Products.Name";
            this.nameGridColumn.Name = "nameGridColumn";
            this.nameGridColumn.Visible = true;
            this.nameGridColumn.VisibleIndex = 0;
            // 
            // mainToolstrip
            // 
            this.mainToolstrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mainToolstrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.separator,
            this.btnCopy,
            this.btnEdit,
            this.btnDelete,
            this.separator2,
            this.btnRefresh,
            this.toolStripSeparator1,
            this.searchButton});
            this.mainToolstrip.Location = new System.Drawing.Point(0, 0);
            this.mainToolstrip.Name = "mainToolstrip";
            this.mainToolstrip.Size = new System.Drawing.Size(710, 31);
            this.mainToolstrip.TabIndex = 3;
            this.mainToolstrip.Text = "toolStrip1";
            // 
            // btnAdd
            // 
            this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdd.Image = global::LaRenzo.Properties.Resources.add_wh;
            this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(28, 28);
            this.btnAdd.Text = "Создать поставщика";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // separator
            // 
            this.separator.Name = "separator";
            this.separator.Size = new System.Drawing.Size(6, 31);
            // 
            // btnCopy
            // 
            this.btnCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCopy.Image = global::LaRenzo.Properties.Resources.copy;
            this.btnCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(28, 28);
            this.btnCopy.Text = "Скопировать поставщика";
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEdit.Image = global::LaRenzo.Properties.Resources.edit_wh;
            this.btnEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(28, 28);
            this.btnEdit.Text = "Редактировать поставщика";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = global::LaRenzo.Properties.Resources.close_wh;
            this.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(28, 28);
            this.btnDelete.Text = "Пометить на удаление";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // separator2
            // 
            this.separator2.Name = "separator2";
            this.separator2.Size = new System.Drawing.Size(6, 31);
            // 
            // btnRefresh
            // 
            this.btnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRefresh.Image = global::LaRenzo.Properties.Resources.refresh;
            this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(28, 28);
            this.btnRefresh.Text = "Обновить";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // searchButton
            // 
            this.searchButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.searchButton.Image = global::LaRenzo.Properties.Resources.search;
            this.searchButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(28, 28);
            this.searchButton.Text = "Найти";
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // PartnerListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 455);
            this.Controls.Add(this.mainGridControl);
            this.Controls.Add(this.mainToolstrip);
            this.Name = "PartnerListForm";
            this.Text = "Контрагенты";
            ((System.ComponentModel.ISupportInitialize)(this.productGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsGridView)).EndInit();
            this.mainToolstrip.ResumeLayout(false);
            this.mainToolstrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip mainToolstrip;
        private System.Windows.Forms.ToolStripButton btnAdd;
        private System.Windows.Forms.ToolStripSeparator separator;
        private System.Windows.Forms.ToolStripButton btnCopy;
        private System.Windows.Forms.ToolStripButton btnEdit;
        private System.Windows.Forms.ToolStripButton btnDelete;
        private System.Windows.Forms.ToolStripSeparator separator2;
        private DevExpress.XtraGrid.GridControl mainGridControl;
		private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
		private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private DevExpress.XtraGrid.Columns.GridColumn columnPhone;
		private System.Windows.Forms.ToolStripButton btnRefresh;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton searchButton;
		private DevExpress.XtraGrid.Columns.GridColumn dayOrderColumn;
		private DevExpress.XtraGrid.Columns.GridColumn contactFaceColumn;
		private DevExpress.XtraGrid.Columns.GridColumn productsColumn;
		private DevExpress.XtraGrid.Columns.GridColumn idColumn;
		private DevExpress.XtraGrid.Views.Grid.GridView productsGridView;
		private DevExpress.XtraGrid.Columns.GridColumn nameGridColumn;
		private DevExpress.XtraGrid.Views.Grid.GridView productGridView;
		private DevExpress.XtraGrid.Columns.GridColumn subColumnId;
		private DevExpress.XtraGrid.Columns.GridColumn subColumnName;
		private DevExpress.XtraGrid.Columns.GridColumn subColumnIsSet;
		private DevExpress.XtraGrid.Columns.GridColumn subColumnMeasure;
		private DevExpress.XtraGrid.Columns.GridColumn subColumnMinAmount;
    }
}