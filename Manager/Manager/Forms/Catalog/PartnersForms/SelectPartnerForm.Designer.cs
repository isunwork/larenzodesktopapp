﻿namespace LaRenzo.Forms.Catalog.PartnersForms
{
    partial class SelectPartnerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnInn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnKPP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnlBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.btnSelect);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 408);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(504, 24);
            this.pnlBottom.TabIndex = 11;
            // 
            // btnSelect
            // 
            this.btnSelect.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSelect.Location = new System.Drawing.Point(328, 0);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(87, 22);
            this.btnSelect.TabIndex = 3;
            this.btnSelect.Text = "Выбрать";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.Location = new System.Drawing.Point(415, 0);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 22);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // mainGridControl
            // 
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl.Location = new System.Drawing.Point(0, 0);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(504, 408);
            this.mainGridControl.TabIndex = 12;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnId,
            this.columnName,
            this.columnPhone,
            this.columnAddress,
            this.columnInn,
            this.columnKPP});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.mainGridView.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.mainGridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.mainGridView_RowStyle);
            // 
            // columnId
            // 
            this.columnId.Caption = "ИД";
            this.columnId.FieldName = "ID";
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.AllowEdit = false;
            this.columnId.OptionsColumn.ReadOnly = true;
            this.columnId.Visible = true;
            this.columnId.VisibleIndex = 0;
            this.columnId.Width = 58;
            // 
            // columnName
            // 
            this.columnName.Caption = "Наименование";
            this.columnName.FieldName = "PartnerName";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.OptionsColumn.ReadOnly = true;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 1;
            this.columnName.Width = 200;
            // 
            // columnPhone
            // 
            this.columnPhone.Caption = "Телефон";
            this.columnPhone.FieldName = "PartnerPhone";
            this.columnPhone.Name = "columnPhone";
            this.columnPhone.OptionsColumn.AllowEdit = false;
            this.columnPhone.OptionsColumn.ReadOnly = true;
            this.columnPhone.Visible = true;
            this.columnPhone.VisibleIndex = 3;
            this.columnPhone.Width = 163;
            // 
            // columnAddress
            // 
            this.columnAddress.Caption = "Адрес";
            this.columnAddress.FieldName = "PartnerAddress";
            this.columnAddress.Name = "columnAddress";
            this.columnAddress.OptionsColumn.AllowEdit = false;
            this.columnAddress.OptionsColumn.ReadOnly = true;
            this.columnAddress.Visible = true;
            this.columnAddress.VisibleIndex = 2;
            this.columnAddress.Width = 271;
            // 
            // columnInn
            // 
            this.columnInn.Caption = "ИНН";
            this.columnInn.FieldName = "PartnerInn";
            this.columnInn.Name = "columnInn";
            this.columnInn.Visible = true;
            this.columnInn.VisibleIndex = 4;
            this.columnInn.Width = 100;
            // 
            // columnKPP
            // 
            this.columnKPP.Caption = "КПП";
            this.columnKPP.FieldName = "PartnerKpp";
            this.columnKPP.Name = "columnKPP";
            this.columnKPP.Visible = true;
            this.columnKPP.VisibleIndex = 5;
            this.columnKPP.Width = 100;
            // 
            // SelectPartnerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 432);
            this.Controls.Add(this.mainGridControl);
            this.Controls.Add(this.pnlBottom);
            this.Name = "SelectPartnerForm";
            this.Text = "Выберите партнера";
            this.pnlBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnCancel;
        private DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private DevExpress.XtraGrid.Columns.GridColumn columnPhone;
        private DevExpress.XtraGrid.Columns.GridColumn columnAddress;
        private DevExpress.XtraGrid.Columns.GridColumn columnInn;
        private DevExpress.XtraGrid.Columns.GridColumn columnKPP;
    }
}