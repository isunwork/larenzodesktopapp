﻿namespace LaRenzo.Forms.Catalog.PartnersForms
{
    partial class PartnerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.pnlBottom = new System.Windows.Forms.Panel();
			this.btnPrint = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
			this.ReportsPanel = new System.Windows.Forms.Panel();
			this.lLblSettlements = new System.Windows.Forms.LinkLabel();
			this.label1 = new System.Windows.Forms.Label();
			this.mainTabControl = new System.Windows.Forms.TabControl();
			this.AddressPhonesTab = new System.Windows.Forms.TabPage();
			this.remarkDayOrderTextBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.contactFaceTextBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.btnFillFactAddress = new System.Windows.Forms.Button();
			this.btnCheck = new System.Windows.Forms.Button();
			this.btnPostAddress = new System.Windows.Forms.Button();
			this.btnFillAddress = new System.Windows.Forms.Button();
			this.txtFactAddress = new System.Windows.Forms.TextBox();
			this.lblPostAddress = new System.Windows.Forms.Label();
			this.lblAddress = new System.Windows.Forms.Label();
			this.lblFactAddress = new System.Windows.Forms.Label();
			this.txtPostAddress = new System.Windows.Forms.TextBox();
			this.txtAddress = new System.Windows.Forms.TextBox();
			this.lblOtherInfo = new System.Windows.Forms.Label();
			this.txtEMail = new System.Windows.Forms.TextBox();
			this.txtFax = new System.Windows.Forms.TextBox();
			this.txtPhone = new System.Windows.Forms.TextBox();
			this.lblFax = new System.Windows.Forms.Label();
			this.lblPhone = new System.Windows.Forms.Label();
			this.txtOtherInfo = new System.Windows.Forms.TextBox();
			this.lblEMail = new System.Windows.Forms.Label();
			this.OtherInfoTab = new System.Windows.Forms.TabPage();
			this.txtOptionsInfo = new System.Windows.Forms.TextBox();
			this.productsPage = new System.Windows.Forms.TabPage();
			this.productsGridControl = new DevExpress.XtraGrid.GridControl();
			this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnIsSet = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnMeasure = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnMinAmount = new DevExpress.XtraGrid.Columns.GridColumn();
			this.topPanel = new System.Windows.Forms.Panel();
			this.btnFillKPP = new System.Windows.Forms.Button();
			this.cmbPartnerType = new System.Windows.Forms.ComboBox();
			this.txtID = new System.Windows.Forms.TextBox();
			this.lblKpp = new System.Windows.Forms.Label();
			this.lblOKATO = new System.Windows.Forms.Label();
			this.txtShortName = new System.Windows.Forms.TextBox();
			this.txtOkpo = new System.Windows.Forms.TextBox();
			this.txtKpp = new System.Windows.Forms.TextBox();
			this.txtInn = new System.Windows.Forms.TextBox();
			this.lblInn = new System.Windows.Forms.Label();
			this.txtName = new System.Windows.Forms.TextBox();
			this.lblPartnerType = new System.Windows.Forms.Label();
			this.lblShortName = new System.Windows.Forms.Label();
			this.lblName = new System.Windows.Forms.Label();
			this.pnlBottom.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
			this.mainSplitContainer.Panel1.SuspendLayout();
			this.mainSplitContainer.Panel2.SuspendLayout();
			this.mainSplitContainer.SuspendLayout();
			this.ReportsPanel.SuspendLayout();
			this.mainTabControl.SuspendLayout();
			this.AddressPhonesTab.SuspendLayout();
			this.OtherInfoTab.SuspendLayout();
			this.productsPage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.productsGridControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
			this.topPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlBottom
			// 
			this.pnlBottom.BackColor = System.Drawing.SystemColors.ScrollBar;
			this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pnlBottom.Controls.Add(this.btnPrint);
			this.pnlBottom.Controls.Add(this.btnSave);
			this.pnlBottom.Controls.Add(this.btnCancel);
			this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlBottom.Location = new System.Drawing.Point(0, 714);
			this.pnlBottom.Margin = new System.Windows.Forms.Padding(4);
			this.pnlBottom.Name = "pnlBottom";
			this.pnlBottom.Size = new System.Drawing.Size(912, 41);
			this.pnlBottom.TabIndex = 1;
			// 
			// btnPrint
			// 
			this.btnPrint.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnPrint.Dock = System.Windows.Forms.DockStyle.Right;
			this.btnPrint.Location = new System.Drawing.Point(562, 0);
			this.btnPrint.Margin = new System.Windows.Forms.Padding(4, 4, 27, 4);
			this.btnPrint.Name = "btnPrint";
			this.btnPrint.Size = new System.Drawing.Size(116, 39);
			this.btnPrint.TabIndex = 4;
			this.btnPrint.Text = "Печать";
			this.btnPrint.UseVisualStyleBackColor = true;
			this.btnPrint.Visible = false;
			// 
			// btnSave
			// 
			this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
			this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnSave.Location = new System.Drawing.Point(678, 0);
			this.btnSave.Margin = new System.Windows.Forms.Padding(4);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(116, 39);
			this.btnSave.TabIndex = 3;
			this.btnSave.Text = "Записать";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
			this.btnCancel.Location = new System.Drawing.Point(794, 0);
			this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 27, 4);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(116, 39);
			this.btnCancel.TabIndex = 2;
			this.btnCancel.Text = "Отмена";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// mainSplitContainer
			// 
			this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainSplitContainer.Location = new System.Drawing.Point(0, 0);
			this.mainSplitContainer.Margin = new System.Windows.Forms.Padding(4);
			this.mainSplitContainer.Name = "mainSplitContainer";
			// 
			// mainSplitContainer.Panel1
			// 
			this.mainSplitContainer.Panel1.Controls.Add(this.ReportsPanel);
			// 
			// mainSplitContainer.Panel2
			// 
			this.mainSplitContainer.Panel2.Controls.Add(this.mainTabControl);
			this.mainSplitContainer.Panel2.Controls.Add(this.topPanel);
			this.mainSplitContainer.Size = new System.Drawing.Size(912, 714);
			this.mainSplitContainer.SplitterDistance = 170;
			this.mainSplitContainer.SplitterWidth = 5;
			this.mainSplitContainer.TabIndex = 11;
			// 
			// ReportsPanel
			// 
			this.ReportsPanel.BackColor = System.Drawing.SystemColors.ScrollBar;
			this.ReportsPanel.Controls.Add(this.lLblSettlements);
			this.ReportsPanel.Controls.Add(this.label1);
			this.ReportsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ReportsPanel.Location = new System.Drawing.Point(0, 0);
			this.ReportsPanel.Margin = new System.Windows.Forms.Padding(4);
			this.ReportsPanel.Name = "ReportsPanel";
			this.ReportsPanel.Size = new System.Drawing.Size(170, 714);
			this.ReportsPanel.TabIndex = 12;
			// 
			// lLblSettlements
			// 
			this.lLblSettlements.ActiveLinkColor = System.Drawing.Color.PaleGreen;
			this.lLblSettlements.AutoSize = true;
			this.lLblSettlements.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lLblSettlements.LinkColor = System.Drawing.Color.DarkGreen;
			this.lLblSettlements.Location = new System.Drawing.Point(4, 22);
			this.lLblSettlements.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lLblSettlements.Name = "lLblSettlements";
			this.lLblSettlements.Size = new System.Drawing.Size(139, 20);
			this.lLblSettlements.TabIndex = 1;
			this.lLblSettlements.TabStop = true;
			this.lLblSettlements.Text = "Взаиморасчеты";
			this.lLblSettlements.Visible = false;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.ForeColor = System.Drawing.Color.Firebrick;
			this.label1.Location = new System.Drawing.Point(4, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(147, 18);
			this.label1.TabIndex = 0;
			this.label1.Text = "Отчеты                ";
			// 
			// mainTabControl
			// 
			this.mainTabControl.Controls.Add(this.AddressPhonesTab);
			this.mainTabControl.Controls.Add(this.OtherInfoTab);
			this.mainTabControl.Controls.Add(this.productsPage);
			this.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainTabControl.Location = new System.Drawing.Point(0, 140);
			this.mainTabControl.Margin = new System.Windows.Forms.Padding(4);
			this.mainTabControl.Name = "mainTabControl";
			this.mainTabControl.SelectedIndex = 0;
			this.mainTabControl.Size = new System.Drawing.Size(737, 574);
			this.mainTabControl.TabIndex = 10;
			// 
			// AddressPhonesTab
			// 
			this.AddressPhonesTab.BackColor = System.Drawing.SystemColors.Control;
			this.AddressPhonesTab.Controls.Add(this.remarkDayOrderTextBox);
			this.AddressPhonesTab.Controls.Add(this.label3);
			this.AddressPhonesTab.Controls.Add(this.contactFaceTextBox);
			this.AddressPhonesTab.Controls.Add(this.label2);
			this.AddressPhonesTab.Controls.Add(this.btnFillFactAddress);
			this.AddressPhonesTab.Controls.Add(this.btnCheck);
			this.AddressPhonesTab.Controls.Add(this.btnPostAddress);
			this.AddressPhonesTab.Controls.Add(this.btnFillAddress);
			this.AddressPhonesTab.Controls.Add(this.txtFactAddress);
			this.AddressPhonesTab.Controls.Add(this.lblPostAddress);
			this.AddressPhonesTab.Controls.Add(this.lblAddress);
			this.AddressPhonesTab.Controls.Add(this.lblFactAddress);
			this.AddressPhonesTab.Controls.Add(this.txtPostAddress);
			this.AddressPhonesTab.Controls.Add(this.txtAddress);
			this.AddressPhonesTab.Controls.Add(this.lblOtherInfo);
			this.AddressPhonesTab.Controls.Add(this.txtEMail);
			this.AddressPhonesTab.Controls.Add(this.txtFax);
			this.AddressPhonesTab.Controls.Add(this.txtPhone);
			this.AddressPhonesTab.Controls.Add(this.lblFax);
			this.AddressPhonesTab.Controls.Add(this.lblPhone);
			this.AddressPhonesTab.Controls.Add(this.txtOtherInfo);
			this.AddressPhonesTab.Controls.Add(this.lblEMail);
			this.AddressPhonesTab.Location = new System.Drawing.Point(4, 25);
			this.AddressPhonesTab.Margin = new System.Windows.Forms.Padding(4);
			this.AddressPhonesTab.Name = "AddressPhonesTab";
			this.AddressPhonesTab.Padding = new System.Windows.Forms.Padding(4);
			this.AddressPhonesTab.Size = new System.Drawing.Size(729, 545);
			this.AddressPhonesTab.TabIndex = 0;
			this.AddressPhonesTab.Text = "Контактная информация";
			// 
			// remarkDayOrderTextBox
			// 
			this.remarkDayOrderTextBox.Location = new System.Drawing.Point(8, 359);
			this.remarkDayOrderTextBox.Margin = new System.Windows.Forms.Padding(4);
			this.remarkDayOrderTextBox.Name = "remarkDayOrderTextBox";
			this.remarkDayOrderTextBox.Size = new System.Drawing.Size(696, 22);
			this.remarkDayOrderTextBox.TabIndex = 28;
			this.remarkDayOrderTextBox.Tag = "PartnerRemarkDayOrder";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(8, 338);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(194, 17);
			this.label3.TabIndex = 29;
			this.label3.Text = "Примечание по дню заказа:";
			// 
			// contactFaceTextBox
			// 
			this.contactFaceTextBox.Location = new System.Drawing.Point(8, 312);
			this.contactFaceTextBox.Margin = new System.Windows.Forms.Padding(4);
			this.contactFaceTextBox.Name = "contactFaceTextBox";
			this.contactFaceTextBox.Size = new System.Drawing.Size(696, 22);
			this.contactFaceTextBox.TabIndex = 26;
			this.contactFaceTextBox.Tag = "PartnerContactFace";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(7, 292);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(126, 17);
			this.label2.TabIndex = 27;
			this.label2.Text = "Контактное лицо:";
			// 
			// btnFillFactAddress
			// 
			this.btnFillFactAddress.Location = new System.Drawing.Point(673, 23);
			this.btnFillFactAddress.Margin = new System.Windows.Forms.Padding(4);
			this.btnFillFactAddress.Name = "btnFillFactAddress";
			this.btnFillFactAddress.Size = new System.Drawing.Size(32, 25);
			this.btnFillFactAddress.TabIndex = 25;
			this.btnFillFactAddress.Text = "...";
			this.btnFillFactAddress.UseVisualStyleBackColor = true;
			this.btnFillFactAddress.Click += new System.EventHandler(this.FillAddress_Click);
			// 
			// btnCheck
			// 
			this.btnCheck.BackgroundImage = global::LaRenzo.Properties.Resources.agt_web_1892;
			this.btnCheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnCheck.Location = new System.Drawing.Point(673, 262);
			this.btnCheck.Margin = new System.Windows.Forms.Padding(4);
			this.btnCheck.Name = "btnCheck";
			this.btnCheck.Size = new System.Drawing.Size(32, 27);
			this.btnCheck.TabIndex = 25;
			this.btnCheck.UseVisualStyleBackColor = true;
			this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
			// 
			// btnPostAddress
			// 
			this.btnPostAddress.Location = new System.Drawing.Point(673, 118);
			this.btnPostAddress.Margin = new System.Windows.Forms.Padding(4);
			this.btnPostAddress.Name = "btnPostAddress";
			this.btnPostAddress.Size = new System.Drawing.Size(32, 25);
			this.btnPostAddress.TabIndex = 25;
			this.btnPostAddress.Text = "...";
			this.btnPostAddress.UseVisualStyleBackColor = true;
			this.btnPostAddress.Click += new System.EventHandler(this.FillAddress_Click);
			// 
			// btnFillAddress
			// 
			this.btnFillAddress.Location = new System.Drawing.Point(673, 70);
			this.btnFillAddress.Margin = new System.Windows.Forms.Padding(4);
			this.btnFillAddress.Name = "btnFillAddress";
			this.btnFillAddress.Size = new System.Drawing.Size(32, 25);
			this.btnFillAddress.TabIndex = 25;
			this.btnFillAddress.Text = "...";
			this.btnFillAddress.UseVisualStyleBackColor = true;
			this.btnFillAddress.Click += new System.EventHandler(this.FillAddress_Click);
			// 
			// txtFactAddress
			// 
			this.txtFactAddress.Location = new System.Drawing.Point(8, 23);
			this.txtFactAddress.Margin = new System.Windows.Forms.Padding(4);
			this.txtFactAddress.Name = "txtFactAddress";
			this.txtFactAddress.Size = new System.Drawing.Size(664, 22);
			this.txtFactAddress.TabIndex = 24;
			this.txtFactAddress.Tag = "PartnerFactAddress";
			// 
			// lblPostAddress
			// 
			this.lblPostAddress.AutoSize = true;
			this.lblPostAddress.Location = new System.Drawing.Point(8, 100);
			this.lblPostAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblPostAddress.Name = "lblPostAddress";
			this.lblPostAddress.Size = new System.Drawing.Size(121, 17);
			this.lblPostAddress.TabIndex = 23;
			this.lblPostAddress.Text = "Почтовый адрес:";
			// 
			// lblAddress
			// 
			this.lblAddress.AutoSize = true;
			this.lblAddress.Location = new System.Drawing.Point(4, 52);
			this.lblAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblAddress.Name = "lblAddress";
			this.lblAddress.Size = new System.Drawing.Size(147, 17);
			this.lblAddress.TabIndex = 23;
			this.lblAddress.Text = "Юридический адрес:";
			// 
			// lblFactAddress
			// 
			this.lblFactAddress.AutoSize = true;
			this.lblFactAddress.Location = new System.Drawing.Point(8, 4);
			this.lblFactAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFactAddress.Name = "lblFactAddress";
			this.lblFactAddress.Size = new System.Drawing.Size(144, 17);
			this.lblFactAddress.TabIndex = 23;
			this.lblFactAddress.Text = "Фактический адрес:";
			// 
			// txtPostAddress
			// 
			this.txtPostAddress.Location = new System.Drawing.Point(8, 119);
			this.txtPostAddress.Margin = new System.Windows.Forms.Padding(4);
			this.txtPostAddress.Name = "txtPostAddress";
			this.txtPostAddress.Size = new System.Drawing.Size(664, 22);
			this.txtPostAddress.TabIndex = 22;
			this.txtPostAddress.Tag = "PartnerPostAddress";
			// 
			// txtAddress
			// 
			this.txtAddress.Location = new System.Drawing.Point(8, 71);
			this.txtAddress.Margin = new System.Windows.Forms.Padding(4);
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Size = new System.Drawing.Size(664, 22);
			this.txtAddress.TabIndex = 22;
			this.txtAddress.Tag = "PartnerAddress";
			// 
			// lblOtherInfo
			// 
			this.lblOtherInfo.AutoSize = true;
			this.lblOtherInfo.Location = new System.Drawing.Point(8, 391);
			this.lblOtherInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblOtherInfo.Name = "lblOtherInfo";
			this.lblOtherInfo.Size = new System.Drawing.Size(147, 17);
			this.lblOtherInfo.TabIndex = 22;
			this.lblOtherInfo.Text = "Другая информация:";
			// 
			// txtEMail
			// 
			this.txtEMail.Location = new System.Drawing.Point(8, 263);
			this.txtEMail.Margin = new System.Windows.Forms.Padding(4);
			this.txtEMail.Name = "txtEMail";
			this.txtEMail.Size = new System.Drawing.Size(664, 22);
			this.txtEMail.TabIndex = 21;
			this.txtEMail.Tag = "PartnerEmail";
			// 
			// txtFax
			// 
			this.txtFax.Location = new System.Drawing.Point(8, 215);
			this.txtFax.Margin = new System.Windows.Forms.Padding(4);
			this.txtFax.Name = "txtFax";
			this.txtFax.Size = new System.Drawing.Size(696, 22);
			this.txtFax.TabIndex = 21;
			this.txtFax.Tag = "PartnerFax";
			// 
			// txtPhone
			// 
			this.txtPhone.Location = new System.Drawing.Point(8, 167);
			this.txtPhone.Margin = new System.Windows.Forms.Padding(4);
			this.txtPhone.Name = "txtPhone";
			this.txtPhone.Size = new System.Drawing.Size(696, 22);
			this.txtPhone.TabIndex = 21;
			this.txtPhone.Tag = "PartnerPhone";
			// 
			// lblFax
			// 
			this.lblFax.AutoSize = true;
			this.lblFax.Location = new System.Drawing.Point(8, 196);
			this.lblFax.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFax.Name = "lblFax";
			this.lblFax.Size = new System.Drawing.Size(47, 17);
			this.lblFax.TabIndex = 21;
			this.lblFax.Text = "Факс:";
			// 
			// lblPhone
			// 
			this.lblPhone.AutoSize = true;
			this.lblPhone.Location = new System.Drawing.Point(8, 148);
			this.lblPhone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblPhone.Name = "lblPhone";
			this.lblPhone.Size = new System.Drawing.Size(72, 17);
			this.lblPhone.TabIndex = 21;
			this.lblPhone.Text = "Телефон:";
			// 
			// txtOtherInfo
			// 
			this.txtOtherInfo.Location = new System.Drawing.Point(8, 410);
			this.txtOtherInfo.Margin = new System.Windows.Forms.Padding(4);
			this.txtOtherInfo.Multiline = true;
			this.txtOtherInfo.Name = "txtOtherInfo";
			this.txtOtherInfo.Size = new System.Drawing.Size(696, 127);
			this.txtOtherInfo.TabIndex = 16;
			this.txtOtherInfo.Tag = "PartnerOtherInfo";
			// 
			// lblEMail
			// 
			this.lblEMail.AutoSize = true;
			this.lblEMail.Location = new System.Drawing.Point(8, 244);
			this.lblEMail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblEMail.Name = "lblEMail";
			this.lblEMail.Size = new System.Drawing.Size(51, 17);
			this.lblEMail.TabIndex = 20;
			this.lblEMail.Text = "E-Mail:";
			// 
			// OtherInfoTab
			// 
			this.OtherInfoTab.BackColor = System.Drawing.SystemColors.Control;
			this.OtherInfoTab.Controls.Add(this.txtOptionsInfo);
			this.OtherInfoTab.Location = new System.Drawing.Point(4, 25);
			this.OtherInfoTab.Margin = new System.Windows.Forms.Padding(4);
			this.OtherInfoTab.Name = "OtherInfoTab";
			this.OtherInfoTab.Padding = new System.Windows.Forms.Padding(4);
			this.OtherInfoTab.Size = new System.Drawing.Size(729, 545);
			this.OtherInfoTab.TabIndex = 1;
			this.OtherInfoTab.Text = "Дополнительная информация";
			// 
			// txtOptionsInfo
			// 
			this.txtOptionsInfo.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtOptionsInfo.Location = new System.Drawing.Point(4, 4);
			this.txtOptionsInfo.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
			this.txtOptionsInfo.Multiline = true;
			this.txtOptionsInfo.Name = "txtOptionsInfo";
			this.txtOptionsInfo.Size = new System.Drawing.Size(721, 537);
			this.txtOptionsInfo.TabIndex = 0;
			this.txtOptionsInfo.Tag = "PartnerOptionInfo";
			// 
			// productsPage
			// 
			this.productsPage.BackColor = System.Drawing.SystemColors.Control;
			this.productsPage.Controls.Add(this.productsGridControl);
			this.productsPage.Location = new System.Drawing.Point(4, 25);
			this.productsPage.Name = "productsPage";
			this.productsPage.Padding = new System.Windows.Forms.Padding(3);
			this.productsPage.Size = new System.Drawing.Size(729, 545);
			this.productsPage.TabIndex = 2;
			this.productsPage.Text = "Продукты";
			// 
			// productsGridControl
			// 
			this.productsGridControl.Cursor = System.Windows.Forms.Cursors.Default;
			this.productsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.productsGridControl.Location = new System.Drawing.Point(3, 3);
			this.productsGridControl.MainView = this.gridView1;
			this.productsGridControl.Name = "productsGridControl";
			this.productsGridControl.Size = new System.Drawing.Size(723, 539);
			this.productsGridControl.TabIndex = 0;
			this.productsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
			// 
			// gridView1
			// 
			this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnId,
            this.columnName,
            this.columnIsSet,
            this.columnMeasure,
            this.columnMinAmount});
			this.gridView1.GridControl = this.productsGridControl;
			this.gridView1.Name = "gridView1";
			// 
			// columnId
			// 
			this.columnId.Caption = "ИД";
			this.columnId.FieldName = "Product.ID";
			this.columnId.Name = "columnId";
			this.columnId.OptionsColumn.AllowEdit = false;
			this.columnId.OptionsColumn.ReadOnly = true;
			this.columnId.Visible = true;
			this.columnId.VisibleIndex = 0;
			this.columnId.Width = 67;
			// 
			// columnName
			// 
			this.columnName.Caption = "Наименование";
			this.columnName.FieldName = "Product.Name";
			this.columnName.Name = "columnName";
			this.columnName.OptionsColumn.AllowEdit = false;
			this.columnName.OptionsColumn.ReadOnly = true;
			this.columnName.Visible = true;
			this.columnName.VisibleIndex = 1;
			this.columnName.Width = 419;
			// 
			// columnIsSet
			// 
			this.columnIsSet.Caption = "Составной";
			this.columnIsSet.FieldName = "Product.IsSetProduct";
			this.columnIsSet.Name = "columnIsSet";
			this.columnIsSet.OptionsColumn.AllowEdit = false;
			this.columnIsSet.OptionsColumn.ReadOnly = true;
			this.columnIsSet.Visible = true;
			this.columnIsSet.VisibleIndex = 2;
			// 
			// columnMeasure
			// 
			this.columnMeasure.Caption = "Ед.Изм";
			this.columnMeasure.FieldName = "Product.Measure.ShortName";
			this.columnMeasure.Name = "columnMeasure";
			this.columnMeasure.OptionsColumn.AllowEdit = false;
			this.columnMeasure.OptionsColumn.ReadOnly = true;
			this.columnMeasure.Visible = true;
			this.columnMeasure.VisibleIndex = 3;
			// 
			// columnMinAmount
			// 
			this.columnMinAmount.Caption = "Минимальный остаток";
			this.columnMinAmount.FieldName = "Product.MinAmount";
			this.columnMinAmount.Name = "columnMinAmount";
			this.columnMinAmount.OptionsColumn.AllowEdit = false;
			this.columnMinAmount.OptionsColumn.ReadOnly = true;
			this.columnMinAmount.Visible = true;
			this.columnMinAmount.VisibleIndex = 4;
			// 
			// topPanel
			// 
			this.topPanel.Controls.Add(this.btnFillKPP);
			this.topPanel.Controls.Add(this.cmbPartnerType);
			this.topPanel.Controls.Add(this.txtID);
			this.topPanel.Controls.Add(this.lblKpp);
			this.topPanel.Controls.Add(this.lblOKATO);
			this.topPanel.Controls.Add(this.txtShortName);
			this.topPanel.Controls.Add(this.txtOkpo);
			this.topPanel.Controls.Add(this.txtKpp);
			this.topPanel.Controls.Add(this.txtInn);
			this.topPanel.Controls.Add(this.lblInn);
			this.topPanel.Controls.Add(this.txtName);
			this.topPanel.Controls.Add(this.lblPartnerType);
			this.topPanel.Controls.Add(this.lblShortName);
			this.topPanel.Controls.Add(this.lblName);
			this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.topPanel.Location = new System.Drawing.Point(0, 0);
			this.topPanel.Margin = new System.Windows.Forms.Padding(4);
			this.topPanel.Name = "topPanel";
			this.topPanel.Size = new System.Drawing.Size(737, 140);
			this.topPanel.TabIndex = 11;
			// 
			// btnFillKPP
			// 
			this.btnFillKPP.BackgroundImage = global::LaRenzo.Properties.Resources.forward_wh;
			this.btnFillKPP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnFillKPP.Location = new System.Drawing.Point(319, 105);
			this.btnFillKPP.Margin = new System.Windows.Forms.Padding(4);
			this.btnFillKPP.Name = "btnFillKPP";
			this.btnFillKPP.Size = new System.Drawing.Size(25, 25);
			this.btnFillKPP.TabIndex = 25;
			this.btnFillKPP.UseVisualStyleBackColor = true;
			this.btnFillKPP.Click += new System.EventHandler(this.btnFillKPP_Click);
			// 
			// cmbPartnerType
			// 
			this.cmbPartnerType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.cmbPartnerType.FormattingEnabled = true;
			this.cmbPartnerType.Items.AddRange(new object[] {
            "Юридическое лицо",
            "Физическое лицо"});
			this.cmbPartnerType.Location = new System.Drawing.Point(205, 73);
			this.cmbPartnerType.Margin = new System.Windows.Forms.Padding(4);
			this.cmbPartnerType.Name = "cmbPartnerType";
			this.cmbPartnerType.Size = new System.Drawing.Size(502, 24);
			this.cmbPartnerType.TabIndex = 24;
			this.cmbPartnerType.Tag = "PartnerIsIndividual";
			// 
			// txtID
			// 
			this.txtID.Location = new System.Drawing.Point(605, 73);
			this.txtID.Margin = new System.Windows.Forms.Padding(4);
			this.txtID.Name = "txtID";
			this.txtID.Size = new System.Drawing.Size(127, 22);
			this.txtID.TabIndex = 23;
			this.txtID.Tag = "id";
			this.txtID.Visible = false;
			// 
			// lblKpp
			// 
			this.lblKpp.AutoSize = true;
			this.lblKpp.Location = new System.Drawing.Point(345, 110);
			this.lblKpp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblKpp.Name = "lblKpp";
			this.lblKpp.Size = new System.Drawing.Size(41, 17);
			this.lblKpp.TabIndex = 19;
			this.lblKpp.Text = "КПП:";
			// 
			// lblOKATO
			// 
			this.lblOKATO.AutoSize = true;
			this.lblOKATO.Location = new System.Drawing.Point(499, 110);
			this.lblOKATO.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblOKATO.Name = "lblOKATO";
			this.lblOKATO.Size = new System.Drawing.Size(102, 17);
			this.lblOKATO.TabIndex = 19;
			this.lblOKATO.Text = "Код по ОКПО:";
			// 
			// txtShortName
			// 
			this.txtShortName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtShortName.Location = new System.Drawing.Point(205, 39);
			this.txtShortName.Margin = new System.Windows.Forms.Padding(4);
			this.txtShortName.Name = "txtShortName";
			this.txtShortName.Size = new System.Drawing.Size(502, 22);
			this.txtShortName.TabIndex = 14;
			this.txtShortName.Tag = "PartnerShortName";
			// 
			// txtOkpo
			// 
			this.txtOkpo.Location = new System.Drawing.Point(605, 105);
			this.txtOkpo.Margin = new System.Windows.Forms.Padding(4);
			this.txtOkpo.Name = "txtOkpo";
			this.txtOkpo.Size = new System.Drawing.Size(104, 22);
			this.txtOkpo.TabIndex = 14;
			this.txtOkpo.Tag = "PartnerOKPO";
			// 
			// txtKpp
			// 
			this.txtKpp.Location = new System.Drawing.Point(391, 106);
			this.txtKpp.Margin = new System.Windows.Forms.Padding(4);
			this.txtKpp.Name = "txtKpp";
			this.txtKpp.Size = new System.Drawing.Size(105, 22);
			this.txtKpp.TabIndex = 14;
			this.txtKpp.Tag = "PartnerKpp";
			// 
			// txtInn
			// 
			this.txtInn.Location = new System.Drawing.Point(205, 105);
			this.txtInn.Margin = new System.Windows.Forms.Padding(4);
			this.txtInn.Name = "txtInn";
			this.txtInn.Size = new System.Drawing.Size(105, 22);
			this.txtInn.TabIndex = 18;
			this.txtInn.Tag = "PartnerInn";
			// 
			// lblInn
			// 
			this.lblInn.AutoSize = true;
			this.lblInn.Location = new System.Drawing.Point(4, 108);
			this.lblInn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblInn.Name = "lblInn";
			this.lblInn.Size = new System.Drawing.Size(42, 17);
			this.lblInn.TabIndex = 12;
			this.lblInn.Text = "ИНН:";
			// 
			// txtName
			// 
			this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtName.Location = new System.Drawing.Point(205, 7);
			this.txtName.Margin = new System.Windows.Forms.Padding(4);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(502, 22);
			this.txtName.TabIndex = 10;
			this.txtName.Tag = "PartnerName";
			// 
			// lblPartnerType
			// 
			this.lblPartnerType.AutoSize = true;
			this.lblPartnerType.Location = new System.Drawing.Point(4, 81);
			this.lblPartnerType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblPartnerType.Name = "lblPartnerType";
			this.lblPartnerType.Size = new System.Drawing.Size(102, 17);
			this.lblPartnerType.TabIndex = 9;
			this.lblPartnerType.Text = "Юр./Физлицо:";
			// 
			// lblShortName
			// 
			this.lblShortName.AutoSize = true;
			this.lblShortName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lblShortName.Location = new System.Drawing.Point(4, 43);
			this.lblShortName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblShortName.Name = "lblShortName";
			this.lblShortName.Size = new System.Drawing.Size(159, 17);
			this.lblShortName.TabIndex = 9;
			this.lblShortName.Text = "Раб. наименование:";
			// 
			// lblName
			// 
			this.lblName.AutoSize = true;
			this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lblName.Location = new System.Drawing.Point(4, 11);
			this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(182, 17);
			this.lblName.TabIndex = 9;
			this.lblName.Text = "Полное наименование:";
			// 
			// PartnerForm
			// 
			this.AcceptButton = this.btnSave;
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(912, 755);
			this.Controls.Add(this.mainSplitContainer);
			this.Controls.Add(this.pnlBottom);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Margin = new System.Windows.Forms.Padding(4);
			this.MaximizeBox = false;
			this.MaximumSize = new System.Drawing.Size(930, 800);
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(930, 800);
			this.Name = "PartnerForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Контрагент";
			this.pnlBottom.ResumeLayout(false);
			this.mainSplitContainer.Panel1.ResumeLayout(false);
			this.mainSplitContainer.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
			this.mainSplitContainer.ResumeLayout(false);
			this.ReportsPanel.ResumeLayout(false);
			this.ReportsPanel.PerformLayout();
			this.mainTabControl.ResumeLayout(false);
			this.AddressPhonesTab.ResumeLayout(false);
			this.AddressPhonesTab.PerformLayout();
			this.OtherInfoTab.ResumeLayout(false);
			this.OtherInfoTab.PerformLayout();
			this.productsPage.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.productsGridControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
			this.topPanel.ResumeLayout(false);
			this.topPanel.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.SplitContainer mainSplitContainer;
        private System.Windows.Forms.Panel ReportsPanel;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label lblOtherInfo;
        private System.Windows.Forms.Label lblEMail;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblOKATO;
        private System.Windows.Forms.TextBox txtKpp;
        private System.Windows.Forms.TextBox txtInn;
        private System.Windows.Forms.Label lblInn;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TabControl mainTabControl;
        private System.Windows.Forms.TabPage AddressPhonesTab;
        private System.Windows.Forms.TabPage OtherInfoTab;
        private System.Windows.Forms.TextBox txtOptionsInfo;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.TextBox txtOtherInfo;
        private System.Windows.Forms.TextBox txtShortName;
        private System.Windows.Forms.Label lblShortName;
        private System.Windows.Forms.TextBox txtFactAddress;
        private System.Windows.Forms.Button btnFillKPP;
        private System.Windows.Forms.ComboBox cmbPartnerType;
        private System.Windows.Forms.Label lblPartnerType;
        private System.Windows.Forms.Label lblKpp;
        private System.Windows.Forms.TextBox txtOkpo;
        private System.Windows.Forms.Label lblFactAddress;
        private System.Windows.Forms.Button btnFillAddress;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Button btnFillFactAddress;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.Label lblPostAddress;
        private System.Windows.Forms.TextBox txtPostAddress;
        private System.Windows.Forms.TextBox txtEMail;
        private System.Windows.Forms.TextBox txtFax;
        private System.Windows.Forms.Label lblFax;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnPostAddress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel lLblSettlements;
		private System.Windows.Forms.TabPage productsPage;
		private DevExpress.XtraGrid.GridControl productsGridControl;
		private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
		private DevExpress.XtraGrid.Columns.GridColumn columnId;
		private DevExpress.XtraGrid.Columns.GridColumn columnName;
		private DevExpress.XtraGrid.Columns.GridColumn columnMeasure;
		private DevExpress.XtraGrid.Columns.GridColumn columnMinAmount;
		private DevExpress.XtraGrid.Columns.GridColumn columnIsSet;
		private System.Windows.Forms.TextBox remarkDayOrderTextBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox contactFaceTextBox;
		private System.Windows.Forms.Label label2;
    }
}