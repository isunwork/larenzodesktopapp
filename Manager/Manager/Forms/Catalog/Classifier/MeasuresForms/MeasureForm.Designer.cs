﻿namespace LaRenzo.Forms.Catalog.Classifier.MeasuresForms
{
    partial class MeasureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblShortName = new System.Windows.Forms.Label();
            this.lblFullName = new System.Windows.Forms.Label();
            this.lblForeign = new System.Windows.Forms.Label();
            this.lblBase = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.txtFullName = new System.Windows.Forms.TextBox();
            this.txtForeignName = new System.Windows.Forms.TextBox();
            this.txtParentMeasure = new System.Windows.Forms.TextBox();
            this.chbIsBase = new System.Windows.Forms.CheckBox();
            this.btnSelectMeasure = new System.Windows.Forms.Button();
            this.lblMeasureValue = new System.Windows.Forms.Label();
            this.txtMeasureValue = new System.Windows.Forms.TextBox();
            this.txtShortName = new System.Windows.Forms.TextBox();
            this.pnlBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.btnSave);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 158);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(424, 24);
            this.pnlBottom.TabIndex = 2;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.ForeColor = System.Drawing.Color.Black;
            this.btnSave.Location = new System.Drawing.Point(248, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 22);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Записать";
            this.btnSave.UseCompatibleTextRendering = true;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.Location = new System.Drawing.Point(335, 0);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 22);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblShortName
            // 
            this.lblShortName.AutoSize = true;
            this.lblShortName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblShortName.Location = new System.Drawing.Point(12, 9);
            this.lblShortName.Name = "lblShortName";
            this.lblShortName.Size = new System.Drawing.Size(150, 13);
            this.lblShortName.TabIndex = 3;
            this.lblShortName.Text = "Краткое наименование:";
            // 
            // lblFullName
            // 
            this.lblFullName.AutoSize = true;
            this.lblFullName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblFullName.Location = new System.Drawing.Point(12, 35);
            this.lblFullName.Name = "lblFullName";
            this.lblFullName.Size = new System.Drawing.Size(145, 13);
            this.lblFullName.TabIndex = 4;
            this.lblFullName.Text = "Наименование полное:";
            // 
            // lblForeign
            // 
            this.lblForeign.AutoSize = true;
            this.lblForeign.Location = new System.Drawing.Point(12, 61);
            this.lblForeign.Name = "lblForeign";
            this.lblForeign.Size = new System.Drawing.Size(158, 13);
            this.lblForeign.TabIndex = 5;
            this.lblForeign.Text = "Международное сокращение:";
            // 
            // lblBase
            // 
            this.lblBase.AutoSize = true;
            this.lblBase.Location = new System.Drawing.Point(12, 87);
            this.lblBase.Name = "lblBase";
            this.lblBase.Size = new System.Drawing.Size(98, 13);
            this.lblBase.TabIndex = 7;
            this.lblBase.Text = "Базовая единица:";
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(12, 135);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(143, 20);
            this.txtId.TabIndex = 8;
            this.txtId.Tag = "id";
            this.txtId.Visible = false;
            // 
            // txtFullName
            // 
            this.txtFullName.Location = new System.Drawing.Point(173, 32);
            this.txtFullName.Name = "txtFullName";
            this.txtFullName.Size = new System.Drawing.Size(234, 20);
            this.txtFullName.TabIndex = 8;
            this.txtFullName.Tag = "Name";
            // 
            // txtForeignName
            // 
            this.txtForeignName.Location = new System.Drawing.Point(173, 58);
            this.txtForeignName.Name = "txtForeignName";
            this.txtForeignName.Size = new System.Drawing.Size(234, 20);
            this.txtForeignName.TabIndex = 8;
            this.txtForeignName.Tag = "foreignName";
            // 
            // txtParentMeasure
            // 
            this.txtParentMeasure.Location = new System.Drawing.Point(173, 84);
            this.txtParentMeasure.Name = "txtParentMeasure";
            this.txtParentMeasure.ReadOnly = true;
            this.txtParentMeasure.Size = new System.Drawing.Size(210, 20);
            this.txtParentMeasure.TabIndex = 8;
            this.txtParentMeasure.Tag = "parentmeasureid";
            // 
            // chbIsBase
            // 
            this.chbIsBase.AutoSize = true;
            this.chbIsBase.Location = new System.Drawing.Point(173, 135);
            this.chbIsBase.Name = "chbIsBase";
            this.chbIsBase.Size = new System.Drawing.Size(193, 17);
            this.chbIsBase.TabIndex = 9;
            this.chbIsBase.Tag = "isbasemeasure";
            this.chbIsBase.Text = "Это базовая единица измерения";
            this.chbIsBase.UseVisualStyleBackColor = true;
            // 
            // btnSelectMeasure
            // 
            this.btnSelectMeasure.Location = new System.Drawing.Point(383, 82);
            this.btnSelectMeasure.Name = "btnSelectMeasure";
            this.btnSelectMeasure.Size = new System.Drawing.Size(24, 23);
            this.btnSelectMeasure.TabIndex = 10;
            this.btnSelectMeasure.Text = "...";
            this.btnSelectMeasure.UseVisualStyleBackColor = true;
            this.btnSelectMeasure.Click += new System.EventHandler(this.btnSelectMeasure_Click);
            // 
            // lblMeasureValue
            // 
            this.lblMeasureValue.AutoSize = true;
            this.lblMeasureValue.Location = new System.Drawing.Point(12, 113);
            this.lblMeasureValue.Name = "lblMeasureValue";
            this.lblMeasureValue.Size = new System.Drawing.Size(128, 13);
            this.lblMeasureValue.TabIndex = 4;
            this.lblMeasureValue.Text = "Значение в базовых ед:";
            // 
            // txtMeasureValue
            // 
            this.txtMeasureValue.Location = new System.Drawing.Point(173, 110);
            this.txtMeasureValue.Name = "txtMeasureValue";
            this.txtMeasureValue.Size = new System.Drawing.Size(234, 20);
            this.txtMeasureValue.TabIndex = 8;
            this.txtMeasureValue.Tag = "parentmeasurevalue";
            this.txtMeasureValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMeasureValue_KeyPress);
            // 
            // txtShortName
            // 
            this.txtShortName.Location = new System.Drawing.Point(173, 6);
            this.txtShortName.Name = "txtShortName";
            this.txtShortName.Size = new System.Drawing.Size(234, 20);
            this.txtShortName.TabIndex = 8;
            this.txtShortName.Tag = "shortname";
            // 
            // MeasureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(424, 182);
            this.Controls.Add(this.btnSelectMeasure);
            this.Controls.Add(this.chbIsBase);
            this.Controls.Add(this.txtParentMeasure);
            this.Controls.Add(this.txtMeasureValue);
            this.Controls.Add(this.txtForeignName);
            this.Controls.Add(this.txtFullName);
            this.Controls.Add(this.txtShortName);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.lblBase);
            this.Controls.Add(this.lblForeign);
            this.Controls.Add(this.lblMeasureValue);
            this.Controls.Add(this.lblFullName);
            this.Controls.Add(this.lblShortName);
            this.Controls.Add(this.pnlBottom);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(440, 220);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(440, 220);
            this.Name = "MeasureForm";
            this.Text = "Единица измерения";
            this.pnlBottom.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblShortName;
        private System.Windows.Forms.Label lblFullName;
        private System.Windows.Forms.Label lblForeign;
        private System.Windows.Forms.Label lblBase;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.TextBox txtFullName;
        private System.Windows.Forms.TextBox txtForeignName;
        private System.Windows.Forms.TextBox txtParentMeasure;
        private System.Windows.Forms.CheckBox chbIsBase;
        private System.Windows.Forms.Button btnSelectMeasure;
        private System.Windows.Forms.Label lblMeasureValue;
        private System.Windows.Forms.TextBox txtMeasureValue;
        private System.Windows.Forms.TextBox txtShortName;

    }
}