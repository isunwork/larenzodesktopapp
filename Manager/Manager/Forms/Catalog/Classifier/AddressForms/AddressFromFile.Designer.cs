﻿namespace LaRenzo.Forms.Catalog.Classifier.AddressForms
{
    partial class AddressFromFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddressFromFile));
            this.panel1 = new System.Windows.Forms.Panel();
            this.LoadAddressWizard = new DevExpress.XtraWizard.WizardControl();
            this.Start = new DevExpress.XtraWizard.WelcomeWizardPage();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.txtPanel = new System.Windows.Forms.Panel();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.topPanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPanel = new System.Windows.Forms.Panel();
            this.lblSecond = new System.Windows.Forms.Label();
            this.lblFirst = new System.Windows.Forms.Label();
            this.lblWarning = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.LoadWizard1 = new DevExpress.XtraWizard.WizardPage();
            this.progressLoadFromFile = new DevExpress.XtraWaitForm.ProgressPanel();
            this.CompleateLoadWizard = new DevExpress.XtraWizard.CompletionWizardPage();
            this.loadedGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ChoosePage = new DevExpress.XtraWizard.WizardPage();
            this.mainDataGrid = new System.Windows.Forms.DataGridView();
            this.IsCheckedColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.AddressColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnUnSelectAll = new System.Windows.Forms.Button();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.LoadWizard2 = new DevExpress.XtraWizard.WizardPage();
            this.progressLoadFromGrid = new DevExpress.XtraWaitForm.ProgressPanel();
            this.AddressDlg = new System.Windows.Forms.OpenFileDialog();
            this.LoadFromFile = new System.ComponentModel.BackgroundWorker();
            this.SaveToDataBase = new System.ComponentModel.BackgroundWorker();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LoadAddressWizard)).BeginInit();
            this.LoadAddressWizard.SuspendLayout();
            this.Start.SuspendLayout();
            this.mainPanel.SuspendLayout();
            this.txtPanel.SuspendLayout();
            this.topPanel.SuspendLayout();
            this.tabPanel.SuspendLayout();
            this.LoadWizard1.SuspendLayout();
            this.CompleateLoadWizard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loadedGridView)).BeginInit();
            this.ChoosePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataGrid)).BeginInit();
            this.panel2.SuspendLayout();
            this.LoadWizard2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LoadAddressWizard);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(430, 321);
            this.panel1.TabIndex = 1;
            // 
            // LoadAddressWizard
            // 
            this.LoadAddressWizard.CancelText = "Отмена";
            this.LoadAddressWizard.Controls.Add(this.Start);
            this.LoadAddressWizard.Controls.Add(this.LoadWizard1);
            this.LoadAddressWizard.Controls.Add(this.CompleateLoadWizard);
            this.LoadAddressWizard.Controls.Add(this.ChoosePage);
            this.LoadAddressWizard.Controls.Add(this.LoadWizard2);
            this.LoadAddressWizard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LoadAddressWizard.FinishText = "&Завершить";
            this.LoadAddressWizard.Location = new System.Drawing.Point(0, 0);
            this.LoadAddressWizard.Name = "LoadAddressWizard";
            this.LoadAddressWizard.NextText = "&Далее >";
            this.LoadAddressWizard.Pages.AddRange(new DevExpress.XtraWizard.BaseWizardPage[] {
            this.Start,
            this.LoadWizard1,
            this.ChoosePage,
            this.LoadWizard2,
            this.CompleateLoadWizard});
            this.LoadAddressWizard.PreviousText = "< &Назад";
            this.LoadAddressWizard.Size = new System.Drawing.Size(430, 321);
            this.LoadAddressWizard.Text = "Назад";
            this.LoadAddressWizard.WizardStyle = DevExpress.XtraWizard.WizardStyle.WizardAero;
            this.LoadAddressWizard.NextClick += new DevExpress.XtraWizard.WizardCommandButtonClickEventHandler(this.LoadAddressWizard_NextClick);
            // 
            // Start
            // 
            this.Start.Controls.Add(this.mainPanel);
            this.Start.IntroductionText = "Этот помошник поможет вам загрузить адреса в базу данных из текстовых файлов";
            this.Start.Name = "Start";
            this.Start.ProceedText = "Нажмите Далее чтобы продолжить";
            this.Start.Size = new System.Drawing.Size(370, 159);
            this.Start.Text = "Выберите файл для загрузки адресов";
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(this.txtPanel);
            this.mainPanel.Controls.Add(this.topPanel);
            this.mainPanel.Controls.Add(this.tabPanel);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 0);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(370, 159);
            this.mainPanel.TabIndex = 3;
            // 
            // txtPanel
            // 
            this.txtPanel.Controls.Add(this.btnSelect);
            this.txtPanel.Controls.Add(this.btnClear);
            this.txtPanel.Controls.Add(this.txtPath);
            this.txtPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtPanel.Location = new System.Drawing.Point(0, 111);
            this.txtPanel.Name = "txtPanel";
            this.txtPanel.Size = new System.Drawing.Size(370, 40);
            this.txtPanel.TabIndex = 5;
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(317, 6);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(24, 24);
            this.btnSelect.TabIndex = 4;
            this.btnSelect.Text = "...";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.select_Click);
            // 
            // btnClear
            // 
            this.btnClear.Image = global::LaRenzo.Properties.Resources.cross;
            this.btnClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClear.Location = new System.Drawing.Point(343, 6);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(24, 24);
            this.btnClear.TabIndex = 3;
            this.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(3, 8);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(308, 20);
            this.txtPath.TabIndex = 0;
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.label1);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 92);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(370, 19);
            this.topPanel.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Файл с адресным классификатором:";
            // 
            // tabPanel
            // 
            this.tabPanel.Controls.Add(this.lblSecond);
            this.tabPanel.Controls.Add(this.lblFirst);
            this.tabPanel.Controls.Add(this.lblWarning);
            this.tabPanel.Controls.Add(this.lblTitle);
            this.tabPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabPanel.Location = new System.Drawing.Point(0, 0);
            this.tabPanel.Name = "tabPanel";
            this.tabPanel.Size = new System.Drawing.Size(370, 92);
            this.tabPanel.TabIndex = 4;
            // 
            // lblSecond
            // 
            this.lblSecond.AutoSize = true;
            this.lblSecond.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblSecond.Location = new System.Drawing.Point(3, 71);
            this.lblSecond.Name = "lblSecond";
            this.lblSecond.Size = new System.Drawing.Size(278, 13);
            this.lblSecond.TabIndex = 2;
            this.lblSecond.Text = "2) Каждый адрес в файле начинается с новой строки";
            // 
            // lblFirst
            // 
            this.lblFirst.AutoSize = true;
            this.lblFirst.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblFirst.Location = new System.Drawing.Point(3, 52);
            this.lblFirst.Name = "lblFirst";
            this.lblFirst.Size = new System.Drawing.Size(179, 13);
            this.lblFirst.TabIndex = 2;
            this.lblFirst.Text = "1) Текстовый файл формата \".txt\"";
            // 
            // lblWarning
            // 
            this.lblWarning.AutoSize = true;
            this.lblWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblWarning.ForeColor = System.Drawing.Color.Firebrick;
            this.lblWarning.Location = new System.Drawing.Point(3, 9);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(70, 13);
            this.lblWarning.TabIndex = 1;
            this.lblWarning.Text = "Внимание!";
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTitle.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblTitle.Location = new System.Drawing.Point(3, 22);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(364, 30);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Для корректной загрузки адресного классификатора из файла необходимо чтобы он соо" +
    "тветствовал следующим требованиям:";
            // 
            // LoadWizard1
            // 
            this.LoadWizard1.AllowBack = false;
            this.LoadWizard1.Controls.Add(this.progressLoadFromFile);
            this.LoadWizard1.Name = "LoadWizard1";
            this.LoadWizard1.Size = new System.Drawing.Size(370, 159);
            this.LoadWizard1.Text = "Загрузка данных из файла";
            // 
            // progressLoadFromFile
            // 
            this.progressLoadFromFile.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.progressLoadFromFile.Appearance.Options.UseBackColor = true;
            this.progressLoadFromFile.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.progressLoadFromFile.AppearanceCaption.Options.UseFont = true;
            this.progressLoadFromFile.AppearanceDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.progressLoadFromFile.AppearanceDescription.Options.UseFont = true;
            this.progressLoadFromFile.Caption = "Пожалуйста подождите";
            this.progressLoadFromFile.Description = "Идет загрузка ...";
            this.progressLoadFromFile.ImageHorzOffset = 60;
            this.progressLoadFromFile.Location = new System.Drawing.Point(3, 3);
            this.progressLoadFromFile.Name = "progressLoadFromFile";
            this.progressLoadFromFile.Size = new System.Drawing.Size(364, 171);
            this.progressLoadFromFile.TabIndex = 0;
            // 
            // CompleateLoadWizard
            // 
            this.CompleateLoadWizard.AllowBack = false;
            this.CompleateLoadWizard.Controls.Add(this.loadedGridView);
            this.CompleateLoadWizard.FinishText = "";
            this.CompleateLoadWizard.Name = "CompleateLoadWizard";
            this.CompleateLoadWizard.ProceedText = "Для выхода нажмите Завершить";
            this.CompleateLoadWizard.Size = new System.Drawing.Size(370, 159);
            this.CompleateLoadWizard.Text = "Адреса успешно загружены";
            // 
            // loadedGridView
            // 
            this.loadedGridView.AllowUserToAddRows = false;
            this.loadedGridView.AllowUserToDeleteRows = false;
            this.loadedGridView.BackgroundColor = System.Drawing.Color.White;
            this.loadedGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.loadedGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1});
            this.loadedGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loadedGridView.Location = new System.Drawing.Point(0, 0);
            this.loadedGridView.Name = "loadedGridView";
            this.loadedGridView.ReadOnly = true;
            this.loadedGridView.RowHeadersVisible = false;
            this.loadedGridView.Size = new System.Drawing.Size(370, 159);
            this.loadedGridView.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Address";
            this.dataGridViewTextBoxColumn1.HeaderText = "Адрес";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 360;
            // 
            // ChoosePage
            // 
            this.ChoosePage.AllowBack = false;
            this.ChoosePage.Controls.Add(this.mainDataGrid);
            this.ChoosePage.Controls.Add(this.panel2);
            this.ChoosePage.Name = "ChoosePage";
            this.ChoosePage.Size = new System.Drawing.Size(370, 159);
            this.ChoosePage.Text = "Выберите адреса для загрузки в базу данных";
            // 
            // mainDataGrid
            // 
            this.mainDataGrid.AllowUserToAddRows = false;
            this.mainDataGrid.AllowUserToDeleteRows = false;
            this.mainDataGrid.AllowUserToOrderColumns = true;
            this.mainDataGrid.BackgroundColor = System.Drawing.Color.White;
            this.mainDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mainDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IsCheckedColumn,
            this.AddressColumn});
            this.mainDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainDataGrid.Location = new System.Drawing.Point(0, 23);
            this.mainDataGrid.Name = "mainDataGrid";
            this.mainDataGrid.RowHeadersVisible = false;
            this.mainDataGrid.Size = new System.Drawing.Size(370, 136);
            this.mainDataGrid.TabIndex = 2;
            this.mainDataGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.mainDataGrid_CellContentClick);
            // 
            // IsCheckedColumn
            // 
            this.IsCheckedColumn.DataPropertyName = "IsChecked";
            this.IsCheckedColumn.HeaderText = "Загрузить";
            this.IsCheckedColumn.Name = "IsCheckedColumn";
            this.IsCheckedColumn.ReadOnly = true;
            this.IsCheckedColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IsCheckedColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.IsCheckedColumn.Width = 65;
            // 
            // AddressColumn
            // 
            this.AddressColumn.DataPropertyName = "Address";
            this.AddressColumn.HeaderText = "Адрес";
            this.AddressColumn.Name = "AddressColumn";
            this.AddressColumn.ReadOnly = true;
            this.AddressColumn.Width = 300;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnUnSelectAll);
            this.panel2.Controls.Add(this.btnSelectAll);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(370, 23);
            this.panel2.TabIndex = 1;
            // 
            // btnUnSelectAll
            // 
            this.btnUnSelectAll.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUnSelectAll.BackgroundImage")));
            this.btnUnSelectAll.Location = new System.Drawing.Point(25, 3);
            this.btnUnSelectAll.Name = "btnUnSelectAll";
            this.btnUnSelectAll.Size = new System.Drawing.Size(18, 18);
            this.btnUnSelectAll.TabIndex = 1;
            this.btnUnSelectAll.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnUnSelectAll.UseCompatibleTextRendering = true;
            this.btnUnSelectAll.UseVisualStyleBackColor = true;
            this.btnUnSelectAll.Click += new System.EventHandler(this.unSelectAll_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.BackgroundImage = global::LaRenzo.Properties.Resources.check_box_icon;
            this.btnSelectAll.Location = new System.Drawing.Point(3, 3);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(18, 18);
            this.btnSelectAll.TabIndex = 0;
            this.btnSelectAll.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnSelectAll.UseCompatibleTextRendering = true;
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.selectAll_Click);
            // 
            // LoadWizard2
            // 
            this.LoadWizard2.AllowBack = false;
            this.LoadWizard2.Controls.Add(this.progressLoadFromGrid);
            this.LoadWizard2.Name = "LoadWizard2";
            this.LoadWizard2.Size = new System.Drawing.Size(370, 159);
            this.LoadWizard2.Text = "Запись адресов в базу данных";
            // 
            // progressLoadFromGrid
            // 
            this.progressLoadFromGrid.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.progressLoadFromGrid.Appearance.Options.UseBackColor = true;
            this.progressLoadFromGrid.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.progressLoadFromGrid.AppearanceCaption.Options.UseFont = true;
            this.progressLoadFromGrid.AppearanceDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.progressLoadFromGrid.AppearanceDescription.Options.UseFont = true;
            this.progressLoadFromGrid.Caption = "Пожалуйста подождите";
            this.progressLoadFromGrid.Description = "Идет загрузка ...";
            this.progressLoadFromGrid.ImageHorzOffset = 60;
            this.progressLoadFromGrid.Location = new System.Drawing.Point(3, 3);
            this.progressLoadFromGrid.Name = "progressLoadFromGrid";
            this.progressLoadFromGrid.Size = new System.Drawing.Size(364, 171);
            this.progressLoadFromGrid.TabIndex = 1;
            // 
            // LoadFromFile
            // 
            this.LoadFromFile.DoWork += new System.ComponentModel.DoWorkEventHandler(this.LoadFromFile_DoWork);
            this.LoadFromFile.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.LoadFromFile_RunWorkerCompleted);
            // 
            // SaveToDataBase
            // 
            this.SaveToDataBase.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.SaveToDataBase_RunWorkerCompleted);
            // 
            // AddressFromFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 321);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(446, 360);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(446, 360);
            this.Name = "AddressFromFile";
            this.Text = "Загрузка адресного классификатора";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AddressFromFile_FormClosing);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LoadAddressWizard)).EndInit();
            this.LoadAddressWizard.ResumeLayout(false);
            this.Start.ResumeLayout(false);
            this.mainPanel.ResumeLayout(false);
            this.txtPanel.ResumeLayout(false);
            this.txtPanel.PerformLayout();
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            this.tabPanel.ResumeLayout(false);
            this.tabPanel.PerformLayout();
            this.LoadWizard1.ResumeLayout(false);
            this.CompleateLoadWizard.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.loadedGridView)).EndInit();
            this.ChoosePage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainDataGrid)).EndInit();
            this.panel2.ResumeLayout(false);
            this.LoadWizard2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.OpenFileDialog AddressDlg;
        private System.ComponentModel.BackgroundWorker LoadFromFile;
        private DevExpress.XtraWizard.WelcomeWizardPage Start;
        private DevExpress.XtraWizard.WizardPage LoadWizard1;
        private DevExpress.XtraWizard.CompletionWizardPage CompleateLoadWizard;
        private System.Windows.Forms.TextBox txtPath;
        private DevExpress.XtraWizard.WizardPage ChoosePage;
        private DevExpress.XtraWizard.WizardPage LoadWizard2;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel tabPanel;
        private System.Windows.Forms.Panel txtPanel;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSelect;
        public DevExpress.XtraWaitForm.ProgressPanel progressLoadFromFile;
        public DevExpress.XtraWaitForm.ProgressPanel progressLoadFromGrid;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnSelectAll;
        private System.Windows.Forms.Button btnUnSelectAll;
        private System.ComponentModel.BackgroundWorker SaveToDataBase;
        private DevExpress.XtraWizard.WizardControl LoadAddressWizard;
        private System.Windows.Forms.DataGridView mainDataGrid;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsCheckedColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddressColumn;
        private System.Windows.Forms.DataGridView loadedGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.Label lblSecond;
        private System.Windows.Forms.Label lblFirst;
        private System.Windows.Forms.Label lblWarning;
        private System.Windows.Forms.Label lblTitle;
    }
}