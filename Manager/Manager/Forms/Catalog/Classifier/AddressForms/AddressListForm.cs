﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Catalogs.Addresses;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Addresses;
using LaRenzo.Forms.StaffForms;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.Classifier.AddressForms
{
    public partial class AddressListForm : Form
    {
        public AddressListForm()
        {
            InitializeComponent();

            btnAdd.Click += async (s, e) => await btnAdd_Click();
        }

        private async Task btnAdd_Click()
        {
            var addAddressDialog = new SimpleTextDialogForm
            {
                FieldName = Resources.AddressFieldText,
                Text = Resources.AddAddressDialogText
            };

            if (DialogResult.OK == addAddressDialog.ShowDialog(this))
            {
                await Content.AddressManager.AddAddress(new Address { Name = addAddressDialog.Result });
                FillAddressDataSources();
            }
        }

        private void FillAddressDataSources()
        {
            var first = gridViewMain.TopRowIndex;
            var id = gridViewMain.GetDataSourceRowIndex(gridViewMain.FocusedRowHandle);
            var address = Content.AddressManager.GetAddressList();
            gridControlMain.DataSource = address;
            gridViewMain.FocusedRowHandle = gridViewMain.GetRowHandle(id);
            gridViewMain.MakeRowVisible(first);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            FillAddressDataSources();
        }

        private void AddressesForm_Load(object sender, EventArgs e)
        {
            FillAddressDataSources();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            var selectedRow = (Address)gridViewMain.GetFocusedRow();
            if (selectedRow != null)
            {
                if (DialogResult.OK ==
                    MessageBox.Show(Resources.DeleteAddressMessage, Resources.WarningTitle, MessageBoxButtons.OKCancel,
                                    MessageBoxIcon.Question))
                {
                    Content.AddressManager.DeleteAddress(selectedRow.ID);
                    FillAddressDataSources();
                }
            }
            else
            {
                MessageBox.Show(Resources.AddressNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var selectedRow = (Address)gridViewMain.GetFocusedRow();
            if (selectedRow != null)
            {
                var editAddressDialog = new AddressEditForm(selectedRow);

                if (DialogResult.OK == editAddressDialog.ShowDialog(this))
                {
                    FillAddressDataSources();
                }
            }
            else
            {
                MessageBox.Show(Resources.AddressNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
        }

        private void btnLoadFromFile_Click(object sender, EventArgs e)
        {
            new AddressFromFile().ShowDialog();
        }

        private void gridViewMain_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            gridViewMain.FindPanelVisible = true;
        }
    }
}
