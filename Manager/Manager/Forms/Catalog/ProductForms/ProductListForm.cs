﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Products;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.ProductForms
{
    public partial class ProductListForm : Form
    {
        public ProductListForm()
        {
            InitializeComponent();
        }

        private void ProductListForm_Load(object sender, EventArgs e)
        {
            try
            {
                LoadCategoriesTree();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

        private int LoadCategoriesTree()
        {
            int catId = 0;
            var nodes = treeListCategories.FocusedNode;
            treeListCategories.DataSource = Content.ProductManager.GetCategoriesList();
            
            var nodeAll = treeListCategories.AppendNode(null, null);
            
            nodeAll.SetValue(NameCol, "Все");
            nodeAll.SetValue(IdCol, 0);
            
            treeListCategories.SetNodeIndex(nodeAll, 0);
            if (nodes != null && treeListCategories.Nodes.Count >= nodes.Id + 1)
            {
                treeListCategories.FocusedNode = treeListCategories.Nodes[nodes.Id + 1];
                catId = (int)treeListCategories.FocusedNode.GetValue(IdCol);
            }

            return catId;
        }

        private void LoadProductList(int catId)
        {
            if (catId != 0)
            {
                var first = mainGridView.TopRowIndex;
                var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);
                mainGridControl.DataSource = Content.ProductManager.GetAllProductsInCategory(catId, btnShowHide.Checked).OrderBy(x => x.Name).ToList();
                mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
                mainGridView.MakeRowVisible(first);
            }
            else
            {
                mainGridControl.DataSource = Content.ProductManager.GetAllProductsInCategory(catId, btnShowHide.Checked).OrderBy(x => x.Name).ToList();
            }
        }

        private void treeListCategories_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            if (e.Node != null)
            {
                var id = (int)e.Node.GetValue(IdCol);
                LoadProductList(id);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var productForm = new ProductForm();
            if (productForm.ShowDialog() == DialogResult.OK)
            {
                if (treeListCategories.FocusedNode != null)
                {
                    var id = (int)treeListCategories.FocusedNode.GetValue(IdCol);
                    LoadProductList(id);
                }
            }
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            var selectedValue = (Product)mainGridView.GetFocusedRow();
            if (selectedValue != null)
            {
                GetProductForm(false, selectedValue.ID);
            }
            else
            {
                MessageBox.Show(Resources.ProductIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void GetProductForm(bool isEdit, int id)
        {
            var product = Content.ProductManager.GetById(id);
            
            var productForm = new ProductForm(isEdit, product);

            if (productForm.ShowDialog() == DialogResult.OK)                
            {    
                if (treeListCategories.FocusedNode != null)
                {
                    var identy = (int)treeListCategories.FocusedNode.GetValue(IdCol);
                    LoadProductList(identy);
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var selectedValue = (Product) mainGridView.GetFocusedRow();
            if (selectedValue != null)
            {
                GetProductForm(true, selectedValue.ID);
            }
            else
            {
                MessageBox.Show(Resources.ProductIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }


        private void btnRefresh_Click(object sender, EventArgs e)
        {
            int catId = LoadCategoriesTree();
            LoadProductList(catId);
        }

        private void mainGridView_DoubleClick(object sender, EventArgs e)
        {
            var selectedValue = (Product) mainGridView.GetFocusedRow();
            if (selectedValue != null)
            {
                GetProductForm(true, selectedValue.ID);   
            }
            else
            {
                MessageBox.Show(Resources.ProductIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (
                MessageBox.Show(Resources.DeleteProductQuestion, Resources.WarningTitle,
                                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                var selectedValue = (Product) mainGridView.GetFocusedRow();
                if (selectedValue != null)
                {
                    try
                    {
                        Content.ProductManager.Remove(selectedValue.ID);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(string.Format(Resources.DeleteProductError, ex.Message),
                                        Resources.WarningTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }

                btnRefresh_Click(sender, e);
            }
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            var currentRow = (Product)view.GetRow(e.RowHandle);

            if (currentRow.IsMarkToDelete)
                e.Appearance.BackColor = Color.Salmon;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }

        private void btnShowHide_Click(object sender, EventArgs e)
        {
            btnShowHide.Checked = !btnShowHide.Checked;
            btnRefresh_Click(sender, e);
        }
    }
}