﻿namespace LaRenzo.Forms.Catalog.ProductForms
{
    partial class ProductListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductListForm));
            this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.treeListCategories = new DevExpress.XtraTreeList.TreeList();
            this.NameCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.IdCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnIsSet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnMeasure = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnMinAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.mainToolstrip = new System.Windows.Forms.ToolStrip();
            this.btnAdd = new System.Windows.Forms.ToolStripButton();
            this.separator = new System.Windows.Forms.ToolStripSeparator();
            this.btnCopy = new System.Windows.Forms.ToolStripButton();
            this.btnEdit = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.separator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.searchButton = new System.Windows.Forms.ToolStripButton();
            this.btnShowHide = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListCategories)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            this.mainToolstrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainSplitContainer
            // 
            this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitContainer.Location = new System.Drawing.Point(0, 31);
            this.mainSplitContainer.Name = "mainSplitContainer";
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.Controls.Add(this.treeListCategories);
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.Controls.Add(this.mainGridControl);
            this.mainSplitContainer.Size = new System.Drawing.Size(612, 311);
            this.mainSplitContainer.SplitterDistance = 204;
            this.mainSplitContainer.TabIndex = 0;
            // 
            // treeListCategories
            // 
            this.treeListCategories.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.NameCol,
            this.IdCol});
            this.treeListCategories.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListCategories.DragNodesMode = DevExpress.XtraTreeList.TreeListDragNodesMode.Standard;
            this.treeListCategories.Location = new System.Drawing.Point(0, 0);
            this.treeListCategories.Name = "treeListCategories";
            this.treeListCategories.OptionsBehavior.DragNodes = true;
            this.treeListCategories.OptionsBehavior.Editable = false;
            this.treeListCategories.OptionsBehavior.EnableFiltering = true;
            this.treeListCategories.ParentFieldName = "ParentCategoryID";
            this.treeListCategories.Size = new System.Drawing.Size(204, 311);
            this.treeListCategories.TabIndex = 8;
            this.treeListCategories.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeListCategories_FocusedNodeChanged);
            // 
            // NameCol
            // 
            this.NameCol.Caption = "Категории";
            this.NameCol.FieldName = "Name";
            this.NameCol.MinWidth = 37;
            this.NameCol.Name = "NameCol";
            this.NameCol.Visible = true;
            this.NameCol.VisibleIndex = 0;
            // 
            // IdCol
            // 
            this.IdCol.Caption = "ID";
            this.IdCol.FieldName = "ID";
            this.IdCol.Name = "IdCol";
            // 
            // mainGridControl
            // 
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl.Location = new System.Drawing.Point(0, 0);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(404, 311);
            this.mainGridControl.TabIndex = 8;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnId,
            this.columnName,
            this.columnIsSet,
            this.columnMeasure,
            this.columnMinAmount});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.mainGridView.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.mainGridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.mainGridView_RowStyle);
            this.mainGridView.DoubleClick += new System.EventHandler(this.mainGridView_DoubleClick);
            // 
            // columnId
            // 
            this.columnId.Caption = "ИД";
            this.columnId.FieldName = "ID";
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.AllowEdit = false;
            this.columnId.OptionsColumn.ReadOnly = true;
            this.columnId.Visible = true;
            this.columnId.VisibleIndex = 0;
            this.columnId.Width = 67;
            // 
            // columnName
            // 
            this.columnName.Caption = "Наименование";
            this.columnName.FieldName = "Name";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.OptionsColumn.ReadOnly = true;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 1;
            this.columnName.Width = 419;
            // 
            // columnIsSet
            // 
            this.columnIsSet.Caption = "Составной";
            this.columnIsSet.FieldName = "IsSetProduct";
            this.columnIsSet.Name = "columnIsSet";
            this.columnIsSet.OptionsColumn.AllowEdit = false;
            this.columnIsSet.OptionsColumn.ReadOnly = true;
            this.columnIsSet.Visible = true;
            this.columnIsSet.VisibleIndex = 2;
            // 
            // columnMeasure
            // 
            this.columnMeasure.Caption = "Ед.Изм";
            this.columnMeasure.FieldName = "Measure.ShortName";
            this.columnMeasure.Name = "columnMeasure";
            this.columnMeasure.OptionsColumn.AllowEdit = false;
            this.columnMeasure.OptionsColumn.ReadOnly = true;
            this.columnMeasure.Visible = true;
            this.columnMeasure.VisibleIndex = 3;
            // 
            // columnMinAmount
            // 
            this.columnMinAmount.Caption = "Минимальный остаток";
            this.columnMinAmount.FieldName = "MinAmount";
            this.columnMinAmount.Name = "columnMinAmount";
            this.columnMinAmount.OptionsColumn.AllowEdit = false;
            this.columnMinAmount.OptionsColumn.ReadOnly = true;
            this.columnMinAmount.Visible = true;
            this.columnMinAmount.VisibleIndex = 4;
            // 
            // mainToolstrip
            // 
            this.mainToolstrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mainToolstrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.separator,
            this.btnCopy,
            this.btnEdit,
            this.btnDelete,
            this.separator2,
            this.btnRefresh,
            this.toolStripSeparator1,
            this.searchButton,
            this.btnShowHide});
            this.mainToolstrip.Location = new System.Drawing.Point(0, 0);
            this.mainToolstrip.Name = "mainToolstrip";
            this.mainToolstrip.Size = new System.Drawing.Size(612, 31);
            this.mainToolstrip.TabIndex = 7;
            this.mainToolstrip.Text = "toolStrip1";
            // 
            // btnAdd
            // 
            this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdd.Image = global::LaRenzo.Properties.Resources.add_wh;
            this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(28, 28);
            this.btnAdd.Text = "Создать коэфициент";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // separator
            // 
            this.separator.Name = "separator";
            this.separator.Size = new System.Drawing.Size(6, 31);
            // 
            // btnCopy
            // 
            this.btnCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCopy.Image = global::LaRenzo.Properties.Resources.copy;
            this.btnCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(28, 28);
            this.btnCopy.Text = "Скопировать коэфициент";
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEdit.Image = global::LaRenzo.Properties.Resources.edit_wh;
            this.btnEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(28, 28);
            this.btnEdit.Text = "Редактировать коэфициент";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = global::LaRenzo.Properties.Resources.close_wh;
            this.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(28, 28);
            this.btnDelete.Text = "Пометить на удаление";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // separator2
            // 
            this.separator2.Name = "separator2";
            this.separator2.Size = new System.Drawing.Size(6, 31);
            // 
            // btnRefresh
            // 
            this.btnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRefresh.Image = global::LaRenzo.Properties.Resources.refresh;
            this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(28, 28);
            this.btnRefresh.Text = "Обновить";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // searchButton
            // 
            this.searchButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.searchButton.Image = global::LaRenzo.Properties.Resources.search;
            this.searchButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(28, 28);
            this.searchButton.Text = "Найти";
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // btnShowHide
            // 
            this.btnShowHide.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnShowHide.Image = ((System.Drawing.Image)(resources.GetObject("btnShowHide.Image")));
            this.btnShowHide.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnShowHide.Name = "btnShowHide";
            this.btnShowHide.Size = new System.Drawing.Size(61, 28);
            this.btnShowHide.Text = "Скрытые";
            this.btnShowHide.Click += new System.EventHandler(this.btnShowHide_Click);
            // 
            // ProductListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 342);
            this.Controls.Add(this.mainSplitContainer);
            this.Controls.Add(this.mainToolstrip);
            this.Name = "ProductListForm";
            this.Text = "Продукты";
            this.Load += new System.EventHandler(this.ProductListForm_Load);
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
            this.mainSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListCategories)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            this.mainToolstrip.ResumeLayout(false);
            this.mainToolstrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer mainSplitContainer;
        private System.Windows.Forms.ToolStrip mainToolstrip;
        private System.Windows.Forms.ToolStripButton btnAdd;
        private System.Windows.Forms.ToolStripSeparator separator;
        private System.Windows.Forms.ToolStripButton btnCopy;
        private System.Windows.Forms.ToolStripButton btnEdit;
        private System.Windows.Forms.ToolStripButton btnDelete;
        private System.Windows.Forms.ToolStripSeparator separator2;
        private System.Windows.Forms.ToolStripButton btnRefresh;
        private DevExpress.XtraTreeList.TreeList treeListCategories;
        private DevExpress.XtraTreeList.Columns.TreeListColumn NameCol;
        private DevExpress.XtraTreeList.Columns.TreeListColumn IdCol;
        private DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private DevExpress.XtraGrid.Columns.GridColumn columnMeasure;
        private DevExpress.XtraGrid.Columns.GridColumn columnIsSet;
        private DevExpress.XtraGrid.Columns.GridColumn columnMinAmount;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton searchButton;
        private System.Windows.Forms.ToolStripButton btnShowHide;
    }
}