﻿using System;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Products;

namespace LaRenzo.Forms.Catalog.ProductForms
{
    public partial class SelectProductCategoryForm : Form
    {
        public ProductCategory ProductCategory { set; get; }

        public SelectProductCategoryForm()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void SelectProductCategoryForm_Load(object sender, EventArgs e)
        {
            LoadCategoriesTree();
        }

        private void LoadCategoriesTree()
        {
            treeListCategories.DataSource = Content.ProductManager.GetCategoriesList();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            var selectedValue = (int?)treeListCategories.FocusedNode.GetValue(IdCol);
            if (selectedValue != null)
            {
                ProductCategory = Content.ProductManager.GetCategoryById(selectedValue.Value);
                DialogResult = DialogResult.OK;
            }
        }

    }
}