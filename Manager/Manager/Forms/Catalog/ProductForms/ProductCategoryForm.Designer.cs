﻿namespace LaRenzo.Forms.Catalog.ProductForms
{
    partial class ProductCategoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblCoefficient = new System.Windows.Forms.Label();
            this.lblMinValue = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtParent = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtMeasure = new System.Windows.Forms.TextBox();
            this.btnSelectMeasure = new System.Windows.Forms.Button();
            this.btnSelectParent = new System.Windows.Forms.Button();
            this.pnlBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.btnSave);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 98);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(464, 24);
            this.pnlBottom.TabIndex = 4;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.Location = new System.Drawing.Point(288, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 22);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Записать";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.Location = new System.Drawing.Point(375, 0);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 22);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblCoefficient
            // 
            this.lblCoefficient.AutoSize = true;
            this.lblCoefficient.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCoefficient.Location = new System.Drawing.Point(29, 41);
            this.lblCoefficient.Name = "lblCoefficient";
            this.lblCoefficient.Size = new System.Drawing.Size(113, 13);
            this.lblCoefficient.TabIndex = 14;
            this.lblCoefficient.Text = "Категория родитель:";
            // 
            // lblMinValue
            // 
            this.lblMinValue.AutoSize = true;
            this.lblMinValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblMinValue.Location = new System.Drawing.Point(43, 15);
            this.lblMinValue.Name = "lblMinValue";
            this.lblMinValue.Size = new System.Drawing.Size(99, 13);
            this.lblMinValue.TabIndex = 15;
            this.lblMinValue.Text = "Наименование:";
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(342, 12);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(100, 20);
            this.txtId.TabIndex = 22;
            this.txtId.Tag = "id";
            this.txtId.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Единица измерения:";
            // 
            // txtParent
            // 
            this.txtParent.Location = new System.Drawing.Point(148, 38);
            this.txtParent.Name = "txtParent";
            this.txtParent.ReadOnly = true;
            this.txtParent.Size = new System.Drawing.Size(264, 20);
            this.txtParent.TabIndex = 22;
            this.txtParent.Tag = "parentid";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(148, 12);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(294, 20);
            this.txtName.TabIndex = 22;
            this.txtName.Tag = "Name";
            // 
            // txtMeasure
            // 
            this.txtMeasure.Location = new System.Drawing.Point(148, 64);
            this.txtMeasure.Name = "txtMeasure";
            this.txtMeasure.ReadOnly = true;
            this.txtMeasure.Size = new System.Drawing.Size(264, 20);
            this.txtMeasure.TabIndex = 22;
            this.txtMeasure.Tag = "measureid";
            // 
            // btnSelectMeasure
            // 
            this.btnSelectMeasure.Location = new System.Drawing.Point(418, 67);
            this.btnSelectMeasure.Name = "btnSelectMeasure";
            this.btnSelectMeasure.Size = new System.Drawing.Size(24, 20);
            this.btnSelectMeasure.TabIndex = 27;
            this.btnSelectMeasure.Text = "...";
            this.btnSelectMeasure.UseVisualStyleBackColor = true;
            this.btnSelectMeasure.Click += new System.EventHandler(this.btnSelectMeasure_Click);
            // 
            // btnSelectParent
            // 
            this.btnSelectParent.Location = new System.Drawing.Point(418, 37);
            this.btnSelectParent.Name = "btnSelectParent";
            this.btnSelectParent.Size = new System.Drawing.Size(24, 20);
            this.btnSelectParent.TabIndex = 27;
            this.btnSelectParent.Text = "...";
            this.btnSelectParent.UseVisualStyleBackColor = true;
            this.btnSelectParent.Click += new System.EventHandler(this.btnSelectParent_Click);
            // 
            // ProductCategoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 122);
            this.Controls.Add(this.btnSelectParent);
            this.Controls.Add(this.btnSelectMeasure);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtMeasure);
            this.Controls.Add(this.txtParent);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.lblCoefficient);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblMinValue);
            this.Controls.Add(this.pnlBottom);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(480, 160);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(480, 160);
            this.Name = "ProductCategoryForm";
            this.Text = "Категория продуктов";
            this.pnlBottom.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblCoefficient;
        private System.Windows.Forms.Label lblMinValue;
        public System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtParent;
        public System.Windows.Forms.TextBox txtName;
        public System.Windows.Forms.TextBox txtMeasure;
        private System.Windows.Forms.Button btnSelectMeasure;
        private System.Windows.Forms.Button btnSelectParent;
    }
}