﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Products;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.ProductForms
{
    public partial class SelectProductForm : Form
    {
		
		/// <summary> Список продуктов на добавление </summary>
		public List<Product> ProductsList { set; get; }

        public SelectProductForm()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
		{
            DialogResult = DialogResult.Cancel;
        }

        private void SelectProductForm_Load(object sender, EventArgs e)
        {
            ProductsList = new List<Product>();
			
			try
            {
                LoadCategoriesTree();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

        private void LoadCategoriesTree()
        {
            treeListCategories.DataSource = Content.ProductManager.GetCategoriesList();
        }


		//________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Выбрать" </summary>
		////=====================================
        private void btnSelect_Click(object sender, EventArgs e)
        {
			if (ProductsList.Count > 0)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show(Resources.ProductIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }


        private void LoadProductList(int catId)
        {
            mainGridControl.DataSource = Content.ProductManager.GetAllProductsInCategory(catId).OrderBy(x => x.Name).ToList();
        }

        private void treeListCategories_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            if (e.Node != null)
            {
                var id = (int)e.Node.GetValue(IdCol);
                LoadProductList(id);
            }
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }


		//________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Двойной клик по "Список продуктов" </summary>
		////=======================================================
		private void mainGridControl_DoubleClick(object sender, EventArgs e)
		{
			var selectedValue = (Product)mainGridView.GetFocusedRow();

			if (selectedValue != null)
			{
				ProductsList.Add(selectedValue);
			}

			productsGridControl.DataSource = null;
			productsGridControl.DataSource = ProductsList;

		}


		//________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Двойной клик по "Выбранные список продуктов" </summary>
		////=================================================================
		private void productsGridControl_DoubleClick(object sender, EventArgs e)
		{
			var selectedValue = (Product)productGridView.GetFocusedRow();

			if (selectedValue != null)
			{
				ProductsList.Remove(selectedValue);
			}

			productsGridControl.DataSource = null;
			productsGridControl.DataSource = ProductsList;
		}
    }
}