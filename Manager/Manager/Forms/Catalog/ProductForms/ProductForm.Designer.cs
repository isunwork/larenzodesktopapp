﻿namespace LaRenzo.Forms.Catalog.ProductForms
{
    partial class ProductForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.pnlBottom = new System.Windows.Forms.Panel();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnSelectParent = new System.Windows.Forms.Button();
			this.txtName = new System.Windows.Forms.TextBox();
			this.txtMeasure = new System.Windows.Forms.TextBox();
			this.txtParent = new System.Windows.Forms.TextBox();
			this.lblCoefficient = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.lblMinValue = new System.Windows.Forms.Label();
			this.lblMinAmmount = new System.Windows.Forms.Label();
			this.btnSelectMeasure = new System.Windows.Forms.Button();
			this.numMinAmount = new System.Windows.Forms.NumericUpDown();
			this.Container = new System.Windows.Forms.TabControl();
			this.tbMainPage = new System.Windows.Forms.TabPage();
			this.mainGridControl = new DevExpress.XtraGrid.GridControl();
			this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnAmount = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnMeasure = new DevExpress.XtraGrid.Columns.GridColumn();
			this.mainToolstrip = new System.Windows.Forms.ToolStrip();
			this.btnAdd = new System.Windows.Forms.ToolStripButton();
			this.separator = new System.Windows.Forms.ToolStripSeparator();
			this.btnDelete = new System.Windows.Forms.ToolStripButton();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.partnerGridControl = new DevExpress.XtraGrid.GridControl();
			this.partnerGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.columnPartnerId = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnPartnerName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnPartnerPhone = new DevExpress.XtraGrid.Columns.GridColumn();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.btnAddPartner = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.btnDeletePartner = new System.Windows.Forms.ToolStripButton();
			this.chkProductSet = new System.Windows.Forms.CheckBox();
			this.txtId = new System.Windows.Forms.TextBox();
			this.lblFixPrice = new System.Windows.Forms.Label();
			this.numFixPrice = new System.Windows.Forms.NumericUpDown();
			this.pnlBottom.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numMinAmount)).BeginInit();
			this.Container.SuspendLayout();
			this.tbMainPage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
			this.mainToolstrip.SuspendLayout();
			this.tabPage1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.partnerGridControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.partnerGridView)).BeginInit();
			this.toolStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numFixPrice)).BeginInit();
			this.SuspendLayout();
			// 
			// pnlBottom
			// 
			this.pnlBottom.BackColor = System.Drawing.SystemColors.ScrollBar;
			this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pnlBottom.Controls.Add(this.btnSave);
			this.pnlBottom.Controls.Add(this.btnCancel);
			this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlBottom.Location = new System.Drawing.Point(0, 445);
			this.pnlBottom.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.pnlBottom.Name = "pnlBottom";
			this.pnlBottom.Size = new System.Drawing.Size(616, 29);
			this.pnlBottom.TabIndex = 5;
			// 
			// btnSave
			// 
			this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
			this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnSave.Location = new System.Drawing.Point(382, 0);
			this.btnSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(116, 27);
			this.btnSave.TabIndex = 3;
			this.btnSave.Text = "Записать";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
			this.btnCancel.Location = new System.Drawing.Point(498, 0);
			this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 27, 4);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(116, 27);
			this.btnCancel.TabIndex = 2;
			this.btnCancel.Text = "Отмена";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnSelectParent
			// 
			this.btnSelectParent.Location = new System.Drawing.Point(559, 41);
			this.btnSelectParent.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.btnSelectParent.Name = "btnSelectParent";
			this.btnSelectParent.Size = new System.Drawing.Size(32, 25);
			this.btnSelectParent.TabIndex = 34;
			this.btnSelectParent.Text = "...";
			this.btnSelectParent.UseVisualStyleBackColor = true;
			this.btnSelectParent.Click += new System.EventHandler(this.btnSelectParent_Click);
			// 
			// txtName
			// 
			this.txtName.Location = new System.Drawing.Point(199, 10);
			this.txtName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(391, 22);
			this.txtName.TabIndex = 31;
			this.txtName.Tag = "name";
			// 
			// txtMeasure
			// 
			this.txtMeasure.Location = new System.Drawing.Point(199, 74);
			this.txtMeasure.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtMeasure.Name = "txtMeasure";
			this.txtMeasure.ReadOnly = true;
			this.txtMeasure.Size = new System.Drawing.Size(351, 22);
			this.txtMeasure.TabIndex = 32;
			this.txtMeasure.Tag = "MeasureID";
			// 
			// txtParent
			// 
			this.txtParent.Location = new System.Drawing.Point(199, 42);
			this.txtParent.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtParent.Name = "txtParent";
			this.txtParent.ReadOnly = true;
			this.txtParent.Size = new System.Drawing.Size(351, 22);
			this.txtParent.TabIndex = 33;
			this.txtParent.Tag = "ProductCategoryID";
			// 
			// lblCoefficient
			// 
			this.lblCoefficient.AutoSize = true;
			this.lblCoefficient.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lblCoefficient.Location = new System.Drawing.Point(88, 46);
			this.lblCoefficient.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblCoefficient.Name = "lblCoefficient";
			this.lblCoefficient.Size = new System.Drawing.Size(91, 17);
			this.lblCoefficient.TabIndex = 28;
			this.lblCoefficient.Text = "Категория:";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(17, 78);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(163, 17);
			this.label1.TabIndex = 29;
			this.label1.Text = "Единица измерения:";
			// 
			// lblMinValue
			// 
			this.lblMinValue.AutoSize = true;
			this.lblMinValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lblMinValue.Location = new System.Drawing.Point(59, 14);
			this.lblMinValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMinValue.Name = "lblMinValue";
			this.lblMinValue.Size = new System.Drawing.Size(123, 17);
			this.lblMinValue.TabIndex = 30;
			this.lblMinValue.Text = "Наименование:";
			// 
			// lblMinAmmount
			// 
			this.lblMinAmmount.AutoSize = true;
			this.lblMinAmmount.Location = new System.Drawing.Point(17, 108);
			this.lblMinAmmount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMinAmmount.Name = "lblMinAmmount";
			this.lblMinAmmount.Size = new System.Drawing.Size(161, 17);
			this.lblMinAmmount.TabIndex = 36;
			this.lblMinAmmount.Text = "Минимальный остаток:";
			// 
			// btnSelectMeasure
			// 
			this.btnSelectMeasure.Location = new System.Drawing.Point(559, 73);
			this.btnSelectMeasure.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.btnSelectMeasure.Name = "btnSelectMeasure";
			this.btnSelectMeasure.Size = new System.Drawing.Size(32, 25);
			this.btnSelectMeasure.TabIndex = 35;
			this.btnSelectMeasure.Text = "...";
			this.btnSelectMeasure.UseVisualStyleBackColor = true;
			this.btnSelectMeasure.Click += new System.EventHandler(this.btnSelectMeasure_Click);
			// 
			// numMinAmount
			// 
			this.numMinAmount.DecimalPlaces = 2;
			this.numMinAmount.Location = new System.Drawing.Point(199, 106);
			this.numMinAmount.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.numMinAmount.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
			this.numMinAmount.Name = "numMinAmount";
			this.numMinAmount.Size = new System.Drawing.Size(392, 22);
			this.numMinAmount.TabIndex = 37;
			this.numMinAmount.Tag = "MinAmount";
			this.numMinAmount.ValueChanged += new System.EventHandler(this.numMinAmount_ValueChanged);
			// 
			// Container
			// 
			this.Container.Controls.Add(this.tbMainPage);
			this.Container.Controls.Add(this.tabPage1);
			this.Container.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.Container.Location = new System.Drawing.Point(0, 194);
			this.Container.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.Container.Name = "Container";
			this.Container.SelectedIndex = 0;
			this.Container.Size = new System.Drawing.Size(616, 251);
			this.Container.TabIndex = 38;
			// 
			// tbMainPage
			// 
			this.tbMainPage.Controls.Add(this.mainGridControl);
			this.tbMainPage.Controls.Add(this.mainToolstrip);
			this.tbMainPage.Location = new System.Drawing.Point(4, 25);
			this.tbMainPage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.tbMainPage.Name = "tbMainPage";
			this.tbMainPage.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.tbMainPage.Size = new System.Drawing.Size(608, 222);
			this.tbMainPage.TabIndex = 1;
			this.tbMainPage.Text = "Состав";
			this.tbMainPage.UseVisualStyleBackColor = true;
			// 
			// mainGridControl
			// 
			this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainGridControl.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.mainGridControl.Location = new System.Drawing.Point(4, 35);
			this.mainGridControl.MainView = this.mainGridView;
			this.mainGridControl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.mainGridControl.Name = "mainGridControl";
			this.mainGridControl.Size = new System.Drawing.Size(600, 183);
			this.mainGridControl.TabIndex = 9;
			this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
			// 
			// mainGridView
			// 
			this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnId,
            this.columnName,
            this.columnAmount,
            this.columnMeasure});
			this.mainGridView.GridControl = this.mainGridControl;
			this.mainGridView.Name = "mainGridView";
			// 
			// columnId
			// 
			this.columnId.Caption = "ИД";
			this.columnId.FieldName = "Product.ID";
			this.columnId.Name = "columnId";
			this.columnId.OptionsColumn.AllowEdit = false;
			this.columnId.OptionsColumn.ReadOnly = true;
			this.columnId.Visible = true;
			this.columnId.VisibleIndex = 0;
			this.columnId.Width = 58;
			// 
			// columnName
			// 
			this.columnName.Caption = "Наименование";
			this.columnName.FieldName = "Product.Name";
			this.columnName.Name = "columnName";
			this.columnName.OptionsColumn.AllowEdit = false;
			this.columnName.OptionsColumn.ReadOnly = true;
			this.columnName.Visible = true;
			this.columnName.VisibleIndex = 1;
			this.columnName.Width = 150;
			// 
			// columnAmount
			// 
			this.columnAmount.Caption = "Количество";
			this.columnAmount.FieldName = "Amount";
			this.columnAmount.Name = "columnAmount";
			this.columnAmount.Visible = true;
			this.columnAmount.VisibleIndex = 2;
			// 
			// columnMeasure
			// 
			this.columnMeasure.Caption = "Ед. Изм.";
			this.columnMeasure.FieldName = "Measure.ShortName";
			this.columnMeasure.Name = "columnMeasure";
			this.columnMeasure.OptionsColumn.AllowEdit = false;
			this.columnMeasure.OptionsColumn.ReadOnly = true;
			this.columnMeasure.Visible = true;
			this.columnMeasure.VisibleIndex = 3;
			// 
			// mainToolstrip
			// 
			this.mainToolstrip.ImageScalingSize = new System.Drawing.Size(24, 24);
			this.mainToolstrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.separator,
            this.btnDelete});
			this.mainToolstrip.Location = new System.Drawing.Point(4, 4);
			this.mainToolstrip.Name = "mainToolstrip";
			this.mainToolstrip.Size = new System.Drawing.Size(600, 31);
			this.mainToolstrip.TabIndex = 8;
			this.mainToolstrip.Text = "toolStrip1";
			// 
			// btnAdd
			// 
			this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnAdd.Image = global::LaRenzo.Properties.Resources.add_wh;
			this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(28, 28);
			this.btnAdd.Text = "Создать коэфициент";
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// separator
			// 
			this.separator.Name = "separator";
			this.separator.Size = new System.Drawing.Size(6, 31);
			// 
			// btnDelete
			// 
			this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnDelete.Image = global::LaRenzo.Properties.Resources.close_wh;
			this.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new System.Drawing.Size(28, 28);
			this.btnDelete.Text = "Пометить на удаление";
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.partnerGridControl);
			this.tabPage1.Controls.Add(this.toolStrip1);
			this.tabPage1.Location = new System.Drawing.Point(4, 25);
			this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.tabPage1.Size = new System.Drawing.Size(608, 222);
			this.tabPage1.TabIndex = 2;
			this.tabPage1.Text = "Поставщики";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// partnerGridControl
			// 
			this.partnerGridControl.Cursor = System.Windows.Forms.Cursors.Default;
			this.partnerGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.partnerGridControl.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.partnerGridControl.Location = new System.Drawing.Point(4, 35);
			this.partnerGridControl.MainView = this.partnerGridView;
			this.partnerGridControl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.partnerGridControl.Name = "partnerGridControl";
			this.partnerGridControl.Size = new System.Drawing.Size(600, 183);
			this.partnerGridControl.TabIndex = 5;
			this.partnerGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.partnerGridView});
			// 
			// partnerGridView
			// 
			this.partnerGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnPartnerId,
            this.columnPartnerName,
            this.columnPartnerPhone});
			this.partnerGridView.GridControl = this.partnerGridControl;
			this.partnerGridView.Name = "partnerGridView";
			// 
			// columnPartnerId
			// 
			this.columnPartnerId.Caption = "ИД";
			this.columnPartnerId.FieldName = "ID";
			this.columnPartnerId.Name = "columnPartnerId";
			this.columnPartnerId.OptionsColumn.AllowEdit = false;
			this.columnPartnerId.OptionsColumn.ReadOnly = true;
			this.columnPartnerId.Visible = true;
			this.columnPartnerId.VisibleIndex = 0;
			this.columnPartnerId.Width = 58;
			// 
			// columnPartnerName
			// 
			this.columnPartnerName.Caption = "Поставщик";
			this.columnPartnerName.FieldName = "Partner.PartnerName";
			this.columnPartnerName.Name = "columnPartnerName";
			this.columnPartnerName.OptionsColumn.AllowEdit = false;
			this.columnPartnerName.OptionsColumn.ReadOnly = true;
			this.columnPartnerName.Visible = true;
			this.columnPartnerName.VisibleIndex = 1;
			this.columnPartnerName.Width = 200;
			// 
			// columnPartnerPhone
			// 
			this.columnPartnerPhone.Caption = "Телефон";
			this.columnPartnerPhone.FieldName = "Partner.PartnerPhone";
			this.columnPartnerPhone.Name = "columnPartnerPhone";
			this.columnPartnerPhone.OptionsColumn.AllowEdit = false;
			this.columnPartnerPhone.OptionsColumn.ReadOnly = true;
			this.columnPartnerPhone.Visible = true;
			this.columnPartnerPhone.VisibleIndex = 2;
			this.columnPartnerPhone.Width = 163;
			// 
			// toolStrip1
			// 
			this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddPartner,
            this.toolStripSeparator1,
            this.btnDeletePartner});
			this.toolStrip1.Location = new System.Drawing.Point(4, 4);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(600, 31);
			this.toolStrip1.TabIndex = 9;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// btnAddPartner
			// 
			this.btnAddPartner.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnAddPartner.Image = global::LaRenzo.Properties.Resources.add_wh;
			this.btnAddPartner.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnAddPartner.Name = "btnAddPartner";
			this.btnAddPartner.Size = new System.Drawing.Size(28, 28);
			this.btnAddPartner.Text = "Создать коэфициент";
			this.btnAddPartner.Click += new System.EventHandler(this.btnAddPartner_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
			// 
			// btnDeletePartner
			// 
			this.btnDeletePartner.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnDeletePartner.Image = global::LaRenzo.Properties.Resources.close_wh;
			this.btnDeletePartner.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnDeletePartner.Name = "btnDeletePartner";
			this.btnDeletePartner.Size = new System.Drawing.Size(28, 28);
			this.btnDeletePartner.Text = "Пометить на удаление";
			this.btnDeletePartner.Click += new System.EventHandler(this.btnDeletePartner_Click);
			// 
			// chkProductSet
			// 
			this.chkProductSet.AutoSize = true;
			this.chkProductSet.Location = new System.Drawing.Point(199, 174);
			this.chkProductSet.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.chkProductSet.Name = "chkProductSet";
			this.chkProductSet.Size = new System.Drawing.Size(157, 21);
			this.chkProductSet.TabIndex = 39;
			this.chkProductSet.Tag = "IsSetProduct";
			this.chkProductSet.Text = "Составной продукт";
			this.chkProductSet.UseVisualStyleBackColor = true;
			this.chkProductSet.CheckedChanged += new System.EventHandler(this.chkProductSet_CheckedChanged);
			// 
			// txtId
			// 
			this.txtId.Location = new System.Drawing.Point(531, 10);
			this.txtId.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.txtId.Name = "txtId";
			this.txtId.Size = new System.Drawing.Size(59, 22);
			this.txtId.TabIndex = 31;
			this.txtId.Tag = "id";
			this.txtId.Visible = false;
			// 
			// lblFixPrice
			// 
			this.lblFixPrice.AutoSize = true;
			this.lblFixPrice.Location = new System.Drawing.Point(25, 140);
			this.lblFixPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblFixPrice.Name = "lblFixPrice";
			this.lblFixPrice.Size = new System.Drawing.Size(154, 17);
			this.lblFixPrice.TabIndex = 40;
			this.lblFixPrice.Text = "Фиксированная цена:";
			// 
			// numFixPrice
			// 
			this.numFixPrice.DecimalPlaces = 2;
			this.numFixPrice.Location = new System.Drawing.Point(199, 138);
			this.numFixPrice.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.numFixPrice.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
			this.numFixPrice.Name = "numFixPrice";
			this.numFixPrice.Size = new System.Drawing.Size(392, 22);
			this.numFixPrice.TabIndex = 41;
			this.numFixPrice.Tag = "FixPrice";
			this.numFixPrice.ValueChanged += new System.EventHandler(this.numFixPrice_ValueChanged);
			// 
			// ProductForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(616, 474);
			this.Controls.Add(this.numFixPrice);
			this.Controls.Add(this.lblFixPrice);
			this.Controls.Add(this.chkProductSet);
			this.Controls.Add(this.Container);
			this.Controls.Add(this.numMinAmount);
			this.Controls.Add(this.lblMinAmmount);
			this.Controls.Add(this.btnSelectParent);
			this.Controls.Add(this.btnSelectMeasure);
			this.Controls.Add(this.txtId);
			this.Controls.Add(this.txtName);
			this.Controls.Add(this.txtMeasure);
			this.Controls.Add(this.txtParent);
			this.Controls.Add(this.lblCoefficient);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.lblMinValue);
			this.Controls.Add(this.pnlBottom);
			this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.MaximizeBox = false;
			this.MaximumSize = new System.Drawing.Size(634, 519);
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(634, 494);
			this.Name = "ProductForm";
			this.Text = "Продукт";
			this.Load += new System.EventHandler(this.ProductForm_Load);
			this.pnlBottom.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.numMinAmount)).EndInit();
			this.Container.ResumeLayout(false);
			this.tbMainPage.ResumeLayout(false);
			this.tbMainPage.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
			this.mainToolstrip.ResumeLayout(false);
			this.mainToolstrip.PerformLayout();
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.partnerGridControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.partnerGridView)).EndInit();
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numFixPrice)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSelectParent;
        public System.Windows.Forms.TextBox txtName;
        public System.Windows.Forms.TextBox txtMeasure;
        public System.Windows.Forms.TextBox txtParent;
        private System.Windows.Forms.Label lblCoefficient;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMinValue;
        private System.Windows.Forms.Label lblMinAmmount;
        private System.Windows.Forms.Button btnSelectMeasure;
        private System.Windows.Forms.NumericUpDown numMinAmount;
        private System.Windows.Forms.TabControl Container;
        private System.Windows.Forms.TabPage tbMainPage;
        private DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private DevExpress.XtraGrid.Columns.GridColumn columnAmount;
        private DevExpress.XtraGrid.Columns.GridColumn columnMeasure;
        private System.Windows.Forms.ToolStrip mainToolstrip;
        private System.Windows.Forms.ToolStripButton btnAdd;
        private System.Windows.Forms.ToolStripSeparator separator;
        private System.Windows.Forms.ToolStripButton btnDelete;
        private System.Windows.Forms.CheckBox chkProductSet;
        public System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.TabPage tabPage1;
        private DevExpress.XtraGrid.GridControl partnerGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView partnerGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnPartnerId;
        private DevExpress.XtraGrid.Columns.GridColumn columnPartnerName;
        private DevExpress.XtraGrid.Columns.GridColumn columnPartnerPhone;
        private System.Windows.Forms.Label lblFixPrice;
        private System.Windows.Forms.NumericUpDown numFixPrice;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnAddPartner;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnDeletePartner;
    }
}