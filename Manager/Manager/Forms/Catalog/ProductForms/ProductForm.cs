﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Measures;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Measures;
using LaRenzo.DataRepository.Repositories.Catalogs.Partners;
using LaRenzo.DataRepository.Repositories.Catalogs.Products;
using LaRenzo.Forms.Catalog.Classifier.MeasuresForms;
using LaRenzo.Forms.Catalog.PartnersForms;
using LaRenzo.FormsProvider;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.ProductForms
{
    public partial class ProductForm : Form
    {
        private readonly bool _isEditWindow;

        private List<ItemOfProductSet> _itemOfProductSets;
        private List<PartnerOfProduct> _productPartnerses;

        private Measure _measure;

        private Measure Measure
        {
            set 
            {
                _measure = value;
                if (value != null)
                    txtMeasure.Text = value.Name;
            }
            get { return _measure; }
        }

        private ProductCategory _productCategory;

        private ProductCategory ProductCategory
        {
            set
            {
                _productCategory = value;
                if (value != null)
                    txtParent.Text = ProductCategory.Name;
            }
            get { return _productCategory; }
        }

        public ProductForm()
        {
            InitializeComponent();
            SetControlsParams();
        }

        private void SetControlsParams()
        {
            _productPartnerses = new List<PartnerOfProduct>();
            _itemOfProductSets = new List<ItemOfProductSet>();
            tbMainPage.Enabled = false;
        }

        public ProductForm(bool isEdit, Product product):this()
        {
            _isEditWindow = isEdit;
            
            WindowsFormProvider.FillControlsFromModel(this, product);

            numMinAmount.Value = Convert.ToDecimal(product.MinAmount, CultureInfo.InvariantCulture);
            numFixPrice.Value = Convert.ToDecimal(product.FixPrice, CultureInfo.InvariantCulture);

            Measure = Content.MeasureManager.GetMeasure(product.MeasureID);

            if (product.ProductCategoryID != 0)
                ProductCategory = Content.ProductManager.GetCategoryById(product.ProductCategoryID);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            if (!CheckRequireFields())
                return;

            var product = GetProductByControl();

            if (_isEditWindow)
            {
                Content.ProductManager.Update(new ProductData
                {
                    Product = product,
                    ProductSets = _itemOfProductSets,
                    Partners = _productPartnerses
                });
            }
            else
            {
                Content.ProductManager.Add(new ProductData
                {
                    Product = product,
                    ProductSets = _itemOfProductSets,
                    Partners = _productPartnerses
                });
            }

            DialogResult = DialogResult.OK;
        }

        private Product GetProductByControl()
        {
            var product = new Product
                {
                    ID = string.IsNullOrEmpty(txtId.Text) ? 0 : Convert.ToInt32(txtId.Text.Trim()),
                    Name = txtName.Text,
                    MeasureID = Measure.ID,
                    MinAmount = Convert.ToDecimal(numMinAmount.Value),
                    ProductCategoryID = ProductCategory.ID,
                    IsSetProduct = chkProductSet.Checked,
                    FixPrice = Convert.ToDouble(numFixPrice.Value),
                    BranchId = Content.BranchRepository.SelectedBranchId
                };

            return product;
        }

        private bool CheckRequireFields()
        {
            if (Measure == null)
            {
                SetError(btnSelectMeasure, Resources.MeasureIsNotSelected);
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtName.Text))
            {
                SetError(txtName, Resources.ProductNameIsNotFill);
                return false;
            }

            if (ProductCategory == null)
            {
                SetError(btnSelectParent, Resources.ProductCategoryIsNotSelected);
                return false;
            }

            return true;
        }

        private void SetError(Control control, string message)
        {
            var ep = new ErrorProvider();
            ep.SetError(control, message);
        }

        private void btnSelectParent_Click(object sender, EventArgs e)
        {
            var selectCategoryForm = new SelectProductCategoryForm();

            if (selectCategoryForm.ShowDialog() == DialogResult.OK)
            {
                ProductCategory = selectCategoryForm.ProductCategory;
            }
        }

        private void btnSelectMeasure_Click(object sender, EventArgs e)
        {
            var measureSelectForm = new SelectMeasureForm();

            if (measureSelectForm.ShowDialog() == DialogResult.OK)
            {
                Measure = measureSelectForm.Measure;
            }
        }

        private void chkProductSet_CheckedChanged(object sender, EventArgs e)
        {
            tbMainPage.Enabled = chkProductSet.Checked;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            mainGridControl.BeginUpdate();
            
            var selectProductForm = new SelectProductForm();

            if (selectProductForm.ShowDialog() == DialogResult.OK && selectProductForm.ProductsList != null && selectProductForm.ProductsList.Count > 0)
			{
				foreach (Product product in selectProductForm.ProductsList)
				{
					_itemOfProductSets.Add(new ItemOfProductSet
					{
						ID = -1,
						MeasureId = product.MeasureID,
						ProductId = product.ID,
						Measure = Content.MeasureManager.GetMeasure(product.MeasureID),
						Product = product
					});
				}
			}mainGridControl.DataSource = _itemOfProductSets;
            
            mainGridControl.EndUpdate();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Resources.DeleteFromProductSetQuestion, Resources.WarningTitle, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                mainGridControl.BeginUpdate();
                
                var selectedRow = (ItemOfProductSet) mainGridView.GetFocusedRow();
                if (selectedRow != null)
                {
                    _itemOfProductSets.Remove(selectedRow);
                }

                mainGridControl.EndUpdate();
            }
        }

        private void ProductForm_Load(object sender, EventArgs e)
        {
            LoadDetailItem();
        }

        private void LoadDetailItem()
        {
            if (string.IsNullOrEmpty(txtId.Text))
                return;

            _itemOfProductSets = Content.ProductManager.GetDetailProduct(Convert.ToInt32(txtId.Text));
            mainGridControl.DataSource = _itemOfProductSets;

            _productPartnerses = Content.PartnerManager.GetPartnerByProductId(Convert.ToInt32(txtId.Text));
            partnerGridControl.DataSource = _productPartnerses;
        }

        private void numMinAmount_ValueChanged(object sender, EventArgs e)
        {
            numMinAmount.Value = Math.Round(numMinAmount.Value, 3);
        }

        private void numFixPrice_ValueChanged(object sender, EventArgs e)
        {
            numFixPrice.Value = Math.Round(numFixPrice.Value, 3);
        }

        private void btnAddPartner_Click(object sender, EventArgs e)
        {
            partnerGridControl.BeginUpdate();
            partnerGridView.BeginUpdate();
            var selectedPartnerForm = new SelectPartnerForm();

            if (selectedPartnerForm.ShowDialog() == DialogResult.OK && selectedPartnerForm.Partner != null)
            {
                _productPartnerses.Add(new PartnerOfProduct
                    {
                        ID = -1,
                        PartnerId = selectedPartnerForm.Partner.ID,
                        Partner = selectedPartnerForm.Partner
                    });
            }

            partnerGridControl.DataSource = _productPartnerses;
            partnerGridView.EndUpdate();
            partnerGridControl.EndUpdate();
        }

        private void btnDeletePartner_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Resources.DeletePartnerFromProduct, Resources.WarningTitle, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                mainGridControl.BeginUpdate();

                var selectedRow = (PartnerOfProduct)partnerGridView.GetFocusedRow();
                if (selectedRow != null)
                {
                    _productPartnerses.Remove(selectedRow);
                }

                mainGridControl.EndUpdate();
            }
        }
    }
}