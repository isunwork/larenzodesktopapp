﻿namespace LaRenzo.Forms.DiscountCardForms
{
    partial class CouponDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tableFields = new System.Windows.Forms.TableLayoutPanel();
            this.labelAdditional = new System.Windows.Forms.Label();
            this.listBoxSelected = new System.Windows.Forms.ListBox();
            this.textBoxDiscountValue = new System.Windows.Forms.TextBox();
            this.labelCouponCount = new System.Windows.Forms.Label();
            this.textBoxCount = new System.Windows.Forms.TextBox();
            this.textBoxLength = new System.Windows.Forms.TextBox();
            this.labelLegth = new System.Windows.Forms.Label();
            this.buttonGenerate = new System.Windows.Forms.Button();
            this.labelStat = new System.Windows.Forms.Label();
            this.labelBrand = new System.Windows.Forms.Label();
            this.comboBoxBrand = new System.Windows.Forms.ComboBox();
            this.listBoxSelectedFor = new System.Windows.Forms.ListBox();
            this.checkBoxSumToApply = new System.Windows.Forms.CheckBox();
            this.textBoxSumToApply = new System.Windows.Forms.TextBox();
            this.textBoxApplyCount = new System.Windows.Forms.TextBox();
            this.labelApplyCount = new System.Windows.Forms.Label();
            this.labelFrom = new System.Windows.Forms.Label();
            this.labelTo = new System.Windows.Forms.Label();
            this.dateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerTo = new System.Windows.Forms.DateTimePicker();
            this.labelName = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.panelAdditional = new System.Windows.Forms.Panel();
            this.checkBoxApplyToAll = new System.Windows.Forms.CheckBox();
            this.checkBoxAllRequired = new System.Windows.Forms.CheckBox();
            this.panelSelectedFor = new System.Windows.Forms.Panel();
            this.checkBoxSelectedFor = new System.Windows.Forms.RadioButton();
            this.panelSelected = new System.Windows.Forms.Panel();
            this.checkBoxSelected = new System.Windows.Forms.RadioButton();
            this.listViewDiscountOverlap = new System.Windows.Forms.ListView();
            this.Position = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DiscontInPercent = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SumToApply = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DaysOfWeek = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TimeBegin = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TimeEnd = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonAddCouponDiscount = new System.Windows.Forms.Button();
            this.comboBoxDiscountType = new System.Windows.Forms.ComboBox();
            this.listBoxCategory = new System.Windows.Forms.ListBox();
            this.listBoxDish = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel.SuspendLayout();
            this.tableFields.SuspendLayout();
            this.panelAdditional.SuspendLayout();
            this.panelSelectedFor.SuspendLayout();
            this.panelSelected.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 3;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel.Controls.Add(this.tableFields, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.listBoxCategory, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.listBoxDish, 1, 0);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 1;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(1387, 945);
            this.tableLayoutPanel.TabIndex = 4;
            // 
            // tableFields
            // 
            this.tableFields.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableFields.AutoScroll = true;
            this.tableFields.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.tableFields.ColumnCount = 3;
            this.tableFields.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableFields.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableFields.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableFields.Controls.Add(this.labelAdditional, 0, 6);
            this.tableFields.Controls.Add(this.listBoxSelected, 1, 8);
            this.tableFields.Controls.Add(this.textBoxDiscountValue, 1, 2);
            this.tableFields.Controls.Add(this.labelCouponCount, 0, 9);
            this.tableFields.Controls.Add(this.textBoxCount, 1, 9);
            this.tableFields.Controls.Add(this.textBoxLength, 1, 10);
            this.tableFields.Controls.Add(this.labelLegth, 0, 10);
            this.tableFields.Controls.Add(this.buttonGenerate, 1, 14);
            this.tableFields.Controls.Add(this.labelStat, 0, 14);
            this.tableFields.Controls.Add(this.labelBrand, 0, 0);
            this.tableFields.Controls.Add(this.comboBoxBrand, 1, 0);
            this.tableFields.Controls.Add(this.listBoxSelectedFor, 1, 7);
            this.tableFields.Controls.Add(this.checkBoxSumToApply, 0, 3);
            this.tableFields.Controls.Add(this.textBoxSumToApply, 1, 3);
            this.tableFields.Controls.Add(this.textBoxApplyCount, 1, 11);
            this.tableFields.Controls.Add(this.labelApplyCount, 0, 11);
            this.tableFields.Controls.Add(this.labelFrom, 0, 12);
            this.tableFields.Controls.Add(this.labelTo, 0, 13);
            this.tableFields.Controls.Add(this.dateTimePickerFrom, 1, 12);
            this.tableFields.Controls.Add(this.dateTimePickerTo, 1, 13);
            this.tableFields.Controls.Add(this.labelName, 0, 1);
            this.tableFields.Controls.Add(this.textBoxName, 1, 1);
            this.tableFields.Controls.Add(this.buttonSave, 2, 14);
            this.tableFields.Controls.Add(this.panelAdditional, 1, 6);
            this.tableFields.Controls.Add(this.panelSelectedFor, 0, 7);
            this.tableFields.Controls.Add(this.panelSelected, 0, 8);
            this.tableFields.Controls.Add(this.listViewDiscountOverlap, 0, 5);
            this.tableFields.Controls.Add(this.buttonAddCouponDiscount, 0, 4);
            this.tableFields.Controls.Add(this.comboBoxDiscountType, 0, 2);
            this.tableFields.Location = new System.Drawing.Point(420, 4);
            this.tableFields.Margin = new System.Windows.Forms.Padding(4);
            this.tableFields.Name = "tableFields";
            this.tableFields.RowCount = 15;
            this.tableFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 86F));
            this.tableFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableFields.Size = new System.Drawing.Size(963, 937);
            this.tableFields.TabIndex = 4;
            // 
            // labelAdditional
            // 
            this.labelAdditional.AutoSize = true;
            this.labelAdditional.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAdditional.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelAdditional.Location = new System.Drawing.Point(7, 383);
            this.labelAdditional.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelAdditional.Name = "labelAdditional";
            this.labelAdditional.Size = new System.Drawing.Size(709, 80);
            this.labelAdditional.TabIndex = 6;
            this.labelAdditional.Text = "Дополнительные настройки применимости";
            this.labelAdditional.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBoxSelected
            // 
            this.tableFields.SetColumnSpan(this.listBoxSelected, 2);
            this.listBoxSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxSelected.FormattingEnabled = true;
            this.listBoxSelected.ItemHeight = 16;
            this.listBoxSelected.Location = new System.Drawing.Point(727, 553);
            this.listBoxSelected.Margin = new System.Windows.Forms.Padding(4);
            this.listBoxSelected.Name = "listBoxSelected";
            this.listBoxSelected.Size = new System.Drawing.Size(712, 72);
            this.listBoxSelected.TabIndex = 0;
            this.listBoxSelected.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxSelected_MouseDoubleClick);
            // 
            // textBoxDiscountValue
            // 
            this.tableFields.SetColumnSpan(this.textBoxDiscountValue, 2);
            this.textBoxDiscountValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxDiscountValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxDiscountValue.Location = new System.Drawing.Point(727, 99);
            this.textBoxDiscountValue.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDiscountValue.Name = "textBoxDiscountValue";
            this.textBoxDiscountValue.Size = new System.Drawing.Size(712, 30);
            this.textBoxDiscountValue.TabIndex = 4;
            // 
            // labelCouponCount
            // 
            this.labelCouponCount.AutoSize = true;
            this.labelCouponCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCouponCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCouponCount.Location = new System.Drawing.Point(7, 632);
            this.labelCouponCount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelCouponCount.Name = "labelCouponCount";
            this.labelCouponCount.Size = new System.Drawing.Size(709, 43);
            this.labelCouponCount.TabIndex = 8;
            this.labelCouponCount.Text = "Сколько купонов сгенерировать";
            this.labelCouponCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelCouponCount.Click += new System.EventHandler(this.labelCouponCount_Click);
            // 
            // textBoxCount
            // 
            this.tableFields.SetColumnSpan(this.textBoxCount, 2);
            this.textBoxCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxCount.Location = new System.Drawing.Point(727, 636);
            this.textBoxCount.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCount.Name = "textBoxCount";
            this.textBoxCount.Size = new System.Drawing.Size(712, 30);
            this.textBoxCount.TabIndex = 9;
            this.textBoxCount.Text = "1";
            // 
            // textBoxLength
            // 
            this.tableFields.SetColumnSpan(this.textBoxLength, 2);
            this.textBoxLength.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxLength.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxLength.Location = new System.Drawing.Point(727, 682);
            this.textBoxLength.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxLength.Name = "textBoxLength";
            this.textBoxLength.Size = new System.Drawing.Size(712, 30);
            this.textBoxLength.TabIndex = 10;
            this.textBoxLength.Text = "5";
            // 
            // labelLegth
            // 
            this.labelLegth.AutoSize = true;
            this.labelLegth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelLegth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelLegth.Location = new System.Drawing.Point(7, 678);
            this.labelLegth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelLegth.Name = "labelLegth";
            this.labelLegth.Size = new System.Drawing.Size(709, 43);
            this.labelLegth.TabIndex = 11;
            this.labelLegth.Text = "Количество символов в купоне";
            this.labelLegth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelLegth.Click += new System.EventHandler(this.labelLegth_Click);
            // 
            // buttonGenerate
            // 
            this.buttonGenerate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonGenerate.Enabled = false;
            this.buttonGenerate.Location = new System.Drawing.Point(727, 866);
            this.buttonGenerate.Margin = new System.Windows.Forms.Padding(4);
            this.buttonGenerate.Name = "buttonGenerate";
            this.buttonGenerate.Size = new System.Drawing.Size(350, 149);
            this.buttonGenerate.TabIndex = 12;
            this.buttonGenerate.Text = "Сохранить и сгенерировать";
            this.buttonGenerate.UseVisualStyleBackColor = true;
            this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
            // 
            // labelStat
            // 
            this.labelStat.AutoSize = true;
            this.labelStat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStat.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelStat.Location = new System.Drawing.Point(7, 862);
            this.labelStat.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStat.Name = "labelStat";
            this.labelStat.Size = new System.Drawing.Size(709, 157);
            this.labelStat.TabIndex = 13;
            this.labelStat.Text = "По данному правилу уже создно: 100 купонов, из них использовано 83";
            this.labelStat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelBrand
            // 
            this.labelBrand.AutoSize = true;
            this.labelBrand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelBrand.Location = new System.Drawing.Point(7, 3);
            this.labelBrand.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelBrand.Name = "labelBrand";
            this.labelBrand.Size = new System.Drawing.Size(709, 43);
            this.labelBrand.TabIndex = 14;
            this.labelBrand.Text = "Бренд";
            this.labelBrand.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBrand.Click += new System.EventHandler(this.labelBrand_Click);
            // 
            // comboBoxBrand
            // 
            this.tableFields.SetColumnSpan(this.comboBoxBrand, 2);
            this.comboBoxBrand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBoxBrand.FormattingEnabled = true;
            this.comboBoxBrand.Location = new System.Drawing.Point(727, 7);
            this.comboBoxBrand.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxBrand.Name = "comboBoxBrand";
            this.comboBoxBrand.Size = new System.Drawing.Size(712, 33);
            this.comboBoxBrand.TabIndex = 15;
            this.comboBoxBrand.SelectedIndexChanged += new System.EventHandler(this.comboBoxBrand_SelectedIndexChanged);
            // 
            // listBoxSelectedFor
            // 
            this.tableFields.SetColumnSpan(this.listBoxSelectedFor, 2);
            this.listBoxSelectedFor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxSelectedFor.FormattingEnabled = true;
            this.listBoxSelectedFor.ItemHeight = 16;
            this.listBoxSelectedFor.Location = new System.Drawing.Point(727, 470);
            this.listBoxSelectedFor.Margin = new System.Windows.Forms.Padding(4);
            this.listBoxSelectedFor.Name = "listBoxSelectedFor";
            this.listBoxSelectedFor.Size = new System.Drawing.Size(712, 72);
            this.listBoxSelectedFor.TabIndex = 17;
            this.listBoxSelectedFor.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxSelectedFor_MouseDoubleClick);
            // 
            // checkBoxSumToApply
            // 
            this.checkBoxSumToApply.AutoSize = true;
            this.checkBoxSumToApply.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxSumToApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBoxSumToApply.Location = new System.Drawing.Point(7, 145);
            this.checkBoxSumToApply.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxSumToApply.Name = "checkBoxSumToApply";
            this.checkBoxSumToApply.Size = new System.Drawing.Size(709, 35);
            this.checkBoxSumToApply.TabIndex = 18;
            this.checkBoxSumToApply.Text = "Скидка применяется от суммы";
            this.checkBoxSumToApply.UseVisualStyleBackColor = true;
            this.checkBoxSumToApply.CheckedChanged += new System.EventHandler(this.checkBoxSumToApply_CheckedChanged);
            // 
            // textBoxSumToApply
            // 
            this.tableFields.SetColumnSpan(this.textBoxSumToApply, 2);
            this.textBoxSumToApply.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxSumToApply.Enabled = false;
            this.textBoxSumToApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxSumToApply.Location = new System.Drawing.Point(727, 145);
            this.textBoxSumToApply.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxSumToApply.Name = "textBoxSumToApply";
            this.textBoxSumToApply.Size = new System.Drawing.Size(712, 30);
            this.textBoxSumToApply.TabIndex = 19;
            // 
            // textBoxApplyCount
            // 
            this.tableFields.SetColumnSpan(this.textBoxApplyCount, 2);
            this.textBoxApplyCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxApplyCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxApplyCount.Location = new System.Drawing.Point(727, 728);
            this.textBoxApplyCount.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxApplyCount.Name = "textBoxApplyCount";
            this.textBoxApplyCount.Size = new System.Drawing.Size(712, 30);
            this.textBoxApplyCount.TabIndex = 20;
            this.textBoxApplyCount.Text = "1";
            // 
            // labelApplyCount
            // 
            this.labelApplyCount.AutoSize = true;
            this.labelApplyCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelApplyCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelApplyCount.Location = new System.Drawing.Point(7, 724);
            this.labelApplyCount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelApplyCount.Name = "labelApplyCount";
            this.labelApplyCount.Size = new System.Drawing.Size(709, 43);
            this.labelApplyCount.TabIndex = 21;
            this.labelApplyCount.Text = "Количество применений купона";
            this.labelApplyCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelApplyCount.Click += new System.EventHandler(this.labelApplyCount_Click);
            // 
            // labelFrom
            // 
            this.labelFrom.AutoSize = true;
            this.labelFrom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelFrom.Location = new System.Drawing.Point(7, 770);
            this.labelFrom.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelFrom.Name = "labelFrom";
            this.labelFrom.Size = new System.Drawing.Size(709, 43);
            this.labelFrom.TabIndex = 24;
            this.labelFrom.Text = "Действует с";
            this.labelFrom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelFrom.Click += new System.EventHandler(this.labelFrom_Click);
            // 
            // labelTo
            // 
            this.labelTo.AutoSize = true;
            this.labelTo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTo.Location = new System.Drawing.Point(7, 816);
            this.labelTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTo.Name = "labelTo";
            this.labelTo.Size = new System.Drawing.Size(709, 43);
            this.labelTo.TabIndex = 25;
            this.labelTo.Text = "Действует по";
            this.labelTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelTo.Click += new System.EventHandler(this.labelTo_Click);
            // 
            // dateTimePickerFrom
            // 
            this.tableFields.SetColumnSpan(this.dateTimePickerFrom, 2);
            this.dateTimePickerFrom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateTimePickerFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateTimePickerFrom.Location = new System.Drawing.Point(727, 774);
            this.dateTimePickerFrom.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePickerFrom.Name = "dateTimePickerFrom";
            this.dateTimePickerFrom.Size = new System.Drawing.Size(712, 30);
            this.dateTimePickerFrom.TabIndex = 26;
            // 
            // dateTimePickerTo
            // 
            this.tableFields.SetColumnSpan(this.dateTimePickerTo, 2);
            this.dateTimePickerTo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateTimePickerTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateTimePickerTo.Location = new System.Drawing.Point(727, 820);
            this.dateTimePickerTo.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePickerTo.Name = "dateTimePickerTo";
            this.dateTimePickerTo.Size = new System.Drawing.Size(712, 30);
            this.dateTimePickerTo.TabIndex = 27;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelName.Location = new System.Drawing.Point(7, 49);
            this.labelName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(709, 43);
            this.labelName.TabIndex = 28;
            this.labelName.Text = "Название купона";
            this.labelName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelName.Click += new System.EventHandler(this.labelName_Click);
            // 
            // textBoxName
            // 
            this.tableFields.SetColumnSpan(this.textBoxName, 2);
            this.textBoxName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxName.Location = new System.Drawing.Point(727, 53);
            this.textBoxName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(712, 30);
            this.textBoxName.TabIndex = 29;
            // 
            // buttonSave
            // 
            this.buttonSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSave.Enabled = false;
            this.buttonSave.Location = new System.Drawing.Point(1088, 866);
            this.buttonSave.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(351, 149);
            this.buttonSave.TabIndex = 30;
            this.buttonSave.Text = "Сохранить";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // panelAdditional
            // 
            this.tableFields.SetColumnSpan(this.panelAdditional, 2);
            this.panelAdditional.Controls.Add(this.checkBoxApplyToAll);
            this.panelAdditional.Controls.Add(this.checkBoxAllRequired);
            this.panelAdditional.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAdditional.Location = new System.Drawing.Point(723, 383);
            this.panelAdditional.Margin = new System.Windows.Forms.Padding(0);
            this.panelAdditional.Name = "panelAdditional";
            this.panelAdditional.Size = new System.Drawing.Size(720, 80);
            this.panelAdditional.TabIndex = 33;
            // 
            // checkBoxApplyToAll
            // 
            this.checkBoxApplyToAll.AutoSize = true;
            this.checkBoxApplyToAll.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBoxApplyToAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBoxApplyToAll.Location = new System.Drawing.Point(0, 34);
            this.checkBoxApplyToAll.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxApplyToAll.Name = "checkBoxApplyToAll";
            this.checkBoxApplyToAll.Padding = new System.Windows.Forms.Padding(0, 12, 0, 0);
            this.checkBoxApplyToAll.Size = new System.Drawing.Size(720, 36);
            this.checkBoxApplyToAll.TabIndex = 1;
            this.checkBoxApplyToAll.Text = "Применять ко всем блюдам";
            this.checkBoxApplyToAll.UseVisualStyleBackColor = true;
            // 
            // checkBoxAllRequired
            // 
            this.checkBoxAllRequired.AutoSize = true;
            this.checkBoxAllRequired.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBoxAllRequired.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBoxAllRequired.Location = new System.Drawing.Point(0, 0);
            this.checkBoxAllRequired.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxAllRequired.Name = "checkBoxAllRequired";
            this.checkBoxAllRequired.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.checkBoxAllRequired.Size = new System.Drawing.Size(720, 34);
            this.checkBoxAllRequired.TabIndex = 0;
            this.checkBoxAllRequired.Text = "Применять только если все блюда добавлены";
            this.checkBoxAllRequired.UseVisualStyleBackColor = true;
            // 
            // panelSelectedFor
            // 
            this.panelSelectedFor.Controls.Add(this.checkBoxSelectedFor);
            this.panelSelectedFor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSelectedFor.Location = new System.Drawing.Point(6, 469);
            this.panelSelectedFor.Name = "panelSelectedFor";
            this.panelSelectedFor.Size = new System.Drawing.Size(711, 74);
            this.panelSelectedFor.TabIndex = 34;
            // 
            // checkBoxSelectedFor
            // 
            this.checkBoxSelectedFor.AutoSize = true;
            this.checkBoxSelectedFor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxSelectedFor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.checkBoxSelectedFor.Location = new System.Drawing.Point(0, 0);
            this.checkBoxSelectedFor.Name = "checkBoxSelectedFor";
            this.checkBoxSelectedFor.Size = new System.Drawing.Size(711, 74);
            this.checkBoxSelectedFor.TabIndex = 0;
            this.checkBoxSelectedFor.TabStop = true;
            this.checkBoxSelectedFor.Text = "Блюда и/или категории необходимые для применения скидки";
            this.checkBoxSelectedFor.UseVisualStyleBackColor = true;
            this.checkBoxSelectedFor.CheckedChanged += new System.EventHandler(this.checkBoxSelectedFor_CheckedChanged);
            // 
            // panelSelected
            // 
            this.panelSelected.Controls.Add(this.checkBoxSelected);
            this.panelSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSelected.Location = new System.Drawing.Point(6, 552);
            this.panelSelected.Name = "panelSelected";
            this.panelSelected.Size = new System.Drawing.Size(711, 74);
            this.panelSelected.TabIndex = 35;
            // 
            // checkBoxSelected
            // 
            this.checkBoxSelected.AutoSize = true;
            this.checkBoxSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.checkBoxSelected.Location = new System.Drawing.Point(0, 0);
            this.checkBoxSelected.Name = "checkBoxSelected";
            this.checkBoxSelected.Size = new System.Drawing.Size(711, 74);
            this.checkBoxSelected.TabIndex = 0;
            this.checkBoxSelected.TabStop = true;
            this.checkBoxSelected.Text = "Блюда и/или категории блюд на которые распространяется скидка";
            this.checkBoxSelected.UseVisualStyleBackColor = true;
            this.checkBoxSelected.CheckedChanged += new System.EventHandler(this.checkBoxSelected_CheckedChanged);
            // 
            // listViewDiscountOverlap
            // 
            this.listViewDiscountOverlap.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Position,
            this.DiscontInPercent,
            this.SumToApply,
            this.DaysOfWeek,
            this.TimeBegin,
            this.TimeEnd});
            this.tableFields.SetColumnSpan(this.listViewDiscountOverlap, 3);
            this.listViewDiscountOverlap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewDiscountOverlap.HideSelection = false;
            this.listViewDiscountOverlap.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.listViewDiscountOverlap.Location = new System.Drawing.Point(6, 233);
            this.listViewDiscountOverlap.Name = "listViewDiscountOverlap";
            this.listViewDiscountOverlap.Size = new System.Drawing.Size(1434, 144);
            this.listViewDiscountOverlap.TabIndex = 37;
            this.listViewDiscountOverlap.UseCompatibleStateImageBehavior = false;
            this.listViewDiscountOverlap.View = System.Windows.Forms.View.Details;
            this.listViewDiscountOverlap.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewDiscountOverlap_MouseDoubleClick);
            // 
            // Position
            // 
            this.Position.Text = "";
            this.Position.Width = 43;
            // 
            // DiscontInPercent
            // 
            this.DiscontInPercent.Text = "Скидка в %";
            this.DiscontInPercent.Width = 109;
            // 
            // SumToApply
            // 
            this.SumToApply.Text = "Применяется от Σ ";
            this.SumToApply.Width = 141;
            // 
            // DaysOfWeek
            // 
            this.DaysOfWeek.Text = "Дни недели";
            this.DaysOfWeek.Width = 266;
            // 
            // TimeBegin
            // 
            this.TimeBegin.Text = "Действует с";
            this.TimeBegin.Width = 116;
            // 
            // TimeEnd
            // 
            this.TimeEnd.Text = "Действует до";
            this.TimeEnd.Width = 135;
            // 
            // buttonAddCouponDiscount
            // 
            this.tableFields.SetColumnSpan(this.buttonAddCouponDiscount, 3);
            this.buttonAddCouponDiscount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonAddCouponDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonAddCouponDiscount.Location = new System.Drawing.Point(6, 190);
            this.buttonAddCouponDiscount.Name = "buttonAddCouponDiscount";
            this.buttonAddCouponDiscount.Size = new System.Drawing.Size(1434, 34);
            this.buttonAddCouponDiscount.TabIndex = 38;
            this.buttonAddCouponDiscount.Text = "Переопределить скидку";
            this.buttonAddCouponDiscount.UseVisualStyleBackColor = true;
            this.buttonAddCouponDiscount.Click += new System.EventHandler(this.buttonAddCouponDiscount_Click);
            // 
            // comboBoxDiscountType
            // 
            this.comboBoxDiscountType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxDiscountType.FormattingEnabled = true;
            this.comboBoxDiscountType.Location = new System.Drawing.Point(6, 98);
            this.comboBoxDiscountType.Name = "comboBoxDiscountType";
            this.comboBoxDiscountType.Size = new System.Drawing.Size(711, 24);
            this.comboBoxDiscountType.TabIndex = 39;
            this.comboBoxDiscountType.SelectedIndexChanged += new System.EventHandler(this.comboBoxDiscountType_SelectedIndexChanged);
            // 
            // listBoxCategory
            // 
            this.listBoxCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxCategory.FormattingEnabled = true;
            this.listBoxCategory.ItemHeight = 16;
            this.listBoxCategory.Location = new System.Drawing.Point(4, 4);
            this.listBoxCategory.Margin = new System.Windows.Forms.Padding(4);
            this.listBoxCategory.Name = "listBoxCategory";
            this.listBoxCategory.Size = new System.Drawing.Size(200, 937);
            this.listBoxCategory.TabIndex = 5;
            this.listBoxCategory.SelectedIndexChanged += new System.EventHandler(this.listBoxCategory_SelectedIndexChanged);
            this.listBoxCategory.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxCategory_MouseDoubleClick);
            // 
            // listBoxDish
            // 
            this.listBoxDish.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxDish.FormattingEnabled = true;
            this.listBoxDish.ItemHeight = 16;
            this.listBoxDish.Location = new System.Drawing.Point(212, 4);
            this.listBoxDish.Margin = new System.Windows.Forms.Padding(4);
            this.listBoxDish.Name = "listBoxDish";
            this.listBoxDish.Size = new System.Drawing.Size(200, 937);
            this.listBoxDish.TabIndex = 6;
            this.listBoxDish.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxDish_MouseDoubleClick);
            // 
            // CouponDetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1387, 945);
            this.Controls.Add(this.tableLayoutPanel);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1327, 851);
            this.Name = "CouponDetailsForm";
            this.Text = "Настройка купона";
            this.Load += new System.EventHandler(this.CouponDetailsForm_Load);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableFields.ResumeLayout(false);
            this.tableFields.PerformLayout();
            this.panelAdditional.ResumeLayout(false);
            this.panelAdditional.PerformLayout();
            this.panelSelectedFor.ResumeLayout(false);
            this.panelSelectedFor.PerformLayout();
            this.panelSelected.ResumeLayout(false);
            this.panelSelected.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel tableFields;
        private System.Windows.Forms.Label labelAdditional;
        private System.Windows.Forms.ListBox listBoxSelected;
        private System.Windows.Forms.TextBox textBoxDiscountValue;
        private System.Windows.Forms.ListBox listBoxCategory;
        private System.Windows.Forms.ListBox listBoxDish;
        private System.Windows.Forms.Label labelCouponCount;
        private System.Windows.Forms.TextBox textBoxCount;
        private System.Windows.Forms.TextBox textBoxLength;
        private System.Windows.Forms.Label labelLegth;
        private System.Windows.Forms.Button buttonGenerate;
        private System.Windows.Forms.Label labelStat;
        private System.Windows.Forms.Label labelBrand;
        private System.Windows.Forms.ComboBox comboBoxBrand;
        private System.Windows.Forms.ListBox listBoxSelectedFor;
        private System.Windows.Forms.CheckBox checkBoxSumToApply;
        private System.Windows.Forms.TextBox textBoxSumToApply;
        private System.Windows.Forms.TextBox textBoxApplyCount;
        private System.Windows.Forms.Label labelApplyCount;
        private System.Windows.Forms.Label labelFrom;
        private System.Windows.Forms.Label labelTo;
        private System.Windows.Forms.DateTimePicker dateTimePickerFrom;
        private System.Windows.Forms.DateTimePicker dateTimePickerTo;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Panel panelAdditional;
        private System.Windows.Forms.CheckBox checkBoxApplyToAll;
        private System.Windows.Forms.CheckBox checkBoxAllRequired;
        private System.Windows.Forms.Panel panelSelectedFor;
        private System.Windows.Forms.RadioButton checkBoxSelectedFor;
        private System.Windows.Forms.Panel panelSelected;
        private System.Windows.Forms.RadioButton checkBoxSelected;
        private System.Windows.Forms.ListView listViewDiscountOverlap;
        private System.Windows.Forms.ColumnHeader DiscontInPercent;
        private System.Windows.Forms.ColumnHeader SumToApply;
        private System.Windows.Forms.ColumnHeader DaysOfWeek;
        private System.Windows.Forms.ColumnHeader TimeBegin;
        private System.Windows.Forms.ColumnHeader TimeEnd;
        private System.Windows.Forms.Button buttonAddCouponDiscount;
        private System.Windows.Forms.ColumnHeader Position;
        private System.Windows.Forms.ComboBox comboBoxDiscountType;
    }
}