﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.FormsProvider;

namespace LaRenzo.Forms.DiscountCardForms
{
    public partial class CouponListForm : Form
    {
        public CouponListForm()
        {
            InitializeComponent();
            btnAdd.Click += btnAdd_Click;
            listBoxCoupons.MouseDoubleClick += listBoxCoupons_MouseDoubleClick;
        }
        public void Initialize()
        {
            listBoxCoupons.DisplayMember = "Name";
            FillCouponsList();
            FillCodes();
        }

        private void FillCouponsList()
        {
            listBoxCoupons.Items.Clear();
            var list = Content.CouponRuleRepository.GetItems().OrderBy(y => y.Name);
            foreach (var coupon in list)
            {
                listBoxCoupons.Items.Add(coupon);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var form = new CouponDetailsForm();
            form.Closed += delegate { Initialize(); };
            SingletonFormProvider<CouponDetailsForm>.ActivateForm(() => form);

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Initialize();
        }

        private void listBoxCoupons_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = listBoxCoupons.IndexFromPoint(e.Location);
            if (index != ListBox.NoMatches)
            {
                var item = listBoxCoupons.Items[index];
                var form = new CouponDetailsForm((CouponRule)item);
                form.Closed += delegate { Initialize(); };
                SingletonFormProvider<CouponDetailsForm>.ActivateForm(() => form);
            }
        }

        private void listBoxCoupons_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillCodes();
        }

        private void FillCodes()
        {
            listViewCodes.Items.Clear();
            var selectedCouponRule = (CouponRule)listBoxCoupons.SelectedItem;
            if (selectedCouponRule != null)
            {
                var list = Content.CouponRepository.GetList(null, null, selectedCouponRule.ID);
                foreach (var coupon in list)
                {
                    var listViewItem = new ListViewItem(new[]
                    {
                        coupon.Code, coupon.ActivationCount.ToString(),
                        coupon.Activated ? "Активирован" : "Не активирован",
                        coupon.ActivatedCount.ToString(),
                        coupon.ID.ToString()
                    });
                    listViewCodes.Items.Add(listViewItem);
                }
            }
        }

        private void toolStripButtonDeleteGroups_Click(object sender, EventArgs e)
        {
            foreach (CouponRule coupon in listBoxCoupons.SelectedItems)
            {
                Content.CouponRuleRepository.DeleteItem(coupon);
            }
            Initialize();
        }

        private void toolStripButtonDeleteCoupons_Click(object sender, EventArgs e)
        {
            foreach (var coupon in listViewCodes.SelectedItems)
            {
                ListViewItem.ListViewSubItem codeId = ((ListViewItem)coupon).SubItems[4];
                var code = Content.CouponRepository.GetItem(Convert.ToInt32(codeId.Text));
                Content.CouponRepository.DeleteItem(code);
            }
            Initialize();
        }

        private void toolStripButtonAddCoupon_Click(object sender, EventArgs e)
        {
            var selectedCouponRule = (CouponRule)listBoxCoupons.SelectedItem;
            if (selectedCouponRule != null)
            {
                var form = new CouponEdit(selectedCouponRule.ID);
                SingletonFormProvider<CouponEdit>.ActivateForm(() => form);
            }
            else
            {
                MessageBox.Show("Выберите правило для добавления купона");
            }
        }

        private void listViewCodes_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            foreach (var coupon in listViewCodes.SelectedItems)
            {
                ListViewItem.ListViewSubItem codeId = ((ListViewItem)coupon).SubItems[4];
                var code = Content.CouponRepository.GetItem(Convert.ToInt32(codeId.Text));
                var form = new CouponEdit(code);
                SingletonFormProvider<CouponEdit>.ActivateForm(() => form);
            }
        }

        private void toolStripButtonShowReport_Click(object sender, EventArgs e)
        {
            var selectedCouponRule = (CouponRule)listBoxCoupons.SelectedItem;
            if (selectedCouponRule != null)
            {
                var form = new CouponRuleReportForm(selectedCouponRule);
                SingletonFormProvider<CouponRuleReportForm>.ActivateForm(() => form);
            }
            else
            {
                MessageBox.Show("Выберите правило для просмотра отчета");
            }
        }
    }
}
