﻿namespace LaRenzo.Forms.DiscountCardForms
{
    partial class CouponRuleReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CouponRuleReportView = new System.Windows.Forms.ListView();
            this.OrderId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.OrderCreatedDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ClientName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TotalCost = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TotalCostWithoutDiscount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Source = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Phone = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // CouponRuleReportView
            // 
            this.CouponRuleReportView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.OrderId,
            this.OrderCreatedDate,
            this.Phone,
            this.ClientName,
            this.TotalCost,
            this.TotalCostWithoutDiscount,
            this.Source});
            this.CouponRuleReportView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CouponRuleReportView.Location = new System.Drawing.Point(0, 0);
            this.CouponRuleReportView.Name = "CouponRuleReportView";
            this.CouponRuleReportView.Size = new System.Drawing.Size(1220, 450);
            this.CouponRuleReportView.TabIndex = 0;
            this.CouponRuleReportView.UseCompatibleStateImageBehavior = false;
            this.CouponRuleReportView.View = System.Windows.Forms.View.Details;
            this.CouponRuleReportView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.CouponRuleReportView_MouseDoubleClick);
            // 
            // OrderId
            // 
            this.OrderId.Text = "Заказ";
            // 
            // OrderCreatedDate
            // 
            this.OrderCreatedDate.Text = "Дата заказа";
            this.OrderCreatedDate.Width = 140;
            // 
            // ClientName
            // 
            this.ClientName.Text = "Имя клиента";
            this.ClientName.Width = 161;
            // 
            // TotalCost
            // 
            this.TotalCost.Text = "Сумма";
            this.TotalCost.Width = 145;
            // 
            // TotalCostWithoutDiscount
            // 
            this.TotalCostWithoutDiscount.Text = "Сумма без скидки";
            this.TotalCostWithoutDiscount.Width = 181;
            // 
            // Source
            // 
            this.Source.Text = "Источник заказв";
            this.Source.Width = 139;
            // 
            // Phone
            // 
            this.Phone.Text = "Телефон";
            this.Phone.Width = 170;
            // 
            // CouponRuleReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1220, 450);
            this.Controls.Add(this.CouponRuleReportView);
            this.Name = "CouponRuleReportForm";
            this.Text = "Отчет по группе купонов";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView CouponRuleReportView;
        private System.Windows.Forms.ColumnHeader OrderCreatedDate;
        private System.Windows.Forms.ColumnHeader ClientName;
        private System.Windows.Forms.ColumnHeader TotalCost;
        private System.Windows.Forms.ColumnHeader TotalCostWithoutDiscount;
        private System.Windows.Forms.ColumnHeader Source;
        private System.Windows.Forms.ColumnHeader OrderId;
        private System.Windows.Forms.ColumnHeader Phone;
    }
}