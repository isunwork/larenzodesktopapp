﻿namespace LaRenzo.Forms.DiscountCardForms
{
    partial class CouponDiscountEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelDiscountInPercent = new System.Windows.Forms.Label();
            this.textBoxDiscountInPercent = new System.Windows.Forms.TextBox();
            this.labelSumToApply = new System.Windows.Forms.Label();
            this.textBoxSumToApply = new System.Windows.Forms.TextBox();
            this.labelTimeBegin = new System.Windows.Forms.Label();
            this.labelTimeEnd = new System.Windows.Forms.Label();
            this.pickerTimeBegin = new System.Windows.Forms.DateTimePicker();
            this.pickerTimeEnd = new System.Windows.Forms.DateTimePicker();
            this.checkedListBoxDaysOfWeek = new System.Windows.Forms.CheckedListBox();
            this.labelDaysOfWeek = new System.Windows.Forms.Label();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonWeekends = new System.Windows.Forms.RadioButton();
            this.radioButtonWeekdays = new System.Windows.Forms.RadioButton();
            this.radioButtonAllDays = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelDiscountInPercent
            // 
            this.labelDiscountInPercent.AutoSize = true;
            this.labelDiscountInPercent.Location = new System.Drawing.Point(12, 6);
            this.labelDiscountInPercent.Name = "labelDiscountInPercent";
            this.labelDiscountInPercent.Size = new System.Drawing.Size(82, 17);
            this.labelDiscountInPercent.TabIndex = 0;
            this.labelDiscountInPercent.Text = "Скидка в %";
            // 
            // textBoxDiscountInPercent
            // 
            this.textBoxDiscountInPercent.Location = new System.Drawing.Point(158, 6);
            this.textBoxDiscountInPercent.Name = "textBoxDiscountInPercent";
            this.textBoxDiscountInPercent.Size = new System.Drawing.Size(100, 22);
            this.textBoxDiscountInPercent.TabIndex = 1;
            this.textBoxDiscountInPercent.TextChanged += new System.EventHandler(this.textBoxDiscountInPercent_TextChanged);
            // 
            // labelSumToApply
            // 
            this.labelSumToApply.AutoSize = true;
            this.labelSumToApply.Location = new System.Drawing.Point(12, 44);
            this.labelSumToApply.Name = "labelSumToApply";
            this.labelSumToApply.Size = new System.Drawing.Size(129, 17);
            this.labelSumToApply.TabIndex = 2;
            this.labelSumToApply.Text = "Применяется от Σ";
            // 
            // textBoxSumToApply
            // 
            this.textBoxSumToApply.Location = new System.Drawing.Point(158, 44);
            this.textBoxSumToApply.Name = "textBoxSumToApply";
            this.textBoxSumToApply.Size = new System.Drawing.Size(100, 22);
            this.textBoxSumToApply.TabIndex = 3;
            this.textBoxSumToApply.TextChanged += new System.EventHandler(this.textBoxSumToApply_TextChanged);
            // 
            // labelTimeBegin
            // 
            this.labelTimeBegin.AutoSize = true;
            this.labelTimeBegin.Location = new System.Drawing.Point(12, 82);
            this.labelTimeBegin.Name = "labelTimeBegin";
            this.labelTimeBegin.Size = new System.Drawing.Size(89, 17);
            this.labelTimeBegin.TabIndex = 4;
            this.labelTimeBegin.Text = "Действует с";
            // 
            // labelTimeEnd
            // 
            this.labelTimeEnd.AutoSize = true;
            this.labelTimeEnd.Location = new System.Drawing.Point(12, 120);
            this.labelTimeEnd.Name = "labelTimeEnd";
            this.labelTimeEnd.Size = new System.Drawing.Size(98, 17);
            this.labelTimeEnd.TabIndex = 5;
            this.labelTimeEnd.Text = "Действует до";
            // 
            // pickerTimeBegin
            // 
            this.pickerTimeBegin.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.pickerTimeBegin.Location = new System.Drawing.Point(158, 82);
            this.pickerTimeBegin.Name = "pickerTimeBegin";
            this.pickerTimeBegin.ShowUpDown = true;
            this.pickerTimeBegin.Size = new System.Drawing.Size(100, 22);
            this.pickerTimeBegin.TabIndex = 6;
            this.pickerTimeBegin.ValueChanged += new System.EventHandler(this.pickerTimeBegin_ValueChanged);
            // 
            // pickerTimeEnd
            // 
            this.pickerTimeEnd.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.pickerTimeEnd.Location = new System.Drawing.Point(158, 120);
            this.pickerTimeEnd.Name = "pickerTimeEnd";
            this.pickerTimeEnd.ShowUpDown = true;
            this.pickerTimeEnd.Size = new System.Drawing.Size(100, 22);
            this.pickerTimeEnd.TabIndex = 7;
            this.pickerTimeEnd.ValueChanged += new System.EventHandler(this.pickerTimeEnd_ValueChanged);
            // 
            // checkedListBoxDaysOfWeek
            // 
            this.checkedListBoxDaysOfWeek.FormattingEnabled = true;
            this.checkedListBoxDaysOfWeek.Items.AddRange(new object[] {
            "Понедельник",
            "Вторник",
            "Среда",
            "Четверг",
            "Пятница",
            "Суббота",
            "Воскресенье"});
            this.checkedListBoxDaysOfWeek.Location = new System.Drawing.Point(433, 6);
            this.checkedListBoxDaysOfWeek.Name = "checkedListBoxDaysOfWeek";
            this.checkedListBoxDaysOfWeek.Size = new System.Drawing.Size(229, 140);
            this.checkedListBoxDaysOfWeek.TabIndex = 8;
            this.checkedListBoxDaysOfWeek.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBoxDaysOfWeek_ItemCheck);
            // 
            // labelDaysOfWeek
            // 
            this.labelDaysOfWeek.AutoSize = true;
            this.labelDaysOfWeek.Location = new System.Drawing.Point(290, 6);
            this.labelDaysOfWeek.Name = "labelDaysOfWeek";
            this.labelDaysOfWeek.Size = new System.Drawing.Size(87, 17);
            this.labelDaysOfWeek.TabIndex = 9;
            this.labelDaysOfWeek.Text = "Дни недели";
            // 
            // buttonSave
            // 
            this.buttonSave.Enabled = false;
            this.buttonSave.Location = new System.Drawing.Point(433, 228);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(229, 23);
            this.buttonSave.TabIndex = 10;
            this.buttonSave.Text = "Сохранить";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Enabled = false;
            this.buttonDelete.ForeColor = System.Drawing.Color.Red;
            this.buttonDelete.Location = new System.Drawing.Point(198, 228);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(229, 23);
            this.buttonDelete.TabIndex = 11;
            this.buttonDelete.Text = "Удалить";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonWeekends);
            this.groupBox1.Controls.Add(this.radioButtonWeekdays);
            this.groupBox1.Controls.Add(this.radioButtonAllDays);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F);
            this.groupBox1.Location = new System.Drawing.Point(293, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(121, 100);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Выделить группу";
            // 
            // radioButtonWeekends
            // 
            this.radioButtonWeekends.AutoSize = true;
            this.radioButtonWeekends.Location = new System.Drawing.Point(7, 70);
            this.radioButtonWeekends.Name = "radioButtonWeekends";
            this.radioButtonWeekends.Size = new System.Drawing.Size(99, 17);
            this.radioButtonWeekends.TabIndex = 2;
            this.radioButtonWeekends.TabStop = true;
            this.radioButtonWeekends.Text = "Выходные дни";
            this.radioButtonWeekends.UseVisualStyleBackColor = true;
            this.radioButtonWeekends.CheckedChanged += new System.EventHandler(this.radioButtonWeekends_CheckedChanged);
            // 
            // radioButtonWeekdays
            // 
            this.radioButtonWeekdays.AutoSize = true;
            this.radioButtonWeekdays.Location = new System.Drawing.Point(7, 45);
            this.radioButtonWeekdays.Name = "radioButtonWeekdays";
            this.radioButtonWeekdays.Size = new System.Drawing.Size(85, 17);
            this.radioButtonWeekdays.TabIndex = 1;
            this.radioButtonWeekdays.TabStop = true;
            this.radioButtonWeekdays.Text = "Будние дни";
            this.radioButtonWeekdays.UseVisualStyleBackColor = true;
            this.radioButtonWeekdays.CheckedChanged += new System.EventHandler(this.radioButtonWeekdays_CheckedChanged);
            // 
            // radioButtonAllDays
            // 
            this.radioButtonAllDays.AutoSize = true;
            this.radioButtonAllDays.Location = new System.Drawing.Point(7, 20);
            this.radioButtonAllDays.Name = "radioButtonAllDays";
            this.radioButtonAllDays.Size = new System.Drawing.Size(67, 17);
            this.radioButtonAllDays.TabIndex = 0;
            this.radioButtonAllDays.TabStop = true;
            this.radioButtonAllDays.Text = "Все дни";
            this.radioButtonAllDays.UseVisualStyleBackColor = true;
            this.radioButtonAllDays.CheckedChanged += new System.EventHandler(this.radioButtonAllDays_CheckedChanged);
            // 
            // CouponDiscountEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 263);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.labelDaysOfWeek);
            this.Controls.Add(this.checkedListBoxDaysOfWeek);
            this.Controls.Add(this.pickerTimeEnd);
            this.Controls.Add(this.pickerTimeBegin);
            this.Controls.Add(this.labelTimeEnd);
            this.Controls.Add(this.labelTimeBegin);
            this.Controls.Add(this.textBoxSumToApply);
            this.Controls.Add(this.labelSumToApply);
            this.Controls.Add(this.textBoxDiscountInPercent);
            this.Controls.Add(this.labelDiscountInPercent);
            this.Name = "CouponDiscountEditForm";
            this.Text = "Добавление/редактирование скидки";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelDiscountInPercent;
        private System.Windows.Forms.TextBox textBoxDiscountInPercent;
        private System.Windows.Forms.Label labelSumToApply;
        private System.Windows.Forms.TextBox textBoxSumToApply;
        private System.Windows.Forms.Label labelTimeBegin;
        private System.Windows.Forms.Label labelTimeEnd;
        private System.Windows.Forms.DateTimePicker pickerTimeBegin;
        private System.Windows.Forms.DateTimePicker pickerTimeEnd;
        private System.Windows.Forms.CheckedListBox checkedListBoxDaysOfWeek;
        private System.Windows.Forms.Label labelDaysOfWeek;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonAllDays;
        private System.Windows.Forms.RadioButton radioButtonWeekends;
        private System.Windows.Forms.RadioButton radioButtonWeekdays;
    }
}