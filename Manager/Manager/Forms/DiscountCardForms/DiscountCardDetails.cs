﻿using System;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Addresses;
using LaRenzo.DataRepository.Repositories.Catalogs.DiscountCards;
using LaRenzo.Forms.Documents.DeliveryOrderForms;
using LaRenzo.Properties;

namespace LaRenzo.Forms.DiscountCardForms
{
	public partial class DiscountCardDetails : Form
	{
		private DiscountCard _card;
		private readonly bool _forUpdate;
		private readonly int _oldId;


		public DiscountCardDetails(int id)
		{
		    InitializeComponent();
		    mainGridView.DoubleClick += async (s,e)=> await gridView1_DoubleClick(s);
			_card = Content.DiscountCardManager.GetByID(id);
			_forUpdate = true;
			_oldId = id;
		}

		public DiscountCardDetails()
		{
			InitializeComponent();
            mainGridView.DoubleClick += async (s,e)=> await gridView1_DoubleClick(s);
		}

		private void Bind()
		{
			if (_card != null)
			{
				numCardNumber.Text = _card.CardNumber.ToString(CultureInfo.InvariantCulture);
				//numCardNumber.Enabled = true;
				txtName.Text = _card.Name;
				cmbStreet.Text = _card.Street;
				txtFlat.Text = _card.Appaertment.ToString();
				txtHouse.Text = _card.House;
				txtPorch.Text = _card.Porch.ToString();
				txtPhone.Text = _card.Phone;
				txtFatherName.Text = _card.FatherName;
				txtEmail.Text = _card.Email;


				// Устиновка элемента в комбобоксе в зависимости от значения скидки
				switch (_card.Discount)
				{
					case 3:
						discComboBox.SelectedIndex = 1;
						break;
					case 5:
						discComboBox.SelectedIndex = 2;
						break;
					case 7:
						discComboBox.SelectedIndex = 3;
						break;
					case 10:
						discComboBox.SelectedIndex = 3;
						break;
					case 15:
						discComboBox.SelectedIndex = 3;
						break;
					case 20:
						discComboBox.SelectedIndex = 3;
						break;
					default:
						discComboBox.SelectedIndex = 0;
						break;
				}


				mainGridControl.DataSource = Content.DiscountCardManager.GetOrdersByID(_card.ID);

			}
			else numCardNumber.Text = (Content.DiscountCardManager.GetMaxCardNumber() + 1).ToString(CultureInfo.InvariantCulture);
		}

		private void Parse()
		{
			if (_card == null)
			{
				_card = new DiscountCard();
			}

			_card.CardNumber = int.Parse(numCardNumber.Text ?? "");
			_card.Name = txtName.Text;
			_card.Street = cmbStreet.Text;
			_card.Appaertment = int.Parse(txtFlat.Text ?? "");
			_card.House = txtHouse.Text;
			_card.Porch = string.IsNullOrWhiteSpace(txtPorch.Text) ? (int?)null : int.Parse(txtPorch.Text);
			_card.Phone = txtPhone.Text;
			_card.FatherName = txtFatherName.Text;_card.Email = txtEmail.Text;

			// Если комбарь в "теме"
			if (discComboBox != null && discComboBox.SelectedIndex >= 0)
			{
				// Устиновка элемента в комбобоксе в зависимости от значения скидки
				switch (discComboBox.SelectedIndex)
				{
					case 1:
						_card.Discount = 3;
						break;
					case 2:
						_card.Discount = 5;
						break;
					case 3:
						_card.Discount = 7;
						break;
					case 4:
						_card.Discount = 10;
						break;
					case 5:
						_card.Discount = 15;
						break;
					case 6:
						_card.Discount = 20;
						break;
					default:
						_card.Discount = 0;
						break;
				}
			}
			else
			{
				_card.Discount = 0;
			}

		}



		private void btnSave_Click(object sender, EventArgs e)
		{
			Parse();

			if (!Content.DiscountCardManager.IsCardNumberValid(_card, _oldId))
			{
				MessageBox.Show(@"Карта с таким номером уже есть в базе данных");
				return;
			}

			if (_forUpdate)
			{
                Content.DiscountCardManager.Update(_card);
			}
			else
			{
                Content.DiscountCardManager.Add(_card);
			}

			Close();
		}


		private void gridView1_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
		{
			if (e.Column.Name == "columnOrderCostDiff")
			{
				var order = e.Row as Order;
				if (order != null) e.Value = order.TotalCostWithoutDiscount - order.TotalCost;
			}
		}

		private void DiscountCardDetails_Load(object sender, EventArgs e)
		{
			Bind();
		    var addresses = Content.AddressManager.GetAddressList();
			cmbStreet.AutoCompleteCustomSource.AddRange(addresses.Select(x => x.Name).ToArray());
		}

		private async Task gridView1_DoubleClick(object sender)
		{
			var view = (GridView)sender;

			Point pt = view.GridControl.PointToClient(MousePosition);

			GridHitInfo info = view.CalcHitInfo(pt);
			if (view.IsDataRow(info.RowHandle))
			{
				splashScreenManager1.ShowWaitForm();
				splashScreenManager1.SetWaitFormCaption("Открывается заказ...");
				splashScreenManager1.SetWaitFormDescription("Подождите немного");

				var form = new DeliveryOrderForm(DetailsFormModes.ViewByManager, (int)mainGridView.GetRowCellValue(info.RowHandle, columnID));
			    await form.Initialize();
				form.Show();

				splashScreenManager1.CloseWaitForm();
			}
		}
        
		private void TextPhone_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
		{

		}

		private void TextPhone_TextChanged(object sender, EventArgs e)
		{
			txtPhone.Text = txtPhone.Text.Trim();

			if (txtPhone.Text == Resources.Patern_8 || txtPhone.Text == Resources.Patern_7)
				txtPhone.Text = string.Empty;

			if (txtPhone.Text.StartsWith("2"))
			{
				txtPhone.Mask = Resources.Patern_000_00_00;
				var startIndex = txtPhone.Text.Length;

				if (startIndex >= 5) startIndex += 2;
				else if (startIndex >= 3) startIndex++;

				BeginInvoke(new Action(() => txtPhone.Select(startIndex, 0)));

			}
			else
			{
				txtPhone.Mask = Resources.Patern_000_000_00_00;
				if (txtPhone.Text.Length == 1)
					BeginInvoke(new Action(() => txtPhone.Select(2, 0)));

			}
		}
	}
}
