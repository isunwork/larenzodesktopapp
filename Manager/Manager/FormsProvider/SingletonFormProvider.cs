﻿using System;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.Forms.HelpForms;

namespace LaRenzo.FormsProvider
{
    public static class SingletonFormProvider<T> where T : Form
    {
        private static T _instance;

        private static void DisposeInstanse(object sender, FormClosedEventArgs e)
        {
            _instance = null;
        }

        public static Action GetShowFormAction(Func<T> constructor)
        {
            return () => ActivateForm(constructor);
        }

        public static T ActivateForm(Func<T> constructor)
        {
            var form = GetInstance(constructor);
            if (form != null)
            {
                form.ActivateHelpPage(form.Name, form.Text);
                form.Show();
                form.Focus();
            }
            return form;
        }

        public static T ActivateForm(Func<FilterModel, T> constructor, FilterModel filter)
        {
            var form = GetInstance(constructor, filter);
            form.ActivateHelpPage(form.Name, form.Text);
            form.Show();
            form.Focus();

            return form;
        }
        
        public static T GetInstance(Func<FilterModel, T> constructor, FilterModel filter)
        {
            if (_instance == null)
            {
                try
                {
                    _instance = constructor(filter);
                    _instance.FormClosed += DisposeInstanse;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            return _instance;
        }

        public static T GetInstance(Func<T> constructor)
        {
            if (_instance == null)
            {
                try
                {
                    _instance = constructor();
                    _instance.FormClosed += DisposeInstanse;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            return _instance;
        }
    }

}
