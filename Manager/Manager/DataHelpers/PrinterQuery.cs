﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities.Catalogs.Printers;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Printers
{
    public struct PrintTask
    {
        /// <summary> Принтер для печати </summary>
        public Printer ToPrinter;
        /// <summary> Документ для печати </summary>
        public PrintDocument PrintDocument;
    }


    public static class PrinterQuery
    {
        /// <summary> Список на печать </summary>
        private static readonly List<PrintTask> PrintTaskList = new List<PrintTask>();

        /// <summary> Идёт печать </summary>
        private static bool _isPrinting;

        /// <summary> Таймер ожидания </summary>
        private static readonly System.Windows.Forms.Timer WaitTimer;

        static PrinterQuery()
        {
            WaitTimer = new System.Windows.Forms.Timer();
            WaitTimer.Tick += async (s, e) => await WaitTimerTick();
            WaitTimer.Interval = 500;
        }

        //__________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Добавить задачу в очередь печати </summary>
        ////=====================================================
        public static void AddPrintTask(IEnumerable<Printer> printers, PrintDocument printDocument)
        {
            foreach (var printer in printers)
            {
                PrintTaskList.Add(new PrintTask { ToPrinter = printer, PrintDocument = printDocument });
            }

            // Если есть что печатать, но печать выключена
            if (PrintTaskList.Count > 0 && _isPrinting == false && WaitTimer.Enabled == false)
            {
                WaitTimer.Start();
            }
        }


        /// <summary>
        /// Таймер печати сработал
        /// </summary>
		static async Task WaitTimerTick()
        {
            // Если не идт печать - печатаем
            if (!_isPrinting)
            {
                await PrintOnDevice();
            }
        }


        //__________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Получить текущую задачу на печать </summary>
        ////======================================================
        private static PrintTask GetCurrentPrintTask()
        {
            var currentPrintTask = new PrintTask();

            if (PrintTaskList.Count > 0)
            {
                currentPrintTask = PrintTaskList[0];
            }

            return currentPrintTask;
        }


        //_____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Печать на устройства </summary>
        ////=========================================
        private static async Task PrintOnDevice()
        {
            PrintTask currentPrintTask = GetCurrentPrintTask();

            await AsyncPrintOnDevice(currentPrintTask.ToPrinter, currentPrintTask.PrintDocument);
        }


        //_____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Печать на устройства </summary>
        /// <param name="printer"> Принтер </param>
        /// <param name="printDoc"> Тело печати </param>
        ////===========================================
        private static Task AsyncPrintOnDevice(Printer printer, PrintDocument printDoc)
        {
            return Task.Run(() =>
            {
                _isPrinting = true;
#if !DEBUG
                var settings = new PrinterSettings { PrinterName = printer.Name };
                printDoc.PrinterSettings = settings;
#else
                var settings = new PrinterSettings { PrintToFile = true, PrintFileName = $"{printer.Name}.xps" };
                printDoc.PrinterSettings = settings;
                printDoc.PrinterSettings.PrintToFile = true;
                printDoc.PrinterSettings.PrintFileName = $"{printer.Name}{printer.ID}.xps";
                printDoc.PrinterSettings.PrinterName = "Microsoft XPS Document Writer";
#endif
                printDoc.Print();

                DeleteLastPrint();

                _isPrinting = false;
            });
        }


        private static void DeleteLastPrint()
        {
            PrintTaskList.Remove(PrintTaskList[0]);

            if (PrintTaskList.Count == 0)
            {
                WaitTimer.Stop();
            }
        }
    }
}
