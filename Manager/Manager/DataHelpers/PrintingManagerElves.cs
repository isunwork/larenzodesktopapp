﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DrvFRLib;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Catalogs.Users;
using LaRenzo.DataRepository.Entities.Documents.CafeOrders;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.TransferLibrary;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Printers
{
    public class PrintingManagerElves
    {
        public static void PrintToNalogPrinter(Order order, int portNumber, string ipAddress, int deliveryPrice)
        {
            var paymentType = PaymentType.Card;
            if (order.PaymentMethod == PaymentMethod.OnlineCard || order.PaymentMethod == PaymentMethod.OfflineCard)
            {
                paymentType = PaymentType.Card;
            }
            else
            {
                paymentType = PaymentType.Money;
            }

            if (portNumber != 0 && !String.IsNullOrEmpty(ipAddress))
            {
                if (order.DoCalculateDelivery && deliveryPrice > 0)
                {
                    var item = new OrderItem
                    {
                        Amount = 1,
                        Dish = new Dish
                        {
                            Name = "Доставка",
                            Price = deliveryPrice,
                            DiscountAppliсability = false
                        },
                        OrderID = order.ID
                    };
                    order.OrderItems.Add(item);
                }
                int? discountPercent = null;
                if (order.DiscountCardID != null)
                {
                    discountPercent = Content.DiscountCardManager.GetByID((int)order.DiscountCardID).Discount;
                }


                PrintToNalogPrinter(order.TotalCost, paymentType, order.TotalCostWithoutDiscount - order.TotalCost,
                    portNumber, ipAddress, null,
                    order.OrderItems, discountPercent);

                order.FiscalCheckPrinted = true;
                Content.OrderManager.ChangePrintToNalogPrinterStatus(order.ID, true);
            }
        }

        public static void PrintZ(int portNumber, string ipAddress)
        {
            try
            {
                var driver = SetConnectionToPrinter(portNumber, ipAddress);
                driver.PrintReportWithCleaning();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public static void PrintX(int portNumber, string ipAddress)
        {
            try
        {
                var driver = SetConnectionToPrinter(portNumber, ipAddress);
                driver.PrintReportWithoutCleaning();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public static void OpenSession(int portNumber, string ipAddress)
        {
            try
            {
                var driver = SetConnectionToPrinter(portNumber, ipAddress);
                driver.OpenSession();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public static void CloseSession(int portNumber, string ipAddress)
        {
            try
            {
                var driver = SetConnectionToPrinter(portNumber, ipAddress);
                driver.FNCloseSession();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public static void CheckConnection(int portNumber, string ipAddress)
        {
            try
            {
                var driver = SetConnectionToPrinter(portNumber, ipAddress);
                driver.Connect();
                driver.GetDeviceMetrics();
                if (driver.Connected)
                {
                    MessageBox.Show("Принтер успешно подключён");
                }
                else
                {
                    MessageBox.Show("Принтер не найден");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        private static DrvFR SetConnectionToPrinter(int portNumber, string ipAddress)
        {
            var driver = new DrvFR();
            driver.ConnectionType = 1;
            driver.ComNumber = 3;
            driver.UseIPAddress = true;
            driver.PortNumber = portNumber;
            driver.IPAddress = ipAddress;
            driver.Password = Content.UserManager.UserLogged.CashboxPassword == 0
                ? (int)CashboxPassword.Administrator
                : (int)Content.UserManager.UserLogged.CashboxPassword;
            driver.CancelCheck();
            //driver.Reser
            return driver;
        }

        private static void PrintToNalogPrinter(decimal price, PaymentType paymentType, decimal discount, int portNumber, string ipAddress, IList<CafeOrderDocumentItem> cafeDocs = null, ICollection<OrderItem> orderDocs = null, int? discountPercent = null)
        {
            try
            {
                var driver = SetConnectionToPrinter(portNumber, ipAddress);
                driver.StringForPrinting = "";
                if (cafeDocs != null)
                {
                    var totalCountWithoutDelivery = cafeDocs.Where(x => x.ID > 0 && x.Dish.DiscountAppliсability).Sum(x => x.Amount);
                    var discountOnEveryDish = discount / totalCountWithoutDelivery;
                    foreach (CafeOrderDocumentItem codi in cafeDocs)
                    {
                        driver.Tax1 = 0;
                        if (codi.ID != 0 && discountOnEveryDish > 0 && codi.Dish.DiscountAppliсability)
                        {
                            driver.Price = codi.Dish.Price - discountOnEveryDish;
                        }
                        else
                        {
                            driver.Price = codi.Dish.Price;
                        }
                        driver.Quantity = codi.Amount;

                        driver.StringForPrinting = codi.Dish.Name;
                        driver.Department = 1;
                        driver.Sale();
                        if (driver.ResultCodeDescription != "Ошибок нет")
                        {
                            MessageBox.Show("ОШИБКА ПЕЧАТИ (продажа): " + driver.ResultCode + "\r\n" +
                                            driver.ResultCodeDescription);
                        }
                    }
                }

                if (orderDocs != null)
                {
                    var totalCountWithoutDelivery = orderDocs.Where(x => x.ID > 0 && x.Dish.DiscountAppliсability).Sum(x => x.Amount);
                    var discountOnEveryDish = discount / totalCountWithoutDelivery;
                    foreach (var codi in orderDocs)
                    {
                        driver.Tax1 = 0;
                        if (codi.ID != 0 && discountOnEveryDish > 0 && codi.Dish.DiscountAppliсability)
                        {
                            driver.Price = codi.Dish.Price - discountOnEveryDish;
                        }
                        else
                        {
                            driver.Price = codi.Dish.Price;
                        }

                        driver.Quantity = codi.Amount;
                        driver.StringForPrinting = codi.Dish.Name;
                        driver.Department = 1;
                        driver.Sale();

                        if (driver.ResultCodeDescription != "Ошибок нет")
                        {
                            MessageBox.Show("ОШИБКА ПЕЧАТИ (продажа): " + driver.ResultCode + "\r\n" +
                                            driver.ResultCodeDescription);
                        }
                    }
                }

                switch (paymentType)
                {
                    case PaymentType.Money:
                        driver.Summ1 = price;
                        break;
                    case PaymentType.Card:
                        driver.Summ2 = price;
                        break;
                    case PaymentType.NoMoney:
                        driver.Summ3 = price;
                        break;
                }

                if (discount > 0 && discountPercent != null)
                {
                    driver.StringForPrinting = $"Скидка по чеку составила ({discountPercent}%): {discount}";
                }

                driver.CloseCheck();                //Закрываем чек 

                if (driver.ResultCodeDescription != "Ошибок нет")
                {
                    MessageBox.Show("ОШИБКА ПЕЧАТИ (закрытие): " + driver.ResultCode + "\r\n" + driver.ResultCodeDescription);
                }
                driver.Disconnect();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public static void PrintToNalogPrinter(decimal price, PaymentType paymentType, IList<CafeOrderDocumentItem> cafeDocs)
        {
            try
            {
                var driver = new DrvFR { ComNumber = 12, StringForPrinting = "" };


                foreach (CafeOrderDocumentItem codi in cafeDocs)
                {
                    driver.Tax1 = 0; driver.Price = codi.Price;

                    if (Content.UserManager.UserLogged.Name.ToLower().Contains("хрипченко"))
                    {
                        driver.Password = 1;
                    }
                    else if (Content.UserManager.UserLogged.Name.ToLower().Contains("гасратова"))
                    {
                        driver.Password = 2;
                    }
                    else
                    {
                        driver.Password = 30;
                    }

                    driver.DiscountOnCheck = 0;
                    driver.Quantity = codi.Amount;

                    driver.StringForPrinting = codi.Dish.Name;
                    driver.Department = 1;

                    driver.Sale();

                    if (driver.ResultCodeDescription != "Ошибок нет")
                    {
                        MessageBox.Show("ОШИБКА ПЕЧАТИ (продажа): " + driver.ResultCode + "\r\n" + driver.ResultCodeDescription);
                    }
                }

                switch (paymentType)
                {
                    case PaymentType.Money:
                        driver.Summ1 = price;
                        break;
                    case PaymentType.Card:
                        driver.Summ2 = price;
                        break;
                    case PaymentType.NoMoney:
                        driver.Summ3 = price;
                        break;
                }

                driver.CloseCheck();                //Закрываем чек 

                if (driver.ResultCodeDescription != "Ошибок нет")
                {
                    MessageBox.Show("ОШИБКА ПЕЧАТИ (закрытие): " + driver.ResultCode + "\r\n" + driver.ResultCodeDescription);
                }

                driver.Disconnect();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

    }
}
