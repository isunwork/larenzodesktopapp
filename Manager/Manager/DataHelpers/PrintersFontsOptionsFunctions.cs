﻿using System.Drawing;

namespace LaRenzo.DataRepository.Repositories.Others.GlobalOptions
{
	public class PrintersFontsOptionsFunctions
    {
        public static readonly PrintersFontsOptionsFunctions Instance = new PrintersFontsOptionsFunctions();

        public int? CurrentBrandIdForSave;

        private PrintersFontsOptionsFunctions()
        {
        }

        /// <summary> Шрифт заголовка </summary>
        public Font PrinterHeaderFont
		{
			get { return GetPrinterFont(PrinterFontUseNames.HEADER); }
			set { SetPrinterFont(value, PrinterFontUseNames.HEADER); }
		}

		/// <summary> Шрифт тела большенства чеков </summary>
		public Font PrinterBodyFont
		{
			get { return GetPrinterFont(PrinterFontUseNames.BODY); }
			set { SetPrinterFont(value, PrinterFontUseNames.BODY); }
		}

		/// <summary> Шрифт блюд для кухни  </summary>
		public Font PrinterItemsFont
		{
			get { return GetPrinterFont(PrinterFontUseNames.ITEMS); }
			set { SetPrinterFont(value, PrinterFontUseNames.ITEMS); }
		}

		/// <summary> Шрифт тела чека клиента </summary>
		public Font PrinterSmallFont
		{
			get { return GetPrinterFont(PrinterFontUseNames.SMALL); }
			set { SetPrinterFont(value, PrinterFontUseNames.SMALL); }
		}

		/// <summary> Шрифт стекера для блюд </summary>
		public Font PrinterStikyFont
		{
			get { return GetPrinterFont(PrinterFontUseNames.STICKY); }
			set { SetPrinterFont(value, PrinterFontUseNames.STICKY); }
		}

		/// <summary> Шрифт чека сессии </summary>
		public Font PrinterSessionFont
		{
			get { return GetPrinterFont(PrinterFontUseNames.SESSION_BODY); }
			set { SetPrinterFont(value, PrinterFontUseNames.SESSION_BODY); }
		}


		//==================

		/// <summary> Шрифт заголовка </summary>
		public Font PrinterHeaderFontByBrandId(int orderBrandId)
		{
			return GetPrinterFont(PrinterFontUseNames.HEADER, orderBrandId); 
		}

		/// <summary> Шрифт тела большенства чеков </summary>
		public Font PrinterBodyFontByBrandId(int orderBrandId)
		{
			return GetPrinterFont(PrinterFontUseNames.BODY, orderBrandId);
		}

		/// <summary> Шрифт блюд для кухни  </summary>
		public Font PrinterItemsFontByBrandId(int orderBrandId)
		{
			return GetPrinterFont(PrinterFontUseNames.ITEMS, orderBrandId); 
		}

		/// <summary> Шрифт тела чека клиента </summary>
		public Font PrinterSmallFontByBrandId(int orderBrandId)
		{
			return GetPrinterFont(PrinterFontUseNames.SMALL, orderBrandId); 
		}

		/// <summary> Шрифт стекера для блюд </summary>
		public Font PrinterStikyFontByBrandId(int orderBrandId)
		{
			return GetPrinterFont(PrinterFontUseNames.STICKY, orderBrandId); 
		}

		/// <summary> Шрифт чека сессии </summary>
		public Font PrinterSessionFontByBrandId(int orderBrandId)
		{
			return GetPrinterFont(PrinterFontUseNames.SESSION_BODY, orderBrandId); 
		}


		//________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Установить настройки шрифтов по умолчанию </summary>
		///===============================================================
		public void SetDefaultFontsParams()
		{
			SetPrinterFont(GetDefaultFont(PrinterFontUseNames.HEADER), PrinterFontUseNames.HEADER);
			SetPrinterFont(GetDefaultFont(PrinterFontUseNames.BODY), PrinterFontUseNames.BODY);
			SetPrinterFont(GetDefaultFont(PrinterFontUseNames.ITEMS), PrinterFontUseNames.ITEMS);
			SetPrinterFont(GetDefaultFont(PrinterFontUseNames.SMALL), PrinterFontUseNames.SMALL);
			SetPrinterFont(GetDefaultFont(PrinterFontUseNames.STICKY), PrinterFontUseNames.STICKY);
			SetPrinterFont(GetDefaultFont(PrinterFontUseNames.SESSION_BODY), PrinterFontUseNames.SESSION_BODY);
		}


		private int CurentBrandId
		{
			get
			{
			    if (CurrentBrandIdForSave != null)
			    {
			        return (int)CurrentBrandIdForSave;
			    }
			    else
			    {
			        return Content.BrandManager.SelectedBrand.ID;
			    }
			}
		}


		
		//____________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Вернуть шрифт по имени (собирает из текстовых параметров) </summary>
		////==============================================================================
		private Font GetPrinterFont(string useName, int brandId = -1)
		{
			Font font;
			string brendUseName;

			if (brandId == -1)
			{
				brendUseName = "Brend_" + CurentBrandId.ToString() + "_" + useName;
			}
			else
			{
				brendUseName = "Brend_" + brandId.ToString() + "_" + useName;
			}

			string fontName = Content.BaseOptionsFunctions.GetValueOrEmptyString(brendUseName + "PrinterFontName");

			if (string.IsNullOrEmpty(fontName))
			{
				font = GetDefaultFont(useName);
			}
			else
			{
				string fontSizeStr = Content.BaseOptionsFunctions.GetValueOrEmptyString(brendUseName + "PrinterFontSize");
				string fontStyleStr = Content.BaseOptionsFunctions.GetValueOrEmptyString(brendUseName + "PrinterFontStyle");
				string graphicsUnitName = Content.BaseOptionsFunctions.GetValueOrEmptyString(brendUseName + "PrinterFontUnit");

				float fontSize;
				float.TryParse(fontSizeStr, out fontSize);

				// Проверка на минимально возможный шрифт
				if (fontSize < 5)
				{
					fontSize = 5;
				}

				// Проверка на максимально возможный шрифт
				if (fontSize > 72)
				{
					fontSize = 72;
				}

				font = new Font(fontName, fontSize, GetFontStyleByName(fontStyleStr), GetGraphicsUnitByName(graphicsUnitName));
			}

			return font;
		}


		//________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить шрифт "по умолчанию" для использование шрифта </summary>
		///============================================================================
		private Font GetDefaultFont(string useName)
		{
			Font font;

			switch (useName)
			{
				case PrinterFontUseNames.HEADER:
					font = new Font("Courier New", 24, FontStyle.Bold, GraphicsUnit.Point);
					break;
				case PrinterFontUseNames.BODY:
					font = new Font("Courier New", 12, FontStyle.Bold, GraphicsUnit.Point);
					break;
				case PrinterFontUseNames.ITEMS:
					font = new Font("Courier New", 18, FontStyle.Bold, GraphicsUnit.Point);
					break;
				case PrinterFontUseNames.SMALL:
					font = new Font("Courier New", 9.5f, FontStyle.Bold, GraphicsUnit.Point);
					break;
				case PrinterFontUseNames.STICKY:
					font = new Font("Courier New", 5.5f, FontStyle.Bold, GraphicsUnit.Point);
					break;
				case PrinterFontUseNames.SESSION_BODY:
					font = new Font("Courier New", 11, FontStyle.Bold, GraphicsUnit.Point);
					break;
				default:
					font = new Font("Courier New", 10, FontStyle.Regular, GraphicsUnit.Pixel);
					break;
			}

			return font;
		}


		//________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Сохранить шрифт через текстовые параметры </summary>
		///===============================================================
		private void SetPrinterFont(Font font, string useName)
		{
			string brendUseName = "Brend_" + CurentBrandId.ToString() + "_" + useName;

            Content.BaseOptionsFunctions.SetOrCreateValue(brendUseName + "PrinterFontName", font.FontFamily.Name);
            Content.BaseOptionsFunctions.SetOrCreateValue(brendUseName + "PrinterFontSize", font.Size.ToString());
            Content.BaseOptionsFunctions.SetOrCreateValue(brendUseName + "PrinterFontStyle", font.Style.ToString());
            Content.BaseOptionsFunctions.SetOrCreateValue(brendUseName + "PrinterFontUnit", font.Unit.ToString());
		}


		//________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить GraphicsUnit по имени </summary>
		///====================================================
		private GraphicsUnit GetGraphicsUnitByName(string name)
		{
			GraphicsUnit gf;

			switch (name)
			{
				case "Display":
					gf = GraphicsUnit.Display;
					break;
				case "Document":
					gf = GraphicsUnit.Document;
					break;
				case "Inch":
					gf = GraphicsUnit.Inch;
					break;
				case "Millimeter":
					gf = GraphicsUnit.Millimeter;
					break;
				case "Pixel":
					gf = GraphicsUnit.Pixel;
					break;
				case "World":
					gf = GraphicsUnit.World;
					break;
				default:
					gf = GraphicsUnit.Point;
					break;
			}

			return gf;
		}


		//________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить FontStyle по имени </summary>
		///====================================================
		private FontStyle GetFontStyleByName(string name)
		{
			FontStyle fs;

			switch (name)
			{
				case "Bold":
					fs = FontStyle.Bold;
					break;
				case "Italic":
					fs = FontStyle.Italic;
					break;
				case "Strikeout":
					fs = FontStyle.Strikeout;
					break;
				case "Underline":
					fs = FontStyle.Underline;
					break;
				default:
					fs = FontStyle.Regular;
					break;
			}

			return fs;
		}
	}

	
}
