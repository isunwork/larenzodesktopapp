﻿using LaRenzo.DataRepository.Repositories.Settings;
using LaRenzo.DataRepository.Repositories.Catalogs;
using LaRenzo.DataRepository.Repositories.Catalogs.Categories;
using LaRenzo.DataRepository.Repositories.Catalogs.Client;
using LaRenzo.DataRepository.Repositories.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Repositories.Catalogs.Dishes;
using LaRenzo.DataRepository.Repositories.Catalogs.Sections;
using LaRenzo.DataRepository.Repositories.Catalogs.Tables;
using LaRenzo.DataRepository.Repositories.Documents.CaffeOrders;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Documents.Orders;
using LaRenzo.DataRepository.Repositories.Others.GlobalOptions;
using LaRenzo.DataRepository.Repositories.Others.Times;
using LaRenzo.DataRepository.Repositories.Catalogs.Printers;
using LaRenzo.DataRepository.Repositories.Documents.WebOrders;
using LaRenzo.DataRepository.Repositories.Catalogs.BlackList;
using LaRenzo.DataRepository.Repositories.Others.Statistics;
using LaRenzo.DataRepository.Repositories.Documents.Sessions;
using LaRenzo.DataRepository.Repositories.Documents.Writeoffs;
using LaRenzo.DataRepository.Repositories.Wiringses.RemainsOfGood;
using LaRenzo.DataRepository.Repositories.Catalogs.Products;
using LaRenzo.DataRepository.Repositories.Catalogs.Warehouses;
using LaRenzo.DataRepository.Repositories.Documents.WriteoffDish;
using LaRenzo.DataRepository.Repositories.Reports.TransferOfGoodReport;
using LaRenzo.DataRepository.Repositories.Documents.Posting;
using LaRenzo.DataRepository.Repositories.Catalogs.Partners;
using LaRenzo.DataRepository.Repositories.Documents.Supply;
using LaRenzo.DataRepository.Repositories.Wiringses.SettlementsWithPartner;
using LaRenzo.DataRepository.Repositories.Wiringses.AveragesPrice;
using LaRenzo.DataRepository.Repositories.Documents.Returns;
using LaRenzo.DataRepository.Repositories.Documents.Moving;
using LaRenzo.DataRepository.Repositories.Documents.Inventory;
using LaRenzo.DataRepository.Repositories.Catalogs.Sets;
using LaRenzo.DataRepository.Repositories.PrintWorksModels;
using LaRenzo.DataRepository.Repositories.Catalogs.Booklets;
using LaRenzo.DataRepository.Repositories.Catalogs.Catleries;
using LaRenzo.DataRepository.Repositories.Others.AmountFactories;
using LaRenzo.DataRepository.Repositories.Catalogs.Cutlery;
using LaRenzo.DataRepository.Repositories.Catalogs.Measures;
using LaRenzo.DataRepository.Repositories.Catalogs.Addresses;
using LaRenzo.DataRepository.Repositories.Others.DeliverySchedulies;
using LaRenzo.DataRepository.Repositories.Catalogs.Drivers;
using LaRenzo.DataRepository.Repositories.Transfers;
using LaRenzo.DataRepository.Repositories.Wiringses.IntermadiatesGood;
using LaRenzo.DataRepository.Repositories.Reports.PartnersProduct;
using LaRenzo.DataRepository.Repositories.Call;
using LaRenzo.DataRepository.Repositories.Reports.DishCombinationReport;
using LaRenzo.DataRepository.Repositories.Reports.RemainsOfGoodReport;
using LaRenzo.DataRepository.Repositories.Finance;
using LaRenzo.DataRepository.Repositories.Reports.SettlsWithPartnerReport;
using LaRenzo.DataRepository.Repositories.Others.HelpPages;
using LaRenzo.DataRepository.Entities.Finance;
using Ninject;
using System.Reflection;
using LaRenzo.Interfaces;

namespace LaRenzo.DataRepository.Repositories
{
    public static class Content
    {
        public static IBaseOptionsFunctions BaseOptionsFunctions;
        public static ICategoryRepository CategoryRepository;
        public static IDishRepository DishRepository;

        public static IAdditionalOfferDishRepository AdditionalOfferDishRepository;
        public static ISectionRepository SectionRepository;
        public static IPointOfSaleRepository PointOfSaleRepository;
        public static IBranchRepository BranchRepository;
        public static IClientRepository ClientRepository;
        public static IClientAddressRepository ClientAddressRepository;
        public static IClientPhoneRepository ClientPhoneRepository;
        public static IProgrammVersionRepository ProgrammVersionRepository;
        public static ICouponRepository CouponRepository;
        public static ICouponRuleRepository CouponRuleRepository;
        public static ICouponDiscountRepository CouponDiscountRepository;
        public static ICouponRuleElementRepository CouponRuleElementRepository;

        public static ICafeOrderDocumentRepository CafeOrderDocumentRepository;
        public static ITableRepository TableRepository;
        public static IPrintingRepository PrintingRepository;
        public static IPrinterRepository PrinterRepository;
        public static ICutleryRepository CutleryRepository;
        public static ICallRepository CallRepository;
        public static IPaymentCategoryRepository PaymentCategoryRepository;
        public static IWalletRepository WalletRepository;
        public static IPaymentRepository PaymentRepository;
        public static IPaymentDashboardRepository PaymentDashboardRepository;
        public static IDishCombinationRepository DishCombinationRepository;

        public static IBrandManager BrandManager;
        public static IUserManager UserManager;
        public static IOrderManager OrderManager;
        public static IStateManager StateManager;
        public static IUserAccessManager UserAccessManager;
        public static IGlobalOptionsManager GlobalOptionsManager;
        public static ITimeSheetManager TimeSheetManager;
        public static IPrintWorksRepository PrintWorksRepository;
        public static IWebOrderManager WebOrderManager;
        public static IBlackListManager BlackListManager;
        public static IStatisticManager StatisticManager;
        public static ISessionManager SessionManager;
        public static IWriteoffManager WriteoffManager;
        public static IRemainsOfGoodWiringsManager RemainsOfGoodWiringsManager;
        public static IProductManager ProductManager;
        public static IWarehouseManager WarehouseManager;
        public static IWriteoffDishManager WriteoffDishManager;
        public static IWarehouseForDishManager WarehouseForDishManager;
        public static ITransferOfGoodReportManager TransferOfGoodReportManager;
        public static IPostingManager PostingManager;
        public static IPartnerManager PartnerManager;
        public static ISupplyManager SupplyManager;
        public static ISettlementsWithPartnerManager SettlementsWithPartnerManager;
        public static IAveragePriceWiringsManager AveragePriceWiringsManager;
        public static IReturnManager ReturnManager;
        public static IMovingManager MovingManager;
        public static IInventoryManager InventoryManager;
        public static ISetCalculationManager SetCalculationManager;
        public static IBookletManager BookletManager;
        public static ICutleryManager CutleryManager;
        public static IDiscountCardManager DiscountCardManager;
        public static IAmountFactorManager AmountFactorManager;
        public static ISetManager SetManager;
        public static IMeasureManager MeasureManager;
        public static IAddressManager AddressManager;
        public static IPreOrderManager PreOrderManager;
        public static IDeliveryScheduleManager DeliveryScheduleManager;
        public static IDriverManager DriverManager;
        public static IChiefExpertTransferManager ChiefExpertTransferManager;
        public static IItermadiateGoodWirningManager ItermadiateGoodWirningManager;
        public static ISetParamsManager SetParamsManager;
        public static IPartnersProductManager PartnersProductManager;
        public static IRemainsOfGoodReportManager RemainsOfGoodReportManager;
        public static ISettlsWithPartnerReportManager SettlsWithPartnerReportManager;
        public static IBankManager BankManager;
        public static IClearDataManager ClearDataManager;
        public static IHelpPageManager HelpPageManager;

        public static ISignalProcessor SignalProcessor;

        public static IThreadLocalStorage ThreadLocalStorage;


        public static void RersolveRepositories()
        {
            IKernel kernel = new StandardKernel(new NinjectSettings
            {
                InjectAttribute = typeof(DataLib.Models.InjectAttribute)
            });
            kernel.Load(Assembly.GetExecutingAssembly());

            UserMessage.UserMessageStrategy = kernel.Get<IUserMessageStrategy>();
            SignalProcessor = kernel.Get<ISignalProcessor>();

            ThreadLocalStorage = kernel.Get<IThreadLocalStorage>();

            BaseOptionsFunctions = kernel.Get<IBaseOptionsFunctions>();
            CategoryRepository = kernel.Get<ICategoryRepository>();
            DishRepository = kernel.Get<IDishRepository>();
            AdditionalOfferDishRepository = kernel.Get<IAdditionalOfferDishRepository>();
            SectionRepository = kernel.Get<ISectionRepository>();
            PointOfSaleRepository = kernel.Get<IPointOfSaleRepository>();
            BranchRepository = kernel.Get<IBranchRepository>();
            ClientRepository = kernel.Get<IClientRepository>();
            ClientAddressRepository = kernel.Get<IClientAddressRepository>();
            ClientPhoneRepository = kernel.Get<IClientPhoneRepository>();
            ProgrammVersionRepository = kernel.Get<IProgrammVersionRepository>();
            CouponRepository = kernel.Get<ICouponRepository>();
            CouponRuleRepository = kernel.Get<ICouponRuleRepository>();
            CouponDiscountRepository = kernel.Get<ICouponDiscountRepository>();
            CouponRuleElementRepository = kernel.Get<ICouponRuleElementRepository>();
            CafeOrderDocumentRepository = kernel.Get<ICafeOrderDocumentRepository>();
            TableRepository = kernel.Get<ITableRepository>();
            PrintingRepository = kernel.Get<IPrintingRepository>();
            PrinterRepository = kernel.Get<IPrinterRepository>();
            CutleryRepository = kernel.Get<ICutleryRepository>();
            CallRepository = kernel.Get<ICallRepository>();
            PaymentCategoryRepository = kernel.Get<IPaymentCategoryRepository>();
            WalletRepository = kernel.Get<IWalletRepository>();
            PaymentRepository = kernel.Get<IPaymentRepository>();
            PaymentDashboardRepository = kernel.Get<IPaymentDashboardRepository>();
            DishCombinationRepository = kernel.Get<IDishCombinationRepository>();
            BrandManager = kernel.Get<IBrandManager>();
            UserManager = kernel.Get<IUserManager>();
            OrderManager = kernel.Get<IOrderManager>();
            StateManager = kernel.Get<IStateManager>();
            UserAccessManager = kernel.Get<IUserAccessManager>();
            GlobalOptionsManager = kernel.Get<IGlobalOptionsManager>();
            TimeSheetManager = kernel.Get<ITimeSheetManager>();
            PrintWorksRepository = kernel.Get<IPrintWorksRepository>();
            WebOrderManager = kernel.Get<IWebOrderManager>();
            BlackListManager = kernel.Get<IBlackListManager>();
            StatisticManager = kernel.Get<IStatisticManager>();
            SessionManager = kernel.Get<ISessionManager>();
            WriteoffManager = kernel.Get<IWriteoffManager>();
            RemainsOfGoodWiringsManager = kernel.Get<IRemainsOfGoodWiringsManager>();
            ProductManager = kernel.Get<IProductManager>();
            WarehouseManager = kernel.Get<IWarehouseManager>();
            WriteoffDishManager = kernel.Get<IWriteoffDishManager>();
            WarehouseForDishManager = kernel.Get<IWarehouseForDishManager>();
            TransferOfGoodReportManager = kernel.Get<ITransferOfGoodReportManager>();
            PostingManager = kernel.Get<IPostingManager>();
            PartnerManager = kernel.Get<IPartnerManager>();
            SupplyManager = kernel.Get<ISupplyManager>();
            SettlementsWithPartnerManager = kernel.Get<ISettlementsWithPartnerManager>();
            AveragePriceWiringsManager = kernel.Get<IAveragePriceWiringsManager>();
            ReturnManager = kernel.Get<IReturnManager>();
            MovingManager = kernel.Get<IMovingManager>();
            InventoryManager = kernel.Get<IInventoryManager>();
            SetCalculationManager = kernel.Get<ISetCalculationManager>();
            BookletManager = kernel.Get<IBookletManager>();
            CutleryManager = kernel.Get<ICutleryManager>();
            DiscountCardManager = kernel.Get<IDiscountCardManager>();
            AmountFactorManager = kernel.Get<IAmountFactorManager>();
            SetManager = kernel.Get<ISetManager>();
            MeasureManager = kernel.Get<IMeasureManager>();
            AddressManager = kernel.Get<IAddressManager>();
            PreOrderManager = kernel.Get<IPreOrderManager>();
            DeliveryScheduleManager = kernel.Get<IDeliveryScheduleManager>();
            DriverManager = kernel.Get<IDriverManager>();
            ChiefExpertTransferManager = kernel.Get<IChiefExpertTransferManager>();
            ItermadiateGoodWirningManager = kernel.Get<IItermadiateGoodWirningManager>();
            SetParamsManager = kernel.Get<ISetParamsManager>();
            PartnersProductManager = kernel.Get<IPartnersProductManager>();
            RemainsOfGoodReportManager = kernel.Get<IRemainsOfGoodReportManager>();
            SettlsWithPartnerReportManager = kernel.Get<ISettlsWithPartnerReportManager>();
            BankManager = kernel.Get<IBankManager>();
            ClearDataManager = kernel.Get<IClearDataManager>();
            HelpPageManager = kernel.Get<IHelpPageManager>();
        }
    }
}
