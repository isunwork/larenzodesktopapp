﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.Interfaces.SignalMessages
{
    public class BrokenInventorySignalMessage
    {
        public int DocumentId { get; set; }
        public DateTime DocumentDate { get; set; }
        public string Reason { get; set; }
    }
}
