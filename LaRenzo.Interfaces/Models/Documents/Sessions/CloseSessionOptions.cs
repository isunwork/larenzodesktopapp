﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Documents.Sessions
{
    public class CloseSessionOptions
    {
        public bool DoOpenNewSession { get; set; }
        public bool DoTransferOpenOrders { get; set; }
        public bool DoSendBankInteractions { get; set; }
    }
}
