﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Documents.WebOrders
{
    public class GrabOrdersResult
    {
        public int WebOrderCount { get; set; }
        public int ExistCount { get; set; }
        public int NewCount { get; set; }
    }
}
