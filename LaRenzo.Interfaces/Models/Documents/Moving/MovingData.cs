﻿using LaRenzo.DataRepository.Entities.Documents.Moving;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Documents.Moving
{
    public class MovingData
    {
        public MovingDocument Document { get; set; }
        public List<MovingDocumentItem> DocumentItems { get; set; }
    }
}
