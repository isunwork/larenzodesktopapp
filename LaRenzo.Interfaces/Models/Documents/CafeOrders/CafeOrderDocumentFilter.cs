﻿using LaRenzo.DataRepository.Entities.Catalogs.Tables;

namespace LaRenzo.DataRepository.Entities.Documents.CafeOrders
{
	// ReSharper disable InconsistentNaming
	public class CafeOrderDocumentFilter : FilterModel
	{
		public bool IsIDFilter { set; get; }

		public int DocumentID { set; get; }

		public string CategoryName { set; get; }

		public Table CurrentTable { set; get; }

		public bool IsTableFilter { set; get; }

		public bool? IsOpen { set; get; }

		public bool IsSessionFilter { set; get; }

		public int SessionId { set; get; }

		public int Position { set; get; }

		public bool IsPositionFilter { set; get; }
	}
}
