﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Documents.Orders
{
    public struct PreOrderProduct
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public decimal NeededAmount { get; set; }
        public decimal StockAmount { get; set; }
    }
}
