﻿using LaRenzo.DataRepository.Entities.Documents.Posting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Documents.Posting
{
    public class PostingData
    {
        public PostingDocument Document { get; set; }
        public List<PostingDocumentItem> DocumentItems { get; set; }
    }
}
