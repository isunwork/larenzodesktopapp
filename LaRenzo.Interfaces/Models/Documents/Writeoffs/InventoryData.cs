﻿using LaRenzo.DataRepository.Entities.Documents.Writeoff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Documents.Writeoffs
{
    public class WriteoffData
    {
        public WriteoffDocument Document { get; set; }
        public List<WriteoffDocumentItem> DocumentItems { get; set; }
    }
}
