﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace LaRenzo.DataRepository.Entities.Transfers.ChefExpertTransfer
{
    [XmlRoot(ElementName = "folder")]
    public class Folder
    {
        // ReSharper disable InconsistentNaming
        [XmlElement(ElementName = "prod")]
        public List<Product> Products { set; get; }

        [XmlElement(ElementName = "folder")]
        public List<Folder> Childs { set; get; }

        [XmlAttribute(AttributeName = "id")]
        public int ID { set; get; }

        [XmlAttribute(AttributeName = "name")]
        public string Name { set; get; }

        [XmlAttribute(AttributeName = "view")]
        public string ProductView { set; get; }

        [XmlAttribute(AttributeName = "color")]
        public string Color { set; get; }

        [XmlAttribute(AttributeName = "cons")]
        public string Consist { set; get; }

        [XmlAttribute(AttributeName = "taste")]
        public string Taste { set; get; }

        [XmlAttribute(AttributeName = "fdry")]
        public double DRY { set; get; }

        [XmlAttribute(AttributeName = "ffat")]
        public double Fat { set; get; }

        [XmlAttribute(AttributeName = "fsug")]
        public double Suger { set; get; }

        [XmlAttribute(AttributeName = "fsalt")]
        public double Salt { set; get; }

        [XmlAttribute(AttributeName = "gerb")]
        public double Gerber { set; get; }

        [XmlAttribute(AttributeName = "kma")]
        public string KMA { set; get; }

        [XmlAttribute(AttributeName = "bgkp")]
        public string BGKP { set; get; }

        [XmlAttribute(AttributeName = "ecoli")]
        public string Ecoli { set; get; }

        [XmlAttribute(AttributeName = "saur")]
        public string Saur { set; get; }

        [XmlAttribute(AttributeName = "prot")]
        public string Prot { set; get; }

        [XmlAttribute(AttributeName = "pato")]
        public string Pato { set; get; }

        [XmlAttribute(AttributeName = "dryk")]
        public string Dryk { set; get; }

        [XmlAttribute(AttributeName = "smax")]
        public string Smax { set; get; }

        [XmlAttribute(AttributeName = "sp")]
        public string Sp { set; get; }

        [XmlAttribute(AttributeName = "rem")]
        public string Rem { set; get; }

        [XmlAttribute(AttributeName = "fid")]
        public string FID { set; get; }
        // ReSharper restore InconsistentNaming
    }
}
