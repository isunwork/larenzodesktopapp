﻿using LaRenzo.DataRepository.Entities.Catalogs.Dishes;

namespace LaRenzo.DataRepository.Entities.Transfers.ChefExpertTransfer
{
    public class DishConformity
    {
        public Dish Dish { set; get; }

        public Product DishChef { set; get; }
    }
}  