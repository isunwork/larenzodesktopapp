﻿using System.Xml.Serialization;

namespace LaRenzo.DataRepository.Entities.Transfers.ChefExpertTransfer
{
    [XmlRoot(ElementName = "rd")]
    public class RecipeDetail
    {
        [XmlAttribute(AttributeName = "id")]
        public int ID { set; get; }

        [XmlAttribute(AttributeName = "order")]
        public string Order { set; get; }

        [XmlAttribute(AttributeName = "prod")]
        public string Prod { set; get; }

        [XmlAttribute(AttributeName = "proc")]
        public string Proc { set; get; }

        [XmlAttribute(AttributeName = "brutto")]
        public string Brutto { set; get; }

        [XmlAttribute(AttributeName = "netto")]
        public string Netto { set; get; }

        [XmlAttribute(AttributeName = "total")]
        public string Total { set; get; }

        [XmlAttribute(AttributeName = "fn")]
        public string FN { set; get; }

        [XmlAttribute(AttributeName = "ft")]
        public string FT { set; get; }

        [XmlAttribute(AttributeName = "fs")]
        public string FS { set; get; }
    }
}