﻿using System.Xml.Serialization;

namespace LaRenzo.DataRepository.Entities.Transfers.ChefExpertTransfer
{
    [XmlRoot(ElementName = "proc")]
    public class Proc
    {
        [XmlAttribute(AttributeName = "id")]
        public int ID { set; get; }

        [XmlAttribute(AttributeName = "name")]
        public string Name { set; get; }

        [XmlAttribute(AttributeName = "cold")]
        public string Cold { set; get; }

        [XmlAttribute(AttributeName = "hot")]
        public string Hot { set; get; }

        [XmlAttribute(AttributeName = "fin")]
        public string Fin { set; get; }

        [XmlAttribute(AttributeName = "wh")]
        public string Wh { set; get; }

        [XmlAttribute(AttributeName = "gr")]
        public string Gr { set; get; }

        [XmlAttribute(AttributeName = "ch")]
        public string Ch { set; get; }

        [XmlAttribute(AttributeName = "fd")]
        public string Fd { set; get; }
    }
}
