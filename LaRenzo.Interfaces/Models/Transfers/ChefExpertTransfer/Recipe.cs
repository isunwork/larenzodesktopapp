﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace LaRenzo.DataRepository.Entities.Transfers.ChefExpertTransfer
{
    [XmlRoot(ElementName = "recipe")]
    public class Recipe
    {
        [XmlElement(ElementName = "rd")]
        public List<RecipeDetail> RecipeDetails { set; get; }

        [XmlAttribute(AttributeName = "id")]
        public int ID { set; get; }

        [XmlAttribute(AttributeName = "number")]
        public string Number { set; get; }

        [XmlAttribute(AttributeName = "org")]
        public string Organization { set; get; }

        [XmlAttribute(AttributeName = "date")]
        public string Date { set; get; }

        [XmlAttribute(AttributeName = "price")]
        public string Price { set; get; }

        [XmlAttribute(AttributeName = "total")]
        public string Total { set; get; }

        [XmlAttribute(AttributeName = "stotal")]
        public string STotal { set; get; }

        [XmlAttribute(AttributeName = "tech")]
        public string Tech { set; get; }

        [XmlAttribute(AttributeName = "view")]
        public string ProductView { set; get; }

        [XmlAttribute(AttributeName = "color")]
        public string Color { set; get; }

        [XmlAttribute(AttributeName = "cons")]
        public string Consist { set; get; }

        [XmlAttribute(AttributeName = "taste")]
        public string Taste { set; get; }

        [XmlAttribute(AttributeName = "fd")]
        public string FD { set; get; }

        [XmlAttribute(AttributeName = "fid")]
        public string FID { set; get; }
    }
}
