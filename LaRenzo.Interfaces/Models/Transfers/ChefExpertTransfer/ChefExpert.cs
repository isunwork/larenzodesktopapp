﻿using System.Collections.Generic;
using System.Xml.Serialization;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Entities.Catalogs.Products;

namespace LaRenzo.DataRepository.Entities.Transfers.ChefExpertTransfer
{
    [XmlRoot(ElementName = "chefexpert")]
    public class ChefExpert
    {
        [XmlElement(ElementName = "ingredients")]
        public List<Folder> Ingredients { set; get; }

        [XmlElement(ElementName = "products")]
        public List<Folder> Products { set; get; }

        [XmlElement(ElementName = "semiproducts")]
        public List<Folder> SemiProducts { set; get; }

        public List<Product> GetIngridients()
        {
            var ingridients = new List<Product>();
            GetProductList(Ingredients, ingridients);
            return ingridients;
        }

        public List<Product> GetDishes()
        {
            var dishes = new List<Product>();
            GetDishList(Products, dishes);
            return dishes;
        }

        private void GetDishList(List<Folder> products, List<Product> dishes)
        {
            if (products == null || products.Count == 0)
                return;

            foreach (var folder in products)
            {
                if (folder.Products != null && folder.Products.Count > 0)
                {
                    foreach (var product in folder.Products)
                        product.DishCategory = new Category { Name = folder.Name ?? "Загружены из Шеф Эксперт", ID = -1 };

                    dishes.AddRange(folder.Products);
                }

                GetDishList(folder.Childs, dishes);
            }
        }

        public List<Product> GetSemiProducts()
        {
            var ingridients = new List<Product>();
            GetProductList(SemiProducts, ingridients);
            return ingridients;
        }

        private void GetProductList(List<Folder> hierarchy, List<Product> list)
        {
            if (hierarchy == null || hierarchy.Count == 0)
                return;

            foreach (var folder in hierarchy)
            {
                if (folder.Products != null && folder.Products.Count > 0)
                {
                    foreach (var product in folder.Products)
                        product.ProductCategory = new ProductCategory {Name = folder.Name ?? "Загружены из Шеф Эксперт", ID = -1};

                    list.AddRange(folder.Products);
                }

                GetProductList(folder.Childs, list);
            }
        }
    }
}
