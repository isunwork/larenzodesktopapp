﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Wiringses.RemainsOfGood
{
    public class ProductRemain
    {
        public int ProductID { get; set; }
        public decimal Amount { get; set; }
    }
}
