﻿using LaRenzo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository
{
    public class SignalProcessor : ISignalProcessor
    {
        private Dictionary<Type, object> _subscribers = new Dictionary<Type, object>();

        public void Bind<TSignalMessage>(Action<TSignalMessage> action)
        {
            _subscribers[typeof(TSignalMessage)] = action;
        }

        public void UnBind<TSignalMessage>()
        {
            var type = typeof(TSignalMessage);
            if (!_subscribers.ContainsKey(type))
                return;
            _subscribers.Remove(type);
        }

        public void Raise<TSignalMessage>(TSignalMessage signalMessage) where TSignalMessage : class, new()
        {
            var type = typeof(TSignalMessage);
            if (!_subscribers.ContainsKey(type))
                return;
            var action = _subscribers[type] as Action<TSignalMessage>;            
            action?.Invoke(signalMessage);
        }
    }
}
