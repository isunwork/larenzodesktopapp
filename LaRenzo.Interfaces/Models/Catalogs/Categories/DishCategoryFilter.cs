﻿namespace LaRenzo.DataRepository.Entities.Catalogs.Categories
{
    public class DishCategoryFilter : FilterModel
    {
        public int DishCategoryId { set; get; }
    }
}
