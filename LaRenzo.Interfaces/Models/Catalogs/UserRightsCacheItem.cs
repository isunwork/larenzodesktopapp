﻿using System.Collections.Generic;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Users
{
    public class UserRightsCacheItem
    {
        public int UserId { get; set; }
        public bool DoIgnoreAdminFlag { get; set; }
        public List<AccessRightInfo> UserRights { get; set; }
    }
}
