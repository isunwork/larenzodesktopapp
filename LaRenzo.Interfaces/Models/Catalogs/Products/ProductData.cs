﻿using LaRenzo.DataRepository.Entities.Catalogs.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Products
{
    public class ProductData
    {
        public Product Product { get; set; }
        public List<ItemOfProductSet> ProductSets { get; set; }
        public List<PartnerOfProduct> Partners { get; set; }
    }
}
