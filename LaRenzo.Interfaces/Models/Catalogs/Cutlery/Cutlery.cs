﻿using System.Collections.Generic;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Cutlery
{
    public class Cutlery
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
