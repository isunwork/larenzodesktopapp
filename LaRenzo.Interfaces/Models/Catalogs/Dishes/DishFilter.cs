﻿namespace LaRenzo.DataRepository.Entities.Catalogs.Dishes
{
    public class DishFilter : FilterModel
    {
        public bool FilterBySKU { set; get; }

        public int SKU { set; get; }
        public int[] SKUArray { set; get; }

		public int BrandId { set; get; }

        public int DishId { set; get; }

        public string CategoryName { set; get; }

        public bool FilterByBrandId { set; get; }
        public bool FilterByDishId { set; get; }

        public bool FilterByCategoryName { set; get; }

        public bool FilterByIsDeleteMark { set; get; }
    }
}
