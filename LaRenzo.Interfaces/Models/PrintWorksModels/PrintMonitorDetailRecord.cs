﻿namespace LaRenzo.DataRepository.Entities.PrintWorksModels
{
    public class PrintMonitorDetailRecord
    {
        public int ID { set; get; }

        public int? PrintMonitorID { set; get; }

        public string Name { set; get; }

        public string State { set; get; }
    }
}
