﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.PrintWorksModels
{
    public class CardsData
    {
        public int onlineCafeCount { get; set; }
        public int offlineCafeCount { get; set; }
        public int cardsCafeCount { get; set; }
        public decimal onlineCafeSum { get; set; }
        public decimal offlineCafeSum { get; set; }
        public decimal cardsCafeSum { get; set; }
    }
}
