﻿using System;
using LaRenzo.DataRepository.Entities.Catalogs.Client;

namespace LaRenzo.DataRepository.Repositories.Reports
{
    public class ClientsReport
    {
        public string ClientName { get; set; }
        public string ClientPhone { get; set; }
        public string ClientAddress { get; set; }
        public GenderEnum Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public string Email { get; set; }
    }
}
