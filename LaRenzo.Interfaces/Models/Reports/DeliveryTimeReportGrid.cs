﻿using System.Collections.Generic;

namespace LaRenzo.DataRepository.Repositories.Reports
{
    public class DeliveryTimeReportGrid
    {
        public string Steet { get; set; }
        public double CookingTime { get; set; }
        public double DeliveryTime { get; set; }
        
        public int CookingTimeCount { get; set; }
        public int DeliveryTimeCount { get; set; }
    }
}
