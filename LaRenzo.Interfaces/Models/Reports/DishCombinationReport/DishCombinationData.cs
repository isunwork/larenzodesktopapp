﻿namespace LaRenzo.DataRepository.Repositories.Reports.DishCombinationReport
{
    public class DishCombinationData
    {
        public string MainDishName { get; set; }

        public int MainDishCount { get; set; }

        public string Dish1Name { get; set; }

        public int Dish1Count { get; set; }

        public string Dish2Name { get; set; }

        public int Dish2Count { get; set; }

        public string Dish3Name { get; set; }

        public int Dish3Count { get; set; }

        public string Dish4Name { get; set; }

        public int Dish4Count { get; set; }

        public string Dish5Name { get; set; }

        public int Dish5Count { get; set; }
    }
}
