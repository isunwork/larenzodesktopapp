﻿namespace LaRenzo.DataRepository.Repositories.Reports.DishCombinationReport
{
    public class DishCombinationTempData
    {
        public int DishId { get; set; }

        public int DishCount { get; set; }
    }
}
