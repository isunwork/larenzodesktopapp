﻿namespace LaRenzo.DataRepository.Repositories.Reports.RemainsOfGoodReport
{
    public class RemainsOfGoodReportRecord
    {
        public int ProductId { set; get; }

        public string ProductName { set; get; }

        public string ProductCategory { set; get; }

        public decimal ProductCount { set; get; }

        public decimal ProductMinCount { set; get; }

        public decimal ProductDelta { set; get; }

        public string ProductMeasure { set; get; }

        public decimal Price { set; get; }

        public decimal Sum { set; get; }

        public double AveragePrice { get; set; }

        public double LastPrice { get; set; }

        public double Median { get; set; }

        public double MaxPriceGapPercantage { get; set; }

        public double WeightedPrice { get; set; }
    }
}
