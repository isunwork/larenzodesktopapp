﻿using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Partners;

namespace LaRenzo.DataRepository.Entities.Reports.SettlsWithPartnerReport
{
    public class SettlsWithPartnerRepRecord
    {
        [ForeignKey("Partner")]
        public int PartnerID { set; get; }

        public Partner Partner { set; get; }

        public string SupplyDocument { set; get; }

        public double OverPayment { set; get; }

        public double DeficitPayment { set; get; }
    }
}
