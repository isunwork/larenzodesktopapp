﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Others.Times
{
    public class OutOfTimeSheetsException : Exception
    {
        public OutOfTimeSheetsException() : base("Невозможно получить необходимое количество ячеек времени")
        {

        }
    }
}
