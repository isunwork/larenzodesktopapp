﻿using System;

namespace LaRenzo.DataRepository.Repositories.Catalogs.DiscountCards
{
    public class DiscountCardMergeException : Exception
    {
        public DiscountCardMergeException() : base("Дубли не найдены")
        {

        }
    }
}
