﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Users
{
    /// <summary>
    /// Должен вернуть список, который поддерживает ивенты
    /// </summary>
    public class RightListWithChangesDetection
    {
        
        public BindingList<AccessRightInfo> Data = new BindingList<AccessRightInfo>();

        public RightListWithChangesDetection(List<AccessRightInfo> data)
        {
            Data = new BindingList<AccessRightInfo>(data);
           
            
            foreach (var accessRightInfo in Data)
            {
                var right = accessRightInfo;
                
                right.PropertyChanged += (sender, args) =>
                {
                    
                    var arguments = (AccessRightInfo.RightPropertyChangedEventArgs) args;
                    if (arguments.PropertyName == "Allowed") CheckAnotherPermissions(right, arguments.OnlyUpwardUpdate);
                };
            }
        }

        public AccessRightInfo Get(int code)
        {
            return Data.FirstOrDefault(x => x.Code == code);
        }

        public AccessRightInfo GetParent(AccessRightInfo right)
        {
            return right.ParentCode == 0 ? null : Data.FirstOrDefault(x => x.Code == right.ParentCode);
        }

        public IEnumerable<AccessRightInfo> GetChilds(AccessRightInfo right)
        {
            return Data.Where(x => x.ParentCode == right.Code);
        }

        public bool AnyAllowedAtLevel(AccessRightInfo right)
        {
            return right.ParentCode != 0 && Data.Any(x => x.Allowed && x.ParentCode == right.ParentCode);
        }

        public void CheckAnotherPermissions(AccessRightInfo right, bool onlyUpwardUpdate)
        {
            Console.Out.WriteLine("ChekAnotherPermissions fired!");
            
            var parent = GetParent(right);
            var childs = GetChilds(right).ToList();
            var anyAtLevel = AnyAllowedAtLevel(right);

            if (right.Allowed)
            {
                if (parent != null) parent.AllowedWithoutAuthoChanges = (true);
                if (!onlyUpwardUpdate) childs.ForEach(x => x.Allowed = true);
            }
            else
            {
                if (!anyAtLevel && parent != null) parent.AllowedWithoutAuthoChanges = (false);
                if (!onlyUpwardUpdate) childs.ForEach(x => x.Allowed = false);
            }


        }

    
    }
}
