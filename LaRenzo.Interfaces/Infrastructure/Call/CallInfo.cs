﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities.Calls;
using System;

namespace LaRenzo.DataRepository.Repositories.Call
{
	public class CallInfo
	{
		/// <summary> Данные звонка </summary>
		public CallData CallData;
		
		/// <summary> Адрес </summary>
		public string Street;

		/// <summary> Адрес </summary>
		public string House;

		/// <summary> Адрес </summary>
		public string Appartament;

		/// <summary> Адрес: подъезд </summary>
		public string Porch;

		/// <summary> Номер карты </summary>
		public string CardNumber;

		/// <summary> Всего заказал на </summary>
		public float TotalSumm;

		/// <summary> Старые имена </summary>
		public readonly List<string> OldNames = new List<string>();

		/// <summary> Текущие заказы </summary>
		public List<Order> CurentOrders;
		/// <summary> Старые заказы </summary>
		public List<Order> LastOrders;

		public CallInfo(CallData сallData)
		{
			CallData = сallData;
		}

	    public async Task Initialyze(Action<CallData> saveAction, Func<string, bool, Task<List<Order>>> getOrdersByPhone)
	    {
	        await ContainOrders(getOrdersByPhone);
	        ContainName(saveAction);

	        GetInfoFromOrders();
	    }

		//_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Заполнить заказы </summary>
		////=====================================
		private async Task ContainOrders(Func<string, bool,Task<List<Order>>> getOrdersByPhone)
		{
			const bool IS_CURRENT_SESSION = true;
            
			CurentOrders = await getOrdersByPhone(CallData.PhoneNumber, IS_CURRENT_SESSION);
			LastOrders = await getOrdersByPhone(CallData.PhoneNumber, false);
		}

		
		//_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить информацию из заказов </summary>
		////===================================================
		private void GetInfoFromOrders()
		{
			ProcessList(CurentOrders);
			ProcessList(LastOrders);
		}


		private void ProcessList(List<Order> orders)
		{
			if (orders != null)
			{
				foreach (Order order in orders)
				{
					TotalSumm += order.TotalCost;

					if (string.IsNullOrEmpty(order.Street))
					{
						Street = order.Street;
					}

					if (string.IsNullOrEmpty(order.House))
					{
						House = order.House;
					}

					if (string.IsNullOrEmpty(order.Appartament))
					{
						Appartament = order.Appartament;
					}

					if (string.IsNullOrEmpty(order.Porch))
					{
						Porch = order.Porch;
					}

					if (order.DiscountCardID.HasValue)
					{
						CardNumber = order.DiscountCard.CardNumber.ToString();
					}
				}
			}
		}


		//_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Добавить старое имя </summary>
		////========================================
		private void AddOldName(string name)
		{

			if (string.IsNullOrEmpty(CallData.Name))
			{
				CallData.Name = name;
			}
			else
			{
				if (CallData.Name != name && !OldNames.Contains(name))
				{
					OldNames.Add(name);
				}
			}
		}
		

		//_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Установить имя </summary>
		////===================================
		private void ContainName(Action<CallData> saveAction)
		{
			// Установить текущее имя звонящего
			if (CurentOrders != null && CurentOrders.Count > 0 && string.IsNullOrEmpty(CallData.Name))
			{
				CallData.Name = CurentOrders[0].Name;

                saveAction(CallData);
			}

			// Установить прошлые имена
			if (CurentOrders != null)
			{
				foreach (Order order in LastOrders)
				{
					AddOldName(order.Name);
				}
			}
		}
	}
}
