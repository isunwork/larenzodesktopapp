﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using LaRenzo.DataRepository;
using NLog;

namespace LaRenzo.TransferLibrary
{
    public class TransferSerializer
    {
        public string Serialize<T>(T obj)
        {
            try
            {
                var stream = new StringWriter();
                new XmlSerializer(typeof(T)).Serialize(stream, obj);

                return stream.ToString();
            }
            catch (Exception ex)
            {
                UserMessage.Show(ex.Message);
                return null;
            }
        }

        public T Deserialize<T>(string xml, string source)
        {
            try
            {
                var stream = new StringReader(xml);
                var serializer = new XmlSerializer(typeof(T));
                serializer.UnknownElement += Serializer_UnknownElement;
                return (T)serializer.Deserialize(stream);
            }
            catch (Exception ex)
            {
                var logger = LogManager.GetCurrentClassLogger();
                UserMessage.Show(String.Format("Источник: {0}, {1}Текст сообщения: {2}. Подробности смотрите в папке logs", source, Environment.NewLine, ex.Message));

                var exceptionText = ex.Message;

                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    exceptionText += Environment.NewLine + ex.Message;
                    
                }

                logger.Error(string.Format("Произошла ошибка загрузки данных! {0} Исчтоник: {1}{0} Полученный xml-файл: {2}{0} Текст исключения: {3}", Environment.NewLine, source, xml, exceptionText));

                return default(T);
            }
        }

        private void Serializer_UnknownElement(object sender, XmlElementEventArgs e)
        {
            var element = (OrderInfo)e.ObjectBeingDeserialized;
            if (e.Element.Name == "Name")
            {
                element.Name = e.Element.InnerXml;
            }

            if (e.Element.Name == "Items")
            {
                var serializer = new XmlSerializer(typeof(OrderItemInfo));
                element.Items = new List<OrderItemInfo>();
                foreach (var orderItemInfo in e.Element.ChildNodes)
                {
                    try
                    {
                        var stream = new StringReader("<OrderItemInfo>" + ((XmlElement)orderItemInfo).InnerXml + "</OrderItemInfo>");
                        var newOrderItem = (OrderItemInfo)serializer.Deserialize(stream);
                        element.Items.Add(newOrderItem);
                    }
                    catch
                    {
                        element.Name += "Ошибка: Не удалось разобрать весь список блюд, уточните у клиента";
                    }
                }
            }
        }
    }
}
