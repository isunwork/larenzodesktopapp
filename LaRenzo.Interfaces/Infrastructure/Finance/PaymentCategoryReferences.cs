﻿using System;
using System.Collections.Generic;
using System.Linq;
using LaRenzo.DataRepository.Entities.Finance;

namespace LaRenzo.DataRepository.Repositories.Finance
{
    public class PaymentCategoryReferences
    {
        /// <summary>
        /// Возвращает ID категории и всей подкатегорий
        /// </summary>
        /// <param name="categoryID"></param>
        /// <param name="allCategories"></param>
        /// <returns></returns>
        /// 
        public List<int> GetCategoryReferences(int categoryID, List<PaymentCategory> allCategories)
        {
            var references = new List<int>();
            
            references.Add(categoryID);

            var parents = new List<int>();
            parents.Add(categoryID);

            var childs = new List<int>();

            do
            {
                childs = allCategories.Where(x => parents.Contains(x.Parent)).Select(x => x.ID).ToList();
                references.AddRange(childs);
                parents = childs;

            } while (childs.Any());

            return references;

        }
    }
}
