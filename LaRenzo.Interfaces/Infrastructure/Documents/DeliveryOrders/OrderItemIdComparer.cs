﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Entities.Documents.DeliveryOrders
{
    public class OrderItemIdComparer : IEqualityComparer<OrderItem>
    {
        public bool Equals(OrderItem x, OrderItem y)
        {
            return x.ID == y.ID;
        }

        public int GetHashCode(OrderItem obj)
        {
            return obj.ID.GetHashCode();
        }
    }
}
