﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LaRenzo.TransferLibrary
{
    public class PaymentMethodAlias
    {
        public PaymentMethod PaymentMethod { get; private set; }
        public string Alias { get; private set; }

        public List<PaymentMethodAlias> GetAll()
        {
            var list = new List<PaymentMethodAlias>
            {
                new PaymentMethodAlias
                {
                    Alias = "Наличные",
                    PaymentMethod = PaymentMethod.Cash
                },
                new PaymentMethodAlias
                {
                    Alias = "Картой курьеру",
                    PaymentMethod = PaymentMethod.OfflineCard
                },
                new PaymentMethodAlias
                {
                    Alias = "Картой онлайн",
                    PaymentMethod = PaymentMethod.OnlineCard
                },
            };
            return list;
        }
    }

}
