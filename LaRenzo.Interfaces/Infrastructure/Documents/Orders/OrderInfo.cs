﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LaRenzo.TransferLibrary
{
    public class OrderInfo
    {
        public int BrandId { get; set; }
        [XmlIgnore]
        public string Name { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public string Time { get; set; }

        public string DiscountCard { get; set; }

        /// <summary> Источник заказа </summary>
        public string Source { get; set; }

        public string Comment { get; set; }

        public int WebOrderId { get; set; }

        public Guid Guid { get; set; }

        public int IsNewSiteOrder { get; set; }

        public bool IsCallback { get; set; }

        public bool IsPaid { get; set; }

        public PaymentMethod PaymentMethod { get; set; }

        public int DeliveryMethod { get; set; }

        public string DeliveryMethodName
        {
            get
            {
                string retValue = "";
                switch (DeliveryMethod)
                {
                    case 1:
                        retValue = "Cамовывоз";
                        break;
                    case 2:
                        retValue = "Курьер";
                        break;
                }

                return retValue;
            }
        }
        [XmlIgnore]
        public List<OrderItemInfo> Items { get; set; }

        public string CouponCode { get; set; }
    }
    

}
