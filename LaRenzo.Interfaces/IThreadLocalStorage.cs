﻿using LaRenzo.DataRepository.Entities.Catalogs.Users;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.Interfaces
{
    public interface IThreadLocalStorage
    {
        Brand SelectedBrand { get; set; }
        User UserLogged { get; set; }
        int SelectedBranchId { get; set; }
        List<AccessRightInfo> UserRights { get; set; }
    }
}
