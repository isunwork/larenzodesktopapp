﻿using System;

namespace LaRenzo.DataRepository.Repositories.Documents.Interface
{
    public interface IDocumentWithDate
    {
        DateTime DocumentDate { get; set; }
    }
}
