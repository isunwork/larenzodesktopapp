﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Entities.Catalogs.DiscountCards
{
    public interface ICouponDish
    {
        int ID { get; }
        int CategoryID { get; }
        int Price { get; }
    }
}
