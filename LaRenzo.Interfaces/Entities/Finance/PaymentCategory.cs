﻿using System;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Finance
{
    public class PaymentCategory: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }
        public int Parent { get; set; }
        public string Name { get; set; }
        public bool IsImmutable { get; set; }
        public int BranchId { set; get; }
        public PaymentCategory()
        {

        }

        public PaymentCategory(string name, int parentId = 0)
        {
            Name = name;
            Parent = parentId;
        }
    }
}
