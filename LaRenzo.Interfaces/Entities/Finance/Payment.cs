﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Finance
{
    public class Payment: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public PaymentCategory Category { get; set; }

        public User User { get; set; }
        [ForeignKey("User")]
        public int UserID { get; set; }

        [ForeignKey("Category")]
        public int CategoryID { get; set; }

        public DateTime Created { get; set; }

        public string Comment { get; set; }

        [ForeignKey("Wallet")]
        public int WalletID { get; set; }
        public Wallet Wallet { get; set; }

        //Если платеж являются частью составной транзакции 
        //(например, перевод денег) То по полю TransferID можно получить 
        //все связанные транзакции

        public Guid? TransferID { get; set; }

        public decimal Total { get; set; }

        public bool IsActive { get; set; }
        public bool IsImmutable { get; set; }

        public int BranchId { set; get; }

    }
}
