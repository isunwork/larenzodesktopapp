﻿using System;
using LaRenzo.DataRepository.Repositories.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaRenzo.DataRepository.Entities.Catalogs.Printers
{
    public class PrinterTask: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [ForeignKey("Printer")]
        public int PrinterID { get; set; }
        public Printer Printer { get; set; }

        public string InstructionAlias { get; set; }
        public int BranchId { set; get; }
    }
}
