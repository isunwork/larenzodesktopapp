﻿using System;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Catalogs.Printers
{
    public class Printer: IBaseItem, IBaseBranchItem
    {    
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }
        public string Name { get; set; }
        public int BranchId { set; get; }

        public PointOfSale PointOfSale { get; set; }

        public int PointOfSaleId { get;set; }

        public override string ToString()
        {
            if (PointOfSale != null)
            {
                return $"{Name} - {PointOfSale.PointName}";
            }
            return Name;
        }
    }
}
