﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Catalogs.Sets
{
    public class SetOfDishes: IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [ForeignKey("Set")]
        public int SetID { set; get; }

        public Set Set { set; get; }

        [ForeignKey("Dish")]
        public int DishID { set; get; }

        public Dish Dish { set; get; }

        public int BranchId { set; get; }
    }
}
