﻿using System;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Catalogs
{
    public class ProgrammVersion: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }
        public string CurrentVersion { get; set; }
        public string SupportedVersion { get; set; }
        public string TestVersion { get; set; }
        public int BranchId { get; set; }
    }
}
