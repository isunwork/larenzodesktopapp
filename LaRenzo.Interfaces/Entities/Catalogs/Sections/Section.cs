﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Others.AmountFactories;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Catalogs.Sections
{
    public class Section: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [Required]
        public string Name { get; set; }

        public bool DoPrintCheck { get; set; }

        public decimal PeopleWorks { get; set; }

        public decimal EquipmentWorks { get; set; }

        [ForeignKey("PeopleFactorCollection")]
        public int? PeopleFactorCollectionID { get; set; }

        public AmountFactorCollection PeopleFactorCollection { get; set; }

        [ForeignKey("EquipmentFactorCollection")]
        public int? EquipmentFactorCollectionID { get; set; }

        public AmountFactorCollection EquipmentFactorCollection { get; set; }

		/// <summary> Id бренда </summary>
		public int IdBrand { get; set; }

		/// <summary> Бренд </summary>
		public Brand Brand { get; set; }
        public int BranchId { set; get; }
    }
}
