﻿using System;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Catalogs.Partners
{
    public class Partner: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public string PartnerName { set; get; }

        public string PartnerShortName { set; get; }

        public bool PartnerIsIndividual { set; get; }

        public string PartnerInn { set; get; }

        public string PartnerKpp { set; get; }

        public string PartnerOKPO { set; get; }

        public string PartnerFactAddress { set; get; }

        public string PartnerPostAddress { set; get; }

        public string PartnerAddress { set; get; }

        public string PartnerPhone { set; get; }

        public string PartnerFax { set; get; }

        public string PartnerEmail { set; get; }

        public string PartnerOtherInfo { set; get; }

        public string PartnerOptionInfo { set; get; }

		public string PartnerContactFace { set; get; }

		public string PartnerRemarkDayOrder { set; get; }
        public int BranchId { set; get; }
		
    }
}
