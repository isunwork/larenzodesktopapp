﻿using System;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Catalogs.Warehouses
{
    public class Warehouse : IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }
        public string Name { get; set; }
        public bool IsBase { get; set; }
        public int BranchId { set; get; }
        public int PointOfSaleId { get; set; } 
        public PointOfSale PointOfSale { get; set; }
        public int? WarehouseForDishId { get; set; } 
        public WarehouseForDish WarehouseForDish { get; set; }
    }
}
