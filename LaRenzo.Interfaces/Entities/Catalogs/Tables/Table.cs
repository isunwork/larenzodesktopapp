﻿namespace LaRenzo.DataRepository.Entities.Catalogs.Tables
{
    public class Table : Entity
    {
        public bool IsOpen { get; set; }
        
        public string Name{ get; set; }
        public string Comment{ get; set; }

	    public bool IsOpenP1 { get; set; }

        public bool IsOpenP2 { get; set; }

        public bool IsOpenP3 { get; set; }

        public bool IsOpenP4 { get; set; }

        public new int BranchId { set; get; }

        public override string ToString()
        {
            return Name;
        }
    }
}
