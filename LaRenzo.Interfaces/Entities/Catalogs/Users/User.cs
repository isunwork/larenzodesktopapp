﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Roles;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Catalogs.Users
{
    public class User: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Password { get; set; }

        /// <summary>
        /// Пароль кассового аппарата
        /// </summary>
        public CashboxPassword CashboxPassword { get; set; }

        [ForeignKey("Role")]
        public int RoleID { get; set; }

        public Role Role { get; set; }
        public int BranchId { set; get; }
    }
}
