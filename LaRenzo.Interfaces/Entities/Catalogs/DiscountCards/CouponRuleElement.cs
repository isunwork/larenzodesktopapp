﻿using System;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Catalogs.DiscountCards
{
    public class CouponRuleElement : IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }
        public int BranchId { set; get; }
        public int CouponRuleId { get; set; }
        public CouponRule CouponRule { get; set; }
        public int ElementId { get; set; }
        public CouponRuleElementEnum ElementContext { get; set; }
        public ElementApplyDirection ElementApplyDirection { get; set; }
    }

}
