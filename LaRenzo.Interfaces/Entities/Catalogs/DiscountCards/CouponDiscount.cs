﻿using LaRenzo.DataRepository.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Entities.Catalogs.DiscountCards
{
    public class CouponDiscount: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }
        public int BranchId { set; get; }
        public int DiscountInPercent { get; set; }
        public int SumToApply { get; set; }
        public DaysOfWeekEnum DaysOfWeek { get; set; }
        public TimeSpan TimeBegin { set; get; }
        public TimeSpan TimeEnd { set; get; }
        public int CouponRuleId { get; set; }
        public CouponRule CouponRule { get; set; }
    }
}
