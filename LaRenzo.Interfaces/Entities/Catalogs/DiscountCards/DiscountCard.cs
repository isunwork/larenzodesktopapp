﻿using System;
using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Documents.CafeOrders;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Catalogs.DiscountCards
{
    public class DiscountCard: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public int CardNumber { get; set; }

        public string Name { get; set; }

        public string FatherName { get; set; }

        public string Email { get; set; }

        public string Street { get; set; }

        public string House { get; set; }

        public int? Appaertment { get; set; }

        public int? Porch { get; set; }

        public string Phone { get; set; }

		public int Discount { get; set; }

        public ICollection<Order> Orders { get; set; }

        public ICollection<CafeOrderDocument> CafeOrders { get; set; }
        public int BranchId { set; get; }
    }
}
