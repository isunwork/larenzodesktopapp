﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Measures;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Catalogs.Products
{
    public class ItemOfProductSet: IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public int ParentProductId { set; get; }

        [ForeignKey("Product")]
        public int ProductId { set; get; }

        public Product Product { set; get; }

        [ForeignKey("Measure")]
        public int MeasureId { set; get; }

        public Measure Measure { set; get; }

        public decimal Amount { set; get; }
        public int BranchId { set; get; }
    }
}
