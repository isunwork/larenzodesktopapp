﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Partners;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Catalogs.Products
{
    public class PartnerOfProduct: IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [ForeignKey("Product")]
        public int ProductId { set; get; }

        public Product Product { set; get; }

        [ForeignKey("Partner")]
        public int PartnerId { set; get; }

        public Partner Partner { set; get; }
        public int BranchId { set; get; }
    }
}
