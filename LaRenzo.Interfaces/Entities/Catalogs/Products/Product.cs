﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Measures;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Catalogs.Products
{
    public class Product: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public string Name { get; set; }

        [ForeignKey("Measure")]
        public int MeasureID { get; set; }
        
        public Measure Measure { get; set; }

        [ForeignKey("ProductCategory")]
        public int ProductCategoryID { get; set; }
        
        public ProductCategory  ProductCategory  { get; set; }

        public decimal MinAmount { get; set; }

        public double FixPrice { get; set; }

        public bool IsSetProduct { set; get; }

        public string ChefExpertCode { set; get; }

        public bool IsMarkToDelete { get; set; }
        public int BranchId { set; get; }
    }
}
