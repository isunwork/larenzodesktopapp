﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Entities.Catalogs.Sections;
using LaRenzo.DataRepository.Entities.Catalogs.Sets;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Entities.Others.AmountFactories;
using LaRenzo.DataRepository.Entities.Settings;

namespace LaRenzo.DataRepository.Entities.Catalogs.Dishes
{
    // ReSharper disable InconsistentNaming
    public class Dish : Entity, ICouponDish
    {
        private string _internalName;

        [NotMapped]
        public string Comment{ get; set; }

        public IEnumerable<ItemOfDishSet> ItemsOfDishSets { set; get; }
        public string Name{ get; set; }
        public string InternalName
        {
            get { return string.IsNullOrWhiteSpace(_internalName) ? Name : _internalName; }
            set { _internalName = value; }
        }

        [DefaultValue(true)]
        public bool DiscountAppliсability { get; set; }

        public int Price { get; set; }

        public int CafePrice { get; set; }

        public int Weight { get; set; }

        public bool DoCountWhenDeliveryCostCalculated { get; set; }

        public bool IsDeleted { get; set; }

        [ForeignKey("Category")]
        public int CategoryID { get; set; }

        public Category Category { get; set; }

        [DefaultValue(5)]
        public int CookingTime { get; set; }

        [ForeignKey("FactorCollection")]
        public int? FactorCollectionID { get; set; }

        public AmountFactorCollection FactorCollection { get; set; }

        public int Cutlery { get; set; }

        public int SKU { get; set; }

        public bool CalculateByCoefficient { get; set; }

        [ForeignKey("Set")]
        public int? CoefficientId { get; set; }

        public Set Set { get; set; }

        public bool UseCustomWarehouse { get; set; }

        [ForeignKey("Warehouse")]
        public int? WarehouseID { get; set; }
        public Warehouse Warehouse { get; set; }

        public int? WarehouseForDishId { get; set; } 
        public WarehouseForDish WarehouseForDish { get; set; }

        public bool MarkGOST { get; set; }

        public bool MarkEAC { get; set; }

        public string Structure { get; set; }

        public int Protein { get; set; }

        public int Fat { get; set; }

        public int Carbohydrates { get; set; }

        public int Netto { get; set; }

        public int CoeffEnergy { get; set; }

        public int FitHour { get; set; }

        public int StartTemperature { get; set; }

        public int EndTemperature { get; set; }

        public string Manufacturer { get; set; }

        public string AddressProduction { get; set; }

        public string ChefExpertCode { get; set; }

        public string Barcode { get; set; }

        public string CTO { get; set; }

        public bool IsAlcohole { get; set; }

        public new int BranchId { set; get; }

        
        public override string ToString()
        {
            return Name;
        }

        /// <summary> Id бренда </summary>
        public int IdBrand { get; set; }

        /// <summary> Бренд </summary>
        public Brand Brand { get; set; }

        [ForeignKey("Section")]
        public int? SectionID{ get; set; }

        public Section Section { get; set; }
    }
}
