﻿using System;
using System.ComponentModel.DataAnnotations;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Catalogs.Client
{
    public class Client: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [Required]
        public string FirstName { get; set; }
        
        public string MiddleName { get; set; }

        public string LastName { get; set; }
        
        public string Email { get; set; }
        
        public DateTime? BirthDate { get; set; }

        public GenderEnum Gender { get; set; }
        
        public int BranchId { set; get; }
    }
}
