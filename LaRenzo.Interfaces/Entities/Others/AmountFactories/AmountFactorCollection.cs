﻿using System;
using LaRenzo.DataRepository.Repositories.Base;
using System.Collections.Generic;

namespace LaRenzo.DataRepository.Entities.Others.AmountFactories
{
    public class AmountFactorCollection: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public string Name { get; set; }

        public ICollection<AmountFactor> Data { get; set; }

        public int BranchId { set; get; }
    }
}
