﻿using System;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Others.HelpPages
{
    public class HelpPage : IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }
        public string InternalName { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public int? ParentID { get; set; }
        public int BranchId { set; get; }
    }
}
