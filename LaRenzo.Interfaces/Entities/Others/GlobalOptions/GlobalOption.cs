﻿using System;
using LaRenzo.DataRepository.Repositories.Base;
using System.ComponentModel.DataAnnotations;

namespace LaRenzo.DataRepository.Entities.Others.GlobalOptions
{
    public class GlobalOption: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [MaxLength(40)]
        public string Name { get; set; }

        [MaxLength(400)]
        public string Value { get; set; }        
        public int BrandId { set; get; }
        public int BranchId { set; get; }
    }
}
