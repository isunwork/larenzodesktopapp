﻿using LaRenzo.DataRepository.Repositories.Base;
using System;

namespace LaRenzo.DataRepository.Entities.Others.Messages
{
    public class Message : IBaseItem, IBaseBranchItem
    {
        public Message()
        {
        }

        public Message(string text, int parentId = 0)
        {
            Content = text;
            ParentID = parentId;
            Created = DateTime.Now.ToLocalTime();
        }

        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public int ParentID { get; set; }

        public string Content { get; set; }

        public DateTime Created { get; set; }

        public int BranchId { set; get; }

        public bool IsClosed { get; set; }

    }
}
