﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Partners;
using LaRenzo.DataRepository.Entities.Documents.Supply;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Wirings
{
    public class SettlsWithPartnerWirings : IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public DateTime Date { set; get; }

        [ForeignKey("Partner")]
        public int PartnerID { set; get; }

        public Partner Partner { set; get; }

        [ForeignKey("SupplyDocument")]
        public int SupplyDocumentID { set; get; }

        public SupplyDocument SupplyDocument { set; get; }

        public double Payment { set; get; }

        public double Sum { set; get; }

        public int BranchId { set; get; }
    }
}
