﻿using LaRenzo.DataRepository.Repositories.Base;
using System;

namespace LaRenzo.DataRepository.Entities.Wirings
{
    public class IntermediateGoodWirings : IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public DateTime DateTime { set; get; }

        public bool IsActual { set; get; }

        public int BranchId { set; get; }
    }
}
