﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Entities.Documents.Supply;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Wirings
{
    public class AveragePriceWirings: IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public DateTime Date { set; get; }

        [ForeignKey("Product")]
        public int ProductID { set; get; }

        public Product Product { set; get; }

        [ForeignKey("SupplyDocument")]
        public int SupplyDocumentID { set; get; }

        public SupplyDocument SupplyDocument { set; get; }

        public double Price { set; get; }        
        
        public int BranchId { set; get; }
    }
}
