﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Sections;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.PrintWorksModels
{
    public class DetailPrintWork : IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [ForeignKey("PrintWork")]
        public int? PrintWorksID { set; get; }

        public virtual PrintWork PrintWork { set; get; }

        [ForeignKey("Section")]
        public int? SectionID { set; get; }

        public virtual Section Section { set; get; }

        public bool IsBag { set; get; }

        public bool IsClient { set; get; }

        public bool IsDelivery { set; get; }

        public bool IsGeneral { set; get; }

        public int State { set; get; }

        public int BranchId { set; get; }
    }
}