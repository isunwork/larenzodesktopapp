﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Entities.Documents.Sessions;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.PrintWorksModels
{
    public class PrintWork : IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [ForeignKey("Order")]
        public int? OrderId { get; set; }
        public virtual Order Order { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Runned { get; set; }

        public bool IsSession { get; set; }

        [ForeignKey("Session")]
        public int? SessionId { get; set; }
        public Session Session { get; set; }

        public int State { get; set; }

        public int BranchId { set; get; }
    }
}
