﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Documents.Supply
{
    public class SupplyDocumentItem: IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [ForeignKey("SupplyDocument")]
        public int SupplyDocumentID { set; get; }

        public SupplyDocument SupplyDocument { set; get; }

        [ForeignKey("Product")]
        public int ProductID { set; get; }

        public Product Product { set; get; }

        public decimal Amount { set; get; }

        public double Price { set; get; }

        public double Sum { set; get; }
        public int BranchId { set; get; }
    }
}
