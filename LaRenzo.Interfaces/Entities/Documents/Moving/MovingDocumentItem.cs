﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Documents.Moving
{
    public class MovingDocumentItem: IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [ForeignKey("MovingDocument")]
        public int MovingDocumentID { set; get; }

        public MovingDocument MovingDocument { set; get; }

        [ForeignKey("Product")]
        public int ProductID { set; get; }

        public Product Product { set; get; }

        public decimal Amount { set; get; }

        public int BranchId { set; get; }
    }
}
