﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Tables;
using LaRenzo.DataRepository.Entities.Documents.Sessions;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Documents.CafeOrders
{
    

    // ReSharper disable InconsistentNaming
    public class CafeOrderDocument : DocumentEntity, IBaseBranchItem
    {
        public string Comment{ get; set; }
        public DateTime Created { get; set; }

        public string Code { get; set; }

        [DisplayName("Список блюд")]
        public IList<CafeOrderDocumentItem> CafeOrderItems { get; set; }


        [ForeignKey("Session")]
        public int? SessionID { get; set; }

        public Session Session { get; set; }

        [ForeignKey("Table")]
        public int? TableID { get; set; }

        public Table Table { set; get; }

        public string Note { get; set; }

        [ForeignKey("DiscountCard")]
        public int? DiscountCardID { get; set; }

        public DiscountCard DiscountCard { get; set; }

        public bool IsOnlinePayment { get; set; }

        public string OnlinePayCardName { get; set; }

        /// <summary> Тип оплаты </summary>
        public PaymentType PaymentType { get; set; }

        public int PersonsCount { get; set; }

        public int PlaceIndex { get; set; }

        public new int BranchId { set; get; }

        public PointOfSale PointOfSale { get; set; }

        public int PointOfSaleId { get;set; }
    }
}
