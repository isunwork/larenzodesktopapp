﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Documents.WriteoffDish
{
    public class WriteoffDishDocumentItem: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [ForeignKey("Dish")]
        public int DishID { get; set; }

        public Dish Dish { get; set; }

        public int Amount { get; set; }

        public double Price { set; get; }

        public double Sum { set; get; }

        [ForeignKey("WriteoffDishDocument")]
        public int WriteoffDishDocumentID { set; get; }

        public WriteoffDishDocument WriteoffDishDocument { set; get; }
        public int BranchId { set; get; }
    }
}