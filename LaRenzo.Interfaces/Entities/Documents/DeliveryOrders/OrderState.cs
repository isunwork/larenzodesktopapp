﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using LaRenzo.DataRepository.Entities.Finance;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Documents.DeliveryOrders
{
    public class OrderState: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public bool DoSessionCanBeClosed { get; set; }

        [Required]
        [DefaultValue((BankInteractionCommand)0)]
        public BankInteractionCommand BankInteractionMode { get; set; }

        public int BranchId { set; get; }
    }

}
