﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Documents.DeliveryOrders
{
    public class OrderItem : IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [ForeignKey("Dish")]
        public int DishID { get; set; }

        /*public**virtual*/
        public Dish Dish { get; set; }

        [Required]
        public int Amount { get; set; }

        public bool IsEditable { get; set; }

        [ForeignKey("Order")]
        public int OrderID { get; set; }
        public int BranchId { set; get; }
        public Order Order { get; set; }        
    }
}
