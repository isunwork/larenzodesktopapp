﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Documents.Sessions
{
    public class Session:IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [Required]
        public DateTime Opened { get; set; }

        public DateTime? Closed { get; set; }

        
        public int? TotalOrderCount { get; set; }

        [DisplayName("Выручка")]
        public int? TotalRevenue { get; set; }

        public int? TotalCafeOrderCount { get; set; }

        public decimal? TotalCafeRevenue { get; set; }

        [DisplayName("Заказы")]
        public ICollection<Order> Orders { get; set; }
        
        [DisplayName("Доставка выходного дня")]
        public bool IsVacationDelivery { get; set; }

        public TimeSpan Delta { set; get; }

        public TimeSpan DeltaWindow { set; get; }

        public bool IsProcessed { set; get; }

        public bool IsMarkToDelete { set; get; }

        public bool IsDepricated { set; get; }

        public bool CanEditTime { set; get; }
        
        public int BranchId { set; get; }
    }
}
