﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LaRenzo.DataRepository.Entities.Documents
{
    public class DocumentEntity : Entity
    {
        [NotMapped]
        public string Name{ get; set; }

        public decimal DocumentSum { get; set; }

        public string User { get; set; }

        public bool IsProcessed { get; set; }

        public bool IsMarkToDelete { get; set; }
    }
}
