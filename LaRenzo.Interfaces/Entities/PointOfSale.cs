﻿using System;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities
{
    /// <summary>
    /// Точка продаж
    /// </summary>
    public class PointOfSale : IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        /// <summary>
        /// Название филиала
        /// </summary>
        public string PointName { get; set; }
        
        /// <summary>
        /// Активна ли точка продаж
        /// </summary>
        public bool Active { get; set; }

        public int BranchId { get; set; }

        /// <summary> Ветка </summary>
        public Branch.Branch Branch { get; set; }
    }
}
