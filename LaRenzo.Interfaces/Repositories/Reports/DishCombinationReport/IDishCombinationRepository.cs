﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;

namespace LaRenzo.DataRepository.Repositories.Reports.DishCombinationReport
{
    public interface IDishCombinationRepository
    {
        List<Dish> GetActualDishList(IEnumerable<int> categories);
        List<DishCombinationSource> GetMostPopularDishCombination(int dishAmount);
    }
}