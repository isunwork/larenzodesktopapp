﻿using System;
using System.Collections.Generic;

namespace LaRenzo.DataRepository.Repositories.Reports.TransferOfGoodReport
{
    public interface ITransferOfGoodReportManager
    {
        List<TransferOfGoodDetailReportRecord> GetDetailRecords(DateTime start, DateTime end, int warehouseId, int productId);
        List<TransferOfGoodReportRecord> GetReportDate(DateTime start, DateTime end, int warehouseId);
    }
}