﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.Partners;
using LaRenzo.DataRepository.Entities.Catalogs.Products;

namespace LaRenzo.DataRepository.Repositories.Reports.PartnersProduct
{
    public interface IPartnersProductManager
    {
        List<PartnerOfProduct> GetDetailRecords(int partnerId);
        List<Partner> GetReportData();
    }
}