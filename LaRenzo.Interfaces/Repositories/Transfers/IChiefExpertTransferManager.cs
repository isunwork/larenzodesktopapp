﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Entities.Transfers;
using LaRenzo.DataRepository.Entities.Transfers.ChefExpertTransfer;

namespace LaRenzo.DataRepository.Repositories.Transfers
{
    /// <summary>
    /// TODO: Problem
    /// </summary>
    public interface IChiefExpertTransferManager
    {
        List<NewItemAction> GetActionList();
        List<ProductConformity> GetIngridients(string path);
        List<DishConformity> GetProducts(string path);
        bool SaveIngridientList(List<ProductConformity> conformities, int actionId, bool fromIng, ProductCategory folder, bool isSet = false);
        bool SaveProductList(List<DishConformity> conformities, int actionId, bool fromDish, Category category, string path);
        void SaveSemiproducts(string path);
    }
}