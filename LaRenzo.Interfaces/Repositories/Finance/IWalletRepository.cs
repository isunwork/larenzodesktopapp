﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Finance;

namespace LaRenzo.DataRepository.Repositories.Finance
{
    public interface IWalletRepository
    {
        void CreateDefaultWallets();
        void DeleteItem(Wallet entity);
        Wallet GetBankWallet();
        Wallet GetCashWallet();
        Wallet GetDebtWallet();
        Wallet GetDefaultWallet(WalletType type);
        Wallet GetDefaultWalletOrCreate(WalletType type);
        Wallet GetItem(FilterModel filter);
        Wallet GetItem(Wallet entity);
        IEnumerable<Wallet> GetItems();
        IEnumerable<Wallet> GetItems(FilterModel filter);
        void InsertItem(Wallet entity);
        void UpdateItem(Wallet entity);
    }
}