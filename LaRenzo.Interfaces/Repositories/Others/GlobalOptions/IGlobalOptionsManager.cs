﻿using System.Collections.Generic;

namespace LaRenzo.DataRepository.Repositories.Others.GlobalOptions
{
    public interface IGlobalOptionsManager
    {
        string AppDevEmail { get; set; }
        string EmailAddress { get; set; }
        string EmailPass { get; set; }
        int EmailSMTPPort { get; set; }
        string EmailSMTPPortStr { set; }
        string EmailSMTPServer { get; set; }
        string EmailUser { get; set; }
        bool EmailUseSSL { get; set; }
        string SiteDevEmail { get; set; }

        string GetCheckFooter(int brandId);
        string GetCheckFooterByBrandId(int orderBrandId);
        int GetCookTime(int brandId);
        int GetDeliveryPrice(int brandId);
        string GetFirstStringCheck(int brandId);
        int GetFreeDeliveryFrom(int brandId);
        int GetMaxTimeGap(int brandId);
        string GetOperName(int brandId);
        int GetOverheadTime(int brandId);
        int GetReserveTime(int brandId);
        string GetSites(int brandId);
        List<string> GetSitesList(int brandId);
        string GetVlCancelUrl(int brandId, int webOrderId);
        void SetCheckFooter(string value, int brandId);
        void SetCookTime(int value, int brandId);
        void SetDeliveryPrice(int value, int brandId);
        void SetFirstStringCheck(string value, int brandId);
        void SetFreeDeliveryFrom(int value, int brandId);
        void SetMaxTimeGap(int value, int brandId);
        void SetOperName(int value, int brandId);
        void SetOverheadTime(int value, int brandId);
        void SetReserveTime(int value, int brandId);
        void SetSites(string value, int brandId);
    }
}