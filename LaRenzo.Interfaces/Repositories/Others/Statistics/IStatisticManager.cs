﻿using System;
using System.Collections.Generic;
using LaRenzo.DataRepository.Repositories.Reports;
using LaRenzo.DataRepository.Repositories.Reports.DishCombinationReport;
using LaRenzo.DataRepository.Repositories.Reports.SalesReport;

namespace LaRenzo.DataRepository.Repositories.Others.Statistics
{
    public interface IStatisticManager
    {
        List<ClientsReport> GetAllClients();
        DeliveryTimeReport GetDeliveryTimeReportData(DateTime from, DateTime to);
        List<DishCombinationData> GetMostCompatibilityDishes();
        List<ClientsReportByOrders> GetNewClientReportData(DateTime from, DateTime to);
        List<ClientsReportByOrders> GetNotActiveClientReportData(DateTime from, DateTime to);
        List<SaleReportRecord> GetSalesReportData(DateTime start, DateTime end, int mode, bool isActiveDish);
        List<SaleReportRecord> GetSimpleSalesReportData(int sessionId, ReportModeEnum mode);
    }
}