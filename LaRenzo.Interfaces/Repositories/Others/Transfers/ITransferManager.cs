﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities.Documents.WebOrders;
using LaRenzo.TransferLibrary;

namespace LaRenzo.DataRepository.Repositories.Others.Transfers
{
    public interface ITransferManager
    {
        Task<string> GetPageAsString(Uri address);
        Task<List<OrderInfo>> GetUnhandledWebOrders(string site, string siteProtocol, int brandId);
        Task<List<OrderInfo>> GetWebOrderderFromSite(string uri, string source);
        Task<bool> MarkOrderProccesedOnWebSite(WebOrder order, int brandId, string siteProtocol);
        Task<string> SendBankCommandApprove(Guid orderGuid, int brandId, string siteProtocol);
        string SendBankCommandCancel(Guid orderGuid, int brandId, string siteProtocol);
    }
}