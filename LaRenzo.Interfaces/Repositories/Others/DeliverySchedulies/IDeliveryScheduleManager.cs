﻿using System;
using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.Addresses;
using LaRenzo.DataRepository.Entities.Others.DeliverySchedulies;

namespace LaRenzo.DataRepository.Repositories.Others.DeliverySchedulies
{
    public interface IDeliveryScheduleManager
    {
        void AddAddressGroup(AddressGroup addressGroup);
        void AddDeliverySchedule(List<int> dayOfWeek, TimeSpan timeBegin, TimeSpan timeEnd, TimeSpan waiting, int groupId, int poinOfSaleId, int priority);
        List<int> CheckDeliverySchedule(List<int> dayOfWeek, TimeSpan timeBegin, TimeSpan timeEnd, int groupId, int poinOfSaleId);
        void DeleteAddressGroup(AddressGroup addressGroup);
        void DeleteAddressGroup(int id);
        void DeleteDeliverySchedule(int id);
        List<AddressGroup> GetAddressGroupList();
        TimeSpan GetDeliveryTime(Address address, DateTime startTime, int pointOfSaleId);
        DeliverySchedule GetSchedule(Address address, DateTime startTime);
        List<DeliverySchedule> GetScheduleList();
        List<DeliverySchedule> GetScheduleList(Address address, DateTime startTime);
        bool TimePeriodOverlap(TimeSpan bs, TimeSpan be, TimeSpan ts, TimeSpan te);
        void UpdateAddressGroup(AddressGroup addressGroup);
        void UpdateAddressGroup(int id, string value);
        void UpdateDeliverySchedule(int scheduleId, List<int> dayOfWeek, TimeSpan timeBegin, TimeSpan timeEnd, TimeSpan waiting, int groupId, int poinOfSaleId, int priority);
    }
}