﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Documents.Moving;

namespace LaRenzo.DataRepository.Repositories.Documents.Moving
{
    public interface IMovingManager
    {
        int AddMovingDocument(MovingDocument document);
        void DeleteMovingDocument(MovingDocument document);
        MovingDocument GetMovingDocument(int movingDocId);
        List<MovingDocumentItem> GetMovingDocumentItems(int movingDocId);
        List<MovingDocument> GetMovingDocuments();
        string TestMovingDocument(int fromDocumentId, List<MovingDocumentItem> documentItems);
        void UpdateMovingDocument(MovingData movingData);
    }
}