﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Documents.Posting;

namespace LaRenzo.DataRepository.Repositories.Documents.Posting
{
    public interface IPostingManager
    {
        int AddPostingDocument(PostingDocument document);
        void DeletePostingDocument(PostingDocument document);
        PostingDocument GetPostingDocument(int postingDocId);
        List<PostingDocumentItem> GetPostingDocumentItems(int postingDocumentId);
        List<PostingDocument> GetPostingDocuments();
        void UpdatePostingDocument(PostingData postingData);
    }
}