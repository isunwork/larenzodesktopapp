﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Entities.Documents.Sessions;

namespace LaRenzo.DataRepository.Repositories.Documents.Sessions
{
    public interface ISessionManager
    {
        int AddDayCount(Session session);
        Task<CloseSessionResult> Close(Session session, CloseSessionOptions options, string siteProtocol);
        void DeleteSessionDocument(Session document);
        Session GetById(int id);
        List<Session> GetFullList();
        int GetLastClosedSessionId();
        DateTime GetLastSessionStartTime();
        Task<Session> GetOpened(int? branchId = null);
        List<Order> GetOrdersInSession(int sessionId);
        Session GetSessionDocument(int sessionId);
        List<Session> GetSessionDocuments();
        Task<bool> IsSessionOpen();
        bool IsSessionProcessed(int sessionId);
        List<SessionDocumentItem> LoadItemOfSession(int sessionId);
        Task<Session> Open(TimeSpan? time, bool isVacationDelivery, TimeSpan delta, TimeSpan deltaWindow, bool canEditTime);
        Task ProcessSessionDocument(int sessionId, BackgroundWorker bw = null);
        void ProcessSessionDocument(Session openSession, BackgroundWorker bw = null);
        bool SessionHasWirings(int sessionId);
        void TransferOrders(Session session, Session newSession);
        void UpdateSessionState(TimeSpan delta, TimeSpan deltaWindow, bool isVacation, bool canEditTime);
    }
}