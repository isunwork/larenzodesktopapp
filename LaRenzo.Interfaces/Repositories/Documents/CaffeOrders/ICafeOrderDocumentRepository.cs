﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Documents.CafeOrders;

namespace LaRenzo.DataRepository.Repositories.Documents.CaffeOrders
{
    public interface ICafeOrderDocumentRepository
    {
        int Count(int? sessionId, bool? isMarkToDelete);
        void DeleteItem(CafeOrderDocument entity);
        CafeOrderDocument GetItem(FilterModel filter);
        IEnumerable<CafeOrderDocument> GetItems(DateTime start, DateTime end);
        IEnumerable<CafeOrderDocument> GetItems(FilterModel filter);
        List<CafeOrderDocument> GetList(int? sessionId, bool? isMarkToDelete = null, PaymentType? paymentType = null);
        Task InsertItemAsync(CafeOrderDocument entity);
        void UpdateItem(CafeOrderDocument entity);
    }
}