﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Branch;

namespace LaRenzo.DataRepository.Repositories.Settings
{
    public interface IBranchRepository
    {
        int SelectedBranchId { get; set; }

        List<Branch> GetItems();
        List<Branch> GetList(FilterModel filter, bool withBranch = true);
    }
}