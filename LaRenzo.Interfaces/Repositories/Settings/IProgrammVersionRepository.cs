﻿using LaRenzo.DataRepository.Entities.Catalogs;
using System.Collections.Generic;

namespace LaRenzo.DataRepository.Repositories.Settings
{
    public interface IProgrammVersionRepository
    {
        List<ProgrammVersion> GetItems(bool withBranch = true);
    }
}
