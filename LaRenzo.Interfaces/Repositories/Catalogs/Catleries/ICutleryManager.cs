﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Catleries
{
    public interface ICutleryManager
    {
        List<OrderItem> GetCutleries(List<OrderItem> items);
    }
}