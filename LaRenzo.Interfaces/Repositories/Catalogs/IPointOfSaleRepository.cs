﻿
using LaRenzo.DataRepository.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Catalogs
{
    public interface IPointOfSaleRepository
    {
        List<PointOfSale> GetItems(bool withBranch = true);

        Task<int> InsertOrUpdateAsync(PointOfSale entity);
        void UpdateItems(List<PointOfSale> entities);
    }
}
