﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.Users;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Users
{
    public interface IUserAccessManager
    {
        List<AccessRightInfo> GetForUser(int userId, bool doIgnoreAdminFlag = false);
        bool GroupContainsRight(List<AccessRightInfo> list, AccessRightAlias rightAlias);
        void SetForUser(int userId, List<AccessRightInfo> rights);
    }
}