﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Warehouses
{
    public interface IWarehouseForDishManager
    {
        void AddWarehouse(WarehouseForDish entity);
        void DeleteWarehouse(WarehouseForDish entity);
        WarehouseForDish GetWarehouse(int id);
        List<WarehouseForDish> GetWarehouses();
        void UpdateWarehouse(WarehouseForDish entity);
    }
}