﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Catalogs.Tables;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Tables
{
    public interface ITableRepository
    {
        void DeleteItem(Table table);
        Table GetItem(FilterModel filter);
        Table GetItem(Table entity);
        IEnumerable<Table> GetItems();
        void InsertItem(Table table);
        void UpdateItem(Table table);
    }
}