﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Catalogs.Sets;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Sets
{
    public interface ISetManager
    {
        void AddDetailDishes(int setId, Dish dish);
        void AddSet(Set entity);
        void AddSet(Set entity, List<SetOfDishes> setOfDishes);
        void DeleteDetailDishes(SetOfDishes setOfDishes);
        void DeleteSet(Set entity);
        List<SetOfDishes> GetDetailDishes(int setId);
        Set GetSet(int id);
        List<Set> GetSets();
        void UpdateDetailSetItem(int setId, List<SetOfDishes> setOfDish);
        void UpdateSet(Set entity);
        void UpdateSet(Set entity, List<SetOfDishes> setOfDishes);
    }
}