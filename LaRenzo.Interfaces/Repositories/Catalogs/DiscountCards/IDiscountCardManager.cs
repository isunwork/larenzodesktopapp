﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.DiscountCards
{
    public interface IDiscountCardManager
    {
        bool Add(DiscountCard card);
        DiscountCard GetByID(int id);
        DiscountCard GetByNumber(int num);
        List<DiscountCard> GetList(); // TODO: cache
        int GetMaxCardNumber();
        List<Order> GetOrdersByID(int id);
        bool IsCardNumberValid(DiscountCard card, int oldId = 0);
        void MergeAll();
        void Update(DiscountCard card);
    }
}