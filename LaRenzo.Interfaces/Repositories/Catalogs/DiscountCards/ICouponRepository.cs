﻿using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Catalogs.DiscountCards
{
    public interface ICouponRepository
    {
        Coupon GetItem(int id);
        List<Coupon> GetList(string couponCode = null, bool? activated = null, int? couponRuleId = null, bool withBranch = true);
        Coupon GetCouponByCode(string code);
        Coupon GetCouponById(int id);
        void DeleteItem(Coupon entity);
        Task<int> InsertOrUpdateAsync(Coupon entity);
    }
}