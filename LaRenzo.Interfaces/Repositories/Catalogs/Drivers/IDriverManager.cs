﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities.Catalogs.Drivers;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Drivers
{
    public interface IDriverManager
    {
        void Add(Driver driver);
        Driver GetById(int id);
        Driver GetByName(string name);
        List<Driver> GetList();
        Driver Login(string name, string password);
        Task<Driver> LoginAsync(string name, string password);
        void Remove(int id);
        void Update(Driver driver);
        string Validate(Driver driver, bool requiredPass);
    }
}