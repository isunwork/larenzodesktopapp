﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Dishes
{
    public interface IDishRepository
    {
        Dish GetItem(int id);

        void DeleteItem(Dish entity);
        List<Tuple<Dish, double>> GetDishListWithCostPrice();
        Dish GetItem(FilterModel filter);
        IEnumerable<Dish> GetItems(FilterModel filter, bool withBranch = true);
        List<Tuple<Dish, int, int>> GetTopList(int dishId, int max = 0, bool byAmount = false);
        void InsertItem(Dish entity);
        void UpdateItem(Dish entity);
    }
}