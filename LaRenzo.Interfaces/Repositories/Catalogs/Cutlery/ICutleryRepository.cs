﻿using System.Collections.Generic;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Cutlery
{
    public interface ICutleryRepository
    {
        List<Cutlery> GetCutleryList();
    }
}