﻿using System;
using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Products
{
    public interface IProductManager
    {
        void Add(ProductData productData);
        void AddCategory(ProductCategory productCategory);
        void ChangeCategoryParent(int categoryId, int parentId);
        void CollapseItemsOfDishSet(List<ItemOfDishSet> items);
        IEnumerable<ItemOfDishSet> GetAllItemsOfDishSets();
        List<PartnerOfProduct> GetAllProductsByPartnerId(int patrnerId);
        List<Product> GetAllProductsInCategory(int catId, bool showHide = false);
        Product GetById(int id);
        List<ProductCategory> GetCategoriesList();
        ProductCategory GetCategoryById(int id);
        List<ItemOfProductSet> GetDetailProduct(int productId);
        List<Product> GetList(bool showHide = false);
        List<Tuple<Product, double>> GetProductListWithWeightedPrice();
        void Remove(int id);
        void RemoveCategory(int id);
        void Update(ProductData productData);
        void UpdateCategory(ProductCategory productCat);
    }
}