﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Categories
{
    public interface ICategoryRepository
    {
        List<Category> GetItems(bool withBranch = true);
        Category GetItem(int id);

        void UpdateItem(Category entity);

        void DeleteItem(Category entity);

        int InsertItem(Category entity);
    }
}