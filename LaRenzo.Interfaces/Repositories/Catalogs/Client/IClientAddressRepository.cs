﻿using LaRenzo.DataRepository.Entities.Catalogs.Client;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Client
{
    public interface IClientAddressRepository
    {
        Task<List<ClientAddress>> GetListAsync(int clientId, string street, string house, bool checkDateModified = false);
        Task<int> InsertOrUpdateAsync(ClientAddress entity);
    }
}
