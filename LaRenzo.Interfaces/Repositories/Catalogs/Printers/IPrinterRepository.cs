﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.Printers;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Printers
{
    public interface IPrinterRepository
    {
        void AddPrinter(Printer printer);
        void AddPrinterTask(PrinterTask printerTask);
        List<KeyValuePair<string, string>> GetOrderCheckTypes();
        Printer GetPrinterByName(string name);
        List<Printer> GetPrinterList();
        List<PrinterTask> GetPrinterTaskList();
        void RemovePrinter(int id);
        void RemovePrinterTask(int id);
    }
}