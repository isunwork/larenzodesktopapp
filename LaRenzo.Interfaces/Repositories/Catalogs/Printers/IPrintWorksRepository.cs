﻿using System;
using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.Printers;
using LaRenzo.DataRepository.Entities.Catalogs.Sections;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Entities.PrintWorksModels;
using LaRenzo.DataRepository.Entities.States;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Printers
{
    public interface IPrintWorksRepository
    {
        void AddDetailPrintWork(DetailPrintWork detailPrintWork);
        int AddPrintWork(PrintWork printWork);
        List<Printer> GetAllPrinters();
        string GetCheckName(DetailPrintWork detailPrintWork);
        List<PrintMonitorDetailRecord> GetDetailMonitorRecords(int printWorkId);
        DetailPrintWork GetDetailPrintWork(int printWorksId);
        List<PrintMonitorRecord> GetNMonitorRecords(int count, DateTime date);
        Order GetOrder(int? printWorksId);
        List<Printer> GetPrinter(Section section, int pointOfSaleId);
        List<Printer> GetPrinter(string instructionAlias, int pointOfSaleId);
        List<Printer> GetPrinterToRstAndEac(int sectionId, bool isRst, int pointOfSaleId);
        PrintWork GetPrintWork(int id);
        List<PrintWork> GetPrintWorks(State state);
        List<PrintWork> GetPrintWorks(State state, DateTime date);
        List<Section> GetSectionsByCafeOrderId(int? cafeDocumentOrderId);
        List<Section> GetSectionsByOrderId(int? orderId);
        string GetStateById(int state);
        void UpdatePrintWorkState(int id, State state);
        void UpdatePrintWorkState(PrintWork printWork, State state);
    }
}