﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.Interfaces
{
    public interface ISignalProcessor
    {
        void Raise<TSignalMessage>(TSignalMessage signalMessage) where TSignalMessage : class, new();
        void Bind<TSignalMessage>(Action<TSignalMessage> action);
        void UnBind<TSignalMessage>();
    }
}
