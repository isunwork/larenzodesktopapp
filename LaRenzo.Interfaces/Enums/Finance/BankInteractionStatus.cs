﻿namespace LaRenzo.DataRepository.Entities.Finance
{
    public enum BankInteractionStatus
    {
        Created,
        Sent,
        Overwritten
    }
    
}
