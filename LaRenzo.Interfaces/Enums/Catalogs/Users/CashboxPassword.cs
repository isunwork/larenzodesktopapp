﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Entities.Catalogs.Users
{
    public enum CashboxPassword
    {
        DefaultValue = 0,
        User1 = 1,
        User2 = 2,
        User3 = 3,
        User4 = 4,
        User5 = 5,
        User6 = 6,
        User7 = 7,
        User8 = 8,
        User9 = 9,
        User10 = 10,
        User11 = 11,
        User12 = 12,
        User13 = 13,
        User14 = 14,
        User15 = 15,
        Administrator = 30
    }
}
