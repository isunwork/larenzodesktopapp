﻿namespace LaRenzo.DataRepository.Entities.Catalogs.Client
{
    public enum GenderEnum
    {
        Unknown = 0,
        Male = 1,
        Female = 2
    }
}
