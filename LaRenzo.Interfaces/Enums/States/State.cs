﻿namespace LaRenzo.DataRepository.Entities.States
{
    public enum State
    {
        Creating,
        Processing,
        Compleated,
        Error
    }
}
