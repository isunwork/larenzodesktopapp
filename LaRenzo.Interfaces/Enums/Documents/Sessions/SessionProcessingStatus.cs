﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Documents.Sessions
{
    public enum SessionProcessingStatus
    {
        Processing,
        Saving,
        Done,
        LoadingData
    }
}
