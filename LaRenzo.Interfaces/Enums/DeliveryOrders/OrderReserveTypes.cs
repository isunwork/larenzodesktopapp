﻿namespace LaRenzo.DataRepository.Entities.Documents.DeliveryOrders
{
    public enum OrderReserveTypes
    {
        ForwardReserve = 0,

        BackwardReserve = 1
    }
}
