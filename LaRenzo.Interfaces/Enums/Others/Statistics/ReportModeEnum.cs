﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Others.Statistics
{
    public enum ReportModeEnum
    {
        All = 0,
        Cafe = 1,
        Delivery = 2
    }
}
