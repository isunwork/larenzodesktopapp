﻿namespace LaRenzo.DataRepository.Repositories.Others.States
{
    public enum EntityChangeState
    {
        EditEntity = 0,

        NewEntity = 1
    }
}
