﻿namespace LaRenzo.ControlsLib.Other
{
    partial class NowYesterdayOtherDatePicker
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nowButton = new System.Windows.Forms.Button();
            this.YesterdayButton = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // nowButton
            // 
            this.nowButton.BackColor = System.Drawing.Color.LightGreen;
            this.nowButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nowButton.Location = new System.Drawing.Point(3, 3);
            this.nowButton.Name = "nowButton";
            this.nowButton.Size = new System.Drawing.Size(144, 65);
            this.nowButton.TabIndex = 0;
            this.nowButton.Text = "Сейчас";
            this.nowButton.UseVisualStyleBackColor = false;
            this.nowButton.Click += new System.EventHandler(this.nowButton_Click);
            // 
            // YesterdayButton
            // 
            this.YesterdayButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.YesterdayButton.Location = new System.Drawing.Point(3, 74);
            this.YesterdayButton.Name = "YesterdayButton";
            this.YesterdayButton.Size = new System.Drawing.Size(144, 34);
            this.YesterdayButton.TabIndex = 1;
            this.YesterdayButton.Text = "Вчера";
            this.YesterdayButton.UseVisualStyleBackColor = true;
            this.YesterdayButton.Click += new System.EventHandler(this.YesterdayButton_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateTimePicker1.Location = new System.Drawing.Point(3, 114);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(144, 20);
            this.dateTimePicker1.TabIndex = 2;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // NowYesterdayOtherDatePicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.YesterdayButton);
            this.Controls.Add(this.nowButton);
            this.Name = "NowYesterdayOtherDatePicker";
            this.Size = new System.Drawing.Size(153, 139);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button nowButton;
        private System.Windows.Forms.Button YesterdayButton;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;

    }
}
