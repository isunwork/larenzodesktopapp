﻿namespace LaRenzo.ControlsLib.BindFilterListControl
{
    partial class BindFilterList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbFilter = new System.Windows.Forms.TextBox();
            this.lbData = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // tbFilter
            // 
            this.tbFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbFilter.Location = new System.Drawing.Point(0, 0);
            this.tbFilter.Name = "tbFilter";
            this.tbFilter.Size = new System.Drawing.Size(473, 20);
            this.tbFilter.TabIndex = 0;
            this.tbFilter.TextChanged += new System.EventHandler(this.tbFilter_TextChanged);
            this.tbFilter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbFilter_KeyDown);
            // 
            // lbData
            // 
            this.lbData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbData.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbData.FormattingEnabled = true;
            this.lbData.IntegralHeight = false;
            this.lbData.Location = new System.Drawing.Point(0, 20);
            this.lbData.Name = "lbData";
            this.lbData.Size = new System.Drawing.Size(473, 499);
            this.lbData.TabIndex = 2;
            this.lbData.SelectedIndexChanged += new System.EventHandler(this.lbData_SelectedIndexChanged);
            this.lbData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lbData_KeyDown);
            // 
            // BindFilterList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lbData);
            this.Controls.Add(this.tbFilter);
            this.Name = "BindFilterList";
            this.Size = new System.Drawing.Size(473, 519);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbFilter;
        private System.Windows.Forms.ListBox lbData;
    }
}
