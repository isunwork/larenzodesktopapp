﻿namespace LaRenzo.ControlsLib.TableControls
{
    partial class TableControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.btnMain = new System.Windows.Forms.Button();
			this.lblTimer = new System.Windows.Forms.Label();
			this.placeButton1 = new System.Windows.Forms.Button();
			this.placeButton2 = new System.Windows.Forms.Button();
			this.placeButton3 = new System.Windows.Forms.Button();
			this.placeButton4 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnMain
			// 
			this.btnMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnMain.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnMain.Location = new System.Drawing.Point(0, 0);
			this.btnMain.Name = "btnMain";
			this.btnMain.Size = new System.Drawing.Size(324, 130);
			this.btnMain.TabIndex = 0;
			this.btnMain.Text = "Столик №1";
			this.btnMain.UseVisualStyleBackColor = true;
			// 
			// lblTimer
			// 
			this.lblTimer.AutoSize = true;
			this.lblTimer.Location = new System.Drawing.Point(470, 45);
			this.lblTimer.Name = "lblTimer";
			this.lblTimer.Size = new System.Drawing.Size(34, 13);
			this.lblTimer.TabIndex = 1;
			this.lblTimer.Text = "18:22";
			// 
			// placeButton1
			// 
			this.placeButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.placeButton1.Location = new System.Drawing.Point(0, 0);
			this.placeButton1.Name = "placeButton1";
			this.placeButton1.Size = new System.Drawing.Size(144, 56);
			this.placeButton1.TabIndex = 2;
			this.placeButton1.Text = "1";
			this.placeButton1.UseVisualStyleBackColor = true;
			// 
			// placeButton2
			// 
			this.placeButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.placeButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.placeButton2.Location = new System.Drawing.Point(176, 0);
			this.placeButton2.Name = "placeButton2";
			this.placeButton2.Size = new System.Drawing.Size(148, 56);
			this.placeButton2.TabIndex = 3;
			this.placeButton2.Text = "2";
			this.placeButton2.UseVisualStyleBackColor = true;
			// 
			// placeButton3
			// 
			this.placeButton3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.placeButton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.placeButton3.Location = new System.Drawing.Point(0, 80);
			this.placeButton3.Name = "placeButton3";
			this.placeButton3.Size = new System.Drawing.Size(144, 50);
			this.placeButton3.TabIndex = 4;
			this.placeButton3.Text = "3";
			this.placeButton3.UseVisualStyleBackColor = true;
			// 
			// placeButton4
			// 
			this.placeButton4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.placeButton4.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.placeButton4.Location = new System.Drawing.Point(176, 80);
			this.placeButton4.Name = "placeButton4";
			this.placeButton4.Size = new System.Drawing.Size(148, 50);
			this.placeButton4.TabIndex = 5;
			this.placeButton4.Text = "4";
			this.placeButton4.UseVisualStyleBackColor = true;
			// 
			// TableControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.placeButton3);
			this.Controls.Add(this.placeButton4);
			this.Controls.Add(this.placeButton2);
			this.Controls.Add(this.placeButton1);
			this.Controls.Add(this.lblTimer);
			this.Controls.Add(this.btnMain);
			this.Name = "TableControl";
			this.Size = new System.Drawing.Size(324, 130);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnMain;
        private System.Windows.Forms.Label lblTimer;
		private System.Windows.Forms.Button placeButton1;
		private System.Windows.Forms.Button placeButton2;
		private System.Windows.Forms.Button placeButton3;
		private System.Windows.Forms.Button placeButton4;
    }
}
