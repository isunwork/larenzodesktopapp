﻿using DataLib.Models;
using System.Data.Entity;

namespace LaRenzo.DataRepository
{
    public class DatabaseInitializer
    {
        public void Init()
        {
            //My initialization, enable to migrate DB to latest version
            Database.SetInitializer<PizzaAppDB>(null);
        }
    }
}
