﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Catalogs.Tables;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Tables
{
    public class TableRepository: ITableRepository
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        public Table GetItem(Table entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Tables.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == entity.ID);
            }
        }

        public IEnumerable<Table> GetItems()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Tables.Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
            }
        }

        public void InsertItem(Table table)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (table.IsNew)
                {
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        table.BranchId = BranchRepository.SelectedBranchId;
                    }
                    table.DateModified = DateTime.Now;
                    table.AuthorModified = UserManager.UserLogged.Name;
                    db.Tables.Add(table);
                    db.SaveChanges();
                }
            }
        }

        public void DeleteItem(Table table)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var entity = db.Tables.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == table.ID);

                if (entity != null)
                {
                    db.Tables.Remove(table);

                    db.SaveChanges();
                }
            }
        }

        public void UpdateItem(Table table)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (table.IsEdit && table.IsNew)
                {
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        table.BranchId = BranchRepository.SelectedBranchId;
                    }
                    table.DateModified = DateTime.Now;
                    table.AuthorModified = UserManager.UserLogged.Name;
                    db.Tables.Attach(table);

                    db.Entry(table).State = EntityState.Modified;

                    db.SaveChanges();
                }
            }
        }

        public Table GetItem(FilterModel filter)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var value = (TableFilter)filter;

                return db.Tables.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == value.TableID);
            }
        }
    }
}
