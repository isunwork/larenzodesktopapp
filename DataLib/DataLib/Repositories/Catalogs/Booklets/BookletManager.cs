﻿using System;
using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Booklets
{
    public class BookletManager: IBookletManager
    {
        public List<OrderItem> GetBooklets(int totalSum)
        {
            var items = new List<OrderItem>();

            if (totalSum < 700)
            {
                items.AddRange(SelectBooklets(1));
                return items;
            }
            if (totalSum <= 2000)
            {
                items.AddRange(SelectBooklets(2));
                return items;
            }
            if (totalSum > 2000 && totalSum < 10000)
            {
                var amount = (int)Math.Ceiling((float)totalSum/1000);
                items.AddRange(SelectBooklets(amount));
                return items;
            }

            items.AddRange(SelectBooklets(10));
            return items;
        }

        private IEnumerable<OrderItem> SelectBooklets(int amount)
        {
            var result = new List<OrderItem>();

            if (amount > 0)
                result.Add(new OrderItem
                               {
                                   Amount = amount,
                                   Dish = new Dish
                                              {
                                                  Name = "Буклет"
                                              }
                               });
            return result;
        }
    }
}
