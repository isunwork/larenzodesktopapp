﻿using DataLib.Models;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Catalogs
{
    public class PointOfSaleRepository: IPointOfSaleRepository
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }


        public List<PointOfSale> GetItems(bool withBranch = true)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = db.Set<PointOfSale>();
                if (withBranch)
                {
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        return result.Where(x => x.BranchId == BranchRepository.SelectedBranchId || x.BranchId == 0).ToList();
                    }
                }
                else
                {
                    return result.ToList();
                }
                return result.ToList();
            }
        }

        public async Task UpdateItemAsync(PointOfSale entity)
        {
            if (entity.ID != 0)
            {
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                using (var db = PizzaAppDB.GetNewContext())
                {
                    db.Entry(entity).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }
            }
        }

        public async Task<int> InsertItemAsync(PointOfSale entity)
        {
            if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
            {
                entity.BranchId = BranchRepository.SelectedBranchId;
            }
            entity.DateModified = DateTime.Now;

            entity.AuthorModified = UserManager.UserLogged.Name;
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Set<PointOfSale>().Add(entity);
                await db.SaveChangesAsync();
            }
               
            return entity.ID;
        }

        public async Task<int> InsertOrUpdateAsync(PointOfSale entity)
        {
            if (entity.ID == 0)
            {
                await InsertItemAsync(entity);
            }
            else
            {
                await UpdateItemAsync(entity);
            }
            return entity.ID;
        }

        public void UpdateItems(List<PointOfSale> entities)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                foreach (var entity in entities)
                {
                    if (entity.ID != 0)
                    {
                        entity.DateModified = DateTime.Now;
                        entity.AuthorModified = UserManager.UserLogged.Name;
                        db.Entry(entity).State = EntityState.Modified;
                    }
                }
                db.SaveChanges();
            }
            
        }
    }
}
