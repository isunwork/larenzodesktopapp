﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Catalogs.Measures;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;
using LaRenzo.DataRepository.Repositories.Wiringses.RemainsOfGood;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Products
{
    public class ProductManager: IProductManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public IRemainsOfGoodWiringsManager RemainsOfGoodWiringsManager { get; set; }

        public List<Product> GetList(bool showHide = false)
        {
            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                return showHide
                           ? db.Products.IncludeAll().Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList()
                           : db.Products.IncludeAll().Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.IsMarkToDelete == false).ToList();
            }
        }

        public List<ProductCategory> GetCategoriesList()
        {
            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                return db.ProductCategories.IncludeAll().Where(x => x.BranchId == BranchRepository.SelectedBranchId).OrderBy(x => x.Name).ToList();
            }
        }

        public List<Product> GetAllProductsInCategory(int catId, bool showHide = false)
        {
            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                if (catId == 0)
                {
                    return showHide
                               ? db.Products.Include("Measure").Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList()
                               : db.Products.Include("Measure").Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.IsMarkToDelete == false).ToList();

                }

                if (showHide)
                {
                    return
                   db.Products.Include("Measure")
                     .Include("ProductCategory")
                     .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                     .Where(x => x.ProductCategory.ID == catId)
                     .ToList();
                }
                return
                    db.Products.Include("Measure")
                      .Include("ProductCategory")
                      .Where(x => x.ProductCategory.ID == catId && x.IsMarkToDelete == false)
                      .ToList();
            }
        }


        //_____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Получить продукты по партнёру </summary>
        ////==================================================
        public List<PartnerOfProduct> GetAllProductsByPartnerId(int patrnerId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.PartnerOfProducts.Include("Product")
                    .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                    .Where(x => x.PartnerId == patrnerId).ToList();
            }
        }


        public void Add(ProductData productData)
        {
            var product = productData.Product;
            var productSets = productData.ProductSets;
            var partners = productData.Partners;
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    product.BranchId = BranchRepository.SelectedBranchId;
                }
                product.DateModified = DateTime.Now;
                product.AuthorModified = UserManager.UserLogged.Name;
                db.Products.Attach(product);
                db.Entry(product).State = EntityState.Added;
                db.SaveChanges();

                foreach (var ofProductSet in productSets)
                {
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        ofProductSet.BranchId = BranchRepository.SelectedBranchId;
                    }
                    ofProductSet.ParentProductId = product.ID;
                    ofProductSet.DateModified = DateTime.Now;
                    ofProductSet.AuthorModified = UserManager.UserLogged.Name;
                    db.ItemOfProductSets.Add(ofProductSet);
                }

                foreach (var partner in partners)
                {
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        partner.BranchId = BranchRepository.SelectedBranchId;
                    }
                    partner.ProductId = product.ID;
                    partner.DateModified = DateTime.Now;
                    partner.AuthorModified = UserManager.UserLogged.Name;
                    db.PartnerOfProducts.Add(partner);
                }

                db.SaveChanges();
            }
        }

        public void AddCategory(ProductCategory productCategory)
        {
            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    productCategory.BranchId = BranchRepository.SelectedBranchId;
                }
                db.ProductCategories.Attach(productCategory);
                productCategory.DateModified = DateTime.Now;
                productCategory.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(productCategory).State = EntityState.Added;
                db.SaveChanges();
            }
        }

        public void Remove(int id)
        {
            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                var product = db.Products.FirstOrDefault(x => x.BranchId == BranchRepository.SelectedBranchId && x.ID == id);

                if (product != null)
                {
                    product.IsMarkToDelete = !product.IsMarkToDelete;
                }

                db.SaveChanges();
            }
        }

        public void RemoveCategory(int id)
        {
            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                db.ProductCategories.Remove(db.ProductCategories.FirstOrDefault(x => x.BranchId == BranchRepository.SelectedBranchId && x.ID == id));
                db.SaveChanges();
            }
        }

        public void Update(ProductData productData)
        {
            var product = productData.Product;
            var productSets = productData.ProductSets;
            var partners = productData.Partners;
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    product.BranchId = BranchRepository.SelectedBranchId;
                }
                product.DateModified = DateTime.Now;
                product.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(product).State = EntityState.Modified;

                var existenItem = db.ItemOfProductSets.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.ParentProductId == product.ID).ToList();

                foreach (var itemOfProductSet in productSets)
                {
                    if (itemOfProductSet.ID == -1)
                    {
                        var iop = new ItemOfProductSet
                        {
                            MeasureId = itemOfProductSet.MeasureId,
                            ParentProductId = product.ID,
                            ProductId = itemOfProductSet.ProductId,
                            Amount = itemOfProductSet.Amount
                        };
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            iop.BranchId = BranchRepository.SelectedBranchId;
                        }
                        iop.DateModified = DateTime.Now;
                        iop.AuthorModified = UserManager.UserLogged.Name;
                        db.ItemOfProductSets.Add(iop);
                    }
                    else
                    {
                        if (existenItem.Any(x => x.ID == itemOfProductSet.ID))
                        {
                            var value = existenItem.FirstOrDefault(x => x.ID == itemOfProductSet.ID);
                            var entityToUpdate = db.ItemOfProductSets.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == itemOfProductSet.ID);
                            if (entityToUpdate != null) entityToUpdate.Amount = itemOfProductSet.Amount;
                            existenItem.Remove(value);
                        }
                    }
                }

                foreach (var productSet in existenItem)
                {
                    db.ItemOfProductSets.Remove(productSet);
                }


                var existenProducts = db.PartnerOfProducts.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.ProductId == product.ID).ToList();

                foreach (var ofProduct in partners)
                {
                    if (ofProduct.ID == -1)
                    {
                        var ofP = new PartnerOfProduct
                        {
                            PartnerId = ofProduct.PartnerId,
                            ProductId = product.ID,
                        };
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            ofP.BranchId = BranchRepository.SelectedBranchId;
                        }
                        ofP.DateModified = DateTime.Now;
                        ofP.AuthorModified = UserManager.UserLogged.Name;
                        db.PartnerOfProducts.Add(ofP);
                    }
                    else
                    {
                        if (existenProducts.Any(x => x.ID == ofProduct.ID))
                        {
                            var value = existenProducts.FirstOrDefault(x => x.ID == ofProduct.ID);
                            existenProducts.Remove(value);
                        }
                    }
                }

                foreach (var existenProduct in existenProducts)
                {
                    db.PartnerOfProducts.Remove(existenProduct);
                }

                db.SaveChanges();
            }
        }

        public void UpdateCategory(ProductCategory productCat)
        {
            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    productCat.BranchId = BranchRepository.SelectedBranchId;
                }
                productCat.DateModified = DateTime.Now;
                productCat.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(productCat).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
        
        public void ChangeCategoryParent(int categoryId, int parentId)
        {
            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                ProductCategory cat = db.ProductCategories.FirstOrDefault(x => x.BranchId == BranchRepository.SelectedBranchId && x.ID == categoryId);
                cat.DateModified = DateTime.Now;
                cat.AuthorModified = UserManager.UserLogged.Name;
                cat.ParentCategoryID = parentId;
                db.SaveChanges();
            }
        }

        public Product GetById(int id)
        {
            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                return db.Products.IncludeAll().Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == id);
            }
        }

        public ProductCategory GetCategoryById(int id)
        {
            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                return db.ProductCategories.IncludeAll().Where(x => x.BranchId == BranchRepository.SelectedBranchId).First(x => x.ID == id);
            }
        }
                
        public List<ItemOfProductSet> GetDetailProduct(int productId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return
                    db.ItemOfProductSets.Include("Measure")
                      .Include("Product")
                      .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                      .Where(x => x.ParentProductId == productId)
                      .ToList();
            }
        }

        public IEnumerable<ItemOfDishSet> GetAllItemsOfDishSets()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.ItemOfDishSets.Include(x => x.Dish).Include(x => x.Product).Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
            }
        }

        public void CollapseItemsOfDishSet(List<ItemOfDishSet> items)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                foreach (var item in items)
                {
                    db.Entry(item).State = EntityState.Modified;
                    ItemOfDishSet item1 = item;
                    db.ItemOfDishSets.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(
                        x => x.ID != item1.ID && x.DishId == item1.DishId && x.ProductId == item1.ProductId)
                        .ToList().ForEach(x => db.ItemOfDishSets.Remove(x));
                }

                db.SaveChanges();
            }
        }

        public List<Tuple<Product, double>> GetProductListWithWeightedPrice()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = new List<Tuple<Product, double>>();

                var productsAmountDataAllWh = RemainsOfGoodWiringsManager.GetRemainsOnDate(DateTime.Now).ProductRemains;

                var allSupplyItems = db.SupplyDocumentItems
                    .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                    .Where(x => x.SupplyDocument.IsProcessed && x.Price > 0)
                    .GroupBy(x => x.ProductID).Select(x => new
                {
                    productId = x.Key,
                    items = x.OrderByDescending(a => a.SupplyDocument.DocumentDate).Select(a => new
                    {
                        a.Price,
                        a.Amount,
                        Date = a.SupplyDocument.DocumentDate
                    }).Take(10)
                }).ToList();

                var allProducts = db.Products.Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();


                foreach (var product in allProducts)
                {

                    var weightedPrice = 0d;
                    var averagePrice = 0d;

                    var productSupplyItems = allSupplyItems.FirstOrDefault(x => x.productId == product.ID);

                    if (productSupplyItems == null)
                    {
                        result.Add(new Tuple<Product, double>(product, 0));
                        continue;
                    }

                    if (productSupplyItems.items.Any())
                    {
                        averagePrice = productSupplyItems.items.Average(x => x.Price);
                    }

                    var productAmountOnAllWh = productsAmountDataAllWh.Any(x => x.ProductID == product.ID)
                                ? productsAmountDataAllWh.First(x => x.ProductID == product.ID).Amount
                                : 0;

                    if (productAmountOnAllWh <= 0) weightedPrice = averagePrice;
                    else
                    {
                        var edgeTime = DateTime.Now.AddMonths(-1);

                        //Разделяем поставки на те, что были за последний месяц (календарный, не зависит от периода отчета) и до этого
                        var thisMonthRecords = productSupplyItems.items.Where(x => x.Date >= edgeTime).ToList();
                        var oldRecords = productSupplyItems.items.Where(x => x.Date < edgeTime).ToList();

                        //Находим средние цены за эти два периода
                        var thisMonthAveragePrice = thisMonthRecords.Any() ? thisMonthRecords.Average(x => x.Price) : 0;
                        var oldRecordsAveragePrice = oldRecords.Any() ? oldRecords.Average(x => x.Price) : 0;

                        //Находим количество остатков, которые нужно учитывать в разных периодах
                        var thisMonthTotal = thisMonthRecords.Any() ? thisMonthRecords.Sum(x => x.Amount) : 0;



                        var oldSuppliedTotal = productAmountOnAllWh - thisMonthTotal;
                        if (oldSuppliedTotal < 0)
                        {
                            thisMonthTotal = productAmountOnAllWh;
                            oldSuppliedTotal = 0;
                        }


                        weightedPrice = ((double)thisMonthTotal * thisMonthAveragePrice +
                                                      (double)oldSuppliedTotal * oldRecordsAveragePrice) /
                                                     (double)(thisMonthTotal + oldSuppliedTotal);

                    }

                    var item = new Tuple<Product, double>(product, weightedPrice);
                    result.Add(item);

                }

                return result;
            }
        }
    }
    public static class ProductManagerExtension
    {
        public static IEnumerable<ProductCategory> IncludeAll(this DbSet<ProductCategory> set)
        {
            return set.Include(x => x.DefaultMeasure);
        }

        public static IEnumerable<Product> IncludeAll(this DbSet<Product> set)
        {
            return set.Include(x => x.Measure).Include(x => x.ProductCategory);
        }
    }
}