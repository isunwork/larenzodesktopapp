﻿using System.Collections.Generic;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Cutlery
{
    public class CutleryRepository: ICutleryRepository
    {
        public List<Cutlery> GetCutleryList()
        {
            return new List<Cutlery>
                       {
                           new Cutlery{ID = 0, Name = "Нет прибора"},
                           new Cutlery{ID = 1, Name = "Ложка"},
                           new Cutlery{ID = 2, Name = "Вилка"},
                           new Cutlery{ID = 3, Name = "Палочки"},
                       };
        }
    }
}
