﻿using System.Collections.Generic;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Repositories.Catalogs.Cutlery;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Catleries
{
    public class CutleryManager: ICutleryManager
    {
        [Inject]
        public ICutleryRepository CutleryRepository { get; set; }

        public List<OrderItem> GetCutleries(List<OrderItem> items)
        {
            var returnItems = new List<OrderItem>();

            var cultleryTypes = CutleryRepository.GetCutleryList();
            
            // Удаляем тип, который описывает блюда без столовых приборов
            cultleryTypes.Remove(cultleryTypes.First(x => x.ID == 0));
            
            foreach (var type in cultleryTypes)
            {
                var amount = items.Where(x => x.Dish.Cutlery == type.ID).Sum(x => x.Amount);
                if (amount > 0)

                returnItems.Add(new OrderItem
                                    {
                                        Amount = amount,
                                        Dish = new Dish
                                                   {
                                                       Name = type.Name,
                                                   }
                                    });
            }
            
            return returnItems;
        }
    }
}
