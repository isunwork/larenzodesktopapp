﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Repositories.Catalogs.Categories;
using LaRenzo.DataRepository.Repositories.Catalogs.Products;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Dishes
{
    public class DishRepository: IDishRepository
    {
        [Inject]
        public IProductManager ProductManager { get; set; }
        [Inject]
        public ICategoryRepository CategoryRepository { get; set; }
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
                
        private IQueryable<Dish> GetItems(PizzaAppDB db, bool withBranch = true)
        {
            var result = db.Dishes
                .Include(x => x.Category)
                .Include(x => x.Category.Brand)
                .Include(x => x.FactorCollection)
                .Include(x => x.Set)
                .Include(x => x.Brand);
            if (withBranch)
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    result = result.Where(x => x.BranchId == BranchRepository.SelectedBranchId || x.BranchId == 0);
                }
            }
            result = result.OrderBy(x => x.Name);
            return result;
            
        }

        public virtual Dish GetItem(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = db.Dishes.Include(x => x.Category).Include(x => x.Category.Brand).Include(x => x.FactorCollection).Include(x => x.Set).Include(x => x.Brand);
                return result.FirstOrDefault(x => x.ID == id);
            }
        }
        

        public virtual IEnumerable<Dish> GetItems(FilterModel filter, bool withBranch = true)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var items = GetItems(db, withBranch);
                var value = filter as DishFilter;
                if (value == null)
                {
                    return items.ToList();
                }
                if (value.FilterByBrandId)
                {
                    return items.Where(x => x.Brand.ID == value.BrandId).OrderBy(x => x.Name).ToList();
                }
                if (value.FilterByCategoryName)
                {
                    if (value.FilterByIsDeleteMark)
                    {
                        return items.Where(x => x.Category.Name == value.CategoryName && x.Brand.ID == value.BrandId)
                          .ToList();
                    }

                    return items.Where(x => x.Category.Name == value.CategoryName && x.IsDeleted == false && x.Brand.ID == value.BrandId)
                         .ToList();
                }

                if (value.FilterByIsDeleteMark)
                {
                    return items.Where(x => x.Brand.ID == value.BrandId).ToList();
                }

                if (value.FilterBySKU)
                {
                    return items.Where(x => value.SKUArray.Contains(x.SKU) && x.Brand.ID == value.BrandId);
                }

                var dishes = items.Where(x => x.IsDeleted == false).ToList();
                dishes = dishes.Where(x => x.Brand.ID == value.BrandId).ToList();
                return dishes.ToList();
            }
        }

        public virtual Dish GetItem(FilterModel filter)
        {
            var value = filter as DishFilter;
            Dish dish = null;
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (value.FilterBySKU)
                {
                    dish = GetItems(db).FirstOrDefault(x => x.SKU == value.SKU && x.Brand.ID == value.BrandId);
                }

                if (value.FilterByDishId)
                {
                    dish = GetItems(db).FirstOrDefault(x => x.ID == value.DishId && x.Brand.ID == value.BrandId);
                }
            }

            if (dish != null)
            {
                using (var db = PizzaAppDB.GetNewContext())
                {
                    dish.ItemsOfDishSets = db.ItemOfDishSets
                        .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                        .Where(x => x.DishId == dish.ID)
                        .Include(x => x.Product).Include(x => x.Measure).ToList();
                }
            }

            return dish;
        }


        public virtual void UpdateItem(Dish entity)
        {
            entity.Category = null;
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (entity.IsEdit)
                {
                    entity.DateModified = DateTime.Now;
                    entity.AuthorModified = UserManager.UserLogged.Name;
                    var en = db.Entry(entity);
                    db.Set<Dish>().Attach(entity);
                    en.State = EntityState.Modified;
                }

                var existsItem = db.ItemOfDishSets.Where(x => x.DishId == entity.ID).ToList();

                foreach (var itemOfDishSet in entity.ItemsOfDishSets)
                {
                    if (itemOfDishSet.IsNew)
                    {
                        var itemOfDish = new ItemOfDishSet
                        {
                            Amount = itemOfDishSet.Amount,
                            DishId = itemOfDishSet.DishId,
                            MeasureId = itemOfDishSet.MeasureId,
                            ProductId = itemOfDishSet.ProductId
                        };
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            itemOfDish.BranchId = BranchRepository.SelectedBranchId;
                        }
                        db.ItemOfDishSets.Add(itemOfDish);
                    }
                    else
                    {
                        if (existsItem.Any(x => x.ID == itemOfDishSet.ID))
                        {
                            var item = existsItem.FirstOrDefault(x => x.ID == itemOfDishSet.ID);

                            if (item != null && itemOfDishSet.IsEdit)
                            {
                                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                                {
                                    item.BranchId = BranchRepository.SelectedBranchId;
                                }
                                item.Amount = itemOfDishSet.Amount;
                                db.Entry(item).State = EntityState.Modified;

                                existsItem.Remove(item);
                            }
                        }
                    }
                }

                foreach (var itemOfDishSet in existsItem)
                {
                    db.ItemOfDishSets.Remove(itemOfDishSet);
                }

                db.SaveChanges();
            }

            entity.Category = CategoryRepository.GetItem(entity.CategoryID);
        }


        public virtual void DeleteItem(Dish entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var dish = db.Dishes.FirstOrDefault(x => x.ID == entity.ID);

                if (dish != null)
                {
                    dish.IsDeleted = !dish.IsDeleted;
                }

                db.SaveChanges();
            }
        }


        public virtual void InsertItem(Dish entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    entity.BranchId = BranchRepository.SelectedBranchId;
                }
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                db.Dishes.Attach(entity);
                db.Entry(entity).State = EntityState.Added;

                foreach (var item in entity.ItemsOfDishSets)
                {
                    if (item.IsNew)
                    {
                        var itemOfSet = new ItemOfDishSet
                        {
                            Amount = item.Amount,
                            DishId = entity.ID,
                            MeasureId = item.MeasureId,
                            ProductId = item.ProductId
                        };
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            itemOfSet.BranchId = BranchRepository.SelectedBranchId;
                        }
                        db.ItemOfDishSets.Add(itemOfSet);
                    }
                }
                db.SaveChanges();
            }
        }

        public List<Tuple<Dish, double>> GetDishListWithCostPrice()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = new List<Tuple<Dish, double>>();

                var productsWithPrice = ProductManager.GetProductListWithWeightedPrice();

                // Список блюд
                var dishesList = db.Dishes
                    .Include(x => x.Category)
                    .Include(x => x.Category.Section)
                    .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                    .ToList();

                foreach (var dish in dishesList)
                {
                    double cost = 0;
                    Dish dish1 = dish;
                    var items = db.ItemOfDishSets.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DishId == dish1.ID);

                    foreach (var position in items)
                    {
                        var productPrice = productsWithPrice.First(x => x.Item1.ID == position.ProductId).Item2;
                        cost += productPrice * (double)position.Amount;
                    }

                    result.Add(new Tuple<Dish, double>(dish, cost));
                }
                return result;
            }
        }


        /// <summary> Возвращает список блюд, которые покупают чаще всего вместе с этим блюдом </summary>
        /// <param name="dishId">Id блюда, для которого выполняется поиск</param>
        /// <param name="max">Количество топ-блюд. 0 - все.</param>
        /// <param name="byAmount">Сортировать по количеству заказанных единиц (иначе по количеству заказов)</param>
        /// <returns></returns>
        public List<Tuple<Dish, int, int>> GetTopList(int dishId, int max = 0, bool byAmount = false)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var orders = db.OrderItems.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DishID == dishId).Select(x => x.OrderID).Distinct().ToList();
                var items =
                    db.OrderItems.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Include(x => x.Dish).Include(x => x.Dish.Category).Where(x => orders.Contains(x.OrderID))
                      .Where(x => x.DishID != dishId).ToList()
                      .GroupBy(x => x.DishID).Select(g => Tuple.Create(
                          g.Select(x => x.Dish).First(),
                          g.Sum(x => x.Amount),
                          g.GroupBy(x => x.OrderID).Count())).OrderByDescending(x => byAmount ? x.Item2 : x.Item3);
                return max == 0 ? items.ToList() : items.Take(max).ToList();
            }
        }
    }
}