﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using DataLib.Models;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Client
{
    public class ClientRepository: IClientRepository
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }

        public async Task<Entities.Catalogs.Client.Client> GetItemAsync(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return await db.Set<Entities.Catalogs.Client.Client>().FindAsync(id);
            }                
        }

        private async Task UpdateItemAsync(Entities.Catalogs.Client.Client entity)
        {
            if (entity.ID != 0)
            {
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                using (var db = PizzaAppDB.GetNewContext())
                {
                    db.Entry(entity).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }                    
            }
        }

        private async Task<int> InsertItemAsync(Entities.Catalogs.Client.Client entity)
        {
            if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
            {
                entity.BranchId = BranchRepository.SelectedBranchId;
            }
            entity.DateModified = DateTime.Now;
            entity.AuthorModified = UserManager.UserLogged.Name;
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Set<Entities.Catalogs.Client.Client>().Add(entity);
                await db.SaveChangesAsync();
                return entity.ID;
            }
               
        }

        public async Task<int> InsertOrUpdateAsync(Entities.Catalogs.Client.Client entity)
        {
            if (entity.ID == 0)
            {
                await InsertItemAsync(entity);
            }
            else
            {
                await UpdateItemAsync(entity);
            }
            return entity.ID;
        }
    }
}
