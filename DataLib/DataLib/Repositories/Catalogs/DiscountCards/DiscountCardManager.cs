﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.DiscountCards
{
    public class DiscountCardManager: IDiscountCardManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }

        public DiscountCard GetByNumber(int num)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.DiscountCards.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.CardNumber == num);
            }
        }


        public DiscountCard GetByID(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.DiscountCards.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == id);
            }
        }


        public bool IsCardNumberValid(DiscountCard card, int oldId = 0)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {

                return !db.DiscountCards.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Any(x => x.ID != oldId && x.CardNumber == card.CardNumber);
            }
        }


        public int GetMaxCardNumber()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                //Первые 300 карт не выдаются
                if (!db.DiscountCards.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Any()) return 300;
                return db.DiscountCards.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Max(x => x.CardNumber);
            }
        }


        public List<Order> GetOrdersByID(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.DiscountCards.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.ID == id).Include(x => x.Orders).First().Orders.ToList();
            }
        }


        public bool Add(DiscountCard card)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    card.BranchId = BranchRepository.SelectedBranchId;
                }
                card.DateModified = DateTime.Now;
                card.AuthorModified = UserManager.UserLogged.Name;
                db.DiscountCards.Add(card);
                db.SaveChanges();
                return true;
            }
        }


        public List<DiscountCard> GetList()
        {
            using (var db = PizzaAppDB.GetNewContext(120))
            {
                return db.DiscountCards.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Include(x => x.Orders).ToList();
            }
        }


        public void Update(DiscountCard card)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    card.BranchId = BranchRepository.SelectedBranchId;
                }
                card.DateModified = DateTime.Now;
                card.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(card).State = EntityState.Modified;
                db.SaveChanges();
            }
        }


        public void MergeAll()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var cards = db.DiscountCards.Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
                var orders = db.Orders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DiscountCardID != null).ToList();
                var cafeOrders = db.CaffeOrderDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DiscountCardID != null).ToList();

                var cardGroups = cards.GroupBy(x => x.CardNumber).Select(x => new
                {
                    CardNumber = x.Key,
                    List = x
                }).ToList();

                if (!cardGroups.Any(x => x.List.Count() > 1)) throw new DiscountCardMergeException();

                foreach (var cardGroup in cardGroups)
                {
                    var cardIds = cardGroup.List.Select(x => x.ID).ToList();
                    var groupOrders = orders.Where(x => cardIds.Contains(x.DiscountCardID.Value)).ToList();
                    var groupCafeOrders = cafeOrders.Where(x => cardIds.Contains(x.DiscountCardID.Value)).ToList();

                    var lastCardId = cardGroup.List.Max(x => x.ID);

                    foreach (var order in groupOrders)
                    {
                        order.DiscountCardID = lastCardId;
                    }

                    foreach (var cafeOrder in groupCafeOrders)
                    {
                        cafeOrder.DiscountCardID = lastCardId;
                    }

                    var cardsToRemove = cardGroup.List.Where(x => x.ID != lastCardId).ToList();
                    foreach (var discountCard in cardsToRemove)
                    {
                        db.DiscountCards.Remove(discountCard);
                    }
                }

                db.SaveChanges();
            }
        }
    }    
}
