﻿using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Catalogs.DiscountCards
{
    public class CouponRuleRepository: ICouponRuleRepository
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }

        public List<CouponRule> GetItems(bool withBranch = true)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = db.Set<CouponRule>();
                if (withBranch)
                {
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        return result.Where(x => x.BranchId == BranchRepository.SelectedBranchId || x.BranchId == 0).ToList();
                    }
                }
                else
                {
                    return result.ToList();
                }
                return result.ToList();
            }
        }

        public void DeleteItem(CouponRule entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Set<CouponRule>().Remove(entity);
                db.SaveChanges();
            }
        }

        private async Task UpdateItemAsync(CouponRule entity)
        {
            if (entity.ID != 0)
            {
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                using (var db = PizzaAppDB.GetNewContext())
                {
                    db.Entry(entity).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }
            }
        }

        private async Task<int> InsertItemAsync(CouponRule entity)
        {
            if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
            {
                entity.BranchId = BranchRepository.SelectedBranchId;
            }
            entity.DateModified = DateTime.Now;
            entity.AuthorModified = UserManager.UserLogged.Name;
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Set<CouponRule>().Add(entity);
                await db.SaveChangesAsync();
                return entity.ID;
            }
        }

        public async Task<int> InsertOrUpdateAsync(CouponRule entity)
        {
            if (entity.ID == 0)
            {
                await InsertItemAsync(entity);
            }
            else
            {
                await UpdateItemAsync(entity);
            }
            return entity.ID;
        }
    }
}
