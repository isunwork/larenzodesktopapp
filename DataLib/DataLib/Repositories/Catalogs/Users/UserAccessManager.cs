﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Users
{
    public class UserAccessManager: IUserAccessManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }

        /// <summary>  Права связаны с элементами интерфейса через поле tag </summary>
        private List<AccessRightInfo> List = new List<AccessRightInfo>
        {
            new AccessRightInfo("Главное меню", 1),

                new AccessRightInfo("История", 11, 1),
                    new AccessRightInfo("Сессии", 111, 11),
                    new AccessRightInfo("Кафе", 112, 11),
                    new AccessRightInfo("Управление временем", 12, 1),
                    new AccessRightInfo("Таблица времени", 121, 12),
                    new AccessRightInfo("Расписание доставки", 122, 12),

                new AccessRightInfo("Справочники", 13, 1),
                    new AccessRightInfo("Поставщики", 131, 13),
                    new AccessRightInfo("Склады", 132, 13),
                    new AccessRightInfo("Столики", 133, 13),
                    new AccessRightInfo("Сотрудники", 134, 13),
                        new AccessRightInfo("Пользователи", 1341, 134),
                        new AccessRightInfo("Водители", 1342, 134),
                        new AccessRightInfo("Права доступа", 1343, 134),

                    new AccessRightInfo("Блюда", 135, 13),
                        new AccessRightInfo("Категории", 1351, 135),
                        new AccessRightInfo("Блюда", 1352, 135),
                        new AccessRightInfo("Цеха", 1353, 135),
                        new AccessRightInfo("Наборы коэффициентов", 1354, 135),

                    new AccessRightInfo("Продукты", 136, 13),
                        new AccessRightInfo("Продукты", 1361, 136),
                        new AccessRightInfo("Категории продуктов", 1362, 136),

                    new AccessRightInfo("Клиенты", 137, 13),
                        new AccessRightInfo("Скидочные карты", 1371, 137),
                        new AccessRightInfo("Черный список", 1372, 137),

                    new AccessRightInfo("Классификаторы", 138, 13),
                        new AccessRightInfo("Единицы измерения", 1381, 138),
                        new AccessRightInfo("Адреса", 1382, 138),

                    new AccessRightInfo("Наборы", 139, 13),
                        new AccessRightInfo("Наборы", 1391, 139),
                        new AccessRightInfo("Правила рассчета наборов", 1392, 139),

                new AccessRightInfo("Документы", 14, 1),
                    new AccessRightInfo("Продажи", 141, 14),
                        new AccessRightInfo("Кафе", 1411, 141),
                        new AccessRightInfo("Доставка", 1412, 141),

                    new AccessRightInfo("Закупка и возврат", 142, 14),
                        new AccessRightInfo("Документы поступления", 1421, 142),
                        new AccessRightInfo("Документы возвратов", 1422, 142),

                    new AccessRightInfo("Внутреннее товародвижение", 143, 14),
                        new AccessRightInfo("Документы перемещения", 1431, 143),
                        new AccessRightInfo("Документы списания", 1432, 143),
                        new AccessRightInfo("Документы оприходывания", 1433, 143),
                        new AccessRightInfo("Инвентаризация", 1434, 143),
                        new AccessRightInfo("Списание блюд", 1435, 143),

                    new AccessRightInfo("Регламентные документы", 144, 14),
                        new AccessRightInfo("Закрытие кассовой смены", 1441, 144),

                new AccessRightInfo("Отчеты", 15, 1),
                    new AccessRightInfo("По продажам", 151, 15),
                    new AccessRightInfo("Остатки товаров на складах", 152, 15),
                    new AccessRightInfo("Взаиморачеты с контрагентами", 153, 15),
                    new AccessRightInfo("Движение товаров на складах", 154, 15),
                    new AccessRightInfo("Поставщик -> продукты", 155, 15),
                    new AccessRightInfo("Комбинации блюд", 156, 15),
                    new AccessRightInfo("Отчет по отказам", 157, 15),

                new AccessRightInfo("Настройки", 16, 1),
                    new AccessRightInfo("Настройки программы", 161, 16),
                    new AccessRightInfo("Принтеры", 162, 16),
                    new AccessRightInfo("Обмен данными", 163, 16),

                new AccessRightInfo("Помощь", 18, 1),
                    new AccessRightInfo("Помощь по программе", 181, 18),
                    new AccessRightInfo("О программе", 182, 18),
                    new AccessRightInfo("Связь с разработчиками", 183, 18),

            new AccessRightInfo("Подменю", 2),
                new AccessRightInfo("Открытие / закрытие сессии", 20, 2),
                new AccessRightInfo("Настройки сессии", 21, 2),
                new AccessRightInfo("Чат", 22, 2),
                new AccessRightInfo("Монитор производительности", 23, 2),
                new AccessRightInfo("Монитор печати", 24, 2),
                new AccessRightInfo("Окно тестирования принтеров", 25, 2),

            new AccessRightInfo("Основные вкладки", 3),
                new AccessRightInfo("Заказы (Доставка)", 31, 3),
                    new AccessRightInfo("Просмотр", 311, 31),
                    new AccessRightInfo("Редактирование", 313, 31),
                    new AccessRightInfo("Редактирование статуса заказа через выпадающее меню", 314, 31),
                    new AccessRightInfo("Редактирование типа оплаты через выпадающее меню", 315, 31),
                    new AccessRightInfo("Просмотр суммы и количества всех заказов", 316, 31),
                    new AccessRightInfo("Предзаказы", 317, 31),
                        new AccessRightInfo("Просмотр списка предзаказов за следующие дни", 3171, 317),
                        new AccessRightInfo("Создание заказов из предзаказа", 3172, 317),
                        new AccessRightInfo("Редактирование предзаказов", 3173, 317),
                        new AccessRightInfo("Удаление предзаказов", 3174, 317),
                        new AccessRightInfo("Восстановление предзаказов", 3175, 317),
                    new AccessRightInfo("Изменение типа оплаты", 318, 31),

                new AccessRightInfo("Заказы (Кафе)", 32, 3),
                    new AccessRightInfo("Просмотр", 321, 32),
                    new AccessRightInfo("Редактирование", 323, 32),

            new AccessRightInfo("Дополнительно", 4),
                new AccessRightInfo("Повторная печать чека", 41, 4),
                new AccessRightInfo("Всплывающие уведомления", 42, 4),
                new AccessRightInfo("Редактирование документов учета", 44, 4),
        };


        private List<int> GetAccessCodesByUserId(int userId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.UserAccesses.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.UserID == userId).Select(x => x.Code).ToList();
            }
        }

        private void SetAccessCodesForUser(int userId, List<int> codes)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var currentRights = GetAccessCodesByUserId(userId);


                var deleteTargets = currentRights.Except(codes);
                var addTargets = codes.Except(currentRights);

                db.UserAccesses.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.UserID == userId && deleteTargets.Contains(x.Code)).ToList().ForEach(x => db.UserAccesses.Remove(x));
                foreach (var userAccess in addTargets.ToList())
                {
                    var user = new UserAccess { Code = userAccess, UserID = userId };
                    if (BranchRepository.SelectedBranchId  != (int)BranchIdEnum.AllBranchId)
                    {
                        user.BranchId = BranchRepository.SelectedBranchId;
                    }                    
                    user.DateModified = DateTime.Now;
                    user.AuthorModified = UserManager.UserLogged.Name;
                    db.UserAccesses.Add(user);
                }
                db.SaveChanges();
            }
        }

        public virtual List<AccessRightInfo> GetForUser(int userId, bool doIgnoreAdminFlag = false)
        {
            var listForUser = new List<AccessRightInfo>(List);

            listForUser.ForEach(x => x.AllowedWithoutAuthoChanges = false);

            if (userId == 0) return listForUser;

            if (!doIgnoreAdminFlag && UserManager.IsAdmin(userId))
            {
                listForUser.ForEach(x => x.AllowedWithoutAuthoChanges = true);
                return listForUser;
            }

            var rights = GetAccessCodesByUserId(userId);

            foreach (var right in rights)
            {
                var current = listForUser.FirstOrDefault(x => x.Code == right);
                if (current != null) current.AllowedWithoutAuthoChanges = true;
            }

            return listForUser;
        }

        public virtual void SetForUser(int userId, List<AccessRightInfo> rights)
        {
            SetAccessCodesForUser(userId, rights.Where(x => x.Allowed).Select(x => x.Code).ToList());
        }

        public virtual bool GroupContainsRight(List<AccessRightInfo> list, AccessRightAlias rightAlias)
        {
            return list.Where(x => x.Allowed).Any(x => x.Code == (int)rightAlias);
        }
    }

}
