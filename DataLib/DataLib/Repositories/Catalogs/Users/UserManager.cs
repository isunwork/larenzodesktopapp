﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Others;
using LaRenzo.DataRepository.Repositories.Settings;
using LaRenzo.Interfaces;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Users
{
    public class UserManager: IUserManager
    {
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public IThreadLocalStorage ThreadLocalStorage { get; set; }
        
        public User UserLogged
        {
            get => ThreadLocalStorage.UserLogged;
            set => ThreadLocalStorage.UserLogged = value;
        }

        public virtual void Add(User user)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    user.BranchId = BranchRepository.SelectedBranchId;
                }
                user.DateModified = DateTime.Now;
                user.AuthorModified = UserLogged.Name;
                db.Users.Attach(user);
                db.Entry(user).State = EntityState.Added;
                db.SaveChanges();
            }
        }

        public virtual void Remove(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (UserLogged != null && UserLogged.ID == id)
                    throw new Exception("Невозможно активную учетную запись себя.");

                db.Users.Remove(db.Users.FirstOrDefault(x => x.BranchId == BranchRepository.SelectedBranchId && x.ID == id));
                db.SaveChanges();
            }
        }

        public virtual void Update(User user)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    user.BranchId = BranchRepository.SelectedBranchId;
                }
                user.DateModified = DateTime.Now;
                user.AuthorModified = UserLogged.Name;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public virtual List<User> GetList()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Users.Where(x => x.BranchId == BranchRepository.SelectedBranchId).OrderBy(x => x.Name).ToList();
            }
        }

        /// <summary>
        /// Авторизация пользователя
        /// </summary>
        /// <param name="name"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public virtual User Login(string name, string password)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                password = new Hasher().GetHashString(password);
                UserLogged = db.Users.Include(x => x.Role).FirstOrDefault(x => x.BranchId == BranchRepository.SelectedBranchId && x.Name == name && x.Password == password);
                return UserLogged;
            }
        }

        public User GetById(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Users.Include(x => x.Role).Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == id);
            }
        }

        public User GetByName(string name)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Users.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.Name == name);
            }
        }

        public string Validate(User user, bool isUnique, bool requiredPass)
        {
            string errors = string.Empty;

            if (user.Name.Length < 5) errors += "Имя должно быть длиннее 4 символов\n";
            else if (isUnique && DoExist(user.Name)) errors += "Пользователь с таким именем уже существует. \n";

            if ((requiredPass && user.Password.Length < 5) || (!requiredPass && user.Password.Length > 0 && user.Password.Length < 5)) errors += "Пароль должен быть длиннее 4 символов.\n";

            return errors;
        }

        public bool DoExist(string name)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Users.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Any(x => x.Name == name);
            }
        }

        public virtual bool IsAdmin()
        {
            return IsAdmin(UserLogged);
        }

        public virtual bool IsAdmin(int userId)
        {
            var user = GetById(userId);
            return IsAdmin(user);
        }

        public virtual bool IsAdmin(User user)
        {
            return user != null && user.Role != null && user.Role.Name == "Admin";
        }

        public virtual bool IsWarehouseManager()
        {
            return UserLogged != null && UserLogged.Role != null && UserLogged.Role.Name == "WarehouseManager";
        }


        public virtual void Logout()
        {
            UserLogged = null;
        }

        public virtual bool IsLogged()
        {
            return (UserLogged != null);
        }

        public virtual string GetUserNameOrEmptyString()
        {
            return IsLogged() ? UserLogged.Name : string.Empty;
        }
    }
}
