﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Measures;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Measures
{
    public class MeasureManager: IMeasureManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        public Measure GetMeasure(int measureId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Measures.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == measureId);
            }
        }

        public Measure GetMeasure(Measure entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Measures.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == entity.ID);
            }
        }

        public List<Measure> GetMeasures()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Measures.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Include("ParentMeasure").OrderBy(x => x.ID).ToList();
            }
        }

        public void DeleteMeasure(Measure entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var measure = db.Measures.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == entity.ID);

                if (measure == null) return;

                db.Measures.Remove(measure);
                db.SaveChanges();
            }
        }

        public void UpdateMeasure(Measure entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    entity.BranchId = BranchRepository.SelectedBranchId;
                }                    
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void AddMeasure(Measure entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    entity.BranchId = BranchRepository.SelectedBranchId;
                }
                db.Measures.Attach(entity);
                db.Entry(entity).State = EntityState.Added;
                db.SaveChanges();
            }
        }
    }
}
