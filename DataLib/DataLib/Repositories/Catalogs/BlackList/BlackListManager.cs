﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.BlackList;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.BlackList
{
    public class BlackListManager: IBlackListManager
    {
        [Inject]
        public IBranchRepository BranchRepository { get; set; }

        /// <summary> Закешированный лист телефонов чёрного списка </summary>
        private List<string> cachBlackPhonesList = null;
        /// <summary> Закешированный лист адресов чёрного списка </summary>
        private List<string> cachBlackAddressList = null;

        /// <summary> Закэшированный чёрный список </summary>
        private List<string> CachPhoneList
        {
            get
            {
                if (cachBlackPhonesList == null)
                {
                    List<BlackListItem> list = GetList();
                    cachBlackPhonesList = new List<string>();

                    if (list != null)
                    {
                        foreach (var blackListItem in list)
                        {
                            cachBlackPhonesList.Add(blackListItem.Phone);
                        }
                    }
                }

                return cachBlackPhonesList;
            }
        }

        /// <summary> Закэшированный чёрный список </summary>
        private List<string> CachAddressList
        {
            get
            {
                if (cachBlackAddressList == null)
                {
                    List<BlackListItem> list = GetList();
                    cachBlackAddressList = new List<string>();

                    if (list != null)
                    {
                        foreach (var blackListItem in list)
                        {
                            cachBlackAddressList.Add(blackListItem.Street + blackListItem.House + blackListItem.Appartment);
                        }
                    }
                }

                return cachBlackAddressList;
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Является ли номер чёрным </summary>
        ////=============================================
        public bool IsNumberBlack(string phoneNumber)
        {
            bool isBlack = CachPhoneList.Contains(phoneNumber);

            return isBlack;
        }


        //____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Является ли адрес чёрным </summary>
        ////=============================================
        public bool IsAddressBlack(string street, string house, string appartment)
        {
            string allAddres = street + house + appartment;

            bool isBlack = CachAddressList.Contains(allAddres);

            return isBlack;
        }


        //____________________________________________________________________________________________________________________________________________________________________________________
        public List<BlackListItem> GetList()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.BlackListItems.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).ToList();
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________
        public List<BlackListItem> GetListByFilter(string phone, string street, string house, string appartment)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var address = street + house + appartment;
                var isAdressIsNotEmpty = !string.IsNullOrWhiteSpace(address);

                return db.BlackListItems.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.Phone == phone || (x.Street + x.House + x.Appartment == address && isAdressIsNotEmpty)).ToList();
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Сбросить кэш листы (делается при изменении данных в чёрном списе, чтобы при следующем к нему обращении перезагрузился) </summary>
        ////===========================================================================================================================================
        private void ResetCachLists()
        {
            cachBlackAddressList = null;
            cachBlackPhonesList = null;
        }


        //____________________________________________________________________________________________________________________________________________________________________________________
        public void Add(BlackListItem blackListItem)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (blackListItem.Created == default(DateTime))
                {
                    blackListItem.Created = DateTime.Now;
                }
                if (BranchRepository.SelectedBranchId  != (int)BranchIdEnum.AllBranchId)
                {
                    blackListItem.BranchId = BranchRepository.SelectedBranchId;
                }
                db.BlackListItems.Add(blackListItem);
                db.SaveChanges();
                // Сбросить кэш листы (делается при изменении данных в чёрном списе, чтобы при следующем к нему обращении перезагрузился) 
                ResetCachLists();
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________
        public void Remove(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var blackListItem = db.BlackListItems.Find(id);
                db.BlackListItems.Remove(blackListItem);
                db.SaveChanges();

                ResetCachLists();
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________
        public void Update(BlackListItem blackListItem)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (blackListItem.Created == default(DateTime))
                {
                    blackListItem.Created = DateTime.Now;
                }
                blackListItem.BranchId = BranchRepository.SelectedBranchId;
                db.Entry(blackListItem).State = EntityState.Modified;
                db.SaveChanges();

                ResetCachLists();
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________
        public BlackListItem GetById(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.BlackListItems.FirstOrDefault(x=>x.BranchId == BranchRepository.SelectedBranchId && x.ID == id);
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________
        public BlackListItem GetByName(string name)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.BlackListItems.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.Name == name);
            }
        }
    }
}
