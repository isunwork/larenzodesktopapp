﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Partners;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Partners
{
    public class PartnerManager: IPartnerManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        public List<Partner> GetPartners()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Partners.Where(x => x.BranchId == BranchRepository.SelectedBranchId).OrderBy(x => x.ID).ToList();
            }
        }

        public Partner GetPartner(int partnerId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Partners.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == partnerId);
            }
        }

        public void UpdatePartner(Partner entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    entity.BranchId = BranchRepository.SelectedBranchId;
                }                    
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void AddPartner(Partner entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    entity.BranchId = BranchRepository.SelectedBranchId;
                }                    
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                db.Partners.Add(entity);
                db.SaveChanges();
            }
        }

        public void DeletePartner(Partner entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                Partner partner = db.Partners.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == entity.ID);
                if (partner != null)
                {
                    db.Partners.Remove(partner);
                    db.SaveChanges();
                }
            }
        }


        //____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Получить партнёра по ID продукта </summary>
        ////=====================================================
        public List<PartnerOfProduct> GetPartnerByProductId(int prodId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.PartnerOfProducts.Include("Partner").Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.ProductId == prodId).ToList();
            }
        }
    }
}