﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Warehouses
{
    public class WarehouseForDishManager: IWarehouseForDishManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }

        public List<WarehouseForDish> GetWarehouses()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.WarehouseForDishes.Where(x => x.BranchId == BranchRepository.SelectedBranchId).OrderBy(x => x.ID).ToList();
            }
        }

        public WarehouseForDish GetWarehouse(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.WarehouseForDishes.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == id);
            }
        }

        public void AddWarehouse(WarehouseForDish entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    entity.BranchId = BranchRepository.SelectedBranchId;
                }
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                db.WarehouseForDishes.Add(entity);
                db.SaveChanges();
            }
        }

        public  void UpdateWarehouse(WarehouseForDish entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    entity.BranchId = BranchRepository.SelectedBranchId;
                }
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void DeleteWarehouse(WarehouseForDish entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var warehouse = db.WarehouseForDishes.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == entity.ID);
                if (warehouse != null)
                {
                    db.WarehouseForDishes.Remove(warehouse);
                    db.SaveChanges();
                }
            }
        }
    }
}
