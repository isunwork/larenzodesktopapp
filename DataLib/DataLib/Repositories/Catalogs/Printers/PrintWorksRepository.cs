﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLib.Models;
using System.Data.Entity;
using LaRenzo.DataRepository.Entities.Catalogs.Printers;
using LaRenzo.DataRepository.Entities.Catalogs.Sections;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Entities.PrintWorksModels;
using LaRenzo.DataRepository.Entities.States;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Printers
{
    public class PrintWorksRepository: IPrintWorksRepository
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        public int AddPrintWork(PrintWork printWork)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    printWork.BranchId = BranchRepository.SelectedBranchId;
                }
                db.PrintWorks.Add(printWork);
                db.SaveChanges();
                return printWork.ID;
            }
        }

        public void UpdatePrintWorkState(PrintWork printWork, State state)
        {
            printWork.State = (int)state;
            printWork.Runned = DateTime.Now;
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    printWork.BranchId = BranchRepository.SelectedBranchId;
                }
                db.Entry(printWork).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void UpdatePrintWorkState(int id, State state)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                PrintWork printWork = db.PrintWorks.FirstOrDefault(x => x.ID == id);
                if (printWork != null)
                {
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        printWork.BranchId = BranchRepository.SelectedBranchId;
                    }
                    printWork.State = (int)state;
                    db.Entry(printWork).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }

        public Order GetOrder(int? printWorksId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return
                    db.PrintWorks.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.ID == printWorksId)
                      .Select(x => x.Order)
                      .Include(x => x.OrderItems)
                      .Include(x => x.Driver)
                      .FirstOrDefault();
            }
        }

        public List<PrintWork> GetPrintWorks(State state)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return
                    db.PrintWorks.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Include(x => x.Order)
                      .Where(x => x.State == (int)state)
                      .OrderBy(x => x.Created)
                      .ToList();
            }
        }

        public List<PrintWork> GetPrintWorks(State state, DateTime date)
        {
            var startOfDay = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, 0);
            var endOfDay = new DateTime(date.Year, date.Month, date.Day, 23, 59, 59, 999);
            using (var db = PizzaAppDB.GetNewContext())
            {
                return
                    db.PrintWorks.Include(x => x.Order)
                      .Where(x=>x.BranchId == BranchRepository.SelectedBranchId)
                      .Where(x => x.State == (int)state && (x.Created > startOfDay && x.Created < endOfDay))
                      .OrderBy(x => x.Created)
                      .ToList();
            }
        }

        public PrintWork GetPrintWork(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.PrintWorks.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == id);
            }
        }

        public List<PrintMonitorDetailRecord> GetDetailMonitorRecords(int printWorkId)
        {
            var records = new List<PrintMonitorDetailRecord>();
            using (var db = PizzaAppDB.GetNewContext())
            {
                List<DetailPrintWork> detailPrintWorks =
                    db.DetailPrintWorks.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.PrintWorksID == printWorkId).Include(x => x.Section).ToList();

                records.AddRange(detailPrintWorks.Select(detailPrintWork => new PrintMonitorDetailRecord
                {
                    Name = GetCheckName(detailPrintWork),
                    State = GetStateById(detailPrintWork.State),
                    ID = detailPrintWork.ID,
                    PrintMonitorID = detailPrintWork.PrintWorksID
                }));

                return records;
            }
        }

        public string GetCheckName(DetailPrintWork detailPrintWork)
        {
            if (detailPrintWork.IsBag)
                return "Чек на сумку";

            if (detailPrintWork.IsClient)
                return "Чек для клиента";

            if (detailPrintWork.IsDelivery)
                return "Доставочный чек";

            if (detailPrintWork.IsGeneral)
                return "Кухня: Общий";

            if (detailPrintWork.Section != null)
            {
                return "Кухня: " + detailPrintWork.Section.Name;
            }

            return "<Неопределен>";
        }

        public List<PrintMonitorRecord> GetNMonitorRecords(int count, DateTime date)
        {
            var startOfDay = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, 0);
            var endOfDay = new DateTime(date.Year, date.Month, date.Day, 23, 59, 59, 999);
            var records = new List<PrintMonitorRecord>();
            using (var db = PizzaAppDB.GetNewContext())
            {
                List<PrintWork> printWorks =
                    db.PrintWorks.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).OrderByDescending(x => x.ID)
                      .Take(count)
                      .Include(x => x.Order)
                      .Include(x => x.Session)
                      .Where(x => (x.Created > startOfDay && x.Created < endOfDay))
                      .ToList();

                foreach (PrintWork printWork in printWorks)
                {
                    var record = new PrintMonitorRecord();

                    if (printWork.IsSession)
                    {
                        record.OrderId = printWork.SessionId;
                        record.OrderName = "Чек за сессию";
                        if (printWork.Session != null)
                            record.OrderTime = printWork.Session.Closed;
                    }
                    else
                    {
                        if (printWork.Order != null)
                        {
                            record.OrderId = printWork.Order.ID;
                            record.OrderName = printWork.Order.Code;
                            record.OrderTime = printWork.Order.Created;
                        }
                    }

                    record.ID = printWork.ID;
                    record.Created = printWork.Created;
                    record.LastTry = printWork.Runned;
                    record.State = GetStateById(printWork.State);

                    records.Add(record);
                }

                return records;
            }
        }

        public string GetStateById(int state)
        {
            switch (state)
            {
                case 0:
                    return "Создан";
                case 1:
                    return "В процессе";
                case 2:
                    return "Успешно";
                default:
                    return "Ошибка";
            }
        }

        //_____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Получить цеха кухни, которые задействованы в заказе </summary>
        ////========================================================================
        public List<Section> GetSectionsByOrderId(int? orderId)
        {
            var sectionList = new List<Section>(0);

            using (var db = PizzaAppDB.GetNewContext())
            {
                var sections = (from orderItem in db.OrderItems
                                where orderItem.OrderID == orderId
                                && orderItem.BranchId == BranchRepository.SelectedBranchId
                                select orderItem.Dish.Category.Section).Distinct().ToList();

                if (sections.Count != 0)
                {
                    sectionList = sections.Where(x => x.DoPrintCheck).ToList();
                }

                return sectionList;
            }
        }


        //_____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Получить цеха кухни, которые задействованы в заказе кафе </summary>
        ////=============================================================================
        public List<Section> GetSectionsByCafeOrderId(int? cafeDocumentOrderId)
        {
            var sectionList = new List<Section>(0);

            using (var db = PizzaAppDB.GetNewContext())
            {
                var sections = (from cafeOrderDocumentItem in db.CafeOrderDocumentItems
                                where cafeOrderDocumentItem.CafeOrderDocumentID == cafeDocumentOrderId
                                && cafeOrderDocumentItem.BranchId == BranchRepository.SelectedBranchId
                                select cafeOrderDocumentItem.Dish.Category.Section).Distinct().ToList();

                if (sections.Count != 0)
                {
                    sectionList = sections.Where(x => x.DoPrintCheck).ToList();
                }

                return sectionList;
            }
        }

        public DetailPrintWork GetDetailPrintWork(int printWorksId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return
                    db.DetailPrintWorks.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.ID == printWorksId)
                      .Include(x => x.Section)
                      .Include(x => x.PrintWork)
                      .FirstOrDefault();
            }
        }

        //_______________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Получить принетр по цеху для дипких чеков </summary>
        ///===============================================================
        public List<Printer> GetPrinterToRstAndEac(int sectionId, bool isRst, int pointOfSaleId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                string alias = "";
                if (sectionId != 0)
                {
                    if (isRst)
                    {
                        alias = PrinterRepository.DISH_CHECK_RST_DB_PREFIX + sectionId;
                    }
                    else
                    {
                        alias = PrinterRepository.DISH_CHECK_EAC_DB_PREFIX + sectionId;
                    }
                }

                var tasks = db.PrinterTasks.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.InstructionAlias == alias).Include(x => x.Printer).ToList();

                List<Printer> printerList = tasks.Count == 0 ? new List<Printer>(0) : (from task in tasks select task.Printer).Distinct().ToList();

                return printerList.Where(x=>x.PointOfSaleId == pointOfSaleId).ToList();
            }
        }


        //_______________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Получить принетр по цеху </summary>
        ///==============================================
        public List<Printer> GetPrinter(Section section, int pointOfSaleId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                string alias = PrinterRepository.KITCHEN_CHECK_DB_PREFIX + section.ID;

                var tasks = db.PrinterTasks.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.InstructionAlias == alias).Include(x => x.Printer).ToList();

                List<Printer> printerList = tasks.Count == 0 ? new List<Printer>(0) : (from task in tasks.Where(x=>x.Printer.PointOfSaleId == pointOfSaleId) select task.Printer).Distinct().ToList();

                return printerList;
            }
        }


        public List<Printer> GetAllPrinters()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Printers.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).ToList();
            }
        }

        public List<Printer> GetPrinter(string instructionAlias, int pointOfSaleId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return
                    db.PrinterTasks.Where(x=>x.BranchId == BranchRepository.SelectedBranchId)
                        .Where(x => x.InstructionAlias == instructionAlias)
                        .Include(x => x.Printer).Where(x=>x.Printer.PointOfSaleId == pointOfSaleId)
                        .Select(x => x.Printer)
                        .Distinct()
                        .ToList();
            }
        }

        public void AddDetailPrintWork(DetailPrintWork detailPrintWork)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    detailPrintWork.BranchId = BranchRepository.SelectedBranchId;
                }                    
                detailPrintWork.DateModified = DateTime.Now;
                detailPrintWork.AuthorModified = UserManager.UserLogged.Name;
                db.DetailPrintWorks.Add(detailPrintWork);
                db.SaveChanges();
            }
        }
    }
}