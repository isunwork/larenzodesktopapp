﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Printers;
using LaRenzo.DataRepository.Entities.Catalogs.Sections;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Printers
{
    public class PrinterRepository: IPrinterRepository
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }

        public const string SESSION_CHECK_DB_NAME = "session";
        public const string BAG_CHECK_DB_NAME = "bag";
        public const string CLIENT_CHECK_DB_NAME = "client";
        public const string DELIVERY_CHECK_DB_NAME = "delivery";
        public const string KITCHEN_CHECK_DB_PREFIX = "kitchen_";
        public const string CAFE_CHECK_DB_PREFFIX = "cafe_clnt";
        public const string CAFE_KITCHEN_DB_PREFFIX = "cafe_kitchen";
        public const string BAR_KITCHEN_DB_PREFFIX = " cafe_bar";
        public const string DISH_CHECK_EAC_DB_PREFIX = "dish_eac_";
        public const string DISH_CHECK_RST_DB_PREFIX = "dish_rst_";

        public List<KeyValuePair<string, string>> GetOrderCheckTypes()
        {
            var result = new List<KeyValuePair<string, string>>
                             {
                                 new KeyValuePair<string, string>(BAR_KITCHEN_DB_PREFFIX, "Бар"),
                                 new KeyValuePair<string, string>(CAFE_CHECK_DB_PREFFIX, "Чек для клиента кафе"),
                                 new KeyValuePair<string, string>(
                                     SESSION_CHECK_DB_NAME, "Чек за сессию"),
                                 new KeyValuePair<string, string>(
                                     BAG_CHECK_DB_NAME, "Чек на сумку"),
                                 new KeyValuePair<string, string>(
                                     CLIENT_CHECK_DB_NAME, "Чек для клиента"),
                                 new KeyValuePair<string, string>(
                                     DELIVERY_CHECK_DB_NAME, "Доставочный чек"),
                                 new KeyValuePair<string, string>(
                                     KITCHEN_CHECK_DB_PREFIX + "all",
                                     "Кухня: Общий"),
                                 new KeyValuePair<string, string>(CAFE_KITCHEN_DB_PREFFIX, "Кухня: Кафе"),
                             };

            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                List<Section> sections = db.Sections.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DoPrintCheck).ToList();

                // Список цехов кухни
                sections.ForEach(x => result.Add(new KeyValuePair<string, string>(string.Format("{0}{1}", KITCHEN_CHECK_DB_PREFIX, x.ID), string.Format("Кухня: {0}", x.Name))));

                // Список цехов ЕАС
                sections.ForEach(x => result.Add(new KeyValuePair<string, string>(string.Format("{0}{1}", DISH_CHECK_EAC_DB_PREFIX, x.ID), string.Format("Блюда ЕАС: {0}", x.Name))));

                // Список цехов РСТ
                sections.ForEach(x => result.Add(new KeyValuePair<string, string>(string.Format("{0}{1}", DISH_CHECK_RST_DB_PREFIX, x.ID), string.Format("Блюда РСТ: {0}", x.Name))));
            }

            return result;
        }

        public List<Printer> GetPrinterList()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Printers.Include("PointOfSale").Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
            }
        }

        public List<PrinterTask> GetPrinterTaskList()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.PrinterTasks.Include("Printer").Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
            }
        }

        public Printer GetPrinterByName(string name)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return
                    db.Printers.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(
                        x => x.Name == name);
            }
        }

        public void AddPrinter(Printer printer)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId  != (int)BranchIdEnum.AllBranchId)
                {
                    printer.BranchId = BranchRepository.SelectedBranchId;
                }                    
                printer.DateModified = DateTime.Now;
                printer.AuthorModified = UserManager.UserLogged.Name;
                db.Printers.Add(printer);
                db.SaveChanges();
            }
        }

        public void AddPrinterTask(PrinterTask printerTask)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId  != (int)BranchIdEnum.AllBranchId)
                {
                    printerTask.BranchId = BranchRepository.SelectedBranchId;
                }
                printerTask.DateModified = DateTime.Now;
                printerTask.AuthorModified = UserManager.UserLogged.Name;
                db.PrinterTasks.Add(printerTask);
                db.SaveChanges();
            }
        }

        public void RemovePrinter(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Printers.Remove(
                    db.Printers.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(
                        x => x.ID == id));
                db.SaveChanges();
            }
        }

        public void RemovePrinterTask(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.PrinterTasks.Remove(
                    db.PrinterTasks.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(
                        x => x.ID == id));
                db.SaveChanges();
            }
        }
    }
}
