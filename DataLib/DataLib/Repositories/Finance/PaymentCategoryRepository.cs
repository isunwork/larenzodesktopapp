﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Finance;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Finance
{
    public class PaymentCategoryRepository: IPaymentCategoryRepository
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }

        private PaymentCategoryReferences _paymentCategoryReferences;

        public PaymentCategoryRepository()
        {
            _paymentCategoryReferences = new PaymentCategoryReferences();
        }
        public void CreateTestCategories()
        {
            InsertItem(new PaymentCategory
            {
                Name = "Корневая"
            });

            var parentID = GetItems().Select(x => x.ID).FirstOrDefault();

            InsertItem(new PaymentCategory
            {
                Parent = parentID,
                Name = "Суб категория 1",
            });

            InsertItem(new PaymentCategory
            {
                Parent = parentID,
                Name = "Суб категория 2",
            });
        }
        
        public void RemoveAllWithPayments()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.PaymentCategories.RemoveRange(db.PaymentCategories);
                db.SaveChanges();
            }
        }


        public IEnumerable<PaymentCategory> GetItems()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.PaymentCategories.Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
            }
        }
        
        public void UpdateItem(PaymentCategory entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    entity.BranchId = BranchRepository.SelectedBranchId;
                }
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void DeleteItem(PaymentCategory entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Entry(entity).State = EntityState.Deleted;
                db.SaveChanges();
            }
        }

        public PaymentCategory InsertItem(PaymentCategory entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    entity.BranchId = BranchRepository.SelectedBranchId;
                }
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                db.PaymentCategories.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public List<Payment> GetPayments(int categoryId)
        {

            using (var db = PizzaAppDB.GetNewContext())
            {
                var categories = db.PaymentCategories.Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
                var references = _paymentCategoryReferences.GetCategoryReferences(categoryId, categories);
                return db.Payments.Include("Category").Include("User").Include("Wallet")
                    .Where(x=>x.BranchId == BranchRepository.SelectedBranchId)
                    .Where(x => references.Contains(x.CategoryID)).ToList();
            }
        }
    }
}
