﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Finance;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Others.Transfers;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Finance
{
    public class BankManager: IBankManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public ITransferManager TransferManager { get; set; }
        public void AddBankInteraction(Guid orderGuid, BankInteractionCommand command, string comment, int orderId = 0)
        {
            var interaction = new BankInteraction
            {
                Command = command,
                CommandCreated = DateTime.Now,
                Comment = comment,
                Status = BankInteractionStatus.Created,
                UserID = UserManager.UserLogged != null ? UserManager.UserLogged.ID : 0,
                OrderGuid = orderGuid
            };

            using (var db = PizzaAppDB.GetNewContext())
            {
                var existingEntities = db.BankInteractions.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.OrderGuid == orderGuid).ToList();

                if (existingEntities.Any())
                {
                    // Если информация уже была отправлена
                    if (existingEntities.Any(x => x.Status == BankInteractionStatus.Sent))
                    {
                        UserMessage.Show(@"Информацию о данном заказе уже отправлена, повторной отправки не будет");
                    }
                    else
                    {
                        existingEntities.ForEach(x =>
                        {
                            x.Status = BankInteractionStatus.Overwritten;
                            db.Entry(x).State = EntityState.Modified;
                        });
                    }

                }
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    interaction.BranchId = BranchRepository.SelectedBranchId;
                }
                interaction.DateModified = DateTime.Now;
                interaction.AuthorModified = UserManager.UserLogged.Name;
                db.BankInteractions.Add(interaction);
                db.SaveChanges();
            }
        }


        public async Task<bool> ProcessAll(int brandId, string siteProtocol)
        {
            try
            {
                using (var db = PizzaAppDB.GetNewContext())
                {
                    var list = db.BankInteractions.Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                        .Where(x => x.Status == BankInteractionStatus.Created &&
                                    x.Command != BankInteractionCommand.Hold)
                        .ToList();

                    foreach (var bankInteraction in list)
                    {
                        var response = bankInteraction.Command == BankInteractionCommand.ApproveOrder
                            ? await TransferManager.SendBankCommandApprove(bankInteraction.OrderGuid, brandId, siteProtocol)
                            : TransferManager.SendBankCommandCancel(bankInteraction.OrderGuid, brandId, siteProtocol);

                        bankInteraction.BankResponse = response;
                        bankInteraction.Status = BankInteractionStatus.Sent;
                        bankInteraction.CommandSent = DateTime.Now;
                        bankInteraction.DateModified = DateTime.Now;
                        bankInteraction.AuthorModified = UserManager.UserLogged.Name;
                    }

                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                UserMessage.Show($"Ошибка про формировании банковских операций на сайте: {e.Message} {e.StackTrace}");
                return false;
            }
        }

    }
}
