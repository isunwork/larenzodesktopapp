﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using DataLib.Models;
using EntityFramework.BulkInsert.Extensions;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Entities.Documents.Inventories;
using LaRenzo.DataRepository.Entities.Documents.Sessions;
using LaRenzo.DataRepository.Entities.Documents.WriteoffDish;
using LaRenzo.DataRepository.Entities.Wirings;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Documents.Inventory;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Wiringses.RemainsOfGood
{
    public class RemainsOfGoodWiringsManager: IRemainsOfGoodWiringsManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public IInventoryManager InventoryManager { get; set; }
        public void ProcessSupplyDocument(int documentId, List<RemainsOfGoodWirings> wiringses)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var doc = db.SupplyDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentId);

                var intrmd =
                    db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.IntermediateGoodWiringsId != null && x.Date <= doc.DocumentDate)
                      .OrderByDescending(x => x.Date).FirstOrDefault();

                if (intrmd != null)
                {
                    var intm = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == intrmd.ID);

                    if (intm != null)
                    {
                        var intms = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DateTime >= intm.DateTime).ToList();

                        foreach (var wiringse in intms)
                        {
                            wiringse.IsActual = false;
                        }
                    }
                }

                if (doc != null)
                {
                    doc.IsProcessed = true;
                    doc.IsMarkToDelete = false;

                    foreach (var wirings in wiringses)
                    {
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            wirings.BranchId = BranchRepository.SelectedBranchId;
                        }
                        wirings.DateModified = DateTime.Now;
                        wirings.AuthorModified = UserManager.UserLogged.Name;
                        db.RemainsOfGoodWirings.Add(wirings);
                    }
                }

                db.SaveChanges();
            }
        }

        public void UnProcessSupplyDocument(int documentId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var doc = db.SupplyDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentId);

                var intrmd =
                    db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.IntermediateGoodWiringsId != null && x.Date <= doc.DocumentDate)
                      .OrderByDescending(x => x.Date).FirstOrDefault();

                if (intrmd != null)
                {
                    var intm = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == intrmd.ID);

                    if (intm != null)
                    {
                        var intms = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DateTime >= intm.DateTime).ToList();

                        foreach (var wiringse in intms)
                        {
                            wiringse.IsActual = false;
                        }
                    }
                }
                if (doc != null)
                {
                    doc.IsProcessed = false;

                    var wirings = db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SupplyDocumentID == documentId).ToList();

                    foreach (var wiring in wirings)
                    {
                        db.RemainsOfGoodWirings.Remove(wiring);
                    }
                }

                db.SaveChanges();
            }
        }

        public void UnProcessReturnDocument(int documentId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var doc = db.ReturnDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentId);

                var intrmd =
                    db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.IntermediateGoodWiringsId != null && x.Date <= doc.DocumentDate)
                      .OrderByDescending(x => x.Date).FirstOrDefault();

                if (intrmd != null)
                {
                    var intm = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == intrmd.ID);

                    if (intm != null)
                    {
                        var intms = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DateTime >= intm.DateTime).ToList();

                        foreach (var wiringse in intms)
                        {
                            wiringse.IsActual = false;
                        }
                    }
                }

                if (doc != null)
                {
                    doc.IsProcessed = false;

                    var wirings = db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.ReturnDocumentID == documentId).ToList();

                    foreach (var wiring in wirings)
                    {
                        db.RemainsOfGoodWirings.Remove(wiring);
                    }
                }

                db.SaveChanges();
            }
        }

        public void ProcessReturnDocument(int documentId, List<RemainsOfGoodWirings> wiringses)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var doc = db.ReturnDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentId);

                var intrmd =
                    db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.IntermediateGoodWiringsId != null && x.Date <= doc.DocumentDate)
                      .OrderByDescending(x => x.Date).FirstOrDefault();

                if (intrmd != null)
                {
                    var intm = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == intrmd.ID);

                    if (intm != null)
                    {
                        var intms = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DateTime >= intm.DateTime).ToList();

                        foreach (var wiringse in intms)
                        {
                            wiringse.IsActual = false;
                        }
                    }
                }

                if (doc != null)
                {
                    doc.IsProcessed = true;
                    doc.IsMarkToDelete = false;

                    foreach (var wirings in wiringses)
                    {
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            wirings.BranchId = BranchRepository.SelectedBranchId;
                        }

                        wirings.DateModified = DateTime.Now;
                        wirings.AuthorModified = UserManager.UserLogged.Name;
                        db.RemainsOfGoodWirings.Add(wirings);
                    }
                }

                db.SaveChanges();
            }
        }

        public void ProcessMovingDocument(int documentId, List<RemainsOfGoodWirings> wiringses,
                                                 Warehouse toWarehouse)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var doc = db.MovingDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentId);

                var intrmd =
                    db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.IntermediateGoodWiringsId != null && x.Date <= doc.DocumentDate)
                      .OrderByDescending(x => x.Date).FirstOrDefault();
                if (intrmd != null)
                {
                    var intm = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == intrmd.ID);

                    if (intm != null)
                    {
                        var intms = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DateTime >= intm.DateTime).ToList();

                        foreach (var wiringse in intms)
                        {
                            wiringse.IsActual = false;
                        }
                    }
                }

                if (doc != null)
                {
                    doc.IsProcessed = true;
                    doc.IsMarkToDelete = false;

                    foreach (var wirings in wiringses)
                    {
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            wirings.BranchId = BranchRepository.SelectedBranchId;
                        }

                        wirings.DateModified = DateTime.Now;
                        wirings.AuthorModified = UserManager.UserLogged.Name;
                        db.RemainsOfGoodWirings.Add(wirings);
                    }

                    foreach (var wiring in wiringses.Select(wirings => new RemainsOfGoodWirings
                    {
                        Date = wirings.Date,
                        Amount = -wirings.Amount,
                        MovingDocumentID = wirings.MovingDocumentID,
                        ProductID = wirings.ProductID,
                        WarehouseID = toWarehouse.ID
                    }))
                    {
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            wiring.BranchId = BranchRepository.SelectedBranchId;
                        }

                        wiring.DateModified = DateTime.Now;
                        wiring.AuthorModified = UserManager.UserLogged.Name;
                        db.RemainsOfGoodWirings.Add(wiring);
                    }
                }

                db.SaveChanges();
            }
        }

        public void UnProcessMovingDocument(int documentId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var doc = db.MovingDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentId);

                var intrmd =
                    db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.IntermediateGoodWiringsId != null && x.Date <= doc.DocumentDate)
                      .OrderByDescending(x => x.Date).FirstOrDefault();
                if (intrmd != null)
                {
                    var intm = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == intrmd.ID);

                    if (intm != null)
                    {
                        var intms = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DateTime >= intm.DateTime).ToList();

                        foreach (var wiringse in intms)
                        {
                            wiringse.IsActual = false;
                        }
                    }
                }

                if (doc != null)
                {
                    doc.IsProcessed = false;

                    var wirings = db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.MovingDocumentID == documentId).ToList();

                    foreach (var wiring in wirings)
                    {
                        db.RemainsOfGoodWirings.Remove(wiring);
                    }
                }

                db.SaveChanges();
            }
        }

        public void ProcessWriteoffDocument(int documentId, List<RemainsOfGoodWirings> wiringses)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var doc = db.WriteoffDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentId);

                var intrmd =
                    db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.IntermediateGoodWiringsId != null && x.Date <= doc.DocumentDate)
                      .OrderByDescending(x => x.Date).FirstOrDefault();

                if (intrmd != null)
                {
                    var intm = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == intrmd.ID);

                    if (intm != null)
                    {
                        var intms = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DateTime >= intm.DateTime).ToList();

                        foreach (var wiringse in intms)
                        {
                            wiringse.IsActual = false;
                        }
                    }
                }

                if (doc != null)
                {
                    doc.IsProcessed = true;
                    doc.IsMarkToDelete = false;

                    foreach (var wirings in wiringses)
                    {
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            wirings.BranchId = BranchRepository.SelectedBranchId;
                        }

                        wirings.DateModified = DateTime.Now;
                        wirings.AuthorModified = UserManager.UserLogged.Name;
                        db.RemainsOfGoodWirings.Add(wirings);
                    }
                }

                db.SaveChanges();
            }
        }

        public void UnProcessWriteoffDocument(int documentId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var doc = db.WriteoffDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentId);

                var intrmd =
                    db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.IntermediateGoodWiringsId != null && x.Date <= doc.DocumentDate)
                      .OrderByDescending(x => x.Date).FirstOrDefault();

                if (intrmd != null)
                {
                    var intm = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == intrmd.ID);

                    if (intm != null)
                    {
                        var intms = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DateTime >= intm.DateTime).ToList();

                        foreach (var wiringse in intms)
                        {
                            wiringse.IsActual = false;
                        }
                    }
                }

                if (doc != null)
                {
                    doc.IsProcessed = false;

                    var wirings = db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.WriteoffDocumentID == documentId).ToList();

                    foreach (var wiring in wirings)
                    {
                        db.RemainsOfGoodWirings.Remove(wiring);
                    }
                }

                db.SaveChanges();
            }
        }

        public void ProcessPostingDocument(int documentId, List<RemainsOfGoodWirings> wiringses)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var doc = db.PostingDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentId);

                var intrmd =
                    db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.IntermediateGoodWiringsId != null && x.Date <= doc.DocumentDate)
                      .OrderByDescending(x => x.Date).FirstOrDefault();
                if (intrmd != null)
                {
                    var intm = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == intrmd.ID);

                    if (intm != null)
                    {
                        var intms = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DateTime >= intm.DateTime).ToList();

                        foreach (var wiringse in intms)
                        {
                            wiringse.IsActual = false;
                        }
                    }
                }

                if (doc != null)
                {
                    doc.IsProcessed = true;
                    doc.IsMarkToDelete = false;

                    foreach (var wirings in wiringses)
                    {
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            wirings.BranchId = BranchRepository.SelectedBranchId;
                        }

                        wirings.DateModified = DateTime.Now;
                        wirings.AuthorModified = UserManager.UserLogged.Name;
                        db.RemainsOfGoodWirings.Add(wirings);
                    }
                }

                db.SaveChanges();
            }
        }

        public void UnProcessPostingDocument(int documentId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var doc = db.PostingDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentId);

                var intrmd =
                    db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.IntermediateGoodWiringsId != null && x.Date <= doc.DocumentDate)
                      .OrderByDescending(x => x.Date).FirstOrDefault();
                if (intrmd != null)
                {
                    var intm = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == intrmd.ID);

                    if (intm != null)
                    {
                        var intms = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DateTime >= intm.DateTime).ToList();

                        foreach (var wiringse in intms)
                        {
                            wiringse.IsActual = false;
                        }
                    }
                }

                if (doc != null)
                {
                    doc.IsProcessed = false;

                    var wirings = db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.PostingDocumentID == documentId).ToList();

                    foreach (var wiring in wirings)
                    {
                        db.RemainsOfGoodWirings.Remove(wiring);
                    }
                }

                db.SaveChanges();
            }
        }

        public void UnProcessSessionDocument(int documentId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    var doc = db.Sessions.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentId);

                    if (doc != null)
                    {
                        doc.IsProcessed = false;


                        var allWarehouseIds =
                            db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SessionDocumentID == documentId)
                                .Select(x => x.WarehouseID)
                                .Distinct()
                                .ToList();

                        allWarehouseIds.ForEach(x => InventoryManager.MarkInventoryDocumentRecalculate(x, doc.Closed.Value));

                        // Прямой запрос для ускорение отмены проводки
                        db.Database.ExecuteSqlCommand("DELETE FROM RemainsOfGoodWirings WHERE SessionDocumentID = @id", new SqlParameter("id", documentId));

                    }

                    db.SaveChanges();
                    transaction.Commit();
                }
            }
        }

        public void ProcessSessionDocument(int documentId, List<RemainsOfGoodWirings> wiringses)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var doc = db.Sessions.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentId);

                var intrmd =
                    db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.IntermediateGoodWiringsId != null && x.Date <= doc.Closed)
                      .OrderByDescending(x => x.Date).FirstOrDefault();
                if (intrmd != null)
                {
                    var intm = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == intrmd.ID);

                    if (intm != null)
                    {
                        var intms = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DateTime >= intm.DateTime).ToList();

                        foreach (var wiringse in intms)
                        {
                            wiringse.IsActual = false;
                        }
                    }
                }
                if (doc != null)
                {
                    doc.IsProcessed = true;
                    doc.IsMarkToDelete = false;

                    foreach (var wirings in wiringses)
                    {
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            wirings.BranchId = BranchRepository.SelectedBranchId;
                        }

                        wirings.DateModified = DateTime.Now;
                        wirings.AuthorModified = UserManager.UserLogged.Name;
                        db.RemainsOfGoodWirings.Add(wirings);
                    }
                }

                db.SaveChanges();
            }
        }

        public void UnProcessInventoryDocument(int documentId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var doc = db.InventoryDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentId);

                if (doc != null)
                {
                    doc.IsProcessed = false;

                    var allWarehouseIds =
                            db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.InventoryDocumentID == documentId)
                                .Select(x => x.WarehouseID)
                                .Distinct()
                                .ToList();

                    allWarehouseIds.ForEach(x => InventoryManager.MarkInventoryDocumentRecalculate(x, doc.DocumentDate));

                    db.Database.ExecuteSqlCommand("DELETE FROM RemainsOfGoodWirings WHERE InventoryDocumentID = @id", new SqlParameter("id", documentId));
                    db.SaveChanges();
                }
            }
        }

        public void ProcessInventoryDocument(int documentId, List<RemainsOfGoodWirings> wiringses)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var doc = db.InventoryDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentId);

                if (doc != null)
                {
                    doc.IsProcessed = true;
                    doc.IsMarkToDelete = false;
                    doc.NeedToRecalculate = false;
                    foreach (var wiringse in wiringses)
                    {
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            wiringse.BranchId = BranchRepository.SelectedBranchId;
                        }

                        wiringse.DateModified = DateTime.Now;
                        wiringse.AuthorModified = UserManager.UserLogged.Name;
                    }
                    db.BulkInsert(wiringses);
                    wiringses.Select(x => x.WarehouseID).Distinct().ToList().ForEach(x => InventoryManager.MarkInventoryDocumentRecalculate(x, doc.DocumentDate));

                    db.SaveChanges();
                }

            }
        }
        
        public void UnProcessWriteoffDishDocument(int documentId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var doc = db.WriteoffDishDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentId);

                var intrmd =
                    db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.IntermediateGoodWiringsId != null && x.Date <= doc.DocumentDate)
                      .OrderByDescending(x => x.Date).FirstOrDefault();
                if (intrmd != null)
                {
                    var intm = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == intrmd.ID);

                    if (intm != null)
                    {
                        var intms = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DateTime >= intm.DateTime).ToList();

                        foreach (var wiringse in intms)
                        {
                            wiringse.IsActual = false;
                        }
                    }
                }
                if (doc != null)
                {
                    doc.IsProcessed = false;

                    var wirings = db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.WriteoffDishDocumentID == documentId).ToList();

                    foreach (var wiring in wirings)
                    {
                        db.RemainsOfGoodWirings.Remove(wiring);
                    }
                }

                db.SaveChanges();
            }
        }

        public void ProcessWriteoffDishDocument(int documentId, List<RemainsOfGoodWirings> wiringses)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var doc = db.WriteoffDishDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentId);

                var intrmd =
                    db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.IntermediateGoodWiringsId != null && x.Date <= doc.DocumentDate)
                      .OrderByDescending(x => x.Date).FirstOrDefault();
                if (intrmd != null)
                {
                    var intm = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == intrmd.ID);

                    if (intm != null)
                    {
                        var intms = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DateTime >= intm.DateTime).ToList();

                        foreach (var wiringse in intms)
                        {
                            wiringse.IsActual = false;
                        }
                    }
                }
                if (doc != null)
                {
                    doc.IsProcessed = true;
                    doc.IsMarkToDelete = false;

                    foreach (var wirings in wiringses)
                    {
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            wirings.BranchId = BranchRepository.SelectedBranchId;
                        }
                        wirings.DateModified = DateTime.Now;
                        wirings.AuthorModified = UserManager.UserLogged.Name;
                        db.RemainsOfGoodWirings.Add(wirings);
                    }
                }

                db.SaveChanges();
            }
        }

        public void ProcessWriteoffDishDocument(int documentId, List<WriteoffDishDocumentItem> documentItems)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var doc = db.WriteoffDishDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentId);

                var intrmd =
                    db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.IntermediateGoodWiringsId != null && x.Date <= doc.DocumentDate)
                      .OrderByDescending(x => x.Date).FirstOrDefault();
                if (intrmd != null)
                {
                    var intm = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == intrmd.ID);

                    if (intm != null)
                    {
                        var intms = db.IntermediateGoodWiringses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DateTime >= intm.DateTime).ToList();

                        foreach (var wiringse in intms)
                        {
                            wiringse.IsActual = false;
                        }
                    }
                }
                if (doc != null)
                {
                    doc.IsProcessed = true;
                    doc.IsMarkToDelete = false;

                    foreach (var item in documentItems)
                    {
                        var productList =
                            db.ItemOfDishSets.Include("Product").Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DishId == item.DishID).ToList();

                        foreach (var ofDishSet in productList)
                        {
                            if (ofDishSet.Product.IsSetProduct)
                            {
                                var set = ofDishSet;
                                var items =
                                    db.ItemOfProductSets.Include("Product")
                                    .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                                      .Where(x => x.ParentProductId == set.ProductId)
                                      .ToList();

                                foreach (var ofProductSet in items)
                                {
                                    var wiring = new RemainsOfGoodWirings
                                    {
                                        WarehouseID = doc.WarehouseID,
                                        WriteoffDishDocumentID = doc.ID,
                                        Date = doc.DocumentDate,
                                        ProductID = ofProductSet.ProductId,
                                        Amount = -(ofProductSet.Amount * item.Amount * ofDishSet.Amount)
                                    };
                                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                                    {
                                        wiring.BranchId = BranchRepository.SelectedBranchId;
                                    }
                                    wiring.DateModified = DateTime.Now;
                                    wiring.AuthorModified = UserManager.UserLogged.Name;
                                    db.RemainsOfGoodWirings.Add(wiring);
                                }
                            }
                            else
                            {
                                var wiring = new RemainsOfGoodWirings
                                {
                                    WarehouseID = doc.WarehouseID,
                                    WriteoffDishDocumentID = doc.ID,
                                    Date = doc.DocumentDate,
                                    ProductID = ofDishSet.ProductId,
                                    Amount = -(ofDishSet.Amount * item.Amount)
                                };
                                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                                {
                                    wiring.BranchId = BranchRepository.SelectedBranchId;
                                }
                                wiring.DateModified = DateTime.Now;
                                wiring.AuthorModified = UserManager.UserLogged.Name;
                                db.RemainsOfGoodWirings.Add(wiring);
                            }
                        }
                    }
                }

                db.SaveChanges();
            }
        }

        public List<RemainsOfGoodWirings> GroupByProductAndWarehouse(List<RemainsOfGoodWirings> list)
        {
            var remainsGrouped = new List<RemainsOfGoodWirings>();

            list.GroupBy(x => x.WarehouseID).Select(x => new
            {
                Remains = x.GroupBy(a => a.ProductID).Select(a => new RemainsOfGoodWirings
                {
                    ProductID = a.Key,
                    Amount = a.Sum(b => b.Amount),
                    WarehouseID = x.Key,
                })
            }).Select(x => x.Remains).ToList().ForEach(remainsGrouped.AddRange);

            return remainsGrouped;
        }

        /// <summary>
        /// Возвращает остатки на заданном складе на указанную дату. Проверяет, есть ли некорректные инвентаризации до этой даты
        /// </summary>
        /// <param name="warehouseId"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public RemainsOfGoodWiringsOnDate GetRemainsOnDate(int warehouseId, DateTime date)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var remainList = db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.WarehouseID == warehouseId && x.Date <= date)
                    .GroupBy(x => x.ProductID)
                    .Select(x => new ProductRemain()
                    {
                        ProductID = x.Key,
                        Amount = x.Sum(a => a.Amount)
                    }).ToList();

                var hasBrokenInvent = db.InventoryDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Any(x => x.DocumentDate <= date && x.NeedToRecalculate);

                return new RemainsOfGoodWiringsOnDate
                {
                    DateTime = date,
                    WarehouseID = warehouseId,
                    ProductRemains = remainList,
                    HasBrokenInventarisationsDueDate = hasBrokenInvent
                };
            }
        }

        public RemainsOfGoodWiringsOnDate GetRemainsOnDate(DateTime date)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var remainList = db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.Date <= date)
                    .GroupBy(x => x.ProductID)
                    .Select(x => new ProductRemain()
                    {
                        ProductID = x.Key,
                        Amount = x.Sum(a => a.Amount)
                    }).ToList();

                var hasBrokenInvent = db.InventoryDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Any(x => x.DocumentDate <= date && x.NeedToRecalculate);

                return new RemainsOfGoodWiringsOnDate
                {
                    DateTime = date,
                    WarehouseID = -1,
                    ProductRemains = remainList,
                    HasBrokenInventarisationsDueDate = hasBrokenInvent
                };
            }
        }
    }

    

    
}