﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Calls;
using LaRenzo.DataRepository.Entities.Documents.Sessions;
using LaRenzo.DataRepository.Repositories.Documents.Sessions;
using LaRenzo.DataRepository.Repositories.Settings;
using NLog;

namespace LaRenzo.DataRepository.Repositories.Call
{
    public class CallRepository: ICallRepository
    {
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public ISessionManager SessionManager { get; set; }

        /// <summary>
        ///  Название таблицы
        /// </summary>
        private string _tableName;

        /// <summary>
        /// Обновление
        /// </summary>
        /// <param name="item"></param>
        private void Update(object item)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Entry(item).State = EntityState.Modified;
                SetUnchangedInternalObjects(db, item);
                db.SaveChanges();
            }
        }



        /// <summary>
        /// Удалить элемент
        /// </summary>
        /// <param name="id"></param>
        public void Remove(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                try
                {
                    db.Database.ExecuteSqlCommand("delete from " + _tableName + " where id = " + id.ToString());
                }
                catch (Exception ex)
                {
                    UserMessage.Show($"Ошибка удаления элемента {typeof(CallData).Name} с идентификатором {id}.", ex);
                }

            }
        }


        /// <summary>
        /// Установить не изменяемые внутренние объекты
        /// </summary>
        /// <param name="db"></param>
        /// <param name="obj"></param>
         //_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        public void SetUnchangedInternalObjects(PizzaAppDB db, object obj)
        {
            var item = (CallData)obj;


            if (item.Session != null)
            {
                db.Entry(item.Session).State = EntityState.Unchanged;
            }

            if (item.User != null)
            {
                db.Entry(item.User).State = EntityState.Unchanged;

                if (item.User.Role != null)
                {
                    db.Entry(item.User.Role).State = EntityState.Unchanged;
                }
            }
        }

        /// <summary>
        /// Сохранить элемент
        /// </summary>
        /// <param name="item"></param>
        public void Save(CallData item)
        {
            if (item.ID > 0)
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    item.BranchId = BranchRepository.SelectedBranchId;
                }
                Update(item);
            }
            else
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    item.BranchId = BranchRepository.SelectedBranchId;
                }
                Add(item);
            }
        }


        /// <summary>
        /// Вернуть состояния для сохранения объекта
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private EntityState GetStateSaveSubItem(CallData item)
        {
            var state = EntityState.Unchanged;

            if (item.ID == 0)
            {
                state = EntityState.Added;
            }

            return state;
        }

        /// <summary> Список позвонивших </summary>
        public async Task<List<CallData>> ListCalls()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                Session curentSession = await SessionManager.GetOpened();

                if (curentSession != null)
                {
                    try
                    {
                        var callData = await db.CallData.Include(x => x.User).Where(x => x.Session.ID == curentSession.ID && x.BranchId == BranchRepository.SelectedBranchId).OrderBy(x => x.Created).ToListAsync();
                        return callData;
                    }
                    catch (Exception e)
                    {
                        var logger = LogManager.GetCurrentClassLogger();
                        logger.Error(e);
                    }
                }
            }

            return null;
        }
        //_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Добавить в БД  </summary>
        ////===================================
        public void Add(CallData item)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Configuration.AutoDetectChangesEnabled = false; // Увеличение производительности
                SetUnchangedInternalObjects(db, item);
                db.Configuration.AutoDetectChangesEnabled = true;
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    item.BranchId = BranchRepository.SelectedBranchId;
                }
                db.CallData.Add(item);
                db.SaveChanges();
            }
        }
    }
}
