﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Documents.WebOrders;
using LaRenzo.DataRepository.Entities.Finance;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Finance;
using LaRenzo.DataRepository.Repositories.Others.Transfers;
using LaRenzo.DataRepository.Repositories.Settings;
using LaRenzo.TransferLibrary;

namespace LaRenzo.DataRepository.Repositories.Documents.WebOrders
{
    
    public class WebOrderManager: IWebOrderManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public IBrandManager BrandManager { get; set; }
        [Inject]
        public ITransferManager TransferManager { get; set; }
        [Inject]
        public IBankManager BankManager { get; set; }
        public async Task<GrabOrdersResult> GrabOrders(string site, int brandId, string siteProtocol, int selectedBranchId)
        {
            var result = new GrabOrdersResult();
            if (BrandManager.SelectedBrand != null)
            {
                Brand brand = BrandManager.GetById(brandId, false);
                var orderInfos = await TransferManager.GetUnhandledWebOrders(site, siteProtocol, brandId);
                List<WebOrder> incomingWebOrders = OrderInfoToWebOrder(orderInfos, brand);
                result.WebOrderCount = 0;
                if (incomingWebOrders == null) return result;
                result.WebOrderCount = incomingWebOrders.Count;

                using (PizzaAppDB db = PizzaAppDB.GetNewContext())
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions{IsolationLevel = IsolationLevel.ReadCommitted}))
                    {

                        IEnumerable<Guid> newGuidList = incomingWebOrders.Select(g => g.Guid);

                        List<WebOrder> alreadyExistsOrders = db.WebOrders.Include(x => x.Brand)
                            .Where(x => x.BranchId == selectedBranchId).Where(x => newGuidList.Contains(x.Guid))
                            .ToList();

                        result.ExistCount = alreadyExistsOrders.Count;
                        // Выбор тех, что не обрабатывались
                        foreach (var order in alreadyExistsOrders)
                        {
                            var incomingData = incomingWebOrders.FirstOrDefault(x =>
                                x.Guid == order.Guid && x.WebOrderId == order.WebOrderId);
                            if (incomingData == null) continue;

                            if (order.PaymentMethod != incomingData.PaymentMethod)
                            {
                                order.PaymentMethod = incomingData.PaymentMethod;
                            }

                            if (order.IsPaid != incomingData.IsPaid)
                            {
                                order.IsPaid = incomingData.IsPaid;
                            }

                            order.DateModified = DateTime.Now;
                            order.AuthorModified = "WebOrderProcessProgramm";
                        }

                        db.SaveChanges();

                        List<Guid> alreadyExistGuids = alreadyExistsOrders.Select(x => x.Guid).ToList();

                        List<Guid> paidOrdersGuid = incomingWebOrders.Where(x => x.IsPaid).Select(x => x.Guid).ToList();
                        incomingWebOrders = incomingWebOrders.Where(x => !alreadyExistGuids.Contains(x.Guid)).ToList();

                        result.NewCount = incomingWebOrders.Count;
                        List<WebOrder> paymentsToUpdate = db.WebOrders.Include(x => x.Brand)
                            .Where(x => x.BranchId == selectedBranchId)
                            .Where(x => !x.IsPaid && paidOrdersGuid.Contains(x.Guid)).ToList();

                        paymentsToUpdate.ForEach(x => x.IsPaid = true);

                        db.SaveChanges();
                        scope.Complete();
                    }
                }
                AddMultiple(incomingWebOrders, selectedBranchId);
                return result;
            }
            return result;
        }

        public void ChangePaymentMethod(int id, PaymentMethod method)
        {
            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                WebOrder entity = db.WebOrders.Include(x => x.Brand).Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == id);
                if (entity == null)
                {
                    throw new ArgumentException("Онлайн заказ с таким ID не найден");
                }

                if (entity.PaymentMethod == PaymentMethod.OnlineCard && method != PaymentMethod.OnlineCard)
                {
                    BankManager.AddBankInteraction(entity.Guid, BankInteractionCommand.CancelOrder, "В окне интернет заказа сменен тип оплаты с онлайн платежа на другой метод");
                }
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                entity.PaymentMethod = method;
                db.SaveChanges();
            }
        }


        public List<WebOrder> GetUnhandledWebOrders(bool onlyNewWebOrders)
        {
            bool isAdmin = UserManager.IsAdmin();
            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                var expired = DateTime.Now.AddHours(-10);
                return db.WebOrders.Include("Items")
                    .Include(x => x.Brand)
                    .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                    .Where(x => x.Status == WebOrderStatus.New || (!onlyNewWebOrders && isAdmin && x.Status == WebOrderStatus.Processing && x.CreateDate.CompareTo(expired) > 0))
                    .ToList().GroupBy(x => x.Guid)
                    .Select(x => x.FirstOrDefault()).ToList();
            }
        }

        public int GetUnhandledWebOrdersCount()
        {
            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                return db.WebOrders
                    .GroupBy(x => x.Guid)
                    .Select(x => x.FirstOrDefault())
                    .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                    .Count(x => x.Status == WebOrderStatus.New);
            }
        }

        public void AddMultiple(List<WebOrder> list, int selectedBranchId)
        {
            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions{IsolationLevel = IsolationLevel.ReadCommitted}))
                {
                    foreach (WebOrder order in list.GroupBy(x => x.Guid).Select(x => x.FirstOrDefault()))
                    {
                        if (order != null)
                        {
                            var webOrder = db.WebOrders.FirstOrDefault(x =>
                                x.Guid == order.Guid && x.WebOrderId == order.WebOrderId);
                            if (webOrder == null)
                            {
                                order.BranchId = selectedBranchId;
                                order.DateModified = DateTime.Now;
                                order.AuthorModified = "WebOrderProcessProgramm";
                                db.Entry(order.Brand).State = EntityState.Unchanged;
                                db.WebOrders.Add(order);
                            }
                        }
                    }
                    db.SaveChanges();
                    scope.Complete();
                }
            }
        }

        public void SetOrderStatus(int id, string status)
        {
            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                WebOrder order = db.WebOrders.Include(x => x.Brand)
                    .Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == id);
                if (order != null)
                {
                    order.DateModified = DateTime.Now;
                    order.AuthorModified = UserManager.UserLogged.Name;
                    order.Status = status;
                    db.SaveChanges();
                }
            }
        }

        private List<WebOrder> OrderInfoToWebOrder(List<OrderInfo> info, Brand brand)
        {
            if (info == null) return null;
            return info.Select(orderInfo => {
                return new WebOrder
                {
                    Address = orderInfo.Address,
                    Source = orderInfo.Source,
                    Comment = orderInfo.Comment,
                    WebOrderId = orderInfo.WebOrderId,
                    Phone = orderInfo.Phone,
                    Guid = orderInfo.Guid,
                    IsCallback = orderInfo.IsCallback,
                    DiscountCard = orderInfo.DiscountCard,
                    Name = orderInfo.Name,
                    Status = "New",
                    Time = orderInfo.Time,
                    BrandId = orderInfo.BrandId,
                    IsNewSiteOrder = orderInfo.IsNewSiteOrder,
                    Items = orderInfo.Items.Select(x =>
                    {
                        return new WebOrderItem
                        {
                            Amount = x.Amount,
                            Name = x.Name,
                            SKU = x.SKU.HasValue ? x.SKU.Value : -1,
                            BranchId = BranchRepository.SelectedBranchId
                        };
                    }).ToList(),
                    IsPaid = orderInfo.IsPaid,
                    PaymentMethod = orderInfo.PaymentMethod,
                    DeliveryMethod = orderInfo.DeliveryMethod,
                    CreateDate = DateTime.Now,
                    Brand = brand,
                    BranchId = BranchRepository.SelectedBranchId,
                    CouponCode = orderInfo.CouponCode
                };
            }).ToList();
        }


        public string CheckIfOrderCanBeProcessed(WebOrder order, int brandId, string siteProtocol)
        {
            string orderProcessedState = string.Empty;

            WebOrder orderInActualState = GetById(order.ID);

            if (orderInActualState.Status == WebOrderStatus.Processed)
            {
                orderProcessedState = "Интренет заказ уже обработан";
            }

            if (orderInActualState.Status == WebOrderStatus.Processing)
            {
                orderProcessedState = "Заказ обрабатывается в данный момент";
            }

            return orderProcessedState;
        }


        public WebOrder GetById(int id)
        {
            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                return db.WebOrders.Include(x => x.Brand).Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == id);
            }
        }


        public async Task SetOrderProcessed(WebOrder order, int brandId, string siteProtocol)
        {
            if (order != null)
            {
                SetOrderStatus(order.ID, WebOrderStatus.Processed);
                var result = await TransferManager.MarkOrderProccesedOnWebSite(order, brandId, siteProtocol);
                if (!result)
                {
                    UserMessage.Show("Проблемы с сайтом");
                }
            }
        }

        public void StartProcessing(WebOrder order)
        {
            if (order != null)
            {
                SetOrderStatus(order.ID, WebOrderStatus.Processing);
            }
        }

        public void StopProcessing(WebOrder order)
        {
            if (order != null)
            {
                WebOrder actualOrder = GetById(order.ID);
                if (actualOrder.Status == WebOrderStatus.Processing)
                {
                    SetOrderStatus(order.ID, WebOrderStatus.New);
                }
            }
        }

        

        public string ToString(WebOrder order)
        {
            var result = new StringBuilder();

            if (order == null) result.Append("Дополнительных данных нет");
            else
            {
                if (order.IsCallback)
                {
                    result.Append("ЗАКАЗАЛИ ОБРАТНЫЙ ЗВОНОК");
                    result.Append(Environment.NewLine);
                    result.Append(Environment.NewLine);
                }
                if (order.IdBrand == 1)
                {
                    result.Append(string.Format("Заказ сделан с сайта {0}", order.IsNewSiteOrder == 1
                        ? "http://larenzo.ru/"
                        : order.IsNewSiteOrder == 2
                            ? "http://new.larenzo.ru/"
                            : "http://www.pizzalarenzo.ru/"));
                }
                else if (order.IdBrand == 2)
                {
                    result.Append("Заказ сделан с сайта http://pizzarambo.ru/");
                }
                result.Append(Environment.NewLine);
                result.Append(@"Имя: ");
                result.Append(order.Name);
                result.Append(Environment.NewLine);

                result.Append(@"Телефон:");
                result.Append(order.Phone);
                result.Append(Environment.NewLine);

                result.Append(@"Адрес: ");
                result.Append(order.Address);
                result.Append(Environment.NewLine);

                result.Append(@"Приготовить заказ к: ");
                result.Append(order.Time);
                result.Append(Environment.NewLine);
                result.Append(Environment.NewLine);
                result.Append("*********************");
                result.Append(Environment.NewLine);

                foreach (WebOrderItem item in order.Items)
                {
                    result.Append(item.Name);
                    result.Append(" > ");
                    result.Append(item.Amount);
                    result.Append("шт");
                    result.Append(Environment.NewLine);
                }
            }

            return result.ToString();
        }
    }
}
