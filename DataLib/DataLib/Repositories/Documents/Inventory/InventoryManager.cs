﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using DataLib.Models;
using EntityFramework.BulkInsert.Extensions;
using LaRenzo.DataRepository.Entities.Documents.Inventories;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;
using LaRenzo.DataRepository.Repositories.Wiringses.RemainsOfGood;
using LaRenzo.Interfaces;
using LaRenzo.Interfaces.SignalMessages;

namespace LaRenzo.DataRepository.Repositories.Documents.Inventory
{
    /// <summary>
    /// Отвечает за работу с инвентаризациями
    /// </summary>
    public class InventoryManager: IInventoryManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public ISignalProcessor SignalProcessor { get; set; }
        [Inject]
        public IRemainsOfGoodWiringsManager RemainsOfGoodWiringsManager { get; set; }

        public List<InventoryDocumentItem> GetInventoryDocumentItems(int inventoryId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var listItems =
                    db.InventoryDocumentItems.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Include("Product")
                      .Include("Product.Measure")
                      .Where(x => x.InventoryDocumentID == inventoryId)
                      .ToList();

                foreach (var item in listItems.Where(item => item.Product != null))
                {
                    item.Price = item.Product.FixPrice;
                    item.DeltaFin = Math.Round(item.Delta * (decimal)item.Product.FixPrice, 2);
                    item.AmmountFactFin = Math.Round(item.AmountFact * (decimal)item.Product.FixPrice, 2); ;
                }

                return listItems;
            }
        }

        public object GetInventoryDocuments()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.InventoryDocuments.Include("Warehouse").Where(x => x.BranchId == BranchRepository.SelectedBranchId).OrderBy(x => x.ID).ToList();
            }
        }

        public InventoryDocument GetInventoryDocument(int inventoryId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.InventoryDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == inventoryId);
            }
        }

        public void UpdateInventoryDocument(InventoryData inventoryData)
        {
            InventoryDocument document = inventoryData.Document;
            List<InventoryDocumentItem> documentItems = inventoryData.DocumentItems;
            using (var db = PizzaAppDB.GetNewContext())
            {
                documentItems.ForEach(x =>
                    {
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            x.BranchId = BranchRepository.SelectedBranchId;
                        }
                        x.DateModified = DateTime.Now;
                        x.AuthorModified = UserManager.UserLogged.Name;
                        x.InventoryDocumentID = document.ID;
                        x.ID = 0;
                    }
                );

                db.Database.ExecuteSqlCommand(
                    $"DELETE FROM InventoryDocumentItems WHERE InventoryDocumentID = @id and BranchId = {BranchRepository.SelectedBranchId}",
                    new SqlParameter("id", document.ID));

                db.BulkInsert(documentItems);
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    document.BranchId = BranchRepository.SelectedBranchId;
                }
                document.DateModified = DateTime.Now;
                document.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(document).State = EntityState.Modified;

                db.SaveChanges();
            }
        }

        public int AddInventoryDocument(InventoryDocument document)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    document.BranchId = BranchRepository.SelectedBranchId;
                }
                document.DateModified = DateTime.Now;
                document.AuthorModified = UserManager.UserLogged.Name;
                db.InventoryDocuments.Add(document);

                db.SaveChanges();

                return document.ID;
            }
        }

        public void DeleteInventoryDocument(InventoryDocument document)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (!document.IsMarkToDelete)
                {
                    document.IsProcessed = false;

                    var wirings = db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.InventoryDocumentID == document.ID).ToList();

                    foreach (var wiring in wirings)
                    {
                        db.RemainsOfGoodWirings.Remove(wiring);
                    }
                }

                document.IsMarkToDelete = !document.IsMarkToDelete;

                db.Entry(document).State = EntityState.Modified;

                db.SaveChanges();
            }
        }

        public List<InventoryDocumentItem> FillInventoryItems(int warehouseId, DateTime docDate)

        {
            var inventoryItems = new List<InventoryDocumentItem>();

            using (var db = PizzaAppDB.GetNewContext())
            {
                var products = db.Products.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Include("Measure").ToList();

                var totalAmounts = RemainsOfGoodWiringsManager.GetRemainsOnDate(warehouseId, docDate);

                foreach (var product in products)
                {
                    var info = totalAmounts.ProductRemains.FirstOrDefault(x => x.ProductID == product.ID);
                    if (info == null) continue;

                    var item = new InventoryDocumentItem
                    {
                        ID = -1,
                        ProductID = product.ID,
                        Product = product,
                        Amount = info.Amount,
                        AmountFact = 0,
                        Delta = -info.Amount,
                        DeltaFin = -info.Amount * (decimal)product.FixPrice,
                        AmmountFactFin = 0 * (decimal)product.FixPrice
                    };
                    inventoryItems.Add(item);
                }
            }
            return inventoryItems;
        }

        /// <summary>
        /// Находит инвентаризацию на складе после указанной даты и помечает ее, как требующую перепроведения.
        /// </summary>
        /// <param name="warehouseId">ID склада</param>
        /// <param name="changesDate"></param>
        /// <param name="reasonForNotify"></param>
        /// <param name="notifyAction"></param>
        /// <returns>Документ, который нужно перепровести, либо null, если такой документ не найден</returns>
        public InventoryDocument MarkInventoryDocumentRecalculate(int warehouseId, DateTime changesDate, string reasonForNotify = null)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var inventoryDocument = db.InventoryDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).OrderBy(x => x.DocumentDate).
                    FirstOrDefault(x => x.WarehouseID == warehouseId && x.DocumentDate > changesDate);

                if (inventoryDocument == null || inventoryDocument.NeedToRecalculate == true) return null;
                inventoryDocument.DateModified = DateTime.Now;
                inventoryDocument.AuthorModified = UserManager.UserLogged.Name;
                inventoryDocument.NeedToRecalculate = true;

                db.SaveChanges();
                SignalProcessor?.Raise(new BrokenInventorySignalMessage {
                    DocumentId = inventoryDocument.ID,
                    DocumentDate = inventoryDocument.DocumentDate,
                    Reason = reasonForNotify
                });
                return inventoryDocument;
            }
        }
    }
}