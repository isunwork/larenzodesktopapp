﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using DataLib.Models;
using System.Data.Entity;
using System.Threading.Tasks;
using EntityFramework.BulkInsert.Extensions;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Entities.Documents.CafeOrders;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Entities.Documents.Sessions;
using LaRenzo.DataRepository.Entities.Wirings;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Catalogs.Warehouses;
using LaRenzo.DataRepository.Repositories.Documents.Inventory;
using LaRenzo.DataRepository.Repositories.Finance;
using LaRenzo.DataRepository.Repositories.Others.Times;
using LaRenzo.DataRepository.Repositories.Wiringses.RemainsOfGood;
using LaRenzo.DataRepository.Repositories.Settings;
using NLog;
using LaRenzo.Interfaces;
using LaRenzo.Interfaces.SignalMessages;
using LaRenzo.DataRepository.Repositories.Documents.WebOrders;

namespace LaRenzo.DataRepository.Repositories.Documents.Sessions
{
    public class SessionManager: ISessionManager
    {
        [Inject]
        public TimeSheetManager TimeSheetManager { get; set; }
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public IBrandManager BrandManager { get; set; }
        [Inject]
        public IInventoryManager InventoryManager { get; set; }
        [Inject]
        public IWarehouseManager WarehouseManager { get; set; }
        [Inject]
        public ISignalProcessor SignalProcessor { get; set; }
        [Inject]
        public IRemainsOfGoodWiringsManager RemainsOfGoodWiringsManager { get; set; }
        [Inject]
        public IBankManager BankManager { get; set; }
        [Inject]
        public IWebOrderManager WebOrderManager { get; set; }

        public int AddDayCount(Session session)
        {
            if (DateTime.Now.Day == session.Opened.Day)
            {
                return 0;
            }

            var totalDays = (DateTime.Now - session.Opened).TotalDays;
            if (totalDays > 0 && totalDays < 1)
            {
                return 1;
            }

            return 2;
        }

        public async Task<bool> IsSessionOpen()
        {
            return await GetOpened() != null;
        }

        public bool IsSessionProcessed(int sessionId)
        {
            var session = GetById(sessionId);
            return session != null && session.IsProcessed;
        }

        public bool SessionHasWirings(int sessionId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Any(x => x.Session.ID == sessionId);
            }
        }
               
        private void OnSessionProcessing(int totalOrders, int ordersProcessed, int totalCafeOrders, int cafeOrdersProcessed, SessionProcessingStatus status)
        {
            SignalProcessor?.Raise(new SessionProcessingSignalMessage
            {
                TotalOrders = totalOrders,
                OrdersProcessed = ordersProcessed,
                TotalCafeOrders = totalCafeOrders,
                CafeOrdersProcessed = cafeOrdersProcessed,
                Status = status
            });
        }

        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Открытие сессии </summary>
        ////====================================
        public async Task<Session> Open(TimeSpan? time, bool isVacationDelivery = false, TimeSpan delta = default(TimeSpan), TimeSpan deltaWindow = default(TimeSpan), bool canEditTime = false)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                // Если сессия как-то оказалась открыта - поругаться
                if (await IsSessionOpen())
                {
                    throw new SessionAlreadyOpenedException();
                }

                // Параметры сессии
                var session = new Session
                {
                    Opened = DateTime.Now,                      // Дата открытия
                    IsVacationDelivery = isVacationDelivery,    // 
                    Delta = delta,                              // 
                    DeltaWindow = deltaWindow,                  // 
                    CanEditTime = canEditTime                   // 
                };
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    session.BranchId = BranchRepository.SelectedBranchId;
                }
                session.DateModified = DateTime.Now;
                session.AuthorModified = UserManager.UserLogged.Name;
                var newSession = db.Sessions.Add(session);
                // Сохранить
                db.SaveChanges();

                // Сбросить все статусы
                TimeSheetManager.ClearAllStatuses();
                // Установить начальное время
                TimeSheetManager.SetStartTime(TimeSheetManager.GetNormalizedDateTime(time ?? new TimeSpan(10, 0, 0)));

                return newSession;
            }
        }

        public void UpdateSessionState(TimeSpan delta, TimeSpan deltaWindow, bool isVacation, bool canEditTime)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                Session openSession = db.Sessions.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.Closed == null);
                if (openSession != null)
                {
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        openSession.BranchId = BranchRepository.SelectedBranchId;
                    }
                    openSession.IsVacationDelivery = isVacation;
                    openSession.Delta = delta;
                    openSession.DeltaWindow = deltaWindow;
                    openSession.CanEditTime = canEditTime;
                    openSession.DateModified = DateTime.Now;
                    openSession.AuthorModified = UserManager.UserLogged.Name;
                    db.Entry(openSession).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }

        public DateTime GetLastSessionStartTime()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Sessions.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Any() ? db.Sessions.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Max(x => x.Opened) : DateTime.MinValue;
            }
        }

        /// <summary>
        /// Возвращает последнюю открытыю сессию
        /// </summary>
        /// <returns></returns>
        public async Task<Session> GetOpened(int? branchId = null)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return await db.Sessions.Where(x => x.BranchId == (branchId ?? BranchRepository.SelectedBranchId)).FirstOrDefaultAsync(x => x.Closed == null);
            }
        }


        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Закрыть сессию </summary>
        ////===================================
        public async Task<CloseSessionResult> Close(Session session, CloseSessionOptions options, string siteProtocol)
        {
            // Отменить зависшие веб заказы
            //var webOrders = WebOrderManager.GetUnhandledWebOrders(false);
            //if (webOrders.Any(x => x.Status == WebOrderStatus.Processing))
            //    return CloseSessionResult.WebOrdersInProcessing;

            if (options == null)
            {
                options = new CloseSessionOptions();
            }

            using (var db = PizzaAppDB.GetNewContext(120))
            {
                if (session == null)
                {
                    return CloseSessionResult.Closed;
                }

                // Заказы для этой сессии
                var orders = db.Orders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SessionID == session.ID);

                // Если есть подвешеные заказы, не закрывать сессию.
                if (orders.Any(x => x.State.DoSessionCanBeClosed == false) && !options.DoTransferOpenOrders)
                {
                    return CloseSessionResult.OrdersOpened;
                }

                // заказы кафе для этой сесии
                List<CafeOrderDocument> cafeOrders = db.CaffeOrderDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SessionID == session.ID && x.IsProcessed == false && x.IsMarkToDelete == false).ToList();

                // Если есть не закрытые заказы кафе
                if (cafeOrders.Any())
                {
                    return CloseSessionResult.CafeOrdersOpened;
                }

                db.Sessions.Attach(session);
                session.Closed = session.Closed ?? DateTime.Now;
                session.TotalOrderCount = orders.Count();
                session.TotalRevenue = orders.Any(x => x.State.Name == "Оплачен") ? orders.Where(x => x.State.Name == "Оплачен").Sum(x => x.TotalCost) : 0;

                var query = db.CaffeOrderDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => !x.IsMarkToDelete && x.SessionID == session.ID);

                session.DateModified = DateTime.Now;
                session.AuthorModified = UserManager.UserLogged.Name;
                session.TotalCafeOrderCount = query.Count();
                session.TotalCafeRevenue = (session.TotalCafeOrderCount > 0) ? query.Sum(x => x.DocumentSum) : 0;
                db.SaveChanges();

                if (options.DoOpenNewSession)
                {
                    var newSession = await Open(DateTime.Now.TimeOfDay, session.IsVacationDelivery, session.Delta, session.DeltaWindow, session.CanEditTime);
                    if (options.DoTransferOpenOrders)
                    {
                        TransferOrders(session, newSession);
                    }
                }

                List<Entities.Settings.Brand> brands = BrandManager.GetList();

                foreach (Entities.Settings.Brand brand in brands)
                {
                    if (options.DoSendBankInteractions)
                    {
                        await BankManager.ProcessAll(brand.ID, siteProtocol);
                    }

                }
                return CloseSessionResult.Closed;
            }
        }

        public int GetLastClosedSessionId()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Sessions.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.Closed != null).Max(x => x.ID);
            }
        }

        public void TransferOrders(Session session, Session newSession)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var list = db.Orders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SessionID == session.ID && x.State.DoSessionCanBeClosed == false).ToList();

                foreach (var order in list)
                {
                    order.DateModified = DateTime.Now;
                    order.AuthorModified = UserManager.UserLogged.Name;
                    order.SessionID = newSession.ID;
                }

                db.SaveChanges();
            }
        }

        public List<Session> GetFullList()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Sessions.Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
            }
        }

        public List<Order> GetOrdersInSession(int sessionId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return
                    db.Orders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SessionID == sessionId)
                      .Include(x => x.Driver)
                      .Include(x => x.Session)
                      .ToList();
            }
        }

        public Session GetById(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Sessions.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == id);
            }
        }


        public List<Session> GetSessionDocuments()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Sessions.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.Closed != null && x.IsDepricated != true).ToList();
            }
        }

        public void DeleteSessionDocument(Session document)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (!document.IsMarkToDelete)
                {
                    document.IsProcessed = false;

                    var wirings = db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SessionDocumentID == document.ID).ToList();

                    foreach (var wiring in wirings)
                    {
                        db.RemainsOfGoodWirings.Remove(wiring);
                    }
                }
                document.DateModified = DateTime.Now;
                document.AuthorModified = UserManager.UserLogged.Name;

                document.IsMarkToDelete = !document.IsMarkToDelete;

                db.Entry(document).State = EntityState.Modified;

                db.SaveChanges();
            }
        }

        public Session GetSessionDocument(int sessionId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Sessions.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == sessionId);
            }
        }

        public List<SessionDocumentItem> LoadItemOfSession(int sessionId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.SessionDocumentItems.Include(x => x.Dish).Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SessionID == sessionId).ToList();
            }
        }

        public async Task ProcessSessionDocument(int sessionId, BackgroundWorker bw = null)
        {
            if (sessionId >= 0)
            {
                ProcessSessionDocument(GetById(sessionId), bw);
            }
            else
            {
                ProcessSessionDocument(await GetOpened(), bw);
            }
        }


        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary>  </summary>
        /// <param name="openSession"></param>
        /// <param name="bw"></param>
        public void ProcessSessionDocument(Session openSession, BackgroundWorker bw = null)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                try
                {
                    openSession.IsProcessed = true;
                    openSession.DateModified = DateTime.Now;
                    openSession.AuthorModified = UserManager.UserLogged.Name;
                    db.Entry(openSession).State = EntityState.Modified;
                    var baseWarehouse = db.Warehouses
                        .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                        .Where(x => x.IsBase).ToList();

                    if (!baseWarehouse.Any())
                    {
                        throw new Exception("Не назначен основной склад, списание товаров невозможно");
                    }

                    var orderDenyState =
                        db.OrderStates.Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                            .FirstOrDefault(x => x.Name == "Отказ") ?? new OrderState { ID = -1 };

                    var orderItemsCount = db.OrderItems.Where(x => x.Order.State.ID != orderDenyState.ID)
                        .Count(x => x.Order.SessionID == openSession.ID);
                    var cafeOrderItemsCount = db.CafeOrderDocumentItems
                        .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                        .Count(x => x.CafeOrderDocument.SessionID == openSession.ID);

                    OnSessionProcessing(orderItemsCount, 0, cafeOrderItemsCount, 0,
                        SessionProcessingStatus.LoadingData);

                    //Данные на обработку
                    var sessionDocumentItems = new List<SessionDocumentItem>();
                    var remainsOfGoodWirings = new List<RemainsOfGoodWirings>();

                    //---------Заказы-------------

                    var allOrderItems = db.OrderItems.Include(x => x.Dish).Include(x => x.Order)
                        .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                        .Where(x => x.Order.State.ID != orderDenyState.ID && x.Order.SessionID == openSession.ID)
                        .ToList();

                    var allCafeOrderItems = db.CafeOrderDocumentItems.Include(x => x.CafeOrderDocument)
                        .Include(x => x.Dish)
                        .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                        .Where(x => x.CafeOrderDocument.SessionID == openSession.ID)
                        .ToList();

                    var orderDishIds = allOrderItems.Select(x => x.DishID).Distinct().ToList();
                    var cafeOrderDishIds = allCafeOrderItems.Select(x => x.DishID).Distinct().ToList();

                    orderDishIds.AddRange(cafeOrderDishIds);

                    var allIds = orderDishIds.Distinct().ToList();

                    var allProductLists = db.ItemOfDishSets.Include(x => x.Product)
                        .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                        .Where(x => allIds.Contains(x.DishId)).ToList();

                    OnSessionProcessing(orderItemsCount, 0, cafeOrderItemsCount, 0, SessionProcessingStatus.Processing);

                    int orderItemsProcessed = 0;
                    var warehouses = WarehouseManager.GetWarehouses();
                    foreach (var ordItem in allOrderItems)
                    {
                        if (bw != null && bw.CancellationPending) return;
                        OrderItem item = ordItem;
                        var baseWarehouseForOrder = baseWarehouse.FirstOrDefault(x => x.PointOfSaleId == item.Order.PointOfSaleId);
                        var warehousesForPointOfSale = warehouses.Where(x => x.PointOfSaleId == item.Order.PointOfSaleId);
                        var productList = allProductLists.Where(x => x.DishId == item.DishID).ToList();

                        var sessionDocumentItem = new SessionDocumentItem
                        {
                            SessionID = openSession.ID,
                            DishID = item.DishID,
                            Amount = item.Amount,
                            Sum = item.Dish.Price * item.Amount,
                            IdBrand = item.Order.IdBrand,
                            PayTypeIndex = item.Order.IsUnCash ? 2 : 1
                        };
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            sessionDocumentItem.BranchId = BranchRepository.SelectedBranchId;
                        }
                        sessionDocumentItem.DateModified = DateTime.Now;
                        sessionDocumentItem.AuthorModified = UserManager.UserLogged.Name;
                        sessionDocumentItems.Add(sessionDocumentItem);

                        foreach (var ofDishSet in productList)
                        {
                            if (ofDishSet.Product.IsSetProduct)
                            {
                                var set = ofDishSet;
                                var items =
                                    db.ItemOfProductSets.Include(x => x.Product).Where(x =>
                                            x.BranchId == BranchRepository.SelectedBranchId)
                                        .Where(x => x.ParentProductId == set.ProductId)
                                        .ToList();

                                foreach (var ofProductSet in items)
                                {
                                    var warehouseId = GetWarehouseId(baseWarehouseForOrder, item.Dish, warehousesForPointOfSale);
                                    remainsOfGoodWirings.Add(new RemainsOfGoodWirings
                                    {
                                        WarehouseID = warehouseId,
                                        ProductID = ofProductSet.ProductId,
                                        Amount = -(ofProductSet.Amount * item.Amount * ofDishSet.Amount)
                                    });
                                }
                            }
                            else
                            {
                                var warehouseId = GetWarehouseId(baseWarehouseForOrder, item.Dish, warehousesForPointOfSale);
                                var wiring = new RemainsOfGoodWirings
                                {
                                    WarehouseID = warehouseId,
                                    ProductID = ofDishSet.ProductId,
                                    Amount = -(ofDishSet.Amount * item.Amount)
                                };
                                wiring.DateModified = DateTime.Now;
                                wiring.AuthorModified = UserManager.UserLogged.Name;
                                remainsOfGoodWirings.Add(wiring);
                            }


                        }

                        OnSessionProcessing(orderItemsCount, ++orderItemsProcessed, cafeOrderItemsCount, 0,
                            SessionProcessingStatus.Processing);

                    }

                    //--------Заказы кафе--------------

                    int cafeOrdersProcessed = 0;

                    foreach (var cafeItem in allCafeOrderItems)
                    {
                        if (bw != null && bw.CancellationPending) return;
                        CafeOrderDocumentItem item = cafeItem;
                        var baseWarehouseForOrder = baseWarehouse.FirstOrDefault(x => x.PointOfSaleId == item.CafeOrderDocument.PointOfSaleId);
                        var warehousesForPointOfSale = warehouses.Where(x => x.PointOfSaleId == item.CafeOrderDocument.PointOfSaleId);


                        var productList = allProductLists.Where(x => x.DishId == item.DishID).ToList();

                        var documentItem = new SessionDocumentItem
                        {
                            SessionID = openSession.ID,
                            DishID = item.DishID,
                            Amount = item.Amount,
                            Sum = item.Dish.CafePrice * item.Amount,
                            IdBrand = item.Dish.IdBrand,
                            PayTypeIndex = ((int)item.CafeOrderDocument.PaymentType) + 1

                        };
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            documentItem.BranchId = BranchRepository.SelectedBranchId;
                        }
                        documentItem.DateModified = DateTime.Now;
                        documentItem.AuthorModified = UserManager.UserLogged.Name;
                        sessionDocumentItems.Add(documentItem);

                        foreach (var ofDishSet in productList)
                        {
                            if (ofDishSet.Product.IsSetProduct)
                            {
                                var set = ofDishSet;
                                var items =
                                    db.ItemOfProductSets.Include(x => x.Product)
                                        .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                                        .Where(x => x.ParentProductId == set.ProductId)
                                        .ToList();

                                foreach (var ofProductSet in items)
                                {
                                    var warehouseId = GetWarehouseId(baseWarehouseForOrder, item.Dish, warehousesForPointOfSale);
                                    var wiring = new RemainsOfGoodWirings
                                    {
                                        WarehouseID = warehouseId,
                                        ProductID = ofProductSet.ProductId,
                                        Amount = -(ofProductSet.Amount * item.Amount * ofDishSet.Amount)
                                    };
                                    wiring.DateModified = DateTime.Now;
                                    wiring.AuthorModified = UserManager.UserLogged.Name;
                                    remainsOfGoodWirings.Add(wiring);
                                }
                            }
                            else
                            {
                                var warehouseId = GetWarehouseId(baseWarehouseForOrder, item.Dish, warehousesForPointOfSale);
                                var wiring = new RemainsOfGoodWirings
                                {
                                    WarehouseID = warehouseId,
                                    ProductID = ofDishSet.ProductId,
                                    Amount = -(ofDishSet.Amount * item.Amount)
                                };
                                wiring.DateModified = DateTime.Now;
                                wiring.AuthorModified = UserManager.UserLogged.Name;
                                remainsOfGoodWirings.Add(wiring);
                            }
                        }

                        OnSessionProcessing(orderItemsCount, orderItemsProcessed, cafeOrderItemsCount,
                            ++cafeOrdersProcessed, SessionProcessingStatus.Processing);
                    }

                    if (bw != null && bw.CancellationPending) return;

                    OnSessionProcessing(orderItemsCount, orderItemsProcessed, cafeOrderItemsCount, cafeOrdersProcessed,
                        SessionProcessingStatus.Saving);

                    int sessionDocumentAlreadyExistCount = db.SessionDocumentItems
                        .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                        .Count(x => x.SessionID == openSession.ID);

                    var remainsGrouped =
                        RemainsOfGoodWiringsManager.GroupByProductAndWarehouse(remainsOfGoodWirings);

                    var remainsDate = openSession.Closed.HasValue ? openSession.Closed.Value : DateTime.Now;

                    remainsGrouped.ForEach(x =>
                    {
                        x.DateModified = DateTime.Now;
                        x.AuthorModified = UserManager.UserLogged.Name;
                        x.Date = remainsDate;
                        x.SessionDocumentID = openSession.ID;
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            x.BranchId = BranchRepository.SelectedBranchId;
                        }
                    });

                    //Добавлять документы сессии, только если их еще нет
                    if (sessionDocumentAlreadyExistCount == 0)
                    {
                        db.BulkInsert(sessionDocumentItems);
                    }

                    db.BulkInsert(remainsGrouped);

                    //Проверяем, нужно ли пометить какую либо инвентаризацию на просчет.
                    var allWarehouseIds = remainsGrouped.Select(x => x.WarehouseID).Distinct().ToList();

                    allWarehouseIds.ForEach(x => InventoryManager.MarkInventoryDocumentRecalculate(x, remainsDate,
                        string.Format("Операции с документом сессии, закрытой {0:d}", openSession.Closed)));

                    db.SaveChanges();
                    OnSessionProcessing(orderItemsCount, orderItemsProcessed, cafeOrderItemsCount, cafeOrdersProcessed,
                        SessionProcessingStatus.Done);
                }
                catch (Exception exception)
                {
                    var logger = LogManager.GetCurrentClassLogger();
                    logger.Log(LogLevel.Error, exception);
                }
            }
        }

        private int GetWarehouseId(Warehouse baseWarehouseForOrder, Dish item, IEnumerable<Warehouse> warehousesForPointOfSale)
        {
            var warehouseId = baseWarehouseForOrder.ID;
            if (item.UseCustomWarehouse && item.WarehouseForDishId != null)
            {
                var warehouseForDish = warehousesForPointOfSale.FirstOrDefault(x =>
                    x.WarehouseForDishId == item.WarehouseForDishId);
                warehouseId = warehouseForDish?.ID ?? baseWarehouseForOrder.ID;
            }

            return warehouseId;
        }
    }

}