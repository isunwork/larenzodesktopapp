﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Documents.Posting;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Documents.Inventory;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Documents.Posting
{
    public class PostingManager: IPostingManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public IInventoryManager InventoryManager { get; set; }
        public List<PostingDocument> GetPostingDocuments()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.PostingDocuments.Include("Warehouse").Where(x => x.BranchId == BranchRepository.SelectedBranchId).OrderBy(x => x.ID).ToList();
            }
        }

        public PostingDocument GetPostingDocument(int postingDocId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.PostingDocuments.Include("Warehouse").Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                         .FirstOrDefault(x => x.ID == postingDocId);
            }
        }

        public void UpdatePostingDocument(PostingData postingData)
        {
            PostingDocument document = postingData.Document;
            List<PostingDocumentItem> documentItems = postingData.DocumentItems;
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    document.BranchId = BranchRepository.SelectedBranchId;
                }
                document.DateModified = DateTime.Now;
                document.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(document).State = EntityState.Modified;

                var existsItem = db.PostingDocumentItems.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.PostingDocumentID == document.ID).ToList();

                var oldDocument = db.PostingDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == document.ID);

                InventoryManager.MarkInventoryDocumentRecalculate(document.WarehouseID, new DocumentWithDateHelper().GetMinDate(document, oldDocument));

                foreach (var documentItem in documentItems)
                {
                    if (documentItem.ID == -1)
                    {
                        var docItem = new PostingDocumentItem
                        {
                            Amount = documentItem.Amount,
                            Sum = documentItem.Sum,
                            Price = documentItem.Price,
                            ProductID = documentItem.ProductID,
                            PostingDocumentID = document.ID
                        };
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            docItem.BranchId = BranchRepository.SelectedBranchId;
                        }
                        docItem.DateModified = DateTime.Now;
                        docItem.AuthorModified = UserManager.UserLogged.Name;
                        db.PostingDocumentItems.Add(docItem);
                    }
                    else
                    {
                        if (existsItem.Any(x => x.ID == documentItem.ID))
                        {
                            var item = existsItem.FirstOrDefault(x => x.ID == documentItem.ID);
                            if (item != null)
                            {
                                item.Amount = documentItem.Amount;
                                item.Price = documentItem.Price;
                                item.Sum = documentItem.Sum;
                                existsItem.Remove(item);
                            }
                        }
                    }
                }

                foreach (var documentItem in existsItem)
                {
                    db.PostingDocumentItems.Remove(documentItem);
                }

                db.SaveChanges();
            }
        }

        public int AddPostingDocument(PostingDocument document)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    document.BranchId = BranchRepository.SelectedBranchId;
                }
                document.DateModified = DateTime.Now;
                document.AuthorModified = UserManager.UserLogged.Name;
                db.PostingDocuments.Add(document);

                db.SaveChanges();

                InventoryManager.MarkInventoryDocumentRecalculate(document.WarehouseID, document.DocumentDate);

                return document.ID;
            }
        }

        public void DeletePostingDocument(PostingDocument document)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (!document.IsMarkToDelete)
                {
                    document.IsProcessed = false;

                    var wirings = db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.PostingDocumentID == document.ID).ToList();

                    foreach (var wiring in wirings)
                    {
                        db.RemainsOfGoodWirings.Remove(wiring);
                    }
                }
                document.DateModified = DateTime.Now;
                document.AuthorModified = UserManager.UserLogged.Name;
                document.IsMarkToDelete = !document.IsMarkToDelete;

                db.Entry(document).State = EntityState.Modified;

                db.SaveChanges();

                InventoryManager.MarkInventoryDocumentRecalculate(document.WarehouseID, document.DocumentDate);
            }
        }

        public List<PostingDocumentItem> GetPostingDocumentItems(int postingDocumentId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.PostingDocumentItems.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Include("Product")
                  .Include("Product.Measure")
                  .Where(x => x.PostingDocumentID == postingDocumentId)
                  .ToList();
            }
        }
    }
}