﻿using System.Collections.Generic;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Documents.Orders
{
    public class StateManager: IStateManager
    {
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        public OrderState GetByName(string name)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.OrderStates.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.Name == name);
            }
        }

        public List<OrderState> GetList()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.OrderStates.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).ToList();
            }
        }
    }
}
