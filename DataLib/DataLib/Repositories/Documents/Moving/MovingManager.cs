﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Entities.Documents.Moving;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Documents.Inventory;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Documents.Moving
{
    public class MovingManager: IMovingManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public IInventoryManager InventoryManager { get; set; }
        public List<MovingDocumentItem> GetMovingDocumentItems(int movingDocId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return
                    db.MovingDocumentItems.Include("Product")
                      .Include("Product.Measure")
                      .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                      .Where(x => x.MovingDocumentID == movingDocId)
                      .ToList();
            }
        }

        public List<MovingDocument> GetMovingDocuments()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.MovingDocuments.Include("Warehouse").Include("PurposeWarehouse")
                    .Where(x => x.BranchId == BranchRepository.SelectedBranchId).OrderBy(x => x.ID).ToList();
            }
        }

        public MovingDocument GetMovingDocument(int movingDocId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.MovingDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == movingDocId);
            }
        }

        public void UpdateMovingDocument(MovingData movingData)
        {
            MovingDocument document = movingData.Document;
            List<MovingDocumentItem> documentItems = movingData.DocumentItems;
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    document.BranchId = BranchRepository.SelectedBranchId;
                }
                document.DateModified = DateTime.Now;
                document.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(document).State = EntityState.Modified;
                var existsItem = db.MovingDocumentItems.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.MovingDocumentID == document.ID).ToList();
                var oldDocument = db.MovingDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == document.ID);

                InventoryManager.MarkInventoryDocumentRecalculate(document.WarehouseID, new DocumentWithDateHelper().GetMinDate(document, oldDocument));

                foreach (var documentItem in documentItems)
                {
                    if (documentItem.ID == -1)
                    {
                        var docItem = new MovingDocumentItem
                        {
                            Amount = documentItem.Amount,
                            ProductID = documentItem.ProductID,
                            MovingDocumentID = document.ID
                        };
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            docItem.BranchId = BranchRepository.SelectedBranchId;
                        }
                        docItem.DateModified = DateTime.Now;
                        docItem.AuthorModified = UserManager.UserLogged.Name;
                        db.MovingDocumentItems.Add(docItem);
                    }
                    else
                    {
                        if (existsItem.Any(x => x.ID == documentItem.ID))
                        {
                            var item = existsItem.FirstOrDefault(x => x.ID == documentItem.ID);
                            if (item != null)
                            {
                                item.Amount = documentItem.Amount;
                                existsItem.Remove(item);
                            }
                        }
                    }
                }

                foreach (var documentItem in existsItem)
                {
                    db.MovingDocumentItems.Remove(documentItem);
                }

                db.SaveChanges();
            }
        }


        public string TestMovingDocument(int fromDocumentId, List<MovingDocumentItem> documentItems)
        {

            string resultError = "";

            using (var db = PizzaAppDB.GetNewContext())
            {
                foreach (MovingDocumentItem documentItem in documentItems)
                {

                    MovingDocumentItem findDocumentItem = db.MovingDocumentItems.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ProductID == documentItem.ProductID && x.MovingDocumentID == fromDocumentId);

                    if (findDocumentItem != null)
                    {
                        Product product = db.Products.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentItem.ProductID);

                        if (product != null)
                        {
                            if (documentItem.Amount > findDocumentItem.Amount)
                            {
                                resultError += "Количество  " + product.Name + " = " + findDocumentItem.Amount + ", а запрашивается " + documentItem.Amount + "\r\n";
                            }
                        }
                        else
                        {
                            resultError += "Не было найдено продукта с id = " + documentItem.ProductID + "\r\n";
                        }
                    }
                    else
                    {
                        Product product = db.Products.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentItem.ProductID);

                        if (product == null)
                        {
                            resultError += "Неизвестный продукт с id = " + documentItem.ProductID + "\r\n";
                        }
                        else
                        {
                            resultError += "Не было перемещения на склад продукта " + product.Name + "\r\n";
                        }
                    }
                }
            }

            return resultError;
        }



        public int AddMovingDocument(MovingDocument document)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    document.BranchId = BranchRepository.SelectedBranchId;
                }
                document.DateModified = DateTime.Now;
                document.AuthorModified = UserManager.UserLogged.Name;
                db.MovingDocuments.Add(document);

                db.SaveChanges();

                InventoryManager.MarkInventoryDocumentRecalculate(document.WarehouseID, document.DocumentDate);

                return document.ID;
            }
        }

        public void DeleteMovingDocument(MovingDocument document)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (!document.IsMarkToDelete)
                {
                    document.IsProcessed = false;

                    var wirings = db.RemainsOfGoodWirings.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.MovingDocumentID == document.ID).ToList();

                    foreach (var wiring in wirings)
                    {
                        db.RemainsOfGoodWirings.Remove(wiring);
                    }
                }

                document.IsMarkToDelete = !document.IsMarkToDelete;

                db.Entry(document).State = EntityState.Modified;

                db.SaveChanges();

                InventoryManager.MarkInventoryDocumentRecalculate(document.WarehouseID, document.DocumentDate);
            }
        }
    }
}