﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Documents.WebOrders;
using LaRenzo.DataRepository.Repositories.Others.GlobalOptions;
using LaRenzo.DataRepository.Repositories.Settings;
using LaRenzo.TransferLibrary;

namespace LaRenzo.DataRepository.Repositories.Others.Transfers
{
    public class TransferManager: ITransferManager
    {
        [Inject]
        public IBrandManager BrandManager { get; set; }
        [Inject]
        public IGlobalOptionsManager GlobalOptionsManager { get; set; }
        /// <summary>  </summary>
        /// <returns>Null if WebException catched</returns>
        public async Task<List<OrderInfo>> GetUnhandledWebOrders(string site, string siteProtocol, int brandId)
        {
            var unhandledWebOrders = new List<OrderInfo>();

            if (BrandManager.SelectedBrand != null)
            {
                if (!string.IsNullOrEmpty(site) && site.Trim().Length > 3)
                {
                    List<OrderInfo> ordersFromWeb = await GetWebOrderderFromSite(
                        $"{siteProtocol}://{site}/api/getorders.php?secureSeed=kkkkk09xqqqkd293k", site);
                    if (ordersFromWeb != null)
                    {
                        foreach (var order in ordersFromWeb)
                        {
                            order.BrandId = brandId;
                        }
                        unhandledWebOrders.AddRange(ordersFromWeb);
                    }
                }
            }
            return unhandledWebOrders.Count == 0 ? null : unhandledWebOrders;
        }


        public async Task<string> SendBankCommandApprove(Guid orderGuid, int brandId, string siteProtocol)
        {
            var response = new StringBuilder();

            var sitesList = GlobalOptionsManager.GetSitesList(brandId);
            foreach (var site in sitesList)
            {
                var result = await ApproveOrder(site, orderGuid, siteProtocol);
                response.Append($"{site}: {result}{Environment.NewLine}");
            }
            return response.ToString();
        }

        private async Task<string> ApproveOrder(string site, Guid orderGuid, string siteProtocol)
        {
            var url = $"{siteProtocol}://{site}/api/ApproveOrder.php?guid={orderGuid}&secureSeed=kkkkk09xqqqkd293k";
            var pageContent = await GetPageAsString(new Uri(url));

            var response = new Regex("(?<=<Result>)(.*)(?=<\\/Result>)").Match(pageContent).Value;

            return string.IsNullOrWhiteSpace(response) ? "Заказ не найден" : response;
        }

        public string SendBankCommandCancel(Guid orderGuid, int brandId, string siteProtocol)
        {
            var response = new StringBuilder();

            var sitesList = GlobalOptionsManager.GetSitesList(brandId);
            foreach (var site in sitesList)
            {
                response.Append($"{site}: {CancelOrder(site, orderGuid, siteProtocol)}{Environment.NewLine}");
            }

            return response.ToString();
        }


        private async Task<string> CancelOrder(string site, Guid orderGuid, string siteProtocol)
        {
            var url = $"{siteProtocol}://{site}/api/CancelOrder.php?guid={orderGuid}&secureSeed=kkkkk09xqqqkd293k";
            var pageContent = await GetPageAsString(new Uri(url));

            var response = new Regex("(?<=<Result>)(.*)(?=<\\/Result>)").Match(pageContent).Value;

            return string.IsNullOrWhiteSpace(response) ? "Заказ не найден" : response;
        }


        public async Task<List<OrderInfo>> GetWebOrderderFromSite(string uri, string source)
        {
            List<OrderInfo> retItems = null;

            var xml = await GetPageAsString(new Uri(uri));
            if (xml == "404")
            {
            }
            else
            {
                try
                {
                    // Ремонт пустых SKU
                    xml = xml.Replace("<SKU></SKU>", "<SKU>-1</SKU>");
                    xml = xml.Replace("<SKU> </SKU>", "<SKU>-1</SKU>");
                    xml = xml.Replace("<SKU>  </SKU>", "<SKU>-1</SKU>");
                    xml = xml.Replace("<SKU>   </SKU>", "<SKU>-1</SKU>");
                    xml = xml.Replace("<SKU>    </SKU>", "<SKU>-1</SKU>");
                    retItems = new TransferSerializer().Deserialize<List<OrderInfo>>(xml, source);
                }
                catch
                {
                }
            }

            return retItems;
        }

        public async Task<bool> MarkOrderProccesedOnWebSite(WebOrder order, int brandId, string siteProtocol)
        {
            if (order == null)
            {
                return true;
            }

            var sitesList = GlobalOptionsManager.GetSitesList(brandId);
#if !DEBUG
            foreach (var site in sitesList)
            {
                var result = await GetPageAsString(new Uri(
                    $"{siteProtocol}://{site}/api/MarkProcessed.php?guid={order.Guid}&secureseed=kkkkk09xqqqkd293k"));
                if (result == "404")
                {
                    return false;
                }
            }
#endif
            return true;
        }

        public async Task<string> GetPageAsString(Uri address)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(address);
                using (var responseTask = request.GetResponseAsync())
                {
                    var response = await responseTask;
                    var stream = response?.GetResponseStream();
                    if (stream != null)
                    {
                        var streamReader = new StreamReader(stream);
                        return streamReader.ReadToEnd();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return "404";
            }
            return null;
        }


    }

}
