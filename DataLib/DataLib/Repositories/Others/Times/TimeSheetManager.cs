﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Catalogs.Sections;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Entities.Others.AmountFactories;
using LaRenzo.DataRepository.Entities.Others.TimeSheets;
using LaRenzo.DataRepository.Repositories.Catalogs;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Documents.Sessions;
using LaRenzo.DataRepository.Repositories.Others.AmountFactories;
using LaRenzo.DataRepository.Repositories.Others.GlobalOptions;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Others.Times
{
    public class TimeSheetManager: ITimeSheetManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public IBrandManager BrandManager { get; set; }
        [Inject]
        public IPointOfSaleRepository PointOfSaleRepository { get; set; }
        [Inject]
        public ISessionManager SessionManager { get; set; }
        [Inject]
        public IAmountFactorManager AmountFactorManager { get; set; }
        [Inject] 
        public IBaseOptionsFunctions BaseOptionsFunctions { get; set; }
        [Inject] 
        public IGlobalOptionsManager GlobalOptionsManager { get; set; }

        private const int AmountOfBlocksCanDivideOrder = 5;

        //__________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Добавить цех в таблицу использования времени </summary>
        ////=================================================================
        public void AddSectionToTimeSheetTable(Section section, DateTime startSessionTime, int brandId, int pointOfSaleId)
        {
            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                // Получить начальное время из открытой сессии или проигнорировать (потом заполниться при открытии сессии)
                var openSession = db.Sessions.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.Closed == null);

                if (openSession != null)
                {
                    DateTime normalizedStartSessionTime = GetNormalizedDateTime(new TimeSpan(startSessionTime.Hour, startSessionTime.Minute, 0));

                    var sql = new StringBuilder();

                    // Минут в 28 часах 
                    const int minutsIn28Hours = 28 * 60;

                    // Собрать команду для цеха по всему времени работы
                    for (int i = 0; i < minutsIn28Hours; i++)
                    {
                        sql.Append($"INSERT INTO dbo.TimeSheets (SectionID, Status, Time, BranchId, BrandId, PointOfSaleId) VALUES ({section.ID.ToString()}, '', '{normalizedStartSessionTime}', '{BranchRepository.SelectedBranchId}', '{brandId}', '{pointOfSaleId}')");

                        normalizedStartSessionTime = normalizedStartSessionTime.AddMinutes(1);
                    }

                    db.Database.ExecuteSqlCommand(sql.ToString());
                }
            }
        }


        public List<TimeSheet> GetNoTrackingList()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.TimeSheets.AsNoTracking().Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
            }
        }

        //__________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Удалить все цеха для таблицы использования времени </summary>
        ////=======================================================================
        public void RemoveSectionFromTimeSheetTable(Section section)
        {
            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                db.Database.ExecuteSqlCommand($@"DELETE FROM dbo.TimeSheets WHERE SectionID = {section.ID} 
                                                 WHERE BranchId = {BranchRepository.SelectedBranchId}", section.ID);
            }
        }


        //_________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Удалить все статусы </summary>
        ////========================================
        public void ClearAllStatuses()
        {
            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                db.Database.ExecuteSqlCommand($@"UPDATE dbo.TimeSheets SET Status = '', ReservationGuid = NULL, ReservationDate = NULL
                                                WHERE BranchId = {BranchRepository.SelectedBranchId}");
            }
        }


        //_________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Установить начальное время открытой сессии в таблице времени </summary>
        ////=================================================================================
        public void SetStartTime(DateTime startSessionTime)
        {
            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                // Удалить все данные из таблицы 
                db.Database.ExecuteSqlCommand($"delete from dbo.TimeSheets where BranchId = {BranchRepository.SelectedBranchId}");

                // Получить все Цеха
                var sections = db.Sections.Where(x => x.BranchId == BranchRepository.SelectedBranchId);
                var brands = BrandManager.GetList();
                var pointOfSales = PointOfSaleRepository.GetItems();
                foreach (var brand in brands)
                {
                    foreach (var pointOfSale in pointOfSales)
                    {
                        // Добавить упоминание о цехе в таблицу времени работы
                        foreach (var section in sections)
                        {
                            AddSectionToTimeSheetTable(section, startSessionTime, brand.ID, pointOfSale.ID);
                        }
                    }
                }
            }
        }


        //_________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Separate order items to lists by section </summary>
        /// <param name="orderItems"></param>
        /// <returns></returns>
        public Dictionary<Section, List<OrderItem>> SeparateToSections(List<OrderItem> orderItems)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                List<Section> sections = db.Sections.Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();

                return sections.ToDictionary(section => section,
                                             section =>
                                             orderItems.Where(
                                                 x =>
                                                 x.Dish.Category.SectionID ==
                                                 section.ID).ToList());
            }
        }

        private int GetOverheadTime(int brandId)
        {
            return int.Parse(BaseOptionsFunctions.GetValueOrZero("OverheadTime" + brandId));

        }
        //_________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Перевести список заказов во время их готовки </summary>
        ////=================================================================
        public Dictionary<Section, int> ConvertOrderItemsToTimeCooking(Dictionary<Section, List<OrderItem>> separatedList, int brandId)
        {
            return separatedList.ToDictionary(
                pair => pair.Key,
                pair =>
                    {
                        if (pair.Key.DoPrintCheck)
                        {
                            var time = 0;
                            int selectedBranchId = BranchRepository.SelectedBranchId;
                            var sameFactorCollections = pair.Value.GroupBy(x => x.Dish.GetFactorCollection(selectedBranchId));

                            foreach (var sameFactorCollection in sameFactorCollections)
                            {
                                var quantity = sameFactorCollection.Sum(x => x.Amount);

                                if (quantity > 0)
                                {
                                    using (var db = PizzaAppDB.GetNewContext())
                                    {
                                        var topOrderItem = sameFactorCollection.FirstOrDefault();

                                        var objDish = db.Dishes.Include(x => x.Category.Section.PeopleFactorCollection)
                                            .Include(x => x.Category.Section.EquipmentFactorCollection).Include("FactorCollection")
                                        .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                                        .First(x => x.ID == topOrderItem.DishID);
                                        var objSection = objDish.Category.Section;

                                        var outDatedCollection = objDish.FactorCollection;
                                        var outDatedPepoleCollection = objSection?.PeopleFactorCollection;
                                        var outDatedEquipmentCollection =
                                            objSection?.EquipmentFactorCollection;

                                        var qtyCollection = (outDatedCollection == null) ? null : AmountFactorManager.GetCollectionById(outDatedCollection.ID);
                                        var peopleCollection = (outDatedPepoleCollection == null)
                                                                   ? null
                                                                   : AmountFactorManager.GetCollectionById(
                                                                       outDatedPepoleCollection.ID);
                                        var equipmentCollection = (outDatedEquipmentCollection == null)
                                                                      ? null
                                                                      : AmountFactorManager.GetCollectionById(
                                                                          outDatedEquipmentCollection.ID);

                                        float qtyFactor = GetFactor(qtyCollection, quantity);
                                        float peopleFactor = GetFactor(peopleCollection, objSection?.PeopleWorks);
                                        float equipmentFactor = GetFactor(equipmentCollection, objSection?.EquipmentWorks);

                                        var multiplayer = qtyFactor * peopleFactor * equipmentFactor;

                                        var cleanTime = sameFactorCollection.Sum(x => x.Amount * x.Dish.CookingTime);

                                        float wetTime = cleanTime * multiplayer;

                                        time += (int)Math.Ceiling(wetTime);
                                    }
                                }
                            }

                            return time == 0 ? 0 : time + GetOverheadTime(brandId);
                        }
                        else
                        {
                            return pair.Value.Any() ? GetOverheadTime(brandId) : 0;
                        }
                    });
        }

        private float GetFactor(AmountFactorCollection collection, decimal? quantity)
        {
            AmountFactor factor = null;
            if (collection != null)
            {
                factor =
                    collection.Data.OrderBy(x => x.Amount).FirstOrDefault(x => x.Amount >= quantity);
                if (factor == null)
                    factor = collection.Data.OrderByDescending(x => x.Amount).FirstOrDefault(x => x.Amount < quantity);
            }

            return factor == null ? 1 : factor.Factor;
        }

        public int ContainsImmutableBlocks(List<TimeSheet> list, DateTime currentTime, bool shiftDownward)
        {
            list = (shiftDownward)
                       ? list.OrderBy(x => x.Time).ToList()
                       : list.OrderByDescending(x => x.Time).ToList();

            var indexOfImmutableBlock = -1;

            indexOfImmutableBlock = Math.Max(
                indexOfImmutableBlock,
                list.FindLastIndex(x => x.Status == "Reserved" && x.Time < currentTime));

            indexOfImmutableBlock = Math.Max(
                indexOfImmutableBlock,
                list.FindLastIndex(x => x.Status == "Cooking"));


            var blockedCellsInRow = 0;
            var previousIsBlocked = false;

            foreach (var timeSheet in list)
            {
                if (timeSheet.Status == "Blocked")
                {
                    if (previousIsBlocked)
                    {
                        var isSequenceImmutable = (++blockedCellsInRow > AmountOfBlocksCanDivideOrder);

                        if (isSequenceImmutable)
                        {
                            indexOfImmutableBlock = Math.Max(
                                indexOfImmutableBlock,
                                list.IndexOf(timeSheet));
                        }
                    }
                    else
                    {
                        blockedCellsInRow = 1;
                    }

                    previousIsBlocked = true;
                }
                else
                {
                    previousIsBlocked = false;
                }
            }

            return indexOfImmutableBlock;
        }

        /// <summary>
        /// Находит клетки, которые можно зарезервировать.
        /// Вводится понятия immutableBlocks - это те клетки, которые не могут делить заказ.
        /// К ним относятся все занятые клетки, кроме истекшего резерва и "Blocked" длинной 5 и менне клеток подряд.
        /// Если мы натыкаеся на immutableBlock, мы рекурсивно вызываем функцию сразу за ним 
        /// (точнее мы сразу определяем самый крайний (в зависимости от направления сортировки) и уже после него вызываем снова ф-ию)
        /// Если мы натыкаемся на заблокированные клетки (меньше или равно 5 подряд), мы добавляем shift - параметр, указ.
        /// Что нужно взять больше клеток. При возврате значения, лишние клетки выкидываем.
        /// Если мы в процессе увеличения значения shift натыкаемся на immutableBlock, мы снова вызываем функцию за этим блоком,
        /// Полагая shift равным нулю.
        /// </summary>
        /// <returns></returns>
		public List<TimeSheet> GetSheetsForReserveInSection(int sectionId, int minutesForSection, int shift, DateTime targetTime, bool isNearTime, bool shiftDownward, int addDayCount, int pointOfSaleId, int brandId)
        {

            DateTime timeToReserve = GetNormalizedDateTime(new TimeSpan(targetTime.Hour, targetTime.Minute, 0), addDayCount);

            var nowRaw = DateTime.Now;
            DateTime now = GetNormalizedDateTime(new TimeSpan(nowRaw.Hour, nowRaw.Minute, 0), addDayCount);

            var timeSheetsToUpdate = new List<TimeSheet>();

            if (minutesForSection == 0) return timeSheetsToUpdate;

            using (PizzaAppDB db = PizzaAppDB.GetNewContext())
            {
                if (isNearTime)
                {
                    timeSheetsToUpdate = db.TimeSheets
                        .Where(x => x.BranchId == BranchRepository.SelectedBranchId
                                    && x.PointOfSaleId == pointOfSaleId
                                    && x.BrandId == brandId)
                        .Where(x => x.Time >= timeToReserve && x.SectionID == sectionId)
                        .Where(x => x.Status != "Reserved" && x.Status != "Cooking")
                                .OrderBy(x => x.Time).
                                Take(minutesForSection + shift).
                                ToList();
                }
                else
                {
                    timeToReserve = timeToReserve.AddMinutes((shiftDownward) ? shift : 0);

                    timeSheetsToUpdate = db.TimeSheets
                        .Where(x => x.BranchId == BranchRepository.SelectedBranchId
                                    && x.PointOfSaleId == pointOfSaleId
                                    && x.BrandId == brandId)
                        .Where(x => x.Time >= now && x.Time < timeToReserve && x.SectionID == sectionId)
                        .Where(x => x.Status != "Reserved" && x.Status != "Cooking")
                                .OrderByDescending(x => x.Time).
                                Take(minutesForSection + shift).
                                ToList();
                }


                if (timeSheetsToUpdate.Count < minutesForSection + shift)
                {
                    throw new OutOfTimeSheetsException();
                }

                var immutableBlockIndex = ContainsImmutableBlocks(timeSheetsToUpdate, timeToReserve, shiftDownward);

                if (immutableBlockIndex != -1)
                {
                    if (!isNearTime && shiftDownward)
                    {
                        var timeShift = immutableBlockIndex + 1;
                        if (timeShift > timeSheetsToUpdate.Count)
                        {
                            timeShift -= timeSheetsToUpdate.Count;
                        }
                        // Новое целевое время
                        DateTime newTargetTime = targetTime.AddMinutes(timeShift);
                        timeSheetsToUpdate = GetSheetsForReserveInSection(sectionId, minutesForSection, 0, newTargetTime, false, true, GetAddDayCountByTime(newTargetTime), pointOfSaleId, brandId);
                    }
                    else
                    {
                        // Новое целевое время
                        DateTime newTargetTime = targetTime.AddMinutes((immutableBlockIndex + 1) * (isNearTime ? 1 : -1));
                        timeSheetsToUpdate = GetSheetsForReserveInSection(sectionId, minutesForSection, 0, newTargetTime, isNearTime, shiftDownward, GetAddDayCountByTime(newTargetTime), pointOfSaleId, brandId);
                    }
                }

                while (timeSheetsToUpdate.Count(x => x.Status != "Blocked") < minutesForSection)
                {
                    var blockedAmount = timeSheetsToUpdate.Count(x => x.Status == "Blocked");
                    timeSheetsToUpdate = GetSheetsForReserveInSection(sectionId, minutesForSection, blockedAmount, targetTime, isNearTime, shiftDownward, addDayCount, pointOfSaleId, brandId);
                }
                var checkTime = true;
                if (timeSheetsToUpdate.Count > 0)
                {
                    var time = timeSheetsToUpdate.OrderBy(x => x.Time).FirstOrDefault().Time;
                    foreach (var timeSheet in timeSheetsToUpdate.OrderBy(x => x.Time))
                    {
                        if (timeSheet.Time == time)
                        {
                            time = time.AddMinutes(1);
                        }
                        else
                        {
                            checkTime = false;
                        }

                    }
                    if (!checkTime)
                    {
                        timeSheetsToUpdate = GetSheetsForReserveInSection(sectionId, minutesForSection, 0,
                            time, isNearTime, shiftDownward, GetAddDayCountByTime(time), pointOfSaleId, brandId);
                    }
                }
            }

            return timeSheetsToUpdate.Where(x => x.Status != "Blocked").ToList();
        }


        //_________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Сколько дней прошло </summary>
        ////==============================================
        public int GetAddDayCountByTime(DateTime dateTime)
        {
            return dateTime.Day - 1;
        }


        public int CalculateMaxTimeGap(IEnumerable<TimeSheet> list)
        {
            int maxTimeGap = 0;

            var maxTimes = list.GroupBy(x => x.SectionID).Select(x => x.Max(y => y.Time));

            if (maxTimes.Any())
            {
                maxTimeGap = (int)(maxTimes.Max() - maxTimes.Min()).TotalMinutes;
            }

            return maxTimeGap;
        }

        public void MarkReservedAndSave(Guid orderGuid, List<TimeSheet> timeSheetsToUpdate, int brandId)
        {
            DateTime reserveTill = DateTime.Now.ToLocalTime().AddMinutes(GlobalOptionsManager.GetReserveTime(brandId));

            using (var db = PizzaAppDB.GetNewContext())
            {
                foreach (var x in timeSheetsToUpdate)
                {
                    x.Status = "Reserved";
                    x.ReservationDate = reserveTill;
                    x.ReservationGuid = orderGuid;
                    x.BranchId = BranchRepository.SelectedBranchId;
                    x.DateModified = DateTime.Now;
                    x.AuthorModified = UserManager.UserLogged.Name;
                    db.Entry(x).State = EntityState.Modified;
                }

                db.SaveChanges();
            }
        }


        //__________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Добавить заказ на таблицу времени </summary>
        ////======================================================
        public bool AddOrderToTimeSheet(Guid orderGuid)
        {
            bool isSuccsessfull = true;

            //DateTime now = DateTime.Now.ToLocalTime();
            //DateTime currentTime = GetNormalizedDateTime(new TimeSpan(now.Hour, now.Minute, 0), false);

            using (var db = PizzaAppDB.GetNewContext())
            {
                List<TimeSheet> items = db.TimeSheets.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.ReservationGuid == orderGuid).ToList();

                if (items.Count > 0)
                {
                    items.ForEach(x =>
                    {
                        x.Status = "Cooking";
                        x.ReservationDate = null;
                        x.DateModified = DateTime.Now;
                        x.AuthorModified = UserManager.UserLogged.Name;
                    });
                    db.SaveChanges();
                }
                else
                {
                    isSuccsessfull = false;
                }
            }

            return isSuccsessfull;
        }


        /// <summary>
        /// Clear non-empty cells in time sheet
        /// </summary>
        /// <param name="orderGuid">Reservation Guid</param>
        /// <param name="withCooking">True: Deletes all cells with taken Guid; False: Deletes only cells with "Reserved" status</param>
        public void ClearReservation(Guid? orderGuid, bool withCooking = true)
        {
            if (orderGuid != null)
            {
                using (PizzaAppDB db = PizzaAppDB.GetNewContext())
                {
                    var itemsToClear = db.TimeSheets
                        .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                        .Where(x => x.ReservationGuid == orderGuid && (x.Status == "Reserved" || withCooking)).ToList();
                    itemsToClear.ForEach(x =>
                    {
                        x.ReservationGuid = null;
                        x.ReservationDate = null;
                        x.Status = string.Empty;
                        x.DateModified = DateTime.Now;
                        x.AuthorModified = UserManager.UserLogged.Name;
                    });

                    db.SaveChanges();
                }
            }
        }


        //__________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Вернуть дату под таблицу времени </summary>
        ////=====================================================
        public DateTime GetNormalizedDateTime(TimeSpan shift, int addDayCount = 0)
        {
            DateTime result = DateTime.MinValue.AddYears(1999).Add(shift);
            result = result.AddDays(addDayCount);
            return result;
        }

        public List<TimeSheet> GetList()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = db.TimeSheets.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.ReservationGuid != null);
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    return result.Where(x => x.BranchId == BranchRepository.SelectedBranchId || x.BranchId == 0).ToList();
                }
                return result.ToList();
            }
        }

        /// <summary>
        /// Перенести заказы на таблицу времени
        /// </summary>
        /// <param name="timeSheets"></param>
        public async Task TransferOpenOrdersToTimeSheets(List<TimeSheet> timeSheets)
        {
            using (var db = PizzaAppDB.GetNewContext(120))
            {
                // Получить текущую сессию	
                var openSession = await SessionManager.GetOpened();

                // Выбрать все "открытые" заказы в перенесённой сессии
                var openOrders = await db.Orders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.StateID == 1 && x.Session.ID == openSession.ID).ToListAsync();

                // Пройти по всем открытым заказам	
                foreach (var openOrder in openOrders)
                {
                    // Пройти по всем резервациям
                    foreach (var timeSheet in timeSheets)
                    {
                        if (openOrder.ReservationGuid == timeSheet.ReservationGuid)
                        {
                            using (var db1 = PizzaAppDB.GetNewContext())
                            {
                                string query = $"update TimeSheets set Status='{timeSheet.Status}', ReservationGuid='{timeSheet.ReservationGuid}' where Time = '{timeSheet.Time}' and SectionID = '{timeSheet.SectionID}' and BranchId = '{BranchRepository.SelectedBranchId}'";
                                db1.Database.ExecuteSqlCommand(query);

                            }
                        }
                    }
                }
            }
        }

        public DataTable GetTimeSheets(List<TimeSheet> data)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var table = new DataTable();

                table.Columns.Add(new DataColumn("Time", typeof(DateTime)));
                var sections = db.Sections.Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
                sections.ForEach(x => table.Columns.Add(x.ID.ToString(CultureInfo.InvariantCulture)));

                var timeSheets = data.GroupBy(x => x.Time);
                var timeSheetStateMapper = new TimeSheetStateMapper();
                foreach (var timeSheet in timeSheets)
                {
                    var row = table.NewRow();
                    row.SetField("Time", timeSheet.Key);

                    foreach (var sheet in timeSheet)
                    {
                        row.SetField(sheet.SectionID.ToString(CultureInfo.InvariantCulture), timeSheetStateMapper.GetRus(sheet.Status));
                    }
                    table.Rows.Add(row);
                }


                return table;
            }

        }

        //TODO: Возможно стоит переписать запрос руками, на грамотный апдейт, чтобы не генерировать лишние запросы
        public void SetTimeCells(List<KeyValuePair<DateTime, int>> cells, string value, int pointOfSaleId, int brandId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var keys = cells.Select(y => y.Key);
                var values = cells.Select(j => j.Value);
                var timeSheets =
                    db.TimeSheets.Where(x => x.BranchId == BranchRepository.SelectedBranchId
                                             && x.BrandId == brandId
                                             && x.PointOfSaleId == pointOfSaleId).Where(
                        x =>
                        keys.Contains(x.Time) &&
                        values.Contains(x.SectionID)).ToList();

                timeSheets.ForEach(x =>
                {
                    x.ReservationDate = null;
                    x.ReservationGuid = null;
                    x.Status = value;
                    x.DateModified = DateTime.Now;
                    x.AuthorModified = UserManager.UserLogged.Name;
                });

                db.SaveChanges();
            }
        }
    }   

    internal static class FactorCollectionRepository
    {
        public static AmountFactorCollection GetFactorCollection(this Dish dish, int selectedBranchId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.AmountFactorCollections.Where(x => x.BranchId == selectedBranchId).FirstOrDefault(x => x.ID == dish.FactorCollectionID);
            }
        }
    }
}

