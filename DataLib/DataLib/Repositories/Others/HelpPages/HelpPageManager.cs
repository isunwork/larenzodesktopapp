﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DataLib.Models;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Others.HelpPages;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Others.HelpPages
{
    public class HelpPageManager : IHelpPageManager 
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        public IEnumerable<HelpPage> GetItems()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.HelpPages.Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
            }
        }

        public IEnumerable<HelpPage> GetItems(int t)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.HelpPages.Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
            }
        }

        public IEnumerable<HelpPage> GetItems(FilterModel filter)
        {
            throw new NotImplementedException();
        }

        public HelpPage GetItem(HelpPage entity)
        {
            throw new NotImplementedException();
        }

        public HelpPage GetItem(FilterModel filter)
        {
            throw new NotImplementedException();
        }

        public void UpdateItem(HelpPage entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void DeleteItem(HelpPage entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Entry(entity).State = EntityState.Deleted;
                db.SaveChanges();
            }
        }

        public void InsertItem(HelpPage entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    entity.BranchId = BranchRepository.SelectedBranchId;
                }
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                db.HelpPages.Add(entity);
                db.SaveChanges();
            }
        }

        public Task InsertItemAsync(HelpPage entity)
        {
            throw new NotImplementedException();
        }

        public void InsertOrUpdateItem(HelpPage entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (entity.ID != 0 && db.HelpPages.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Any(x => x.ID == entity.ID))
                {
                    entity.DateModified = DateTime.Now;
                    entity.AuthorModified = UserManager.UserLogged.Name;
                    db.Entry(entity).State = EntityState.Modified;
                }
                else
                {
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        entity.BranchId = BranchRepository.SelectedBranchId;
                    }
                    entity.DateModified = DateTime.Now;
                    entity.AuthorModified = UserManager.UserLogged.Name;
                    db.HelpPages.Add(entity);
                }
                db.SaveChanges();
            }
        }

        public bool InternalNameAllowed(HelpPage entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return !(db.HelpPages.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Any(x => x.ID != entity.ID &&
                                                  x.InternalName == entity.InternalName));

            }
        }

        public HelpPage GetExistingOrCreate(string internalName, string name)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var page = db.HelpPages.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.InternalName == internalName);

                if (page != null) return page;

                page = new HelpPage
                {
                    InternalName = internalName,
                    Name = name
                };

                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    page.BranchId = BranchRepository.SelectedBranchId;
                }
                page.DateModified = DateTime.Now;
                page.AuthorModified = UserManager.UserLogged.Name;
                page = db.HelpPages.Add(page);
                db.SaveChanges();

                return page;
            }
        }
    }
}
