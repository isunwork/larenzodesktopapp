﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Others.GlobalOptions;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Others.GlobalOptions
{
    public class CachedBaseOptionsFunctions: BaseOptionsFunctions
    {
        private List<GlobalOption> _cachedItemList { get; set; }

        private List<GlobalOption> GetAllItemsFromCache()
        {
            if (_cachedItemList == null)
            {
                _cachedItemList = base.GetItems(false).ToList();
                return _cachedItemList;
            }
            else
            {
                return _cachedItemList;
            }
        }

        //_________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Вернуть значение по имени </summary>
        ////==============================================
        private string GetValue(string optionName)
        {
            var option = GetAllItemsFromCache().Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.Name == optionName);

            if (option != null)
            {
                return option.Value;
            }
            else
            {
                throw new Exception(String.Format("Настройки с именем `{0}` не существует в базе данных", optionName));
            }
        }


        //_________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Вернуть значение по имени или пустую строку </summary>
        ////================================================================
        public override string GetValueOrEmptyString(string optionName)
        {
            string retVal = "";

            try
            {
                retVal = GetValue(optionName);
            }
            catch
            {

            }

            return retVal;
        }


        //_________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Вернуть значение по имени или 0 </summary>
        ////====================================================
        public override string GetValueOrZero(string optionName)
        {
            string retVal = "0";

            try
            {
                retVal = GetValue(optionName);
            }
            catch
            {

            }

            return retVal;
        }
    }
}
