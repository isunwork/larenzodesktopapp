﻿using System;
using System.Text;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Finance;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Others.GlobalOptions
{
    public class ClearDataManager: IClearDataManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }

        public void RemoveHistory(DateTime from)
        {
            UserManager.Logout();

            var scriptBuilder = new StringBuilder();
            var fromText = from.ToString("yyyyMMdd");
            
            using (var db = PizzaAppDB.GetNewContext())
            {
                AddToScript(scriptBuilder, $"DELETE FROM BankInteractions Where status='{(int)BankInteractionStatus.Sent}' and commandSent < '{fromText}' and BranchId = '{BranchRepository.SelectedBranchId}'");

                AddToScript(scriptBuilder, string.Format(
$@"
  -- Region Parameters
DECLARE @p0 DateTime = '{0}';
DECLARE @id int;
DECLARE @status varchar(30) = 'Processed';
-- EndRegion

if exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = '#TempRemains' AND TABLE_SCHEMA = 'dbo')
    drop table dbo.#TempRemains;

	UPDATE [dbo].[RemainsOfGoodWirings]
   SET [SupplyDocumentID] = NULL
      ,[ReturnDocumentID] = NULL
      ,[MovingDocumentID] = NULL
      ,[WriteoffDocumentID] = NULL
      ,[PostingDocumentID] = NULL
      ,[SessionDocumentID] = NULL
      ,[InventoryDocumentID] = NULL
      ,[WriteoffDishDocumentID] = NULL
      ,[IntermediateGoodWiringsId] = NULL
 WHERE Date < @p0 and BranchId = {BranchRepository.SelectedBranchId}

SELECT
	WarehouseID,
	ProductID,
	Sum(Amount) as Amount
  INTO #TempRemains
  FROM [RemainsOfGoodWirings]
  WHERE Date < @p0 and BranchId = {BranchRepository.SelectedBranchId}
  GROUP BY WarehouseID, ProductID
  

DELETE FROM [RemainsOfGoodWirings]
  WHERE Date < @p0 and BranchId = {BranchRepository.SelectedBranchId}
  
INSERT INTO [RemainsOfGoodWirings] (Date, WarehouseID, ProductID, Amount, BranchId)
  SELECT 
	@p0,
	WarehouseID,
	ProductID,
	Amount,
    {BranchRepository.SelectedBranchId}
  FROM #TempRemains WHERE Amount > 0

DROP TABLE #TempRemains


/* Удалить задачи на печать чеков */

Declare @OrderID int;
SELECT TOP(1) @OrderID = ID FROM [Orders] WHERE Created < @p0 and BranchId = {BranchRepository.SelectedBranchId} ORDER BY id DESC
EndSessionCheckPrint(@OrderID)

Declare @SessionID int;
SELECT TOP(1) @SessionID = ID FROM [Sessions] WHERE Opened < @p0 and BranchId = {BranchRepository.SelectedBranchId} ORDER BY id DESC
EndSessionCheckPrint(@SessionID)


Declare @PrintWorksID int;
SELECT TOP(1) @PrintWorksID = ID FROM [PrintWorks] WHERE (Created < @p0 OR OrderId <= @OrderID OR SessionId <= @SessionID) and BranchId = {BranchRepository.SelectedBranchId} ORDER BY id DESC
EndSessionCheckPrint(@PrintWorksID)

Delete FROM DetailPrintWorks WHERE [PrintWorksID] <= @PrintWorksID and BranchId = {BranchRepository.SelectedBranchId}
DELETE FROM PrintWorks WHERE ID <= @PrintWorksID and BranchId = {BranchRepository.SelectedBranchId}

	
/* ---------------------------- */

DELETE FROM PreOrders WHERE OrderId <= @OrderID and BranchId = {BranchRepository.SelectedBranchId}

/* ----------------------------- */

/* Удаление CafeOrderItems */
DELETE FROM
[CafeOrderDocumentItems]
WHERE EXISTS(
    SELECT NULL AS [EMPTY]
    FROM (
        SELECT [t1].[ID] AS [value], [t1].[SessionID]
        FROM [CafeOrderDocuments] AS [t1]
        ) AS [t2]
    WHERE ([t2].[value] = ([CafeOrderDocumentItems].[CafeOrderDocumentID])) AND (EXISTS(
        SELECT NULL AS [EMPTY]
        FROM (
            SELECT [t3].[ID] AS [value], [t3].[Opened]
            FROM [Sessions] AS [t3]
            ) AS [t4]
        WHERE ([t4].[value] = [t2].[SessionID]) AND ([t4].[Opened] < @p0)
        ))
    ) and BranchId = {BranchRepository.SelectedBranchId}

DELETE
FROM [CafeOrderDocuments]
WHERE EXISTS(
    SELECT NULL AS [EMPTY]
    FROM (
        SELECT [t1].[ID] AS [value], [t1].[Opened]
        FROM [Sessions] AS [t1]
        ) AS [t2]
    WHERE ([t2].[value] = [CafeOrderDocuments].[SessionID]) AND ([t2].[Opened] < @p0)
    ) and BranchId = {BranchRepository.SelectedBranchId}

/* Delete orderItems */

DELETE
FROM [OrderItems]
WHERE EXISTS(
    SELECT NULL AS [EMPTY]
    FROM (
        SELECT [t1].[ID] AS [value], [t1].[Created]
        FROM [Orders] AS [t1]
        ) AS [t2]
    WHERE ([t2].[value] = ([OrderItems].[OrderID])) AND ([t2].[Created] < @p0)
    ) and BranchId = {BranchRepository.SelectedBranchId}

DELETE FROM Orders WHERE Created < @p0 and BranchId = {BranchRepository.SelectedBranchId}


-- EndRegion
UPDATE [RemainsOfGoodWirings]
SET SessionDocumentID = NULL
WHERE EXISTS(
    SELECT NULL AS [EMPTY]
    FROM (
        SELECT [t1].[ID] AS [value], [t1].[Opened]
        FROM [Sessions] AS [t1]
        ) AS [t2]
    WHERE ([t2].[value] = [RemainsOfGoodWirings].[SessionDocumentID]) AND ([t2].[Opened] < @p0)
    ) and BranchId = {BranchRepository.SelectedBranchId}


DELETE FROM [Sessions] WHERE ID <= @SessionID and BranchId = {BranchRepository.SelectedBranchId}

/* Delete supply info */

DELETE
FROM [SettlsWithPartnerWirings]
WHERE EXISTS(
    SELECT NULL AS [EMPTY]
    FROM (
        SELECT [t1].[ID] AS [value], [t1].[PaySum], [t1].[DocumentSum], [t1].[DocumentDate]
        FROM [SupplyDocuments] AS [t1]
        ) AS [t2]
    WHERE ([t2].[value] = ([SettlsWithPartnerWirings].[SupplyDocumentID])) AND (ABS((CONVERT(Float,[t2].[PaySum])) - [t2].[DocumentSum]) < 1) AND ([t2].[DocumentDate] < @p0)
    ) and BranchId = {BranchRepository.SelectedBranchId}

-- EndRegion
DELETE
FROM [SupplyDocumentItems]
WHERE EXISTS(
    SELECT NULL AS [EMPTY]
    FROM (
        SELECT [t1].[ID] AS [value], [t1].[PaySum], [t1].[DocumentSum], [t1].[DocumentDate]
        FROM [SupplyDocuments] AS [t1]
        ) AS [t2]
    WHERE ([t2].[value] = ([SupplyDocumentItems].[SupplyDocumentID])) AND (ABS((CONVERT(Float,[t2].[PaySum])) - [t2].[DocumentSum]) < 1) AND ([t2].[DocumentDate] < @p0)
    ) and BranchId = {BranchRepository.SelectedBranchId}

DELETE FROM [SupplyDocuments] WHERE (ABS((CONVERT(Float,[PaySum])) - [DocumentSum]) < 1) AND ([DocumentDate] < @p0) and BranchId = {BranchRepository.SelectedBranchId}


/* Delete WriteOffs */

DELETE
FROM [WriteoffDishDocumentItems]
WHERE EXISTS(
    SELECT NULL AS [EMPTY]
    FROM (
        SELECT [t1].[ID] AS [value], [t1].[DocumentDate]
        FROM [WriteoffDishDocuments] AS [t1]
        ) AS [t2]
    WHERE ([t2].[value] = ([WriteoffDishDocumentItems].[WriteoffDishDocumentID])) AND ([t2].[DocumentDate] < @p0)
    ) and BranchId = {BranchRepository.SelectedBranchId}

DELETE FROM [WriteoffDishDocuments] WHERE ([DocumentDate] < @p0) and BranchId = {BranchRepository.SelectedBranchId}

DELETE
FROM [WriteoffDocumentItems]
WHERE EXISTS(
    SELECT NULL AS [EMPTY]
    FROM (
        SELECT [t1].[ID] AS [value], [t1].[DocumentDate]
        FROM [WriteoffDocuments] AS [t1]
        ) AS [t2]
    WHERE ([t2].[value] = ([WriteoffDocumentItems].[WriteoffDocumentID])) AND ([t2].[DocumentDate] < @p0)
    ) and BranchId = {BranchRepository.SelectedBranchId}

		
DELETE FROM [WriteoffDocuments] WHERE ([DocumentDate] < @p0) and BranchId = {BranchRepository.SelectedBranchId}

/* Delete Web Orders */



SELECT TOP(1) @id = [ID]
  FROM [WebOrders]
  WHERE Status = @status and BranchId = {BranchRepository.SelectedBranchId}
  Order by id desc

SET @id=@id-1000;
PRINT(@id)


DELETE FROM [WebOrderItems]
WHERE EXISTS(
    SELECT NULL AS [EMPTY]
    FROM (
        SELECT [t1].[ID] AS [value], [t1].[ID], [t1].[Status]
        FROM [WebOrders] AS [t1]
        ) AS [t2]
    WHERE ([t2].[value] = ([WebOrderItems].[WebOrderID])) AND ([t2].[ID] < @id) AND ([t2].[Status] = @status)
    ) and BranchId = {BranchRepository.SelectedBranchId}

Delete From [WebOrders] WHERE ([ID] < @id) AND ([Status] = @status) and BranchId = {BranchRepository.SelectedBranchId}
", fromText));

                Console.WriteLine(scriptBuilder.ToString());
                db.Database.CommandTimeout = 1500;
                db.Database.ExecuteSqlCommand(scriptBuilder.ToString());
            }
        }

        private void AddToScript(StringBuilder scriptBuilder, string statement)
        {
            scriptBuilder.Append(statement);
            scriptBuilder.Append(Environment.NewLine);
        }
    }


    
}
