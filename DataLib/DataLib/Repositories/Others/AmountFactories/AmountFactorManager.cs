﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Others.AmountFactories;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Others.AmountFactories
{
    public class AmountFactorManager: IAmountFactorManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        public List<AmountFactorCollection> GetList()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.AmountFactorCollections.Include("Data").Where(x=>x.BranchId == BranchRepository.SelectedBranchId).ToList();
            }
        }

        public void Add(string name)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var amountFactorCollection = new AmountFactorCollection { Name = name };
                if (BranchRepository.SelectedBranchId  != (int)BranchIdEnum.AllBranchId)
                {
                    amountFactorCollection.BranchId = BranchRepository.SelectedBranchId;
                }
                amountFactorCollection.DateModified = DateTime.Now;
                amountFactorCollection.AuthorModified = UserManager.UserLogged.Name;
                db.AmountFactorCollections.Add(amountFactorCollection);
                db.SaveChanges();
            }
        }

        public List<AmountFactor> GetFactors(int collectionId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.AmountFactors.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.ParentID == collectionId).ToList();
            }
        }

        public void RemoveCollection(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var entity = db.AmountFactorCollections.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == id);

                if (entity != null)
                {
                    db.Dishes.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.FactorCollectionID == entity.ID).ToList().ForEach(x => x.FactorCollectionID = null);
                    db.AmountFactorCollections.Remove(entity);
                    db.SaveChanges();
                }

            }
        }

        public void AddOrUpdateFactor(AmountFactor factor)
        {
            if (factor == null) throw new ArgumentNullException("factor");
            using (var db = PizzaAppDB.GetNewContext())
            {
                var tracked = db.AmountFactors.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == factor.ID);
                if (tracked != null)
                {
                    if (BranchRepository.SelectedBranchId  != (int)BranchIdEnum.AllBranchId)
                    {
                        factor.BranchId = BranchRepository.SelectedBranchId;
                    }
                    tracked.DateModified = DateTime.Now;
                    tracked.AuthorModified = UserManager.UserLogged.Name;
                    db.Entry(tracked).CurrentValues.SetValues(factor);
                }
                else
                {
                    if (BranchRepository.SelectedBranchId  != (int)BranchIdEnum.AllBranchId)
                    {
                        factor.BranchId = BranchRepository.SelectedBranchId;
                    }
                    factor.DateModified = DateTime.Now;
                    factor.AuthorModified = UserManager.UserLogged.Name;
                    db.AmountFactors.Add(factor);
                }
                db.SaveChanges();
            }

        }

        public void RemoveFactor(int factorId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var entity =
                    db.AmountFactors.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == factorId);
                if (entity != null)
                {
                    db.AmountFactors.Remove(entity);
                    db.SaveChanges();
                }

            }
        }

        public AmountFactorCollection GetCollectionById(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.AmountFactorCollections.Include("Data").Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == id);
            }
        }
    }
}
