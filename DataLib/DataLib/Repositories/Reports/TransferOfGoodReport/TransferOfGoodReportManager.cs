﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Repositories.Settings;
using LaRenzo.DataRepository.Repositories.Wiringses.RemainsOfGood;

namespace LaRenzo.DataRepository.Repositories.Reports.TransferOfGoodReport
{
    public class TransferOfGoodReportManager: ITransferOfGoodReportManager
    {
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public IRemainsOfGoodWiringsManager RemainsOfGoodWiringsManager { get; set; }
        public List<TransferOfGoodReportRecord> GetReportDate(DateTime start, DateTime end, int warehouseId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Database.CommandTimeout = 600;

                var result = new List<TransferOfGoodReportRecord>();

                //Новый код

                var products = db.Products.Include("Measure").Where(x=>x.BranchId == BranchRepository.SelectedBranchId).ToList();


                var productsAmountStartData = RemainsOfGoodWiringsManager.GetRemainsOnDate(warehouseId, start);
                var productsAmountEndData = RemainsOfGoodWiringsManager.GetRemainsOnDate(warehouseId, end);

                var productsAmountShiftData = db.RemainsOfGoodWirings.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(
                    x => x.Date >= start && x.Date <= end && x.WarehouseID == warehouseId).GroupBy(x => x.ProductID).
                    Select(x => new
                    {
                        productId = x.Key,
                        added = x.Any(a => a.Amount >= 0) ? x.Where(a => a.Amount >= 0).Sum(a => a.Amount) : 0,
                        deleted = x.Any(a => a.Amount <= 0) ?x.Where(a => a.Amount <= 0).Sum(a => a.Amount) : 0
                    }).ToList();

                products.ForEach(x => result.Add(new TransferOfGoodReportRecord
                {
                    Price = (decimal)x.FixPrice,
                    ProductAmmountAdded = productsAmountShiftData.Any(a => a.productId == x.ID) ? productsAmountShiftData.First(a => a.productId == x.ID).added : 0,
                    ProductAmmountDeleted = productsAmountShiftData.Any(a => a.productId == x.ID) ? productsAmountShiftData.First(a => a.productId == x.ID).deleted : 0,
                    ProductAmmountEnd = productsAmountEndData.ProductRemains.Any(a => a.ProductID == x.ID) ? productsAmountEndData.ProductRemains.First(a => a.ProductID == x.ID).Amount : 0,
                    ProductAmmountStart = productsAmountStartData.ProductRemains.Any(a => a.ProductID == x.ID) ? productsAmountStartData.ProductRemains.First(a => a.ProductID == x.ID).Amount : 0,
                    ProductId = x.ID,
                    ProductMeasure = x.Measure.ShortName,
                    ProductName = x.Name,
                    Sum = productsAmountEndData.ProductRemains.Any(a => a.ProductID == x.ID) ? productsAmountEndData.ProductRemains.First(a => a.ProductID == x.ID).Amount * (decimal)x.FixPrice : 0 
                }));                
                return result;
            }
        }
        
        public List<TransferOfGoodDetailReportRecord> GetDetailRecords(DateTime start, DateTime end, int warehouseId, int productId)
        {
            var listOfDocuments = new List<TransferOfGoodDetailReportRecord>();

            using (var db = PizzaAppDB.GetNewContext())
            {
                var wirings = warehouseId == -1
                                ? db.RemainsOfGoodWirings.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(
                                    x =>
                                    x.ProductID == productId && x.Date >= start && x.Date <= end).ToList()
                                : db.RemainsOfGoodWirings.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(
                                    x =>
                                    x.ProductID == productId && x.Date >= start && x.Date <= end &&
                                    x.WarehouseID == warehouseId).ToList();

                foreach (var ofGoodWiringse in wirings)
                {
                    var detailRecord = new TransferOfGoodDetailReportRecord();

                    if (ofGoodWiringse.Amount < 0)
                        detailRecord.ProductAmmountDeleted += ofGoodWiringse.Amount;
                    else
                        detailRecord.ProductAmmountAdded += ofGoodWiringse.Amount;

                    if (ofGoodWiringse.SupplyDocumentID != null)
                        detailRecord.DocumentName = string.Format("Поступление товаров №{0}", ofGoodWiringse.SupplyDocumentID);
                    else if (ofGoodWiringse.ReturnDocumentID != null)
                        detailRecord.DocumentName = string.Format("Возврат товаров №{0}", ofGoodWiringse.ReturnDocumentID);
                    else if (ofGoodWiringse.MovingDocumentID != null)
                        detailRecord.DocumentName = string.Format("Перемещение товаров №{0}", ofGoodWiringse.MovingDocumentID);
                    else if (ofGoodWiringse.WriteoffDocumentID != null)
                        detailRecord.DocumentName = string.Format("Списание товаров №{0}", ofGoodWiringse.WriteoffDocumentID);
                    else if (ofGoodWiringse.PostingDocumentID != null)
                        detailRecord.DocumentName = string.Format("Оприходование товаров №{0}", ofGoodWiringse.PostingDocumentID);
                    else if (ofGoodWiringse.SessionDocumentID != null)
                        detailRecord.DocumentName = string.Format("Закрытие смены №{0}", ofGoodWiringse.SessionDocumentID);
                    else if (ofGoodWiringse.InventoryDocumentID != null)
                        detailRecord.DocumentName = string.Format("Инвентаризация №{0}", ofGoodWiringse.InventoryDocumentID);
                    else
                        detailRecord.DocumentName = string.Format("Списание блюд №{0}", ofGoodWiringse.WriteoffDishDocumentID);

                    listOfDocuments.Add(detailRecord);
                }

                return listOfDocuments;
            }           
        }
    }
}
