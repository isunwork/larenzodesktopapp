﻿using System.Collections.Generic;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Partners;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Reports.PartnersProduct
{
    public class PartnersProductManager: IPartnersProductManager
    {
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        
        public List<Partner> GetReportData()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Partners.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).ToList();
            }
        }

        public List<PartnerOfProduct> GetDetailRecords(int partnerId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.PartnerOfProducts.Include("Product").Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.PartnerId == partnerId).ToList();
            }
        }
    }
}

