﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Reports.DishCombinationReport
{
    public class DishCombinationRepository: IDishCombinationRepository
    {
        [Inject]
        public IBranchRepository BranchRepository { get; set; }

        public List<DishCombinationSource> GetMostPopularDishCombination(int dishAmount)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var temp = db.OrderItems.Where(x => x.BranchId == BranchRepository.SelectedBranchId).GroupBy(x => x.OrderID).Select(x => new
                {
                    OrderID = x.Key,
                    DishList = x.Select(a => a.DishID).ToList()
                }).Where(x => x.DishList.Count >= dishAmount).ToList();

                var data = temp.Select(x => new DishCombinationSource
                {
                    OrderId = x.OrderID,
                    DishList = x.DishList
                }).ToList();
                return data;
            }
        }

        public List<Dish> GetActualDishList(IEnumerable<int> categories)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Dishes.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => !x.IsDeleted && categories.Contains(x.CategoryID)).ToList();
            }
        }
    }







}

