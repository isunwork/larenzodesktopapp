﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DataLib.Models;
using DataRepository.Entities;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Branch;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.Interfaces;

namespace LaRenzo.DataRepository.Repositories.Settings
{
    
    public class BranchRepository: IBranchRepository
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IThreadLocalStorage ThreadLocalStorage { get; set; }

        /// <summary>
        /// Выбранный филиал при входе
        /// </summary>
        public int SelectedBranchId
        {
            get => ThreadLocalStorage.SelectedBranchId;
            set => ThreadLocalStorage.SelectedBranchId = value;
        }
        
        public List<Branch> GetList(FilterModel filter, bool withBranch = true)
        {
            var value = filter as BranchFilter;
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = db.Set<Branch>().AsQueryable();
                if (value?.NotID > 0)
                    result = result.Where(x => x.ID != value.NotID);
                if (withBranch)
                {
                    if (SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        return result.Where(x => x.BranchId == SelectedBranchId || x.BranchId == 0).ToList();
                    }
                }
                return result.ToList();
            }
                
        }

        public List<Branch> GetItems()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = db.Set<Branch>();
                return result.ToList();
            }
        }
    }
}
