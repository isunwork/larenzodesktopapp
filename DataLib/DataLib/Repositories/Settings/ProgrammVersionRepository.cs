﻿using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using System.Collections.Generic;
using System.Linq;

namespace LaRenzo.DataRepository.Repositories.Settings
{
    public class ProgrammVersionRepository: IProgrammVersionRepository
    {
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        public List<ProgrammVersion> GetItems(bool withBranch = true)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = db.Set<ProgrammVersion>();
                if (withBranch)
                {
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        return result.Where(x => x.BranchId == BranchRepository.SelectedBranchId || x.BranchId == 0).ToList();
                    }
                }
                else
                {
                    return result.ToList();
                }
                return result.ToList();
            }
            
        }
    }
}
