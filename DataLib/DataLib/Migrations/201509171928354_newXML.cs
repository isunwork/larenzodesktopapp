namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class newXML : DbMigration
    {
        public override void Up()
        {
			AddColumn("dbo.Orders", "DeliveryMethod", c => c.Int(nullable: false));
			AddColumn("dbo.WebOrder", "DeliveryMethod", c => c.Int(nullable: false));
			
        }
        
        public override void Down()
        {
			DropColumn("dbo.Orders", "DeliveryMethod");
			DropColumn("dbo.WebOrder", "DeliveryMethod");
        }
    }
}
