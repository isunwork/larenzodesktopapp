namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class fildSources : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WebOrders", "Source", c => c.String());
			//AddColumn("dbo.OrderInfo", "Source", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.WebOrders", "Source");
			//DropColumn("dbo.OrderInfo", "Source");
        }
    }
}