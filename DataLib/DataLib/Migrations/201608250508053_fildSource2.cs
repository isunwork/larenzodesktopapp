namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class fildSource2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "Source", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "Source");
        }
    }
}
