namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Init210115 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HelpPages",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        InternalName = c.String(),
                        Name = c.String(),
                        Content = c.String(),
                        ParentID = c.Int(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.HelpPages");
        }
    }
}
