namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddWarehouseForDish : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WarehouseForDishes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuthorModified = c.String(),
                        DateModified = c.DateTime(precision: 7, storeType: "datetime2"),
                        Name = c.String(),
                        BranchId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Warehouses", "WarehouseForDishId", c => c.Int());
            AddColumn("dbo.Dishes", "WarehouseForDishId", c => c.Int());
            CreateIndex("dbo.Warehouses", "WarehouseForDishId");
            CreateIndex("dbo.Dishes", "WarehouseForDishId");
            AddForeignKey("dbo.Warehouses", "WarehouseForDishId", "dbo.WarehouseForDishes", "ID");
            AddForeignKey("dbo.Dishes", "WarehouseForDishId", "dbo.WarehouseForDishes", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Dishes", "WarehouseForDishId", "dbo.WarehouseForDishes");
            DropForeignKey("dbo.Warehouses", "WarehouseForDishId", "dbo.WarehouseForDishes");
            DropIndex("dbo.Dishes", new[] { "WarehouseForDishId" });
            DropIndex("dbo.Warehouses", new[] { "WarehouseForDishId" });
            DropColumn("dbo.Dishes", "WarehouseForDishId");
            DropColumn("dbo.Warehouses", "WarehouseForDishId");
            DropTable("dbo.WarehouseForDishes");
        }
    }
}
