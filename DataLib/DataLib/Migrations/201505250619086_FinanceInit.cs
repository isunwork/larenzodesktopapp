namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class FinanceInit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PaymentCategories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Parent = c.Int(nullable: false),
                        Name = c.String(),
                        IsImmutable = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Payments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        CategoryID = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Comment = c.String(),
                        WalletID = c.Int(nullable: false),
                        TransferID = c.Guid(),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsActive = c.Boolean(nullable: false),
                        IsImmutable = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PaymentCategories", t => t.CategoryID, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .ForeignKey("dbo.Wallets", t => t.WalletID, cascadeDelete: true)
                .Index(t => t.UserID)
                .Index(t => t.CategoryID)
                .Index(t => t.WalletID);
            
            CreateTable(
                "dbo.Wallets",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Payments", "WalletID", "dbo.Wallets");
            DropForeignKey("dbo.Payments", "UserID", "dbo.Users");
            DropForeignKey("dbo.Payments", "CategoryID", "dbo.PaymentCategories");
            DropIndex("dbo.Payments", new[] { "WalletID" });
            DropIndex("dbo.Payments", new[] { "CategoryID" });
            DropIndex("dbo.Payments", new[] { "UserID" });
            DropTable("dbo.Wallets");
            DropTable("dbo.Payments");
            DropTable("dbo.PaymentCategories");
        }



        
    }
}
