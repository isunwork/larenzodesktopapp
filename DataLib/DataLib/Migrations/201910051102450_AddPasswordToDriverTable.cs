namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPasswordToDriverTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Drivers", "Password", c => c.String());
            Sql("UPDATE dbo.Drivers SET Password='c31ac605793f580b386c0fb53f1b9775'");//12345
        }
        
        public override void Down()
        {
            DropColumn("dbo.Drivers", "Password");
        }
    }
}
