namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBrandToOptions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GlobalOptions", "BrandId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.GlobalOptions", "BrandId");
        }
    }
}
