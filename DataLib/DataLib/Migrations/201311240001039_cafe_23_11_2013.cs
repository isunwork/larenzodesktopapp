namespace DataLib.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class cafe_23_11_2013 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderItems", "IsEditable", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderItems", "IsEditable");
        }
    }
}
