namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class RemoveOrderFromBankInteraction : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BankInteractions", "OrderId", "dbo.Orders");
            DropIndex("dbo.BankInteractions", new[] { "OrderId" });
            DropColumn("dbo.BankInteractions", "OrderId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BankInteractions", "OrderId", c => c.Int(nullable: false));
            CreateIndex("dbo.BankInteractions", "OrderId");
            AddForeignKey("dbo.BankInteractions", "OrderId", "dbo.Orders", "ID", cascadeDelete: true);
        }
    }
}
