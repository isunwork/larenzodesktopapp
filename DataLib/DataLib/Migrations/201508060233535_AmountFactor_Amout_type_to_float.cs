namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AmountFactor_Amout_type_to_float : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AmountFactors", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AmountFactors", "Amount", c => c.Int(nullable: false));
        }
    }
}
