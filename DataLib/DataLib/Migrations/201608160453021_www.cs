namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class www : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "Is5k", c => c.Boolean(nullable: false));
            AddColumn("dbo.Orders", "IsUnCash", c => c.Boolean(nullable: false));
            AddColumn("dbo.Orders", "IsClientPaid", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "IsClientPaid");
            DropColumn("dbo.Orders", "IsUnCash");
            DropColumn("dbo.Orders", "Is5k");
        }
    }
}
