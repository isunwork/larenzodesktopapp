namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddActiveColumnToPointOfSale : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("dbo.Addresses", "PointOfSaleId", "dbo.PointOfSales");
            //DropIndex("dbo.Addresses", new[] { "PointOfSaleId" });
            //DropColumn("dbo.Addresses", "PointOfSaleId");
            AddColumn("dbo.PointOfSales", "Active", c => c.Boolean(nullable: false));
            Sql("update PointOfSales set Active = 1;");
        }
        
        public override void Down()
        {
            //AddColumn("dbo.Addresses", "PointOfSaleId", c => c.Int(nullable: false));
            //CreateIndex("dbo.Addresses", "PointOfSaleId");
            //AddForeignKey("dbo.Addresses", "PointOfSaleId", "dbo.PointOfSales", "ID", cascadeDelete: true);
            DropColumn("dbo.PointOfSales", "Active");
        }
    }
}
