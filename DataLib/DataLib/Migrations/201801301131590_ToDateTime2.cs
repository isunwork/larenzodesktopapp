namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ToDateTime2 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.TimeSheets");
            AlterColumn("dbo.AveragePriceWirings", "Date", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.SupplyDocuments", "DocumentDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.SupplyDocuments", "PayDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.SupplyDocuments", "InputDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.BankInteractions", "CommandCreated", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.BankInteractions", "CommandSent", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.BlackListItems", "Created", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.CafeOrderDocuments", "Created", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Orders", "Created", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Orders", "TimeClient", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            DropIndex("dbo.Orders","IX_Orders_TimeKitchen");
            AlterColumn("dbo.Orders", "TimeKitchen", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            CreateIndex("dbo.Orders", "TimeKitchen", false, "IX_Orders_TimeKitchen", false);
            AlterColumn("dbo.Sessions", "Opened", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Sessions", "Closed", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.CallDatas", "Created", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.CallDatas", "Answed", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.CallDatas", "Droped", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.PrintWorks", "Created", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.PrintWorks", "Runned", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.IntermediateGoodWirings", "DateTime", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.InventoryDocuments", "DocumentDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Messages", "Created", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.MovingDocuments", "DocumentDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Payments", "Created", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.PostingDocuments", "DocumentDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.PreOrders", "ReservationDateTime", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.RemainsOfGoodWirings", "Date", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.ReturnDocuments", "DocumentDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.WriteoffDishDocuments", "DocumentDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.WriteoffDocuments", "DocumentDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.SettlsWithPartnerWirings", "Date", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.TimeSheets", "Time", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.TimeSheets", "ReservationDate", c => c.DateTime(precision: 7, storeType: "datetime2"));
            Sql("ALTER TABLE dbo.WebOrders DROP CONSTRAINT DF__WebOrders__Creat__4183B671");
            AlterColumn("dbo.WebOrders", "CreateDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AddPrimaryKey("dbo.TimeSheets", new[] { "Time", "SectionID" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.TimeSheets");
            Sql("ALTER TABLE dbo.WebOrders DROP CONSTRAINT DF_dbo.WebOrders_CreateDate");
            AlterColumn("dbo.WebOrders", "CreateDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.TimeSheets", "ReservationDate", c => c.DateTime());
            AlterColumn("dbo.TimeSheets", "Time", c => c.DateTime(nullable: false));
            AlterColumn("dbo.SettlsWithPartnerWirings", "Date", c => c.DateTime(nullable: false));
            AlterColumn("dbo.WriteoffDocuments", "DocumentDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.WriteoffDishDocuments", "DocumentDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.ReturnDocuments", "DocumentDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.RemainsOfGoodWirings", "Date", c => c.DateTime(nullable: false));
            AlterColumn("dbo.PreOrders", "ReservationDateTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.PostingDocuments", "DocumentDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Payments", "Created", c => c.DateTime(nullable: false));
            AlterColumn("dbo.MovingDocuments", "DocumentDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Messages", "Created", c => c.DateTime(nullable: false));
            AlterColumn("dbo.InventoryDocuments", "DocumentDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.IntermediateGoodWirings", "DateTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.PrintWorks", "Runned", c => c.DateTime());
            AlterColumn("dbo.PrintWorks", "Created", c => c.DateTime(nullable: false));
            AlterColumn("dbo.CallDatas", "Droped", c => c.DateTime(nullable: false));
            AlterColumn("dbo.CallDatas", "Answed", c => c.DateTime(nullable: false));
            AlterColumn("dbo.CallDatas", "Created", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Sessions", "Closed", c => c.DateTime());
            AlterColumn("dbo.Sessions", "Opened", c => c.DateTime(nullable: false));
            DropIndex("dbo.Orders","IX_Orders_TimeKitchen");
            AlterColumn("dbo.Orders", "TimeKitchen", c => c.DateTime(nullable: false));
            CreateIndex("dbo.Orders", "TimeKitchen", false, "IX_Orders_TimeKitchen", false);
            AlterColumn("dbo.Orders", "TimeClient", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Orders", "Created", c => c.DateTime(nullable: false));
            AlterColumn("dbo.CafeOrderDocuments", "Created", c => c.DateTime(nullable: false));
            AlterColumn("dbo.BlackListItems", "Created", c => c.DateTime(nullable: false));
            AlterColumn("dbo.BankInteractions", "CommandSent", c => c.DateTime());
            AlterColumn("dbo.BankInteractions", "CommandCreated", c => c.DateTime(nullable: false));
            AlterColumn("dbo.SupplyDocuments", "InputDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.SupplyDocuments", "PayDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.SupplyDocuments", "DocumentDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.AveragePriceWirings", "Date", c => c.DateTime(nullable: false));
            AddPrimaryKey("dbo.TimeSheets", new[] { "Time", "SectionID" });
        }
    }
}
