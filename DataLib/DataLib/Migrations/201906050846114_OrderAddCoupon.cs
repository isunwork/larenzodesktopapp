namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderAddCoupon : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "CouponId", c => c.Int());
            CreateIndex("dbo.Orders", "CouponId");
            AddForeignKey("dbo.Orders", "CouponId", "dbo.Coupons", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "CouponId", "dbo.Coupons");
            DropIndex("dbo.Orders", new[] { "CouponId" });
            DropColumn("dbo.Orders", "CouponId");
        }
    }
}
