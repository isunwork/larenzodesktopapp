namespace DataLib.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class cafe_13_12_2013 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sessions", "TotalCafeOrderCount", c => c.Int());
            AddColumn("dbo.Sessions", "TotalCafeRevenue", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Orders", "IsWebRequest", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "IsWebRequest");
            DropColumn("dbo.Sessions", "TotalCafeRevenue");
            DropColumn("dbo.Sessions", "TotalCafeOrderCount");
        }
    }
}
