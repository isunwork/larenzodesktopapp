namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeCouponRule2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CouponRuleElements", "ElementApplyDirection", c => c.Int(nullable: false));
            AddColumn("dbo.CouponRules", "FixDiscount", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CouponRules", "FixDiscount");
            DropColumn("dbo.CouponRuleElements", "ElementApplyDirection");
        }
    }
}
