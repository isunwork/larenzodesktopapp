namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Street2string : DbMigration
    {
        public override void Up()
        {
            /*
            AlterColumn("dbo.DiscountCards", "House", c => c.String());
            AlterColumn("dbo.GlobalOptions", "Value", c => c.String(maxLength: 400));
             */
        }
        
        public override void Down()
        {
            AlterColumn("dbo.GlobalOptions", "Value", c => c.String(maxLength: 500));
            AlterColumn("dbo.DiscountCards", "House", c => c.Int());
        }
    }
}
