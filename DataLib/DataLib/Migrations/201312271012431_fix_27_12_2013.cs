namespace DataLib.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class fix_27_12_2013 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sessions", "CanEditTime", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sessions", "CanEditTime");
        }
    }
}
