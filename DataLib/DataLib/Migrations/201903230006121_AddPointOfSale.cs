namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPointOfSale : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PointOfSales",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuthorModified = c.String(),
                        DateModified = c.DateTime(precision: 7, storeType: "datetime2"),
                        PointName = c.String(),
                        BranchId = c.Int(nullable: false),
                        BrandId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PointOfSales");
        }
    }
}
