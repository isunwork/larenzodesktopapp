namespace DataLib.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class fix_29_01_2014_1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WebOrders", "IsNewSiteOrder", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WebOrders", "IsNewSiteOrder");
        }
    }
}
