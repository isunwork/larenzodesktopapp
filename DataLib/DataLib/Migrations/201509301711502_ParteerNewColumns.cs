namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ParteerNewColumns : DbMigration
    {
        public override void Up()
        {
			AddColumn("dbo.Partner", "PartnerContactFace", c => c.String());
			AddColumn("dbo.Partner", "PartnerRemarkDayOrder", c => c.String());
        }
        
        public override void Down()
        {
			DropColumn("dbo.Partner", "PartnerContactFace");
			DropColumn("dbo.Partner", "PartnerRemarkDayOrder");
        }
    }
}
