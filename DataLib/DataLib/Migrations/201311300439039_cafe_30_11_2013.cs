namespace DataLib.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class cafe_30_11_2013 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Dishes", "IsAlcohole", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Dishes", "IsAlcohole");
        }
    }
}
