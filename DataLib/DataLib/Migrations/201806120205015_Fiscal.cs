namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Fiscal : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "FiscalCheckPrinted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "FiscalCheckPrinted");
        }
    }
}
