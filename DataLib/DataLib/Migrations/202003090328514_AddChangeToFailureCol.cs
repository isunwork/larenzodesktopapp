namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddChangeToFailureCol : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "ChangeToFailure", c => c.DateTime(precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "ChangeToFailure");
        }
    }
}
