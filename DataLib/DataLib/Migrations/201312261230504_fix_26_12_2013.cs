namespace DataLib.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class fix_26_12_2013 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SupplyDocuments", "Note", c => c.String());
            AddColumn("dbo.ReturnDocuments", "Note", c => c.String());
            AddColumn("dbo.MovingDocuments", "Note", c => c.String());
            AddColumn("dbo.WriteoffDocuments", "Note", c => c.String());
            AddColumn("dbo.PostingDocuments", "Note", c => c.String());
            AddColumn("dbo.InventoryDocuments", "Note", c => c.String());
            AddColumn("dbo.WriteoffDishDocuments", "Note", c => c.String());
            AddColumn("dbo.CafeOrderDocuments", "Note", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CafeOrderDocuments", "Note");
            DropColumn("dbo.WriteoffDishDocuments", "Note");
            DropColumn("dbo.InventoryDocuments", "Note");
            DropColumn("dbo.PostingDocuments", "Note");
            DropColumn("dbo.WriteoffDocuments", "Note");
            DropColumn("dbo.MovingDocuments", "Note");
            DropColumn("dbo.ReturnDocuments", "Note");
            DropColumn("dbo.SupplyDocuments", "Note");
        }
    }
}
