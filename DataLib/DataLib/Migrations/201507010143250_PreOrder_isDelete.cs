namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PreOrder_isDelete : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PreOrders", "IsDeleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PreOrders", "IsDeleted");
        }
    }
}
