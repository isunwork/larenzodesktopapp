namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFailureReasonToOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "FailureReason", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "FailureReason");
        }
    }
}
