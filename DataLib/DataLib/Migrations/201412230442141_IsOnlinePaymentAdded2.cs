namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class IsOnlinePaymentAdded2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CafeOrderDocuments", "IsOnlinePayment", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CafeOrderDocuments", "IsOnlinePayment");
        }
    }
}
