namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CommandSentNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BankInteractions", "CommandSent", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BankInteractions", "CommandSent", c => c.DateTime(nullable: false));
        }
    }
}
