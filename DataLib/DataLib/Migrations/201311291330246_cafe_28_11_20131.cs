namespace DataLib.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class cafe_28_11_20131 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CafeOrderDocumentItems", "Price", c => c.Int(nullable: false));
            AddColumn("dbo.CafeOrderDocumentItems", "Sum", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CafeOrderDocumentItems", "Sum");
            DropColumn("dbo.CafeOrderDocumentItems", "Price");
        }
    }
}
