namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAdditionalOfferDishesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdditionalOfferDishes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuthorModified = c.String(),
                        DateModified = c.DateTime(precision: 7, storeType: "datetime2"),
                        BranchId = c.Int(nullable: false),
                        DishId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Dishes", t => t.DishId, cascadeDelete: false)
                .Index(t => t.DishId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AdditionalOfferDishes", "DishId", "dbo.Dishes");
            DropIndex("dbo.AdditionalOfferDishes", new[] { "DishId" });
            DropTable("dbo.AdditionalOfferDishes");
        }
    }
}
