namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPointOfSaleUpdate : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.PointOfSales", "BranchId");
            CreateIndex("dbo.PointOfSales", "BrandId");
            AddForeignKey("dbo.PointOfSales", "BranchId", "dbo.Branches", "ID", cascadeDelete: true);
            AddForeignKey("dbo.PointOfSales", "BrandId", "dbo.Brands", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PointOfSales", "BrandId", "dbo.Brands");
            DropForeignKey("dbo.PointOfSales", "BranchId", "dbo.Branches");
            DropIndex("dbo.PointOfSales", new[] { "BrandId" });
            DropIndex("dbo.PointOfSales", new[] { "BranchId" });
        }
    }
}
