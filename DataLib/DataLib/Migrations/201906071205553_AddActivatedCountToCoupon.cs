namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddActivatedCountToCoupon : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Coupons", "ActivatedCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Coupons", "ActivatedCount");
        }
    }
}
