namespace DataLib.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class fix_01_01_2014_1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "MinAmount", c => c.Decimal(nullable: false, precision: 18, scale: 3));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "MinAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
