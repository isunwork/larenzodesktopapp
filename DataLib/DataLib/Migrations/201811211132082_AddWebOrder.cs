namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddWebOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "WebOrderId", c => c.Int(nullable: false));
            AddColumn("dbo.WebOrders", "WebOrderId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WebOrders", "WebOrderId");
            DropColumn("dbo.Orders", "WebOrderId");
        }
    }
}
