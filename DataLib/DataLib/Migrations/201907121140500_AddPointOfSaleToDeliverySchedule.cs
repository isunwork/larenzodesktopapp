namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPointOfSaleToDeliverySchedule : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DeliverySchedules", "PointOfSaleId", c => c.Int());
            AddColumn("dbo.DeliverySchedules", "Priority", c => c.Int(nullable: false));
            CreateIndex("dbo.DeliverySchedules", "PointOfSaleId");
            AddForeignKey("dbo.DeliverySchedules", "PointOfSaleId", "dbo.PointOfSales", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DeliverySchedules", "PointOfSaleId", "dbo.PointOfSales");
            DropIndex("dbo.DeliverySchedules", new[] { "PointOfSaleId" });
            DropColumn("dbo.DeliverySchedules", "Priority");
            DropColumn("dbo.DeliverySchedules", "PointOfSaleId");
        }
    }
}
