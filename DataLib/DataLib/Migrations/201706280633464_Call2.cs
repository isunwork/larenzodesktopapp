namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Call2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallDatas", "StatusName", c => c.String());
            AddColumn("dbo.CallDatas", "IsVip", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CallDatas", "IsVip");
            DropColumn("dbo.CallDatas", "StatusName");
        }
    }
}
