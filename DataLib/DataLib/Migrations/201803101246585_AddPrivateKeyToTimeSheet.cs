namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPrivateKeyToTimeSheet : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.TimeSheets");
            AddPrimaryKey("dbo.TimeSheets", new[] { "Time", "SectionID", "BranchId" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.TimeSheets");
            AddPrimaryKey("dbo.TimeSheets", new[] { "Time", "SectionID" });
        }
    }
}
