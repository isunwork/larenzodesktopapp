namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDeliveryPointOfSales : DbMigration
    {
        public override void Up()
        {
            Sql(@"update d set d.PointOfSaleId= a.PointOfSaleId, d.Priority = 1
from Addresses a 
	join DeliverySchedules d on a.AddressGroupID = d.AddressGroupID");
        }
        
        public override void Down()
        {
        }
    }
}
