namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCoouponCodeToWebOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WebOrders", "CouponCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.WebOrders", "CouponCode");
        }
    }
}
