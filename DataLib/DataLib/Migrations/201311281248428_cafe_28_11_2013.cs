namespace DataLib.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class cafe_28_11_2013 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderItems", "CaffeOrderDocument_ID", "dbo.CaffeOrderDocuments");
            DropForeignKey("dbo.CaffeOrderDocuments", "SessionID", "dbo.Sessions");
            DropForeignKey("dbo.CaffeOrderDocuments", "TableID", "dbo.Tables");
            DropIndex("dbo.OrderItems", new[] { "CaffeOrderDocument_ID" });
            DropIndex("dbo.CaffeOrderDocuments", new[] { "SessionID" });
            DropIndex("dbo.CaffeOrderDocuments", new[] { "TableID" });
            CreateTable(
                "dbo.CafeOrderDocuments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Created = c.DateTime(nullable: false),
                        Code = c.String(),
                        SessionID = c.Int(),
                        TableID = c.Int(),
                        DocumentSum = c.Decimal(nullable: false, precision: 18, scale: 2),
                        User = c.String(),
                        IsProcessed = c.Boolean(nullable: false),
                        IsMarkToDelete = c.Boolean(nullable: false),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Sessions", t => t.SessionID)
                .ForeignKey("dbo.Tables", t => t.TableID)
                .Index(t => t.SessionID)
                .Index(t => t.TableID);
            
            CreateTable(
                "dbo.CafeOrderDocumentItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DishID = c.Int(nullable: false),
                        Amount = c.Int(nullable: false),
                        IsEditable = c.Boolean(nullable: false),
                        CafeOrderDocumentID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Dishes", t => t.DishID, cascadeDelete: true)
                .ForeignKey("dbo.CafeOrderDocuments", t => t.CafeOrderDocumentID, cascadeDelete: true)
                .Index(t => t.DishID)
                .Index(t => t.CafeOrderDocumentID);
            
            AddColumn("dbo.Dishes", "CafePrice", c => c.Int(nullable: false));
            DropColumn("dbo.OrderItems", "CaffeOrderDocument_ID");
            DropTable("dbo.CaffeOrderDocuments");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.CaffeOrderDocuments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Created = c.DateTime(nullable: false),
                        Code = c.String(),
                        SessionID = c.Int(),
                        TableID = c.Int(),
                        DocumentSum = c.Decimal(nullable: false, precision: 18, scale: 2),
                        User = c.String(),
                        IsProcessed = c.Boolean(nullable: false),
                        IsMarkToDelete = c.Boolean(nullable: false),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.OrderItems", "CaffeOrderDocument_ID", c => c.Int());
            DropIndex("dbo.CafeOrderDocumentItems", new[] { "CafeOrderDocumentID" });
            DropIndex("dbo.CafeOrderDocumentItems", new[] { "DishID" });
            DropIndex("dbo.CafeOrderDocuments", new[] { "TableID" });
            DropIndex("dbo.CafeOrderDocuments", new[] { "SessionID" });
            DropForeignKey("dbo.CafeOrderDocumentItems", "CafeOrderDocumentID", "dbo.CafeOrderDocuments");
            DropForeignKey("dbo.CafeOrderDocumentItems", "DishID", "dbo.Dishes");
            DropForeignKey("dbo.CafeOrderDocuments", "TableID", "dbo.Tables");
            DropForeignKey("dbo.CafeOrderDocuments", "SessionID", "dbo.Sessions");
            DropColumn("dbo.Dishes", "CafePrice");
            DropTable("dbo.CafeOrderDocumentItems");
            DropTable("dbo.CafeOrderDocuments");
            CreateIndex("dbo.CaffeOrderDocuments", "TableID");
            CreateIndex("dbo.CaffeOrderDocuments", "SessionID");
            CreateIndex("dbo.OrderItems", "CaffeOrderDocument_ID");
            AddForeignKey("dbo.CaffeOrderDocuments", "TableID", "dbo.Tables", "ID");
            AddForeignKey("dbo.CaffeOrderDocuments", "SessionID", "dbo.Sessions", "ID");
            AddForeignKey("dbo.OrderItems", "CaffeOrderDocument_ID", "dbo.CaffeOrderDocuments", "ID");
        }
    }
}
