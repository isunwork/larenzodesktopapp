namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeCouponRule : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CouponRules", "Price", c => c.Int());
            AlterColumn("dbo.CouponRules", "Percent", c => c.Int());
            AlterColumn("dbo.CouponRules", "SumToApply", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CouponRules", "SumToApply", c => c.Int(nullable: false));
            AlterColumn("dbo.CouponRules", "Percent", c => c.Int(nullable: false));
            AlterColumn("dbo.CouponRules", "Price", c => c.Int(nullable: false));
        }
    }
}
