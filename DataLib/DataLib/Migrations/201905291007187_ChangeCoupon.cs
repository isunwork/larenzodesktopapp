namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeCoupon : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Coupons", "ActivationCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Coupons", "ActivationCount");
        }
    }
}
