namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddToWarehouse : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Warehouses", "PointOfSaleId", c => c.Int(nullable: false, defaultValue: 1));
            CreateIndex("dbo.Warehouses", "PointOfSaleId");
            AddForeignKey("dbo.Warehouses", "PointOfSaleId", "dbo.PointOfSales", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Warehouses", "PointOfSaleId", "dbo.PointOfSales");
            DropIndex("dbo.Warehouses", new[] { "PointOfSaleId" });
            DropColumn("dbo.Warehouses", "PointOfSaleId");
        }
    }
}
