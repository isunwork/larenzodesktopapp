namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class order_person_count : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "PersonsCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "PersonsCount");
        }
    }

	// ALTER TABLE [dbo].[Orders] ADD [PersonsCount] [int] NOT NULL DEFAULT 0
}
